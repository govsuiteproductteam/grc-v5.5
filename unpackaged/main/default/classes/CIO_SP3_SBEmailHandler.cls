global with sharing class CIO_SP3_SBEmailHandler implements Messaging.InboundEmailHandler {
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();           
        if(Schema.sObjectType.Task_Order__c.isAccessible() && Schema.sObjectType.Contract_Vehicle__c.isAccessible() && Schema.sObjectType.Contact.isAccessible()){ 
        String myPlainText = email.HTMLBody;
        CIO_SP3_SBEmailParser obj = new CIO_SP3_SBEmailParser();
        Task_Order__c taskOrder = obj.omaParser(myPlainText,email.toAddresses.get(0));
        String subject = ''+email.subject;
        
        if(subject!=Null && subject!=''){
            String torName;
            torName = subject.substring(subject.indexOf('Support_')+8).trim();
            if(torName.length()>=80){
                taskOrder.Name = torName.substring(0,78);
            }else{
                taskOrder.Name = torName;
            }               
        }else{
            taskOrder.Name = 'Temp_1234';
        }
        try{
            DMLManager.insertAsUser(taskOrder);
            EmailHandlerHelper.insertBinaryAttachment(taskOrder,email);
            EmailHandlerHelper.insertTextAttachment(taskOrder,email);
            EmailHandlerHelper.insertActivityHistory(taskOrder,Email);
        }catch(Exception e){
            System.debug('Exception  = '+e);
        }
        }
        return result;
    }
    
    
}