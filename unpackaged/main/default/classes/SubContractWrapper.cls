public class SubContractWrapper {
    @AuraEnabled
    public List<CLMTabWraper> tabWrapList{get;set;}
    @AuraEnabled
    public Subcontract__c contractObj{get;set;}   
    @AuraEnabled
    public Boolean hasNoTabs{get;set;}  
    @AuraEnabled
    public Boolean doUserHaveEditPermission{get;set;}
    @AuraEnabled
    public Boolean doUserHaveCreatePermission{get;set;}// newly added @ 22/05/2019 for Clone if user dont have Create Permis
    @AuraEnabled
    public List<CLMTabListAndDataWrapper> allTabsNameWrpList{get;set;}
    
}