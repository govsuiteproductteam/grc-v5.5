({
    doCheckLicense : function(component, event, helper) {
        console.log('doCheckLicense levelofeffort');
        var action = component.get("c.checkLicensePermition1");
        action.setCallback(this, function(a) {
            //alert('doCheckLicense');
            if(a.getReturnValue() === 'Yes')
            {
                // alert('inside if');
                component.set("v.CheckLicense", 'Yes'); 
                helper.checkValueTablePermissions(component, event, helper); // Check Value Table Permissions 21-08-2018
                helper.getContractVehicle1(component, event, helper); // called in helper
                helper.conValLevelOfEffortTableList(component, event, helper); // called in helper
            }
            else
            {
                // alert('inside else');
                component.set("v.CheckLicense", a.getReturnValue());
            }
            
        });
        $A.enqueueAction(action);  
    },    
    addLevelEffortRow : function(component, event, helper) {
        event.preventDefault();
        var valueTableLevelList = component.get("v.valueTableLevelList");
        var action = component.get("c.newRowLevelEffortTable"); //call apex controller method
        action.setParams({           
            "contractVehicalId": component.get("v.recordId")
        });
        action.setCallback(this,function(a){
            var state= a.getState();
            if(state=="SUCCESS"){
                var newConVal=a.getReturnValue();
                // "If" condition and "Else" block are newly added to restrict user access according to the object permission(17/08/2018)
                if(newConVal != null){
                    newConVal.TM_TOMA__Start_Date__c='';
                    newConVal.TM_TOMA__End_Date__c='';
                    valueTableLevelList.push(newConVal);
                    component.set("v.valueTableLevelList",valueTableLevelList);
                }else{
                    alert('ERROR:- You do not have CREATE permission for Value Table object.'); // New Change - 16/08/2018
                }
            }
            else if (state == "ERROR") 
            {
                console.log(a.getError());            
            }
        });
        $A.enqueueAction(action);       
    },
    getContractVehicle : function(component, event, helper) {
        helper.getContractVehicle1(component, event, helper); // called in helper
        
    },
    valueTableLevelEffortList : function(component, event, helper) {
        helper.conValLevelOfEffortTableList(component, event, helper); // called in helper
    },
    removeIncModFeeRow  : function(component, event, helper) {
        var conf = confirm("Are you sure you want to delete this component?");
        if(conf== true)
        {
            var valueTableLevelList =component.get("v.valueTableLevelList");       
            var delLevelEffortId =component.get("v.delLevelEffortId");       
            var conValLevEffTablesize=component.get("v.valueTableLevelEffortSizeFlag");
            var selectedItem = event.currentTarget;
            var removeCounter = selectedItem.id;
            if (removeCounter < conValLevEffTablesize) {
                delLevelEffortId+=',';
                conValLevEffTablesize-=1;
                component.set("v.valueTableLevelEffortSizeFlag",conValLevEffTablesize);
                delLevelEffortId =delLevelEffortId+valueTableLevelList[removeCounter].Id;
                component.set("v.delLevelEffortId",delLevelEffortId);
            } 
            valueTableLevelList.splice(removeCounter, 1);
            component.set("v.valueTableLevelList", valueTableLevelList);
        }
    },
    saveLevelEffortTable : function(component, event, helper) {
        event.preventDefault();
        //alert('saveLevelEffortTable');
        helper.saveLevelOfEffortTableList(component, event, helper); // Defined in helper
    }
})