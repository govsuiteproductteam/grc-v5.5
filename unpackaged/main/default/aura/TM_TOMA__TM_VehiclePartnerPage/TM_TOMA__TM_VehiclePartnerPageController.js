({
    checkAccess : function(component, event, helper) {
        var isLiecenceValid =  component.get("c.checkLicensePermition");
        isLiecenceValid.setCallback(this,function(validityData){
            var state= validityData.getState();
            if(state=="SUCCESS"){
                console.log("Stages Status"+validityData.getReturnValue());
                if(validityData.getReturnValue() == 'Yes'){
                    component.set("v.CheckLicense",true);
                    helper.getConVehicleContactList1(component, event, helper);
                    helper.getVehiclePartnerWraList1(component, event, helper);
                }/*else if(validityData.getReturnValue() == 'No'){
                    component.set("v.CheckLicense",false);
                }*/else{
                    component.set("v.CheckLicense",false);
                    component.set("v.errorMsg",validityData.getReturnValue());   
                }
            }
            else if (state == "ERROR") 
			{
				console.log(validityData.getError());            
			}
        });
        $A.enqueueAction(isLiecenceValid);
    },
    getConVehicleContactList : function(component, event, helper) {
        helper.getConVehicleContactList1(component, event, helper);
    },
    getVehiclePartnerWraList : function(component, event, helper) {
        helper.getVehiclePartnerWraList1(component, event, helper);
    },
    RefreshVehicalPatnerPageComp : function(component, event, helper) {
        var isLiecenceValid =  component.get("c.checkLicensePermition");
        isLiecenceValid.setCallback(this,function(validityData){
            var state= validityData.getState();
            if(state=="SUCCESS"){
                console.log("Stages Status"+validityData.getReturnValue());
                if(validityData.getReturnValue() == 'Yes'){
                    component.set("v.CheckLicense",true);
                    helper.getConVehicleContactList1(component, event, helper);
                    helper.getVehiclePartnerWraList1(component, event, helper);
                }else if(validityData.getReturnValue() == 'No'){
                    component.set("v.CheckLicense",false);
                }else{
                    component.set("v.CheckLicense",true);
                }
            }
            else if (state == "ERROR") 
			{
				console.log(validityData.getError());            
			}
        });
        $A.enqueueAction(isLiecenceValid);
    },
    AccountRecord : function (component, event, helper) {
        var selectedItem = event.currentTarget;
        var tempId = selectedItem.id;
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": tempId,
            "slideDevName": "detail"
        });
        navEvt.fire();
    },
    
    ContactRecord : function (component, event, helper) {
        var selectedItem = event.currentTarget;
        var tempId = selectedItem.id;
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": tempId,
            "slideDevName": "detail"
        });
        navEvt.fire();
    },
    editConVehicleContact : function (component, event, helper) {
        var selectedItem = event.currentTarget;
        var tempId = selectedItem.id;
        /* var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": tempId,
            "slideDevName": "related",
        });
        navEvt.fire();*/
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            "recordId": tempId
        });
        editRecordEvent.fire();
    },
    deleteConVehicleContact : function (component, event, helper) {
        var conVehicleContactList1=component.get("v.conVehicleContactList");
        var selectedItem = event.currentTarget;
        var removeCounter = selectedItem.id;
        var tempCont = selectedItem.dataset.arg1;
       // var recName = selectedItem.dataset.arg2;
        if (removeCounter != undefined && removeCounter != '') {
            var action = component.get("c.deleteContractVehicalContact");
            action.setParams({
                contractVehicalConId: selectedItem.id
            });
            action.setCallback(this, function(a) {
                var state = a.getState();
                if (state === "SUCCESS")
                {
                    helper.getConVehicleContactList1(component, event, helper);
                    helper.getVehiclePartnerWraList1(component, event, helper);
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "Contract Vehicle Contact Was Deleted",
                        "type":"success"
                    });
                    toastEvent.fire();
                }
                else if (state === "ERROR") 
                {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": "Error occurred while deleting the record.Please refresh the page.",
                        "type":"error"
                    });
                    toastEvent.fire();
                    console.log(a.getError());    
                }
            })
            $A.enqueueAction(action); 
        }
    },
    
    addConVehicleContactList : function (component, event, helper) {
        /*var cmpTarget = component.find('conVehicleContactDiv');
        $A.util.removeClass(cmpTarget, 'slds-show');
        $A.util.addClass(cmpTarget, 'slds-hide');
        var cmpTarget1 = component.find('conVehicleContactAddDiv');
        $A.util.addClass(cmpTarget1, 'slds-show');
        $A.util.removeClass(cmpTarget1, 'slds-hide');*/
        
        
        var Modal = component.find("conVehicleContactAddDiv");
        var ModalBack = component.find("conVehicleContactAddModalbagroundId");
        $A.util.removeClass(Modal, "slds-modal slds-fade-in-close");
        $A.util.addClass(Modal, "slds-modal slds-fade-in-open");
        $A.util.removeClass(ModalBack, "slds-backdrop slds-backdrop--close");
        $A.util.addClass(ModalBack, "slds-backdrop slds-backdrop--open");
    },
    maincheckboxAccount : function (component, event, helper) {
        var obj = component.find("select-all1").get("v.value");
        if(obj === true) {
            var tempList = component.get("v.vehiclePartnerWraList");
            if(tempList != undefined){
                if(tempList.length>0){
                    for(var i=0;i<tempList.length;i++)
                    {
                        tempList[i].status=true;
                    }
                    component.set("v.vehiclePartnerWraList",tempList);
                }
            }
        }
        else{
            var tempList = component.get("v.vehiclePartnerWraList");
            if(tempList != undefined){
                if(tempList.length>0){
                    for(var i=0;i<tempList.length;i++)
                    {
                        tempList[i].status=false;
                    }
                    component.set("v.vehiclePartnerWraList",tempList);
                }
            }
        }
    },
    maincheckboxChildAccount : function (component, event, helper) {
        var checkbox = event.getSource();
        if(checkbox.get("v.value") === false) {
            component.find("select-all1").set("v.value",false);
        }
        else
        {
            var check;
            var tempList = component.get("v.vehiclePartnerWraList");
            if(tempList != undefined){
                if(tempList.length>0){
                    for(var i=0;i<tempList.length;i++)
                    {
                        if(tempList[i].status === false)
                        {
                            check=false;
                            break;
                        }
                        else
                        {
                            check=true;
                        }
                    }
                    if(check === true)
                    {
                        component.find("select-all1").set("v.value",true);
                    }
                    else
                    {
                        component.find("select-all1").set("v.value",false);
                    }
                }
            }
        }
    },
    maincheckboxContact : function (component, event, helper) {
        var obj = component.find("select-all").get("v.value");
        if(obj === true) {
            var tempList = component.get("v.conWraList");
            if(tempList != undefined){
                if(tempList.length>0)
                {
                    for(var i=0;i<tempList.length;i++)
                    {
                        tempList[i].conStatus=true;
                    }
                    component.set("v.conWraList",tempList);
                }
            }
        }
        else{
            var tempList = component.get("v.conWraList");
            if(tempList != undefined){
                if(tempList.length>0)
                {
                    for(var i=0;i<tempList.length;i++)
                    {
                        tempList[i].conStatus=false;
                    }
                    component.set("v.conWraList",tempList);
                }
            }
        }
    },
    maincheckboxChildContact : function (component, event, helper) {
        var checkbox = event.getSource();
        if(checkbox.get("v.value") === false) {
            component.find("select-all").set("v.value",false);
        }
        else
        {
            var check;
            var tempList = component.get("v.conWraList");
            if(tempList != undefined){
                if(tempList.length>0){
                    for(var i=0;i<tempList.length;i++)
                    {
                        if(tempList[i].conStatus === false)
                        {
                            check=false;
                            break;
                        }
                        else
                        {
                            check=true;
                        }
                    }
                    if(check === true)
                    {
                        component.find("select-all").set("v.value",true);
                    }
                    else
                    {
                        component.find("select-all").set("v.value",false);
                    }
                }
            }
        } 
    },
    
    ShowContact1 : function (component, event, helper) {
        var accountIds;
        var tempList = component.get("v.vehiclePartnerWraList");
        if(tempList != undefined)
        {
            for(var i=0;i<tempList.length;i++)
            {
                if(tempList[i].status === true)
                {
                    accountIds=accountIds+' '+tempList[i].vehiclePartner.TM_TOMA__Partner__c;
                }
            }
            if(accountIds !=undefined){
                var action = component.get("c.showContact");
                action.setParams({
                    accountIdsList: accountIds,
                    contractVehicleId: component.get("v.recordId")
                });
                action.setCallback(this, function(a) {
                    var state= a.getState();
                    if(state=="SUCCESS"){
                        if(a.getReturnValue()!= undefined && a.getReturnValue() != null && a.getReturnValue() != ''){
                            component.set("v.conWraList", a.getReturnValue());
                            component.set("v.conWraListTemp", a.getReturnValue());
                        }
                        else{
                            component.set("v.conWraList",);
                            component.set("v.conWraListTemp",);
                        }
                    }
                    else if (state == "ERROR") 
                    {
                        console.log(a.getError());            
                    }
                })
                $A.enqueueAction(action); 
            }
            else{
                //alert('Please at least select one contacts from the list.');
                var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": "Please select at least one Account/Partner.",
                        "type":"error"
                    });
                    toastEvent.fire();
            }
        }
        else{
            //alert('Please at least select one contacts from the list.');
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "message": "Please select at least one Account/Partner.",
                "type":"error"
            });
            toastEvent.fire();
        }
    },
    addPartner1: function (component, event, helper) {
        document.getElementById("addContractVehicleContactBtn").disabled = true;
        var tempList = component.get("v.conWraList");
        var partnerDataString='[';
        //alert(tempList);
        if(tempList != undefined){
            for(var i=0;i<tempList.length;i++)
            {
                if(tempList[i].conStatus === true)
                {
                    var contractVehicleId = component.get("v.recordId");
                    var contact = tempList[i].con.Id;
                    var mainPOC = tempList[i].Main_POC;
                    var receivesMassEmails = tempList[i].Receives_Mass_Emails;
                    partnerDataString += contractVehicleId+'SPLITDATA'+contact+'SPLITDATA'+mainPOC+'SPLITDATA'+receivesMassEmails+'SPLITPARENT';
                }
            }
            if(partnerDataString != '['){
                var action = component.get("c.addPartner");
                action.setParams({
                    partnerData: partnerDataString
                });
                action.setCallback(this, function(a) {
                    var state= a.getState();
                    if(state=="SUCCESS"){
                        helper.getConVehicleContactList1(component, event, helper);
                        helper.getVehiclePartnerWraList1(component, event, helper);
                        component.find("select-all").set("v.value",false);
                        component.find("select-all1").set("v.value",false);
                        component.set("v.conWraList",);
                        
                        var conModal = component.find("conVehicleContactAddDiv");
                        var conModalBack = component.find("conVehicleContactAddModalbagroundId");
                        $A.util.removeClass(conModal, "slds-modal slds-fade-in-open");
                        $A.util.addClass(conModal, "slds-modal slds-fade-in-close");
                        $A.util.removeClass(conModalBack, "slds-backdrop slds-backdrop--open");
                        $A.util.addClass(conModalBack, "slds-backdrop slds-backdrop--close");
                        document.getElementById("addContractVehicleContactBtn").disabled = false;
                    }
                    else if (state == "ERROR") 
                    {
                        console.log(a.getError()); 
                        component.find("select-all").set("v.value",false);
                        component.find("select-all1").set("v.value",false);
                        component.set("v.conWraList",);
                        var conModal = component.find("conVehicleContactAddDiv");
                        var conModalBack = component.find("conVehicleContactAddModalbagroundId");
                        $A.util.removeClass(conModal, "slds-modal slds-fade-in-open");
                        $A.util.addClass(conModal, "slds-modal slds-fade-in-close");
                        $A.util.removeClass(conModalBack, "slds-backdrop slds-backdrop--open");
                        $A.util.addClass(conModalBack, "slds-backdrop slds-backdrop--close");
                        document.getElementById("addContractVehicleContactBtn").disabled = false;
                    }
                })
                $A.enqueueAction(action);
            }
            else
            {
                document.getElementById("addContractVehicleContactBtn").disabled = false;
                var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": "Please select at least one Contact.",
                        "type":"error"
                    });
                    toastEvent.fire();
               // alert('Please at least select one contacts from the list.');
            }
        }
        else
        {
            document.getElementById("addContractVehicleContactBtn").disabled = false;
            var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": "Please select at least one contact.",
                        "type":"error"
                    });
                    toastEvent.fire();
            //alert('Please at least select one contacts from the list.');
        }
    },
    cancel: function (component, event, helper) {
        var conModal = component.find("conVehicleContactAddDiv");
        var conModalBack = component.find("conVehicleContactAddModalbagroundId");
        $A.util.removeClass(conModal, "slds-modal slds-fade-in-open");
        $A.util.addClass(conModal, "slds-modal slds-fade-in-close");
        $A.util.removeClass(conModalBack, "slds-backdrop slds-backdrop--open");
        $A.util.addClass(conModalBack, "slds-backdrop slds-backdrop--close");
        component.find("select-all").set("v.value",false);
        component.find("select-all1").set("v.value",false);
        component.set("v.searchPartner",'');
        component.set("v.searchContact",'');
        var tempList = component.get("v.vehiclePartnerWraList");
        if(tempList != undefined){
            for(var i=0;i<tempList.length;i++)
            {
                tempList[i].status = false;
            }
            component.set("v.vehiclePartnerWraList",tempList); 
            component.set("v.conWraList",); 
          }
                          component.set("v.vehiclePartnerWraList",component.get("v.vehiclePartnerWraListTemp"));
    },
   editInlinePoc : function(component, event, helper) {
            var selectedItem = event.currentTarget;
            var checkCounter = selectedItem.id;
            var d = document.getElementById(checkCounter+'vehEditMainPocInlineOut');
            d.className += " slds-hide";
            d.classList.remove("slds-show");
            var d1 = document.getElementById(checkCounter+'vehEditMainPocInlineIn');
            d1.className += " slds-show";
            d1.classList.remove("slds-hide");
            // var oldval = selectedItem.dataset.arg1;
            var d2 = document.getElementById(checkCounter+'vehEditMassEmailInlineOut');
            d2.className += " slds-hide";
            d2.classList.remove("slds-show");
            var d3 = document.getElementById(checkCounter+'vehEditMassEmailInlineIn');
            d3.className += " slds-show";
            d3.classList.remove("slds-hide");
            
            var d4 = document.getElementById(checkCounter+'vehEditDiv');
            d4.className += " slds-hide";
            d4.classList.remove("slds-show");
            var d5 = document.getElementById(checkCounter+'vehEditUndoDiv');
            d5.className += " slds-show";
            d5.classList.remove("slds-hide");
            
            // var oldval = selectedItem.dataset.arg1;
            document.getElementById(checkCounter+'vehEdit').dataset.arg1 = selectedItem.dataset.arg1;
            document.getElementById(checkCounter+'vehEdit').dataset.arg3 = selectedItem.dataset.arg2;
            var checksave = component.get("v.checkSavebutton");
            checksave=checksave+1;
            component.set("v.checkSavebutton",checksave);
   },
       closeInlinePoc : function(component, event, helper) {
           var selectedItem = event.currentTarget;
           var checkCounter = selectedItem.id;
           var oldval = selectedItem.dataset.arg1;
           var oldval1 = selectedItem.dataset.arg3;
           //event.currentTarget.dataset.caseID;
           // alert(checkCounter);
           var d = document.getElementById(checkCounter+'MainPocInlineOut');
           d.className += " slds-show";
           d.classList.remove("slds-hide");
           var d1 = document.getElementById(checkCounter+'MainPocInlineIn');
           d1.className += " slds-hide";
           d1.classList.remove("slds-show");
           var d2 = document.getElementById(checkCounter+'MassEmailInlineOut');
           d2.className += " slds-show";
           d2.classList.remove("slds-hide");
           var d3 = document.getElementById(checkCounter+'MassEmailInlineIn');
           d3.className += " slds-hide";
           d3.classList.remove("slds-show"); 
           
           var d4 = document.getElementById(checkCounter+'Div');
           d4.className += " slds-show";
           d4.classList.remove("slds-hide");
           var d5 = document.getElementById(checkCounter+'UndoDiv');
           d5.className += " slds-hide";
           d5.classList.remove("slds-show");
           
           var checksave = component.get("v.checkSavebutton");
           checksave=checksave-1;
           component.set("v.checkSavebutton",checksave);
           var index = selectedItem.dataset.arg2;
           var temp = component.get("v.conVehicleContactList");
           if(temp!= undefined){
               for(var i=0;i<temp.length;i++)
               {
                   if(index === i+'')
                   {
                       temp[i].TM_TOMA__Main_POC__c = (oldval == 'true');
                       temp[i].TM_TOMA__Receives_Mass_Emails__c = (oldval1 == 'true');
                       //alert( temp[i].Main_POC__c);
                   }
               }
               component.set("v.conVehicleContactList",temp);
           }
       },
       editInlineEmail : function(component, event, helper) {
           var selectedItem = event.currentTarget;
           var checkCounter = selectedItem.id;
           var d = document.getElementById(checkCounter+'editMassEmailInlineOut');
           d.className += " slds-hide";
           d.classList.remove("slds-show");
           var d1 = document.getElementById(checkCounter+'editMassEmailInlineIn');
           d1.className += " slds-show";
           d1.classList.remove("slds-hide");
           // var oldval = selectedItem.dataset.arg1;
           document.getElementById(checkCounter+'editMassEmail').dataset.arg1 = selectedItem.dataset.arg1;
       },
       closeInlineEmail : function(component, event, helper) {
           var selectedItem = event.currentTarget;
           var checkCounter = selectedItem.id;
           var oldval = selectedItem.dataset.arg1;
           //event.currentTarget.dataset.caseID;
           var d = document.getElementById(checkCounter+'InlineOut');
           d.className += " slds-show";
           d.classList.remove("slds-hide");
           var d1 = document.getElementById(checkCounter+'InlineIn');
           d1.className += " slds-hide";
           d1.classList.remove("slds-show");
           var index = selectedItem.dataset.arg2;
           var temp = component.get("v.conVehicleContactList");
           if(temp != undefined){
               for(var i=0;i<temp.length;i++)
               {
                   if(index === i+'')
                   {
                       temp[i].TM_TOMA__Receives_Mass_Emails__c = (oldval == 'true');
                   }
               }
               component.set("v.conVehicleContactList",temp);
           }
       },
           saveConVehicleContactList : function(component, event, helper) {
               document.getElementById("saveConVehicle").disabled = true;
               var conVehicleConList=component.get("v.conVehicleContactList");
               var conVehicleContactDataString = '[';
               if(conVehicleConList != undefined){
                   for (var i=0; i<conVehicleConList.length; i++){        
                       
                       var vTId = conVehicleConList[i].Id;
                       var mainPoc = conVehicleConList[i].TM_TOMA__Main_POC__c;
                       var massEmail  = conVehicleConList[i].TM_TOMA__Receives_Mass_Emails__c;
                       conVehicleContactDataString += vTId+'SPLITDATA'+mainPoc+'SPLITDATA'+massEmail+'SPLITPARENT';
                   }
               }
               var action = component.get("c.saveContractVehicleContact");
               action.setParams({ 
                   "conVehicleContactList": conVehicleContactDataString
               });
               action.setCallback(this, function(response){
                   var state = response.getState();
                   if (state === "SUCCESS")
                   {
                       helper.getConVehicleContactList1(component, event, helper);
                       helper.getVehiclePartnerWraList1(component, event, helper);
                       component.set("v.checkSavebutton",0);
                       document.getElementById("saveConVehicle").disabled = false;
                   }
                   else if (state === "ERROR") 
                   {
                       component.set("v.checkSavebutton",0);
                       document.getElementById("saveConVehicle").disabled = false;
                       console.log(response.getError());   
                       
                   }
               });
           $A.enqueueAction(action);
       },
           doInitMatchingResult : function(component, event, helper) {
               var partnerStr = component.get("v.searchPartner")
               if(typeof partnerStr === 'undefined' || partnerStr.length < 2)
               {
                   
                   var tempList = component.get("v.vehiclePartnerWraListTemp");
                   if(tempList != undefined){
                       for(var i=0;i<tempList.length;i++)
                       {
                          if(tempList[i].status == false) 
                          {
                              component.find("select-all1").set("v.value",false);
                          }
                       }
                      component.set("v.vehiclePartnerWraList",tempList); 
                                     }
                  component.set("v.vehiclePartnerWraList",component.get("v.vehiclePartnerWraListTemp"));
               }
               else{
                   var action = component.get("c.getSearchVehiclePartnerList");
                   action.setParams({
                       contractVehicalId: component.get("v.recordId"),
                       searchString: partnerStr
                   });
                   action.setCallback(this, function(a) {
                       var state = a.getState();
                       if (component.isValid() && state === "SUCCESS")
                       {
                           if(a.getReturnValue() == undefined || a.getReturnValue() == '' || a.getReturnValue() == null)
                           {
                               component.set("v.vehiclePartnerWraList",);
                                             }
                                             else{
                                             component.set("v.vehiclePartnerWraList", a.getReturnValue());
                           }
                       }else if (state == "ERROR") 
                       {
                           console.log(a.getError());            
                       }
                   })
                   $A.enqueueAction(action); 
               }
           },
               doInitMatchingResultContact : function(component, event, helper) {
                   var contactStr = component.get("v.searchContact")
                   if(typeof contactStr === 'undefined' || contactStr.length < 2)
                   {
                       
                      
                       var tempList = component.get("v.conWraListTemp");
                       if(tempList != undefined){
                           for(var i=0;i<tempList.length;i++)
                           {
                               if(tempList[i].conStatus == false) 
                               {
                                  component.find("select-all").set("v.value",false);
                               }
                           }
                            component.set("v.conWraList",tempList); 
                       }
                       
                       component.set("v.conWraList",component.get("v.conWraListTemp"));
                   }
                   else{
                       var accountIds;
                       var tempList = component.get("v.vehiclePartnerWraList");
                       if(tempList != undefined)
                       {
                           for(var i=0;i<tempList.length;i++)
                           {
                               if(tempList[i].status === true)
                               {
                                   accountIds=accountIds+' '+tempList[i].vehiclePartner.TM_TOMA__Partner__c;
                               }
                           }
                           var action = component.get("c.showSearchContact");
                           action.setParams({
                               accountIdsList: accountIds,
                               contractVehicleId: component.get("v.recordId"),
                               searchString: contactStr
                               
                           });
                           action.setCallback(this, function(a) {
                               var state= a.getState();
                               if(state=="SUCCESS"){
                                   if(a.getReturnValue() == undefined || a.getReturnValue() == '' || a.getReturnValue() == null)
                                   {
                                       component.set("v.conWraList",);
                                                     }
                                                     else{
                                                     component.set("v.conWraList", a.getReturnValue());
                                   }
                               }
                               else if (state == "ERROR") 
                               {
                                   console.log(a.getError());            
                               }
                           })
                           $A.enqueueAction(action); 
                       }
                   }
               }
    })