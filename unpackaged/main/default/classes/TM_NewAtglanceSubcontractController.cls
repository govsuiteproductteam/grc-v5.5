public class TM_NewAtglanceSubcontractController {
    public static Schema.DescribeSObjectResult result;
    static{
        Schema.SObjectType t = Schema.getGlobalDescribe().get('TM_TOMA__Subcontract__c');
        
        result = t.getDescribe();
        
    
    }
    
    @AuraEnabled
    public static String getUserAccesibility()
    {
        string isValidLiecence = LincenseKeyHelper.checkLicensePermitionForFedCLM();
        
        return isValidLiecence;
        
    }
    @AuraEnabled
    public static Map<String,String> getstagePicklistvalue(){
        List<RecordType> rt = [SELECT Id,Name FROM RecordType WHERE SobjectType='TM_TOMA__Subcontract__c'];
        
        Map<String,String> optionsMap=new Map<String,String>();
        for(RecordType f : rt){
            optionsMap.put(f.id, f.Name);
        }
        return optionsMap;
    }
    
    @AuraEnabled
    public static TM_AtglanceWrap  getFields(String recoredTyeId){
        System.debug('inside>>>>'+recoredTyeId);
        
        Subcontract__c op=[Select RecordTypeId From Subcontract__c where id= : recoredTyeId Limit 1 ];
        String oppRecoredTyeId=(''+op.RecordTypeId).substring(0,15);
        // List<CLM_Tab__c> tabPopList = [SELECT  ApiName_for_CongaField__c, TabStatus__c FROM CLM_Tab__c WHERE RecordType_Id__c = :oppRecoredTyeId ORDER By Order__c];
        //List<TM_OppRecordTypeSetting__c> fieldPopList = [SELECT FieldApiName__c, Viewfieldapi__c,FieldLabel__c FROM TM_OppRecordTypeSetting__c WHERE RecordType_Id__c = :oppRecoredTyeId ORDER By Order__c];
        List<CLM_AtaGlanceField__c> atGlancePopList = [SELECT FieldApiName__c,Color__c,ColSpan__c,Document_Id__c,FieldLabel__c,Is_Component__c,Order__c FROM CLM_AtaGlanceField__c WHERE RecordType_Id__c =:oppRecoredTyeId ORDER By Order__c];
        
        //System.debug('tabPopList>>>>'+tabPopList);
        //System.debug('fieldPopList>>>>'+fieldPopList);
        System.debug('atGlancePopList>>>>'+atGlancePopList);
        Map<String,String> filedMap=new Map<String,String>();        
        
        String OppRecordTypeTabSettingFields='';
        String OppRecordTypeSettingFields='';
        String OppAtaGlanceFields='';
        /* Map<String,String> oppFieldMap=new Map<String,String>();
for(TM_OppRecordTypeSetting__c  OppRecordTypeSetting :fieldPopList){
if(OppRecordTypeSetting.FieldApiName__c!=null){
oppFieldMap.put(OppRecordTypeSetting.FieldApiName__c, OppRecordTypeSetting.FieldLabel__c);
OppRecordTypeSettingFields +=''+OppRecordTypeSetting.FieldApiName__c+',';
}
}
if(OppRecordTypeSettingFields!=''){
OppRecordTypeSettingFields=OppRecordTypeSettingFields.removeEnd(',');
filedMap.put('reType', OppRecordTypeSettingFields);
}

for(TM_OppRecordTypeTabSetting__c oppRecordTypeSet :tabPopList ){
if(oppRecordTypeSet.ApiName_for_CongaField__c !=null && oppFieldMap.containsKey(oppRecordTypeSet.ApiName_for_CongaField__c)){
//String label=oppFieldMap.get(oppRecordTypeSet.ApiName_for_CongaField__c);
OppRecordTypeTabSettingFields +=''+oppFieldMap.get(oppRecordTypeSet.ApiName_for_CongaField__c)+':'+oppRecordTypeSet.ApiName_for_CongaField__c+',';  
}
}       
if(OppRecordTypeTabSettingFields!=''){
OppRecordTypeTabSettingFields= OppRecordTypeTabSettingFields.removeEnd(',');
filedMap.put('tab', OppRecordTypeTabSettingFields);
}
*/
           Map<String,String> fieldwarpMap=new Map<String,String>();
        Map<String,String> fieldlevelaccessMap=new Map<String,String>();//25-10-2018
        List<CLM_AtaGlanceField__c> atGlancePopListtemp=new List<CLM_AtaGlanceField__c>();
        for(CLM_AtaGlanceField__c OppAtaGlanceField :atGlancePopList ){
            if(OppAtaGlanceField.FieldApiName__c!=null ){//&& oppFieldMap.containsKey(OppAtaGlanceField.FieldApiName__c)){
                if(!OppAtaGlanceField.Is_Component__c){
                    Schema.DescribeFieldResult descResult = result.fields.getMap().get(OppAtaGlanceField.FieldApiName__c).getDescribe();
                    if (descResult.getType() == Schema.DisplayType.Date){
                        fieldwarpMap.put(OppAtaGlanceField.FieldApiName__c,'Date');
                        OppAtaGlanceFields +=''+OppAtaGlanceField.FieldLabel__c+':'+OppAtaGlanceField.FieldApiName__c+',';
                    }else if(descResult.getType() == Schema.DisplayType.Currency){
                        fieldwarpMap.put(OppAtaGlanceField.FieldApiName__c,'Currency');
                        OppAtaGlanceFields +=''+OppAtaGlanceField.FieldLabel__c+':'+OppAtaGlanceField.FieldApiName__c+',';
                    }else if(descResult.getType() == Schema.DisplayType.Reference){
                        String refField=OppAtaGlanceField.FieldApiName__c;
                        
                        if(refField.endsWith('__c')){
                            refField=refField.replace('__c', '__r');
                            refField+='.Name';
                            OppAtaGlanceField.FieldApiName__c=''+refField.trim();
                            OppAtaGlanceFields +=''+OppAtaGlanceField.FieldLabel__c+':'+refField+',';
                            fieldwarpMap.put(refField,'Reference');
                        }else{
                            OppAtaGlanceFields +=''+OppAtaGlanceField.FieldLabel__c+':'+OppAtaGlanceField.FieldApiName__c+',';
                        }
                    }else if(descResult.getType() == Schema.DisplayType.DateTime){
                        fieldwarpMap.put(OppAtaGlanceField.FieldApiName__c,'DateTime');
                        OppAtaGlanceFields +=''+OppAtaGlanceField.FieldLabel__c+':'+OppAtaGlanceField.FieldApiName__c+',';
                    }else{
                        OppAtaGlanceFields +=''+OppAtaGlanceField.FieldLabel__c+':'+OppAtaGlanceField.FieldApiName__c+',';
                    }
                    
                    //25-10-2018
                    if(descResult.isAccessible())
                    {
                        fieldlevelaccessMap.put(OppAtaGlanceField.FieldApiName__c,'True');
                    }
                    else{
                        fieldlevelaccessMap.put(OppAtaGlanceField.FieldApiName__c,'False');
                    }
                }
                atGlancePopListtemp.add(OppAtaGlanceField);
                
            }
            
        }
        if(OppAtaGlanceFields!=''){
            OppAtaGlanceFields=OppAtaGlanceFields.removeEnd(',');
            filedMap.put('atglace', OppAtaGlanceFields);
        }
         //TM_AtglanceWrap atglancewarp=new TM_AtglanceWrap(atGlancePopListtemp,fieldwarpMap);
         TM_AtglanceWrap atglancewarp=new TM_AtglanceWrap(atGlancePopListtemp,fieldwarpMap,fieldlevelaccessMap);//25-10-2018
        return atglancewarp;
    }
}