({
    render : function(component, helper) {
        var ret = this.superRender();
        //console.log('renderer RecTypeId 1- '+component.get("v.pageReference").state.recordTypeId);
        //console.log('renderer RecTypeId 2- '+component.get("v.recordTypeId"));
        //console.log('refreshedPage-'+component.get("v.pageReference").state.refreshedPage);
        if(component.get("v.pageReference").state.TM_TOMA__refreshedPage !='1'){//newly added TM_TOMA__ to param @ 15 Jan
            var sPageURL = decodeURIComponent(window.location);
            //sPageURL=sPageURL+'&TM_TOMA__refreshedPage=1';//newly added TM_TOMA__ to param @ 15 Jan
            if(!sPageURL.includes("recordTypeId") && !sPageURL.includes("TM_TOMA__clone") && !sPageURL.includes("TM_TOMA__recordId")){
            	sPageURL=sPageURL+'?originalUrl=$0&TM_TOMA__refreshedPage=1';//newly added TM_TOMA__ to param @ 15 Jan    
            }else{
                sPageURL=sPageURL+'&TM_TOMA__refreshedPage=1';//newly added TM_TOMA__ to param @ 15 Jan
            }
            /*var eUrl= $A.get("e.force:navigateToURL");
            eUrl.setParams({
                "url": sPageURL 
            });
            eUrl.fire();*/
            window.open(sPageURL,'_top');
        }else{//newlly added 03-09-2018 to avoid reloading of edit page twice
            return ret; 
        }
        
        
        return ret;
    },
    rerender : function(component, helper){
        this.superRerender();
       
            //console.log('re-Render New recordTypeId'+component.get("v.pageReference").state.recordTypeId);
            //console.log('re-Render old RecordTypeId - '+component.get("v.recordTypeId"));
            var currentRecTypeId = component.get("v.pageReference").state.recordTypeId;
            var oldRecTypeId = component.get("v.recordTypeId");
            // code to resolve platform cache issue
            if(((currentRecTypeId != null && currentRecTypeId != undefined) && (oldRecTypeId != null && oldRecTypeId != undefined)) && currentRecTypeId !=oldRecTypeId){
               console.log('@@@@ @@@@@ '+component.get("v.Isrendar"));
                if(!component.get("v.Isrendar") ){
                    
                    component.set("v.currentTabId","NONE");
                    
                    helper.getTabWrapperData(component, event);
                    component.set("v.Isrendar",true);
                }
            }else{
                var isClone = component.get("v.pageReference").state.TM_TOMA__clone;//newly added TM_TOMA__ to param @ 15 Jan
                if(isClone != null && isClone != undefined && isClone=='1'){
                    var currentRecordId = component.get("v.pageReference").state.TM_TOMA__recordId;//newly added TM_TOMA__ to param @ 15 Jan
                    var oldRecId = component.get("v.recordId");
                    if(((currentRecordId != null && currentRecordId != undefined) && (oldRecId != null && oldRecId != undefined)) && currentRecordId !=oldRecId){
                        component.set("v.currentTabId","NONE");
                        helper.getTabWrapperData(component, event);
                    }
                }
            }
           
        
    }
})