@isTest
public with sharing class TestGSAConnectAttach{
    public static TestMethod void GSAConnectAttachTest(){
        Account acc = TestDataGenerator.createAccount('TaskOrderCapture');
        insert acc;
        
        Contract_Vehicle__c contractVehicle = TestDataGenerator.createContractVehicle('TMconVehicle',acc);
        insert contractVehicle;
        
        Task_Order__c taskOrder = TestDataGenerator.createTaskOrder('TMtaskOrder', acc, contractVehicle);
        insert taskOrder;
        
        StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'ModificationStaticResource' LIMIT 1];
        String body = sr.Body.toString();
               
        Test.startTest();        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        req.requestBody = Blob.valueof(body);
        req.httpMethod = 'POST';
        
        Map<String,String> headerTest = req.headers;
        headerTest.put('fileName','file 1');
        headerTest.put('taskOrderId',taskOrder.id);
        
        RestContext.request = req;
        RestContext.response = res;
        GSAConnectAttach.doPost();
        system.assertNotEquals(taskOrder , null);
        Test.stopTest();
    }
    
    public static TestMethod void GSAConnectAttachTest1(){
        Account acc = TestDataGenerator.createAccount('TaskOrderCapture');
        insert acc;
        
        Contract_Vehicle__c contractVehicle = TestDataGenerator.createContractVehicle('TMconVehicle',acc);
        insert contractVehicle;
        
        Task_Order__c taskOrder = TestDataGenerator.createTaskOrder('TMtaskOrder', acc, contractVehicle);
        insert taskOrder;
        
        StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'ModificationStaticResource' LIMIT 1];
        String body = sr.Body.toString();
               
        Test.startTest();        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        req.requestBody = Blob.valueof(body);
        req.httpMethod = 'POST';    
        RestContext.request = req;
        RestContext.response = res;
        GSAConnectAttach.doPost();
        system.assertNotEquals(taskOrder , null);
        Test.stopTest();
    }
    
   
    
}