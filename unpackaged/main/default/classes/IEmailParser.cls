public Interface IEmailParser {
    void parse(Messaging.InboundEmail inboundEmail);
    Task_Order__c getTaskOrder(String toAddress);
    Task_Order__c updateTaskOrder(Task_Order__c taskOrder, String toAddress);
}