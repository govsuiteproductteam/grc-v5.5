public class DependentFieldSettingCtrl {
    public String recordTypeId{get;set;}
    public String objectType{get;set;}
    public Boolean isContract = true;
    public transient String message{get;set;}
    public transient String dependentSields{get;set;}//akash new change 10 Aug 2017
    private Map<String, List<FieldStructureClass>>mapIdAndFSCList;
    @AuraEnabled
    public List<FieldStructureClass> fieldStructureList{get;set;}
    public Set<String> availabelApis{get;set;}
    
    public transient String masterLabel{get;set;}
    public transient String masterValue{get;set;}
    public transient Integer listIndex{get;set;}
    public transient Integer innerApexIndex{get;set;}
    public transient Integer fieldApeIndex{get;set;}
    
    public Integer tempListIndex{get;set;}
    public String tempInnerMstVal{get;set;}
    
    private Map<String,Set<String>> restrictDuplicateFieldMap;
    //Clone Work
    public Boolean isCloneButton{get;set;}
    public transient Id recordCloinId{get;set;}
    public Boolean cloneInitiated{get;set;}
    public String btnLabel{get;set;}
    //Clone Ends
    //Key Validation
    public String isValidLiecence{get;set;}
    public dependentFieldSettingCtrl(){
        isValidLiecence = LincenseKeyHelper.checkLicensePermitionForFedCLM();
        if(isValidLiecence == 'Yes'){
            masterApiValFieldsSetMap = new Map<String, Set<String>>();
            mapIdAndFSCList = new Map<String, List<FieldStructureClass>>();
            availabelApis = new Set<String>();
            objectType='Contract';
            isContract = true;
            isCloneButton = false;
            cloneInitiated = false;
        }
    }
    
    public void init(){
        List<Document> docList;
        getContractFields();
        if(objectType=='Contract'){
            if(!Test.isRunningTest())
                docList = [SELECT Id, Body from document where DeveloperName = 'Dependent_Field_Document_contract'];
            else
                docList = [SELECT Id, Body from document where DeveloperName = 'Test_Dependent_Field_Document_contract'];   
        }else{
            if(!Test.isRunningTest())
                docList = [SELECT Id, Body from document where DeveloperName = 'Dependent_Field_Document_subContract'];
            else
                docList = [SELECT Id, Body from document where DeveloperName = 'Test_Dependent_Field_Document_subContract'];
        }
        
        if(!docList.isEmpty()){
            Document doc = docList[0];
            
            if(doc.Body!=Null && doc.Body.toString()!='')
                mapIdAndFSCList = (Map<String, List<FieldStructureClass>>)JSON.deserialize(doc.Body.toString(), Map<String, List<FieldStructureClass>>.class);
        }
        if(fieldStructureList==Null){
            fieldStructureList = new List<FieldStructureClass>();
        }
    }
    
    public void addMaster(){
        Boolean isMsterLabelContains = availabelApis.contains(masterLabel.toLowerCase());
        if(sobjectFieldSet.contains(masterLabel.toLowerCase())){
            if(masterLabel!=Null && masterLabel!='' && getRelationType(masterLabel.toLowerCase())){
                if(masterLabel!=Null && masterLabel!='' && !isMsterLabelContains){
                    fieldStructureList.add(new fieldStructureClass(masterLabel));
                    availabelApis.add(masterLabel.toLowerCase());
                }else if(masterLabel!=Null && masterLabel!='' && isMsterLabelContains){
                    message = 'The Field '+masterLabel+' is already Exist.';
                }
            }else{
                message = 'The field '+masterLabel+' is a lookup or master-detail relationship field. You cannot use this type of field as the parent in a field dependency, please select another field.';//'The Field '+masterLabel+' is Reference Field not Allowed.';
            }
        }else{
            if(masterLabel == null || masterLabel == ''){
                message = 'Please enter value.';
            }
            else{
                message = 'The Field '+masterLabel+' not belongs to '+objectType+'.';
            }
            
            
        }
    }
    
    public void addNewValue(){
        if(listIndex!=Null && fieldStructureList.size()>listIndex){
            if(masterLabel!=Null && masterLabel!=''){
                fieldStructureList[listIndex].masterFieldApi = masterLabel;
            }
            if(masterValue != Null && masterValue != ''){
                if(!fieldStructureList[listIndex].valueNdFieldListMap.containsKey(masterValue)){
                    fieldStructureList[listIndex].valueNdFieldListMap.put(masterValue, new List<StringWrapper>());
                    String tempKeyString = fieldStructureList[listIndex].masterFieldApi.toLowerCase() +'~'+masterValue.toLowerCase();
                    masterApiValFieldsSetMap.put(tempKeyString, new Set<String>());
                }
                else{
                    message = 'The Value is already Exist.';
                }
            }
            else{
                message = 'Please enter value.';
            }
            //fieldStructureList[listIndex].valueList.add(new StringWrapper());
        }
        
        tempListIndex = listIndex;
    }
    private Map<String, Set<String>> masterApiValFieldsSetMap;
    public void addField(){
        if(listIndex!=Null && fieldStructureList.size()>listIndex){
            FieldStructureClass tempFieldStru = fieldStructureList[listIndex];
            if(masterValue != Null && tempFieldStru.valueNdFieldListMap.containsKey(masterValue)){
                tempInnerMstVal = masterValue;
                String tempKey = tempFieldStru.masterFieldApi.toLowerCase()+'~'+masterValue.toLowerCase();
                Set<String> tempDepedentSet = masterApiValFieldsSetMap.get(tempKey);
                System.debug('tempFieldStru.masterFieldApi.toLowerCase() = '+tempFieldStru.masterFieldApi.toLowerCase());
                System.debug('dependentSields.toLowerCase() = '+dependentSields.toLowerCase());
                if(tempFieldStru.masterFieldApi.toLowerCase() == dependentSields.toLowerCase()){
                    message = 'You cannot set dependency for same field.';
                }else if(tempDepedentSet.contains(dependentSields.toLowerCase())){
                    message = 'Field already exsist for this value.';
                }else{
                    StringWrapper sWrap = new StringWrapper();
                    sWrap.fieldOrVal = dependentSields;
                    tempFieldStru.valueNdFieldListMap.get(masterValue).add(sWrap);
                    
                    tempDepedentSet.add(dependentSields.toLowerCase());
                }
                
            }
            tempListIndex = listIndex;
            
        }
    }
    
    public void save(){
        List<Document> docList;
        if(objectType=='Contract'){
            
            if(!Test.isRunningTest())
                docList = [SELECT Id, Body from document where DeveloperName = 'Dependent_Field_Document_contract'];          
            else
                docList = [SELECT Id, Body from document where DeveloperName = 'Test_Dependent_Field_Document_contract'];  
        }else{
            if(!Test.isRunningTest())
                docList = [SELECT Id, Body from document where DeveloperName = 'Dependent_Field_Document_subContract'];
            else
                docList = [SELECT Id, Body from document where DeveloperName = 'Test_Dependent_Field_Document_subContract'];
        }
        if(!docList.isEmpty()){
            Document doc = docList[0];
            
            for(String keyStr : mapIdAndFSCList.keySet()){
                for(FieldStructureClass fscTp : mapIdAndFSCList.get(keyStr)){
                    
                    for(String subKey : fscTp.valueNdFieldListMap.keySet()){
                        Set<String> sTempSet = new Set<String>();
                        List<StringWrapper> tempSWList = new List<StringWrapper>();
                        for(StringWrapper sWrapObj : fscTp.valueNdFieldListMap.get(subKey)){
                            if(sWrapObj.fieldOrVal != null)
                            {
                                String sWrapObjValTemp = sWrapObj.fieldOrVal.toLowerCase();
                                if(!sTempSet.contains(sWrapObjValTemp) && sWrapObj.fieldOrVal.trim()!=''){
                                    tempSWList.add(sWrapObj);
                                    sTempSet.add(sWrapObjValTemp);
                                }
                            }
                            
                        }
                        fscTp.valueNdFieldListMap.put(subKey,tempSWList);
                    }
                }
            }
            
            doc.Body = Blob.valueOf(JSON.serialize(mapIdAndFSCList));
            try{
                update doc;
                go();
                message = 'Settings are saved successfully.';
            }catch(Exception ex){}
        }
    }
    
    public void saveclone(){}
    
    
    public void removeFieldStructure(){
        if(listIndex!=Null && fieldStructureList.size()>listIndex){
            FieldStructureClass fieldStru = fieldStructureList.remove(listIndex);
            availabelApis.remove(fieldStru.masterFieldApi.toLowerCase());
            //Boolean b = sobjectFieldSet.remove(fieldStru.masterFieldApi.toLowerCase());
        }
    }
    
    public void removeValue(){
        if(listIndex!=Null && fieldStructureList.size()>listIndex){
            if(masterValue != Null && masterValue != ''){
                fieldStructureList[listIndex].valueNdFieldListMap.remove(masterValue);
            }
            tempListIndex = listIndex;
        }
    }
    
    public void removeField(){
        if(listIndex!=Null && fieldStructureList.size() > listIndex){
            if(masterValue != Null && masterValue != ''){
                if(fieldApeIndex != Null){
                    StringWrapper tempSWrap = fieldStructureList[listIndex].valueNdFieldListMap.get(masterValue).remove(fieldApeIndex);
                    String tempKeyString = fieldStructureList[listIndex].masterFieldApi.toLowerCase() +'~'+masterValue.toLowerCase();
                    Set<String> tempSet = masterApiValFieldsSetMap.get(tempKeyString);
                    if(tempSet!=Null){
                        tempSet.remove(tempSWrap.fieldOrVal.toLowerCase());
                    }
                }
                tempInnerMstVal = masterValue;
            }
            tempListIndex = listIndex;
        }
    }
    
    public class FieldStructureClass{
        @AuraEnabled
        public String masterFieldApi{get;set;}
        
        @AuraEnabled
        public Map<String,List<StringWrapper>> valueNdFieldListMap{get;set;}
        
        public fieldStructureClass(String masterFieldApi){
            this.masterFieldApi = masterFieldApi;
            valueNdFieldListMap = new Map<String,List<StringWrapper>>();
        }
    }
    
    public class StringWrapper{
        @AuraEnabled
        public String fieldOrVal{get;set;}
    }
    
    
    public String contractFields{get;set;}
    private Set<String> sobjectFieldSet = new Set<String>();
    public void getContractFields(){
        String fieldLabels='';
        Map<String, Schema.SObjectField> objectFields;
        if(objectType=='Contract'){
            objectFields = Schema.SObjectType.Contract_Vehicle__c.fields.getMap();
            
        }
        else{
            objectFields = Schema.SObjectType.SubContract__c.fields.getMap();
        }
        for(String fKey : objectFields.keyset()){
            String tempAPI = objectFields.get(fKey).getDescribe().getName();
            sobjectFieldSet.add(tempAPI.toLowerCase());
            fieldLabels += '"'+tempAPI+'",';
        }
        fieldLabels.removeEnd(',');
        contractFields = fieldLabels;
        //return fieldLabels;
    }
    
    public List<SelectOption> getsObjectTypeOptions(){
        List<SelectOption> sObjectTypeOptions = new List<SelectOption>{new SelectOption('Contract','Contract'), new SelectOption('Sub Contract','Sub Contract')};
            return sObjectTypeOptions;
    }
    
    public List<SelectOption> getRecordTypeOptions(){
        List<SelectOption> recordTypeOptions = new List<SelectOption>();
        List<RecordType> recordTypes;
        if(isContract){
            recordTypes = [SELECT Id,Name FROM RecordType WHERE IsActive=true AND SobjectType='TM_TOMA__Contract_Vehicle__c'];
        }else{
            recordTypes = [SELECT Id,Name FROM RecordType WHERE IsActive=true AND SobjectType='TM_TOMA__SubContract__c'];
        }
        //List<TM_RecordTypeSetting__c> lstRecordType = [SELECT RecordType_Id__c,Record_Type_Label__c FROM TM_RecordTypeSetting__c];
        Set<Id> addedIds = new Set<Id>();
        
        for(RecordType rType : recordTypes){
            if(!addedIds.contains(rType.Id)){
                recordTypeOptions.add(new SelectOption(rType.Id,rType.Name));
            }
        }
        
        if(recordTypeOptions.isEmpty()){
            recordTypeOptions.add(new SelectOption('noRec','No RecordType'));
        }
        if(recordTypeOptions.size()>1){
            isCloneButton = true;
        }else{
            isCloneButton = false;
        }
        return recordTypeOptions;
    }
    
    public void cloneLayout(){
        availabelApis.clear();
        init();
        douitility(recordCloinId);
        String deSerilisedStrJSON = JSON.serialize(fieldStructureList);
        fieldStructureList = new List<FieldStructureClass>((List<FieldStructureClass>)JSON.deserialize(deSerilisedStrJSON, List<FieldStructureClass>.class));
        mapIdAndFSCList.put(recordTypeId, fieldStructureList);
        cloneInitiated = true;
        btnLabel = 'Save Clone';
    }
    
    public void go(){
        availabelApis.clear();
        init();
        douitility(recordTypeId);
        cloneInitiated = false;
        btnLabel = 'Save';
    }
    
    private void douitility(String rTypeString){
        if(mapIdAndFSCList.containsKey(rTypeString)){
            fieldStructureList = mapIdAndFSCList.get(rTypeString);
            for(FieldStructureClass fsc : fieldStructureList){
                availabelApis.add(fsc.masterFieldApi.toLowerCase());
                Map<String,List<StringWrapper>> valueNdFieldListMapTemp = fsc.valueNdFieldListMap;
                for(String keyApi : valueNdFieldListMapTemp.keySet()){
                    String tempKey = fsc.masterFieldApi.toLowerCase()+'~'+keyApi.toLowerCase();
                    Set<String> tempDependentApiSet = new Set<String>();
                    for(StringWrapper sWrap : valueNdFieldListMapTemp.get(keyApi)){
                        tempDependentApiSet.add(sWrap.fieldOrVal.toLowerCase());
                    }
                    masterApiValFieldsSetMap.put(tempKey,tempDependentApiSet);
                }
            }
        }else{
            fieldStructureList = new List<FieldStructureClass>();
            mapIdAndFSCList.put(rTypeString, fieldStructureList);
        }
    }
    
    public void selectSObject(){
        if(objectType=='Contract'){
            isContract = true;
        }else{
            isContract = false;
        }
    }
    private Boolean getRelationType(String apiString){
        
        if(objectType=='Contract'){
            Schema.SObjectType t = Schema.getGlobalDescribe().get('TM_TOMA__Contract_Vehicle__c');
            Schema.DescribeSObjectResult r = t.getDescribe();
            Schema.DescribeFieldResult f = r.fields.getMap().get(apiString).getDescribe();
            if (f.getType() == Schema.DisplayType.Reference){
                return false;
            } 
        }else{
            Schema.SObjectType t = Schema.getGlobalDescribe().get('TM_TOMA__Subcontract__c');
            Schema.DescribeSObjectResult r = t.getDescribe();
            Schema.DescribeFieldResult f = r.fields.getMap().get(apiString).getDescribe();
            if (f.getType() == Schema.DisplayType.Reference){
                return false;
            } 
        }
        return true;
    }
}