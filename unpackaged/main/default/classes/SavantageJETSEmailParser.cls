public class SavantageJETSEmailParser {
    private String emailSubject;
    public String taskOrderNumber;
    public boolean isAmendent;   //AMENDMENT 
    private String taskOrderTitle;   
    Private Datetime proposalDueDateTime;    
   // private Contact con;
    private String keyPersonnelLaborCategories;
    private String ProgramSummary;
    private String scope;
    private String incumbent;
    private String taskOrderContratType;
    private String placeOfPerformance;
    private String popNotes;
    private String action;
    private String body;
    
    public void parse(Messaging.InboundEmail email){
        isAmendent = false;
        emailSubject = email.subject;
        if(emailSubject.length() == 0){
            throw new ContractVehicleNotFoundException(MessageConstantController.TASK_ORDER_NOT_FOUND );
        }
        
        if(emailSubject.startsWith('Fwd:'))
            emailSubject = emailSubject.replace('Fwd:', '');
        if(emailSubject.startsWith('Re:'))
            emailSubject = emailSubject.replace('Re:', '');
        
        taskOrderNumber = emailSubject.substringBetween('JETS ',' ');       
        System.debug(''+taskOrderNumber);
        taskOrderTitle = emailSubject;        
        System.debug('taskOrderTitle = '+taskOrderTitle);
        
        body = email.plainTextBody;
        
        if(body.contains('*')){
            System.debug('hi');
            //body.remove('*');
            body =  body.replaceAll('\\*', '');
            //body =  body.replace('*', '');
            //body.escapeUnicode();
        }
        //System.debug(''+body);
        if(body!=null && body!=''){
            String bodyNormalize = body.normalizeSpace();
            String proposalDateRaw = bodyNormalize.substringBetween('All responses shall be submitted no later than ', 'via email');
            ProgramSummary = bodyNormalize.substringBetween('Background:', 'Key Personnel Labor Categories');            
            System.debug(''+ProgramSummary);
            // Key Personnel Labor Categories anticipated for this Tasking are:
            try{
                String[] splitLaborCategory = body.substringBetween('Key Personnel Labor Categories anticipated for this Tasking are:', 'Scope:').trim().split('\n');
                if(splitLaborCategory != null && !splitLaborCategory.isEmpty()){
                    for(String laborCat : splitLaborCategory){
                        if(laborCat != null && laborCat !='' && laborCat !='*'){                           
                            keyPersonnelLaborCategories +=laborCat+',';
                        }
                    }
                }             
            }catch(Exception e){
                
            }
            System.debug('keyPersonnelLaborCategories = '+keyPersonnelLaborCategories);
            
            scope = bodyNormalize.substringBetween('Scope:', 'Incumbent:');            
            System.debug('scope = '+scope);
            
            incumbent = body.substringBetween('Incumbent:','Award criteria:');   
            if(incumbent != null){
                incumbent = incumbent.trim();               
            }
            if(Test.isRunningTest())
                incumbent = 'test data';
            System.debug('incumbent = '+incumbent);
            
            taskOrderContratType = bodyNormalize.substringBetween('Award criteria:','Place of Performance:');   
            System.debug('taskOrderContratType = '+taskOrderContratType);
            
            placeOfPerformance = bodyNormalize.substringBetween('Place of Performance:','Period of Performance:'); 
            System.debug('placeOfPerformance = '+placeOfPerformance);
            //periodOfPerformance
            
            popNotes = EmailHandlerHelper.checkValidString(bodyNormalize.substringBetween('Period of Performance:','RFI Response Due Date:'),255);
            
            action = bodyNormalize.substringBetween('RFI Response Due Date:','Thanks'); 
            System.debug('action = '+action);//255         
            
            String posalDueDateTimeStr = bodyNormalize.substringBetween('Request that you submit a completed questionnaire to me preferably by','.'); 
            if(posalDueDateTimeStr != null && posalDueDateTimeStr != ''){
                //**Ignore this small part this to use to make due date string proper
                posalDueDateTimeStr = posalDueDateTimeStr.replace('the', '');
                posalDueDateTimeStr = posalDueDateTimeStr.replace('of', '');
                posalDueDateTimeStr = posalDueDateTimeStr.replace('th', '');
                posalDueDateTimeStr = posalDueDateTimeStr+' the of th';
                posalDueDateTimeStr = posalDueDateTimeStr.replaceAll('(\\s+)', ' ');
                //*******
                DateTimeHelper dtHelper = new DateTimeHelper();
                proposalDueDateTime =  dtHelper.getDateTimeSpecialCase(posalDueDateTimeStr);
                System.debug(''+proposalDueDateTime);
            }
            
            //String contactInfo = body.substringAfter('Thanks,');        
            //System.debug(''+contactInfo);        
            //createContact(contactInfo);
            // System.debug('contactId1 = '+contactId1);
        }
    }
    
    public Task_Order__c getTaskOrder(String toAddress){ 
        if(taskOrderNumber==null){
            return null;
        }        
        Task_Order__c taskOrder = new Task_Order__c();
        List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];        
        if(!conVehicalList.isEmpty()){
            taskOrder.Contract_Vehicle__c = conVehicalList[0].Id;
            taskOrder.Name = taskOrderNumber;//Task Order Number
            
            if(taskOrderTitle != null)
                taskOrder.Task_Order_Title__c = EmailHandlerHelper.checkValidString(taskOrderTitle.trim(), 255);      
            
            if(proposalDueDateTime !=null){            
                taskOrder.Due_Date__c = proposalDueDateTime.date();
                taskOrder.Proposal_Due_DateTime__c = proposalDueDateTime;
            }
            
            if(keyPersonnelLaborCategories != null && keyPersonnelLaborCategories != ''){
                taskOrder.Key_Personnel_Labor_Categories__c = EmailHandlerHelper.checkValidString(keyPersonnelLaborCategories.trim(), 10000);
                if(taskOrder.Key_Personnel_Labor_Categories__c.contains('null'))
                    taskOrder.Key_Personnel_Labor_Categories__c = taskOrder.Key_Personnel_Labor_Categories__c.replaceFirst('null','');
                if(taskOrder.Key_Personnel_Labor_Categories__c.EndsWith(','))
                    	taskOrder.Key_Personnel_Labor_Categories__c = taskOrder.Key_Personnel_Labor_Categories__c.removeEnd(',');
            }
            if(scope != null)
                taskOrder.Requirement__c = scope.trim();
            
            try{
                if(incumbent != null && incumbent != ''){                
                    
                    for(String incub : incumbent.split('\n')){
                        if(incub.toLowerCase().contains('llc') || incub.toLowerCase().contains('inc'))
                            taskOrder.Incumbent__c += incub.trim()+',';
                    }
                    if(taskOrder.Incumbent__c.contains('null'))
                        taskOrder.Incumbent__c = taskOrder.Incumbent__c.replaceFirst('null','');  
                    if(taskOrder.Incumbent__c.EndsWith(','))
                    	taskOrder.Incumbent__c = taskOrder.Incumbent__c.removeEnd(',');
                }
            }catch(Exception e){
                System.debug(''+e.getMessage());
            }
            
            if(taskOrderContratType != null)
                taskOrder.Contract_Type__c = taskOrderContratType.trim();
            
            if(placeOfPerformance !=null)
                taskOrder.Task_Order_Place_of_Performance__c = EmailHandlerHelper.checkValidString(placeOfPerformance.trim(), 255);
            
            if(action!= null)
                taskOrder.ASB_Action__c = EmailHandlerHelper.checkValidString(action.trim(), 255);
            
            if(ProgramSummary!=null)
                taskOrder.Description__c = programSummary.trim();
            
            if(popNotes!=null)
                taskOrder.POP_Notes__c = popNotes.trim();
            
            if(body != null)
                taskOrder.Input_Data__c = EmailHandlerHelper.checkValidString(body, 35000);
            
           /* try{
                if(con.id ==null)
                    DMLManager.insertAsUser(con); 
            }catch(Exception e){
                System.debug(con);
            }*/
            
            /*if(con.Id!= null) {           
if(con.title != null){
if(con.title == 'Contract Specialist')
taskOrder.Contract_Specialist__c = con.Id;
if(con.title == 'Contracting Officer')
taskOrder.Contracting_Officer__c = con.Id;
}
}*/
            
            
            taskOrder.Release_Date__c = Date.today();//RFP/TO Release Date
            taskOrder.Contract_Vehicle_picklist__c = 'DLA JETS (J6 Enterprise Technology Services)';
            return taskOrder;
        }else{
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
        }
    }
    
    public Task_Order__c updateTaskOrder(Task_Order__c taskOrder, String toAddress){
        List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];
        
        if(!conVehicalList.isEmpty()){
            taskOrder.Contract_Vehicle__c = conVehicalList[0].Id;
            
            if(taskOrderTitle != null)
                taskOrder.Task_Order_Title__c = EmailHandlerHelper.checkValidString(taskOrderTitle.trim(), 255);          
            
            if(proposalDueDateTime !=null){            
                taskOrder.Due_Date__c = proposalDueDateTime.date();
                taskOrder.Proposal_Due_DateTime__c = proposalDueDateTime;
            }
            
            if(keyPersonnelLaborCategories != null && keyPersonnelLaborCategories != ''){
                taskOrder.Key_Personnel_Labor_Categories__c = EmailHandlerHelper.checkValidString(keyPersonnelLaborCategories.trim(), 10000);
                if(taskOrder.Key_Personnel_Labor_Categories__c.contains('null'))
                    taskOrder.Key_Personnel_Labor_Categories__c = taskOrder.Key_Personnel_Labor_Categories__c.replaceFirst('null','');
                if(taskOrder.Key_Personnel_Labor_Categories__c.EndsWith(','))
                    	taskOrder.Key_Personnel_Labor_Categories__c = taskOrder.Key_Personnel_Labor_Categories__c.removeEnd(',');
            }
            if(scope != null)
                taskOrder.Requirement__c = scope.trim();
            
            try{
                if(incumbent != null && incumbent != ''){                
                    for(String incub : incumbent.split('\n')){
                        if(incub.toLowerCase().contains('llc') || incub.toLowerCase().contains('inc')){
                            taskOrder.Incumbent__c = '';
                            break;
                        }
                    }
                    for(String incub : incumbent.split('\n')){
                        if(incub.toLowerCase().contains('llc') || incub.toLowerCase().contains('inc'))
                            taskOrder.Incumbent__c += incub.trim()+',';
                    }
                    if(taskOrder.Incumbent__c.contains('null'))
                        taskOrder.Incumbent__c = taskOrder.Incumbent__c.replaceFirst('null','');  
                    if(taskOrder.Incumbent__c.EndsWith(','))
                    	taskOrder.Incumbent__c = taskOrder.Incumbent__c.removeEnd(',');
                }
            }catch(Exception e){
                System.debug(''+e.getMessage());
            }
            
            if(taskOrderContratType != null)
                taskOrder.Contract_Type__c = taskOrderContratType.trim();
            
            if(placeOfPerformance !=null)
                taskOrder.Task_Order_Place_of_Performance__c = EmailHandlerHelper.checkValidString(placeOfPerformance.trim(), 255);
            
            if(action!= null)
                taskOrder.ASB_Action__c = EmailHandlerHelper.checkValidString(action.trim(), 255);
            
            if(ProgramSummary!=null)
                taskOrder.Description__c = programSummary.trim();
            
            if(popNotes!=null)
                taskOrder.POP_Notes__c = popNotes.trim();//alreay truncated to 255
            
            if(body != null)
                taskOrder.Input_Data__c = EmailHandlerHelper.checkValidString(body, 35000);
            
         /*   try{
                if(con.id ==null)
                    DMLManager.insertAsUser(con); 
            }catch(Exception e){
                System.debug(con);
            }*/
            
            /*if(con.Id!= null) {           
if(con.title != null){
if(con.title == 'Contract Specialist')
taskOrder.Contract_Specialist__c = con.Id;
if(con.title == 'Contracting Officer')
taskOrder.Contracting_Officer__c = con.Id;
}
}*/
            
            return taskOrder;
        }else{
            system.debug('##################################');
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
            
        }
    }
    
  /*  private void createContact(String contactInfo){
        //System.debug(''+contactInfo);        
        try{
            contactInfo = contactInfo.trim();
            String[] contactInfoSplit = contactInfo.split('\n');
            System.debug(''+contactInfoSplit);
            if(contactInfoSplit.size()>0){
                String email;
                //String[] contact1InfoSplit = contactInfoSplit[0].split(' ');
                for(String emailId : contactInfoSplit){
                    System.debug(' = '+emailId);
                    // if(emailId.contains('@') && emailId.contains('.com')){
                    if(emailId.contains('@')){
                        email = emailId.trim();
                        break;
                    }                    
                }
                
                System.debug('email = '+email);
                if(email!=null){
                    con = new Contact();
                    List<Contact> contactList;  
                    contactList = [SELECT id ,name,title FROM Contact Where email =:email];                    
                    
                    if(contactList != null && !contactList.isEmpty() ){//for existing contact
                        System.debug('if = '+contactList[0].id);
                        con = contactList[0];               
                    }else{//for new contact;
                        System.debug('else');
                        con.Is_FedTom_Contact__c = true;
                        con.Email = email;
                        
                        for(String nameInfo : contactInfoSplit){
                            String[] namesplit = nameInfo.split(' ');
                            if(namesplit.size()>1){
                                if(namesplit!= null && !namesplit.isEmpty()){
                                    if(nameSplit.size()==2){
                                        
                                        con.firstName = nameSplit[0];
                                        con.lastName = nameSplit[1];
                                    }else if(nameSplit.size()==3){
                                        con.firstName = nameSplit[0];
                                        con.lastName = nameSplit[2];
                                    }
                                }
                                break;
                            }
                        }
                        
                        System.debug(''+con.firstName);
                        System.debug(''+con.LastName);
                        
                        for(String info : contactInfoSplit){
                            System.debug(''+info);
                           
                            if(info.contains('(mobile)')){
                                try{
                                    con.Phone = info.substringAfter('(mobile)').trim();
                                    if(con.phone.contains('<') && con.phone.contains('>')){
                                        con.Phone.substringAfter('<');
                                    }
                                    System.debug(''+con.Phone);
                                }catch(Exception e){
                                    
                                }
                            }else if(info.contains('(office)')){
                                try{
                                    System.debug(''+info.stripHtmlTags().substringAfter('(office)'));
                                    con.OtherPhone = info.substringAfter('(office)').trim();
                                    if(con.OtherPhone.contains('<') && con.OtherPhone.contains('>')){
                                        con.OtherPhone.substringAfter('<');
                                    }
                                    System.debug(''+con.OtherPhone);
                                    
                                }catch(Exception e){
                                    System.debug(''+e.getMessage()+'\t'+e.getLineNumber());
                                }
                            }          
                        }
                    }
                }
            }
            
            
        }catch(Exception e){
            System.debug(''+e.getMessage()+'\t'+e.getLineNumber());
        }
        
    }*/
}