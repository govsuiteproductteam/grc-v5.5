@isTest
public class GSAConnectDeleteDuplicateTest {
    public static TestMethod void deleteDuplicatehTest(){
        Account acc = TestDataGenerator.createAccount('Test Account');
        insert acc;
        Contract_Vehicle__c cv =  TestDataGenerator.createContractVehicle('1234', acc);
        insert cv; 
        Contract_Vehicle__c cv2 =  TestDataGenerator.createContractVehicle('12345', acc);
        insert cv2; 
        Task_Order__c to1 = TestDataGenerator.createTaskOrder('TO1234', acc, cv);
        insert to1;
        Task_Order__c to2 = TestDataGenerator.createTaskOrder('TO1234', acc, cv);
        insert to2;
        Task_Order__c to3 = TestDataGenerator.createTaskOrder('TO1234', acc, cv2);
        insert to3;
        Task_Order__c to4 = TestDataGenerator.createTaskOrder('TO1234', acc, cv2);
        insert to4;
        
        Test.startTest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        req.requestBody = Blob.valueof('body');
        req.httpMethod = 'POST';
        
        RestContext.request = req;
        RestContext.response = res;
        GSAConnectDeleteDuplicate.doPost();
        List<Task_Order__c> toList = [SELECT Id FROM Task_Order__c];       
        System.assert(toList.size() == 2);
        Test.stopTest();
    }
}