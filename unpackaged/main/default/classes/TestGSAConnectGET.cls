@isTest
public with sharing class TestGSAConnectGET {
    public static TestMethod void testDoPost(){
        
        StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'ActiveRFQStaticResource' LIMIT 1];
        String body = sr.Body.toString();
        
        Test.startTest();        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        req.requestBody = Blob.valueof(body);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        GSAConnectGET.doPost();
        system.assertNotEquals(sr, null);
       
        Test.stopTest();
    }
}