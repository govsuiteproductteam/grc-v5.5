@isTest
public class Test_TM_DistributionCompCont {
    public static testMethod void testDistributionCompContForContrctVehical(){
        List<Profile> profileList = [SELECT Id FROM Profile where (Name = 'System Administrator') Limit 1];//OR Name = 'Custom Partner Community Login User' OR Name  = 'Custom Customer Community Plus Login User'
        User usr;
        if(profileList != null && profileList.size()>0){
            usr = TestDataGenerator.createUser(null,TRUE,'test1', 'atest1','testCheck1packaging@gmail.com','taskordercapturepackaging@demo.com','atest1',profileList[0],'America/Los_Angeles','en_US','UTF-8','en_US');
            insert usr; 
        }
        Account account = TestDataGenerator.createAccount('Test Account');
        insert account;
        Contract_Vehicle__c contractVehicle =  TestDataGenerator.createContractVehicle('12345', account);
        insert contractVehicle;
        
        TM_DistributionCompCont.checkLicensePermition1();
        List<DistributionFieldSetWrapper> DisFieldWrapList = TM_DistributionCompCont.getRelatedDistributionColumeName();
        System.assert(DisFieldWrapList.size() >= 1);
        List<GetDistributionDataWrap> DistDatWrapList = TM_DistributionCompCont.getRelatedDistributionRecords(contractVehicle.Id);
        System.assert(DistDatWrapList.size() == 0);
        
        GetDistributionDataWrap disDataWrap = TM_DistributionCompCont.addNewDistributionRowInWrap(contractVehicle.Id);
        DistDatWrapList.add(disDataWrap);
        string distributionTableJSON = '[';
        string fieldString='';
        for (integer i=0; i<DistDatWrapList.size(); i++){
            distributionTableJSON += 'id'+'SPLITFIELD'+'undefined'+'SPLITDATA';//DistDatWrapList[i].disRecord.Id
            if(DisFieldWrapList.size() > 0 )
            {
                for (integer j=0; j<DisFieldWrapList.size(); j++){
                    if(DisFieldWrapList[j].fieldAPI == 'User__c')
                        distributionTableJSON += DisFieldWrapList[j].fieldAPI+'SPLITFIELD'+usr.Id+'SPLITDATA';//DistDatWrapList[i].disRecord.get(DisFieldWrapList[j].fieldAPI)
                    else
                        distributionTableJSON += DisFieldWrapList[j].fieldAPI+'SPLITFIELD'+'undefined'+'SPLITDATA';//DistDatWrapList[i].disRecord.get(DisFieldWrapList[j].fieldAPI)
                    fieldString =fieldString+',';
                }
                distributionTableJSON += 'SPLITPARENT';
            }
        }
        String fieldApiName='';
        for (integer j=0; j<DisFieldWrapList.Size(); j++){
            fieldApiName=fieldApiName+DisFieldWrapList[j].fieldAPI+',';
        }
        TM_DistributionCompCont.saveDistributionRecordList(distributionTableJSON, '', fieldApiName,contractVehicle.Id);
        DistDatWrapList = TM_DistributionCompCont.getRelatedDistributionRecords(contractVehicle.Id);
        System.assert(DistDatWrapList.size() == 1);
        for (integer i=0; i<DistDatWrapList.size(); i++){
            distributionTableJSON += 'id'+'SPLITFIELD'+DistDatWrapList[i].disRecord.Id+'SPLITDATA';//DistDatWrapList[i].disRecord.Id
            if(DisFieldWrapList.size() > 0 )
            {
                for (integer j=0; j<DisFieldWrapList.size(); j++){
                    if(DisFieldWrapList[j].fieldAPI == 'User__c')
                        distributionTableJSON += DisFieldWrapList[j].fieldAPI+'SPLITFIELD'+usr.Id+'SPLITDATA';//DistDatWrapList[i].disRecord.get(DisFieldWrapList[j].fieldAPI)
                    else
                        distributionTableJSON += DisFieldWrapList[j].fieldAPI+'SPLITFIELD'+'undefined'+'SPLITDATA';//DistDatWrapList[i].disRecord.get(DisFieldWrapList[j].fieldAPI)
                }
                distributionTableJSON += 'SPLITPARENT';
            }
        }
        fieldString = fieldString.removeEnd(',');
        TM_DistributionCompCont.saveDistributionRecordList(distributionTableJSON, '', fieldApiName,contractVehicle.Id);
        DistDatWrapList = TM_DistributionCompCont.getRelatedDistributionRecords(contractVehicle.Id);
        TM_DistributionCompCont.deleteDistribution(DistDatWrapList[0].disRecord.Id);
    }
    public static testMethod void testDistributionCompContForSubcontrct(){
        List<Profile> profileList = [SELECT Id FROM Profile where (Name = 'System Administrator') Limit 1];//OR Name = 'Custom Partner Community Login User' OR Name  = 'Custom Customer Community Plus Login User'
        User usr;
        if(profileList != null && profileList.size()>0){
            usr = TestDataGenerator.createUser(null,TRUE,'test1', 'atest1','testCheck1packaging@gmail.com','taskordercapturepackaging@demo.com','atest1',profileList[0],'America/Los_Angeles','en_US','UTF-8','en_US');
            insert usr; 
        }
        Subcontract__c subContract = TestDataGenerator.createSubContract('12345');
        insert subContract;
        List<DistributionFieldSetWrapper> DisFieldWrapList = TM_DistributionCompCont.getRelatedDistributionColumeName();
        System.assert(DisFieldWrapList.size() >= 1);
        
        TM_DistributionCompCont.getRelatedDistributionRecords(subContract.Id);
        
        GetDistributionDataWrap disDataWrap = TM_DistributionCompCont.addNewDistributionRowInWrap(subContract.Id);
        List<GetDistributionDataWrap> DistDatWrapList = TM_DistributionCompCont.getRelatedDistributionRecords(subContract.Id);
        DistDatWrapList.add(disDataWrap);
        string distributionTableJSON = '[';
        for (integer i=0; i<DistDatWrapList.size(); i++){
            distributionTableJSON += 'id'+'SPLITFIELD'+'undefined'+'SPLITDATA';//DistDatWrapList[i].disRecord.Id
            if(DisFieldWrapList.size() > 0 )
            {
                for (integer j=0; j<DisFieldWrapList.size(); j++){
                    if(DisFieldWrapList[j].fieldAPI == 'User__c')
                        distributionTableJSON += DisFieldWrapList[j].fieldAPI+'SPLITFIELD'+usr.Id+'SPLITDATA';//DistDatWrapList[i].disRecord.get(DisFieldWrapList[j].fieldAPI)
                    else
                        distributionTableJSON += DisFieldWrapList[j].fieldAPI+'SPLITFIELD'+'undefined'+'SPLITDATA';//DistDatWrapList[i].disRecord.get(DisFieldWrapList[j].fieldAPI)
                }
                distributionTableJSON += 'SPLITPARENT';
            }
        }
        String fieldApiName='';
        for (integer j=0; j<DisFieldWrapList.Size(); j++){
            fieldApiName=fieldApiName+DisFieldWrapList[j].fieldAPI+',';
        }
        TM_DistributionCompCont.saveDistributionRecordList(distributionTableJSON, '',fieldApiName, subContract.Id);
        DistDatWrapList = TM_DistributionCompCont.getRelatedDistributionRecords(subContract.Id);
        System.assert(DistDatWrapList.size() == 1);
    }
    
    public static testMethod void testDistributionCompContForTaskOrder(){
        List<Profile> profileList = [SELECT Id FROM Profile where (Name = 'System Administrator') Limit 1];//OR Name = 'Custom Partner Community Login User' OR Name  = 'Custom Customer Community Plus Login User'
        User usr;
        if(profileList != null && profileList.size()>0){
            usr = TestDataGenerator.createUser(null,TRUE,'test1', 'atest1','testCheck1packaging@gmail.com','taskordercapturepackaging@demo.com','atest1',profileList[0],'America/Los_Angeles','en_US','UTF-8','en_US');
            insert usr; 
        }
        Account account = TestDataGenerator.createAccount('Test Account');
        insert account;
        Contract_Vehicle__c contractVehicle =  TestDataGenerator.createContractVehicle('12345', account);
        insert contractVehicle;
      
        Task_Order__c taskOrder = TestDataGenerator.createTaskOrder('123213', account, contractVehicle);
        insert taskOrder;
        
        
        List<DistributionFieldSetWrapper> DisFieldWrapList = TM_DistributionCompCont.getRelatedDistributionColumeName();
        System.assert(DisFieldWrapList.size() >= 1);
        
        TM_DistributionCompCont.getRelatedDistributionRecords(taskOrder.Id);
        
        GetDistributionDataWrap disDataWrap = TM_DistributionCompCont.addNewDistributionRowInWrap(taskOrder.Id);
        List<GetDistributionDataWrap> DistDatWrapList = TM_DistributionCompCont.getRelatedDistributionRecords(taskOrder.Id);
        DistDatWrapList.add(disDataWrap);
        string distributionTableJSON = '[';
        for (integer i=0; i<DistDatWrapList.size(); i++){
            distributionTableJSON += 'id'+'SPLITFIELD'+'undefined'+'SPLITDATA';//DistDatWrapList[i].disRecord.Id
            if(DisFieldWrapList.size() > 0 )
            {
                for (integer j=0; j<DisFieldWrapList.size(); j++){
                    if(DisFieldWrapList[j].fieldAPI == 'User__c')
                        distributionTableJSON += DisFieldWrapList[j].fieldAPI+'SPLITFIELD'+usr.Id+'SPLITDATA';//DistDatWrapList[i].disRecord.get(DisFieldWrapList[j].fieldAPI)
                    else
                        distributionTableJSON += DisFieldWrapList[j].fieldAPI+'SPLITFIELD'+'undefined'+'SPLITDATA';//DistDatWrapList[i].disRecord.get(DisFieldWrapList[j].fieldAPI)
                }
                distributionTableJSON += 'SPLITPARENT';
            }
        }
        String fieldApiName='';
        for (integer j=0; j<DisFieldWrapList.Size(); j++){
            fieldApiName=fieldApiName+DisFieldWrapList[j].fieldAPI+',';
        }
        TM_DistributionCompCont.saveDistributionRecordList(distributionTableJSON, '',fieldApiName, taskOrder.Id);
        DistDatWrapList = TM_DistributionCompCont.getRelatedDistributionRecords(taskOrder.Id);
        System.assert(DistDatWrapList.size() == 1);
        
        TM_DistributionCompCont.deleteDistribution(DistDatWrapList[0].disRecord.Id);
        DistDatWrapList = TM_DistributionCompCont.getRelatedDistributionRecords(taskOrder.Id);
        System.assert(DistDatWrapList.size() == 0);
    }
}