public class TM_getPicklistValuesDynamic {

    //public Static Contract_Vehicle__c bid{get;set;}
   // public Static Contract_Vehicle__c testBid{get;set;}
    
    @AuraEnabled
    public static List<PickFieldwrap> getStages(String Fieldname)// List<String>
    {
        system.debug('in picklist');
         return getPicklistValues('TM_TOMA__Distribution__c',Fieldname);
    }
/*    @AuraEnabled
   public static List<String> getStages(String Fieldname)
    {
        system.debug('in picklist');
        return getPicklistValues('TM_TOMA__Distribution__c',Fieldname);
    }*/
    
  /*  public static List<String> getPicklistValues(String ObjectApi_name,String Field_name){ 
        List<String> lstPickvals=new List<String>();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectApi_name);//From the Object Api name retrieving the SObject
        Sobject Object_name = targetType.newSObject();
        Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
        List<Schema.PicklistEntry> pick_list_values = field_map.get(Field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
        for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
            lstPickvals.add(a.getValue());//add the value  to our final list
        }
        system.debug('lstPickvals '+lstPickvals);
        return lstPickvals;
    }*/
    
    public static List<PickFieldwrap> getPicklistValues(String ObjectApi_name,String Field_name){ 
      List<PickFieldwrap> lstPickvals=new List<PickFieldwrap>();
      Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectApi_name);//From the Object Api name retrieving the SObject
        Sobject Object_name = targetType.newSObject();
      Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
        List<Schema.PicklistEntry> pick_list_values = field_map.get(Field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
    for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
      lstPickvals.add(new PickFieldwrap(''+a.getLabel(),''+a.getValue()));//add the value  to our final list
   }
  //system.debug('PICKKLLLIIISSSTTT'+lstPickvals);
  return lstPickvals;
     }
    
    
}