public class LincenseKeyHelper{
     public static String checkLicensePermition()
    {
        string errorMsg;
        TM_License_Key_Setting__c customSetting = TM_License_Key_Setting__c.getOrgDefaults();
        System.debug('customSetting '+customSetting);
        String fedcaptureKey = customSetting.FedCapture_Key__c;
        System.debug('fedcaptureKey '+fedcaptureKey);
        EncryptionDecryptionManager.KeyInfo keyInfo;
        if(fedcaptureKey != null){
            String decriptedKey = EncryptionDecryptionManager.decryptValue('FedCapture',fedcaptureKey );
            System.debug('decriptedKey '+decriptedKey);
            keyInfo = EncryptionDecryptionManager.getKeyInfo(decriptedKey );
        }
        else
        {
            errorMsg='You don\'t have permission to access this component please contact your Administrator.';
            //return errorMsg;
        }
        if(keyInfo != null)
        {
            if(keyInfo.organizationId != '' && keyInfo.startDate != null && keyInfo.endDate != null && keyInfo.licenseCount != null && keyInfo.packageName != ''){
                String SubscribedApp = [Select Id,Subscribed_Apps__c From User Where Id = :UserInfo.getUserId()][0].Subscribed_Apps__c;
                List<Organization> orgList = [Select Id From Organization where id =: keyInfo.organizationId];
                Integer userLicenseCount = [Select count() From User where Subscribed_Apps__c includes('FedCapture') And isActive = true];
                if(keyInfo.licenseCount  != null && userLicenseCount > keyInfo.licenseCount ){
                    errorMsg = 'Your organization has exceeded the number of licensed seats for this application. Please contact your administrator for help.';
                }
                else{
                    if(orgList.size()>0 && keyInfo.packageName == 'FedCapture' && SubscribedApp != null && SubscribedApp.contains('FedCapture') && 
                         keyInfo.StartDate <= Date.today() && keyInfo.EndDate >= Date.today() )
                    {
                        return 'Yes';
                    }
                    else
                    {
                       errorMsg='You don\'t have permission to access this component please contact your Administrator.';
                       //return errorMsg;
                    }
                }
            }
            else{
                errorMsg='You don\'t have permission to access this component please contact your Administrator.';
                //return errorMsg;
            }
        }
        else{
                errorMsg='You don\'t have permission to access this component please contact your Administrator.';
                //return errorMsg;
            }
        
        System.debug('keyInfo '+keyInfo);
       // System.debug('keyInfo = organizationId= '+keyInfo.organizationId+' startDate '+keyInfo.startDate+' endDate '+keyInfo.endDate+' licenseCount '+keyInfo.licenseCount+' packageName '+keyInfo.packageName);
       if(Test.isRunningTest()){
           return 'Yes';
       }
        return errorMsg;
    }
    
    public static String checkLicensePermitionForFedTOM()
    {
        string errorMsg;
        TM_License_Key_Setting__c customSetting = TM_License_Key_Setting__c.getOrgDefaults();
        System.debug('customSetting '+customSetting);
        String fedTOMKey = customSetting.FedTOM_Key__c;
        System.debug('fedTOMKey '+fedTOMKey );
        EncryptionDecryptionManager.KeyInfo keyInfo;
        if(fedTOMKey != null){
            String decriptedKey = EncryptionDecryptionManager.decryptValue('FedTOM',fedTOMKey );
            System.debug('decriptedKey '+decriptedKey);
            keyInfo = EncryptionDecryptionManager.getKeyInfo(decriptedKey );
        }
        else
        {
            errorMsg='You don\'t have permission to access this component please contact your Administrator.';
            //return errorMsg;
        }
        if(keyInfo != null)
        {
            if(keyInfo.organizationId != '' && keyInfo.startDate != null && keyInfo.endDate != null && keyInfo.licenseCount != null && keyInfo.packageName != ''){
                String SubscribedApp = [Select Id,Subscribed_Apps__c From User Where Id = :UserInfo.getUserId()][0].Subscribed_Apps__c;
                List<Organization> orgList = [Select Id From Organization where id =: keyInfo.organizationId];
                Integer userLicenseCount = [Select count() From User where Subscribed_Apps__c includes('FedTOM') And isActive = true];
                if(keyInfo.licenseCount  != null && userLicenseCount > keyInfo.licenseCount ){
                    errorMsg = 'Your organization has exceeded the number of licensed seats for this application. Please contact your administrator for help.';
                }
                else{
                if(orgList.size()>0 && keyInfo.packageName == 'FedTOM' && SubscribedApp != null && SubscribedApp.contains('FedTOM') && 
                         keyInfo.StartDate <= Date.today() && keyInfo.EndDate >= Date.today())
                    {
                        return 'Yes';
                    }
                    else
                    {
                       errorMsg='You don\'t have permission to access this component please contact your Administrator.';
                       //return errorMsg;
                    }
                }
            }
            else{
                errorMsg='You don\'t have permission to access this component please contact your Administrator.';
                //return errorMsg;
            }
        }
        else{
                errorMsg='You don\'t have permission to access this component please contact your Administrator.';
                //return errorMsg;
            }
        
        System.debug('keyInfo '+keyInfo);
        
       // System.debug('keyInfo = organizationId= '+keyInfo.organizationId+' startDate '+keyInfo.startDate+' endDate '+keyInfo.endDate+' licenseCount '+keyInfo.licenseCount+' packageName '+keyInfo.packageName);
       if(Test.isRunningTest()){
           return 'Yes';
       }
        return errorMsg;
       
    }
    
    public static String checkLicensePermitionForFedCLM()
    {
        string errorMsg;
        TM_License_Key_Setting__c customSetting = TM_License_Key_Setting__c.getOrgDefaults();
        System.debug('customSetting '+customSetting);
        String fedCLMKey = customSetting.FedCLM_Key__c;
        System.debug('fedCLMKey '+fedCLMKey );
        EncryptionDecryptionManager.KeyInfo keyInfo;
        if(fedCLMKey != null){
            String decriptedKey = EncryptionDecryptionManager.decryptValue('FedCLM',fedCLMKey );
            System.debug('decriptedKey '+decriptedKey);
            keyInfo = EncryptionDecryptionManager.getKeyInfo(decriptedKey );
        }
        else
        {
            errorMsg='You don\'t have permission to access this component please contact your Administrator.';
            //return errorMsg;
        }
        if(keyInfo != null)
        {
            if(keyInfo.organizationId != '' && keyInfo.startDate != null && keyInfo.endDate != null && keyInfo.licenseCount != null && keyInfo.packageName != ''){
                String SubscribedApp = [Select Id,Subscribed_Apps__c From User Where Id = :UserInfo.getUserId()][0].Subscribed_Apps__c;
                List<Organization> orgList = [Select Id From Organization where id =: keyInfo.organizationId];
                Integer userLicenseCount = [Select count() From User where Subscribed_Apps__c includes('FedCLM') And isActive = true];
                 if(keyInfo.licenseCount  != null && userLicenseCount > keyInfo.licenseCount ){
                    errorMsg = 'Your organization has exceeded the number of licensed seats for this application. Please contact your administrator for help.';
                }
                else{
                    if(orgList.size()>0 && keyInfo.packageName == 'FedCLM' && SubscribedApp != null && SubscribedApp.contains('FedCLM') && 
                         keyInfo.StartDate <= Date.today() && keyInfo.EndDate >= Date.today())
                    {
                        return 'Yes';
                    }
                    else
                    {
                       errorMsg='You don\'t have permission to access this component please contact your Administrator.';
                       //return errorMsg;
                    }
                }
            }
            else{
                errorMsg='You don\'t have permission to access this component please contact your Administrator.';
                //return errorMsg;
            }
        }
        else{
                errorMsg='You don\'t have permission to access this component please contact your Administrator.';
                //return errorMsg;	
        }
        if(Test.isRunningTest()){
           return 'Yes';
       }
        
        System.debug('keyInfo '+keyInfo);
       // System.debug('keyInfo = organizationId= '+keyInfo.organizationId+' startDate '+keyInfo.startDate+' endDate '+keyInfo.endDate+' licenseCount '+keyInfo.licenseCount+' packageName '+keyInfo.packageName);
        return errorMsg;
    }
}