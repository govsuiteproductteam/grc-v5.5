@isTest
public with sharing class Test_AutomaticResourcesEmailHandler {
    public Static TestMethod void testAutomaticResourcesEmailHandler (){
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Contact con = TestDataGenerator.createContact('TestContact', acc );
        insert con;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('T-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'automaticresources@i-2ccnh767ekmgyioskg4hz5i217j8z763r6ztgn1k4gpi465yio.61-zhsweao.na34.apex.salesforce.com';        
        insert conVehi;    
        
        Messaging.InboundEmail email  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        email.plainTextBody = 'This is test email Body';
        email.fromAddress ='test@test.com';
        email.subject = 'RE: RFQ #D16PS00072';
        email.toaddresses = new List<String>();
        email.toaddresses.add('automaticresources@i-2ccnh767ekmgyioskg4hz5i217j8z763r6ztgn1k4gpi465yio.61-zhsweao.na34.apex.salesforce.com');             
        
        Messaging.InboundEmail email1  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env1 = new Messaging.InboundEnvelope();
        email1.plainTextBody = 'This is test email Body';
        email1.fromAddress ='test@test.com';
        email1.subject = 'Fwd: RFQ #D16PS00072';
        email1.toaddresses = new List<String>();
        email1.toaddresses.add('automaticresources@i-2ccnh767ekmgyioskg4hz5i217j8z763r6ztgn1k4gpi465yio.61-zhsweao.na34.apex.salesforce.com');             
        
        Messaging.InboundEmail email2  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env2 = new Messaging.InboundEnvelope();
        email2.plainTextBody = 'This is test email Body';
        email2.fromAddress ='test1@test.com';
        email2.subject = 'RFQ #D16PS00072';
        email2.toaddresses = new List<String>();
        email2.toaddresses.add('automaticresources@i-2ccnh767ekmgyioskg4hz5i217j8z763r6ztgn1k4gpi465yio.61-zhsweao.na34.apex.salesforce.com');
        
        
        AutomaticResourcesEmailTemplateHandler automaticResourcesEmailHandler = new AutomaticResourcesEmailTemplateHandler();
        
        Test.startTest();
        Messaging.InboundEmailResult result = automaticResourcesEmailHandler.handleInboundEmail(email, env);
        Messaging.InboundEmailResult result1 = automaticResourcesEmailHandler.handleInboundEmail(email1, env1);
        Messaging.InboundEmailResult result2 = automaticResourcesEmailHandler.handleInboundEmail(email2, env2);
        Test.stopTest();
        
        List<Task_Order__c> tOrderList = [SELECT Id FROM Task_Order__c];
        System.assertEquals(tOrderList.size(), 1);
    }
}