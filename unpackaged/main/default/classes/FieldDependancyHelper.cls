public class FieldDependancyHelper{
    public  static Map<String,Map<String,DependantField >> parentMap;
    public static Map<String,String> childmap;
    
    public void process(String recordTypeId,Boolean isContract){
        //System.debug('recordTypeId :- '+recordTypeId);
        List<Document> docList;// = [SELECT Id, Body from document where DeveloperName = 'Dependent_Field_Document_contract'];
        if(isContract){
            if(!Test.isRunningTest())
                docList = [SELECT Id, Body from document where DeveloperName = 'Dependent_Field_Document_contract'];
            else
                docList = [SELECT Id, Body from document where DeveloperName = 'Test_Dependent_Field_Document_contract'];   
        }else{
            if(!Test.isRunningTest())
                docList = [SELECT Id, Body from document where DeveloperName = 'Dependent_Field_Document_subContract'];
            else
                docList = [SELECT Id, Body from document where DeveloperName = 'Test_Dependent_Field_Document_subContract'];
        }
        Map<String,List<DependentFieldSettingCtrl.FieldStructureClass>> fieldStructureListMap;
        if(!docList.isEmpty()){
            Document doc = docList[0];
            if(doc.Body!=Null && doc.Body.toString().trim()!='')
                fieldStructureListMap= (Map<String,List<DependentFieldSettingCtrl.FieldStructureClass>>)JSON.deserialize(doc.Body.toString(), Map<String,List<DependentFieldSettingCtrl.FieldStructureClass>>.class);
        }
        if(fieldStructureListMap==null){
            fieldStructureListMap = new Map<String,List<DependentFieldSettingCtrl.FieldStructureClass>>();
        }
        
        List<DependentFieldSettingCtrl.FieldStructureClass> fieldStructureList = fieldStructureListMap.get(recordTypeId );
        
        if(fieldStructureList== null){
            fieldStructureList = new List<DependentFieldSettingCtrl.FieldStructureClass>();
        }
        
        Integer outerIndex = 1;
        
        //This map will store Parent field agenst Map of value and sting which have class name.
        parentMap = new Map<String,Map<String,DependantField >>();
        childmap = new Map<String,String>();
        for(DependentFieldSettingCtrl.FieldStructureClass fieldStructure : fieldStructureList ){
            //ParentFieldWrapper parentWrapper = new ParentFieldWrapper();
            //parentWrapper.fieldName = parentWrapper.masterFieldApi;
            String fieldName = fieldStructure.masterFieldApi;
            
            //ParentMap Fill
            Map<String,DependantField > valueClassMap = parentMap.get(fieldName.toLowerCase());
            if(valueClassMap == null){
                valueClassMap = new Map<String,DependantField >();
            }
            
            Map<String,List<DependentFieldSettingCtrl.StringWrapper>> valueNdFieldListMap = fieldStructure.valueNdFieldListMap;
            if(valueNdFieldListMap != null){
                Integer innerIndex = 1;
                for(String value : valueNdFieldListMap.keySet()){
                    List<DependentFieldSettingCtrl.StringWrapper> stringWrapperList = valueNdFieldListMap.get(value );
                    
                    
                    if(stringWrapperList != null){
                        Integer k =1;
                        for(DependentFieldSettingCtrl.StringWrapper stringWrap : stringWrapperList){
                            // Child Map Fill
                            String className = fieldName.replaceAll('__c','').replaceAll('__C','').replaceAll('_','-') + '-'+outerIndex+'-'+innerIndex+'-'+k;
                            
                            //ParentMap Fill
                            DependantField dependantField = new DependantField(className ,stringWrap.fieldOrVal,value);
                            valueClassMap.put(className,dependantField );
                            //
                            String fieldClassName = childmap.get(stringWrap.fieldOrVal);
                            if(fieldClassName == null){
                                fieldClassName = className;
                            }
                            else{
                                fieldClassName += ' '+className;
                            }
                            childmap.put(stringWrap.fieldOrVal,fieldClassName);
                            //
                            k++;
                        }
                    }
                    
                    innerIndex++;
                }
            }
            parentMap.put(fieldName.toLowerCase(),valueClassMap);
            outerIndex++;
        }
        
        //System.debug('parentMap '+parentMap);
        
    }
    
    public class DependantField{
        public String className;
        public String apiName;
        public String value;
        
        public DependantField(String className,String apiName,String value){
            this.className = className;
            this.apiName = apiName;
            this.value = value;
        }
    }
    
    /*static{
System.debug('I am in Static');
List<Document> docList = [SELECT Id, Body from document where DeveloperName = 'Dependent_Field_Document'];
List<DependentFieldSettingCtrl.FieldStructureClass> fieldStructureList;
if(!docList.isEmpty()){
Document doc = docList[0];
if(doc.Body!=Null)
fieldStructureList = (List<DependentFieldSettingCtrl.FieldStructureClass>)JSON.deserialize(doc.Body.toString(), List<DependentFieldSettingCtrl.FieldStructureClass>.class);
}
if(fieldStructureList.isEmpty()){
fieldStructureList = new List<DependentFieldSettingCtrl.FieldStructureClass>();
}

Integer outerIndex = 1;

//This map will store Parent field agenst Map of value and sting which have class name.
parentMap = new Map<String,Map<String,String>>();
childmap = new Map<String,String>();
for(DependentFieldSettingCtrl.FieldStructureClass fieldStructure : fieldStructureList ){
//ParentFieldWrapper parentWrapper = new ParentFieldWrapper();
//parentWrapper.fieldName = parentWrapper.masterFieldApi;
String fieldName = fieldStructure.masterFieldApi;

//ParentMap Fill
Map<String,String> valueClassMap = parentMap.get(fieldName);
if(valueClassMap == null){
valueClassMap = new Map<String,String>();
}

Map<String,List<DependentFieldSettingCtrl.StringWrapper>> valueNdFieldListMap = fieldStructure.valueNdFieldListMap;
if(valueNdFieldListMap != null){
Integer innerIndex = 1;
for(String value : valueNdFieldListMap.keySet()){
List<DependentFieldSettingCtrl.StringWrapper> stringWrapperList = valueNdFieldListMap.get(value );
String className = fieldName.replaceAll('__c','').replaceAll('__C','').replaceAll('_','-') + '-'+outerIndex+'-'+innerIndex;

//ParentMap Fill
valueClassMap.put(value , className);
//

if(stringWrapperList != null){
for(DependentFieldSettingCtrl.StringWrapper stringWrap : stringWrapperList){
// Child Map Fill
String fieldClassName = childmap.get(stringWrap.fieldOrVal);
if(fieldClassName == null){
fieldClassName = className;
}
else{
fieldClassName += ' '+className;
}
childmap.put(stringWrap.fieldOrVal,fieldClassName);
//
}
}

innerIndex++;
}
}
parentMap.put(fieldName,valueClassMap);
outerIndex++;
}
}*/
    
    public void display(){
        System.debug('Test 123');
        System.debug('parentMap = '+parentMap);
        System.debug('childmap = '+childmap);
    }
}