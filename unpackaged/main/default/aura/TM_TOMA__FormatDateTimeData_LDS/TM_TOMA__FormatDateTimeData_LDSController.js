({
	doInit : function(component, event, helper) {
        var monthNames = ["JAN", "FEB", "MAR", "APRIL", "MAY", "JUNE",
                              "JULY", "AUG", "SEPT", "OCT", "NOV", "DEC"
                             ];
            
            var actualDate=component.get("v.actualValue");
            console.log("dateFormat"+actualDate);
            if(actualDate != null && actualDate !='null' && actualDate !=''){
                var dateFormat=actualDate.split("-");
                
                    if(component.get("v.fldType")=="DATE"){
                        if(dateFormat != null && typeof dateFormat != undefined){
                        var actualday = dateFormat[2] != null && dateFormat[2]!='' ? dateFormat[2].split(' '):dateFormat[2];
                    component.set("v.dateDay",actualday[0]);
                    
                     
                    component.set("v.dateTime",actualday.length>1 ? actualday[1]:'');
                    component.set("v.dateYear",dateFormat[0]);
                    component.set("v.dateMonth",monthNames[parseInt(dateFormat[1].replace(/^0+/, ''))-1]);
                        }
                    }else{
                        var timezone = $A.get("$Locale.timezone");
                        //toISOString()
                        actualDate = ''+JSON.parse(actualDate);
                var dateTemp = new Date(actualDate).toLocaleString("en-US", {timeZone: timezone})   //2018-09-06T12:17:47.000Z dateTemp
                console.log("dateTemp"+dateTemp);
                 var date = new Date(dateTemp);
                var hours = date.getHours();
                var minutes = date.getMinutes();
                var ampm = hours >= 12 ? ' PM' : ' AM';
                hours = hours % 12;
                hours = hours ? hours : 12; // the hour '0' should be '12'
                minutes = minutes < 10 ? '0'+minutes : minutes;
                var strTime = hours + ':' + minutes + ' ' + ampm;
                //alert(date.getMonth()+1 + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime);
                console.log("TIMMMMEEE-"+strTime);
               // var dateFormat=actualDate.split("-");
                
                    component.set("v.dateDay",date.getDate());
                    component.set("v.dateYear",date.getFullYear());
                    component.set("v.dateMonth",monthNames[date.getMonth()]);
                    component.set("v.dateTime",strTime)
                        
                        
                        /*var timezone = $A.get("$Locale.timezone");
                        var actualTDateTime = new Date(actualDate).toLocaleTimeString("en-US", {timeZone: timezone});
                        console.log("actualTDateTime"+actualTDateTime);
                        var tempDateTime=$A.localizationService.formatDateTime(actualDate);
                        console.log("tempDateTime"+tempDateTime);*/
                    }
                    
                    
                
            }
        //component.set("v.dataLoaded",true);
	}
})