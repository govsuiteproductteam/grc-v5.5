public with sharing class GSAHTMLParserNew {

    public static String parse(String htmlBody){
        System.debug('htmlBody TR count = '+htmlBody.length());
        System.debug('***htmlBody = '+htmlBody);
        String urls='';
        String contractNumber = '';
        /*List<String> rowByTrSplits = htmlBody.split('<tr>');
        System.debug('***rowByTrSplits = '+rowByTrSplits);
        
        for(String rowByTr : rowByTrSplits){*/
        
        //8 May, 2018
        //Above salesorce split(regex) method cannot handle more than 10 lack character 
        //As the String class Split method uses regex as a input paramenter and Salesforce limitations is that, it can not deal with more than 10 lack 
        //character of string when we talk abt regex matcher. So to overcome that issue we have written a class "UtilityRowIterator" which will work like 
        //split method and give us the desired output
        //Below is the given code for it
        
        UtilityRowIterator r = New UtilityRowIterator(htmlBody,'<tr>');

        while(r.hasNext()) {
            String rowByTr = r.next();
            //System.debug('rowByTr = '+rowByTr);
            String sin = '';
             UtilityRowIterator splitterForNewLine = New UtilityRowIterator(rowByTr,'\n');
            while(splitterForNewLine.hasNext()) {
                String row = splitterForNewLine.next();
                boolean isContainSins = row.contains('/advantage/ebuy/seller/category_description.do?');
                if(isContainSins){
                    sin = row.stripHtmlTags().trim();
                }
            /*String[] rows = rowByTr.split('\n');
            for(String row: rows){
                boolean isContainSins = row.contains('/advantage/ebuy/seller/category_description.do?');
                if(isContainSins){
                    sin = row.stripHtmlTags().trim();
                }*/
                //boolean isContains = row.contains('/advantage/ebuy/seller/open_rfq.do');
                boolean isContains = row.contains('/advantage/ebuy/seller/open_rfq.do');
                boolean isContains1 = row.contains('/advantage/ebuy/seller/RFQ_cancel_reason.do');
                //boolean isContains2 = row.contains('RFQ1208002');
                //boolean isContains2 = row.contains('RFQ1076602');
                //boolean isContains3 = row.contains('RFQ1081679');
                //boolean isContains4 = row.contains('RFQ1082029');
                //boolean isContains5 = row.contains('RFQ1084588');
                if((isContains || isContains1 ) ){
                    row = row.replaceAll('&amp;', '&');
                    System.debug('Row === '+row);
                    urls += row.substringBetween('href="', '">').trim()+'&sins='+sin+'&contractNumber='+contractNumber+'SPLITURL'.trim();                
                }
                else{
                    row = row.stripHtmlTags();
                    
                    if(row.contains('Contract Number:')){
                        contractNumber = row.substringAfterLast('Contract Number:').trim();
                    }
                }
            }
        }   
        system.debug('urls = \n'+urls);
        return urls;
    }
}