@isTest
public class SendPartnerEmailCntrl_TEST {
    public static testMethod void testSendPartnerEmailCntrl(){
        List<RecordType> rtypes = [Select Name, Id From RecordType where sObjectType='TM_GovSuite__FedCap_Opportunity__c' and isActive=true];
        string recId='';
        if(rtypes != null && ! rtypes.isEmpty())
        {
            recId=rtypes[0].Id;
        }
        Account acc = TestDataGenerator.createAccount('testAccount');
        insert acc;
        Contact con = new Contact();
        con.LastName='test';
        con.AccountId=acc.Id;
        insert con;
       Contract_Vehicle__c contract = TestDataGenerator.createContractVehicle('999',acc);
       insert contract;
       
       Task_Order__c fedOpp = TestDataGenerator.createTaskOrder('TMtaskOrder', acc, contract);
            insert fedOpp;
       
        /*FedCap_Opportunity__c fedOpp = TestDataGenerator.createFedOpp('TestFedOpp', acc, recId, 'Prospecting');
        //fedOpp.Probability__c=10;
        fedOpp.Budget_Approved__c=true;
        fedOpp.Type__c= 'New Business';
        fedOpp.Closed_Reason__c='Test';
        fedOpp.Marketing_Budget__c=8787;
       fedOpp.Contract_Vehicle_owned__c =contract.Id; 
        insert fedOpp;*/
        Vehicle_Partner__c vehPartnr = TestDataGenerator.createVehiclePartner(acc, contract);
        insert vehPartnr;
        Teaming_Partner__c teamPart = TestDataGenerator.createTeamingPartner(vehPartnr,fedOpp);
        teamPart.Status__c='New';
        insert teamPart;
        Task_Order_Contact__c taskCon = TestDataGenerator.createTaskOrderContact(fedOpp,con);
        insert taskCon;
        Survey__c survey  = new Survey__c();
        insert survey; 
        Attachment attach = TestDataGenerator.createAttachment(fedOpp.Id);
        
        insert attach;
        List<EmailTemplate> eTemplateList = [SELECT Id from EmailTemplate LIMIT 1];
        String emailTempId='';
        if(eTemplateList != null && ! eTemplateList.isEmpty())
         emailTempId =  eTemplateList[0].Id;
        ParentWrapper partnerwrp = SendPartnerEmailCntrl.getParentWraList(fedOpp.Id);
        system.assertNotEquals(null,partnerwrp);
        //when no contact is selected and no attahment is selected
        SendPartnerEmailCntrl.sendEmail(fedOpp.Id, False, survey.Id,JSON.serialize(partnerwrp.conWrapList) , JSON.serialize(partnerwrp.attachWrapList), emailTempId, 'subjectToSet', 'emailBodyToSet');
        
        //selecting one contact and one attachment.
        if(partnerwrp != null){
            List<ContactWrapper> conWrapList = partnerwrp.conWrapList;
            List<AttachmentWrapper> attachWrapList = partnerwrp.attachWrapList;
            if(conWrapList != null && ! conWrapList.isEmpty())
                conWrapList[0].isSelected = true;
            if(attachWrapList != null && ! attachWrapList.isEmpty()){
                attachWrapList[0].isSelected = true;
                 //attachWrapList[0].isFile = true;
            }
               
            //SendPartnerEmailCntrl.sendEmail(fedOpp.Id, False, survey.Id,JSON.serialize(conWrapList) , JSON.serialize(attachWrapList), emailTempId, 'subjectToSet', 'emailBodyToSet');
            SendPartnerEmailCntrl.testFunc(JSON.serialize(conWrapList), JSON.serialize(attachWrapList), fedOpp.Id, null, False, survey.Id, 'subjectToSet', 'emailBodyToSet');
        }
        AttachmentWrapper attachment = new AttachmentWrapper(fedOpp.Id,'test Attach',5);//
        
        
    }
}