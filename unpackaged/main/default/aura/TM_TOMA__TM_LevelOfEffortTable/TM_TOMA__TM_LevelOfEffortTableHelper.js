({
    getContractVehicle1 : function(component, event, helper) {
        
        var action = component.get("c.getContractVehical"); // from apex 
        action.setParams({
            recordId: component.get("v.recordId") //controller defined id set here.
        });
        action.setCallback(this, function(a) {
            var state= a.getState();
            if(state=="SUCCESS"){
                component.set("v.contract", a.getReturnValue());
            }
            else if (state == "ERROR") 
            {
                console.log(a.getError());            
            }
        })
        $A.enqueueAction(action); 
    },
    conValLevelOfEffortTableList : function(component, event, helper) {
        var action = component.get("c.getValueTableLevelEffortList");
        action.setParams({          
            "contractVehicalId": component.get("v.recordId")
        });
        action.setCallback(this, function(a) {
            var conValLevelEffortTableList = a.getReturnValue();
            if(conValLevelEffortTableList == null){ 
                component.set("v.valueTableLevelEffortSizeFlag",0);
            }
            else{
                component.set("v.valueTableLevelEffortSizeFlag",conValLevelEffortTableList.length);
                //17-08-2018 issue fixed for Start_Date__c is undefined  
                for(var i = 0; i < conValLevelEffortTableList.length; i++){
                    console.log('Start_Date__c@@@@@@@@@' + conValLevelEffortTableList[i].TM_TOMA__Start_Date__c);
                    console.log('End_Date__c@@@@@@@@@' + conValLevelEffortTableList[i].TM_TOMA__End_Date__c);
                    if(conValLevelEffortTableList[i].TM_TOMA__Start_Date__c == undefined){
                        //valueTableList[i].Start_Date__c = 
                        conValLevelEffortTableList[i].TM_TOMA__Start_Date__c='';
                    }
                    if(conValLevelEffortTableList[i].TM_TOMA__End_Date__c == undefined){
                        conValLevelEffortTableList[i].TM_TOMA__End_Date__c='';
                    }
                }
                component.set("v.valueTableLevelList", conValLevelEffortTableList);
                //component.set("v.valueTableLevelList", a.getReturnValue());
            } 
        })
        $A.enqueueAction(action); 
    },
    saveLevelOfEffortTableList : function (component, event, helper){
        //alert('saveLevelOfEffortTableList');
        document.getElementById("saveButtonLevel").disabled = true;
        document.getElementById("saveButtonLevel1").disabled = true;
        var invalidStartDateRow='';
        var invalidEndDateRow='';
        var delLevelEffortId =component.get("v.delLevelEffortId");
        var valueTableLevelList =component.get("v.valueTableLevelList");
        var valueTableJSON = '[';
        var checkValidationError='Yes';
        var checkStartDateError = 'Yes';
        var checkEndDateError = 'Yes';
        if(valueTableLevelList != undefined){
            for (var i=0; i<valueTableLevelList.length; i++){        
                if(valueTableJSON.length>1){
                    valueTableJSON +=  ',{"attributes":{"type":"TM_TOMA__Value_Table__c"},';
                    var vTId = valueTableLevelList[i].Id;
                    var startDate = valueTableLevelList[i].TM_TOMA__Start_Date__c;
                    var endDate = valueTableLevelList[i].TM_TOMA__End_Date__c;
                    var plusMinus = valueTableLevelList[i].TM_TOMA__Plus_Minus__c === undefined?null:valueTableLevelList[i].TM_TOMA__Plus_Minus__c;//'Test';//
                    var maximumLOE = valueTableLevelList[i].TM_TOMA__Maximum_LOE__c === undefined?null:valueTableLevelList[i].TM_TOMA__Maximum_LOE__c;//'Test';//
                    var minimumLOE = valueTableLevelList[i].TM_TOMA__Minimum_LOE__c === undefined?null:valueTableLevelList[i].TM_TOMA__Minimum_LOE__c;//'Test';//
                    var targetLOE = valueTableLevelList[i].TM_TOMA__Target_LOE__c === undefined?null:valueTableLevelList[i].TM_TOMA__Target_LOE__c;
                    var RecordTypeId  = valueTableLevelList[i].RecordTypeId ;
                    var Contract_Vehicle__c  = valueTableLevelList[i].TM_TOMA__Contract_Vehicle__c ;
                    var appendData = '';
                    //Code to check a date is invalid or not  
                    var testmsg =helper.validatedate(component,event,helper,startDate);
                    var testmsg1 =helper.validatedate(component,event,helper,endDate);   
                    if(testmsg===false)
                    {
                        checkStartDateError='InvalidStartDate';
                        invalidStartDateRow=invalidStartDateRow+''+i;
                        invalidStartDateRow=invalidStartDateRow+',';
                    }
                    if(testmsg1===false)
                    {
                        checkEndDateError='InvalidEndDate';
                        invalidEndDateRow=invalidEndDateRow+''+i;
                        invalidEndDateRow=invalidEndDateRow+',';
                    }
                    if(startDate==='')
                    {
                        checkValidationError='startDate';
                    }
                    if(endDate==='')
                    {
                        checkValidationError='endDate';
                    }
                    if(!(vTId===undefined))
                    {
                        appendData +='"Id":"'+ vTId+'" ,';
                    }
                    appendData += ' "TM_TOMA__Start_Date__c":"'+startDate+'", "TM_TOMA__End_Date__c":"'+endDate+'", "TM_TOMA__Plus_Minus__c":'+plusMinus+',"TM_TOMA__Maximum_LOE__c":'+maximumLOE+',"TM_TOMA__Minimum_LOE__c":'+minimumLOE+',"TM_TOMA__Target_LOE__c":'+targetLOE+',"RecordTypeId":"'+RecordTypeId+'" }';
                    valueTableJSON += appendData;
                }else{
                    valueTableJSON +=  '{"attributes":{"type":"TM_TOMA__Value_Table__c"},';
                    var vTId = valueTableLevelList[i].Id;
                    var startDate = valueTableLevelList[i].TM_TOMA__Start_Date__c;
                    var endDate = valueTableLevelList[i].TM_TOMA__End_Date__c;
                    var plusMinus = valueTableLevelList[i].TM_TOMA__Plus_Minus__c === undefined?null:valueTableLevelList[i].TM_TOMA__Plus_Minus__c;
                    var maximumLOE = valueTableLevelList[i].TM_TOMA__Maximum_LOE__c === undefined?null:valueTableLevelList[i].TM_TOMA__Maximum_LOE__c;
                    var minimumLOE = valueTableLevelList[i].TM_TOMA__Minimum_LOE__c === undefined?null:valueTableLevelList[i].TM_TOMA__Minimum_LOE__c;
                    var targetLOE = valueTableLevelList[i].TM_TOMA__Target_LOE__c === undefined?null:valueTableLevelList[i].TM_TOMA__Target_LOE__c;
                    var RecordTypeId  = valueTableLevelList[i].RecordTypeId ;
                    var Contract_Vehicle__c  = valueTableLevelList[i].TM_TOMA__Contract_Vehicle__c ;
                    var appendData = '';
                    //Code to check a date is invalid or not          
                    var testmsg =helper.validatedate(component,event,helper,startDate);
                    var testmsg1 =helper.validatedate(component,event,helper,endDate);
                    if(testmsg===false)
                    {
                        checkStartDateError='InvalidStartDate';
                        invalidStartDateRow=invalidStartDateRow+''+i;
                        invalidStartDateRow=invalidStartDateRow+',';
                    }
                    if(testmsg1===false)
                    {
                        checkEndDateError='InvalidEndDate';
                        invalidEndDateRow=invalidEndDateRow+''+i;
                        invalidEndDateRow=invalidEndDateRow+',';
                    }
                    if(startDate==='')
                    {
                        checkValidationError='startDate';
                    }
                    if(endDate==='')
                    {
                        checkValidationError='endDate';
                    }
                    if(!(vTId===undefined))
                    {
                        appendData +='"Id":"'+ vTId+'" ,';
                    }
                    appendData += ' "TM_TOMA__Start_Date__c":"'+startDate+'", "TM_TOMA__End_Date__c":"'+endDate+'", "TM_TOMA__Plus_Minus__c":'+plusMinus+',"TM_TOMA__Maximum_LOE__c":'+maximumLOE+',"TM_TOMA__Minimum_LOE__c":'+minimumLOE+',"TM_TOMA__Target_LOE__c":'+targetLOE+',"RecordTypeId":"'+RecordTypeId+'" }';
                    valueTableJSON += appendData;
                }
            } 
            valueTableJSON += ']'; 
            if(checkValidationError==='Yes' && checkStartDateError==='Yes' && checkEndDateError==='Yes'){
                var action = component.get("c.saveConValLevelEffortTable");
                action.setParams({ 
                    "contValList": valueTableJSON,
                    "recordId": component.get("v.recordId"),
                    "delConValIdsStr": delLevelEffortId
                });
                action.setCallback(this, function(response){
                    var state = response.getState();
                    var messageText = response.getReturnValue();
                    if (state === "SUCCESS")
                    {
                        component.set("v.delLevelEffortId",'');
                        helper.conValLevelOfEffortTableList(component, event, helper); // Defined in helper
                        /* var act3 = component.get("c.isLightningPage");
                    act3.setCallback(this, function(a) {
                        if (a.getState() === "SUCCESS") {
                            if(a.getReturnValue() == false){
                                alert("Data save successfully.");
                            }
                            else{ 
                                var toastEvent = $A.get("e.force:showToast");
                                toastEvent.setParams({
                                    "title": "Success!",
                                    "message": "Data save successfully.",
                                    "type":"success"
                                });
                                toastEvent.fire();
                            }
                        }
                    }); 
                    $A.enqueueAction(act3);*/
                        
                        // "If" condition and "Else" block are newly added to restrict user access according to the object permission(17/08/2018)
                        if(messageText == '' || messageText == undefined || messageText == null){
                            alert("Changes were saved successfully.");
                        }else{
                            alert(messageText);
                        }
                    }
                    else if (state === "ERROR") {
                        console.log(response.getError());            }
                    document.getElementById("saveButtonLevel").disabled = false;
                    document.getElementById("saveButtonLevel1").disabled = false;
                });
                $A.enqueueAction(action);
            }
            
            else if(checkStartDateError==='InvalidStartDate' && checkEndDateError==='Yes' && checkValidationError==='Yes')
            {
                var checkId = invalidStartDateRow.split(',');
                for(var i = 0; i < checkId.length-1; i++) {
                    var d = document.getElementById(checkId[i]+'levelStartDate');
                    d.className += " validationClass";
                }
                alert('ERROR:- Invalid Start Date, in row:- '+invalidStartDateRow);
                /*   var act3 = component.get("c.isLightningPage");
            act3.setCallback(this, function(a) {
                if (a.getState() === "SUCCESS") {
                    if(a.getReturnValue() == false){
                        alert('ERROR:- Invalid Start Date, in row:- '+invalidStartDateRow);
                    }
                    else{ 
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!",
                            "message": 'ERROR:- Invalid Start Date, in row:- '+invalidStartDateRow,
                            "type":"error"
                        });
                        toastEvent.fire();
                    }
                }
            }); 
            $A.enqueueAction(act3);*/
                document.getElementById("saveButtonLevel").disabled = false; 
                document.getElementById("saveButtonLevel1").disabled = false;
            }
                else if(checkEndDateError==='InvalidEndDate' && checkStartDateError==='Yes' && checkValidationError==='Yes')
                {
                    var checkId = invalidEndDateRow.split(',');
                    for(var i = 0; i < checkId.length-1; i++) {
                        var d = document.getElementById(checkId[i]+'levelEndDate');
                        d.className += " validationClass";
                    }
                    alert('ERROR:- Invalid End Date, in row:- '+invalidEndDateRow);
                    /*var act3 = component.get("c.isLightningPage");
                     act3.setCallback(this, function(a) {
                if (a.getState() === "SUCCESS") {
                    if(a.getReturnValue() == false){
                    }
                    else{ 
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!",
                            "message": Invalid End Date, in row:- '+invalidEndDateRow,
                            "type":"error"
                        });
                        toastEvent.fire();
                    }
                }
            }); 
            $A.enqueueAction(act3);*/
                document.getElementById("saveButtonLevel").disabled = false;
                document.getElementById("saveButtonLevel1").disabled = false;
            }
                else if(checkEndDateError==='InvalidEndDate' && checkStartDateError==='InvalidStartDate' && checkValidationError==='Yes')
                {
                    var checkId = invalidStartDateRow.split(',');
                    for(var index = 0; index < checkId.length-1; index++) {
                        var d = document.getElementById(checkId[index]+'levelStartDate');
                        d.className += " validationClass";
                    }
                    var checkId1 = invalidEndDateRow.split(',');
                    for(var jindex = 0; jindex < checkId1.length-1; jindex++) {
                        var d = document.getElementById(checkId1[jindex]+'levelEndDate');
                        d.className += " validationClass";
                    }
                    alert('ERROR:- Invalid Start Date, in row:- '+invalidStartDateRow+'\nERROR:- Invalid End Date, in row:- '+invalidEndDateRow);
                    /*var act3 = component.get("c.isLightningPage");
                     act3.setCallback(this, function(a) {
                    if (a.getState() === "SUCCESS") {
                        if(a.getReturnValue() == false){
                            alert('ERROR:- Invalid Start Date, in row:- '+invalidStartDateRow+'\nERROR:- Invalid End Date, in row:- '+invalidEndDateRow);
                        }
                        else{ 
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                "title": "Error!",
                                "message": "Invalid Start Date and End Date .",
                                "type":"error"
                            });
                            toastEvent.fire();
                        }
                    }
                }); 
                $A.enqueueAction(act3);*/
                document.getElementById("saveButtonLevel").disabled = false;
                document.getElementById("saveButtonLevel1").disabled = false;
            }
                else{
                    alert("ERROR:- Please enter a Start Date or End Date.");
                    /*var act3 = component.get("c.isLightningPage");
                    act3.setCallback(this, function(a) {
                        if (a.getState() === "SUCCESS") {
                            if(a.getReturnValue() == false){
                                alert("ERROR:- Enter date for Start Date or End Date.");
                            }
                            else{ 
                                var toastEvent = $A.get("e.force:showToast");
                                toastEvent.setParams({
                                    "title": "Error!",
                                    "message": "Enter date for Start Date or End Date.",
                                    "type":"error"
                                });
                                toastEvent.fire();
                            }
                        }
                    }); 
                    $A.enqueueAction(act3);*/
                // alert("Enter date for START DATE or END DATE.");
                document.getElementById("saveButtonLevel").disabled = false;
                document.getElementById("saveButtonLevel1").disabled = false;
                // btn.set("v.disabled",false);
            }
        }
        else{
            document.getElementById("saveButtonLevel").disabled = false;
            document.getElementById("saveButtonLevel1").disabled = false;
        }
    },
    validatedate:function(component,event,helper,inputText)
    {
        var dateformat = /(\d{4})-(\d{2})-(\d{2})/;
        // Match the date format through regular expression
        if(inputText.match(dateformat))
        {
            //Test which seperator is used '/' or '-'
            var opera1 = inputText.split('/');
            var opera2 = inputText.split('-');
            var lopera1 = opera1.length;
            var lopera2 = opera2.length;
            // Extract the string into month, date and year
            if (lopera1>1)
            {
                var pdate = inputText.split('/');
            }
            else if (lopera2>1)
            {
                var pdate = inputText.split('-');
            }
            var mm  = parseInt(pdate[1]);
            var dd = parseInt(pdate[2]);
            var yy = parseInt(pdate[0]);
            // Create list of days of a month [assume there is no leap year by default]
            var ListofDays = [31,28,31,30,31,30,31,31,30,31,30,31];
            if (mm==1 || mm>2)
            {
                if (dd>ListofDays[mm-1])
                {
                    return false;
                }
            }
            if (mm==2)
            {
                var lyear = false;
                if ( (!(yy % 4) && yy % 100) || !(yy % 400)) 
                {
                    lyear = true;
                }
                if ((lyear==false) && (dd>=29))
                {
                    return false;
                }
                if ((lyear==true) && (dd>29))
                {
                    return false;
                }
            }
        }
        else
        {
            return false;
        }
    },
    checkValueTablePermissions : function(component, event, helper) {
        //Check "Value Table" object Permissions
        var action = component.get('c.getValueTablePermissions');
        action.setCallback(this,function(response){
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.leveloffeffortTableCtrl', response.getReturnValue());
                var c=response.getReturnValue();
                if(c.isUpdateValueTable===false && c.isCreateValueTable===false && c.isDeleteValueTable===false ){
                    component.set("v.savePermission",false);
                }
            }
        });
        $A.enqueueAction(action);

    }
})