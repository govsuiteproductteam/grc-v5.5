public with sharing class AddCustomerContactCompoController {
    
   @AuraEnabled
    public static String checkLicensePermition()
    {	
        return LincenseKeyHelper.checkLicensePermitionForFedCLM();//updated on 01-02
    }
    
    @AuraEnabled
    public static List<Customer_Contacts__c> getVehicalPatner(Id contractVehicalId)
    {
        if(Schema.sObjectType.Customer_Contacts__c.isAccessible()){
        List<Customer_Contacts__c> customerContactList = new  List<Customer_Contacts__c>();
        if(contractVehicalId!=Null)
        {
            customerContactList = [select Id,Name,Contract_Vehicle__c,Name__c,Name__r.Name,Role__c from Customer_Contacts__c where Contract_Vehicle__c =: contractVehicalId ORDER BY Name__r.Name ASC Limit 1000];
        }
        
        return customerContactList;
        }
        else{
            return null;
        }
    }
  
    
    @AuraEnabled
    public static Map<String,String> getRoleMap()
    {
        if(Schema.sObjectType.Customer_Contacts__c.isAccessible()){
        Map<String,String> optionsMap=new Map<String,String>();
        Schema.DescribeFieldResult feildResult = Customer_Contacts__c.Role__c.getDescribe();
        LIST<Schema.PicklistEntry> ple = feildResult.getPicklistValues();
        for(Schema.PicklistEntry f : ple)
        {
            optionsMap.put(f.getLabel(), f.getValue());
        }
        return optionsMap;
        }
        else{
            return null;
        }
    }
    
    @AuraEnabled
    public static Customer_Contacts__c addNewVehicalPartner(Id contractVehicalId)
    {
        id tempOppId;
        if(Schema.sObjectType.Customer_Contacts__c.isAccessible() && Schema.sObjectType.Contact.isAccessible()){
        list<contact> oaccList=[Select Id from contact limit 1];
        if(oaccList.size()>0){
            tempOppId = oaccList[0].Id;
        }
        else{
            tempOppId=null;
        }
            if(Schema.sObjectType.Customer_Contacts__c.isCreateable()){
        Customer_Contacts__c newCustomerCon = new Customer_Contacts__c (Contract_Vehicle__c = contractVehicalId,Role__c='',Name__c=tempOppId);
        return newCustomerCon; 
            }
            else{
                return null;
            }
        }
        else{
            return null;
        }
        
    }
    
      @AuraEnabled
    public static void deleteCustomerContact(string delId){
        if(delId!=Null)
        {
            List<Customer_Contacts__c> delList = [Select Id from Customer_Contacts__c where Id =: delId];
            try{
                DMLManager.deleteAsUser(delList);
            }
            
            catch(Exception e)
            {
                System.debug(' Error :-  '+e.getLineNumber()+' '+e.getMessage());
            }
            
        }
    }
    
    @AuraEnabled
    public static void saveCustomerContact(string customerContactstrList)
    {
      
    List<Customer_Contacts__c> customerContactList = new List<Customer_Contacts__c>();
    if(customerContactstrList != null && customerContactstrList.length()>1)
    {
        customerContactstrList =customerContactstrList.substring(1);
        List<String> vehicalPatnerstrList = customerContactstrList.split('SPLITPARENT');
        if(vehicalPatnerstrList.size() > 0)
        {
            for(String temp : vehicalPatnerstrList){
                Customer_Contacts__c customerCon = new Customer_Contacts__c();
                customerCon = getVehicalPatner(temp);
                customerContactList.add(customerCon);
            }
        }         
        
        if(!customerContactList.isEmpty()){
            try{
                DMLManager.upsertAsUser(customerContactList);
            }catch(Exception e){
                System.debug('Exception '+e);
            }
        }
    }
    
}

private static Customer_Contacts__c getVehicalPatner(String customerConStr){
    if(customerConStr != null && customerConStr.trim().length() > 0){
        String[] customerConStrList = customerConStr.split('SPLITDATA');
        if(customerConStrList.size() > 0){
            if(Schema.sObjectType.Customer_Contacts__c.isCreateable()  ){
            Customer_Contacts__c cusCon = new Customer_Contacts__c();
            if(customerConStrList[0] != 'undefined'){
                cusCon.id = customerConStrList[0];
            } 
            if(cusCon.id == null && customerConStrList.size() > 1){
                cusCon.Contract_Vehicle__c = customerConStrList[1];
            }
            if(cusCon.id == null && customerConStrList.size() > 2){
                cusCon.Name__c = customerConStrList[2];
            }
            if(customerConStrList.size() > 3){
                cusCon.Role__c = customerConStrList[3];
            }
            return cusCon;
            }
        }
    }
    return null;
}
}