public class SendPartnerEmailCntrl {
    
    @AuraEnabled
    public static String getUserAccesibility()
    {
        return LincenseKeyHelper.checkLicensePermitionForFedTOM();
    }
    
    @AuraEnabled 
    public static parentWrapper getParentWraList(String recordId){
        List<ContactWrapper> conWraList = new List<ContactWrapper>();
        List<Task_Order_Contact__c> tocList = [SELECT Contact__c, Contact__r.Name, Contact__r.Email, Contact__r.Account.Name,Contact__r.AccountId FROM Task_Order_Contact__c WHERE Task_Order__c= :recordId];
        if(!tocList.isEmpty()){
            for(Task_Order_Contact__c toc : tocList){
                conWraList.add(new ContactWrapper(toc));
            }
        }
        
        List<SurveyWrapper> surveyWraList = new List<SurveyWrapper>();
        for(Survey__c surv : [select id,Name,Description__c,No_of_Active_Questions__c, isActive__c from Survey__c WHERE isActive__c=TRUE limit 1000]){
            surveyWraList.add(new SurveyWrapper(surv));
        }
        
        List<AttachmentWrapper> attachmentWraList = new List<AttachmentWrapper>();
        
        Task_Order__c[] taskOrderList = [select id,(select id,name,BodyLength from Attachments)  from Task_Order__c WHERE Id=:recordId];
        if(taskOrderList != null && !taskOrderList.isEmpty()){
            System.debug('taskOrderList ='+taskOrderList);
            if(taskOrderList.get(0).Attachments!=null){
                List<Attachment> attachmentList=taskOrderList.get(0).Attachments;
                System.debug('attachmentList ='+attachmentList);
                if(attachmentList!= null && !attachmentList.isEmpty()){
                    attachmentWraList = new List<AttachmentWrapper>();
                    for(Attachment attachment : attachmentList){
                        AttachmentWrapper attachmentWrapper = new AttachmentWrapper(attachment.Id, attachment.Name,attachment.BodyLength,false);
                        attachmentWraList.add(attachmentWrapper);
                    }
                }
            }
        }
        
        List<ContentDocumentLink> contentDocLinkList = new List<ContentDocumentLink>();
        contentDocLinkList=[SELECT Id, ContentDocumentId, ContentDocument.LatestPublishedVersionId, ContentDocument.Title,ContentDocument.ContentSize FROM ContentDocumentLink WHERE LinkedEntityId =:recordId AND ContentDocumentId != null AND ContentDocument.LatestPublishedVersionId != null];
        if(contentDocLinkList != null && ! contentDocLinkList.isEmpty()){
            for(ContentDocumentLink contentLink :contentDocLinkList){
                AttachmentWrapper attachmentWrapper = new AttachmentWrapper(contentLink.ContentDocument.LatestPublishedVersionId, contentLink.ContentDocument.Title,contentLink.ContentDocument.ContentSize,true);
                attachmentWraList.add(attachmentWrapper);
            }
        }
        
        ParentWrapper parentWraper = new ParentWrapper(conWraList,surveyWraList,selectEmailTemplete(''),attachmentWraList);
        return parentWraper;
    }
    
    @AuraEnabled
    public static void testFunc(String str1,String str2,String str3,String str4,Boolean bool5,String str6,String str7,String str8){
        system.debug('conWraListString--'+str1);
        sendEmail(str3,bool5,str6,str1,str2,str4,str7,str8);
    }
    
    @AuraEnabled
    public static void sendEmail(String taskOrderId,Boolean avoidSurvey,String surveyStrId,String conWraListString,String attachmentWraStrng,String emailTempId,String subjectToSet,String emailBodyToSet){
        system.debug('Holaaaaaaaaaaaaa');
        system.debug('conWraListString='+conWraListString);
        List<Task_Order_Questionnaire__c> communityUrlList;
        List<ContactWrapper> conWraList = (List<ContactWrapper>) System.JSON.deserialize(conWraListString, List<ContactWrapper>.class);
        system.debug('conWraList='+conWraList);
        List<AttachmentWrapper> attachmentWraList = (List<AttachmentWrapper>) System.JSON.deserialize(attachmentWraStrng, List<AttachmentWrapper>.class);
        string msg;
        Task_Order__c taskOrder = [Select Id,Contract_Vehicle__c,Contract_Number__c,Contract_Vehicle__r.Name,Primary_Requirement__c,Contract_Type__c From Task_Order__c where id =: taskOrderId];
        Survey_task__c surveyTask = new Survey_Task__c();
        surveyTask.Task_Order__c = taskOrder.id;
        surveyTask.Contract_Vehicle__c = taskOrder.Contract_Vehicle__r.Name;
        surveyTask.Primary_Requirement__c = taskOrder.Primary_Requirement__c;
        surveyTask.Task_Order_Contract_Type__c = taskOrder.Contract_Type__c;
        //surveyTask.Task_Order_Number__c = taskOrder.Task_Order_Number__c;
        surveyTask.Contract_Number__c = taskOrder.Contract_Number__c ;
        
        Survey__c survey;
        if(!avoidSurvey && surveyStrId!=Null && surveyStrId!=''){               
            surveyTask.Survey__c = (ID)surveyStrId;
            
            ///// insert surveyTask;
            DMLManager.insertAsUser(surveyTask);
            survey = [Select Id,Name , isActive__c from Survey__c where id =: surveyStrId];
        }
        
        Set<Id> ContactIdSet = new Set<Id>();
        List<User> userList = new List<User>();
        Set<Id> accountIdSet = new Set<Id>();
        
        for(ContactWrapper conWrp : conWraList ){
            system.debug('conWrp'+conWrp);
            if(conWrp.isSelected){
                ContactIdSet.add(conWrp.con.Contact__c);
                accountIdSet.add(conWrp.con.Contact__r.AccountId);
            }
            
        }
        
        updateTeamingpartnerStatus(accountIdSet,taskOrderId);
        communityUrlList = [SELECT id, Customer_Community_URL__c, Partner_Community_URL__c FROM Task_Order_Questionnaire__c LIMIT 1];
        
        List<Messaging.singleEmailMessage> mails = new List<Messaging.singleEmailMessage>();
        
        List<Task> taskList = new List<Task>();
        try{
            msg = ''; 
            for(ContactWrapper conWrp : conWraList ){
                if(conWrp.isSelected){
                    mails.add(getSingleEmail(surveyTask.Id,conWrp.con.Contact__c,emailTempId,subjectToSet,emailBodyToSet,avoidSurvey,surveyStrId,taskOrderId));
                    if(!avoidSurvey && surveyStrId!=Null && surveyStrId!=''){ 
                        taskList.add(getTask(survey,surveyTask.Id,null,conWrp.con.Contact__c,taskOrderId,communityUrlList));
                    }
                    
                }
            }
            
            if(attachmentWraList!=null && !attachmentWraList.isEmpty()){
                System.debug(''+attachmentWraList);
                List<Id> attachmentIdList = new List<Id>();
                List<Id> filesIdList = new List<Id>();
                for(AttachmentWrapper wrapper : attachmentWraList){
                    if(wrapper.isSelected){
                        if(! wrapper.isFile){
                            attachmentIdList.add(wrapper.attachmentId);
                        }else{
                            filesIdList.add(wrapper.attachmentId);
                        }
                        
                    }
                    
                }
                System.debug('filesIdList'+filesIdList);
                List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
                for (Attachment attachment : [select Name, Body, BodyLength from Attachment where Id In :attachmentIdList])
                {
                    Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                    efa.setFileName(attachment.Name);
                    efa.setBody(attachment.Body);
                    attachment.Body = null;
                    fileAttachments.add(efa);
                }
                // List of all the possible file types(extensions) - By Payal (11Sept2019)
                List<String> fileExtensionsList = new List<String>{'doc','docx','odt','pdf','csv','xls','xlsx','ods','ppt','pptx','txt','jpg','jpeg','png','gif','svg','bmp',
                    'gif','ai','ico','tif','tiff','psd','ps','odp','pps','xlr','rtf','tex','wks','wps','wpd','zip','accdb','jnt','pub'};
                        for(ContentVersion conv :[SELECT VersionData,FileExtension,FileType,Id,Title FROM ContentVersion where id IN: filesIdList]){
                            Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                            //Added extension with title - By Payal (11Sept2019) Start
                            System.debug('conv Title='+conv.Title);
                            System.debug('conv FileExtension='+conv.FileExtension);
                            String convTitle = conv.Title;
                            try{
                                Integer dotLastIndex = convTitle.lastIndexOf('.');
                                if(dotLastIndex > -1){
                                    String titleExt = convTitle.substring(dotLastIndex+1);
                                    if(fileExtensionsList.contains(titleExt.trim().toLowerCase())){
                                        convTitle = convTitle.substring(0, dotLastIndex);
                                    }
                                }
                            }catch(Exception e){
                                System.debug('Exception in ContentVersion Extension: ' + e);          
                            }
                            //efa.setFileName(conv.Title + '.' + conv.FileExtension);
                            efa.setFileName(convTitle + '.' + conv.FileExtension);
                            //Added extension with title - By Payal (10Sept2019) End
                            efa.setBody(conv.VersionData);
                            fileAttachments.add(efa);
                        }
                if(!fileAttachments.isEmpty()){
                    for(Messaging.singleEmailMessage mail : mails){
                        mail.setSaveAsActivity(true);
                        mail.setFileAttachments(fileAttachments);
                    }
                }
            }
            
            Messaging.sendEmail(mails);
            if(!taskList.isEmpty()){
                ///// insert taskList;
                DMLManager.insertAsUser(taskList);
            }
            msg = 'Email has been sent succesfully.';
        }
        catch(Exception e){
            system.debug('Exception'+e);          
            
        }
        
    }
    
    private static Messaging.singleEmailMessage getSingleEmail(String surveyTaskId,String userOrContactId,String emailStrId,String subjectToSet,String emailBodyToSet,Boolean avoidSurvey,String surveyStrId,String taskOrderId){
        Messaging.singleEmailMessage mail = new Messaging.singleEmailMessage();
        if(emailStrId!=Null && emailStrId!=''){
            mail.setTemplateId((Id)emailStrId); // Mail set email templete Id
        }else{
            mail.setSubject(subjectToSet); //custom mail set Subject
            mail.setPlainTextBody(emailBodyToSet); //custom mail set Body
        }
        if(!avoidSurvey && surveyStrId!=Null && surveyStrId!='' && emailStrId!=Null && emailStrId!=''){
            if(!test.isRunningTest()){
                mail.setWhatId(surveyTaskId); // Mail set survey Id as watId
            }
        }
        else{
            System.debug('Set Task Orser Id = '+taskOrderId);
            mail.setWhatId(taskOrderId); // Mail set Task order id as watId
        }
        mail.setTargetObjectId(userOrContactId);  
        mail.setSaveAsActivity(true);   
        return mail; 
    }
    
    
    
    
    private static Task getTask(Survey__c survey,String surveyTaskId,User usr,String contactId,String taskOrderId,List<Task_Order_Questionnaire__c> communityUrlList){
        Task tsk = new Task();
        /*if(usr != null){
tsk.OwnerId = usr.Id ;
if(usr.Profile.UserLicense.Name.contains('Partner')){
if(communityUrlList[0].Partner_Community_URL__c != null){
tsk.Description =  communityUrlList[0].Partner_Community_URL__c+'/apex/TM_SurveyResponsePage?sid='+surveyTaskId;
}
}
else{
if(communityUrlList[0].Customer_Community_URL__c != null){
tsk.Description = communityUrlList[0].Customer_Community_URL__c+'/apex/TM_SurveyResponsePage?sid='+surveyTaskId;
}
}
}*/
        
        if(survey != null){
            tsk.Subject = survey.Name;
        }
        else{
            tsk.Subject = 'New Questionnaire';
        }
        tsk.Priority = 'Normal';
        tsk.Status = 'Not Started';
        tsk.WhatId = taskOrderId;
        tsk.ActivityDate = Date.today().addDays(3);
        if(contactId != null){
            tsk.whoId = contactId;
        }
        return tsk;
        
    }
    private static void updateTeamingpartnerStatus(Set<Id> accountIdSet,String taskOrderId){
        List<Teaming_Partner__c> teamingPrtnerList = [Select Id,Status__c From Teaming_Partner__c where Task_Order__c =: taskOrderId And Vehicle_Partner__r.Partner__c IN : accountIdSet];
        List<Teaming_Partner__c> updateTeamingPartnerList = new List<Teaming_Partner__c>();
        for(Teaming_Partner__c teamingPartner : teamingPrtnerList){
            if(teamingPartner.Status__c == 'New'){
                teamingPartner.Status__c = 'Request Sent';
                updateTeamingPartnerList.add(teamingPartner);
            }
        }
        if(!updateTeamingPartnerList.isEmpty()){
            //update updateTeamingPartnerList;
            DMLManager.updateAsUser(updateTeamingPartnerList);
        }
    }
    
    @AuraEnabled
    public static List<EmailWrapper> selectEmailTemplete(String surveyStrId){
        List<EmailWrapper> emailWrap = new List<EmailWrapper>();
        List<EmailTemplate> emailTemplateList;
        if(surveyStrId!=Null && surveyStrId!=''){ 
            List<Id> emailTempleteIdList = new List<Id>();
            for(Questionaries_Email_Template__c qet : [SELECT Email_Templete__c From Questionaries_Email_Template__c WHERE Teaming_Request_Questionnaire__c=:surveyStrId LIMIT 1000]){
                emailTempleteIdList.add(qet.Email_Templete__c);
            }
            emailTemplateList = [Select Id, name From EmailTemplate where folder.name = 'TOMA Email Templates' AND Id In :emailTempleteIdList];
        }else{ 
            emailTemplateList = [Select Id, name From EmailTemplate where folder.name = 'TOMA Email Templates'];
        }
        
        if(emailTemplateList != null && emailTemplateList.size()>0){
            for(EmailTemplate etmp : emailTemplateList){
                emailWrap.add(new EmailWrapper(etmp));
            }
            
        }
        system.debug('emailTemplateList'+emailWrap);
        return emailWrap;
    }  
    
    
    /* public class parentWrapper{

@AuraEnabled 
public List<ContactWrapper> conWrapList{get;set;}
@AuraEnabled 
public List<SurveyWrapper> surveyWrapList{get;set;}
@AuraEnabled 
public List<EmailWrapper> emailWrapList{get;set;}
@AuraEnabled 
public List<AttachmentWrapper> attachWrapList{get;set;}


public parentWrapper(List<ContactWrapper> conWrapList,List<SurveyWrapper> surveyWrapList,List<EmailWrapper>emailWrapList,List<AttachmentWrapper> attachWrapList){
this.conWrapList= conWrapList;
this.surveyWrapList = surveyWrapList;
this.emailWrapList=emailWrapList;
this.attachWrapList=attachWrapList;
}
}

public class ContactWrapper{
@AuraEnabled
public boolean isSelected{get;set;}
@AuraEnabled
public Task_Order_Contact__c con {get;set;}

public ContactWrapper(Task_Order_Contact__c con){
this.con = con;
this.isSelected=false;
}
}

public class EmailWrapper{
@AuraEnabled
public boolean isSelected{get;set;}
@AuraEnabled
public EmailTemplate emailTemp {get;set;}

public EmailWrapper(EmailTemplate emailTemp){
this.emailTemp = emailTemp;
this.isSelected=false;
}
}

public class SurveyWrapper{
@AuraEnabled
public boolean isSelected{get;set;}
@AuraEnabled
public Survey__c survey {get;set;}

public SurveyWrapper(Survey__c survey){
this.survey = survey;
this.isSelected = false;
}
}

public class AttachmentWrapper{
@AuraEnabled
public boolean isSelected{get;set;}
@AuraEnabled
public boolean isFile{get;set;}
@AuraEnabled
public Id attachmentId {get;set;}
@AuraEnabled
public String name{get;set;}
@AuraEnabled
Public Integer length{get;set;}
public AttachmentWrapper(Id attachmentId,String name,Integer length){
this.attachmentId = attachmentId;
this.name = name;
this.length = length;
this.isSelected = false;
this.isFile=false;
}
public AttachmentWrapper(Id attachmentId,String name,Integer length,Boolean isFile){
this.attachmentId = attachmentId;
this.name = name;
this.length = length;
this.isSelected = false;
this.isFile=isFile;
}
} */
}