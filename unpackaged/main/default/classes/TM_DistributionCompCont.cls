public with sharing class TM_DistributionCompCont {
    @AuraEnabled
    public static String checkLicensePermition1()
    {
       // return LincenseKeyHelper.checkLicensePermitionForFedCLM();//commented on 01-02-2019 as enterprise we don't need to check anything
       return 'Yes';
    }
    @AuraEnabled
    public static List<DistributionFieldSetWrapper> getRelatedDistributionColumeName()
    {
        //Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        //Map <String, Schema.SObjectField> fieldMap = schemaMap.get('Distribution__c').getDescribe().fields.getMap();
        //set<String> fieldSet = schemaMap.get('Distribution__c').getDescribe().fields.getMap().keySet();
        List<DistributionFieldSetWrapper> FieldList = new List<DistributionFieldSetWrapper>();
        List<Schema.SObjectField> distributionFieldApiList =Distribution__c.SObjectType.getDescribe().fields.getMap().values();
        if(distributionFieldApiList.size() > 0){
            
            
            
            for(Schema.SObjectField sobjectField : distributionFieldApiList){
                //string typeOFField = (String)fieldMap.get(tempField).getDescribe().getType()+'';
                //System.debug('typeOFField '+typeOFField);
                System.debug('sobjectField '+sobjectField);
                String tempField = sobjectField.getDescribe().getName();
                String fieldLabel = sobjectField.getDescribe().getLabel();
                String typeOFField = sobjectField.getDescribe().getType()+'';
                //System.debug('tempField '+tempField);
                if(tempField.endsWith('__c') && typeOFField == 'REFERENCE' && tempField != 'TM_TOMA__Contract_Vehicle__c' && tempField != 'TM_TOMA__Subcontract__c' && tempField != 'TM_TOMA__Task_Order__c')
                {
                    System.debug('Lookup reference object name: ' + sobjectField.getDescribe().getReferenceTo()[0].getDescribe().getName());
                    String parentObjApiName = sobjectField.getDescribe().getReferenceTo()[0].getDescribe().getName();
                    DistributionFieldSetWrapper disField = new DistributionFieldSetWrapper(fieldLabel,typeOFField,tempField,parentObjApiName);
                    fieldList.add(disField);
                }
               
            }
            
        }
        System.debug('FieldList size '+FieldList.size());
        if(FieldList.size()>1)
        {
            for(integer i=0;i<FieldList.size();i++) 
            {
                if(FieldList[i].fieldAPI == 'TM_TOMA__User__c')
                {
                    FieldList.remove(i);
                }
            }
        }
        DistributionFieldSetWrapper roleDisField = new DistributionFieldSetWrapper('Role','PICKLIST','TM_TOMA__Role__c','');
                    fieldList.add(roleDisField);
        
        System.debug('FieldList size '+FieldList.size());
        System.debug('FieldList size '+FieldList);
        return FieldList;
    }
    @AuraEnabled
    public static List<GetDistributionDataWrap> getRelatedDistributionRecords(Id parentRecId)
    {
        
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        String sObjName = parentRecId.getSObjectType().getDescribe().getName();
        set<String> fieldSet = schemaMap.get('TM_TOMA__Distribution__c').getDescribe().fields.getMap().keySet();
        string allCustumField = '';
        if(fieldSet.size() > 0){
            for(string tempField : fieldSet){
                if(tempField.contains('__c')){
                    allCustumField = allCustumField+tempField+',';
                }
            }
        }
        Id parRecId = parentRecId;
        allCustumField = allCustumField.removeEnd(',');
        System.debug(allCustumField);
        String query = '';
        if(sObjName == 'TM_TOMA__Contract_Vehicle__c'){
            query ='SELECT '+allCustumField+',createddate,CreatedById,lastmodifieddate FROM TM_TOMA__Distribution__c WHERE '+' TM_TOMA__Contract_Vehicle__c = :parRecId ';
        }
        else If(sObjName == 'TM_TOMA__Subcontract__c'){
            query ='SELECT '+allCustumField+',createddate,CreatedById,lastmodifieddate FROM TM_TOMA__Distribution__c WHERE '+' TM_TOMA__Subcontract__c = :parRecId ';
        }
        else If(sObjName == 'TM_TOMA__Task_Order__c'){
            query ='SELECT '+allCustumField+',createddate,CreatedById,lastmodifieddate FROM TM_TOMA__Distribution__c WHERE '+' TM_TOMA__Task_Order__c = :parRecId ';
        }
        List<GetDistributionDataWrap> distributionList = new List<GetDistributionDataWrap>();
        List<Distribution__c> tempDisList = Database.query(query);
        for(Distribution__c dis : tempDisList)
        {
            GetDistributionDataWrap tempDistributionData = new GetDistributionDataWrap(dis,'No');
            distributionList.add(tempDistributionData);
        }
        return distributionList;
    }
    @AuraEnabled
    public static GetDistributionDataWrap addNewDistributionRowInWrap(Id parentObjId){
        //system.debug('inside addNewDistributionRowInWrap');
        if(Schema.sObjectType.Distribution__c.isCreateable()){
            Distribution__c distTableNewRec = new Distribution__c ();
            GetDistributionDataWrap newDistribution =new GetDistributionDataWrap(distTableNewRec,'Yes');
            return newDistribution;
        }
        else
        {
            return null;
        }
    }
    @AuraEnabled
    public static void saveDistributionRecordList(String distributionList,String distributionDeletedId,String distributionFieldApiName,Id parentRecId){
        System.debug('distributionList '+distributionList);
        //
        /* if(distributionDeletedId!=Null)
{
string[] delIds=distributionDeletedId.split(',');
delIds.remove(0);
List<Distribution__c> delList = [Select Id from Distribution__c where Id IN: delIds];
DMLManager.deleteAsUser(delList);
}*/
        List<Distribution__c> newdistributionList = new List<Distribution__c>();
        set<Id> existingDistributionIdSet = new set<Id>();
        map<Id,Distribution__c> newValueMap = new map<Id,Distribution__c>();
        
        if(distributionList != null && distributionList.length()>1)
        {
            distributionList =distributionList.substring(1);
            List<String> distributionPatnerstrList = distributionList.split('SPLITPARENT');
            if(distributionPatnerstrList.size() > 0)
            {
                for(String temp : distributionPatnerstrList){
                    Distribution__c distribution = new Distribution__c();
                    distribution = getDistributionRecord(temp);
                    if(distribution != null)
                    {
                        //newdistributionList.add(distribution);
                        System.debug('distribution.Id '+distribution.Id);
                        if(distribution.Id != null)
                        {
                            existingDistributionIdSet.add(distribution.Id);
                            newValueMap.put(distribution.Id, distribution);
                        }
                        else
                        {
                            newdistributionList.add(distribution);
                        }
                    }
                    // System.debug('newdistributionList '+newdistributionList);
                }
            }
        }
        System.debug('existingDistributionIdSet '+existingDistributionIdSet);
        System.debug('newdistributionList '+newdistributionList);
        System.debug('newValueMap '+newValueMap);
        if(existingDistributionIdSet.size()>0)
        {
            string fields = distributionFieldApiName.removeEnd(',');
            string dataQuery ='SELECT id,'+fields+' FROM TM_TOMA__Distribution__c WHERE id In:existingDistributionIdSet';
                    List<Distribution__c> tempDistribution = Database.query(dataQuery);
             System.debug('tempDistribution '+tempDistribution);
            map<Id,Distribution__c> oldValueMap = new map<Id,Distribution__c>(tempDistribution);
            System.debug('oldValueMap '+oldValueMap);
            List<String> fieldList = fields.split(',');
            System.debug('fieldList '+fieldList);
            for(string tempId : existingDistributionIdSet)
            {
                System.debug('tempId '+tempId);
                boolean status = false;
                Distribution__c oldDist =  oldValueMap.get(tempId);
                Distribution__c newDist =  newValueMap.get(tempId);
                for(string fieldApi : fieldList)
                {
                    System.debug('oldDist.get(fieldApi) '+oldDist.get(fieldApi));
                    System.debug('newDist.get(fieldApi) '+newDist.get(fieldApi));
                    if(oldDist.get(fieldApi) != newDist.get(fieldApi))
                    {
                        
                        status = true;
                        System.debug('status '+status);
                    }
                }
                if(status == true)
                {
                    System.debug('status '+status);
                    newdistributionList.add(newDist);
                }
               // status = false;
            }
         }
        
        System.debug('newdistributionList '+newdistributionList);
        String sObjName = parentRecId.getSObjectType().getDescribe().getName();
        Id currentRecordTypeId;
        if(!newdistributionList.isEmpty()){
            for(Distribution__c tempDist : newdistributionList)
            {
                if(sObjName == 'TM_TOMA__Contract_Vehicle__c'){
                    System.debug('sObjName '+sObjName);
                    currentRecordTypeId = Schema.SObjectType.Distribution__c.getRecordTypeInfosByName().get('Contract Vehicle').getRecordTypeId();
                    System.debug('TM_TOMA__Contract_Vehicle__c currentRecordTypeId '+currentRecordTypeId);
                    tempDist.RecordTypeId  = currentRecordTypeId;
                    tempDist.Contract_Vehicle__c = parentRecId;
                    //tempDist.Last_Distribution_Date__c = System.now();
                }
                if(sObjName == 'TM_TOMA__Subcontract__c'){
                    currentRecordTypeId = Schema.SObjectType.Distribution__c.getRecordTypeInfosByName().get('Subcontract').getRecordTypeId();
                    System.debug('TM_TOMA__Subcontract__c currentRecordTypeId '+currentRecordTypeId);
                    tempDist.RecordTypeId  = currentRecordTypeId;
                    tempDist.Subcontract__c = parentRecId;
                   // tempDist.Last_Distribution_Date__c = System.now();
                }
                if(sObjName == 'TM_TOMA__Task_Order__c'){
                    currentRecordTypeId = Schema.SObjectType.Distribution__c.getRecordTypeInfosByName().get('Task Order').getRecordTypeId();
                    System.debug('TM_TOMA__Subcontract__c currentRecordTypeId '+currentRecordTypeId);
                    tempDist.RecordTypeId  = currentRecordTypeId;
                    tempDist.Task_Order__c = parentRecId;
                   // tempDist.Last_Distribution_Date__c = System.now();
                }
            }
            try{
                //DMLManager.upsertAsUser(newdistributionList);
                System.debug('newdistributionList '+newdistributionList);
                upsert (newdistributionList);
            }catch(Exception e){
                System.debug('Exception :- '+e);
            }
        }
    }
    @AuraEnabled
    public static void deleteDistribution(string delId){
        if(delId!=Null)
        {
            List<Distribution__c> delList = [Select Id from Distribution__c where Id =: delId];
            try{
                DMLManager.deleteAsUser(delList);
            }
            
            catch(Exception e)
            {
                System.debug(' Error :-  '+e.getLineNumber()+' '+e.getMessage());
            }
        }
    }
    
    
    
    private static Distribution__c getDistributionRecord(String distributionStr){
        if(distributionStr != null && distributionStr.trim().length() > 0){
           System.debug('distributionStr '+distributionStr);
          //  string fields='';
            String[] distributionStrList = distributionStr.split('SPLITDATA');
            if(distributionStrList.size() > 0){
                Distribution__c dist = new Distribution__c();
                for(String temp : distributionStrList){
                    String[] fieldValueList =temp.split('SPLITFIELD');
                    if(Schema.SObjectType.Distribution__c.isCreateable()){
                        System.debug('fieldValueList '+fieldValueList[0]+'   '+fieldValueList[1]);
                        if(fieldValueList[0] == 'id')
                        {
                            
                            if(fieldValueList[1] != 'undefined')
                                dist.Id = fieldValueList[1];
                        }
                        else
                        {
                          //  fields = fields+fieldValueList[0]+',';
                            if(fieldValueList[1] != 'undefined'){
                                dist.put(fieldValueList[0],fieldValueList[1]);
                            }
                            else{
                                dist.put(fieldValueList[0],null);
                            }
                        }
                    }
                }
              //  System.debug('fields '+fields.removeEnd(','));
                Boolean status = False;
               // fields = fields.removeEnd(',');
                System.debug('dist.id '+dist.id);
              /*  if(dist.id != null){
                    string recId = dist.id;
                    
                    string dataQuery ='SELECT id,'+fields+' FROM TM_TOMA__Distribution__c WHERE id =:recId';
                    TM_TOMA__Distribution__c tempDistribution = Database.query(dataQuery);
                    if(tempDistribution != null){
                        String[] distfieldList = fields.split(',');
                        for(String tempField : distfieldList){
                            if(tempDistribution.get(tempField) != dist.get(tempField)){
                                status = true; 
                            }
                        }
                    }
                    if(status == true){
                        return dist;
                    }
                    else{
                        return null;
                    }
                }
                else{
                    return dist;
                }*/
               return dist;
            }
            else{
                return null;
            }
        }
        else{
            return null;
        }
    }
    
  /*   @AuraEnabled
    public static Map<String,String> getPickvalOfRole()
    {
        //list<String> options = new list<String>();
      //   options.add('--None--');
        Map<String,String> optionsMap=new Map<String,String>();
        Schema.DescribeFieldResult feildResult = TM_TOMA__Distribution__c.TM_TOMA__Role__c.getDescribe();
        LIST<Schema.PicklistEntry> ple = feildResult.getPicklistValues();
        for(Schema.PicklistEntry f : ple)
        {
            //options.add(f.getLabel());
            optionsMap.put(f.getLabel(), f.getValue());
            
        }
        //return options;
       return optionsMap;
    }
    */
    /* public class DistributionFieldSetWrapper{

@AuraEnabled
public String label {get; set;}

@AuraEnabled
public String typeOfField {get; set;}

@AuraEnabled
public String fieldAPI {get; set;}

public DistributionFieldSetWrapper(String leb,String fieldType,String api){
label = leb;
fieldType = fieldType;
fieldAPI = api;
}
}*/
    
    /*  public class GetDistributionDataWrap{

@AuraEnabled
public Distribution__c disTab;


public GetDistributionDataWrap(Distribution__c disTab1)
{
disTab=disTab1;

System.debug('disTab '+disTab);
}
}*/
}