({
  /*  getAllRolePicklistValue: function(component, event, helper) {
        
        var action = component.get("c.getPickvalOfRole"); //call apex controller method
        var opts = []; //create array
       
        action.setCallback(this,function(a){
           
               //var queTypelist = a.getReturnValue();
            //alert(queTypelist);
             var custs = [];
                var conts = a.getReturnValue();
                for (var key in conts ) {
                    custs.push({value:conts[key], key:key});
                }
                component.set("v.roleMap", custs);
        
              //component.set("v.IfNotABookingWhy",queTypelist);  //setting value to variable of Input field
        });
        $A.enqueueAction(action);  
    },*/
    checkValidation : function(component, event, helper,index) {
        var checkCounter = index;

        var elements = document.getElementsByClassName(checkCounter+'distViewCls');
        for(var i=0; i<elements.length; i++) {
            elements[i].className += " slds-hide";
            elements[i].classList.remove("slds-show");
        }
        var elements1 = document.getElementsByClassName(checkCounter+'distEditCls');
        for(var i=0; i<elements1.length; i++) {
            elements1[i].className += " slds-show";
            elements1[i].classList.remove("slds-hide");
        }
        var d4 = document.getElementById(checkCounter+'distEdit');
        d4.className += " slds-hide";
        d4.classList.remove("slds-show");
        var d5 = document.getElementById(checkCounter+'distEditUndo');
        d5.className += " slds-show";
        d5.classList.remove("slds-hide");
        var checksave = component.get("v.checkSavebutton");
        checksave=checksave+1;
        component.set("v.checkSavebutton",checksave);
      
    },
    getDistributionFieldSetWrapperData : function(component, event, helper) {
        var action = component.get("c.getRelatedDistributionColumeName"); //call apex controller method
        action.setCallback(this,function(a){
            var state= a.getState();
            if(state=="SUCCESS"){
               // alert(a.getReturnValue());
                component.set("v.distributionFieldSetWrapperData",a.getReturnValue());
            }
            else if (state == "ERROR") 
            {
                console.log(a.getError());            
            }
        });
        $A.enqueueAction(action);
        
    },
    getDistributionDataWrapList : function(component, event, helper) {
        var action = component.get("c.getRelatedDistributionRecords"); //call apex controller method
       // console.log('@@@@@@@@@@@@@@@'+component.get("v.recordId")); 
        action.setParams({
            "parentRecId": component.get("v.recordId")
        });
        action.setCallback(this,function(a){
            var state= a.getState();
            if(state=="SUCCESS"){
                if(a.getReturnValue() !== undefined){
                    var tempWrap=a.getReturnValue();
                    if(tempWrap == null){ 
                         component.set("v.checkSavebutton",0);
                        component.set("v.distributionSizeFlag",0);
                    }
                    else{
                        component.set("v.checkSavebutton",0);
                        component.set("v.distributionSizeFlag",tempWrap.length);
                        component.set("v.delDistributionId",'');
                        component.set("v.distributionDataWrapList",a.getReturnValue());
                       // component.set("v.tempdistributionDataWrapList",a.getReturnValue());
                    }
                }
            }
            else if (state == "ERROR") 
            {
                console.log(a.getError());            
            }
        });
        $A.enqueueAction(action);
    }
})