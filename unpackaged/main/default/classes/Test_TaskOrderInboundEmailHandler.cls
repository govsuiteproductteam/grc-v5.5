@isTest
public with sharing class Test_TaskOrderInboundEmailHandler  {
    public Static TestMethod void TaskOrderInboundEmailHandler (){
        Account acc = TestDataGenerator.createAccount('Test Acc');
        insert acc;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('T-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'testmail@gmail.com';
        insert conVehi;
        Messaging.InboundEmail email  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        email.plainTextBody = 'This should become a note';
        email.fromAddress ='test@test.com';
        email.subject = 'task order data';
        email.htmlBody ='<div dir="ltr"><div class="gmail_quote"><div dir="ltr"><div style="font-size:12.8000001907349px">An event, to which your company had been invited, has been updated.</div><div style="font-size:12.8000001907349px"><br></div><div style="font-size:12.8000001907349px">Event Information:</div><div style="font-size:12.8000001907349px"><br></div><div style="font-size:12.8000001907349px">    *  Category:    SeaPort Enhanced</div><div style="font-size:12.8000001907349px">    *  Name:        N00024-13-R-3429-5:1</div><div style="font-size:12.8000001907349px">    *  Description: Program management support for the UH-1 (Huey) and the Ah-1 (Cobra) Light/Attack helicopters program office. (Zone 2, Reserved for SDVOSB SB, Activity: NAVAIR Aircraft Division Pax River, UIC: N00421)</div><div style="font-size:12.8000001907349px">    *  Start Time:  3/31/2014 4:10:00 PM (GMT-05:00) Eastern Time (US &amp; Canada)</div><div style="font-size:12.8000001907349px">    *  Stop Time:   4/2/2014 3:00:00 PM (GMT-05:00) Eastern Time (US &amp; Canada)</div><div style="font-size:12.8000001907349px">    *  Creator:     Margaret E. Hayden (<a href="mailto:margaret.hayden1@navy.mil" target="_blank">margaret.hayden1@navy.mil</a>)</div><div style="font-size:12.8000001907349px"><br></div><div style="font-size:12.8000001907349px">    *  Contract Negotiator:     Marquita J. Davis (<a href="mailto:marquita.davis@navy.mil" target="_blank">marquita.davis@navy.mil</a>)</div><div style="font-size:12.8000001907349px">    *  Contracting Officer:     Margaret E. Hayden (<a href="mailto:margaret.hayden1@navy.mil" target="_blank">margaret.hayden1@navy.mil</a>)</div><div style="font-size:12.8000001907349px">    *  Zone:                    2 - National Capital Zone</div><div style="font-size:12.8000001907349px">    *  Set Aside:               SDVOSB</div><div style="font-size:12.8000001907349px"><br></div><div style="font-size:12.8000001907349px"><br></div><div style="font-size:12.8000001907349px">The Government is currently evaluating the proposals received from Offerors in response to solicitation N00024-13-R-3429.  It is the Government&#39;s intent to provide a more detailed status NLT 12 December 2014 directly to those Offeror&#39;s who submitted proposals in response to the aforementioned solicitation.</div><div style="font-size:12.8000001907349px"><br></div><div style="font-size:12.8000001907349px">---------------------------------------------</div><div style="font-size:12.8000001907349px">You may check in through the following link: <a href="https://auction.seaport.navy.mil/Bid/Login.aspx?RC=1_63324&amp;pcode=N" target="_blank">https://auction.seaport.navy.mil/Bid/Login.aspx?RC=1_63324&amp;pcode=N</a>. You may also copy and paste the link into your web browser.</div><div style="font-size:12.8000001907349px"><br></div><div style="font-size:12.8000001907349px">Thank you for your business with SeaPort.</div><div style="font-size:12.8000001907349px"><br></div><div style="font-size:12.8000001907349px">Please do not reply directly to this e-mail. ContactÂ <a href="mailto:SeaportSupport@aquilent.com" target="_blank">SeaportSupport@aquilent.com</a> for technical support.</div></div></div><br></div>';
        
        TaskOrderInboundEmailHandler taskEmailHandler= new TaskOrderInboundEmailHandler ();
        
        Test.startTest();
        Messaging.InboundEmailResult result = taskEmailHandler.handleInboundEmail(email, env);
        List<Task_Order__c> tOrderList = [SELECT Id FROM Task_Order__c];
        System.assertEquals(tOrderList.size(), 0);
        Test.stopTest();
    }
}