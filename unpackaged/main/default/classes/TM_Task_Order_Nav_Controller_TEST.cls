@isTest
public with sharing class TM_Task_Order_Nav_Controller_TEST {

    public static testmethod void TM_Task_Order_Nav_Controller(){
        // Setup test data
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='fedTOMnav@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='fedTOMnav@testorg.com', TM_TOMA__FedTOM_User__c = True);
                
        TM_TOMA__Contract_Vehicle__c cv = new TM_TOMA__Contract_Vehicle__c(Name='Test Contract Vehicle');
        insert cv;
        TM_TOMA__Task_Order__c to = new TM_TOMA__Task_Order__c(Name='RFQ12345', TM_TOMA__Contract_Vehicle__c = cv.id);
        insert to;
        
        Test.startTest();
        System.runAs(u){
            
            ApexPages.StandardController sc = new ApexPages.StandardController(to);
            ApexPages.CurrentPage().getparameters().put('id',to.Id);
            TM_Task_Order_Nav_Controller con = new TM_Task_Order_Nav_Controller(sc);
            
            PageReference pg = con.setRedirect();
            PageReference pgCheck = new PageReference('/'+to.Id+'?nooverride=1');
            System.assertEquals(pg.getUrl(), pgCheck.getUrl());
            
            PageReference pg1 = con.setRedirectEdit();
            PageReference pgCheckEdit = new PageReference('/'+to.Id+'/e?retURL=/'+to.Id+'&nooverride=1');
            System.assertEquals(pg1.getUrl(), pgCheckEdit.getUrl());
        }
        test.stopTest();
        
    }
    
    public static testmethod void TM_Task_Order_Nav_Controller1(){
        // Setup test data
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='fedTOMnav@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='fedTOMnav@testorg.com', TM_TOMA__FedTOM_User__c = True);
                
        TM_TOMA__Contract_Vehicle__c cv = new TM_TOMA__Contract_Vehicle__c(Name='Test Contract Vehicle');
        insert cv;
        TM_TOMA__Task_Order__c to = new TM_TOMA__Task_Order__c(Name='RFQ12345', TM_TOMA__Contract_Vehicle__c = cv.id);
        
        Schema.DescribeSObjectResult r = TM_TOMA__Task_Order__c.sObjectType.getDescribe();
        String keyPrefix = r.getKeyPrefix();
        
        Test.startTest();
        System.runAs(u){
        
            ApexPages.StandardController sc = new ApexPages.StandardController(to);
            
            TM_Task_Order_Nav_Controller con = new TM_Task_Order_Nav_Controller(sc);
            
            PageReference pg = con.setRedirectNew();
            PageReference pgCheck = new PageReference('/'+keyPrefix+'/e?retURL=/'+keyPrefix+'/o&nooverride=1');
            System.assertEquals(pg.getUrl(), pgCheck.getUrl());
        }
        Test.stopTest();
    }
    public static testmethod void TM_Task_Order_Nav_Controller2(){
        // Setup test data
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='fedTOMnav@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='fedTOMnav@testorg.com', TM_TOMA__FedTOM_User__c = False);
                
        TM_TOMA__Contract_Vehicle__c cv = new TM_TOMA__Contract_Vehicle__c(Name='Test Contract Vehicle');
        insert cv;
        TM_TOMA__Task_Order__c to = new TM_TOMA__Task_Order__c(Name='RFQ12345', TM_TOMA__Contract_Vehicle__c = cv.id);
        insert to;
        
        Test.startTest();
        System.runAs(u){
            
            ApexPages.StandardController sc = new ApexPages.StandardController(to);
            ApexPages.CurrentPage().getparameters().put('id',to.Id);
            TM_Task_Order_Nav_Controller con = new TM_Task_Order_Nav_Controller(sc);
            
            PageReference pg = con.setRedirect();
            PageReference pgCheck = new PageReference('/apex/TM_TOMA__TM_License_Required_Error_Notification');
           // System.assertEquals(pg.getUrl(), pgCheck.getUrl());
            
            PageReference pg1 = con.setRedirectEdit();
            //System.assertEquals(pg1.getUrl(), pgCheck.getUrl());
        }
        test.stopTest();
        
    }
    
    public static testmethod void TM_Task_Order_Nav_Controller3(){
        // Setup test data
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='fedTOMnav@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='fedTOMnav@testorg.com', TM_TOMA__FedTOM_User__c = False);
                
        TM_TOMA__Contract_Vehicle__c cv = new TM_TOMA__Contract_Vehicle__c(Name='Test Contract Vehicle');
        insert cv;
        TM_TOMA__Task_Order__c to = new TM_TOMA__Task_Order__c(Name='RFQ12345', TM_TOMA__Contract_Vehicle__c = cv.id);
        
        Test.startTest();
        System.runAs(u){
            
            
            ApexPages.StandardController sc = new ApexPages.StandardController(to);
            
            TM_Task_Order_Nav_Controller con = new TM_Task_Order_Nav_Controller(sc);
            
            PageReference pg = con.setRedirectNew();
            PageReference pgCheck = new PageReference('/apex/TM_TOMA__TM_License_Required_Error_Notification');
           // System.assertEquals(pg.getUrl(), pgCheck.getUrl());
        }
        Test.stopTest();
    }
}