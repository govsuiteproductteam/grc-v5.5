@isTest
public class Test_TM_LevelOfEffortController {
  public static testMethod void testLevelOfEffortTable(){
        Account account = TestDataGenerator.createAccount('Test Account');
        insert account;
        Contract_Vehicle__c contractVehicle =  TestDataGenerator.createContractVehicle('12345', account);
        insert contractVehicle;

        Id levelOfEffortRecordTypeId = Schema.SObjectType.Value_Table__c.getRecordTypeInfosByName().get('Level of Effort').getRecordTypeId();
       
        Value_Table__c valueTable1 = TestDataGenerator.createValueTable(levelOfEffortRecordTypeId,contractVehicle.Id);
        insert valueTable1;
        
        Value_Table__c valueTable2 = TestDataGenerator.createValueTable(levelOfEffortRecordTypeId,contractVehicle.Id);
        insert valueTable2;
        
        Value_Table__c valueTable3 = TestDataGenerator.createValueTable(levelOfEffortRecordTypeId,contractVehicle.Id);
        insert valueTable3;
        
        Id valueTableRecordId = valueTable2.Id;
        Test.startTest();
        List<Value_Table__c> levelTableList = new List<Value_Table__c>();
        levelTableList.add(valueTable1);
        String levelTableListStr = JSON.serialize(levelTableList);
        TM_LevelOfEffortController.saveConValLevelEffortTable(levelTableListStr, contractVehicle.Id, 'abc,'+valueTableRecordId);
        List<Value_Table__c> valueTableList = [SELECT Id FROM Value_Table__c WHERE id =: valueTableRecordId];
        System.assert(valueTableList.size() == 0);
        Contract_Vehicle__c contractVehicle1 =  TM_LevelOfEffortController.getContractVehical(contractVehicle.Id);
        System.assert(contractVehicle1 != null);
        List<Value_Table__c> valueTableList1 = TM_LevelOfEffortController.getValueTableLevelEffortList(contractVehicle.Id);
        System.assert(valueTableList1.size() == 2);
        Value_Table__c newValueTable = TM_LevelOfEffortController.newRowLevelEffortTable(contractVehicle.Id);
        System.assert(newValueTable != null);
        TM_LevelOfEffortController.isLightningPage();
        Test.stopTest();
    }
}