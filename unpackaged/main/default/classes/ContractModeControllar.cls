public with sharing class ContractModeControllar {

    private List<String> apiStringList = new List<String>{'TM_TOMA__Contracting_Officer__c','TM_TOMA__Contract_Specialist__c','TM_TOMA__Contract_Negotiator__c','TM_TOMA__Buyer__c','TM_TOMA__Customer_Agency__c','TM_TOMA__Proposal_Due_Date_Time__c','TM_TOMA__Questions_Due_Date__c','TM_TOMA__Release_Date__c','TM_TOMA__Start_Date__c','TM_TOMA__Stop_Date__c','TM_TOMA__ASB_Action__c','TM_TOMA__Task_Order_Number__c','TM_TOMA__Task_Order_Title__c','TM_TOMA__Description__c',
                                                           'TM_TOMA__Category__c','TM_TOMA__Delivery__c','TM_TOMA__Reference__c','TM_TOMA__Category_Backend__c','TM_TOMA__RTEP_Status__c','TM_TOMA__Requiring_Activity__c','TM_TOMA__Acquisition_Tracker_ID__c','TM_TOMA__Contract_Type__c','TM_TOMA__Competitive_Procedure__c','TM_TOMA__Market_Research_Due_Date__c','TM_TOMA__Primary_Requirement__c', 'TM_TOMA__Requirement__c','TM_TOMA__Individual_Receiving_Shipment__c','TM_TOMA__Task_Order_Contracting_Activity__c',
                                                            'TM_TOMA__Agency__c','TM_TOMA__Agency_City__c','TM_TOMA__Agency_State__c','TM_TOMA__Agency_Street_Address__c','TM_TOMA__Agency_Zip__c','TM_TOMA__GPAT__c','TM_TOMA__SEWP_Agency_ID__c','TM_TOMA__Reseller_Requirements__c',
                                                            'TM_TOMA__Installation__c','TM_TOMA__Contracting_Office_Name__c','TM_TOMA__Zone__c','TM_TOMA__Award_Type__c','TM_TOMA__Expedited_Shipment_Picklist__c','TM_TOMA__Fair_Opportunity_Picklist__c','TM_TOMA__Outside_System_Order_Picklist__c','TM_TOMA__Stage__c','TM_TOMA__ITES_Format__c'};
    public List<String> apiList{get;set;}
    public Contract_Mods__c contractMode{get;set;}
    public ContractModeControllar(ApexPages.StandardController controller){
        if(Schema.sObjectType.Contract_Mods__c.isAccessible()){
        apiList = new List<String>();
        contractMode = [SELECT Contracting_Officer__c,Contract_Specialist__c,Description__c,Due_Date__c,Release_Date__c,Start_Date__c,Stop_Date__c,ASB_Action__c,Task_Order_Number__c,Task_Order_Title__c,New_Contracting_Officer__c,New_Contract_Specialist__c,New_Description__c,New_Due_Date__c,New_Release_Date__c,New_Start_Date__c,New_Stop_Date__c,New_ASB_Action__c,New_Task_Order_Number__c,TaskOrder__c,New_Task_Order_Title__c,
                        Proposal_Due_Date_Time__c,Contract_Negotiator__c,New_Proposal_Due_Date_Time__c, New_Contract_Negotiator__c,Questions_Due_Date__c,New_Questions_Due_Date__c,TM_TOMA__Buyer__c,TM_TOMA__New_Buyer__c,TM_TOMA__Category__c, TM_TOMA__New_Category__c,TM_TOMA__Customer_Agency__c,TM_TOMA__New_Customer_Agency__c,TM_TOMA__Delivery__c,TM_TOMA__New_Delivery__c,TM_TOMA__Reference__c,TM_TOMA__New_Reference__c,
                        Category_Backend__c,NEw_Category_Backend__c,RTEP_Status__c,New_RTEP_Status__c,Requiring_Activity__c,New_Requiring_Activity__c,Acquisition_Tracker_ID__c,New_Acquisition_Tracker_ID__c,Contract_Type__c,New_Contract_Type__c,Competitive_Procedure__c,New_Competitive_Procedure__c,Market_Research_Due_Date__c,New_Market_Research_Due_Date__c, TM_TOMA__New_Primary_Requirement__c, TM_TOMA__New_Requirement__c,
                        TM_TOMA__Primary_Requirement__c, TM_TOMA__Requirement__c,Individual_Receiving_Shipment__c,New_Individual_Receiving_Shipment__c,Task_Order_Contracting_Activity__c,New_Task_Order_Contracting_Activity__c,
                        Agency__c,Agency_City__c,Agency_State__c,Agency_Street_Address__c,New_Agency__c,New_Agency_City__c,New_Agency_State__c,New_Agency_Street_Address__c,New_Agency_Zip__c,
                        New_GPAT__c,Agency_Zip__c,GPAT__c,SEWP_Agency_ID__c,New_SEWP_Agency_ID__c,Reseller_Requirements__c,Zone__c,New_Zone__c,
                        New_Reseller_Requirements__c,Installation__c,New_Installation__c,Contracting_Office_Name__c,New_Contracting_Office_Name__c,Award_Type__c,New_Award_Type__c, 
                        Expedited_Shipment_Picklist__c, New_Expedited_Shipment_Picklist__c,Fair_Opportunity_Picklist__c,New_Fair_Opportunity_Picklist__c,
                        Outside_System_Order_Picklist__c,New_Outside_System_Order_Picklist__c,createdDate,Stage__c,New_Stage__c,ITES_Format__c,New_ITES_Format__c 
                        FROM Contract_Mods__c WHERE id=:controller.getRecord().id And RecordType.Name = 'Update'];
        for(String api : apiStringList){
            if(contractMode.get(api)!=Null || contractMode.get(api.replace('TM_TOMA__','TM_TOMA__New_'))!=Null){
                apiList.add(api);
            }
        }
        System.debug('apiList '+apiList);
        }
    }
  
}