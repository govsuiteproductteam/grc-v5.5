public with sharing class EarnedAwardTableController {
    
    public static Id EarnedAwardTRecordTypeId;
    public static Value_Table__c valueTableEarnedAward{get;set;}
    @AuraEnabled public Boolean isCreateValueTable {get;set;}
    @AuraEnabled public Boolean isUpdateValueTable {get;set;}
    @AuraEnabled public Boolean isDeleteValueTable {get;set;}
    
    @AuraEnabled
    public static EarnedAwardTableController getValueTablePermissions(){
        EarnedAwardTableController earnedAwardTableCtrl = new EarnedAwardTableController();
        earnedAwardTableCtrl.isCreateValueTable = false;
        earnedAwardTableCtrl.isUpdateValueTable = false;
        earnedAwardTableCtrl.isDeleteValueTable = false;
        
        if(Schema.sObjectType.Value_Table__c.isCreateable())
            earnedAwardTableCtrl.isCreateValueTable = true;
        
        if(Schema.sObjectType.Value_Table__c.isUpdateable())
            earnedAwardTableCtrl.isUpdateValueTable = true;
        
        if(Schema.sObjectType.Value_Table__c.isDeletable())
            earnedAwardTableCtrl.isDeleteValueTable = true;
        return earnedAwardTableCtrl;
    }
    
    @AuraEnabled
    public static String checkLicensePermition1()
    {
        return LincenseKeyHelper.checkLicensePermitionForFedCLM();
    }
    @AuraEnabled
    public static Contract_Vehicle__c getContractVehical(Id recordId)
    {
        if(Schema.sObjectType.Contract_Vehicle__c.isAccessible()){
            Contract_Vehicle__c contVech = new Contract_Vehicle__c();
            if(recordId!=Null)
            {
                contVech = [select Id from Contract_Vehicle__c where Id =: recordId];
            }
            
            return contVech;
        }
        else
        {
            return null;
        }
    }
    
    @AuraEnabled
    public static List<Value_Table__c> getValueTableEarnedAwardList (Id contractVehicalId)
    {
        if(Schema.sObjectType.Value_Table__c.isAccessible()){
            EarnedAwardTRecordTypeId = Schema.SObjectType.Value_Table__c.getRecordTypeInfosByName().get('Award Fee').getRecordTypeId();
            List<Value_Table__c> ValueTableEarnedAwardTableList = new List<Value_Table__c> ();
            ValueTableEarnedAwardTableList =[select Id,Start_Date__c,End_Date__c,Dollars__c,Score__c, comments__c,RecordTypeId,Contract_Vehicle__c from Value_Table__c where Contract_Vehicle__c=:contractVehicalId AND RecordTypeId =: EarnedAwardTRecordTypeId ORDER BY Start_Date__c ASC ,End_Date__c ASC];
            return ValueTableEarnedAwardTableList; 
        }
        else
        {
            return null;
        }
    }
    
    @AuraEnabled
    public static Value_Table__c newRowEarnedAwardFeeTable(Id contractVehicalId)
    {
        if(Schema.sObjectType.Value_Table__c.isCreateable()){
            EarnedAwardTRecordTypeId  = Schema.SObjectType.Value_Table__c.getRecordTypeInfosByName().get('Award Fee').getRecordTypeId();
            valueTableEarnedAward = new Value_Table__c (RecordTypeId  = EarnedAwardTRecordTypeId ,Contract_Vehicle__c = contractVehicalId,Start_Date__c=Date.today(),End_Date__c=Date.today(),Score__c=0.0,Dollars__c=00.00, Comments__c ='');
            return valueTableEarnedAward; 
        }
        else
        {
            return null;
        }
    }
    
    @AuraEnabled
    public static Boolean isLightningPage(){
        Boolean var = UserInfo.getUiThemeDisplayed() == 'Theme4d';
        System.debug('@@@@@'+var);
        return var;
    } 
    
    
    @AuraEnabled
    public static String saveEarnedAwardFeeTable(String contValList,Id recordId,string delConValIdsStr)
    {
        String errorMessage = '';
        String successMsg = '';
        try
        {
            if(delConValIdsStr!=Null && delConValIdsStr!='')
            {
                // "If" condition and "Else" block are newly added to restrict user access according to the object permission(17/08/2018)
                if(Schema.sObjectType.Value_Table__c.isDeletable()){
                    string[] delIds = delConValIdsStr.split(',');
                    delIds.remove(0);
                    
                    List<Value_Table__c> delList = [Select Id from Value_Table__c where Id IN: delIds];
                    DMLManager.deleteAsUser(delList);
                }/*else{
                    errorMessage = 'You do not have DELETE permission for Value Table object.\n';
                }*/
            }
            if(contValList != Null && contValList!='')
            {
                // "If" condition and "Else" block are newly added to restrict user access according to the object permission(17/08/2018)
                if(Schema.sObjectType.Value_Table__c.isUpdateable()){
                    List<Value_Table__c> vtList = (List<Value_Table__c>)JSON.deserialize(contValList, List<Value_Table__c>.class);
                    for(Value_Table__c vt : vtList)
                    {
                        System.debug('vtid = '+vt.Id);
                        If(vt.Id==Null){
                            vt.Contract_Vehicle__c=recordId;
                        }
                    }
                    //we are not using DMLManager beacuse during JSON.deserialize string it gives error and  Error :-  186 unexpected token: '(' js and apex serialise and deserilize string is mismatch
                    upsert vtList;
                }/*else{
                    if(errorMessage == ''){
                        errorMessage = 'You do not have UPDATE permission for Value Table object.\n';
                    }else{
                        errorMessage = 'You do not have UPDATE and DELETE permission for Value Table object.\n';
                    }
                }*/
                
                //newlly added 17-08 
                if(Schema.sObjectType.Value_Table__c.isCreateable() && !Schema.sObjectType.Value_Table__c.isUpdateable()){
                    List<Value_Table__c> vtList = (List<Value_Table__c>)JSON.deserialize(contValList, List<Value_Table__c>.class);
                    List<Value_Table__c> newVtList = new List<Value_Table__c>();
                    for(Value_Table__c vt : vtList)
                    {
                        System.debug('vtid = '+vt.Id);
                        If(vt.Id==Null){
                            vt.Contract_Vehicle__c=recordId;
                            newVtList.add(vt);
                        }
                    }
                    //we are not using DMLManager beacuse during JSON.deserialize string it gives error and  Error :-  186 unexpected token: '(' js and apex serialise and deserilize string is mismatch
                    insert newVtList;
                }
            }
        }
        catch(Exception e)
        {
            // To show error message on user-side(17/08/2018)
            errorMessage += 'Error :- Server Error '+e.getMessage();
            System.debug(' Error :-  '+e.getLineNumber()+' '+e.getMessage());
            // return e.getMessage();
        }
        
        //System.debug(' ************************ '+vtList);
        return errorMessage;
    }
}