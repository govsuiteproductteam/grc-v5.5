public class AFCAPIVEmailTemplateparser {
    private String emailSubject;
    public String taskOrderNumber;
    Private String titledName;
    Private Date proposalDueDate;
    Private Datetime proposalDueDateTime;
    private Id contactId;
    
    public void parse(Messaging.InboundEmail email){
        emailSubject = email.subject;
        if(emailSubject.length() == 0){
            throw new ContractVehicleNotFoundException(MessageConstantController.TASK_ORDER_NOT_FOUND );
        }
        if(emailSubject.startsWith('Fwd:'))
           emailSubject = emailSubject.replace('Fwd:', '');
        if(emailSubject.startsWith('Re:'))
            emailSubject = emailSubject.replace('Re:', '');
        emailSubject = emailSubject.trim();
        if(emailSubject.contains('(RFP)') && emailSubject.contains(':'))
            taskOrderNumber = emailSubject.substringBetween('(RFP)', ':').trim();
        else
            throw new ContractVehicleNotFoundException(MessageConstantController.TASK_ORDER_NOT_FOUND );
            
        titledName = emailSubject;
        createContact(email.plainTextBody);
    }
    
    public Task_Order__c getTaskOrder(String toAddress){ 
        if(taskOrderNumber==null){
            return null;
        }        
        if(Schema.sObjectType.Contract_Vehicle__c.isAccessible() && Schema.sObjectType.Task_Order__c.isAccessible() && Schema.sObjectType.Task_Order__c.isCreateable()){
            Task_Order__c taskOrder = new Task_Order__c();
            List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];        
            if(!conVehicalList.isEmpty()){
                taskOrder.Contract_Vehicle__c = conVehicalList[0].Id;
                taskOrder.Name = taskOrderNumber;//Task Order Number
                if(titledName != null){
                    taskOrder.Task_Order_Title__c = EmailHandlerHelper.checkValidString(titledName , 255);//Task Order Title
                }
                
                /*if(proposalDueDate!=null){            
                    taskOrder.Due_Date__c = proposalDueDate;
                    taskOrder.Proposal_Due_DateTime__c = proposalDueDateTime;
                } */
                
                if(contactId != null)            
                    taskOrder.Contracting_Officer__c = contactId;
                
                taskOrder.Release_Date__c = Date.today();//RFP/TO Release Date
                return taskOrder;
            }else{
                throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
            }
        }
        else{
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
        }
    }
    
    public Task_Order__c updateTaskOrder(Task_Order__c taskOrder, String toAddress){    
        if(Schema.sObjectType.Contract_Vehicle__c.isAccessible() && Schema.sObjectType.Task_Order__c.isAccessible() && Schema.sObjectType.Task_Order__c.isCreateable()){    
            List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];
           
            if(!conVehicalList.isEmpty()){
                taskOrder.Contract_Vehicle__c = conVehicalList[0].Id;
                // taskOrder.Name = taskOrderNumber;//Task Order Number
                System.debug('titledName  = '+titledName );
                if(titledName != null){
                    if(titledName.trim().startsWithIgnoreCase('Amendment')){
                        titledName = titledName.subStringAfter('to').trim();
                    }
                    taskOrder.Task_Order_Title__c = EmailHandlerHelper.checkValidString(titledName , 255);//Task Order Title
                }
                // taskOrder.Task_Order_Title__c = checkValidString(titledName , 255);//Task Order Title
                /*if(proposalDueDate!=null){            
                    taskOrder.Due_Date__c = proposalDueDate;
                    taskOrder.Proposal_Due_DateTime__c = proposalDueDateTime;
                } */
                
                //taskOrder.Release_Date__c = Date.today();//RFP/TO Release Date
                if(contactId != null)            
                    taskOrder.Contracting_Officer__c = contactId;
                return taskOrder;
            }else{
                system.debug('##################################');
                throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
                
            }
        }
        else{
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
        }
    }
    
    private void createContact(String contactInfo){
        try{
            Contact contact = new Contact();
            contact.Is_FedTom_Contact__c = true;
            if(contactInfo != null){
                if(contactInfo.contains('Thank you,')){
                    contactInfo = contactInfo.substringAfter('Thank you,');
                }
                contactInfo = contactInfo.trim();
                contactInfo = contactInfo.trim().replaceAll('\n', ',');
                System.debug('contactInfo '+contactInfo);
                List<String> contactDetails = contactInfo.split(',');
                String email;
                if(contactDetails.get(contactDetails.size()-1).contains('@'))
                    email = contactDetails.get(contactDetails.size()-1);
                List<Contact> conOfficerList;
                if(Schema.sObjectType.Contact.isAccessible() ){    
                    if(email!=null)
                    conOfficerList = [SELECT id , name FROM Contact Where email =:email];
                    if(conOfficerList != null && !conOfficerList.isEmpty() ){
                        System.debug('if = '+conOfficerList[0].id);
                        contactId = conOfficerList[0].id;               
                    }else{
                        Contact con = new Contact();
                        String lastName = contactDetails.get(0);
                        if(lastName!= null && lastName != ''){
                            con.LastName = lastName;
                            con.Email = email;
                            if(conOfficerList.size()>1)
                                con.Title = contactDetails[1];
                            con.Is_FedTom_Contact__c = true;
                            DMLManager.insertAsUser(con);    
                            System.debug('new Contact = '+con.id);
                            contactId = con.id;               
                        }
                    }
                }
                
           }
        }catch(Exception e){
            
        }
     
    }
    
}