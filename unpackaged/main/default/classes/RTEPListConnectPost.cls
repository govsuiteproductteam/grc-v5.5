@RestResource(urlMapping='/RTEPConnectPost/*')
global with sharing class RTEPListConnectPost {
    @HttpPost
    global static String doPost(){
        String urls = '';
        try{            
            RestRequest req = RestContext.request;
            String htmlBody = req.requestBody.toString();
            System.debug('Hi This is Body = '+htmlBody);                        
            //RTEPListParser parser = new RTEPListParser();
            urls = RTEPListParser.parse(htmlBody);
            urls = urls.replaceAll('amp;', '');
            System.debug('urls = '+urls);
        }
        catch(Exception e){
            
        }
        return urls;
    } 
}