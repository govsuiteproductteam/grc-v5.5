public with sharing class GetDistributionDataWrap {
 @AuraEnabled
        public Distribution__c disRecord;
    @AuraEnabled
        public string checkEdit = '';
        
        public GetDistributionDataWrap(Distribution__c disRec1 , string check)
        {
            disRecord=disRec1;
            checkEdit=check;
            //System.debug('disRecord '+disRecord);
        }
}