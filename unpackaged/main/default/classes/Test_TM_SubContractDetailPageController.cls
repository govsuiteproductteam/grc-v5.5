@IsTest
public class Test_TM_SubContractDetailPageController {
    Public static testMethod void testContractDetailPageController(){
        Test.startTest();
        List<RecordType> rtypes = [Select Name, Id From RecordType where sObjectType='TM_TOMA__SubContract__c' and isActive=true];
        string recId='';
        if(rtypes.size()>0)
        {
            recId=rtypes[0].Id;
        }
        
        CLM_AtaGlanceField__c atAGlance = TestDataGenerator.createAtaGlanceField(recId);
        atAGlance.Is_Contract__c = False;
        atAGlance.Is_Component__c = False;
        insert atAGlance;
        CLM_Tab__c tabSetting =TestDataGenerator.createCLMTab(recId);
        tabSetting.TabName__c='Internal Organisation';
        tabSetting.Is_Contract__c = false;
        insert tabSetting;  
        CLM_Tab__c tabSetting1 = TestDataGenerator.createCLMTab(recId);
        tabSetting1.Is_Contract__c = false;
        tabSetting1.Order__c = 2;
        tabSetting1.TabName__c='Contract Description';
        insert tabSetting1;   
        CLM_Section__c sectionSetting = TestDataGenerator.createCLMSection(recId);
        sectionSetting.Is_Contract__c = false;
        insert sectionSetting;
        CLM_Section__c sectionSetting1 = TestDataGenerator.createCLMSection(recId);
        sectionSetting1.Is_Contract__c = false;
        sectionSetting1.No_Of_Column__c = null;
        sectionSetting1.Order__c = 20;
        sectionSetting1.Is_Lightning_Component__c= true;
        sectionSetting1.Inline_Api_Name_Component_Api__c='TestComponent';
        insert sectionSetting1;
        CLM_Section__c sectionSetting2 = TestDataGenerator.createCLMSection(recId);
        sectionSetting2.Is_Contract__c = false;
        sectionSetting2.Order__c = 30;
        sectionSetting2.Is_InlineVF_Page__c= true;
        sectionSetting2.Inline_Api_Name_Component_Api__c='TestVFPage';
        insert sectionSetting2;
        CLM_FIeld__c fieldSetting = TestDataGenerator.createCLMField(recId);
        fieldSetting.Is_Contract__c = false;
        fieldSetting.ViewFieldApi__c='';
        fieldSetting.Required__c = true;
        fieldSetting.Required_Error_Message__c='Enter value for field';
        fieldSetting.Order__c = 1001;
        insert fieldSetting;
        CLM_FIeld__c fieldSetting1 = TestDataGenerator.createCLMField(recId);
        fieldSetting1.Is_Contract__c = false;
        fieldSetting1.FieldApiName__c = 'OwnerId';
        fieldSetting1.ViewFieldApi__c='OwnerId';
        fieldSetting1.Order__c = 1002;
        insert fieldSetting1;
        CLM_FIeld__c fieldSetting2 = TestDataGenerator.createCLMField(recId);
        fieldSetting2.Is_Contract__c = false;
        fieldSetting2.FieldApiName__c = 'RecordTypeId';
        fieldSetting2.ViewFieldApi__c='RecordTypeId';
        fieldSetting2.Order__c = 1003;
        insert fieldSetting2;
                
        SubContract__c conVeh = TestDataGenerator.createSubContract('12435');
        conVeh.recordTypeId = recId;
        insert conVeh;
        
        PageReference pageRef = Page.TM_SubContractDetailPage;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id', conVeh.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(conVeh);
        TM_SubContractDetailPageController subContractDetailPageController = new TM_SubContractDetailPageController(sc);
        System.assertEquals(subContractDetailPageController.contractVehicle.Sub_Contract_Number__c, '12435');
        System.assert(subContractDetailPageController.tabWrapperList.size()>0); 
        subContractDetailPageController.changeRecordType();
        subContractDetailPageController.changeOwner();
        subContractDetailPageController.chandeMode();
        System.assertEquals(subContractDetailPageController.mode, 'edit');
        subContractDetailPageController.cancelEdit();
        
        subContractDetailPageController.saveContract1();
        System.assertEquals(subContractDetailPageController.contractVehicle.Subcontract_Status_Backend__c,'Draft');
        subContractDetailPageController.saveAndActivate();
        System.assertEquals(subContractDetailPageController.contractVehicle.Subcontract_Status_Backend__c,'Final');
        //code to cover catch block code     
        subContractDetailPageController.contractVehicle.Sub_Contract_Amount__c = 123123123123121233.123123123;
        subContractDetailPageController.saveContract1();
        System.assertEquals(subContractDetailPageController.mode, 'edit');
        subContractDetailPageController.contractVehicle.Sub_Contract_Amount__c = 123123123123121233.123123123;
        subContractDetailPageController.saveAndActivate();
        subContractDetailPageController.changeOwnerCancel();
        subContractDetailPageController.contractVehicle.ownerId = null;
        subContractDetailPageController.changeOwner();
        subContractDetailPageController.changeRecordTypeCancel();
        Test.stopTest();
    }
}