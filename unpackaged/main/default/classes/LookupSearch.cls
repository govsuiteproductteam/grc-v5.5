global with sharing class LookupSearch {
    public LookupSearch(TM_VFLookUpCtrl controller){}
    public LookupSearch(TM_VFLookUpFilterCtrl controller){}
	 webservice static String searchQuery(String objName, String searchTerm){
        /* System.debug('objName :- '+objName+' searchTerm:- '+searchTerm);
         String jsonString1 = '[';
         if(objName.toLowerCase() == 'group')
             objName = 'User';
         
         searchTerm = searchTerm.replace(' ','%');
         String queryString = 'SELECT Id, Name FROM '+objName+' WHERE name LIKE \'%'+searchTerm+'%\' LIMIT 100';
         List<Sobject> sObjList = Database.query(queryString);
         for(Sobject sObj : sObjList){
             String tName = String.escapeSingleQuotes(''+sObj.get('Name'));
             jsonString1 += '{\'id\': \''+sObj.get('Id')+'\', \'label\': \''+tName+'\'},';
         }
        return jsonString1+']';*/
          return '';
    }
    //////////////////////////Akash remote action updates
    
    @RemoteAction
    global static String getLookupRecords(String searchTerm, String sObjectName) {
        List<JsonObj> objList = new List<JsonObj>();
        if(sObjectName.toLowerCase() == 'group')
            sObjectName = 'User';
        
        sObjectName = String.escapeSingleQuotes(sObjectName);
        searchTerm = String.escapeSingleQuotes(searchTerm);
        //String validFields = FLSController.getQueryWithFLS('Id, Name', sObjectName);
        String validFields;
        String whareConFieldName;
        if(sObjectName.toLowerCase()!='contract'){
            validFields = 'Id, Name';
            whareConFieldName = 'Name';
        }else{
            validFields = 'Id, Name, ContractNumber';
            whareConFieldName = 'ContractNumber';
        }
            SObjectType objToken = Schema.getGlobalDescribe().get(sObjectName); 
            DescribeSObjectResult objDef = objToken.getDescribe();
            if(objDef.isAccessible()){
                String queryString;
                System.debug('searchTerm  @@@@@@@@@@ '+searchTerm);
                if(searchTerm != '')
                queryString = 'SELECT '+validFields +' FROM '+sObjectName+' WHERE '+whareConFieldName+' LIKE \'%'+searchTerm+'%\' LIMIT 10';
                else
                queryString = 'SELECT '+validFields +' FROM '+sObjectName+' WHERE LastViewedDate !=null ORDER BY LastViewedDate DESC limit 10';

                List<Sobject> sobjects = Database.query(queryString);
                
                for(Sobject sObj : sobjects){
                    JsonObj jsObj;
                    if(sObjectName.toLowerCase()!='contract')
                    	jsObj = new JsonObj((String)sObj.get('Name'),(String)sObj.get('Id'));
                    else
                        jsObj = new JsonObj((String)sObj.get('ContractNumber'),(String)sObj.get('Id'));
                    objList.add(jsObj);
                }
            }
        String outputJSONString = JSON.serialize(objList);
        outputJSONString = outputJSONString.replaceAll('"','DOUBLEQUOTES');
        return outputJSONString;
    }
    
    
    @RemoteAction
    global  static string checkConditionValue1(String objrecordId) {
        System.debug('objrecordId '+objrecordId);
        string currentrecordId = objrecordId;
        if(currentrecordId != Null && currentrecordId!=''){
            Id temp = (Id)currentrecordId;
            
            Schema.SObjectType sobjectType = temp.getSObjectType();
            String sobjectName = sobjectType.getDescribe().getName();
            System.debug('SObjectType '+SObjectType);
            System.debug('sobjectName '+sobjectName);
            String queryString = 'SELECT Id, Name FROM '+sobjectName+' WHERE Id=\''+currentrecordId+'\' LIMIT 1';
            List<Sobject> sObj = Database.query(queryString);
            if(!sObj.isEmpty())
            {
               return ''+(sObj[0].get('Name'));
                // searchResultList.add(new SearchResult((String)sObj[0].get('Name'), (String)sObj[0].get('Id')));
            }
            else
               return '';
            
        }else{
            return '';
        }
        //System.debug('recordIdName '+recordIdName);
        //return recordIdName;
    }
 
    
    public class JsonObj{
        public String id{get;set;}
        public String label{get;set;}
        
        public JsonObj(String label, String Id){
            this.id= id;
            this.label = label;
        }
    }
}