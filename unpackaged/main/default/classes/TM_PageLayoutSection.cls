public class TM_PageLayoutSection {
    private TM_PageLayoutController plController;
    
    public transient String sectionName{get;set;}
    public transient Integer tabSequence{get;set;}
    public transient Integer secSequenceUpdate{get;set;}
    public transient Integer noOfClm{get;set;}
    public transient String sectionColor{get;set;}
    //Delete Veriables
    public transient Integer tabSequenceDel{get;set;}
    public transient Integer secSequenceDel{get;set;}
    
    //Related List veriable    
    /*public transient String relatedLstObjApiName{get;set;}
    public transient String relatedLstLabel{get;set;}
    public transient String relatedLstApiName{get;set;}
    public transient Boolean showSubmitApproval{get;set;}*/
        
	public TM_PageLayoutSection(TM_PageLayoutController plController){
        this.plController = plController;
    }
    
    public void addSection(){
        if(sectionName!=Null && sectionName!='' && tabSequence!=Null){
            if(secSequenceUpdate!=Null && secSequenceUpdate>0){
                plController.tabLanding = tabSequence;
                tabSequence -= 1;
                secSequenceUpdate -= 1;
                try{
                    if(plController.tabWrapperList != Null){
                        TM_PageLayoutController.SectionWrapper tempSecWrap = plController.tabWrapperList[tabSequence].sectionWrapperList[secSequenceUpdate];
                        tempSecWrap.vfCustomSection.SectionName__c = sectionName;
                        tempSecWrap.vfCustomSection.No_Of_Column__c = (noOfClm ==Null || noOfClm<=0)?2:noOfClm;
                        tempSecWrap.vfCustomSection.BackgroundColor__c = sectionColor;
                    }
                }catch(Exception e){}
                
            }else{
                if(noOfClm ==Null || noOfClm<=0)
                    noOfClm=2;
                CLM_Section__c sec = new CLM_Section__c(SectionName__c = sectionName, No_Of_Column__c = noOfClm,BackgroundColor__c = sectionColor);
                plController.addSection(sec, tabSequence);
            }
        }
    }
    
    
    /*public void addRelatedLst(){
        if(relatedLstLabel!=Null && relatedLstApiName!='' && relatedLstObjApiName!=Null){
            if(secSequenceUpdate!=Null && secSequenceUpdate>0){
                secSequenceUpdate -= 1;
                try{
                    if(plController.RelatedLstWrapperList != Null){
                        TM_PageLayoutController.RelatedLstWrapper temprelatedLstWrap = plController.RelatedLstWrapperList[secSequenceUpdate];
                        temprelatedLstWrap.relatedLst.RelatedList_Label__c = relatedLstLabel;
                        temprelatedLstWrap.relatedLst.RelatedList_Api_Name__c = relatedLstApiName;
                        temprelatedLstWrap.relatedLst.ObjectAPIName__c = relatedLstObjApiName;
                        temprelatedLstWrap.relatedLst.ShowSubmitApprovalButton__c = showSubmitApproval;
                    }
                }catch(Exception e){}
                
            }else{
                TM_RelatedListSettingForOpp__c relatedLstTemp = new TM_RelatedListSettingForOpp__c(RelatedList_Label__c = relatedLstLabel, RelatedList_Api_Name__c = relatedLstApiName, ObjectAPIName__c = relatedLstObjApiName,ShowSubmitApprovalButton__c = showSubmitApproval);
                plController.addRelatedLst(relatedLstTemp);
            }
        }
    }*/
    
    
    public void deleteSection(){
        if(tabSequenceDel != Null && secSequenceDel != Null){
            plController.deleteSection(tabSequenceDel, secSequenceDel);        	
        }
    }
    
    /*public void deleteRelatedLst(){
        if(secSequenceDel != Null){
            plController.deleteRelatedLst(secSequenceDel);        	
        }
    }*/
    
    //Section Rearrange
     public void SecRearrange(){
         if(tabSequence!=Null && tabSequence>0){
            tabSequence -= 1;
            plController.tabRearrangeList.clear();
            try{
                for(TM_PageLayoutController.SectionWrapper tempSecWrap : plController.tabWrapperList[tabSequence].sectionWrapperList){
                    plController.tabRearrangeList.add(tempSecWrap.vfCustomSection.SectionName__c);
                }
            }catch(Exception e){}
         }
    }
    
    /*public void relatedLstRearrange(){
        plController.tabRearrangeList.clear();
           
        for(TM_PageLayoutController.RelatedLstWrapper tempRelatedLstWrap : plController.RelatedLstWrapperList){
            plController.tabRearrangeList.add(tempRelatedLstWrap.relatedLst.RelatedList_Label__c);
        }
    }
    
    
    public void applyrelatedLstRearrange(){
        if(plController.tabRearrangeString != Null){
            
            Integer i=1;
            try{
                for(String reRelatedName : plController.tabRearrangeString.split(',')){
                    if(reRelatedName!=''){
                        if(reRelatedName.contains('&amp;')){
                            reRelatedName.replace('&amp;','&');
                        }
                        for(TM_PageLayoutController.RelatedLstWrapper tempRelatedLstWrap : plController.RelatedLstWrapperList){
                            if(tempRelatedLstWrap.relatedLst.RelatedList_Label__c.replace(' ','')+''+tempRelatedLstWrap.relatedLstOrder == reRelatedName.replace(' ','')){
                                tempRelatedLstWrap.relatedLstOrder = i;
                                break;
                            }
                        }
                        i++;
                    }                
                }
                plController.RelatedLstWrapperList.sort();
                plController.tabRearrangeList.clear();
            }catch(Exception e){}
        }
    }*/
    
    
    public void applySecRearrange(){
        if(plController.tabRearrangeString != Null && tabSequence!=Null && tabSequence>0){
            plController.tabLanding = tabSequence;
            tabSequence -= 1;
            Integer i=1;
            try{
                for(String reSecName : plController.tabRearrangeString.split('SPLITXCOMMA')){//newlly added split('SPLITXCOMMA') in place of split(',') 17-05-2018
                    if(reSecName!=''){
                        if(reSecName.contains('&amp;')){
                            reSecName.replace('&amp;','&');
                        }
                        for(TM_PageLayoutController.SectionWrapper rsecWrap : plController.tabWrapperList[tabSequence].sectionWrapperList){                           
                            if(rsecWrap.vfCustomSection.SectionName__c.replace(' ','') +''+rsecWrap.sectionOrder == reSecName.replace(' ','')){
                                rsecWrap.sectionOrder = i;
                                break;
                            }
                        }
                        i++;
                    }                
                }
                plController.tabWrapperList[tabSequence].sectionWrapperList.sort();
                plController.tabRearrangeList.clear();
            }catch(Exception e){}
        }
    }
}