@isTest
public with sharing class TM_VehiclePartnerPageControllerTest{
    public static TestMethod void TestTM_VehiclePartnerPageController(){
        List<Profile> profileList = [SELECT Id FROM Profile where (Name = 'Custom Partner Community User' OR Name = 'Custom Partner Community Login User' OR Name  = 'Custom Customer Community Plus Login User') Limit 1];
        if(profileList != null && profileList.size()>0){
            
         
            Account acc = TestDataGenerator.createAccount('TaskOrderCapture');
            insert acc;
            Account acc1 = TestDataGenerator.createAccount('TaskOrder');
            insert acc1;
            
            List<Contact> conList = new List<Contact>();
            Contact con = TestDataGenerator.createContact('roy',acc);
            conList.add(con);
            Contact con1 = TestDataGenerator.createContact('joy',acc);
            conList.add(con1);
            Contact con2 = TestDataGenerator.createContact('lay',acc1);
            conList.add(con2);
            insert conList;
            
            User usr = TestDataGenerator.createUser(con,TRUE,'4test1', 'atest1','4test1@gmail.com','4taskordercapture@demo.com','atest1',profileList[0],'America/Los_Angeles','en_US','UTF-8','en_US');
            insert usr;    
            
            Contract_Vehicle__c contractVehicle = TestDataGenerator.createContractVehicle('TMconVehicle',acc1);
            insert contractVehicle;
          
            Contract_Vehicle_Contact__c contVehicleContact = TestDataGenerator.createContractVehicleContact(con2 , contractVehicle );
            insert contVehicleContact;
           
            Vehicle_Partner__c vehiclePartner = TestDataGenerator.createVehiclePartner(acc,contractVehicle);
            insert vehiclePartner;
            
            Test.startTest();
            ApexPages.CurrentPage().getparameters().put('cid',contractVehicle.id);
            ApexPages.StandardController controller = new ApexPages.StandardController(contractVehicle);
            TM_VehiclePartnerPageController vehiclePartnerController = new TM_VehiclePartnerPageController(controller);
            
            
            TM_VehiclePartnerPageController.ContactWrapper conWra = new TM_VehiclePartnerPageController.ContactWrapper(con);
            conWra.conStatus = TRUE;
            conWra.con = con;
            system.assertEquals(vehiclePartnerController.vehiclePartnerWraList.size(),1);
            
            vehiclePartnerController.vehiclePartnerWraList[0].Status = TRUE;
            vehiclePartnerController.ShowContact(); 
            system.assertEquals(vehiclePartnerController.conWraList.size(), 2);
            
            vehiclePartnerController.conWraList.get(0).conStatus = true;
            PageReference pgRef = vehiclePartnerController.addPartner(); 
            System.assertNotEquals(pgRef, null);
            
            Test.stopTest();
         }
    }
}