({
    render : function(component, helper) {
        var ret = this.superRender();
        console.log('renderer RecTypeId 1- '+component.get("v.pageReference").state.recordTypeId);
        console.log('renderer RecTypeId 2- '+component.get("v.recordTypeId"));
        
        return ret;
    },
    rerender : function(component, helper){
        this.superRerender();
        console.log('re-Render New recordTypeId'+component.get("v.pageReference").state.recordTypeId);
        console.log('re-Render old RecordTypeId - '+component.get("v.recordTypeId"));
        var AccessStatus=component.get("v.AccessStatus");
        var doNotrender=component.get("v.doNotrender");
        doNotrender+=1;
        component.set("v.doNotrender",doNotrender);
        
       // alert("AccessStatus "+AccessStatus);
      // alert("rerender > "+component.get("v.doNotrender"));
        if(doNotrender > 2){
            
            
            if(AccessStatus==='FedTOM'){
                var recordTypeId = component.get("v.pageReference").state.recordTypeId;
                //alert(recordTypeId);
                var createRecordEvent = $A.get("e.force:createRecord");
                if(recordTypeId!='undefined' && recordTypeId!=undefined){
                    // alert("inside if");
                    createRecordEvent.setParams({
                        "entityApiName": "TM_TOMA__Subcontract__c",
                        "recordTypeId": ''+recordTypeId
                    });
                }else{
                    //alert("inside else");
                    createRecordEvent.setParams({
                        "entityApiName": "TM_TOMA__Subcontract__c"
                    });
                }
                
                createRecordEvent.fire(); 
            }
        }
        
        
        var currentRecTypeId = component.get("v.pageReference").state.recordTypeId;
        var oldRecTypeId = component.get("v.recordTypeId");
        // code to resolve platform cache issue
        if(((currentRecTypeId != null && currentRecTypeId != undefined) && (oldRecTypeId != null && oldRecTypeId != undefined)) && currentRecTypeId !=oldRecTypeId){
            component.set("v.currentTabId","NONE");
            component.set("v.showSpinner",true);
            helper.helperDoInit(component, event, helper);
        }
    }
})