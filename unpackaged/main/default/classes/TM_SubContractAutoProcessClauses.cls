global class TM_SubContractAutoProcessClauses {
    @InvocableMethod(label='Create SubContract Clauses' description='Create SubContract Clauses.') 
    global static void copyClausesToSubContract(list<Id> subContractIdList){
        if(subContractIdList.size() >0)
        {
            Map<Id,Set<Id>> conIdSubIdSetMap = new Map<Id,Set<Id>>();//Akash
            map<Id,Id> SubContractIdvscontrctIdMap = new map<Id,Id>();
            List<Id> contractIdList= new List<id>();
            List<SubContract__c> tempSubContractList = [select id,CSC_Contract__c,CSC_Contract__r.Are_there_any_Center_Clauses__c,CSC_Contract__r.Are_there_any_Contract_specific_clauses__c,CSC_Contract__r.Are_there_any_Department_clauses__c,CSC_Contract__r.Are_there_any_FAR_clauses__c,CSC_Contract__r.Are_there_any_agency_specific_clauses__c,CSC_Contract__r.Synopsis_of_specific_H_clauses__c,CSC_Contract__r.Contract_specific_H_clauses__c from SubContract__c where id in:subContractIdList];
            List<SubContract__c> subContractList= new List<SubContract__c>();
            for(SubContract__c subCon : tempSubContractList)
            {
                if(subCon.CSC_Contract__c != null )
                {
                    contractIdList.add(subCon.CSC_Contract__c);
                    SubContractIdvscontrctIdMap.put(subCon.Id,subCon.CSC_Contract__c);
                    Set<Id> tempIdSet = conIdSubIdSetMap.get(subCon.CSC_Contract__c);//Akash Start
                    if(tempIdSet != Null){
                        tempIdSet.add(subCon.Id);
                    }else{
                        conIdSubIdSetMap.put(subCon.CSC_Contract__c, new Set<Id>{subCon.Id});
                    }//Akash ends
                    subCon.Are_there_any_Center_Clauses__c = subCon.CSC_Contract__r.Are_there_any_Center_Clauses__c;
                    subCon.Are_there_any_Contract_specific_clauses__c = subCon.CSC_Contract__r.Are_there_any_Contract_specific_clauses__c;
                    subCon.Are_there_any_Department_Clauses__c = subCon.CSC_Contract__r.Are_there_any_Department_clauses__c;
                    subCon.Are_there_any_FAR_clauses__c = subCon.CSC_Contract__r.Are_there_any_FAR_clauses__c;
                    
                    subCon.Are_there_any_Agency_Supplement_clauses__c = subCon.CSC_Contract__r.Are_there_any_agency_specific_clauses__c;
                    subCon.Synopsis_of_specific_H_clauses__c = subCon.CSC_Contract__r.Synopsis_of_specific_H_clauses__c;
                    subCon.Contract_specific_H_clauses__c = subCon.CSC_Contract__r.Contract_specific_H_clauses__c;
                    subContractList.add(subCon);
                }
            }
            if(subContractList.size()>0)
            {
                update subContractList;
            }
            
            
            // List<Contract_Vehicle__c> tempContractList = [select id,(SELECT Id,Contract_Vehicle__c,Clauses__c FROM Contract_Clauses__r)from Contract_Vehicle__c where id in:contractIdList];
            
            List<Contract_Clauses__c> tempContractClausesList = [SELECT Id,Contract_Vehicle__c,Clauses__c FROM Contract_Clauses__c where Contract_Vehicle__c in:contractIdList];
            List<Contract_Clauses__c> subConClauList=new List<Contract_Clauses__c>();
            
            /*for (Id subConVehId : SubContractIdvscontrctIdMap.keySet())
            {
                if(tempContractClausesList.size()>0)
                {
                    for(Contract_Clauses__c conCls : tempContractClausesList)
                    {
                        Id clausConVehId = SubContractIdvscontrctIdMap.get(subConVehId);
                        if(clausConVehId == conCls.Contract_Vehicle__r.id) 
                        {
                            SubContract_Clauses__c tempSubConCla = new SubContract_Clauses__c();
                            tempSubConCla.Clauses__c=conCls.Clauses__c ;
                            tempSubConCla.SubContract__c=subConVehId;
                            subConClauList.add(tempSubConCla);
                        }
                    }
                }
            }*/
            
            for(Contract_Clauses__c conClos : tempContractClausesList){
                Set<Id> subContractIdSet = conIdSubIdSetMap.get(conClos.Contract_Vehicle__c);
                if(subContractIdSet != Null){
                    for(Id subConId : subContractIdSet){
                        Contract_Clauses__c tempSubConCla = new Contract_Clauses__c();
                        tempSubConCla.Clauses__c=conClos.Clauses__c ;
                        tempSubConCla.SubContract__c=subConId;
                        subConClauList.add(tempSubConCla);
                    }
                }
            }
            try{
                if(subConClauList.size()>0){
                    insert subConClauList;
                } 
            }
            catch(Exception e)
            {
                System.debug('Error:- '+e);
            }
        }
        
    }
}