({
    doInit: function(component, event, helper) {
       var atglanceWrap= component.get("v.objetFields");
       console.log("atglanceWrap > "+JSON.stringify(atglanceWrap));
       var fieldWithLabel=atglanceWrap["atglanceList"];
         var fieldacceccMap=atglanceWrap["fieldacceccMap"];//25-10-2018
        
        console.log("fieldWithLabel > "+JSON.stringify(fieldacceccMap));
        var cust=[];
        const map = new Map();
       
        for(var i  in fieldWithLabel){

            if(!fieldWithLabel[i]["TM_TOMA__Is_Component__c"]){
                
                if(fieldacceccMap[fieldWithLabel[i]["TM_TOMA__FieldApiName__c"]]==="True" ){
                    console.log("accesible Fields > "+fieldWithLabel[i]["TM_TOMA__FieldApiName__c"]);
                    // var vc=fieldWithLabel[i].split(":");
                    var vc=fieldWithLabel[i]["TM_TOMA__FieldApiName__c"];
                    //console.log("vc > "+vc);
                    if(''+vc.endsWith("Id")){
                        vc =''+vc.replace('Id','.Name');
                    }
                    map.set(''+vc.trim(),''+fieldWithLabel[i]["TM_TOMA__FieldLabel__c"]);
                    cust.push(''+vc.trim());
                }
             
            }
        }
        component.set("v.formattedFields",cust);
        component.set("v.dataLabelfields",map);
        //console.log("formattedFields > "+formattedFields.length);
    },
    handleRecordUpdated: function(component, event, helper) {
        console.log("inside handleRecordUpdated");
        var kvArray = new Map([['1', '1'], ['2', '2'],['3', '3'], ['4', '4'],['5', '5'], ['6', '6'],['7', '7'], ['8', '8']
                      ,['9', '9'], ['10', '10'],['11', '11'], ['12', '12'],['13', '1'], ['14', '2'],['15', '3'], ['16', '4']
                      ,['17', '5'], ['18', '6'],['19', '7'], ['20', '8'],['21', '9'], ['22', '10'],['23', '11'], ['24', '12']]);
        var atglanceWrap= component.get("v.objetFields")
         var fieldWithLabel=atglanceWrap["atglanceList"];
        var fieldMap=atglanceWrap["fieldMap"];
       // var fieldacceccMap=atglanceWrap["fieldacceccMap"];//25-10-2018
        var oppOject=component.get("v.simpleRecord");
        var fieldsApi=component.get("v.formattedFields");
        var datafields=component.get("v.datafields");
        var fieldLabelMap=component.get("v.dataLabelfields");
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
            datafields = [];
            var nonValue="";
         
            for(var i  in fieldWithLabel){
                //console.log("Color__c > "+fieldWithLabel[i]["Color__c"]+"  ColSpan__c > "+fieldWithLabel[i]["ColSpan__c"]+"  Document_Id__c > "+fieldWithLabel[i]["Document_Id__c"]+"  FieldApiName__c > "+fieldWithLabel[i]["FieldApiName__c"]+"  FieldLabel__c > "+fieldWithLabel[i]["FieldLabel__c"]+"  Is_Component__c > "+fieldWithLabel[i]["Is_Component__c"]+"  Order__c > "+fieldWithLabel[i]["Order__c"]);
                if(fieldWithLabel[i]["TM_TOMA__Is_Component__c"]){
                     var fieldLabelvalueAPI=fieldWithLabel[i]["TM_TOMA__FieldApiName__c"];
                    if(fieldLabelvalueAPI.startsWith("c:") ||  fieldLabelvalueAPI.startsWith("TM_TOMA:")){
                        
                        $A.createComponent(fieldWithLabel[i]["TM_TOMA__FieldApiName__c"], {
                            recordId : component.get("v.recordId")
                        }, function attachModal(modalCmp, status) {
                            //var container = component.find("container");
                            if (component.isValid() && status === 'SUCCESS') {
                                var body = component.get("v.body");
                                body.push(modalCmp);
                                component.set("v.body", body);    
                            }
                        });
                    }
                }else{
                    var i1=fieldWithLabel[i]["TM_TOMA__FieldApiName__c"];
                      //var isAccesible=fieldacceccMap.get(i1);
                    //console.log("isAccesible > "+isAccesible);
                    var v='';
                    var value1='';
                    var fieldcolr=fieldWithLabel[i]["TM_TOMA__Color__c"];
                    var fieldColspan=fieldWithLabel[i]["TM_TOMA__ColSpan__c"];
                    var colspanCss="";
                    var fieldImg=fieldWithLabel[i]["TM_TOMA__Document_Id__c"];
                    
                    if(fieldColspan===undefined  || fieldColspan==="undefined" || fieldColspan===null ){
                        //colspanCss="slds-col--padded slds-size--12-of-12";
                        colspanCss="slds-col--padded slds-size--12-of-12 slds-medium-size--2-of-12 slds-large-size--2-of-12";
                    }else{
                       // colspanCss="slds-col--padded slds-size--12-of-12 slds-medium-size--"+fieldColspan+"-of-12 large-size--"+fieldColspan+"-of-12";
                       colspanCss= "slds-col--padded slds-col--padded slds-size--12-of-12 slds-medium-size--"+fieldColspan+"-of-12 slds-large-size--"+fieldColspan+"-of-12";
                    }
                   // console.log("colspanCss > "+colspanCss);
                    if(fieldcolr===undefined  || fieldcolr==="undefined"  ){
                        fieldcolr='#131212';
                    }
                  
                    
                    if(i1.endsWith("Id")===true){
                        i1 =''+i1.replace('Id','.Name');
                        v=i1.split('.');
                        if(oppOject[''+v[0]]!=null){
                           value1=oppOject[''+v[0]][''+v[1]]; 
                        }else{
                            value1=''; 
                        }
                        
                        if(value1===undefined || value1==='undefined'){
                           value1=''; 
                        }
                       // console.log(  fieldWithLabel[i]["FieldLabel__c"]+" > inside if component >  "+value1);
                    }else{
                       // console.log("field Type > "+fieldMap[''+i1]);
                        if(fieldMap[''+i1]==="Date"){
                            if(oppOject[''+i1]!=null){
                                /*var d = new Date(oppOject[''+i1]);
                                value1= d.toDateString();
                                value1=value1.substr(3,(value1.length-1));*/
                                value1=$A.localizationService.formatDate(oppOject[''+i1]);
                            }else{
                                
                                value1='';
                            }
                            
                            //oppOject[''+i1];
                        }else if(fieldMap[''+i1]==="DateTime"){
                            if(oppOject[''+i1]!=null){
                                var timezone = $A.get("$Locale.timezone");
                                var mydate = new Date(oppOject[''+i1]).toLocaleTimeString("en-US", {timeZone: timezone})
                                console.log('Date Instance with Salesforce Locale timezone : '+mydate);
                                console.log("timezone > "+timezone);
                                //var mydatetemp=mydate.split(",");
                                value1=$A.localizationService.formatDate(oppOject[''+i1])+" "+mydate;
                                /*var d = new Date(oppOject[''+i1]);
                                var hour = d.getHours();
                                console.log("hour"+hour);
                                var minute = d.getMinutes();
                                var amPM = (hour > 11) ? "pm" : "am";
                                if(hour > 12) {
                                    hour -= 12;
                                } else if(hour == 0) {
                                    hour = "12";
                                }
                                if(minute < 10) {
                                    minute = "0" + minute;
                                }
                                
                                value1= d.toDateString();
                                value1=value1.substr(3,(value1.length-1));
                                
                                var temptime=d.toISOString();
                                console.log(oppOject[''+i1]+"temptime > "+$A.localizationService.formatDateTime(oppOject[''+i1]));
                                var temp2=temptime.split("T");
                                var temp3=temp2[1];
                                console.log("temp3 > "+temp3);
                                var temp4=temp3.split(":");
                                var hours=temp4[0];
                                var ampm = hours >= 12 ? 'PM' : 'AM';
                                
                                var min=temp4[1];
                                var sec=temp4[2];
                                var setemp=sec.split(".");
                                sec=setemp[0];
                                value1+=" "+hour+ ":"+min+":"+sec+" "+ampm;*/
                            }else{
                                
                                value1='';
                            }
                        }else if(fieldMap[''+i1]==="Currency"){
                            if(oppOject[''+i1]!=null){
                                //var currency = $A.get("$Locale.currency");
                                //console.log("currency > "+currency);
                                value1=''+oppOject[''+i1];
                                var val=parseFloat(value1).toFixed(2);
                                value1=val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                value1="$"+value1;
                            }else{
                                value1='';
                            }
                            
                          
                        }else if(fieldMap[''+i1]==="Reference"){
                          
                            v=i1.split('.');
                            if(oppOject[''+v[0]]!=null){
                                value1=oppOject[''+v[0]][''+v[1]];
                            }else{
                                value1='';  
                            }
                            if(value1===undefined || value1==='undefined'){
                                value1=''; 
                            }
                        }else{
                            if(oppOject[''+i1]!=null){
                            	value1=oppOject[''+i1];     
                            }else{
                                value1=''; 
                            }
                               
                        }
                        
                        if(value1===undefined || value1==='undefined' || value1===null){
                            value1=''; 
                        }
                       // console.log(  fieldWithLabel[i]["FieldLabel__c"]+" > inside else component >  "+value1);
                    }
                    
                    var dynamicFildCMP=[];
                    dynamicFildCMP.push(  ["aura:HTML", {
                        "tag": "div",
                        "HTMLAttributes": {
                            "class" :colspanCss,
                            "style" :"margin-top: 3px;margin-bottom: 8px;"
                        }
                    }]);
                    if(fieldImg!=undefined  && fieldImg!="undefined" ){
                       // console.log("fieldImg > "+fieldImg);
                        dynamicFildCMP.push(  ["aura:HTML", {
                            "tag": "span",
                            "HTMLAttributes": {
                                "class" :"fieldImg",
                                "style" :"display:block;"
                            }
                        }]);
                        dynamicFildCMP.push(  ["aura:HTML", {
                            "tag": "img",
                            "HTMLAttributes": {
                                "src" :"/servlet/servlet.FileDownload?file="+fieldImg
                            }
                        }]);
                    }else{
                        dynamicFildCMP.push(  ["aura:HTML", {
                            "tag": "span",
                            "HTMLAttributes": {
                                "class" :"fieldImg",
                                "style" :"display:none;"
                            }
                        }]);
                        dynamicFildCMP.push(  ["aura:HTML", {
                            "tag": "img",
                            "HTMLAttributes": {
                                "src" :"/servlet/servlet.FileDownload?file="+fieldImg
                            }
                        }]);
                    }
                    dynamicFildCMP.push(["aura:HTML", {
                        "tag": "h1",
                        "HTMLAttributes": {
                            "class":"slds-page-header__title_Custom slds-truncate slds-align-middle" , 
                            "style": "color: "+fieldcolr+";",
                            "title":""+fieldWithLabel[i]["TM_TOMA__FieldLabel__c"]
                        }
                    }]);
                    dynamicFildCMP.push(["ui:outputText", {
                        "value" :""+fieldWithLabel[i]["TM_TOMA__FieldLabel__c"]
                    }]);
                    
                    dynamicFildCMP.push(["aura:HTML", {
                        "tag": "p",
                        "HTMLAttributes": {
                            "class":"slds-page-header__title slds-truncate slds-align-middle" ,
                            "style": "color: "+fieldcolr+";overflow: inherit; word-break: break-all;white-space: normal;font-size: 16px;",
                            "title":""+value1
                        }
                    }]);
                    dynamicFildCMP.push(["ui:outputText", {
                        "value" :""+value1
                    }]);
                    
                    $A.createComponents(dynamicFildCMP,
                        function(components, status, errorMessage){
                           // console.log(JSON.stringify(errorMessage)+"status  "+status);
                            if(status === "SUCCESS"){
                               // console.log("status > "+status+" fieldImg "+fieldWithLabel[i]["Document_Id__c"]);
                                var pageBody = component.get("v.body");
                                //if(fieldImg!=undefined  && fieldImg!="undefined"){
                                //  console.log("divContaint > "+fieldImg);
                                    var div = components[0];
                                    var span = components[1];
                                    var img = components[2];
                                    var h1 = components[3];
                                    var h1Value = components[4];
                                    var p = components[5];
                                    var pValue = components[6];
                                    span.set("v.body",img);
                                     p.set("v.body",pValue);
                                    h1.set("v.body",h1Value);
                                   
                                    var divContaint=[];
                                    divContaint.push(span);
                                    
                                    divContaint.push(h1);
                                    divContaint.push(p);
                                    //console.log("divContaint > "+JSON.stringify(divContaint));
                                    div.set("v.body", divContaint);
                                    pageBody.push(div);
                                    
                                /*}else{
                                  
                                    var div = components[0];
                                    var h1 = components[1];
                                    var h1Value = components[2];
                                    var p = components[3];
                                    var pValue = components[4];
                                    h1.set("v.body",h1Value);
                                    p.set("v.body",pValue);
                                    var divContaint=[];
                                    divContaint.push(h1);
                                    divContaint.push(p);
                                      console.log("divContaint 2 > "+JSON.stringify(divContaint));
                                    div.set("v.body", divContaint);
                                    pageBody.push(div);
                                    
                                }*/
                                
                                
                                if (component.isValid()) {
                                    component.set("v.body", pageBody);
                                }
                            } 
                        }
                    );
                }
                
            }
          
        }else{
            console.log("Problem");
        }
    },
})