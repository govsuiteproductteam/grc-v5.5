@isTest
public class ITES3SEmailHandlerTest {
    public static testmethod void testCase1(){
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Contact con = TestDataGenerator.createContact('TestCon', acc );
        insert con;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('T-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'ites3s@n-24up6xo4e6zmm2l54pv81h07hknkcg9jto27ej1csuchhq081t.37-pvateac.na71.apex.salesforce.com';
        
        insert conVehi;
        
        //for new Task Order Creation  
        Messaging.InboundEmail email  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        email.plainTextBody = 'Dear ECS Federal LLC,\n'+
            'The CHESS IT e-mart has brought you a potential customer! Keep in mind that CHESS customers are awaiting\n'+
            'your response, and your timeliness in submitting a Bid or No Bid would be greatly appreciated. By accessing\n'+
            'the Request for Information (RFI) below, you are acknowledging the receipt of the RFI.\n'+
            'Please respond to the customer&#39;s pending RFI by logging into the RFI Tool.\n'+
            'Your Point of Contact Is:Mr. Jenifer Williams\n'+
            'Email: jenifer.l.williams@usace.army.mil\n'+
            'Phone: 916-557-7086\n'+
            'Organization: ARMY / CORPS OF ENGINEERS\n'+
            'The following outlines the details of this request:\n'+
            'Request for Information ID: 269456\n'+
            'Request for Information Name: USACE Data Repository Cloud Services\n'+
            'Request for Information Due Date: 1/30/2019\n'+
            'Request for Information # of Attachments: 1\n'+
            'Details: The U.S. Army Corps of Engineers (USACE) is seeking a budgetary estimate for market research to migrate\n'+
            'the Enterprise Data Warehouse (EDW)  in two phases (production first, followed by COOP and development) to a\n'+
            'PaaS environment.\n'+ 
            'The purpose of this request is to obtain information regarding the availability, capability, and potential cost\n'+
            'with a rough order of magnitude (ROM) for technical services required to accomplish the tasks with three option\n'+
            'years of support.\n'+
            'This is NOT a solicitation for proposals or proposal abstracts.\n'+
            'Due Date:  Please provide ROM response is NO LATER THAN COB Wednesday, January 30, 2019.\n'+
            'Format: Please provide your narrative response in PDF format. ROM costs may be submitted in either\n'+
            'PDF or Excel format.\n'+
            'See attached PDF for more details.\n'+
            'Agency: ARMY\n'+
            'Activity: CORPS OF ENGINEERS\n'+
            'Installation: CE ENTERPRISE INFORMATION SERVICES\n'+
            '\n'+
            'Thank you for using the CHESS IT e-mart system!\n';
        email.fromAddress ='test@test.com';
        email.fromName = 'ABC XYZ';
        email.subject = 'ITES-3S RFI Created - Reference #269456';
        email.toaddresses = new List<String>();
        email.toaddresses.add('ites3s@n-24up6xo4e6zmm2l54pv81h07hknkcg9jto27ej1csuchhq081t.37-pvateac.na71.apex.salesforce.com');
        
        //For Updating existing task order with existing Contact
        Messaging.InboundEmail email1  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env1 = new Messaging.InboundEnvelope();
        email1.plainTextBody = 
            'Dear ECS Federal LLC,\n'+
            'The CHESS IT e-mart has brought you a potential customer! Keep in mind that CHESS customers are awaiting\n'+
            'your response, and your timeliness in submitting a Bid or No Bid would be greatly appreciated. By accessing\n'+
            'the Request for Information (RFI) below, you are acknowledging the receipt of the RFI.\n'+
            'Please respond to the customer&#39;s pending RFI by logging into the RFI Tool.\n'+
            'Your Point of Contact Is:Jenifer Williams\n'+
            'Email: jenifer.l.williams@usace.army.mil\n'+
            'Phone: 916-557-7086\n'+
            'Organization: ARMY / CORPS OF ENGINEERS\n'+
            'The following outlines the details of this request:\n'+
            'Request for Information ID: 269456\n'+
            'Request for Information Name: USACE Data Repository Cloud Services\n'+
            'Request for Information Due Date: 1/30/2019\n'+
            'Request for Information # of Attachments: 1\n'+
            'Details: The U.S. Army Corps of Engineers (USACE) is seeking a budgetary estimate for market research to migrate\n'+
            'the Enterprise Data Warehouse (EDW)  in two phases (production first, followed by COOP and development) to a\n'+
            'PaaS environment.\n'+ 
            'The purpose of this request is to obtain information regarding the availability, capability, and potential cost\n'+
            'with a rough order of magnitude (ROM) for technical services required to accomplish the tasks with three option\n'+
            'years of support.\n'+
            'This is NOT a solicitation for proposals or proposal abstracts.\n'+
            'Due Date:  Please provide ROM response is NO LATER THAN COB Wednesday, January 30, 2019.\n'+
            'Format: Please provide your narrative response in PDF format. ROM costs may be submitted in either\n'+
            'PDF or Excel format.\n'+
            'See attached PDF for more details.\n'+
            'Agency: ARMY\n'+
            'Activity: CORPS OF ENGINEERS\n'+
            'Installation: CE ENTERPRISE INFORMATION SERVICES\n'+
            '\n'+
            'Thank you for using the CHESS IT e-mart system!\n';
        email1.fromAddress ='test@test.com';
        email1.fromName = 'ABC XYZ';
        email1.subject = 'Subject: ITES-3S RFI Created - Reference #269456';
        email1.toaddresses = new List<String>();
        email1.toaddresses.add('ites3s@n-24up6xo4e6zmm2l54pv81h07hknkcg9jto27ej1csuchhq081t.37-pvateac.na71.apex.salesforce.com');
        
        //For task order Amendment
        Messaging.InboundEmail email2  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env2 = new Messaging.InboundEnvelope();
        email2.plainTextBody = 'Dear ECS Federal LLC,\n'+
            'This email is to inform you that a customer has a recently amended a Request For Information (RFI)\n'+
            'that was previously submitted to you.This amendment has re-opened the RFI. Please respond to\n'+
            'the customer&#39;s RFI for action.\n'+
            'The RFI information is:\n'+
            'RFI ID: 269456\n'+
            'Proposal: The U.S. Army Corps of Engineers (USACE) is seeking a budgetary estimate for market research to\n'+
            'migrate the Enterprise Data Warehouse (EDW)  in two phases (production first, followed by COOP and development)\n'+
            'to a PaaS environment.\n'+
            'The purpose of this request is to obtain information regarding the availability, capability, and potential cost\n'+
            'with a rough order of magnitude (ROM) for technical services required to accomplish the tasks with three option\n'+
            'years of support.\n'+
            'This is NOT a solicitation for proposals or proposal abstracts.\n'+
            'Due Date:  Please provide ROM response is NO LATER THAN COB Friday, February 1, 2019.\n'+
            'Format: Please provide your narrative response in PDF format. ROM costs may be submitted in either\n'+
            'PDF or Excel format.\n'+
            'See attached PDF for more details.\n'+
            'Date Due: 2/1/2019\n'+
            '\nAttachments: 1\n'+
            'Thank you for using the CHESS IT e-mart!\n';
        email2.fromAddress ='test@test.com';
        email2.fromName = 'ABC XYZ';
        email2.subject = 'ITES-3S RFI Amendment - Reference #269456';
        email2.toaddresses = new List<String>();
        email2.toaddresses.add('ites3s@n-24up6xo4e6zmm2l54pv81h07hknkcg9jto27ej1csuchhq081t.37-pvateac.na71.apex.salesforce.com');
        
        //For task order cancellation
        Messaging.InboundEmail email3  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env3 = new Messaging.InboundEnvelope();
        email3.plainTextBody = 'Dear ECS Federal LLC,\n'+
            'This email is to inform you that a customer recently cancelled the Request for Proposal (RFP) below.\n'+
            'Proposal ID: 269456\n'+
            'The reason given for cancellation was: "Change in Requirement".\n'+
            '\n'+
            'Thank you for using the CHESS IT e-mart system!\n';
        email3.fromAddress ='test@test.com';
        email3.fromName = 'ABC XYZ';
        email3.subject = 'ITES-3S RFP Cancellation - Reference #269456';
        email3.toaddresses = new List<String>();
        email3.toaddresses.add('ites3s@n-24up6xo4e6zmm2l54pv81h07hknkcg9jto27ej1csuchhq081t.37-pvateac.na71.apex.salesforce.com');
        
        
        
        ITES3SEmailHandler handler = new ITES3SEmailHandler();
        Test.startTest();
        Messaging.InboundEmailResult result1 = handler.handleInboundEmail(email, env);
        Messaging.InboundEmailResult result4 = handler.handleInboundEmail(email1, env1);
        Messaging.InboundEmailResult result2 = handler.handleInboundEmail(email2, env2);
        
        List<Task_Order__c> taskOrderList = [SELECT Id FROM Task_Order__c];
        List<Contract_Mods__c> contractModList = [SELECT Id FROM Contract_Mods__c];
        System.debug('contractModList : '+contractModList);
        
        Messaging.InboundEmailResult result3 = handler.handleInboundEmail(email3, env3);
        
        
        
        
        Test.stopTest();
        
        System.assertEquals(1, taskOrderList.size());
        System.assertEquals(2,contractModList.size());
    }
}