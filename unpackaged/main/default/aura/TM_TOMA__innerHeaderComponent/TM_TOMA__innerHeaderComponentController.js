({
	creatField : function(component, event, helper) {
		var compoName=component.get("v.componentName");
        var compoBody=component.get("v.componentBody");
       // alert(component.get("v.isComponent"));
        $A.createComponent(compoName,compoBody,function(compoData,status){
            if(status=="SUCCESS"){
                var body=component.get("v.body");
                body.push(compoData);
                component.set("v.body",body);
            }
        });
	}
})