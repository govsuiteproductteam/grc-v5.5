public class C5EmailTemplateParser {
    private String emailSubject;
    public String taskOrderNumber;
    private String taskOrderTitle;
    private String programSummary;
    private String primaryRequirement;
    private String requirement;  
    
    //Private String titledName;
    Private Date proposalDueDate;
    Private Datetime proposalDueDateTime;
    Private Date questionDueDate;
    private Id contactId1;
    private Id contactId2;
    
    public void parse(Messaging.InboundEmail email){
        emailSubject = email.subject;
        if(emailSubject.length() == 0){
            throw new ContractVehicleNotFoundException(MessageConstantController.TASK_ORDER_NOT_FOUND );
        }
        System.debug(''+emailSubject);
        if(emailSubject.startsWith('Fwd:'))
            emailSubject = emailSubject.replace('Fwd:', '');
        if(emailSubject.startsWith('Re:'))
            emailSubject = emailSubject.replace('Re:', '');
        
        if(emailSubject.contains('C5 Request for White Papers - '))
            taskOrderNumber = emailSubject.substringBetween('C5 Request for White Papers - ', ' ').trim();        
        System.debug(''+taskOrderNumber);
        
        taskOrderTitle = emailSubject.substringAfterLast('-').trim();
        System.debug(''+taskOrderTitle);
        
        
        
        String body = email.plainTextBody;        
        System.debug(''+body.stripHtmlTags());
        //programSummary = body.stripHtmlTags().substringBetween('*Background:*', '*Requirement:*').stripHtmlTags();
        programSummary = body.stripHtmlTags().substringBetween('*Background:*', '*Requirement:*');
        System.debug(''+programSummary);
        if(programSummary != null){
            programSummary = programSummary.stripHtmlTags();
        }
        primaryRequirement = body.stripHtmlTags().substringBetween('*Requirement:*','Thank you.');
        System.debug(''+primaryRequirement);
        
        requirement = body.stripHtmlTags().substringBetween('*Requirement:*','The attached RWP');
        System.debug(''+requirement);
        
        
        if(body.contains('received by C5 by')){
            //String rawDate = body.substringBetween('received by C5 by', 'via').normalizeSpace();
            String rawDate = body.substringBetween('received by C5 by', 'via');
            if(rawDate != null){
                rawDate =  rawDate.normalizeSpace();
            }
            rawDate = rawdate.replace('*', '');
            System.debug(''+rawDate);
            DateTimeHelper dtHelper = new DateTimeHelper();
            proposalDueDateTime =  dtHelper.getDateTimeSpecialCase(rawDate);
            //proposalDueDateTime =  dtHelper.getDateTime(rawDate);
            System.debug(''+proposalDueDateTime);
        }
        if(body.contains(' to C5 not later than')){
            String rawDate = body.substringBetween(' to C5 not later than', 'via');
            if(rawDate != null){
                rawDate = rawDate.normalizeSpace();
            }
            System.debug(''+rawDate);
            DateTimeHelper dtHelper = new DateTimeHelper();
            questionDueDate =  dtHelper.getDateTimeSpecialCase(rawDate).date();
            //questionDueDate =  dtHelper.getDateTime(rawDate).date();
            System.debug(''+questionDueDate);
        }
        
        createContact(body);        
    }
    
    public Task_Order__c getTaskOrder(String toAddress){
        if(Schema.sObjectType.Task_Order__c.isAccessible() && Schema.sObjectType.Contract_Vehicle__c.isAccessible() && Schema.sObjectType.Contact.isAccessible()){ 
            if(taskOrderNumber==null){
                return null;
            }        
            Task_Order__c taskOrder = new Task_Order__c();
            List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];        
            if(!conVehicalList.isEmpty()){
                taskOrder.Contract_Vehicle__c = conVehicalList[0].Id;
                taskOrder.Name = taskOrderNumber;//Task Order Number
                
                if(proposalDueDateTime !=null){            
                    taskOrder.Due_Date__c = proposalDueDateTime.date();
                    taskOrder.Proposal_Due_DateTime__c = proposalDueDateTime;
                }
                
                if(programSummary!=Null && programSummary.trim() !='')
                    taskOrder.Description__c = programSummary;
                
                if(primaryRequirement!=Null && primaryRequirement.trim() !='')
                    taskOrder.Primary_Requirement__c  = truncateString(primaryRequirement,255);
                
                // For New field Requirement__c
                // Added by Ramandeep
                if(requirement!=Null && requirement.trim() !='')
                    taskOrder.Requirement__c = truncateString(requirement,10000);
                
                if(taskOrderTitle!=Null && taskOrderTitle.trim() !='')
                    taskOrder.Task_Order_Title__c = taskOrderTitle;
                
                if(questionDueDate!=null)
                    taskOrder.Questions_Due_Date__c = questionDueDate;
                
                if(contactId1 != null)            
                    taskOrder.Contracting_Officer__c = contactId1;
                
                if(contactId2 != null)            
                    taskOrder.Contract_Specialist__c = contactId2;
                
                taskOrder.Release_Date__c = Date.today();//RFP/TO Release Date
                return taskOrder;
            }else{
                throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
            }
        }
        return null;
    }
    
    public Task_Order__c updateTaskOrder(Task_Order__c taskOrder, String toAddress){
        if(Schema.sObjectType.Task_Order__c.isAccessible() && Schema.sObjectType.Contract_Vehicle__c.isAccessible() && Schema.sObjectType.Contact.isAccessible()){ 
            List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];
            
            if(!conVehicalList.isEmpty()){
                taskOrder.Contract_Vehicle__c = conVehicalList[0].Id;
                
                if(proposalDueDateTime !=null){            
                    taskOrder.Due_Date__c = proposalDueDateTime.date();
                    taskOrder.Proposal_Due_DateTime__c = proposalDueDateTime;
                } 
                
                // if(programSummary!=Null && programSummary.trim() !='')
                // taskOrder.Description__c = programSummary;
                
                if(programSummary!=Null && programSummary.trim() !='')
                    taskOrder.Description__c  = truncateString(programSummary,10000);
                
                if(primaryRequirement!=Null && primaryRequirement.trim() !='')
                    taskOrder.Primary_Requirement__c  = truncateString(primaryRequirement,255);
                
                
                // For New field Requirement__c
                // Added by Ramandeep
                if(requirement!=Null && requirement.trim() !='')
                    taskOrder.Requirement__c = truncateString(requirement,10000);
                
                if(taskOrderTitle!=Null && taskOrderTitle.trim() !='')
                    taskOrder.Task_Order_Title__c = taskOrderTitle;
                
                taskOrder.Release_Date__c = Date.today();//RFP/TO Release Date
                if(questionDueDate!=null)
                    taskOrder.Questions_Due_Date__c = questionDueDate;
                
                if(contactId1 != null)            
                    taskOrder.Contracting_Officer__c = contactId1;
                
                if(contactId2 != null)            
                    taskOrder.Contract_Specialist__c = contactId2;
                
                return taskOrder;
            }else{
                system.debug('##################################');
                throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
                
            }
        }
        return taskOrder;
    }
    
    
    private void createContact(String body){
        try{
            List<Contact> contactList = new List<Contact>();
            List<Contact> otherContactList = new List<Contact>();
            body = body.stripHtmlTags();            
            List<String> wordList = body.split(' ');
            
            Integer size = wordList.size();
            for(integer i=0; i<size; i++){
                if(wordList.get(i).contains('.com') && wordList.get(i).contains('@')){                    
                    System.debug('EMAIL ID==> '+wordList.get(i));
                    String email = wordList.get(i).replace('(','');
                    email = email.replace(')','');
                    List<Contact> tempContactList = [SELECT id , name FROM Contact Where email =:email];                    
                    
                    if(tempContactList.isEmpty()){
                        Contact contact = new Contact();
                        contact.Email = email.trim(); 
                        String firstName = wordList.get(i-1).trim();
                        String lastName = '';
                        System.debug(i+'==>FIRST NAME==> '+wordList.get(i-1));
                        if(firstName != null){                      
                            
                            for(integer j=0; j<size; j++){
                                if(wordList.get(j).trim().contains(firstName)){
                                    System.debug('LAST NAME==> '+wordList.get(j+1));
                                    if(wordList.get(j+1)!=null && !wordList.get(j+1).contains('.com')){
                                        lastName = wordList.get(j+1);
                                    }
                                }
                            }
                            System.debug('lastName = '+lastName);
                            contact.LastName = lastName;
                            contact.FirstName = firstName;
                            contact.Is_FedTom_Contact__c = true;
                            contactList.add(contact);
                        }                    
                    }else{
                        otherContactList.add(tempContactList.get(0));
                    }
                    
                }
            }
            DMLManager.insertAsUser(contactList);
            if(!contactList.isEmpty()){
                contactId1 = contactList.get(0).Id;
                if(contactList.size()>1)
                    contactId2 = contactList.get(1).Id;
            }
            if(!otherContactList.isEmpty()){
                contactId1 = otherContactList.get(0).Id;
                if(otherContactList.size()>1)
                    contactId2 = otherContactList.get(1).Id;
            }
        }catch(Exception e){
            System.debug(''+e.getMessage()+'\t: '+e.getLineNumber()); 
        }
        
    }
    
    private string truncateString(String trunString, Integer maxLength){
        if(trunString!=Null){
            if(trunString.length()>=maxLength){
                return trunString.subString(0,maxLength);
            }else{
                return trunString;
            }
            
        }else{
            return Null;
        }
    }
}