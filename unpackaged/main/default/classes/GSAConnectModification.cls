@RestResource(urlMapping='/GSAConnectModification/*')
global with sharing class GSAConnectModification{
    static String statusTemp;
    
    @HttpPost
    global static String doPost()
    {
        statusTemp = '';
        String urls = '';
        String result = '';
        try{
            System.debug('Inside doPost11');
            RestRequest req = RestContext.request;
            
            Blob inputBody = req.requestBody;
            String modDes = inputBody.toString();
           // String EncodingUtil.base64Encode(inputBody );

            Map<String,String> headerMap = req.headers;
            
            System.debug('Hi This is Body = '+inputBody);    
            String modName = headerMap.get('modName');    
            String taskOrderId = headerMap.get('taskOrderId');    
            System.debug('Input Text = '+inputBody.toString());    
            if(modName != null && taskOrderId != null && !modDes.contains('Description of modification made at :')){
                result = GSAModifier.parseModification(inputBody.toString() ,modName ,taskOrderId );
            } 
            else{
                result = 'Empty Data';
            }         
                        
            
        }
        catch(Exception ee){
            system.debug('Exception = '+ee);
            result = 'Failed';
        }
        //String retValue = 'rfqId=RFQ1071729##contractNumber=GS-10F-0101S'; 
        
        return result;
    }
    
    private void createGSAData(String htmlBody){
        
    }
    
}