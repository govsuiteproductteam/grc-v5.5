@isTest
public class Test_TM_BookingTable {
 public static testMethod void testBookingTable(){
        Account account = TestDataGenerator.createAccount('Test Account');
        insert account;
        Contract_Vehicle__c contractVehicle =  TestDataGenerator.createContractVehicle('12345', account);
        insert contractVehicle;
        //TestDataGenerator.crea
        Id recordTypeId = Schema.SObjectType.Value_Table__c.getRecordTypeInfosByName().get('Bookings').getRecordTypeId();
        
        Value_Table__c valueTable1 = TestDataGenerator.createValueTable(recordTypeId,contractVehicle.Id);
        insert valueTable1;
       
        Value_Table__c valueTable2 = TestDataGenerator.createValueTable(recordTypeId,contractVehicle.Id);
        insert valueTable2;
        Id valueTableRecordId = valueTable2.Id;
        
        Test.startTest();
        GetBookingTableData bookingTable = TM_BookingTable.addNewBookingTableRowInWrap(contractVehicle.Id);
        System.assert(bookingTable != null);
        List<Value_Table__c> bookingTableList = new List<Value_Table__c>();
        bookingTableList.add(valueTable1);
        TM_BookingTable.getPickvalIfNotABookingWhy();
        TM_BookingTable.getContractVehical(contractVehicle.Id);
        String bookingTableListStr = JSON.serialize(bookingTableList);
        
        TM_BookingTable.saveBookingTableData(bookingTableListStr, contractVehicle.Id, 'abc,'+valueTableRecordId);
        List<Value_Table__c> valueTableList = [SELECT Id FROM Value_Table__c WHERE id =: valueTableRecordId];
        System.assert(valueTableList.size() == 0);
        List<GetBookingTableData> bookingTableList1 = TM_BookingTable.getBookingValueTableList1(contractVehicle.Id);
        System.assert(bookingTableList1 != null);
        List<String> pickListValuesIfnotBooking = TM_BookingTable.getPickvalIfNotABookingWhy();//Baseline
        System.assert(pickListValuesIfnotBooking!=null);
        Contract_Vehicle__c contVehicle = TM_BookingTable.getContractVehical(contractVehicle.Id);
        System.assert(contVehicle.id == contractVehicle.Id);
     TM_BookingTable.isLightningPage();
     TM_BookingTable.checkLicensePermition1();
        Test.stopTest();
    }
}