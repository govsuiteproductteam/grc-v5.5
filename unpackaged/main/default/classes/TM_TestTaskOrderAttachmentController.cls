@isTest
public with sharing class TM_TestTaskOrderAttachmentController{
    @isTest
    public static void testTaskOrderAttachment(){
        //insert test Contract Vehicle
        Contract_Vehicle__c cv = new Contract_Vehicle__c();
        insert cv;
        
        // test task order
        Task_Order__c taskOrder = new Task_Order__c();
        taskOrder.Contract_Vehicle__c = cv.id;
        insert taskOrder;
        
        // test folder
        Folder__c folder = new Folder__c();
        folder.Task_Order__c = taskOrder.Id;
        insert folder;
        
        // test attachment
        Attachment att = new Attachment();
        att.Name = 'Test Attachment';
        att.Body = Blob.ValueOf('test');
        att.ParentId = folder.Id;
        insert att;
        
        // test folder
        Folder__c folder2 = new Folder__c();
        folder2.Task_Order__c = taskOrder.Id;
        insert folder2;
        
        // test attachment
        Note note = new Note();
        note.Title = 'Test Attachment';
        note.Body = 'test';
        note.ParentId = folder2.Id;
        insert note;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(taskOrder);
        TM_TaskOrderAttachmentController obj = new TM_TaskOrderAttachmentController(sc);
        system.assertEquals(obj.modelList.size(),2);
        
        obj.deleteRecordId = att.Id;
        obj.type = 'Attachment';
        obj.confirmDeleteNotesAndAttachment();
        system.assertEquals(obj.modelList.size(),1);
        
        obj.deleteRecordId = note.Id;
        obj.type = 'Note';
        obj.confirmDeleteNotesAndAttachment();
        system.assertEquals(obj.modelList.size(),0);
        
        obj.attachment.Name = 'Test Attachment';
        obj.attachment.Body = Blob.ValueOf('test');
        obj.uploadAttachment();
        system.assertNotEquals(obj.attachment.Id,null);
        
        obj.Note.Title = 'Test Attachment';
        obj.Note.Body = 'test';
        obj.newNote();
        system.assertNotEquals(obj.Note.Id,null);
        
        Set<String> folderIds = TM_TaskOrderAttachmentController.fetchShareFolders();
        system.assertEquals(folderIds.size() ,2);
    }
}