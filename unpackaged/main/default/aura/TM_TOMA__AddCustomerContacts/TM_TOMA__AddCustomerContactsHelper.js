({
    getRole1: function(component, event, helper) {
        
        var action = component.get("c.getRoleMap"); 
        var opts = [];
        action.setCallback(this,function(a){
            var state= a.getState();
            if(state=="SUCCESS"){
                var custs = [];
                var conts = a.getReturnValue();
                for (var key in conts ) {
                    custs.push({value:conts[key], key:key});
                }
                component.set("v.role", custs);
            }
            else if (state == "ERROR") 
            {
                console.log(a.getError());            
            }
        });
        $A.enqueueAction(action);  
    },
    getVehicalPatnerList1 : function(component, event, helper) {
        var action = component.get("c.getVehicalPatner");
        action.setParams({
            contractVehicalId: component.get("v.recordId")
        });
        action.setCallback(this, function(a) {
            var state= a.getState();
            if(state=="SUCCESS"){
                var tempWrap=a.getReturnValue();
                if(tempWrap == null){ 
                    component.set("v.valueTableBookingSizeFlag",0);
                    component.set("v.checkSavebutton",0);
                }
                else{
                    if(tempWrap != undefined){
                        for(var i=0;i<tempWrap.length;i++)
                        {
                            if(tempWrap[i].TM_TOMA__Name__c === '' || tempWrap[i].TM_TOMA__Name__c === undefined || tempWrap[i].TM_TOMA__Name__c === null)
                            {
                                tempWrap[i].TM_TOMA__Name__c='';
                            }
                        }
                        component.set("v.customerContactList",tempWrap);
                    }
                    component.set("v.checkSavebutton",0);
                }
            }
            else if (state == "ERROR") 
            {
                console.log(a.getError());            
            }
        })
        $A.enqueueAction(action); 
    }
})