global with sharing class TM_SendSurveyController1 {
    public String taskOrderId{get;set;}
    public List<Survey__c> surveyList{get;set;}
    public List<ContactWrapper> conWraList {get;set;}
    //public List<User> partnerContactList {get;set;}
    public List<AttachmentWrapper> attachmentWraList {get;set;}//New func
    public boolean flag {get;set;}
    public String msg {get;set;}    
    public Map<Id,String> surveyIdNameMap{get;set;}
    public Map<Id,String> emailIdNameMap{get;set;}
    //public EmailTemplate emailTemplate{get;set;}
    public String surveyStrId{get;set;}
    public String emailStrId{get;set;}
    public boolean avoidSurvey{get;set;}
    //public List<EmailTemplate> emailTemplateList{get;set;}
    
    //Custom Email Veriables
    public String subjectToSet {get;set;}
    public String emailBodyToSet {get;set;}
    public List<Task_Order_Questionnaire__c> communityUrlList;
    //end
    public String isValidUser{get; set;}
    
    public TM_SendSurveyController1(ApexPages.StandardController sc){   
        isValidUser = LincenseKeyHelper.checkLicensePermitionForFedTOM();
        if(isValidUser != 'Yes')
            return;
        
        if(Schema.sObjectType.Task_Order_Contact__c.isAccessible() && Schema.sObjectType.Survey__c.isAccessible() && Schema.sObjectType.Task_Order__c.isAccessible()){ 
            avoidSurvey = false;
            taskOrderId = sc.getRecord().Id;
            conWraList = new  List<ContactWrapper>();
            List<Task_Order_Contact__c> tocList = [SELECT Contact__c, Contact__r.Name, Contact__r.Email, Contact__r.Account.Name,Contact__r.AccountId FROM Task_Order_Contact__c WHERE Task_Order__c = :sc.getRecord().Id];
            if(!tocList.isEmpty()){
                for(Task_Order_Contact__c toc : tocList){
                    conWraList.add(new ContactWrapper(toc));
                }
            }
            
            surveyList = [select id,Name,Description__c,No_of_Active_Questions__c, isActive__c   from Survey__c WHERE isActive__c=TRUE limit 1000]; 
            surveyIdNameMap = new Map<Id,String>();
            for(Survey__c sur : surveyList ){
                //if(sur.isActive__c == True){
                    surveyIdNameMap.put(sur.id, sur.name);   
                //}        
            }
            try{
                Task_Order__c[] taskOrderList = [select id,(select id,name,BodyLength from Attachments)  from Task_Order__c WHERE Id=:taskOrderId];
                if(!taskOrderList.isEmpty()){
                    System.debug('taskOrderList ='+taskOrderList);
                    if(taskOrderList.get(0).Attachments!=null){
                        List<Attachment> attachmentList=taskOrderList.get(0).Attachments;
                        System.debug('attachmentList ='+attachmentList);
                        if(attachmentList!= null && !attachmentList.isEmpty()){
                            attachmentWraList = new List<AttachmentWrapper>();
                            for(Attachment attachment : attachmentList){
                                AttachmentWrapper attachmentWrapper = new AttachmentWrapper(attachment.Id, attachment.Name,attachment.BodyLength);
                                attachmentWraList.add(attachmentWrapper);
                            }
                        }
                    }
                }
                System.debug(''+attachmentWraList);
                if(Test.isRunningTest() && attachmentWraList != null && !attachmentWraList.isEmpty())
                    attachmentWraList.get(0).status = true;
            }catch(Exception e){
                System.debug(''+e.getMessage()+'\t'+e.getLineNumber());
            }
            selectEmailTemplete();
            msg = '';
        }
    }
    
/*************************** The following methods used to send selected survey along with email template to partner or customer community user *********************************************************/
    
    public void selectEmailTemplete(){
         emailStrId = '';
        if(Schema.sObjectType.EmailTemplate.isAccessible() && Schema.sObjectType.Questionaries_Email_Template__c.isAccessible()){
            List<EmailTemplate> emailTemplateList;
            if(surveyStrId!=Null && surveyStrId!=''){            
                List<Id> emailTempleteIdList = new List<Id>();
                for(Questionaries_Email_Template__c qet : [SELECT Email_Templete__c From Questionaries_Email_Template__c WHERE Teaming_Request_Questionnaire__c=:surveyStrId LIMIT 1000]){
                    emailTempleteIdList.add(qet.Email_Templete__c);
                }
                emailTemplateList = [Select Id, name From EmailTemplate where folder.name = 'TOMA Email Templates' AND Id In :emailTempleteIdList];          
            }else{
                emailTemplateList = [Select Id, name From EmailTemplate where folder.name = 'TOMA Email Templates'];
            }
            if(emailIdNameMap!=Null){
                emailIdNameMap.clear();
            }else{
                emailIdNameMap = new Map<Id,String>();
            }
            for( EmailTemplate emailTemp  : emailTemplateList ){
                emailIdNameMap.put(emailTemp.id, emailTemp.Name);            
            }
        }
    }  
    
     
    public PageReference sendEmail(){
      if(Schema.sObjectType.Task_Order__c.isAccessible() && Schema.sObjectType.Survey_task__c.isAccessible() && Schema.sObjectType.Survey__c.isAccessible() && Schema.sObjectType.Task.isAccessible()){
      
            Task_Order__c taskOrder = [Select Id,Contract_Vehicle__c,Contract_Number__c,Contract_Vehicle__r.Name,Primary_Requirement__c,Contract_Type__c From Task_Order__c where id =: taskOrderId];
            Survey_task__c surveyTask = new Survey_Task__c();
            surveyTask.Task_Order__c = taskOrder.id;
            surveyTask.Contract_Vehicle__c = taskOrder.Contract_Vehicle__r.Name;
            surveyTask.Primary_Requirement__c = taskOrder.Primary_Requirement__c;
            surveyTask.Task_Order_Contract_Type__c = taskOrder.Contract_Type__c;
            //surveyTask.Task_Order_Number__c = taskOrder.Task_Order_Number__c;
            surveyTask.Contract_Number__c = taskOrder.Contract_Number__c ;
            
            Survey__c survey;
            if(!avoidSurvey && surveyStrId!=Null && surveyStrId!=''){               
                surveyTask.Survey__c = (ID)surveyStrId;
                
               ///// insert surveyTask;
              DMLManager.insertAsUser(surveyTask);
                survey = [Select Id,Name , isActive__c from Survey__c where id =: surveyStrId];
            }
           
            Set<Id> ContactIdSet = new Set<Id>();
            List<User> userList = new List<User>();
            Set<Id> accountIdSet = new Set<Id>();
            
           for(ContactWrapper conWrp : conWraList ){
                if(conWrp.conStatus){
                    ContactIdSet.add(conWrp.con.Contact__c);
                    accountIdSet.add(conWrp.con.Contact__r.AccountId);
                }
                
            }
            
             updateTeamingpartnerStatus(accountIdSet);
            communityUrlList = [SELECT id, Customer_Community_URL__c, Partner_Community_URL__c FROM Task_Order_Questionnaire__c LIMIT 1];
           
           /* userList = [SELECT id, contactId,Profile.UserLicense.Name FROM User WHERE contactId in : ContactIdSet];
            Map<Id,User> contactIdUserMap = new Map<Id,User>();
            for(User usr : userList ){
                if(usr.contactId != null){
                    contactIdUserMap.put(usr.contactId,usr);
                }
            }*/
            List<Messaging.singleEmailMessage> mails = new List<Messaging.singleEmailMessage>();
            
            List<Task> taskList = new List<Task>();
            try{
                msg = ''; 
                for(ContactWrapper conWrp : conWraList ){
                    if(conWrp.conStatus){
                       /* User usr = contactIdUserMap.get(conWrp.con.Contact__c);
                        if(usr != null){ 
                             mails.add(getSingleEmail(surveyTask.Id,usr.id));
                             if(!avoidSurvey && surveyStrId!=Null && surveyStrId!=''){ 
                                taskList.add(getTask(survey,surveyTask.Id,usr,conWrp.con.Contact__c,taskOrderId));
                             }
                        }
                        else{*/
                             mails.add(getSingleEmail(surveyTask.Id,conWrp.con.Contact__c));
                             if(!avoidSurvey && surveyStrId!=Null && surveyStrId!=''){ 
                                 taskList.add(getTask(survey,surveyTask.Id,null,conWrp.con.Contact__c,taskOrderId));
                             }
                        //}
                    }
                }
                //New code start saurabh
                 if(attachmentWraList!=null && !attachmentWraList.isEmpty()){
                    System.debug(''+attachmentWraList);
                    List<Id> attachmentIdList = new List<Id>();
                    for(AttachmentWrapper wrapper : attachmentWraList){
                        if(wrapper.status)
                            attachmentIdList.add(wrapper.attachmentId);
                    }
                    System.debug(''+attachmentIdList);
                    List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
                    for (Attachment attachment : [select Name, Body, BodyLength from Attachment where Id In :attachmentIdList])
                    {
                        Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                        efa.setFileName(attachment.Name);
                        efa.setBody(attachment.Body);
                        attachment.Body = null;
                        fileAttachments.add(efa);
                    }
                    if(!fileAttachments.isEmpty()){
                        for(Messaging.singleEmailMessage mail : mails){
                            mail.setSaveAsActivity(true);
                            mail.setFileAttachments(fileAttachments);
                        }
                    }
                }
                //new code end
                /*if(userList != null && userList.size()>0 ){
                    
                    for(User usr : userList){
                       
                        //Create Email for sending to user.
                        Messaging.singleEmailMessage mail = new Messaging.singleEmailMessage();
                        if(emailStrId!=Null && emailStrId!=''){
                            mail.setTemplateId((Id)emailStrId); // Mail set email templete Id
                        }else{
                            mail.setSubject(subjectToSet); //custom mail set Subject
                            mail.setPlainTextBody(emailBodyToSet); //custom mail set Body
                        }
                        if(!avoidSurvey && surveyStrId!=Null && surveyStrId!='' && emailStrId!=Null && emailStrId!=''){
                            if(!test.isRunningTest()){
                                mail.setWhatId(surveyTask.Id); // Mail set survey Id as watId
                            }
                        }
                        mail.setTargetObjectId(usr.Id);  
                        mail.setSaveAsActivity(false);            
                        mails.add(mail);
                        
                        
                        //Create task
                        if(!avoidSurvey && surveyStrId!=Null && surveyStrId!=''){    
                            Task tsk = new Task();
                            tsk.OwnerId = usr.id;
                            
                            if(survey != null){
                                tsk.Subject = survey.Name;
                            }
                            else{
                                tsk.Subject = 'New Questionnaire';
                            }
                            tsk.Priority = 'Normal';
                            tsk.Status = 'Not Started';
                            tsk.WhatId = taskOrderId;
                            tsk.ActivityDate = Date.today().addDays(3);
                            if(usr.Profile.UserLicense.Name.contains('Partner')){
                                if(communityUrlList[0].Partner_Community_URL__c != null){
                                    //tsk.Description = 'https://idiqcapture-developer-edition.na31.force.com/partner/apex/TM_SurveyResponsePage?sid='+surveyTask.id;
                                    tsk.Description =  communityUrlList[0].Partner_Community_URL__c+'/apex/TM_SurveyResponsePage?sid='+surveyTask.id;
                                }
                            }
                            else{
                                if(communityUrlList[0].Customer_Community_URL__c != null){
                                    //tsk.Description = 'https://idiqcapture-developer-edition.na31.force.com/Customer/apex/TM_SurveyResponsePage?sid='+surveyTask.id;
                                    tsk.Description = communityUrlList[0].Customer_Community_URL__c+'/apex/TM_SurveyResponsePage?sid='+surveyTask.id;
                                }
                            }
                            
                           // surveyStrId
                            taskList.add(tsk);
                        }
                    }
                     
                }*/
                Messaging.sendEmail(mails);
                if(!taskList.isEmpty()){
                     ///// insert taskList;
                    DMLManager.insertAsUser(taskList);
                }
                msg = 'Email has been sent succesfully.';
            }
            catch(Exception e){
                ApexPages.addMessage (new ApexPages.Message (ApexPages.Severity.Error, e.getMessage()));            
                ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.INFO, 'Error = '+e));            
                return Null;
            }
        
            PageReference pg = new PageReference ('/'+ taskOrderId);        
            pg.setRedirect(true);        
            return pg;
         }
         return null;
    }
    
    private Messaging.singleEmailMessage getSingleEmail(String surveyTaskId,String userOrContactId){
        Messaging.singleEmailMessage mail = new Messaging.singleEmailMessage();
        /*if(emailStrId!=Null && emailStrId!=''){
            mail.setTemplateId((Id)emailStrId); // Mail set email templete Id
        }else{
            mail.setSubject(subjectToSet); //custom mail set Subject
            mail.setPlainTextBody(emailBodyToSet); //custom mail set Body
        }*/
        
        if(emailStrId!=Null && emailStrId!='' && ((subjectToSet == null || subjectToSet == '') && (emailBodyToSet == null || emailBodyToSet == ''))){
            mail.setTemplateId((Id)emailStrId); // Mail set email templete Id
        }else{
            mail.setSubject(subjectToSet); //custom mail set Subject
            mail.setPlainTextBody(emailBodyToSet); //custom mail set Body
            //new Changes
            subjectToSet = '';
            emailBodyToSet = '';
        }
        
        if(!avoidSurvey && surveyStrId!=Null && surveyStrId!='' && emailStrId!=Null && emailStrId!=''){
            if(!test.isRunningTest()){
                mail.setWhatId(surveyTaskId); // Mail set survey Id as watId
            }
        }
        else{
            System.debug('Set Task Orser Id = '+taskOrderId);
            mail.setWhatId(taskOrderId); // Mail set Task order id as watId
        }
        mail.setTargetObjectId(userOrContactId);  
        mail.setSaveAsActivity(true);   
        return mail; 
    }
    
    private Task getTask(Survey__c survey,String surveyTaskId,User usr,String contactId,String taskOrderId){
        Task tsk = new Task();
        if(usr != null){
            tsk.OwnerId = usr.Id ;
            if(usr.Profile.UserLicense.Name.contains('Partner')){
                if(communityUrlList[0].Partner_Community_URL__c != null){
                    //tsk.Description = 'https://idiqcapture-developer-edition.na31.force.com/partner/apex/TM_SurveyResponsePage?sid='+surveyTask.id;
                    tsk.Description =  communityUrlList[0].Partner_Community_URL__c+'/apex/TM_SurveyResponsePage?sid='+surveyTaskId;
                }
            }
            else{
                if(communityUrlList[0].Customer_Community_URL__c != null){
                    //tsk.Description = 'https://idiqcapture-developer-edition.na31.force.com/Customer/apex/TM_SurveyResponsePage?sid='+surveyTask.id;
                    tsk.Description = communityUrlList[0].Customer_Community_URL__c+'/apex/TM_SurveyResponsePage?sid='+surveyTaskId;
                }
            }
        }
        
        if(survey != null){
            tsk.Subject = survey.Name;
        }
        else{
            tsk.Subject = 'New Questionnaire';
        }
        tsk.Priority = 'Normal';
        tsk.Status = 'Not Started';
        tsk.WhatId = taskOrderId;
        tsk.ActivityDate = Date.today().addDays(3);
        if(contactId != null){
            tsk.whoId = contactId;
        }
        return tsk;
        
    }
    
    private void updateTeamingpartnerStatus(Set<Id> accountIdSet){
        List<Teaming_Partner__c> teamingPrtnerList = [Select Id,Status__c From Teaming_Partner__c where Task_Order__c =: taskOrderId And Vehicle_Partner__r.Partner__c IN : accountIdSet];
        List<Teaming_Partner__c> updateTeamingPartnerList = new List<Teaming_Partner__c>();
        for(Teaming_Partner__c teamingPartner : teamingPrtnerList){
            if(teamingPartner.Status__c == 'New'){
                teamingPartner.Status__c = 'Request Sent';
                updateTeamingPartnerList.add(teamingPartner);
            }
        }
        if(!updateTeamingPartnerList.isEmpty()){
            //update updateTeamingPartnerList;
           DMLManager.updateAsUser(updateTeamingPartnerList);
        }
    }
    
    public String getFolderId(){
         if(Schema.sObjectType.Folder.isAccessible()){
            String Id = [Select Id From Folder Where Type = 'Email' AND name='TOMA Email Templates' LIMIT 1].ID;
            return Id.substring(0, 15);
         }
         return null;
    }    
/*************************************************************************************************************************************************************************************************/  
    /*public class TeamingPartnerWrapper{
        public Teaming_Partner__c teamPartner{get;set;}
        public boolean status {get;set;}
        
        public TeamingPartnerWrapper(Teaming_Partner__c teamPartner){
            this.status = false;
            this.teamPartner = teamPartner;
        }
    }*/
    
    /********************************* Wrapper class for contact object*********************************************/
    public class ContactWrapper{
        public boolean conStatus{get;set;}
        public Task_Order_Contact__c con {get;set;}
        
        public ContactWrapper(Task_Order_Contact__c con){
            this.con = con;
        }
    }
    /***************************************************************************************************************/
    public class AttachmentWrapper{
        public boolean status{get;set;}
        public Id attachmentId {get;set;}
        public String name{get;set;}
        Public Integer length{get;set;}
        public AttachmentWrapper(Id attachmentId,String name,Integer length){
            this.attachmentId = attachmentId;
            this.name = name;
            this.length = length;
            this.status = false;
        }
    } 
}