@RestResource(urlMapping='/RTEPRecordConnectPost/*')
global with sharing class RTEPRecordConnectPost {
    @HttpPost
    global static String doPost(){
        String urls = '';
        try{            
            RestRequest req = RestContext.request;
            String htmlBody = req.requestBody.toString();
            System.debug('Hi This is Body = '+htmlBody);                        
            //RTEPListParser parser = new RTEPListParser();
            urls = RTEPRecordParser.parse(htmlBody);
            System.debug('urls = '+urls);
        }
        catch(Exception e){
            
        }
        return urls;
    } 
}