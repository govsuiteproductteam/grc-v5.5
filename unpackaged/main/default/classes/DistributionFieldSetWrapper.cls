public with sharing class DistributionFieldSetWrapper {
    @AuraEnabled
    public String label {get; set;}
    
    @AuraEnabled
    public String typeOfField {get; set;}
    
    @AuraEnabled
    public String fieldAPI {get; set;}
    
    @AuraEnabled
    public String sObjectName {get; set;}
    
    public DistributionFieldSetWrapper(String leb,String fieldType,String api,String sobjName){
        label = leb;
        typeOfField = fieldType;
        fieldAPI = api;
        sObjectName = sobjName;
    }
}