({
    conValTableList : function(component, event, helper) {
        var action = component.get("c.getValueTableContValList");
        action.setParams({
            "contractVehicalId": component.get("v.recordId")
        });
        action.setCallback(this, function(a) {
            var state= a.getState();
            if(state=="SUCCESS"){
                var conValTableList=a.getReturnValue();
                if(conValTableList == null){ 
                    component.set("v.valueTableContValSizeFlag",0);
                }
                else{
                    component.set("v.valueTableContValSizeFlag",conValTableList.length);
                    //17-08-2018 issue fixed for Start_Date__c is undefined  
                    for(var i = 0; i < conValTableList.length; i++){
                        console.log('Start_Date__c@@@@@@@@@' + conValTableList[i].TM_TOMA__Start_Date__c);
                        if(conValTableList[i].TM_TOMA__Start_Date__c == undefined){
                            //valueTableList[i].Start_Date__c = 
                            conValTableList[i].TM_TOMA__Start_Date__c='';
                        }
                    }
                    component.set("v.valueTableContVal", conValTableList);
                    
                    //component.set("v.valueTableContVal", a.getReturnValue());
                }
            }
            else if (state == "ERROR") 
            {
                console.log(a.getError());            
            }
        })
        $A.enqueueAction(action); 
    },
    getTMContractVehicle1 : function(component, event, helper) {
        var action = component.get("c.getContractVehical");
        action.setParams({
            recordId: component.get("v.recordId")
        });
        action.setCallback(this, function(a) {
            var state= a.getState();
            if(state=="SUCCESS"){
                component.set("v.contract", a.getReturnValue());
            }
            else if (state == "ERROR") 
            {
                console.log(a.getError());            
            }
        })
        $A.enqueueAction(action); 
    },
    
    saveContValTableList : function (component, event, helper){
        document.getElementById("saveButton3").disabled = true;
        document.getElementById("saveButton31").disabled = true;
        
        var invaliddDateRow='';   
        var delConValIds=component.get("v.delConValId");
        var valTableCotValList=component.get("v.valueTableContVal");
        var valueTableJSON = '[';
        var checkValidationError='Yes';
        if(valTableCotValList != undefined){
            for (var i=0; i<valTableCotValList.length; i++){        
                if(valueTableJSON.length>1){
                    valueTableJSON +=  ',{"attributes":{"type":"TM_TOMA__Value_Table__c"},';
                    var vTId = valTableCotValList[i].Id;
                    var description = valTableCotValList[i].TM_TOMA__Description__c=== undefined?'':valTableCotValList[i].TM_TOMA__Description__c.replace(/(\r\n|\n|\r)/gm," ");
                    var startDate = valTableCotValList[i].TM_TOMA__Start_Date__c;
                    var dollars = valTableCotValList[i].TM_TOMA__Dollars__c === undefined?null:valTableCotValList[i].TM_TOMA__Dollars__c;

                    var RecordTypeId  = valTableCotValList[i].RecordTypeId ;
                    var Contract_Vehicle__c  = valTableCotValList[i].TM_TOMA__Contract_Vehicle__c ;
                    var appendData = '';
                    //Code to check a date is invalid or not          
                    var testmsg =helper.validatedate(component,event,helper,startDate);
                    
                    if(testmsg===false)
                    {
                        checkValidationError='InvalidStartDate';
                        invaliddDateRow=invaliddDateRow+''+i;
                        invaliddDateRow=invaliddDateRow+',';
                    }
                    if(startDate==='')
                    {
                        checkValidationError='startDate';
                    }
                    if(!(vTId===undefined))
                    {
                        appendData +='"Id":"'+ vTId+'" ,';
                    }
                    appendData += '"TM_TOMA__Description__c":"'+ description+'" , "TM_TOMA__Start_Date__c":"'+startDate+'","TM_TOMA__Dollars__c":'+dollars+',"RecordTypeId":"'+RecordTypeId+'" }';
                    valueTableJSON += appendData;
                }else{
                    valueTableJSON +=  '{"attributes":{"type":"TM_TOMA__Value_Table__c"},';
                    var vTId = valTableCotValList[i].Id;
                    var description = valTableCotValList[i].TM_TOMA__Description__c=== undefined?'':valTableCotValList[i].TM_TOMA__Description__c.replace(/(\r\n|\n|\r)/gm," ");
                    var startDate = valTableCotValList[i].TM_TOMA__Start_Date__c;
                    var dollars = valTableCotValList[i].TM_TOMA__Dollars__c === undefined?null:valTableCotValList[i].TM_TOMA__Dollars__c;
                    var RecordTypeId  = valTableCotValList[i].RecordTypeId ;
                    var Contract_Vehicle__c  = valTableCotValList[i].TM_TOMA__Contract_Vehicle__c ;
                    var appendData = '';
                    //Code to check a date is invalid or not      
                    var testmsg =helper.validatedate(component,event,helper,startDate);
                    
                    if(testmsg===false)
                    {
                        checkValidationError='InvalidStartDate';
                        invaliddDateRow=invaliddDateRow+''+i;
                        invaliddDateRow=invaliddDateRow+',';
                    }
                    if(startDate==='')
                    {
                        checkValidationError='startDate';
                    }
                    if(!(vTId===undefined))
                    {
                        appendData +='"Id":"'+ vTId+'" ,';
                    }
                    
                    appendData += '"TM_TOMA__Description__c":"'+ description+'" , "TM_TOMA__Start_Date__c":"'+startDate+'","TM_TOMA__Dollars__c":'+dollars+',"RecordTypeId":"'+RecordTypeId+'" }';
                    valueTableJSON += appendData;
                }
            } 
            valueTableJSON += ']';   
            if(checkValidationError==='Yes'){
                var action = component.get("c.saveContractValTable");
                action.setParams({ 
                    "contValList": valueTableJSON,
                    "recordId": component.get("v.recordId"),
                    "delConValIdsStr": delConValIds
                });
                action.setCallback(this, function(response){
                    var state = response.getState();
                    var messageText = response.getReturnValue();
                    if (state === "SUCCESS")
                    {
                        component.set("v.delConValId",'');
                        helper.getTMContractVehicle1(component, event, helper);
                        helper.conValTableList(component, event, helper);
                        // "If" condition and "Else" block are newly added to restrict user access according to the object permission(16/08/2018)
                        if(messageText == '' || messageText == undefined || messageText == null){
                            alert("Changes were saved successfully.");
                        }else{
                            alert(messageText);
                        }
                        
                        /*                var act3 = component.get("c.isLightningPage");
                    act3.setCallback(this, function(a) {
                        if (a.getState() === "SUCCESS") {
                            if(a.getReturnValue() == false){
                                alert("Data save successfully.");
                            }
                            else{ 
                               var toastEvent = $A.get("e.force:showToast");
                                toastEvent.setParams({
                                    "title": "Success!",
                                    "message": "Data save successfully.",
                                    "type":"success"
                                });
                                toastEvent.fire();
                            }
                        }
                    }); 
                    $A.enqueueAction(act3);*/
                        
                        
                    }
                    else if (state === "ERROR") {
                        console.log(response.getError());            }
                    document.getElementById("saveButton3").disabled = false;
                    document.getElementById("saveButton31").disabled = false;
                    // btn.set("v.disabled",false);
                });
                $A.enqueueAction(action);
            }else if(checkValidationError==='InvalidStartDate')
            {
                var checkId = invaliddDateRow.split(',');
                //alert(checkId.length);
                for(var i = 0; i < checkId.length-1; i++) {
                    // alert(checkId[i]);
                    var d = document.getElementById(checkId[i]+'mil');
                    d.className += " validationClass";
                }
                alert('ERROR:- Invalid Milestone Date, in row:- '+invaliddDateRow);
                /*  var act3 = component.get("c.isLightningPage");
            act3.setCallback(this, function(a) {
                if (a.getState() === "SUCCESS") {
                    if(a.getReturnValue() == false){
                         var checkId = invaliddDateRow.split(',');
                        //alert(checkId.length);
                        for(var i = 0; i < checkId.length-1; i++) {
                           // alert(checkId[i]);
                            var d = document.getElementById(checkId[i]+'mil');
                            d.className += " validationClass";
                        }
                        alert('ERROR:- Invalid Milestone Date, in row:- '+invaliddDateRow);
                    }
                    else{ 
                         var checkId = invaliddDateRow.split(',');
                       // alert(checkId.length);
                        for(var i = 0; i < checkId.length-1; i++) {
                            //alert(checkId[i]);
                            var d = document.getElementById(checkId[i]+'mil');
                            d.className += " validationClass";
                        }
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!",
                            "message": "Invalid Milestone Date, in row:- "+invaliddDateRow+"",
                            "type":"error"
                        });
                        toastEvent.fire();
                    }
                }
            }); 
            $A.enqueueAction(act3);*/
            document.getElementById("saveButton3").disabled = false; 
                document.getElementById("saveButton31").disabled = false;
        }else{
            alert("ERROR:- Please enter a Milestone Date.");
            /*  var act3 = component.get("c.isLightningPage");
                    act3.setCallback(this, function(a) {
                        if (a.getState() === "SUCCESS") {
                            if(a.getReturnValue() == false){
                                alert("ERROR:- Enter date for MILESTONE DATE.");
                            }
                            else{ 
                                var toastEvent = $A.get("e.force:showToast");
                                toastEvent.setParams({
                                    "title": "Error!",
                                    "message":"Enter date for MILESTONE DATE.",
                                    "type":"error"
                                });
                                toastEvent.fire();
                            }
                        }
                    }); 
                    $A.enqueueAction(act3);*/
            // alert("Enter date for MILESTONE DATE.");
            //btn.set("v.disabled",false);
            document.getElementById("saveButton3").disabled = false;
            document.getElementById("saveButton31").disabled = false;
        }
        }
        else{
            document.getElementById("saveButton3").disabled = false;
            document.getElementById("saveButton31").disabled = false;
        }
    },
    validatedate:function(component,event,helper,inputText)
    {
        
        var dateformat = /(\d{4})-(\d{2})-(\d{2})/;
        // Match the date format through regular expression
        if(inputText.match(dateformat))
        {
            //Test which seperator is used '/' or '-'
            var opera1 = inputText.split('/');
            var opera2 = inputText.split('-');
            var lopera1 = opera1.length;
            var lopera2 = opera2.length;
            // Extract the string into month, date and year
            if (lopera1>1)
            {
                var pdate = inputText.split('/');
            }
            else if (lopera2>1)
            {
                var pdate = inputText.split('-');
            }
            var mm  = parseInt(pdate[1]);
            var dd = parseInt(pdate[2]);
            var yy = parseInt(pdate[0]);
            // Create list of days of a month [assume there is no leap year by default]
            var ListofDays = [31,28,31,30,31,30,31,31,30,31,30,31];
            if (mm==1 || mm>2)
            {
                if (dd>ListofDays[mm-1])
                {
                    return false;
                }
            }
            if (mm==2)
            {
                var lyear = false;
                if ( (!(yy % 4) && yy % 100) || !(yy % 400)) 
                {
                    lyear = true;
                }
                if ((lyear==false) && (dd>=29))
                {
                    return false;
                }
                if ((lyear==true) && (dd>29))
                {
                    return false;
                }
            }
        }
        else
        {
            // document.form1.text1.focus();
            return false;
        }
    },
    
    checkValueTablePermissions : function(component, event, helper) {
        //Check "Value Table" object Permissions
        var action = component.get('c.getValueTablePermissions');
        action.setCallback(this,function(response){
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.mileStoneTableCtrl', response.getReturnValue());
                var c=response.getReturnValue();
                if(c.isUpdateValueTable===false && c.isCreateValueTable===false && c.isDeleteValueTable===false ){
                    component.set("v.savePermission",false);
                }
                console.log(" isUpdateValueTable > "+c.isUpdateValueTable +" isCreateValueTable> "+c.isCreateValueTable+" isDeleteValueTable > "+c.isDeleteValueTable);
            }
        });
        $A.enqueueAction(action);
        
    }
})