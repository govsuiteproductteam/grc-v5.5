@isTest
public class FedBidEmailHandlerTest {
    
    public static testmethod void testCase1(){
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Contact con = TestDataGenerator.createContact('TestCon', acc );
        insert con;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('T-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'fedbid@1-26k6vkbu994b5a8fxxi1ztpd4f32ns91d99oztowo544emghy9.61-zhsweao.na34.apex.salesforce.com';
        
        insert conVehi;
        
        //For new Task Order Creation  
        Messaging.InboundEmail email  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        List<StaticResource> srList = [SELECT Id,Body FROM StaticResource WHERE Name='FedBidNew'];
        email.plainTextBody = srList.get(0).Body.toString();
        email.htmlBody = srList.get(0).Body.toString();
        email.fromAddress ='test@test.com';
        email.fromName = 'ABC XYZ';
        email.subject = 'FedBid Buy No. 900001, Fort Polk GPC, Ghostwriter Max-T2 Autopen';
        email.toaddresses = new List<String>();
        email.toaddresses.add('fedbid@1-26k6vkbu994b5a8fxxi1ztpd4f32ns91d99oztowo544emghy9.61-zhsweao.na34.apex.salesforce.com');
        
        //To update existing task order 
        Messaging.InboundEmail email1  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env1 = new Messaging.InboundEnvelope();
        List<StaticResource> srList1 = [SELECT Id,Body FROM StaticResource WHERE Name='FedBidUpdated'];
        email1.plainTextBody = srList1.get(0).Body.toString();
        email1.htmlBody = srList1.get(0).Body.toString();
        email1.fromAddress ='test@test.com';
        email1.fromName = 'ABC XYZ';
        email1.subject = 'FedBid Buy No. 900001, Fort Polk GPC, Ghostwriter Max-T2 Autopen';
        email1.toaddresses = new List<String>();
        email1.toaddresses.add('fedbid@1-26k6vkbu994b5a8fxxi1ztpd4f32ns91d99oztowo544emghy9.61-zhsweao.na34.apex.salesforce.com');
        
        //For Task Order Amendment
        Messaging.InboundEmail email2  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env2 = new Messaging.InboundEnvelope();
        List<StaticResource> srList2 = [SELECT Id,Body FROM StaticResource WHERE Name='FedBidAmendment'];
        email2.plainTextBody = srList2.get(0).Body.toString();
        email2.htmlBody = srList2.get(0).Body.toString();
        email2.fromAddress ='test@test.com';
        email2.fromName = 'ABC XYZ';
        email2.subject = 'FedBid Buy No. 900001_01 has been Reposted: DOS AQM Charleston-Department of State, FY18_004 - (4) HP DIGITAL SENDERS 8500 FN1';
        email2.toaddresses = new List<String>();
        email2.toaddresses.add('fedbid@1-26k6vkbu994b5a8fxxi1ztpd4f32ns91d99oztowo544emghy9.61-zhsweao.na34.apex.salesforce.com');
       
        //For Task Order Amendment having "Extended" text in email subject
        Messaging.InboundEmail email3  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env3 = new Messaging.InboundEnvelope();
        List<StaticResource> srList3 = [SELECT Id,Body FROM StaticResource WHERE Name='FedBidAmendment'];
        email3.plainTextBody = srList3.get(0).Body.toString();
        email3.htmlBody = srList3.get(0).Body.toString();
        email3.fromAddress ='test@test.com';
        email3.fromName = 'ABC XYZ';
        email3.subject = 'FedBid Buy No.900001 has been Extended: Department of State, OPTION - HP Licnese Renewals The Buyer has Extended this buy.';
        email3.toaddresses = new List<String>();
        email3.toaddresses.add('fedbid@1-26k6vkbu994b5a8fxxi1ztpd4f32ns91d99oztowo544emghy9.61-zhsweao.na34.apex.salesforce.com');

        //For Task Order Repost
        Messaging.InboundEmail email4  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env4 = new Messaging.InboundEnvelope();
        List<StaticResource> srList4 = [SELECT Id,Body FROM StaticResource WHERE Name='FedBidRepost'];
        email4.plainTextBody = srList4.get(0).Body.toString();
        email4.htmlBody = srList4.get(0).Body.toString();
        email4.fromAddress ='test@test.com';
        email4.fromName = 'ABC XYZ';
        email4.subject = 'REPOST: FedBid Buy No. 900003_01, DOJ BOP Field Offices- FCI MENDOTA, IT Supplies - Scanners & Printers';
        email4.toaddresses = new List<String>();
        email4.toaddresses.add('fedbid@1-26k6vkbu994b5a8fxxi1ztpd4f32ns91d99oztowo544emghy9.61-zhsweao.na34.apex.salesforce.com');
       
        //For Task Order Repost Amendment
        Messaging.InboundEmail email5  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env5 = new Messaging.InboundEnvelope();
        List<StaticResource> srList5 = [SELECT Id,Body FROM StaticResource WHERE Name='FedBidRepostAmendment'];
        email5.plainTextBody = srList5.get(0).Body.toString();
        email5.htmlBody = srList5.get(0).Body.toString();
        email5.fromAddress ='test@test.com';
        email5.fromName = 'ABC XYZ';
        email5.subject = 'REPOST: FedBid Buy No. 900003_01, DOJ BOP Field Offices- FCI MENDOTA, IT Supplies - Scanners & Printers';
        email5.toaddresses = new List<String>();
        email5.toaddresses.add('fedbid@1-26k6vkbu994b5a8fxxi1ztpd4f32ns91d99oztowo544emghy9.61-zhsweao.na34.apex.salesforce.com');
         
                
        FedBidEmailHandler handler = new FedBidEmailHandler();
        Test.startTest();
        Messaging.InboundEmailResult result = handler.handleInboundEmail(email, env);
        List<Task_Order__c> taskOrderList = [SELECT Id FROM Task_Order__c];
        Messaging.InboundEmailResult result1 = handler.handleInboundEmail(email1, env1);
        List<Contract_Mods__c> contractModList = [SELECT Id FROM Contract_Mods__c];
        Messaging.InboundEmailResult result2 = handler.handleInboundEmail(email2, env2);
        Messaging.InboundEmailResult result3 = handler.handleInboundEmail(email3, env3);
        Messaging.InboundEmailResult result4 = handler.handleInboundEmail(email4, env4);
        Messaging.InboundEmailResult result5 = handler.handleInboundEmail(email5, env5);

        Test.stopTest();
        
        System.assertEquals(1, taskOrderList.size());
        System.assertEquals(1, contractModList.size());
        
    }
    
}