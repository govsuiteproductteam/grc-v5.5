@isTest
public class JEClassEmailTemplateHandlerTest {
	
    public static testmethod void testCase1(){
        Test.startTest();
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Contact con = TestDataGenerator.createContact('TestCon', acc );
        insert con;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('T-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'jeclass@1r57jajpik1zhx0azswib9h42r3dmtn110xvm6cov8ditc0zma.61-zhsweao.na34.apex.salesforce.com';
        
        insert conVehi;
        Test.stopTest();
        
        JEClassEmailHandler  jeClassEmailTemplateHandler = new JEClassEmailHandler();
        //for new Task Order Creation  
        Messaging.InboundEmail email  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        List<StaticResource> srList = [SELECT Id,Body FROM StaticResource WHERE Name='JEClassEmailTest'];
        
        email.plainTextBody = srList.get(0).Body.toString();
        email.fromAddress ='test@test.com';
        email.fromName = 'ABC XYZ';
        email.subject = 'Fwd:JE-Class Task 0002 Final Task Order Request (UNCLASSIFIED)';
        email.toaddresses = new List<String>();
        email.toaddresses.add('jeclass@1r57jajpik1zhx0azswib9h42r3dmtn110xvm6cov8ditc0zma.61-zhsweao.na34.apex.salesforce.com');
        
        //For Updating existing task order with existing Contact
        List<StaticResource> srList1 = [SELECT Id,Body FROM StaticResource WHERE Name='JEClassEmailTest1'];
        Messaging.InboundEmail email1  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env1 = new Messaging.InboundEnvelope();
        email1.plainTextBody = srList1.get(0).Body.toString();
        email1.fromAddress ='test@test.com';
        email1.fromName = 'ABC XYZ';
        email1.subject = 'Re:JE-Class Task 0002 Final Task Order Request (UNCLASSIFIED)';
        email1.toaddresses = new List<String>();
        email1.toaddresses.add('jeclass@1r57jajpik1zhx0azswib9h42r3dmtn110xvm6cov8ditc0zma.61-zhsweao.na34.apex.salesforce.com');
        
        //For Updating existing task order with new Contact
        Messaging.InboundEmail email2  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env2 = new Messaging.InboundEnvelope();
        email2.plainTextBody = '';
        email2.fromAddress ='test@test.com';
        email2.fromName = 'ABC XYZ';
        email2.subject = 'Test';
        email2.toaddresses = new List<String>();
        email2.toaddresses.add('jeclass@1r57jajpik1zhx0azswib9h42r3dmtn110xvm6cov8ditc0zma.61-zhsweao.na34.apex.salesforce.com');
        Messaging.InboundEmailResult result = jeClassEmailTemplateHandler.handleInboundEmail(email, env);
        List<Task_Order__c> taskOrderList = [SELECT Id FROM Task_Order__c];
        System.assertEquals(taskOrderList.size(), 1);
        Messaging.InboundEmailResult result1 = jeClassEmailTemplateHandler.handleInboundEmail(email1, env1);
        List<Contract_Mods__c> contractModList = [SELECT Id FROM Contract_Mods__c];
        System.assertEquals(contractModList.size(), 1);
        Messaging.InboundEmailResult result2 = jeClassEmailTemplateHandler.handleInboundEmail(email2, env2);
    }
    
}