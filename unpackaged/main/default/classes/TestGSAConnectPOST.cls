@isTest
public with sharing class TestGSAConnectPOST {
    public static TestMethod void testDoPost(){
        
        StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'QuoteResponseStaticResource' LIMIT 1];
        String body = sr.Body.toString();
        
        StaticResource sr1 = [SELECT Id, Body FROM StaticResource WHERE Name = 'QuoteResponseStaticResourceNew' LIMIT 1];
        String body1 = sr1.Body.toString();
         
        StaticResource sr2 = [SELECT Id, Body FROM StaticResource WHERE Name = 'QuoteResponseStaticResource' LIMIT 1];
        String body2 = sr2.Body.toString();
        
        body2=body2.substring(0, body2.indexOf('RFQ1074470')) + '<a href="www.google.com">/advantage/ebuy/seller/RFQ_modification_description.do</a>' + 
             body2.substring(body2.indexOf('Reference #:'), body2.length());
             
        // List<TM_TOMA__Contract_Vehicle__c> contractVehicleList = Test.loadData(TM_TOMA__Contract_Vehicle__c.sObjectType,'ContractVehicleStaticResource');
        // insert contractVehicleList;
        List<Account> accountList = TestDataGeneratorGSA.createAccountList();
        insert accountList;
        List<Contact> contactList = TestDataGeneratorGSA.createContactList(accountList);
        insert contactList;
        TM_TOMA__Contract_Vehicle__c contract = TestDataGeneratorGSA.createContractVehicle(accountList, contactList);
        insert contract;
        List<TM_TOMA__Task_Order__c> taskOrderList = TestDataGeneratorGSA.createTaskOrder(contactList, accountList, contract);
        insert taskOrderList;
        
        
        
        Test.startTest();        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        req.requestBody = Blob.valueof(body);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;        
        GSAConnectPOST.doPost11();
        //List<Task_Order__c> taskOrderList1 = [Select Id,Individual_Receiving_Shipment__c FROM Task_Order__c];
        //System.debug(''+taskOrderList1);
        //With Cancellation Reason
        req.requestBody = Blob.valueof(body1);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        GSAConnectPOST.doPost11();
        
         req.requestBody = Blob.valueof(body2);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        GSAConnectPOST.doPost11();
        
       system.assertNotEquals(taskOrderList.size(), 0);
        Test.stopTest();
        
        
    }
    
    public static TestMethod void testDoPost2(){
        
        StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'QuoteResponseStaticResource' LIMIT 1];
        String body = sr.Body.toString();
       
        StaticResource sr1 = [SELECT Id, Body FROM StaticResource WHERE Name = 'QuoteResponseStaticResourceNew' LIMIT 1];
        String body1 = sr1.Body.toString();
        // List<TM_TOMA__Contract_Vehicle__c> contractVehicleList = Test.loadData(TM_TOMA__Contract_Vehicle__c.sObjectType,'ContractVehicleStaticResource');
        // insert contractVehicleList;
        List<Account> accountList = TestDataGeneratorGSA.createAccountList();
        insert accountList;
        List<Contact> contactList = TestDataGeneratorGSA.createContactList(accountList);
        insert contactList;
        TM_TOMA__Contract_Vehicle__c contract = TestDataGeneratorGSA.createContractVehicle(accountList, contactList);
        insert contract;
        List<TM_TOMA__Task_Order__c> taskOrderList = TestDataGeneratorGSA.createTaskOrder(contactList, accountList, contract);
        insert taskOrderList;
        
        Test.startTest();        
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();        
        req1.requestBody = Blob.valueof(body1);
        req1.httpMethod = 'POST';
        RestContext.request = req1;
        RestContext.response = res1;
        GSAConnectPOST.doPost11();
        //With Cancellation Reason
        req1.requestBody = Blob.valueof(body1);
        req1.httpMethod = 'POST';
        RestContext.request = req1;
        RestContext.response = res1;
        GSAConnectPOST.doPost11();
         system.assertNotEquals(taskOrderList.size(), 0);
        Test.stopTest();
    }
    
    public static TestMethod void testDoPost1(){
        
        StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'eBuyPost' LIMIT 1];
        String body = sr.Body.toString();
        //// List<TM_TOMA__Contract_Vehicle__c> contractVehicleList = Test.loadData(TM_TOMA__Contract_Vehicle__c.sObjectType,'ContractVehicleStaticResource');
        //// insert contractVehicleList;
        List<Account> accountList = TestDataGeneratorGSA.createAccountList();
        insert accountList;
        List<Contact> contactList = TestDataGeneratorGSA.createContactList(accountList);
        insert contactList;
        TM_TOMA__Contract_Vehicle__c contract = TestDataGeneratorGSA.createContractVehicle(accountList, contactList);
        insert contract;
        List<TM_TOMA__Task_Order__c> taskOrderList = TestDataGeneratorGSA.createTaskOrder(contactList, accountList, contract);
        insert taskOrderList;
        
      
        
        Test.startTest();        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        req.requestBody = Blob.valueof(body);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        GSAConnectPOST.doPost11();
         system.assertNotEquals(taskOrderList.size(), 0);
        Test.stopTest();
    }
    
    public static TestMethod void testDoPost3(){
        //written to cover voa method directly
        List<Account> accountList = TestDataGeneratorGSA.createAccountList();
        insert accountList;
        List<Contact> contactList = TestDataGeneratorGSA.createContactList(accountList);
        insert contactList;
        TM_TOMA__Contract_Vehicle__c contract = TestDataGeneratorGSA.createContractVehicle(accountList, contactList);
        insert contract;
        List<TM_TOMA__Task_Order__c> taskOrderList = TestDataGeneratorGSA.createTaskOrder(contactList, accountList, contract);
        insert taskOrderList;
        
        Task_Order__c taskOrder =  new Task_Order__c();
        taskOrder.Name = taskOrderList.get(0).name;
		taskorder.Contract_Vehicle__c = taskOrderList.get(0).TM_TOMA__Contract_Vehicle__c;
        taskOrder.Due_Date__c = Date.today().addDays(2);
        taskOrder.Release_Date__c = Date.today().addDays(2);
        taskOrder.Requiring_Activity__c = 'fdsfasd';
        taskOrder.Acquisition_Tracker_ID__c = 'sdfsa';
        taskOrder.Contract_Type__c = 'BPA';
        taskOrder.Competitive_Procedure__c = 'fsdsdg';
        taskOrder.Contract_Specialist__c = contactList.get(0).Id;
        taskOrder.Market_Research_Due_Date__c = Date.today().addDays(2);
        taskOrder.RTEP_Status__c = 'asdfasd';
        //GSAParseHelper.insertContractModForVoa(taskOrder, taskOrderList.get(0));
        
        //Integer size = taskOrderList.size();
        //Integer size1 = [SELECT count() FROM Task_Order__c];
        //System.assertEquals(size, size1);
    }
}