@isTest
public with sharing class Test_IMCS_3Handler {
    public Static TestMethod void IMCS_3HandlerTest (){
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Contact con = TestDataGenerator.createContact('avan', acc );
        insert con;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('T-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'imcs_3@0-1su9nvr2g9k8gkgyya3g3csg2n5j718ww1xlwceqaiunpmpk9d.61-zhsweao.na34.apex.salesforce.com';
        
        insert conVehi;
        
        
        Messaging.InboundEmail email  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        //email.plainTextBody = 'This should become a note';
        email.fromAddress ='test@test.com';
        email.subject = 'IMCS Email Parsing';
        email.toaddresses = new List<String>();
        email.toaddresses.add('imcs_3@0-1su9nvr2g9k8gkgyya3g3csg2n5j718ww1xlwceqaiunpmpk9d.61-zhsweao.na34.apex.salesforce.com');
        email.fromName = 'Raone Laynson';//
        //String body ='All IMCS III Contractors:<br> \n <br> \n Attached to this email are TOPR documents, in order for you to complete and submit proposal for the IMCS III support, required at Aberdeen Proving Ground and areas listed on PWS.  Make sure to READ CAREFYLLY all documents.  There is a two day site visit planned, and you must be pre-registered a week before the site visit, in order to attend.  There is a question cut-off date and time, as well as cut-off date and time for your proposal or no-bid email.  We have provided for your convenience, a Question Submission Spreadsheet, for you to list all your questions on.  All questions received will be answered at one time.  This will be after the site visit.<br> \n <br> \n Attached are the following:<br> \n <br> \n 1)  TOPR Letter-Aberdeen Proving Ground<br> \n 2)  Clin Pricing Summary Sheet<br> \n 3)  Proposal Submission Instructions<br> \n 4)  Final PWS<br> \n 5)  APG Performance Metrics<br> \n 6)  QASP APG<br> \n 7)  Current Collect Bargaining Agreement (CBA)<br> \n 8)  Service Contract Act 04-2247 Revision 17 dated 08 July 2015<br> \n 9)  Davis Bacon MD48<br> \n 10) Memo Interim Guidance Occ Health<br> \n 11) Draft DD254<br> \n 12) Question Submission Spreadsheet<br> \n <br> \n We look forward to receiving your proposal.<br> \n <br> \n <br> \n LEAH Y BENDIXEN<br> \n Contract Specialist<br> \n Army Contracting Command (ACC) - Aberdeen Proving Ground (APG)<br> \n 2133 Cushing St., Bldg 61801, Greely Hall, Room 3410 Fort Huachuca, AZ  85613-7070<br> \n Tele:  (520) 533-0438 Fax:  (520) 538-1838  DSN:  821-0438<br> \n E-mail:  <a href="mailto:leah.y.bendixen.civ@mail.mil" target="_blank">leah.y.bendixen.civ@mail.mil</a><br> \n <br> \n TIME ZONE:  Mountain Standard Time<br> \n (Throughout the year - no Daylight Savings Time in Arizona)<span class="HOEnZb"><font color="#888888"><br> \n <br> \n </font></span></div><span class="HOEnZb"><font color="#888888"><br></font></span></div><span class="HOEnZb"><font color="#888888"> \n </font></span></div><span class="HOEnZb"><font color="#888888"><br><br clear="all"><div><br></div>-- <br><div><div dir="ltr"><div style="color:rgb(136,136,136);font-size:12.8000001907349px"><font color="#999999"><span style="color:rgb(102,102,102)">----------------------------------------------------------------------------------------------------------</span></font></div><div style="color:rgb(136,136,136);font-size:12.8000001907349px"><font color="#666666">Gajanan Bhad &#124; Team Lead &#124; <b><span style="color:rgb(255,153,0)">T</span><span style="color:rgb(0,0,0)">echno</span><span style="color:rgb(255,153,0)">M</span><span style="color:rgb(0,0,0)">ile</span></b> &#124;</font><font color="#999999"> </font><a href="http://www.technomile.com/" style="color:rgb(17,85,204)" target="_blank"><font color="#000099">www.technomile.com</font></a><span style="padding-right:16px;width:16px;min-height:16px"></span></div><div style="color:rgb(136,136,136);font-size:12.8000001907349px"><font color="#888888"><div style="font-size:13px"><font color="#666666">Off.: +1-</font><a href="tel:703-340-1308%20x%20707" value="+17033401308" style="color:rgb(17,85,204)" target="_blank">703-340-1308 x 707</a> &#124; +91-<font color="#666666"><a href="tel:712-224-0031" value="+17122240031" style="color:rgb(17,85,204)" target="_blank">712-224-0031</a> &#124;</font><span style="color:rgb(102,102,102)"> Cell: +91-9<font color="#888888">02</font>-<font color="#888888">837</font>-<font color="#888888">7903</font></span></div></font></div><div style="color:rgb(136,136,136);font-size:12.8000001907349px"><font color="#666666">----------------------------------------------------------------------------------------------------------</font></div></div></div> \n </font></span></div> \n </div><br></div> \n ';
        //email.htmlBody = body.replace(' ', ' ');
        String body = 'All IMCS III Contractors:\n'+
            '\n'+
            'Attached to this email are TOPR documents, in order for you to complete and submit proposal for the IMCS III support, required at Aberdeen Proving Ground and areas listed on PWS.  Make sure to READ CAREFYLLY all documents.  There is a two day site visit planned, and you must be pre-registered a week before the site visit, in order to attend.  There is a question cut-off date and time, as well as cut-off date and time for your proposal or no-bid email.  We have provided for your convenience, a Question Submission Spreadsheet, for you to list all your questions on.  All questions received will be answered at one time.  This will be after the site visit.\n'+
            
            'Attached are the following:\n'+
            '\n'+
            '1)  TOPR Letter-Aberdeen Proving Ground\n'+
            '2)  Clin Pricing Summary Sheet\n'+
            '3)  Proposal Submission Instructions\n'+
            '4)  Final PWS\n'+
            '5)  APG Performance Metrics\n'+
            '6)  QASP APG\n'+
            '7)  Current Collect Bargaining Agreement (CBA)\n'+
            '8)  Service Contract Act 04-2247 Revision 17 dated 08 July 2015\n'+
            '9)  Davis Bacon MD48\n'+
            '10) Memo Interim Guidance Occ Health\n'+
            '11) Draft DD254\n'+
            '12) Question Submission Spreadsheet\n'+
            '\n'+
            'We look forward to receiving your proposal.'+
            '\n'+
            '\n'+
            '\n'+
            'LEAH Y BENDIXEN\n'+
            'Contract Specialist\n'+
            'Army Contracting Command (ACC) - Aberdeen Proving Ground (APG)\n'+
            '2133 Cushing St., Bldg 61801, Greely Hall, Room 3410 Fort Huachuca, AZ  85613-7070\n'+
            'Tele:  (520) 533-0438 Fax:  (520) 538-1838  DSN:  821-0438\n'+
            'E-mail:  leah.y.bendixen.civ@mail.mil\n'+
            '\n'+
            'TIME ZONE:  Mountain Standard Time\n'+
            '(Throughout the year - no Daylight Savings Time in Arizona)\n';
            email.plainTextBody = body;
            Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        attachment.body = blob.valueOf('my attachment text');
        attachment.fileName = 'textfileone.txt';
        attachment.mimeTypeSubType = 'text/plain';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
            
            Messaging.InboundEmail.TextAttachment attachmenttext = new Messaging.InboundEmail.TextAttachment();
        attachmenttext.body = 'my attachment text';
        attachmenttext.fileName = 'textfiletwo3.txt';
        attachmenttext.mimeTypeSubType = 'texttwo/plain';
        email.textAttachments =   new Messaging.inboundEmail.TextAttachment[] { attachmenttext };
            
            IMCS_3Handler imcsHandler = new IMCS_3Handler();
        
        Test.startTest();
        Messaging.InboundEmailResult result = imcsHandler.handleInboundEmail(email, env);
        Test.stopTest();
        
        List<Task_Order__c> tOrderList = [SELECT Id FROM Task_Order__c];
        System.assertEquals(tOrderList.size(), 1);
    }
}