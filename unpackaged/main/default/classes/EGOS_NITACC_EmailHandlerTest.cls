@isTest
public class EGOS_NITACC_EmailHandlerTest {

    public static testmethod void testCase1(){
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Contact con = TestDataGenerator.createContact('TestCon', acc );
        insert con;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('T-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'egos_nitacc@1ojfakrb1c3pn3ttqjxrj0oixg9snklugvw1mpq799uet5wen7.61-zhsweao.na34.apex.salesforce.com';
        
        insert conVehi;
        
        
        //For new Task Order Creation
        Messaging.InboundEmail email  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        List<StaticResource> srList = [SELECT Id,Body FROM StaticResource WHERE Name='EGOSNITACCNew'];
        email.plainTextBody = srList.get(0).Body.toString();
        //System.debug('Body = '+email.plainTextBody);
        email.fromAddress ='test@test.com';
        email.fromName = 'ABC XYZ';
        email.subject = 'RFQ CS-43499 Released for Competition.';
        email.toaddresses = new List<String>();
        email.toaddresses.add('egos_nitacc@1ojfakrb1c3pn3ttqjxrj0oixg9snklugvw1mpq799uet5wen7.61-zhsweao.na34.apex.salesforce.com');
        
        
        //To update existing task order 
        Messaging.InboundEmail email1  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env1 = new Messaging.InboundEnvelope();
        List<StaticResource> srList1 = [SELECT Id,Body FROM StaticResource WHERE Name='EGOSNITACCUpdated'];
        email1.plainTextBody = srList1.get(0).Body.toString();
        //System.debug('Body = '+email1.plainTextBody);
        email1.fromAddress ='test@test.com';
        email1.fromName = 'ABC XYZ';
        email1.subject = 'RFQ CS-43499 Released for Competition.';
        email1.toaddresses = new List<String>();
        email1.toaddresses.add('egos_nitacc@1ojfakrb1c3pn3ttqjxrj0oixg9snklugvw1mpq799uet5wen7.61-zhsweao.na34.apex.salesforce.com');
        
        
        //For QUESTIONS DUE Update
        Messaging.InboundEmail email2  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env2 = new Messaging.InboundEnvelope();
        List<StaticResource> srList2 = [SELECT Id,Body FROM StaticResource WHERE Name='EGOSNITACCQuestionsDue'];
        email2.plainTextBody = srList2.get(0).Body.toString();
        //System.debug('Body = '+email1.plainTextBody);
        email2.fromAddress ='test@test.com';
        email2.fromName = 'ABC XYZ';
        email2.subject = 'Fwd:Question Period for CS-43499 Closed';
        email2.toaddresses = new List<String>();
        email2.toaddresses.add('egos_nitacc@1ojfakrb1c3pn3ttqjxrj0oixg9snklugvw1mpq799uet5wen7.61-zhsweao.na34.apex.salesforce.com');
        
        
        //For RESPONSES DUE 
        Messaging.InboundEmail email3  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env3 = new Messaging.InboundEnvelope();
        List<StaticResource> srList3 = [SELECT Id,Body FROM StaticResource WHERE Name='EGOSNITACCResponsesDue'];
        email3.plainTextBody = srList3.get(0).Body.toString();
        //System.debug('Body = '+email1.plainTextBody);
        email3.fromAddress ='test@test.com';
        email3.fromName = 'ABC XYZ';
        email3.subject = 'Re:Response due for RFQ CS-43412';
        email3.toaddresses = new List<String>();
        email3.toaddresses.add('egos_nitacc@1ojfakrb1c3pn3ttqjxrj0oixg9snklugvw1mpq799uet5wen7.61-zhsweao.na34.apex.salesforce.com');
        
        
        EGOS_NITACC_EmailHandler handler = new EGOS_NITACC_EmailHandler();
        Test.startTest();
        Messaging.InboundEmailResult result = handler.handleInboundEmail(email, env);
        List<Task_Order__c> taskOrderList = [SELECT Id FROM Task_Order__c];
        Messaging.InboundEmailResult result1 = handler.handleInboundEmail(email1, env1);
        List<Contract_Mods__c> contractModList = [SELECT Id FROM Contract_Mods__c];
        
        Messaging.InboundEmailResult result2 = handler.handleInboundEmail(email2, env2);  
        Messaging.InboundEmailResult result3 = handler.handleInboundEmail(email3, env3);
        Test.stopTest();
        
        System.assertEquals(1, taskOrderList.size());
        System.assertEquals(1, contractModList.size());

    }
}