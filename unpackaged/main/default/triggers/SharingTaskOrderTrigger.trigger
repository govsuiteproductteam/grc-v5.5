trigger SharingTaskOrderTrigger on Teaming_Partner__c (after update){    
    SharingTaskOrderTriggerHelper handler = new SharingTaskOrderTriggerHelper();
    handler.executeSharing(Trigger.new);
}