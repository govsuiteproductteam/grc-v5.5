global with sharing class TM_FeedbackAnswersPageController {
    
    public Id feedbackId {get;set;}
    public List<Feedback__c> feedbackList {get;set;}
    public List<Survey_Answer__c> surveyAnsList {get;set;}
    public List<surveyAnswrapper> surveyAnsWraList {get;set;}
    public Feedback__c feedbck {get;set;}
    public Id taskOrderId {get;set;}
    public String returnUrl{get;set;}
    private List<Task_Order_Questionnaire__c> communityUrlList;
	public String isValidUser{get; set;}
    
    static List<String> optionAPIList = new List<String>{'Option_1__c', 'Option_2__c', 'Option_3__c', 'Option_4__c', 'Option_5__c', 'Option_6__c', 'Option_7__c', 'Option_8__c', 'Option_9__c', 'Option_10__c','Answer_Descriptive__c'};
    
    public TM_FeedbackAnswersPageController(ApexPages.StandardController controller) { 
        isValidUser = LincenseKeyHelper.checkLicensePermitionForFedTOM();
        if(isValidUser != 'Yes')
            return;  
       if(Schema.sObjectType.Feedback__c.isAccessible() && Schema.sObjectType.Survey_Answer__c.isAccessible() && Schema.sObjectType.User.isAccessible()){ 
            init();
       }
    }
    
    // init() method is used to display feedback of respective partner and customer community user.
    public void init(){
        feedbackId = ApexPages.CurrentPage().getparameters().get('id');
        feedbackList = new List<Feedback__c>();
        surveyAnsList = new List<Survey_Answer__c>();
        feedbck = new Feedback__c();
        
        try{
            if(feedbackId != null){
                feedbackList = [SELECT id , User__c, Contact__r.name, Company_Name__r.name,Contact__c, Company_Name__c, Task_Order__c, Task_Order__r.Name, Survey_Task__c, Date_Response_Received__c, Date_Teaming_Request_sent__c FROM Feedback__c WHERE id =: feedbackId];                             
            }
            
            if(feedbackList.size()>0){
                feedbck = feedbackList[0];
                taskOrderId = feedbck.Task_Order__c;
            }
        }
        catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info,e.getMessage()));
        }
        
        surveyAnsList = [SELECT id,Question__c, Question__r.question__c, Answer_Descriptive__c, Feedback__c, Survey_Taken__c, Option_1__c, Option_2__c, Option_3__c, Option_4__c, Option_5__c, Option_6__c, Option_7__c, Option_8__c, Option_9__c, Option_10__c FROM Survey_Answer__c WHERE Feedback__c =: feedbackId Order by Sequence__c ASC];
                
        if(surveyAnsList != null && surveyAnsList.size()>0){
            surveyAnsWralist = new List<surveyAnsWrapper>();           
            for(Survey_Answer__c surveyAns : surveyAnsList){
                SurveyAnsWrapper surveyAnswerWrapper = new SurveyAnsWrapper(surveyAns);
                surveyAnswerWrapper.srNo = surveyAnsWralist.size()+1;
                surveyAnsWralist.add(surveyAnswerWrapper);                
            }
        }
        
        communityUrlList = [SELECT id, Customer_Community_URL__c, Partner_Community_URL__c FROM Task_Order_Questionnaire__c LIMIT 1];
        
            User usr = [SELECT id, contactId,Profile.UserLicense.Name FROM User WHERE id = : UserInfo.getUserId()];
            //This is temporary fix need to create custom setting for that and then URL present in Custom Setting
            //Need to create two custom settinf one for Customer comuninity and another for Partner community
            if(usr.Profile.UserLicense.Name.containsIgnoreCase('partner')  ){
                if(communityUrlList[0].Partner_Community_URL__c != null ){
                    returnUrl = communityUrlList[0].Partner_Community_URL__c+'/' + taskOrderId;
                }
            }
            else{
                if(usr.Profile.UserLicense.Name.containsIgnoreCase('Customer')){
                    if(communityUrlList[0].Customer_Community_URL__c  != null ){
                        returnUrl = communityUrlList[0].Customer_Community_URL__c +'/'+ taskOrderId;
                    }
                }
                else{
                    returnUrl = '/'+ taskOrderId;
                }
            }
        
       
    }
  
    /******************************* Wrapper class ***************************************/  
    public class SurveyAnsWrapper{
        public Survey_Answer__c surveyAnswer {get;set;}
        public String Question{get;set;}
        public List<String> answerList{get ;set;}
        public Integer srNo {get;set;}
        
        
        public SurveyAnsWrapper(Survey_Answer__c surveyanswer){
            this.surveyAnswer = surveyAnswer;         
            answerList = new List<String>();
            
            for(String str : TM_FeedbackAnswersPageController.optionAPIList ){
                String optionAnswer =(String) surveyanswer.get(str);
                if(optionAnswer != null && optionAnswer.length() > 0){
                    answerList.add(optionAnswer );
                }
            }
            
        }
    }
    /*************************************************************************************/
}