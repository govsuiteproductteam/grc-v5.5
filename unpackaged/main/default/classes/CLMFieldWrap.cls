public class CLMFieldWrap {
    @AuraEnabled
    public CLM_FIeld__c field{get;set;}
    @AuraEnabled
    public String fieldType{get;set;}
    @AuraEnabled
    public String viewFieldType{get;set;}
    @AuraEnabled
    public Integer order{get;set;}        
    @AuraEnabled
    public STring lookUpName{get;set;}
    @AuraEnabled
    public STring lookUpParentLabel{get;set;}
    @AuraEnabled
    public Boolean isEditable{get;set;}
    @AuraEnabled
    public Boolean isAccessible{get;set;} //newly added @ 22/10/2018 for field isAccessible
    @AuraEnabled
    public Boolean isRecordType{get;set;}
    @AuraEnabled
    public Boolean isOwner{get;set;}
    @AuraEnabled
    public Boolean isRequired{get;set;}
    @AuraEnabled
    public Boolean isDraftRequired{get;set;}
    @AuraEnabled
    public String fieldHelpText{get;set;}
    @AuraEnabled
    public Boolean isParent{get; set;}
    @AuraEnabled
    public Map<String,FieldDependancyHelper.DependantField> dependantFieldMap{get;set;}
    @AuraEnabled
    public String dependantFieldString{get;set;}
    @AuraEnabled
    public String className{get;set;}
    @AuraEnabled
    public Boolean isUnique{get;set;}
    @AuraEnabled
    public String fldValue{get;set;}//newlly added for Date DateTime and parcent field value 18-09-2018
    
    public CLMFieldWrap(CLM_FIeld__c field,string recordType){
        
        this.field = field;
        
        if(field.Create_Blank_Space__c != true){//newlly added 17-07-2018
            this.isRequired=field.Required__c;
            this.isDraftRequired=field.Draft_Required__c;
            //to make name field required alwats
            if(field.FieldApiName__c != null && field.FieldApiName__c=='Name'){
                this.isRequired=true;
            }
            this.order = ((Integer)field.Order__c) + 1;//*new Changes added +1 in field coz of consideration of the field from order 1 not from 0 
            if(! ContrctNewDynamicFieldsController.fieldsMapAccessible.isEmpty() && ContrctNewDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c) != null){
                this.isEditable= ContrctNewDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().iscreateable();
                this.isAccessible = ContrctNewDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().isAccessible(); //newly added @ 22/10/2018 for field isAccessible
                this.isUnique = ContrctNewDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().isUnique();
                this.fieldHelpText =ContrctNewDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().getInlineHelpText() == null ? '':  ContrctNewDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().getInlineHelpText();
                if(field.Required__c != true && String.ValueOf(ContrctNewDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().getType()) != 'BOOLEAN'){
                    this.isRequired = !(ContrctNewDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().isNillable());
                }else{
                    //  this.isEditable =true;
                    this.fieldHelpText = '';
                }
                
                Schema.SObjectField field1 = ContrctNewDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c);
                if(field1 != null){
                    Schema.DisplayType fldType = field1.getDescribe().getType();
                    this.fieldType = ''+fldType;
                }
            }
            if(field.ViewFieldApi__c == null || field.ViewFieldApi__c == ''){
                field.ViewFieldApi__c = field.FieldApiName__c;
            }
            if(! ContrctNewDynamicFieldsController.fieldsMapAccessible.isEmpty() && ContrctNewDynamicFieldsController.fieldsMapAccessible.get(field.ViewFieldApi__c) != null){
                Schema.SObjectField field1 = ContrctNewDynamicFieldsController.fieldsMapAccessible.get(field.ViewFieldApi__c);
                if(field1 != null){
                    Schema.DisplayType fldType = field1.getDescribe().getType();
                    this.viewFieldType = ''+fldType;
                }
            }
            
            String recId=recordType;
            Map<String,Map<String,FieldDependancyHelper.DependantField>> tempparentMap=ContrctNewDynamicFieldsController.getparentMap(recId);
            Map<String,FieldDependancyHelper.DependantField > dependantFieldMap = tempparentMap.get(field.FieldApiName__c.toLowerCase().trim());//ContrctNewDynamicFieldsController.parentMap.get(field.FieldApiName__c.toLowerCase().trim());
            System.debug('@@@dependantFieldMap '+dependantFieldMap);
            //System.debug(LoggingLevel.NONE,'dependantFieldMap For'+ field.FieldApiName__c +' = '+FieldDependancyHelper.parentMap.get(field.FieldApiName__c.toLowerCase()));
            if(dependantFieldMap == null){
                isParent = false;
                dependantFieldMap = new Map<String,FieldDependancyHelper.DependantField>();
            }
            else{
                isParent = true;
            }
            this.dependantFieldMap = dependantFieldMap;
            
            this.dependantFieldString = '';
            for(String strKey : dependantFieldMap.keyset()){
                this.dependantFieldString += this.dependantFieldMap.get(strKey).value+'SPLITVAL'+this.dependantFieldMap.get(strKey).className+'SPLITVAL'+this.dependantFieldMap.get(strKey).apiName+'SPLITREC';
            }
            
            String fieldClassesName = FieldDependancyHelper.childmap.get(field.FieldApiName__c);
            // System.debug('Field Api Name = ' + field.FieldApiName__c +'     fieldClassesName = '+fieldClassesName);
            if(fieldClassesName == null){
                fieldClassesName = '';
            }
            this.className = fieldClassesName;
            
            
        }
        
    }
    public CLMFieldWrap(CLM_FIeld__c field,string recordType,boolean isEdit){
        if(isEdit){
            this.field = field;
            
            if(field.Create_Blank_Space__c != true){//newlly added 17-07-2018
                this.isRequired=field.Required__c;
                this.isDraftRequired=field.Draft_Required__c;
                //to make name field required alwats
                if(field.FieldApiName__c != null && field.FieldApiName__c=='Name'){
                    this.isRequired=true;
                }
                this.order = ((Integer)field.Order__c) + 1;//*new Changes added +1 in field coz of consideration of the field from order 1 not from 0 
                if(! ContrctEditDynamicFieldsController.fieldsMapAccessible.isEmpty() && ContrctEditDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c) != null){
                    this.isEditable= ContrctEditDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().isUpdateable();
                    this.isAccessible = ContrctEditDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().isAccessible(); //newly added @ 22/10/2018 for field isAccessible
                    this.isUnique = ContrctEditDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().isUnique();
                    this.fieldHelpText =ContrctEditDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().getInlineHelpText() == null ? '':  ContrctEditDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().getInlineHelpText();
                    if(field.Required__c != true && String.ValueOf(ContrctEditDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().getType()) != 'BOOLEAN'){
                        this.isRequired = !(ContrctEditDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().isNillable());
                    }else{
                        //  this.isEditable =true;
                        this.fieldHelpText = '';
                    }
                    
                    Schema.SObjectField field1 = ContrctEditDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c);
                    if(field1 != null){
                        Schema.DisplayType fldType = field1.getDescribe().getType();
                        this.fieldType = ''+fldType;
                    }
                }
                if(field.ViewFieldApi__c == null || field.ViewFieldApi__c == ''){
                    field.ViewFieldApi__c = field.FieldApiName__c;
                }
                if(! ContrctEditDynamicFieldsController.fieldsMapAccessible.isEmpty() && ContrctEditDynamicFieldsController.fieldsMapAccessible.get(field.ViewFieldApi__c) != null){
                    Schema.SObjectField field1 = ContrctEditDynamicFieldsController.fieldsMapAccessible.get(field.ViewFieldApi__c);
                    if(field1 != null){
                        Schema.DisplayType fldType = field1.getDescribe().getType();
                        this.viewFieldType = ''+fldType;
                    }
                }
                
                String recId=recordType;
                Map<String,Map<String,FieldDependancyHelper.DependantField>> tempparentMap=ContrctEditDynamicFieldsController.getparentMap(recId);
                Map<String,FieldDependancyHelper.DependantField > dependantFieldMap = tempparentMap.get(field.FieldApiName__c.toLowerCase().trim());//ContrctNewDynamicFieldsController.parentMap.get(field.FieldApiName__c.toLowerCase().trim());
                System.debug('@@@dependantFieldMap '+dependantFieldMap);
                //System.debug(LoggingLevel.NONE,'dependantFieldMap For'+ field.FieldApiName__c +' = '+FieldDependancyHelper.parentMap.get(field.FieldApiName__c.toLowerCase()));
                if(dependantFieldMap == null){
                    isParent = false;
                    dependantFieldMap = new Map<String,FieldDependancyHelper.DependantField>();
                }
                else{
                    isParent = true;
                }
                this.dependantFieldMap = dependantFieldMap;
                
                this.dependantFieldString = '';
                for(String strKey : dependantFieldMap.keyset()){
                    this.dependantFieldString += this.dependantFieldMap.get(strKey).value+'SPLITVAL'+this.dependantFieldMap.get(strKey).className+'SPLITVAL'+this.dependantFieldMap.get(strKey).apiName+'SPLITREC';
                }
                
                String fieldClassesName = FieldDependancyHelper.childmap.get(field.FieldApiName__c);
                // System.debug('Field Api Name = ' + field.FieldApiName__c +'     fieldClassesName = '+fieldClassesName);
                if(fieldClassesName == null){
                    fieldClassesName = '';
                }
                this.className = fieldClassesName;
                
                
            }
            
        }
        else{
            this.field = field;
            
            if(field.Create_Blank_Space__c != true){//newlly added 17-07-2018
                this.isRequired=field.Required__c;
                this.isDraftRequired=field.Draft_Required__c;
                //to make name field required alwats
                if(field.FieldApiName__c != null && field.FieldApiName__c=='Name'){
                    this.isRequired=true;
                }
                this.order = ((Integer)field.Order__c) + 1;//*new Changes added +1 in field coz of consideration of the field from order 1 not from 0 
                if(! ContrctDynamicFieldsController.fieldsMapAccessible.isEmpty() && ContrctDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c) != null){
                    this.isEditable= ContrctDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().isUpdateable();
                    this.isAccessible = ContrctDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().isAccessible(); //newly added @ 22/10/2018 for field isAccessible
                    this.isUnique = ContrctDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().isUnique();
                    this.fieldHelpText =ContrctDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().getInlineHelpText() == null ? '':  ContrctDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().getInlineHelpText();
                    if(field.Required__c != true && String.ValueOf(ContrctDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().getType()) != 'BOOLEAN'){
                        this.isRequired = !(ContrctDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().isNillable());
                    }else{
                        //  this.isEditable =true;
                        this.fieldHelpText = '';
                    }
                    
                    Schema.SObjectField field1 = ContrctDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c);
                    if(field1 != null){
                        Schema.DisplayType fldType = field1.getDescribe().getType();
                        this.fieldType = ''+fldType;
                    }
                }
                if(field.ViewFieldApi__c == null || field.ViewFieldApi__c == ''){
                    field.ViewFieldApi__c = field.FieldApiName__c;
                }
                if(! ContrctDynamicFieldsController.fieldsMapAccessible.isEmpty() && ContrctDynamicFieldsController.fieldsMapAccessible.get(field.ViewFieldApi__c) != null){
                    Schema.SObjectField field1 = ContrctDynamicFieldsController.fieldsMapAccessible.get(field.ViewFieldApi__c);
                    if(field1 != null){
                        Schema.DisplayType fldType = field1.getDescribe().getType();
                        this.viewFieldType = ''+fldType;
                    }
                }
                
                String recId=recordType;
                Map<String,Map<String,FieldDependancyHelper.DependantField>> tempparentMap=ContrctDynamicFieldsController.getparentMap(recId);
                Map<String,FieldDependancyHelper.DependantField > dependantFieldMap = tempparentMap.get(field.FieldApiName__c.toLowerCase().trim());//ContrctNewDynamicFieldsController.parentMap.get(field.FieldApiName__c.toLowerCase().trim());
                System.debug('@@@dependantFieldMap '+dependantFieldMap);
                //System.debug(LoggingLevel.NONE,'dependantFieldMap For'+ field.FieldApiName__c +' = '+FieldDependancyHelper.parentMap.get(field.FieldApiName__c.toLowerCase()));
                if(dependantFieldMap == null){
                    isParent = false;
                    dependantFieldMap = new Map<String,FieldDependancyHelper.DependantField>();
                }
                else{
                    isParent = true;
                }
                this.dependantFieldMap = dependantFieldMap;
                
                this.dependantFieldString = '';
                for(String strKey : dependantFieldMap.keyset()){
                    this.dependantFieldString += this.dependantFieldMap.get(strKey).value+'SPLITVAL'+this.dependantFieldMap.get(strKey).className+'SPLITVAL'+this.dependantFieldMap.get(strKey).apiName+'SPLITREC';
                }
                
                String fieldClassesName = FieldDependancyHelper.childmap.get(field.FieldApiName__c);
                // System.debug('Field Api Name = ' + field.FieldApiName__c +'     fieldClassesName = '+fieldClassesName);
                if(fieldClassesName == null){
                    fieldClassesName = '';
                }
                this.className = fieldClassesName;
                
                
            }
            
        }
    }
    public CLMFieldWrap(CLM_FIeld__c field,string recordType,String ObjApi,String cmpType){
        if(cmpType == 'Detail'){
            System.debug('OBject API '+ObjApi);
            this.field = field;
            
            if(field.Create_Blank_Space__c != true){//newlly added 17-07-2018
                this.isRequired=field.Required__c;
                this.isDraftRequired=field.Draft_Required__c;
                //to make name field required alwats
                if(field.FieldApiName__c != null && field.FieldApiName__c=='Name'){
                    this.isRequired=true;
                }
                this.order = ((Integer)field.Order__c) + 1;//*new Changes added +1 in field coz of consideration of the field from order 1 not from 0 
                if(! SubContractDynamicFieldsController.fieldsMapAccessible.isEmpty() && SubContractDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c) != null){
                    this.isEditable= SubContractDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().isUpdateable();
                    this.isAccessible = SubContractDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().isAccessible(); //newly added @ 22/10/2018 for field isAccessible
                    this.isUnique = SubContractDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().isUnique();
                    this.fieldHelpText =SubContractDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().getInlineHelpText() == null ? '':  SubContractDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().getInlineHelpText();
                    if(field.Required__c != true && String.ValueOf(SubContractDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().getType()) != 'BOOLEAN'){
                        this.isRequired = !(SubContractDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().isNillable());
                    }else{
                        // this.isEditable =true;
                        this.fieldHelpText = '';
                    }
                    
                    Schema.SObjectField field1 = SubContractDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c);
                    if(field1 != null){
                        Schema.DisplayType fldType = field1.getDescribe().getType();
                        this.fieldType = ''+fldType;
                    }
                }
                if(field.ViewFieldApi__c == null || field.ViewFieldApi__c == ''){
                    field.ViewFieldApi__c = field.FieldApiName__c;
                }
                if(! SubContractDynamicFieldsController.fieldsMapAccessible.isEmpty() && SubContractDynamicFieldsController.fieldsMapAccessible.get(field.ViewFieldApi__c) != null){
                    Schema.SObjectField field1 = SubContractDynamicFieldsController.fieldsMapAccessible.get(field.ViewFieldApi__c);
                    if(field1 != null){
                        Schema.DisplayType fldType = field1.getDescribe().getType();
                        this.viewFieldType = ''+fldType;
                    }
                }
                
                String recId=recordType;
                Map<String,Map<String,FieldDependancyHelper.DependantField>> tempparentMap=SubContractDynamicFieldsController.getparentMap(recId);
                Map<String,FieldDependancyHelper.DependantField > dependantFieldMap = tempparentMap.get(field.FieldApiName__c.toLowerCase().trim());//ContrctDynamicFieldsController.parentMap.get(field.FieldApiName__c.toLowerCase().trim());
                System.debug('@@@dependantFieldMap '+dependantFieldMap);
                //System.debug(LoggingLevel.NONE,'dependantFieldMap For'+ field.FieldApiName__c +' = '+FieldDependancyHelper.parentMap.get(field.FieldApiName__c.toLowerCase()));
                if(dependantFieldMap == null){
                    isParent = false;
                    dependantFieldMap = new Map<String,FieldDependancyHelper.DependantField>();
                }
                else{
                    isParent = true;
                }
                this.dependantFieldMap = dependantFieldMap;
                
                this.dependantFieldString = '';
                for(String strKey : dependantFieldMap.keyset()){
                    this.dependantFieldString += this.dependantFieldMap.get(strKey).value+'SPLITVAL'+this.dependantFieldMap.get(strKey).className+'SPLITVAL'+this.dependantFieldMap.get(strKey).apiName+'SPLITREC';
                }
                
                String fieldClassesName = FieldDependancyHelper.childmap.get(field.FieldApiName__c);
                // System.debug('Field Api Name = ' + field.FieldApiName__c +'     fieldClassesName = '+fieldClassesName);
                if(fieldClassesName == null){
                    fieldClassesName = '';
                }
                this.className = fieldClassesName;
                
                
            }
            
        }
       if(cmpType == 'New'){
            System.debug('OBject API '+ObjApi);
            this.field = field;
            
            if(field.Create_Blank_Space__c != true){//newlly added 17-07-2018
                this.isRequired=field.Required__c;
                this.isDraftRequired=field.Draft_Required__c;
                //to make name field required alwats
                if(field.FieldApiName__c != null && field.FieldApiName__c=='Name'){
                    this.isRequired=true;
                }
                this.order = ((Integer)field.Order__c) + 1;//*new Changes added +1 in field coz of consideration of the field from order 1 not from 0 
                if(! SubContrctNewDynamicFieldsController.fieldsMapAccessible.isEmpty() && SubContrctNewDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c) != null){
                    this.isEditable= SubContrctNewDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().iscreateable();
                    this.isAccessible = SubContrctNewDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().isAccessible(); //newly added @ 22/10/2018 for field isAccessible
                    this.isUnique = SubContrctNewDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().isUnique();
                    this.fieldHelpText =SubContrctNewDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().getInlineHelpText() == null ? '':  SubContrctNewDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().getInlineHelpText();
                    if(field.Required__c != true && String.ValueOf(SubContrctNewDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().getType()) != 'BOOLEAN'){
                        this.isRequired = !(SubContrctNewDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().isNillable());
                    }else{
                        // this.isEditable =true;
                        this.fieldHelpText = '';
                    }
                    
                    Schema.SObjectField field1 = SubContrctNewDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c);
                    if(field1 != null){
                        Schema.DisplayType fldType = field1.getDescribe().getType();
                        this.fieldType = ''+fldType;
                    }
                }
                if(field.ViewFieldApi__c == null || field.ViewFieldApi__c == ''){
                    field.ViewFieldApi__c = field.FieldApiName__c;
                }
                if(! SubContrctNewDynamicFieldsController.fieldsMapAccessible.isEmpty() && SubContrctNewDynamicFieldsController.fieldsMapAccessible.get(field.ViewFieldApi__c) != null){
                    Schema.SObjectField field1 = SubContrctNewDynamicFieldsController.fieldsMapAccessible.get(field.ViewFieldApi__c);
                    if(field1 != null){
                        Schema.DisplayType fldType = field1.getDescribe().getType();
                        this.viewFieldType = ''+fldType;
                    }
                }
                
                String recId=recordType;
                Map<String,Map<String,FieldDependancyHelper.DependantField>> tempparentMap=SubContrctNewDynamicFieldsController.getparentMap(recId);
                Map<String,FieldDependancyHelper.DependantField > dependantFieldMap = tempparentMap.get(field.FieldApiName__c.toLowerCase().trim());//ContrctDynamicFieldsController.parentMap.get(field.FieldApiName__c.toLowerCase().trim());
                System.debug('@@@dependantFieldMap '+dependantFieldMap);
                //System.debug(LoggingLevel.NONE,'dependantFieldMap For'+ field.FieldApiName__c +' = '+FieldDependancyHelper.parentMap.get(field.FieldApiName__c.toLowerCase()));
                if(dependantFieldMap == null){
                    isParent = false;
                    dependantFieldMap = new Map<String,FieldDependancyHelper.DependantField>();
                }
                else{
                    isParent = true;
                }
                this.dependantFieldMap = dependantFieldMap;
                
                this.dependantFieldString = '';
                for(String strKey : dependantFieldMap.keyset()){
                    this.dependantFieldString += this.dependantFieldMap.get(strKey).value+'SPLITVAL'+this.dependantFieldMap.get(strKey).className+'SPLITVAL'+this.dependantFieldMap.get(strKey).apiName+'SPLITREC';
                }
                
                String fieldClassesName = FieldDependancyHelper.childmap.get(field.FieldApiName__c);
                // System.debug('Field Api Name = ' + field.FieldApiName__c +'     fieldClassesName = '+fieldClassesName);
                if(fieldClassesName == null){
                    fieldClassesName = '';
                }
                this.className = fieldClassesName;
                
                
            }
        }
        if(cmpType == 'Edit'){
                System.debug('OBject API '+ObjApi);
                this.field = field;
                
                if(field.Create_Blank_Space__c != true){//newlly added 17-07-2018
                    this.isRequired=field.Required__c;
                    this.isDraftRequired=field.Draft_Required__c;
                    //to make name field required alwats
                    if(field.FieldApiName__c != null && field.FieldApiName__c=='Name'){
                        this.isRequired=true;
                    }
                    this.order = ((Integer)field.Order__c) + 1;//*new Changes added +1 in field coz of consideration of the field from order 1 not from 0 
                    if(! SubContractEditDynamicFieldController.fieldsMapAccessible.isEmpty() && SubContractEditDynamicFieldController.fieldsMapAccessible.get(field.FieldApiName__c) != null){
                        this.isEditable= SubContractEditDynamicFieldController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().isUpdateable();
                        this.isAccessible = SubContractEditDynamicFieldController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().isAccessible(); //newly added @ 22/10/2018 for field isAccessible
                        this.isUnique = SubContractEditDynamicFieldController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().isUnique();
                        this.fieldHelpText =SubContractEditDynamicFieldController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().getInlineHelpText() == null ? '':  SubContractEditDynamicFieldController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().getInlineHelpText();
                        if(field.Required__c != true && String.ValueOf(SubContractEditDynamicFieldController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().getType()) != 'BOOLEAN'){
                            this.isRequired = !(SubContractEditDynamicFieldController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().isNillable());
                        }else{
                            // this.isEditable =true;
                            this.fieldHelpText = '';
                        }
                        
                        Schema.SObjectField field1 = SubContractEditDynamicFieldController.fieldsMapAccessible.get(field.FieldApiName__c);
                        if(field1 != null){
                            Schema.DisplayType fldType = field1.getDescribe().getType();
                            this.fieldType = ''+fldType;
                        }
                    }
                    if(field.ViewFieldApi__c == null || field.ViewFieldApi__c == ''){
                        field.ViewFieldApi__c = field.FieldApiName__c;
                    }
                    if(! SubContractEditDynamicFieldController.fieldsMapAccessible.isEmpty() && SubContractEditDynamicFieldController.fieldsMapAccessible.get(field.ViewFieldApi__c) != null){
                        Schema.SObjectField field1 = SubContractEditDynamicFieldController.fieldsMapAccessible.get(field.ViewFieldApi__c);
                        if(field1 != null){
                            Schema.DisplayType fldType = field1.getDescribe().getType();
                            this.viewFieldType = ''+fldType;
                        }
                    }
                    
                    String recId=recordType;
                    Map<String,Map<String,FieldDependancyHelper.DependantField>> tempparentMap=SubContractEditDynamicFieldController.getparentMap(recId);
                    Map<String,FieldDependancyHelper.DependantField > dependantFieldMap = tempparentMap.get(field.FieldApiName__c.toLowerCase().trim());//ContrctDynamicFieldsController.parentMap.get(field.FieldApiName__c.toLowerCase().trim());
                    System.debug('@@@dependantFieldMap '+dependantFieldMap);
                    //System.debug(LoggingLevel.NONE,'dependantFieldMap For'+ field.FieldApiName__c +' = '+FieldDependancyHelper.parentMap.get(field.FieldApiName__c.toLowerCase()));
                    if(dependantFieldMap == null){
                        isParent = false;
                        dependantFieldMap = new Map<String,FieldDependancyHelper.DependantField>();
                    }
                    else{
                        isParent = true;
                    }
                    this.dependantFieldMap = dependantFieldMap;
                    
                    this.dependantFieldString = '';
                    for(String strKey : dependantFieldMap.keyset()){
                        this.dependantFieldString += this.dependantFieldMap.get(strKey).value+'SPLITVAL'+this.dependantFieldMap.get(strKey).className+'SPLITVAL'+this.dependantFieldMap.get(strKey).apiName+'SPLITREC';
                    }
                    
                    String fieldClassesName = FieldDependancyHelper.childmap.get(field.FieldApiName__c);
                    // System.debug('Field Api Name = ' + field.FieldApiName__c +'     fieldClassesName = '+fieldClassesName);
                    if(fieldClassesName == null){
                        fieldClassesName = '';
                    }
                    this.className = fieldClassesName;
                    
                    
                }
            }
        
    }
    /* public CLMFieldWrap(CLM_FIeld__c field,string recordType,String ObjApi){
System.debug('OBject API '+ObjApi);
this.field = field;

if(field.Create_Blank_Space__c != true){//newlly added 17-07-2018
this.isRequired=field.Required__c;
this.isDraftRequired=field.Draft_Required__c;
//to make name field required alwats
if(field.FieldApiName__c != null && field.FieldApiName__c=='Name'){
this.isRequired=true;
}
this.order = ((Integer)field.Order__c) + 1;//*new Changes added +1 in field coz of consideration of the field from order 1 not from 0 
if(! SubContractDynamicFieldsController.fieldsMapAccessible.isEmpty() && SubContractDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c) != null){
this.isEditable= SubContractDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().isUpdateable();
this.isUnique = SubContractDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().isUnique();
this.fieldHelpText =SubContractDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().getInlineHelpText() == null ? '':  SubContractDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().getInlineHelpText();
if(field.Required__c != true && String.ValueOf(SubContractDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().getType()) != 'BOOLEAN'){
this.isRequired = !(SubContractDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().isNillable());
}else{
// this.isEditable =true;
this.fieldHelpText = '';
}

Schema.SObjectField field1 = SubContractDynamicFieldsController.fieldsMapAccessible.get(field.FieldApiName__c);
if(field1 != null){
Schema.DisplayType fldType = field1.getDescribe().getType();
this.fieldType = ''+fldType;
}
}
if(field.ViewFieldApi__c == null || field.ViewFieldApi__c == ''){
field.ViewFieldApi__c = field.FieldApiName__c;
}
if(! SubContractDynamicFieldsController.fieldsMapAccessible.isEmpty() && SubContractDynamicFieldsController.fieldsMapAccessible.get(field.ViewFieldApi__c) != null){
Schema.SObjectField field1 = SubContractDynamicFieldsController.fieldsMapAccessible.get(field.ViewFieldApi__c);
if(field1 != null){
Schema.DisplayType fldType = field1.getDescribe().getType();
this.viewFieldType = ''+fldType;
}
}

String recId=recordType;
Map<String,Map<String,FieldDependancyHelper.DependantField>> tempparentMap=SubContractDynamicFieldsController.getparentMap(recId);
Map<String,FieldDependancyHelper.DependantField > dependantFieldMap = tempparentMap.get(field.FieldApiName__c.toLowerCase().trim());//ContrctDynamicFieldsController.parentMap.get(field.FieldApiName__c.toLowerCase().trim());
System.debug('@@@dependantFieldMap '+dependantFieldMap);
//System.debug(LoggingLevel.NONE,'dependantFieldMap For'+ field.FieldApiName__c +' = '+FieldDependancyHelper.parentMap.get(field.FieldApiName__c.toLowerCase()));
if(dependantFieldMap == null){
isParent = false;
dependantFieldMap = new Map<String,FieldDependancyHelper.DependantField>();
}
else{
isParent = true;
}
this.dependantFieldMap = dependantFieldMap;

this.dependantFieldString = '';
for(String strKey : dependantFieldMap.keyset()){
this.dependantFieldString += this.dependantFieldMap.get(strKey).value+'SPLITVAL'+this.dependantFieldMap.get(strKey).className+'SPLITVAL'+this.dependantFieldMap.get(strKey).apiName+'SPLITREC';
}

String fieldClassesName = FieldDependancyHelper.childmap.get(field.FieldApiName__c);
// System.debug('Field Api Name = ' + field.FieldApiName__c +'     fieldClassesName = '+fieldClassesName);
if(fieldClassesName == null){
fieldClassesName = '';
}
this.className = fieldClassesName;


}

}*/
    public CLMFieldWrap(CLM_FIeld__c field){//,string recordTypeId
    }   
    
}