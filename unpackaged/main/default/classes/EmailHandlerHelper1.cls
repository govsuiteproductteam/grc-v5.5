public with sharing class EmailHandlerHelper1{
    
    public static void insertBinaryAttachment(List<Task_Order__c> taskOrderList,Messaging.InboundEmail email){
        if(Schema.sObjectType.Attachment.isAccessible()){
            Messaging.InboundEmail.BinaryAttachment[] binaryAttachmentList = email.binaryAttachments;
            System.debug('Binary Attachment Size Internal ');
            
            if(binaryAttachmentList != null && binaryAttachmentList.size() > 0 && taskOrderList != null && taskOrderList.size() > 0){
                System.debug('Binary Attachment Size = '+binaryAttachmentList.size());
                
                List<Attachment> attachmentList = new List<Attachment>();
                for(Task_Order__c taskOrder : taskOrderList){
                    for(Messaging.InboundEmail.BinaryAttachment binaryAttachment : binaryAttachmentList ){
                        System.debug('Binary Attachment Size Internal 11 ');
                        Attachment attach = new Attachment();
                        attach.parentId = taskOrder.id;
                        attach.body = binaryAttachment.body;
                        attach.Name =  binaryAttachment.fileName;
                        attachmentList.add(attach);
                    }
                }
                System.debug('attachmentList = '+attachmentList.size());
                if(!attachmentList.isEmpty()){
                    System.debug('Before Insert');
                    DMLManager.insertAsUser(attachmentList);
                    System.debug('After Insert');
                }
            }
        }
    }
    
    public static  void insertTextAttachment(List<Task_Order__c> taskOrderList,Messaging.InboundEmail email){
        if(Schema.sObjectType.Attachment.isAccessible()){
        Messaging.InboundEmail.TextAttachment[] textAttachmentList = email.textAttachments;
        System.debug('Binary Attachment Size Internal ');
        if(textAttachmentList != null && textAttachmentList .size() > 0 && taskOrderList != null && taskOrderList.size() > 0){
            System.debug('Binary Attachment Size = '+textAttachmentList .size());
            
            List<Attachment> attachmentList = new List<Attachment>();
            for(Task_Order__c taskOrder : taskOrderList){
                for(Messaging.InboundEmail.TextAttachment textAttachment : textAttachmentList ){
                    System.debug('Binary Attachment Size Internal 11 ');
                    Attachment attach = new Attachment();
                    attach.parentId = taskOrder.id;
                    attach.body = Blob.valueOf(textAttachment.body);
                    attach.Name =  textAttachment.fileName;
                    attachmentList.add(attach);
                }
            }
            System.debug('attachmentList = '+attachmentList.size());
            if(!attachmentList.isEmpty()){
                System.debug('Before Insert');
                DMLManager.insertAsUser(attachmentList);
                System.debug('After Insert');
            }
        }
        }
    }
    
    public static  void insertActivityHistory(List<Task_Order__c> taskOrderList,Messaging.InboundEmail email){
        List<Task> newTaskList = new List<Task>();
        for(Task_Order__c taskOrder : taskOrderList){
            Task newTask = new Task();
            String emailSubject = email.Subject;
            if(emailSubject.length()>255){
                emailSubject = emailSubject.substring(0,245);//245 beacause we are apending "Email : " and the field limit is 255 
            }
            newTask.Subject = 'Email : '+emailSubject;
            //String testBodyString = email.plainTextBody;
            String taskBody = 'From : ';
            if(email.fromAddress != null){
                taskBody += email.fromAddress;
            }
            taskBody += '\nCC:';
            if(email.ccAddresses != null){
                taskBody += email.ccAddresses;
            }
            taskBody +='\n'+ 'BCC:\n'+
                'Subject: '+email.Subject+'\n'+ 'Body: '+email.plainTextBody;
            newTask.description = taskBody;
            newTask.OwnerId =UserInfo.getUserId();
            newTask.WhatId = taskOrder.id;
            newTask.priority = 'Normal';
            newTask.status = 'Completed'; 
            newTask.TaskSubtype = 'Email';
            newTaskList.add(newTask);            
        }
        try{
            DMLManager.insertAsUser(newTaskList);
        }
        catch(Exception ex){
            System.debug('Error on task creation = '+ex);
        }
    }
 public static void insertContractMods(List<Task_Order__c> updatedTaskOrderList,List<Task_Order__c> oldTaskOrderList,Messaging.InboundEmail email){
        if(Schema.sObjectType.Contract_Mods__c.isAccessible()){         
            List<Contract_Mods__c> contractModList = new List<Contract_Mods__c>();
            List<Id> toIds = new List<Id>();
            for(Task_Order__c to : updatedTaskOrderList){
                toIds.add(to.id);
            }
            List<Task_Order__c> taskOrderList = [SELECT Id,Name,Email_Reference__c, Task_Order_Title__c,Questions_Due_Date__c, Due_Date__c, Release_Date__c,Contracting_Officer__c,Contract_Specialist__c,ASB_Action__c,Description__c,Start_Date__c,Stop_Date__c,
                                                 Contract_Negotiator__c,Proposal_Due_DateTime__c
                                                 FROM Task_Order__c where id =: toIds];
            System.debug('**size ='+taskOrderList.size()+'\t'+oldTaskOrderList.size());      
            if(taskOrderList != null && oldTaskOrderList != null){
                Integer taskOrderListSize = taskOrderList.size();
                for(Integer i = 0; i<taskOrderListSize; i++){
                    Contract_Mods__c cm = new Contract_Mods__c();
                    cm.RecordTypeId = Schema.SObjectType.Contract_Mods__c.getRecordTypeInfosByName().get('Update').getRecordTypeId();
                    
                    //cm.Task_Order_Number__c = taskOrder.Name;
                    //cm.TaskOrder__c = taskOrderList[i].Id;
                    cm.TaskOrder__c = toIds[i];
                    System.debug(cm.TaskOrder__c);
                    Boolean flag = false;
                    System.debug('Task_Order_Title__c = '+ oldTaskOrderList[i].Task_Order_Title__c);
                    System.debug('Task_Order_Title__c = '+ taskOrderList[i].Task_Order_Title__c);
                    if(taskOrderList[i].Task_Order_Title__c != oldTaskOrderList[i].Task_Order_Title__c){
                        cm.Task_Order_Title__c = oldTaskOrderList[i].Task_Order_Title__c;
                        cm.New_Task_Order_Title__c = taskOrderList[i].Task_Order_Title__c ;
                        System.debug('In Task Order Title');
                        flag = true;
                    }
                    System.debug('Due_Date__c = '+ oldTaskOrderList[i].Due_Date__c );
                    System.debug('Due_Date__c = '+ taskOrderList[i].Due_Date__c );
                    if(taskOrderList[i].Due_Date__c != oldTaskOrderList[i].Due_Date__c){
                        cm.Due_Date__c = oldTaskOrderList[i].Due_Date__c;
                        cm.New_Due_Date__c = taskOrderList[i].Due_Date__c;
                        System.debug('In Proposal Due Date');
                        flag = true;
                    }
                    
                    System.debug('Proposal_Due_DateTime__c= '+ oldTaskOrderList[i].Proposal_Due_DateTime__c);
                    System.debug('Proposal_Due_DateTime__c= '+ taskOrderList[i].Proposal_Due_DateTime__c);
                    if(taskOrderList[i].Proposal_Due_DateTime__c!= oldTaskOrderList[i].Proposal_Due_DateTime__c){
                        cm.Proposal_Due_Date_Time__c= oldTaskOrderList[i].Proposal_Due_DateTime__c;
                        cm.New_Proposal_Due_Date_Time__c= taskOrderList[i].Proposal_Due_DateTime__c;
                        System.debug('In Proposal Due Date');
                        flag = true;
                    }
                    
                    System.debug('Questions_Due_Date__c = '+ oldTaskOrderList[i].Questions_Due_Date__c);
                    System.debug('Questions_Due_Date__c = '+ taskOrderList[i].Questions_Due_Date__c);
                    if(taskOrderList[i].Questions_Due_Date__c != oldTaskOrderList[i].Questions_Due_Date__c){
                        
                        cm.Questions_Due_Date__c = oldTaskOrderList[i].Questions_Due_Date__c;
                        cm.New_Questions_Due_Date__c = taskOrderList[i].Questions_Due_Date__c;
                        System.debug('In Proposal Due Date');
                        flag = true;
                    }
                    
                    System.debug('Release_Date__c = '+ oldTaskOrderList[i].Release_Date__c );
                    System.debug('Release_Date__c = '+ taskOrderList[i].Release_Date__c );
                    if(taskOrderList[i].Release_Date__c!= oldTaskOrderList[i].Release_Date__c){
                        cm.Release_Date__c = oldTaskOrderList[i].Release_Date__c;
                        cm.New_Release_Date__c  = taskOrderList[i].Release_Date__c;
                        System.debug('In Release Date');
                        flag = true;
                    }
                    System.debug('Contracting_Officer__c= '+ oldTaskOrderList[i].Contracting_Officer__c);
                    System.debug('Contracting_Officer__c= '+ taskOrderList[i].Contracting_Officer__c);
                    if(taskOrderList[i].Contracting_Officer__c != oldTaskOrderList[i].Contracting_Officer__c){
                        cm.Contracting_Officer__c = oldTaskOrderList[i].Contracting_Officer__c;
                        cm.New_Contracting_Officer__c = taskOrderList[i].Contracting_Officer__c;
                        System.debug('In Contracting Officer');
                        flag = true;
                    }
                    System.debug('Contract_Specialist__c = '+ oldTaskOrderList[i].Contract_Specialist__c );
                    System.debug('Contract_Specialist__c = '+ taskOrderList[i].Contract_Specialist__c );
                    if(taskOrderList[i].Contract_Specialist__c != oldTaskOrderList[i].Contract_Specialist__c){
                        cm.Contract_Specialist__c = oldTaskOrderList[i].Contract_Specialist__c;
                        cm.New_Contract_Specialist__c = taskOrderList[i].Contract_Specialist__c;
                        System.debug('In Contract Specialist');
                        flag = true;
                    }
                    
                    System.debug('Contract_Negotiator__c = '+ oldTaskOrderList[i].Contract_Negotiator__c);
                    System.debug('Contract_Negotiator__c = '+ taskOrderList[i].Contract_Negotiator__c);
                    if(taskOrderList[i].Contract_Negotiator__c != oldTaskOrderList[i].Contract_Negotiator__c){
                        cm.Contract_Negotiator__c= oldTaskOrderList[i].Contract_Negotiator__c;
                        cm.New_Contract_Negotiator__c = taskOrderList[i].Contract_Negotiator__c;
                        System.debug('In Contract Specialist');
                        flag = true;
                    }
                    
                    System.debug('ASB_Action__c  = '+ oldTaskOrderList[i].ASB_Action__c );
                    System.debug('ASB_Action__c = '+ taskOrderList[i].ASB_Action__c );
                    if(taskOrderList[i].ASB_Action__c!= oldTaskOrderList[i].ASB_Action__c){
                        cm.ASB_Action__c = oldTaskOrderList[i].ASB_Action__c;
                        cm.New_ASB_Action__c = taskOrderList[i].ASB_Action__c;
                        System.debug('In ASB Action');
                        flag = true;
                    }
                    
                    System.debug('Description__c  = '+ oldTaskOrderList[i].Description__c );
                    System.debug('Description__c  = '+ taskOrderList[i].Description__c );
                    if( taskOrderList[i].Description__c != oldTaskOrderList[i].Description__c){
                        cm.Description__c= oldTaskOrderList[i].Description__c;
                        cm.New_Description__c= taskOrderList[i].Description__c;
                        System.debug('In Description');
                        flag = true;
                    }
                    
                    System.debug('Start_Date__c = '+ oldTaskOrderList[i].Start_Date__c );
                    System.debug('Start_Date__c = '+ taskOrderList[i].Start_Date__c );
                    if(taskOrderList[i].Start_Date__c!= oldTaskOrderList[i].Start_Date__c){
                        cm.Start_Date__c = oldTaskOrderList[i].Start_Date__c ;
                        cm.New_Start_Date__c = taskOrderList[i].Start_Date__c ;
                        System.debug('In Start Date');
                        flag = true;
                    }
                    
                    System.debug('Stop_Date__c = '+ oldTaskOrderList[i].Stop_Date__c );
                    System.debug('Stop_Date__c = '+ taskOrderList[i].Stop_Date__c);
                    if(taskOrderList[i].Stop_Date__c != oldTaskOrderList[i].Stop_Date__c){
                        cm.Stop_Date__c= oldTaskOrderList[i].Stop_Date__c;
                        cm.New_Stop_Date__c= taskOrderList[i].Stop_Date__c;
                        System.debug('In Stop Date');
                        flag = true;
                    }
                    
                    if(email != null && email.htmlBody != null){
                        cm.Email_Body__c = email.htmlBody;
                        System.debug('In Email Body');
                    }
                    
                    if(flag &&  cm.TaskOrder__c != null){
                        
                        System.debug('Insert Contract Modes');
                        contractModList.add(cm);
                    }
                }
            }
            if(!contractModList.isEmpty())
                DMLManager.insertAsUser(contractModList);
        }
    }
}