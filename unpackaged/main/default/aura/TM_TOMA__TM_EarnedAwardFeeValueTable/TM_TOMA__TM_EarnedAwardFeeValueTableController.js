({
    
    doCheckLicense : function(component, event, helper) {
        var action = component.get("c.checkLicensePermition1");
        action.setCallback(this, function(a) {
            //alert('doCheckLicense');
            if(a.getReturnValue() === 'Yes')
            {
                // alert('inside if');
                component.set("v.CheckLicense", 'Yes');
                helper.getContractIncVehicle1(component, event, helper);
                helper.EarnedAwardTableList(component, event, helper); // called in helper
                helper.checkValueTablePermissions(component, event, helper); // Check Value Table Permissions
            }
            else
            {
                // alert('inside else');
                component.set("v.CheckLicense", a.getReturnValue());
            }
            
        });
        
        $A.enqueueAction(action);
    },    
    
    addIncModFeeRow : function(component, event, helper) {
        event.preventDefault();
        var earnedAwardTableList = component.get("v.valueTableEarnedAwardTableList");
        var action = component.get("c.newRowEarnedAwardFeeTable"); //call apex controller method
        
        action.setParams({           
            "contractVehicalId": component.get("v.incModFeeRecordId")
        });
        action.setCallback(this,function(a){
            var state= a.getState();
            if(state=="SUCCESS"){
                var newConVal=a.getReturnValue();
                // "If" condition and "Else" block are newly added to restrict user access according to the object permission(17/08/2018)
                if(newConVal != null){
                    newConVal.TM_TOMA__Start_Date__c='';
                    newConVal.TM_TOMA__End_Date__c='';
                    earnedAwardTableList.push(newConVal);
                    component.set("v.valueTableEarnedAwardTableList",earnedAwardTableList);
                }else{
                    alert('ERROR:- You do not have CREATE permission for Value Table object.'); // New Change - 17/08/2018
                }
            }
            else if (state == "ERROR") 
            {
                console.log(a.getError());            
            }
        });
        $A.enqueueAction(action);       
    },
    
    getContractIncVehicle : function(component, event, helper) {
        helper.getContractIncVehicle1(component, event, helper);
    },
    
    valueTableEarnedAwardList : function(component, event, helper) {
        helper.EarnedAwardTableList(component, event, helper); // called in helper
    },
    removeIncModFeeRow  : function(component, event, helper) {
        var conf = confirm("Are you sure you want to delete this component?");
        if(conf== true)
        {
            var earnedAwardTableList =component.get("v.valueTableEarnedAwardTableList");       
            var delIncEarnedAwardTableIds =component.get("v.delIncEarnedAwardTableIds");       
            var conValIncModTablesize=component.get("v.valueTableEarnedAwardSizeFlag");
            var selectedItem = event.currentTarget;
            var removeCounter = selectedItem.id;
            if (removeCounter < conValIncModTablesize) {
                delIncEarnedAwardTableIds+=',';
                conValIncModTablesize-=1;
                component.set("v.valueTableEarnedAwardSizeFlag",conValIncModTablesize);
                delIncEarnedAwardTableIds =delIncEarnedAwardTableIds+earnedAwardTableList[removeCounter].Id;
                component.set("v.delIncEarnedAwardTableIds",delIncEarnedAwardTableIds);
            } 
            earnedAwardTableList.splice(removeCounter, 1);
            component.set("v.valueTableEarnedAwardTableList", earnedAwardTableList);
        }
    },
    //save and json method remaining below.
    saveIncModFeeTable : function(component, event, helper) {
        event.preventDefault();
        helper.saveModFeeTableList(component, event, helper); // Defined in helper
    },
})