public class TM_PageLayoutTab {
    private TM_PageLayoutController plController;
    // Add tab veriables
    public transient String congaApiName{get;set;}
    public transient String bgColor{get;set;}
    //public Integer order{get;set;}
    public transient String tabName{get;set;}
    public transient String tabStatus{get;set;}
    public transient Integer tabNumUpdate{get;set;}
    
    //Delete Tab Veriables
    public transient Integer tabNumberToDelete{get;set;}
    
    //Add Button Veriable
    /*public transient String buttonLabel{get;set;}
    public transient String buttonApiName{get;set;}
    public transient String buttonAction{get;set;}
    public transient String buttonJSAction{get;set;}
    
    //Update / delette Button Veriables
    public transient Integer buttonNuUpdateDelete{get;set;}*/
    
    
    public TM_PageLayoutTab(TM_PageLayoutController plController){
        this.plController = plController;
    }
    
    /*public void addCustomButton(){
        if(buttonLabel != Null && buttonLabel != ''){
            if((buttonApiName != null && buttonApiName != '' && buttonAction != null && buttonAction != '' )|| (buttonJSAction != null && buttonJSAction != '')){
                if(buttonNuUpdateDelete != Null && buttonNuUpdateDelete>0){
                    buttonNuUpdateDelete -= 1;
                    try{
                        if(plController.customButtonWrapperList != Null){
                            TM_PageLayoutController.CustomButtonWrapper tempButtoWrap = plController.customButtonWrapperList[buttonNuUpdateDelete];
                            tempButtoWrap.customButton.ButtonLabel__c = buttonLabel;
                            tempButtoWrap.customButton.ButtonApiName__c = buttonApiName;
                            tempButtoWrap.customButton.Action__c = buttonAction;
                            tempButtoWrap.customButton.JavaScriptAction__c = buttonJSAction;                    
                        }
                    }catch(Exception e){}
                }else{
                    TM_CustomButtonObject__c custButton = new TM_CustomButtonObject__c(ButtonLabel__c=buttonLabel,ButtonApiName__c=buttonApiName,Action__c=buttonAction,JavaScriptAction__c=buttonJSAction);
                    plController.addCustomButton(custButton);
                }
            }
        }
    }*/
    
    public void addTab(){
        if(tabName != Null && tabName !=''){
            System.debug(tabNumUpdate);
            if(tabNumUpdate!=Null && tabNumUpdate>0){
                plController.tabLanding = tabNumUpdate;
                tabNumUpdate -= 1;
                try{
                    if(plController.tabWrapperList != Null){
                        TM_PageLayoutController.TabWrapper tempTabWrap = plController.tabWrapperList[tabNumUpdate];
                        tempTabWrap.vfCustomTab.TabName__c = tabName;
                        tempTabWrap.vfCustomTab.BackgroundColor__c = bgColor;
                        //tempTabWrap.vfCustomTab.ApiName_for_CongaField__c = congaApiName;
                        tempTabWrap.vfCustomTab.TabStatus__c = tabStatus;                    
                    }
                }catch(Exception e){}
                
            }else{
                CLM_Tab__c tab = new CLM_Tab__c(TabName__c=tabName, BackgroundColor__c = bgColor,TabStatus__c = tabStatus);
                plController.addTab(tab);
            }
        }
    }
    
    public void deleteTab(){
        if(tabNumberToDelete!=Null){
            plController.deleteTab(tabNumberToDelete);
        }
    }
    
    /*public void deleteCustButton(){
        if(buttonNuUpdateDelete!=Null){
            plController.deleteCustButton(buttonNuUpdateDelete);
        }
    }*/
    
    //Rearrange Methods
    public void tabRearrange(){
        
        plController.tabRearrangeList.clear();
       
        for(TM_PageLayoutController.TabWrapper tempTabWrap : plController.tabWrapperList){
            plController.tabRearrangeList.add(tempTabWrap.vfCustomTab.TabName__c);
        }
    }
    
    /*public void custButtonRearrange(){
        
        plController.tabRearrangeList.clear();
       
        for(TM_PageLayoutController.CustomButtonWrapper tempbuttonWrap : plController.customButtonWrapperList){
            plController.tabRearrangeList.add(tempbuttonWrap.customButton.ButtonLabel__c);
        }
    }*/
    
    public void applytabRearrange(){
        if(plController.tabRearrangeString != Null){
            Integer i=1;
            for(String reTabName : plController.tabRearrangeString.split('SPLITXCOMMA')){//newlly added split('SPLITXCOMMA') in place of split(',') 17-05-2018
                if(reTabName!=''){
                    if(reTabName.contains('&amp;')){
                        reTabName.replace('&amp;','&');
                    }
                    for(TM_PageLayoutController.TabWrapper rtabWrap : plController.tabWrapperList){
                        if(rtabWrap.vfCustomTab.TabName__c.replace(' ','')+''+rtabWrap.tabOrder == reTabName.replace(' ','')){
                            rtabWrap.tabOrder = i;
                            break;
                        }
                    }
                    i++;
                }                
            }
            plController.tabWrapperList.sort();
            plController.tabRearrangeList.clear();
            plController.tabLanding = 1;
        }
    }
    
    /*public void applyButtonRearrange(){
        if(plController.tabRearrangeString != Null){
            Integer i=1;
            for(String reButtonName : plController.tabRearrangeString.split(',')){
                if(reButtonName!=''){
                    if(reButtonName.contains('&amp;')){
                        reButtonName.replace('&amp;','&');
                    }
                    for(TM_PageLayoutController.CustomButtonWrapper rButtonWrap : plController.customButtonWrapperList){
                        if(rButtonWrap.customButton.ButtonLabel__c.replace(' ','')+''+rButtonWrap.buttonOrder == reButtonName.replace(' ','')){
                            rButtonWrap.buttonOrder = i;
                            break;
                        }
                    }
                    i++;
                }                
            }
            plController.customButtonWrapperList.sort();
            plController.tabRearrangeList.clear();
        }
    }*/
}