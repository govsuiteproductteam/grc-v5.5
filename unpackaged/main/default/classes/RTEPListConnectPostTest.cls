@isTest
public class RTEPListConnectPostTest {
    public static testMethod void testRTEPList(){
        StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'RTEPList' LIMIT 1];
        String body = sr.Body.toString();
        
        Test.startTest();  
        //scenario 1
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        req.requestBody = Blob.valueof(body);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        String urls = RTEPListConnectPost.doPost();
        System.assert(urls.length()>0);
       
        //scenario 2 
        body = '';
        req.requestBody = Blob.valueof(body);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        urls = RTEPListConnectPost.doPost();
        System.assert(urls == '');
        Test.stopTest();
        
    }
    
}