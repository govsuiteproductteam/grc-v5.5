public with sharing class TM_ContractValueTableController {
    public static Id contValRecordTypeId;
    public static Value_Table__c valueTable1{get;set;}
    @AuraEnabled public Boolean isCreateValueTable {get;set;}
    @AuraEnabled public Boolean isUpdateValueTable {get;set;}
    @AuraEnabled public Boolean isDeleteValueTable {get;set;}
    @AuraEnabled public Boolean isUpdateContractVehical {get;set;}
    
    @AuraEnabled
    public static TM_ContractValueTableController getValueTablePermissions(){
        TM_ContractValueTableController contractValueTableCtrl = new TM_ContractValueTableController();
        contractValueTableCtrl.isCreateValueTable = false;
        contractValueTableCtrl.isUpdateValueTable = false;
        contractValueTableCtrl.isDeleteValueTable = false;
        contractValueTableCtrl.isUpdateContractVehical = false;
        
        if(Schema.sObjectType.Value_Table__c.isCreateable())
            contractValueTableCtrl.isCreateValueTable = true;
        
        if(Schema.sObjectType.Value_Table__c.isUpdateable())
            contractValueTableCtrl.isUpdateValueTable = true;
        
        if(Schema.sObjectType.Value_Table__c.isDeletable())
            contractValueTableCtrl.isDeleteValueTable = true;
        
        if(Schema.sObjectType.Contract_Vehicle__c.isUpdateable())
            contractValueTableCtrl.isUpdateContractVehical = true;
        return contractValueTableCtrl;
    }
    @AuraEnabled
    public static String checkLicensePermition1()
    {
        return LincenseKeyHelper.checkLicensePermitionForFedCLM();
    }
    @AuraEnabled
    public static Contract_Vehicle__c getContractVehical(Id recordId)
    {
        if(Schema.sObjectType.Contract_Vehicle__c.isAccessible()){
            Contract_Vehicle__c contVech = new Contract_Vehicle__c();
            if(recordId!=Null)
            {
                contVech = [select Id,Total_Contract_Value_with_all_Options__c,Total_Exercised_Value_with_Options__c,Initial_Contract_Funding_at_Award__c,Contract_Status_Backend__c from Contract_Vehicle__c where Id =: recordId];
            }
            
            return contVech;
        }
        else{
            return null;
        }
        
    }
    
    @AuraEnabled
    public static Boolean isLightningPage(){
        Boolean var = UserInfo.getUiThemeDisplayed() == 'Theme4d';
        return var;
    } 
    
    @AuraEnabled
    public static String saveConValTable(String contValList,Id recordId,string delConValIdsStr,Contract_Vehicle__c contractRec)
    {
        String errorMessage = '';
        
        System.debug('contValList '+contValList);
        System.debug('recordId '+recordId);
        System.debug('delConValIdsStr '+delConValIdsStr);
        try
        {
            if(delConValIdsStr!=Null && delConValIdsStr!='')
            {
                // "If" condition and "Else" block are newly added to restrict user access according to the object permission(16/08/2018)
                if(Schema.sObjectType.Value_Table__c.isDeletable()){
                    string[] delIds=delConValIdsStr.split(',');
                    delIds.remove(0);
                    
                    List<Value_Table__c> delList = [Select Id from Value_Table__c where Id IN: delIds];
                    DMLManager.deleteAsUser(delList);
                }/*else{
                    errorMessage = 'You do not have DELETE permission for Value Table object.\n';
                }*/
            }
            if(contValList != Null)
            {
                // "If" condition and "Else" block are newly added to restrict user access according to the object permission(16/08/2018)
                if(Schema.sObjectType.Value_Table__c.isUpdateable()){
                    List<Value_Table__c> vtList = (List<Value_Table__c>)JSON.deserialize(contValList, List<Value_Table__c>.class);
                    for(Value_Table__c vt : vtList)
                    {
                        If(vt.Id==Null){
                            vt.Contract_Vehicle__c=recordId;
                        }
                    }
                    System.debug('vtList '+vtList);
                    //we are not using DMLManager beacuse during JSON.deserialize string it gives error and  Error :-  186 unexpected token: '(' 
                    upsert(vtList);
                }/*else{
                    if(errorMessage == ''){
                        errorMessage = 'You do not have UPDATE permission for Value Table object.\n';
                    }else{
                        errorMessage = 'You do not have UPDATE and DELETE permission for Value Table object.\n';
                    }
                }*/
                
                //newlly added 17-08 
                if(Schema.sObjectType.Value_Table__c.isCreateable() && !Schema.sObjectType.Value_Table__c.isUpdateable()){
                    List<Value_Table__c> vtList = (List<Value_Table__c>)JSON.deserialize(contValList, List<Value_Table__c>.class);
                    List<Value_Table__c> newVtList = new List<Value_Table__c>();
                    for(Value_Table__c vt : vtList)
                    {
                        System.debug('vtid = '+vt.Id);
                        If(vt.Id==Null){
                            vt.Contract_Vehicle__c=recordId;
                            newVtList.add(vt);
                        }
                    }
                    //we are not using DMLManager beacuse during JSON.deserialize string it gives error and  Error :-  186 unexpected token: '(' js and apex serialise and deserilize string is mismatch
                    insert newVtList;
                }
            }
            if(contractRec != Null)
            {
                // "If" condition and "Else" block are newly added to restrict user access according to the object permission(16/08/2018)
                if(Schema.sObjectType.Contract_Vehicle__c.isUpdateable()){
                    update(contractRec);
                }/*else{
                    errorMessage += 'You do not have UPDATE permission for Contract Vehicle object.';
                }*/
            }
        }
        catch(Exception e)
        {
            // To show error message on user-side(16/8/2018)
            errorMessage += 'Error :- Server Error '+e.getMessage();
            System.debug(' Error :-  '+e.getLineNumber()+' '+e.getMessage());
        }
        return errorMessage;
    }
    
    @AuraEnabled
    public static Value_Table__c newRowContValTable(Id contractVehicalId)
    {
        if(Schema.sObjectType.Value_Table__c.isCreateable()){
            contValRecordTypeId = Schema.SObjectType.Value_Table__c.getRecordTypeInfosByName().get('Contract Value').getRecordTypeId();
            valueTable1 = new Value_Table__c (RecordTypeId  = contValRecordTypeId,Contract_Vehicle__c = contractVehicalId,Description__c='',Start_Date__c=Date.today(),End_Date__c=Date.today(),Dollars__c=00.00,Exercised_Contract_Value_Options__c=00.00);
            return valueTable1; 
        }
        else
        {
            return null;
        }
    }
    
    @AuraEnabled
    public static List<Value_Table__c> getValueTableContValList (Id contractVehicalId)
    {
        if(Schema.sObjectType.Value_Table__c.isAccessible()){
            contValRecordTypeId = Schema.SObjectType.Value_Table__c.getRecordTypeInfosByName().get('Contract Value').getRecordTypeId();
            List<Value_Table__c> ValueTableContValList = new List<Value_Table__c> ();
            ValueTableContValList=[select Id,Description__c,Start_Date__c,End_Date__c,Dollars__c,Exercised_Contract_Value_Options__c,RecordTypeId,Contract_Vehicle__c from Value_Table__c where Contract_Vehicle__c=:contractVehicalId AND RecordTypeId =: contValRecordTypeId ORDER BY Start_Date__c ASC ,End_Date__c ASC];
            return ValueTableContValList;  
        }
        else
        {
            return null;
        }
    }
}