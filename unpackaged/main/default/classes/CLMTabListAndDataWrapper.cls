public class CLMTabListAndDataWrapper {
    @AuraEnabled public String tabNameTrimmed{get;set;}
    @AuraEnabled public String tabDataRecord{get;set;}
    @AuraEnabled public CLM_Tab__c tab{get;set;}
    public CLMTabListAndDataWrapper(CLM_Tab__c tab){
        this.tab = tab;
        this.tabNameTrimmed= (tab.TabName__c).replaceAll('[^a-z^A-z^0-9]', '');
        this.tabDataRecord = (tab.TabName__c).replaceAll('[^a-z^A-z^0-9]', '')+'#####'+tab.BackgroundColor__c;
    }
}