@isTest
public with sharing class TestGSAConnectModification {
    public static TestMethod void testDoPost(){       
        Account acc = TestDataGenerator.createAccount('TaskOrderCapture');
        insert acc;
        
        Contract_Vehicle__c contractVehicle = TestDataGenerator.createContractVehicle('TMconVehicle',acc);
        insert contractVehicle;
        
        Task_Order__c taskOrder = TestDataGenerator.createTaskOrder('TMtaskOrder', acc, contractVehicle);
        insert taskOrder;
        
       
        
        StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'ModificationStaticResource' LIMIT 1];
        String body = sr.Body.toString();
               
        Test.startTest();        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        req.requestBody = Blob.valueof(body);
        req.httpMethod = 'POST';
        Map<String,String> headerTest = req.headers;
        headerTest.put('modName','Modification 1');
        headerTest.put('taskOrderId',taskOrder.id);
        
        
        RestContext.request = req;
        RestContext.response = res;
        GSAConnectModification.doPost();
        system.assertNotEquals(taskOrder, null);
        Test.stopTest();
    }
    
     public static TestMethod void testDoPost1(){       
        Account acc = TestDataGenerator.createAccount('TaskOrderCapture');
        insert acc;
        
        Contract_Vehicle__c contractVehicle = TestDataGenerator.createContractVehicle('TMconVehicle',acc);
        insert contractVehicle;
        
        Task_Order__c taskOrder = TestDataGenerator.createTaskOrder('TMtaskOrder', acc, contractVehicle);
        insert taskOrder;
        
       
        
        StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'ModificationStaticResource' LIMIT 1];
        String body = sr.Body.toString();
               
        Test.startTest();        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        req.requestBody = Blob.valueof(body);
        req.httpMethod = 'POST';
       
        
        
        RestContext.request = req;
        RestContext.response = res;
        GSAConnectModification.doPost();
        system.assertNotEquals(taskOrder, null);
        Test.stopTest();
    }
    
     public static TestMethod void testDoPost2(){       
        Account acc = TestDataGenerator.createAccount('TaskOrderCapture');
        insert acc;
        
        Contract_Vehicle__c contractVehicle = TestDataGenerator.createContractVehicle('TMconVehicle',acc);
        insert contractVehicle;
        
        Task_Order__c taskOrder = TestDataGenerator.createTaskOrder('TMtaskOrder', acc, contractVehicle);
        insert taskOrder;
        
       
        
        StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'ModificationNew' LIMIT 1];
        String body = sr.Body.toString();
               
        Test.startTest();        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        req.requestBody = Blob.valueof(body);
        req.httpMethod = 'POST';
        Map<String,String> headerTest = req.headers;
        headerTest.put('modName','Modification 1');
        headerTest.put('taskOrderId',taskOrder.id);
        
        
        RestContext.request = req;
        RestContext.response = res;
        GSAConnectModification.doPost();
        system.assertNotEquals(taskOrder, null);
        Test.stopTest();
    }
    
    //new method to cover new scenario
    public static TestMethod void testDoPost3(){       
        Account acc = TestDataGenerator.createAccount('TaskOrderCapture');
        insert acc;
        
        Contract_Vehicle__c contractVehicle = TestDataGenerator.createContractVehicle('TMconVehicle',acc);
        insert contractVehicle;
        
        Task_Order__c taskOrder = TestDataGenerator.createTaskOrder('TMtaskOrder', acc, contractVehicle);
        insert taskOrder;
        
        StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'ModificationStaticResource' LIMIT 1];
        String body = sr.Body.toString();
        body = body + ' '+taskOrder.name;  
        body = body + ' The following Q&A document(s) are attached to this RFQ:';
        body = body + ' /advantage/ebuy/view_doc.do';
        body = body + ' href="www.google.com">';
        
        Test.startTest();        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        req.requestBody = Blob.valueof(body);
        req.httpMethod = 'POST';
        Map<String,String> headerTest = req.headers;
        headerTest.put('modName','Modification 1');
        headerTest.put('taskOrderId',taskOrder.id);
        
        
        RestContext.request = req;
        RestContext.response = res;
        GSAConnectModification.doPost();
        system.assertNotEquals(taskOrder, null);
        Test.stopTest();
    }
}