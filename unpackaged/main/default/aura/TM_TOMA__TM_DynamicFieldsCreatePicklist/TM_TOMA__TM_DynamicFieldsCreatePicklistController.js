({
    onInit : function(component, event, helper) {
        var fieldname1=component.get("v.fieldName");
        var selected=component.get("v.selectedPicklist");
        var fieldsvalue= component.get("c.getStages");
        
        fieldsvalue.setParams({
            "Fieldname" : fieldname1
        });
        fieldsvalue.setCallback(this, function(response) {
            
            var state = response.getState();
            if(state ==="SUCCESS")
            {
                var picklistVal=response.getReturnValue();
                // picklistVal.unshift("--None--");
                var noneValue={'pickValue':null,'pickLabel':'--None--'};
                picklistVal.unshift(noneValue);
                component.set("v.picklistvalues", picklistVal);
                //alert(picklistVal);
                var picklst= component.find("InputSelectDynamic");
                // alert('selected picklist'+selected);
                picklst.set("v.value",selected);
            }
            
            
            
        });
        $A.enqueueAction(fieldsvalue); 
    },
    
    selectCurrentValue :function(component, event, helper){
        
        var picklist=component.find("InputSelectDynamic");
        component.set("v.selectedvalue",picklist.get("v.value"));
        //$A.enqueueAction(component.get("v.pickListChanged"));
        var provideDependencyData = $A.get("e.c:providePicklistDependancyData");
        provideDependencyData.setParams({
            "fieldApiName" : component.get("v.fieldName"),
            "selectedValue": picklist.get("v.value")
        });
        provideDependencyData.fire();
        
        /* var picklist=component.find("InputSelectDynamic");
        component.set("v.selectedvalue",picklist.get("v.value"));*/
    },
   /* updateBidStage :function(component, event, helper){
        var fieldname=component.get("v.fieldName");
        if(fieldname =="Stage__c"){
            var stage=event.getParam("stage");
            var picklist=component.find("InputSelectDynamic");
            picklist.set("v.value",stage);
            component.set("v.selectedvalue",stage);
        }     
    },*/
    setOldValue :function(component, event, helper){
        var indx = component.get("v.index");
        var compApi = component.get("v.fieldName");
        var eventIndex = event.getParam("index");
        var eventApi = event.getParam("api");
        var old = event.getParam("oldValue");
        if(compApi == eventApi)
        {
            if(indx.valueOf() == eventIndex.valueOf())
            {
                //alert('picklist '+old);
                var check = old != 'undefined' && old != undefined && old != null;
                //alert(check);
                if(old != 'undefined' && old != undefined && old != null){
                     //alert('picklist if '+old);
                    var picklist=component.find("InputSelectDynamic");
                    picklist.set("v.value",old);
                    //component.set("v.selectedvalue",old);
                    
                    //$A.enqueueAction(component.get("v.pickListChanged"));
                    var provideDependencyData = $A.get("e.c:providePicklistDependancyData");
                    provideDependencyData.setParams({
                        "fieldApiName" : component.get("v.fieldName"),
                        "selectedValue": old
                    });
                    provideDependencyData.fire();
                }
                else{
                    var picklist=component.find("InputSelectDynamic");
                    picklist.set("v.value",'');
                }
                /* var picklist=component.find("InputSelectDynamic");
        component.set("v.selectedvalue",picklist.get("v.value"));*/
                
            }
        }
    }
})