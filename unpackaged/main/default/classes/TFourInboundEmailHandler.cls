global with sharing class TFourInboundEmailHandler implements Messaging.InboundEmailHandler {
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        String myPlainText = email.HTMLBody;        
        TaskOrderEmailParser parser = new TaskOrderEmailParser();
        System.debug('email.toAddresses[0] = '+email.toAddresses[0]);
        Task_Order__c taskOrder = parser.tFourTaskOrderParser(myPlainText, email.toAddresses[0]);       
        try{
            if(!parser.isOldTakOrder){
                DMLManager.insertAsUser(taskOrder);            
                EmailHandlerHelper.insertBinaryAttachment(taskOrder,email);
                EmailHandlerHelper.insertTextAttachment(taskOrder,email);
                EmailHandlerHelper.insertActivityHistory(taskOrder,Email);
            }else{
                Task_Order__c oldTaskOrder = parser.oldTaskOrder;                       
                EmailHandlerHelper.insertContractMods(taskOrder, oldTaskOrder,email);
                DMLManager.updateAsUser(taskOrder);                
                EmailHandlerHelper.insertBinaryAttachment(taskOrder,email);
                EmailHandlerHelper.insertTextAttachment(taskOrder,email);
                EmailHandlerHelper.insertActivityHistory(taskOrder,email);                
                system.debug('Already Exists');
            }
        }catch(Exception e){
            System.debug('Error = '+e.getLineNumber());    
        }
        return result;
    }
}