public Class AllPartnerwrapper{
        @AuraEnabled
        public Account acc{get;set;}
        @AuraEnabled
        public Boolean isSelected{get;set;}
        public AllPartnerwrapper(Account acc){
            this.acc =acc;
            this.isSelected = false;
        }
    }