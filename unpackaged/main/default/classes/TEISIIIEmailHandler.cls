global class TEISIIIEmailHandler implements Messaging.InboundEmailHandler{
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        List<Contract_Vehicle__c> conVehicleList = [SELECT Id FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:email.toAddresses.get(0)];
        if(!conVehicleList.isEmpty()){
            TEISIIIEmailParser parser = new TEISIIIEmailParser();
            parser.parse(email);//Email Parsing will be handle by parse method.
            if(parser.taskOrderNumber!=null && parser.taskOrderNumber.trim().length()>0){
                Task_Order__c taskOrder = EmailHandlerHelper.getExistTaskOrder(parser.taskOrderNumber, conVehicleList[0].Id);
                if(taskOrder == null){ // New Task Order         
                    taskOrder = parser.getTaskOrder(email.toAddresses.get(0));
                    if(taskOrder != null){  
                        try{
                            DMLManager.insertAsUser(taskOrder);
                            EmailHandlerHelper.insertBinaryAttachment(taskOrder,email);
                            EmailHandlerHelper.insertTextAttachment(taskOrder,email);
                            EmailHandlerHelper.insertActivityHistory(taskOrder,email);
                        }catch(Exception e){
                            System.debug('error message = '+e.getMessage());
                        }
                    }
                }else{ // For Existing Task Order 
                    if(!taskOrder.Is_cancelled__c){    
                        if(!parser.isAmendent && !parser.isCancel){ // Updated on 25-Sep-2019 by Swarna
                            Task_Order__c oldTaskOrder = taskOrder.clone(false,true,false,false);       
                            Task_Order__c updatedTaskOrder = parser.updateTaskOrder(taskOrder, email.toAddresses.get(0)); // Map updated values for Existed/Old Task Order
                            try{
                                EmailHandlerHelper.insertContractMods(updatedTaskOrder, oldTaskOrder, email); // Insert Contract Mods with Old & Updated values of Updated Task Order
                                update updatedTaskOrder;
                                EmailHandlerHelper.insertBinaryAttachment(updatedTaskOrder, email);
                                EmailHandlerHelper.insertTextAttachment(updatedTaskOrder, email);
                                EmailHandlerHelper.insertActivityHistory(updatedTaskOrder, email); 
                                system.debug('Task Order Already Exists');
                            }catch(Exception e){
                                System.debug('error message = '+e.getMessage()+'\tline number = '+e.getLineNumber());
                            }
                        }
                        // On 25-Sep-2019 by Swarna -- start
                        else{
                            try{
                                if(taskOrder.id != null){
                                    Contract_Mods__c contractMod = new Contract_Mods__c();       
                                    contractMod.RecordTypeId = Schema.SObjectType.Contract_Mods__c.getRecordTypeInfosByName().get('Contract Mod').getRecordTypeId();
                                    String body = email.plainTextBody;
                                    System.debug('body : '+body);
                                    contractMod.TaskOrder__c = taskOrder.id;
                                    //String amd = body.substringAfter(parser.taskOrderNumber);
                                    // For Amendment
                                    if(parser.isAmendent)
                                    {
                                        String amd = email.subject.substringBetween('TOPR-',' (UNCLASSIFIED)');
                                        System.debug('amd : '+amd);
                                        if(amd!=null && amd!='')
                                        {
                                            contractMod.Modification_Name__c = amd;
                                        }
                                        String cmModification = body.substringBetween(',','v/r');
                                        if(cmModification!=null)
                                        	contractMod.Modification__c = cmModification.trim();
                                    }
                                    // For Cancellation
                                    else if(parser.isCancel)
                                    {
                                        System.debug('Cancellation : ');
                                        contractMod.Modification_Name__c = 'Cancellation';
                                        taskOrder.Is_cancelled__c = true;
                                        update taskOrder;
                                        String cmModification = body.substringBetween(',','V/R');
                                        if(cmModification!=null)
                                        	contractMod.Modification__c = cmModification.trim();
                                    }
                                    
                                    
                                    DMLManager.insertAsUser(contractMod);    
                                    EmailHandlerHelper.insertBinaryAttachment(taskOrder,email);
                                    EmailHandlerHelper.insertTextAttachment(taskOrder,email);
                                    EmailHandlerHelper.insertActivityHistory(taskOrder,email);
                                }
                            }catch(Exception e){
                                
                            }
                        }     
                        // On 25-Sep-2019 by Swarna -- end
                    }
                }
            }
        } else{
            System.debug('Contract Vehicle not found.');
        }
        return new Messaging.InboundEmailresult();
    }
}