//This controller is used for Contract Detail Page 
public class TM_ContractDetailPageController{
    
    public static Map<String, Schema.SObjectField> fieldsMapAccessible{get;set;}
    public static Map<String,Schema.SObjectField> conVehicleFieldTypeMap{get;set;}
    public static Map<String,String> fieldsMapWithRefranceFieldName{get;set;} //newlly added 19-04 for lookup
    public String isValidLiecence{get;set;}
    static{
        conVehicleFieldTypeMap =  Schema.SObjectType.Contract_Vehicle__c.fields.getMap();
        fieldsMapAccessible=Schema.getGlobalDescribe().get('TM_TOMA__Contract_Vehicle__c').getDescribe().SObjectType.getDescribe().fields.getMap();
        
        //newlly added 19-04 for lookup 
        fieldsMapWithRefranceFieldName = new Map<String,String>();
        for(Schema.SObjectField sobjectField : fieldsMapAccessible.values())
        {
            String tempField = sobjectField.getDescribe().getName();
            //System.debug('tempField  '+tempField);
            String typeOFField = sobjectField.getDescribe().getType()+'';
            if(typeOFField == 'REFERENCE' && tempField.endsWith('__c'))
            {
                fieldsMapWithRefranceFieldName.put(tempField, sobjectField.getDescribe().getRelationshipName()+'.Name');
            }
            else if(typeOFField == 'REFERENCE' && tempField == 'OwnerId'){
                fieldsMapWithRefranceFieldName.put(tempField, sobjectField.getDescribe().getRelationshipName()+'.Name');
            }
            
        }
        //end
    }
    
    public Contract_Vehicle__c contractVehicle{get; set;}
    public Contract_Vehicle__c oldContractVehicle{get; set;}
    public String recordId{get; set;}
    public Boolean isEdit{get; set;}
    public Boolean doUserHaveEditPermission{get;set;}//30-05
    public List<TabWrapper> tabWrapperList{get; set;}
    public List<SelectOption> recordTypeOptionList{get; set;}
    public Boolean isChangeRecordType{get; set;}
    public ApexPages.StandardController controller{get; set;}
    public transient Map<String,Map<String,FieldDependancyHelper.DependantField>> parentMap{get; set;}//newlly added transient on 14-06-2018
    public Map<Integer,String> monthMap{get; set;}
    Public String allTab{get;set;} 
    public String mode{get;set;}
    public Boolean stopProcessing{get; set;}
    Set<String> fieldApiNameSet{get; set;}
    
    
    //public String 
    
    public TM_ContractDetailPageController(ApexPages.StandardController controller) {
        isValidLiecence = LincenseKeyHelper.checkLicensePermitionForFedCLM();
        if(isValidLiecence == 'Yes'){
            stopProcessing = false;
             doUserHaveEditPermission = true;//30-05
            putMonthMapData();
            mode = 'view';
            this.controller = controller;
            initMember();
            if(!stopProcessing){
                init();
                contractVehicle = (Contract_Vehicle__c)controller.getRecord();
                System.debug('contractVehicle after init '+contractVehicle);
                recordId= contractVehicle.Id;
                fillRecordTypeOptions();
            }
        }
    }
    
    //newlly added 25-01-2018
    public Double offset{get{
        TimeZone tz = UserInfo.getTimeZone();
        //Milliseconds to Day
        return tz.getOffset(DateTime.now()) / (1000 * 3600 * 24.0);
    }}   
    
    private void putMonthMapData(){
        monthMap = new Map<Integer,String>();
        monthMap.put(1,'JAN');
        monthMap.put(2,'FEB');
        monthMap.put(3,'MAR');
        monthMap.put(4,'APR');
        monthMap.put(5,'MAY');
        monthMap.put(6,'JUN');
        monthMap.put(7,'JUL');
        monthMap.put(8,'AUG');
        monthMap.put(9,'SEP');
        monthMap.put(10,'OCT');
        monthMap.put(11,'NOV');
        monthMap.put(12,'DEC');
    }
    
    private void getFieldDependancyData(){
        FieldDependancyHelper fieldDependancyHelper = new FieldDependancyHelper();
        fieldDependancyHelper.process(contractVehicle.RecordTypeId,true);
        
    }
    
    private void initMember(){
        isChangeRecordType = false;
        System.debug('In getTabStructure');
        //contractVehicle = (Contract_Vehicle__c)controller.getRecord();
        String contractVehivleId = ApexPages.CurrentPage().getparameters().get('id');
        if(contractVehivleId != null){
            List<Contract_Vehicle__c> contractVehicleList = [Select id,RecordTypeId From Contract_Vehicle__c where id =: contractVehivleId  Limit 1];
            if(!contractVehicleList.isEmpty()){
                contractVehicle = contractVehicleList[0];
                
                //30-05
                if(Schema.sObjectType.Contract_Vehicle__c.isAccessible()){
                    List<UserRecordAccess> recAccessList = [SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId =: UserInfo.getUserId() AND RecordId =: contractVehicle.Id];
                    if(recAccessList != null && ! recAccessList.isEmpty()){
                        doUserHaveEditPermission = recAccessList[0].HasEditAccess == null ? true : recAccessList[0].HasEditAccess == true ? true:false;
                    }
                }else{
                    doUserHaveEditPermission = false;
                }
                
            }
            else{
                contractVehicle = new Contract_Vehicle__c();
            }
        }
        // newlly added 16-05-2018 Create_Blank_Space__c
        List<CLM_FIeld__c> fieldCustomSettingList = [Select Id,Draft_Required__c,FieldApiName__c,FieldLabel__c,HelpText__c,Is_Contract__c,
                                                     Order__c,RecordType_Id__c,  Required__c,TabNumber__c,ViewFieldApi__c ,Required_Error_Message__c,Create_Blank_Space__c,Use_Custom_lookup__c
                                                     From CLM_FIeld__c where Is_Contract__c = true order by TabNumber__c asc , Order__c asc];
        fieldApiNameSet = new Set<String>();
        
        Boolean isOwnerIdFound = false;
        Boolean isRecordTypeIdFound = false;                                                 
        if(!fieldCustomSettingList.isEmpty()){
            
            for(CLM_FIeld__c clmField : fieldCustomSettingList ){
                if(clmField.FieldApiName__c != null  && clmField.Create_Blank_Space__c != true){//newlly added 15-05-2018 && clmField.Create_Blank_Space__c != true
                    if(clmField.FieldApiName__c.equalsIgnoreCase('OwnerId')){
                        isOwnerIdFound = true;
                    }
                    if(clmField.FieldApiName__c.equalsIgnoreCase('RecordTypeId')){
                        isRecordTypeIdFound = true;
                    }
                    fieldApiNameSet.add(clmField.FieldApiName__c );
                    
                    //newlly added 19-04 for lookup
                    if(fieldsMapWithRefranceFieldName.containsKey(''+clmField.FieldApiName__c))
                    {
                        fieldApiNameSet.add(fieldsMapWithRefranceFieldName.get(''+clmField.FieldApiName__c));
                    }
                    //end
                }
                //newlly added 25-01-2018
                if(clmField.ViewFieldApi__c != null && clmField.Create_Blank_Space__c != true){//newlly added 15-05-2018 && clmField.Create_Blank_Space__c != true
                    if(clmField.ViewFieldApi__c.equalsIgnoreCase('OwnerId')){
                        isOwnerIdFound = true;
                    }
                    if(clmField.ViewFieldApi__c.equalsIgnoreCase('RecordTypeId')){
                        isRecordTypeIdFound = true;
                    }
                    fieldApiNameSet.add(clmField.ViewFieldApi__c);
                    
                }
            }
            
        }
        else{
            stopProcessing = true;
        }
        isOwnerIdFound = false;
        if(!isOwnerIdFound )
            fieldApiNameSet.add('OwnerId');
        if(!isRecordTypeIdFound )
            fieldApiNameSet.add('RecordTypeId');
        //System.debug('fieldApiNameSet = '+fieldApiNameSet); 
        // System.debug('isOwnerIdFound/isRecordTypeIdFound = '+isOwnerIdFound+' '+isRecordTypeIdFound);
        
        fieldApiNameSet.add('Owner.Name');//on added 11-05 to avoid issue (OwnerID field cannot be removed from the page layout without an error) it may cause to use of number of lookup on page the other option we can create new apex controller for TM_OwnerChange page and get owner name for lookup.
        
        List<CLM_AtaGlanceField__c> atAGlanceSettList = [SELECT Id,FieldApiName__c FROM CLM_AtaGlanceField__c WHERE Is_Contract__c = true AND Is_Component__c = False];
        //system.debug('atAGlanceSettList'+atAGlanceSettList);
        if(atAGlanceSettList != null && ! atAGlanceSettList.isEmpty() && contractVehicle != null && contractVehicle != null){
            for(CLM_AtaGlanceField__c atAGlance :atAGlanceSettList){
                fieldApiNameSet.add(atAGlance.FieldApiName__c);
            }
        }
        
        if (!Test.isRunningTest()) { 
            controller.addFields(new List<String>(fieldApiNameSet));
        }
        /*for(string str : fieldApiNameSet)
{
System.debug(str);
}*/
        contractVehicle = (Contract_Vehicle__c)controller.getRecord();
        System.debug('contractVehicle in initmember '+contractVehicle);
        oldContractVehicle = contractVehicle.clone(true,true,true,true);
    }
    
    private void init(){
        
        getFieldDependancyData();
        this.parentMap = FieldDependancyHelper.parentMap;
        
        isEdit = false;
        //To get contract Vehicle information from database as per record id
        
        //Declair and define Tabwrapper list to retun it to invoking function.
        tabWrapperList = new List<TabWrapper>();
        
        
        //if(!contractVehicleList.isEmpty()){
        //String recordTypeId = contractVehicle.recordTypeId;
        String recordTypeId =contractVehicle.RecordTypeId;
        if(recordTypeId == null){
            recordTypeId = '';
        }
        if(recordTypeId.length() > 15){
            recordTypeId = recordTypeId.substring(0,15);
        }
        
        //All the custom setting for specific record type in order based on tab and order no.
        //we take the data in order to keep it in proper order while creaeting data set.
        // newlly added 15-05-2018 Create_Blank_Space__c
        List<CLM_FIeld__c> fieldCustomSettingList = [Select Id,Draft_Required__c,FieldApiName__c,FieldLabel__c,HelpText__c,Is_Contract__c,
                                                     Order__c,RecordType_Id__c,  Required__c,TabNumber__c,ViewFieldApi__c,Required_Error_Message__c,Create_Blank_Space__c,Use_Custom_lookup__c
                                                     From CLM_FIeld__c where RecordType_Id__c =: recordTypeId And Is_Contract__c = true order by TabNumber__c asc , Order__c asc];
        
        //System.debug('fieldCustomSettingList = '+fieldCustomSettingList);
        //This map is to store data according to tabSequnce there are mutiple section and then according to section order there are multiple field in section
        //we create data set as Map<TabNumber,Map<SectionOrder,List<NumberOfFieldsIn section>>>
        
        Map<Integer,Map<Integer,List<CLM_FIeld__c>>> fieldCustomSettingMap = new Map<Integer,Map<Integer,List<CLM_FIeld__c>>>();
        for(CLM_FIeld__c fieldCustomSetting : fieldCustomSettingList){
            Map<Integer,List<CLM_FIeld__c>> fieldInternalMap = fieldCustomSettingMap.get((Integer)fieldCustomSetting.TabNumber__c);
            if(fieldInternalMap == null){
                fieldInternalMap = new Map<Integer,List<CLM_FIeld__c>>();
            }
            
            if(fieldCustomSetting.Order__c != null){
                //WE have section order in 10, 20 , 30... format and field order in 1001,1002,2001..., where last 2 digit specify squence 
                //and first remainig specify section order so thats why we divide field order with 100 to get section order.
                Integer sectionNumber = ((Integer)fieldCustomSetting.Order__c)/100;
                List<CLM_FIeld__c> internalFieldList = fieldInternalMap.get(sectionNumber);
                if(internalFieldList == null){
                    internalFieldList = new List<CLM_FIeld__c>();
                }
                internalFieldList.add(fieldCustomSetting);
                fieldInternalMap.put(sectionNumber,internalFieldList);
                fieldCustomSettingMap.put((Integer)fieldCustomSetting.TabNumber__c,fieldInternalMap);
            }
            //fieldApiNameSet.add(fieldCustomSetting.FieldApiName__c);
            
        }
        //To get all section custom setting for specific recordType and also it is in order to maintain data structure.
        List<CLM_Section__c> sectionCustomSettingList = [Select Id,BackgroundColor__c,Height__c,Inline_Api_Name_Component_Api__c,Is_Contract__c,Is_InlineVF_Page__c,
                                                         Is_Lightning_Component__c, No_Of_Column__c,Order__c,RecordType_Id__c,SectionName__c,TabNumber__c,Width__c,AppApi__c 
                                                         From CLM_Section__c where RecordType_Id__c =: recordTypeId And Is_Contract__c = true order by TabNumber__c asc , Order__c asc];
        //To keep section List according to tabOrder
        //System.debug('sectionCustomSettingList = '+sectionCustomSettingList);
        Map<Integer,List<CLM_Section__c>> sectionMap = new Map<Integer,List<CLM_Section__c>>();
        for(CLM_Section__c sectionCustomSetting : sectionCustomSettingList){
            List<CLM_Section__c> internalSectionList = sectionMap.get((Integer)sectionCustomSetting.TabNumber__c);
            if(internalSectionList == null){
                internalSectionList = new List<CLM_Section__c>();
            }
            internalSectionList.add(sectionCustomSetting);
            sectionMap.put((Integer)sectionCustomSetting.TabNumber__c,internalSectionList);
        }
        List<CLM_Tab__c> tabCustomSettingList = [Select Id,Is_Contract__c,Order__c,RecordType_Id__c,TabName__c,BackgroundColor__c
                                                 From CLM_Tab__c where RecordType_Id__c =: recordTypeId And Is_Contract__c = true order by Order__c asc];
        //Here we have apply a logic to  create a data set as we have data set that we have mutiple tab and inside one tab there are mutiple sectin
        //and inside section there are either  multiple fields or components or VF page.
        //System.debug('tabCustomSettingList = '+tabCustomSettingList);
        Integer index = 1;
        allTab = '';
         //newly added for Fake tab @ 21/05/2019
        if(tabCustomSettingList.isEmpty()){
            tabCustomSettingList = new List<CLM_Tab__c>(); 
            tabCustomSettingList.add(new CLM_Tab__c(Order__c=0,TabName__c='Fake Tab',BackgroundColor__c='Red'));
        }  
        for(CLM_Tab__c tabCustomSetting : tabCustomSettingList){
            Integer tabOrderNumber = (Integer)tabCustomSetting.Order__c;
            if(tabOrderNumber != null){
                List<CLM_Section__c> internalSectionList = sectionMap.get((Integer)tabCustomSetting.Order__c);   
                Map<Integer,List<CLM_FIeld__c>> fieldInternalMap = fieldCustomSettingMap.get((Integer)tabCustomSetting.Order__c);
                if(fieldInternalMap == null){
                    fieldInternalMap = new Map<Integer,List<CLM_FIeld__c>>();
                }
                if(internalSectionList != null){
                    TabWrapper tabWrap = new TabWrapper(tabCustomSetting,internalSectionList,fieldInternalMap);
                    //if there is no section in tab then we don't want to show empty tab on page layout so we will not add in data set.
                    if(tabWrap.sectionWrapperList != null && !tabWrap.sectionWrapperList.isEmpty()){
                        tabWrap.sequence = index;
                        tabWrapperList.add(tabWrap);
                    }
                }
            }
            allTab += 'tabedit'+index+',';
            index++;
        }
        if(allTab.length() > 0){
            allTab = allTab.substring(0,allTab.length()-1);
        }
        // System.debug('tabWrapperList = '+tabWrapperList);
        
        
        /*for(TabWrapper tabWrapper : tabWrapperList){
System.debug('Tab Name = '+tabWrapper.tab.TabName__c);
System.debug('Tab Order = '+tabWrapper.tab.Order__c);
for(SectionWrapper sctionWrap : tabWrapper.sectionWrapperList){
System.debug('sction Name = '+sctionWrap.section.SectionName__c);
System.debug('sction Order = '+sctionWrap.section.Order__c);
for(FieldWrapper fieldWrap : sctionWrap.fieldWrapperList){
System.debug('field Name = '+fieldWrap.field.FieldApiName__c);
System.debug('field Order = '+fieldWrap.field.Order__c);
} 
}
}*/
    }
    
    private void fillRecordTypeOptions(){
        recordTypeOptionList = new List<SelectOption>();
        Schema.DescribeSObjectResult describeResult = Contract_Vehicle__c.SObjectType.getDescribe();
        List<Schema.RecordTypeInfo> recordTypeInfoList = describeResult .getRecordTypeInfos();
        for(Schema.RecordTypeInfo recordTypeInfo: recordTypeInfoList){
            if(recordTypeInfo.isAvailable() && recordTypeInfo.getName() != 'Master'){
                recordTypeOptionList.add(new SelectOption(recordTypeInfo.getRecordTypeId(),recordTypeInfo.getName()));
            }
        }
    }
    
    public void changeRecordType(){
        isChangeRecordType = true;
        mode = 'edit';
        init();
    }
    
    
    public PageReference saveContract1(){
        try{
            if(contractVehicle.Contract_Status_Backend__c != null || contractVehicle.Contract_Status_Backend__c != ''){
                contractVehicle.Contract_Status_Backend__c = 'Draft';
            }
            
            upsert contractVehicle;
            
            System.debug('Successfully Updated');
            isChangeRecordType = false;
            mode = 'view';
            
            
            Boolean refreshPage = false;
            If(Schema.sObjectType.CLM_AtaGlanceField__c.isAccessible()){
                List<CLM_AtaGlanceField__c> atAGlanceSettList = [SELECT Id,FieldApiName__c FROM CLM_AtaGlanceField__c WHERE  Is_Contract__c = true AND Is_Component__c = False];
                //system.debug('atAGlanceSettList'+atAGlanceSettList);
                if(atAGlanceSettList != null && ! atAGlanceSettList.isEmpty() && contractVehicle != null && contractVehicle != null){
                    for(CLM_AtaGlanceField__c atAGlance :atAGlanceSettList){
                        if(atAGlance.FieldApiName__c != null ){
                            //code added for checking multiselect 17-01-2018
                            string conVal = ''+contractVehicle.get(atAGlance.FieldApiName__c);
                            string oldConVal = ''+oldContractVehicle.get(atAGlance.FieldApiName__c);
                            conVal = conVal.replace(';',' ').replace('null','').trim();
                            oldConVal = oldConVal.replace(';',' ').replace('null','').trim();
                            system.debug('contractVehicle.get(atAGlance.FieldApiName__c)  '+conVal);
                            system.debug('oldContractVehicle.get(atAGlance.FieldApiName__c) '+oldConVal);
                            //if(contractVehicle.get(atAGlance.FieldApiName__c) != oldContractVehicle.get(atAGlance.FieldApiName__c)){
                            if(!conVal.equals(oldConVal) ){
                                refreshPage=true;
                                break;
                            }
                        }
                    }
                }
                if(refreshPage || contractVehicle.get('Name') != oldContractVehicle.get('Name')){
                    ApexPages.currentPage().getParameters().put('refreshParent','1');
                    Pagereference pg = new pagereference('/apex/TM_ContractDetailPage?id='+contractVehicle.Id+'&refreshParent=1');
                    pg.getParameters().put('refreshParent','1');
                    pg.setRedirect(true);
                    return pg;
                }
                
            }
            Pagereference pg = new pagereference('/apex/TM_ContractDetailPage?id='+contractVehicle.Id);
            pg.setRedirect(true);
            return pg;
        }
        catch(Exception ex){
            System.debug(LoggingLevel.info,'Exception = '+ex);
            mode = 'edit';
            String errorMessage = ''+ex;
            errorMessage  = errorMessage.substringAfter('first error:');
            //System.debug(LoggingLevel.info,'errorMessage  = '+errorMessage  );
            Boolean found = false;
            if(ApexPages.hasMessages()){
                // System.debug(LoggingLevel.info,'errorMessage  = '+errorMessage  );
                ApexPages.Message[] messageArray = ApexPages.getMessages();
                SObjectType accountType = Schema.getGlobalDescribe().get('TM_TOMA__Contract_Vehicle__c');
                Map<String,Schema.SObjectField> mfields = accountType.getDescribe().fields.getMap();
                
                //System.debug(LoggingLevel.info,'mfields = '+mfields );
                String fieldApiName = errorMessage.subStringBetween(': [',']');
                //System.debug(LoggingLevel.info,'fieldApiName = '+fieldApiName );
                Schema.SObjectField field = mfields.get(fieldApiName );
                //System.debug(LoggingLevel.info,'field = '+field );
                if(field != null){
                    String fieldLabel = field.getDescribe().getLabel();
                    System.debug(LoggingLevel.info,'fieldLabel = '+fieldLabel );
                    for(ApexPages.Message msg : messageArray ){
                        String msgFieldlabel = msg.getComponentLabel();
                        if(msgFieldlabel != null && msgFieldlabel == fieldLabel  ){
                            found = true;
                        }
                    }
                }
                
                
                
            }
            if(!found){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,''+errorMessage );
                //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'tyutyu'+ex );
                ApexPages.addMessage(myMsg);
            }
        }
        
        return null;
    }
    
    public PageREference saveAndActivate(){
        System.debug('saveAndActivate');
        try{
            contractVehicle.Contract_Status_Backend__c = 'Final';
            upsert contractVehicle;
            System.debug('Successfully Updated');
            isChangeRecordType = false;
            mode = 'view';
            
            Boolean refreshPage = false;
            If(Schema.sObjectType.CLM_AtaGlanceField__c.isAccessible()){
                List<CLM_AtaGlanceField__c> atAGlanceSettList = [SELECT Id,FieldApiName__c FROM CLM_AtaGlanceField__c WHERE  Is_Contract__c = true AND Is_Component__c = False];
                //system.debug('atAGlanceSettList'+atAGlanceSettList);
                if(atAGlanceSettList != null && ! atAGlanceSettList.isEmpty() && contractVehicle != null && contractVehicle != null){
                    for(CLM_AtaGlanceField__c atAGlance :atAGlanceSettList){
                        if(atAGlance.FieldApiName__c != null ){
                            //code added for checking multiselect 16-01-2018
                            string conVal = ''+contractVehicle.get(atAGlance.FieldApiName__c);
                            string oldConVal = ''+oldContractVehicle.get(atAGlance.FieldApiName__c);
                            conVal = conVal.replace(';',' ').replace('null','').trim();
                            oldConVal = oldConVal.replace(';',' ').replace('null','').trim();
                            system.debug('contractVehicle.get(atAGlance.FieldApiName__c)  '+conVal);
                            system.debug('oldContractVehicle.get(atAGlance.FieldApiName__c) '+oldConVal);
                            //if(contractVehicle.get(atAGlance.FieldApiName__c) != oldContractVehicle.get(atAGlance.FieldApiName__c)){
                            if(!conVal.equals(oldConVal) ){
                                refreshPage=true;
                                break;
                            }
                        }
                    }
                }
                if(refreshPage || contractVehicle.get('Name') != oldContractVehicle.get('Name')){
                    ApexPages.currentPage().getParameters().put('refreshParent','1');
                    Pagereference pg = new pagereference('/apex/TM_ContractDetailPage?id='+contractVehicle.Id+'&refreshParent=1');
                    pg.getParameters().put('refreshParent','1');
                    pg.setRedirect(true);
                    return pg;
                }
                
            }
            Pagereference pg = new pagereference('/apex/TM_ContractDetailPage?id='+contractVehicle.Id);
            pg.setRedirect(true);
            return pg;
            
        }
        catch(Exception ex){
            System.debug('Exception = '+ex);
            mode = 'edit';
            String errorMessage = ''+ex;
            
            errorMessage  = errorMessage.substringAfter('first error:');
            //System.debug(LoggingLevel.info,'errorMessage  = '+errorMessage  );
            Boolean found = false;
            if(ApexPages.hasMessages()){
                // System.debug(LoggingLevel.info,'errorMessage  = '+errorMessage  );
                ApexPages.Message[] messageArray = ApexPages.getMessages();
                SObjectType accountType = Schema.getGlobalDescribe().get('TM_TOMA__Contract_Vehicle__c');
                Map<String,Schema.SObjectField> mfields = accountType.getDescribe().fields.getMap();
                
                //System.debug(LoggingLevel.info,'mfields = '+mfields );
                String fieldApiName = errorMessage.subStringBetween(': [',']');
                //System.debug(LoggingLevel.info,'fieldApiName = '+fieldApiName );
                Schema.SObjectField field = mfields.get(fieldApiName );
                //System.debug(LoggingLevel.info,'field = '+field );
                if(field != null){
                    String fieldLabel = field.getDescribe().getLabel();
                    System.debug(LoggingLevel.info,'fieldLabel = '+fieldLabel );
                    for(ApexPages.Message msg : messageArray ){
                        String msgFieldlabel = msg.getComponentLabel();
                        if(msgFieldlabel != null && msgFieldlabel == fieldLabel  ){
                            found = true;
                        }
                    }
                }
                
                
                
            }
            if(!found){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,''+errorMessage );
                ApexPages.addMessage(myMsg);
            }
            return null;
        }
        return null;
    }
    
    public void changeOwner(){
        system.debug('Record updated = '+ contractVehicle.ownerId);
        try{
            update contractVehicle;
        }
        catch(Exception e)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,''+e.getMessage() );
            ApexPages.addMessage(myMsg);
        }
        system.debug('Record updated');
    }
    
    public void changeRecordTypeCancel(){
        system.debug('Record updated = '+ contractVehicle.ownerId);
        system.debug('contractVehicle.id = '+contractVehicle.id);
        if(Schema.sObjectType.Contract_Vehicle__c.isAccessible())
            contractVehicle.RecordTypeId = [Select Id,RecordTypeId From Contract_Vehicle__c where id =: contractVehicle.id].RecordTypeId ;
        //update contractVehicle;
        system.debug('Record updated');
    }
    
    public void changeOwnerCancel(){
        system.debug('Record updated = '+ contractVehicle.ownerId);
        system.debug('contractVehicle.id = '+contractVehicle.id);
        if(Schema.sObjectType.Contract_Vehicle__c.isAccessible())
            contractVehicle.ownerId = [Select Id,OwnerId From Contract_Vehicle__c where id =: contractVehicle.id].ownerId;
        //update contractVehicle;
        system.debug('Record updated');
    }
    
    public PageReference cancelEdit(){
        //mode = 'view';
        
        mode = 'view';
        
        PageReference pageRef = new pagereference('/'+contractVehicle.Id);
        pageRef.setRedirect(true);
        //contractVehicle = (Contract_Vehicle__c)controller.getRecord();
        return pageRef;
        
    }
    
    public PageReference chandeMode(){
        mode = 'edit';
        return null;
    }
    
    public class TabWrapper{
        public CLM_Tab__c tab {get; set;}
        public List<SectionWrapper> sectionWrapperList{get; set;}
        public Integer sequence {get; set;}
        public Map<Integer,SectionWrapper> sectionWrapperMap {get; set;} 
        
        public TabWrapper(CLM_Tab__c tabCustomSetting,List<CLM_Section__c> internalSectionList,Map<Integer,List<CLM_FIeld__c>> fieldInternalMap){
            this.tab = tabCustomSetting;
            this.sectionWrapperList = new List<SectionWrapper>();
            for(CLM_Section__c sectionCustomSetting : internalSectionList){
                if(fieldInternalMap == null){
                    fieldInternalMap = new Map<Integer,List<CLM_FIeld__c>>();
                }
                List<CLM_FIeld__c> internalFieldList = fieldInternalMap.get((Integer)sectionCustomSetting.Order__c);
                //if(internalFieldList != null){
                SectionWrapper sectionWrap = new SectionWrapper(sectionCustomSetting,internalFieldList);
                // if the section is a VF page or component or there are filed in section then only we will add it in tab otherwise we will not add it.
                // We don't want to show empty sections in tabs
                if((sectionWrap.fieldWrapperList != null && !sectionWrap.fieldWrapperList.isEmpty())||(sectionWrap.section.Is_InlineVF_Page__c || sectionWrap.section.Is_Lightning_Component__c)){
                    sectionWrapperList.add(sectionWrap);
                }
                //}
            }
        }
    }
    
    public class SectionWrapper{
        public CLM_Section__c section {get; set;}
        public String sectionId{get; set;}
        public Integer fieldColumn{get; set;}
        public List<FieldWrapper> fieldWrapperList{get; set;}
        public Integer sequence {get; set;}
        public Map<Integer,FieldWrapper> fieldWrapperMap {get; set;} 
        
        public SectionWrapper(CLM_Section__c sectionCustomSetting,List<CLM_FIeld__c> internalFieldList ){
            this.section = sectionCustomSetting;
            this.sequence = (Integer)sectionCustomSetting.Order__c;
            String pageOrComponentCompleteName = section.Inline_Api_Name_Component_Api__c;
            if(pageOrComponentCompleteName != null){
                String pageOrComponentName = pageOrComponentCompleteName.substringAfterLast('/');
                if(pageOrComponentName == null || pageOrComponentName == ''){
                    pageOrComponentName = pageOrComponentCompleteName.substringAfterLast(':');
                }
                if(pageOrComponentName == null || pageOrComponentName == ''){
                    pageOrComponentName = 'test'+section.Order__c;
                }
                sectionId = pageOrComponentName ;
            }
            fieldWrapperList = new List<FieldWrapper>();
            fieldWrapperMap = new Map<Integer,FieldWrapper>();
            if(internalFieldList != null){
                for(CLM_FIeld__c fieldCustomSetting : internalFieldList){
                    FieldWrapper fieldWrap = new FieldWrapper(fieldCustomSetting);
                    
                    fieldWrapperList.add(fieldWrap);        
                    fieldWrapperMap.put(sequence,fieldWrap);
                }
            }
            if(sectionCustomSetting.No_Of_Column__c != null && sectionCustomSetting.No_Of_Column__c != 0){
                fieldColumn = (Integer)( 12/sectionCustomSetting.No_Of_Column__c);
                
            }
            else{
                fieldColumn  = 6;
            }
        }
    }
    
    public class FieldWrapper{
        public CLM_FIeld__c field {get; set;}
        public Integer sequence {get; set;}
        public String fieldType{get;set;}
        public String viewFieldType{get; set;}//newlly added 25-01-2018
        public Boolean isRequired{get;set;}
        public Boolean isDraftRequired{get;set;}
        public Boolean isEditable{get;set;}
        public String lookUpName{get; set;}
        public String viewLookUpName{get; set;}//newlly added 25-01-2018
        public String errorMessage{get;set;}
        public String className{get;set;}
        public Map<String,FieldDependancyHelper.DependantField> dependantFieldMap{get;set;}
        public String dependantFieldString{get;set;}
        public Boolean isParent{get; set;}
        public String refLabel{get;set;}
        
        //newlly added 19-04 for lookup
        public String  relatedFieldName{get;set;}
        
        public FieldWrapper(CLM_FIeld__c fieldCustomSetting){
            this.field = fieldCustomSetting;
            this.sequence = (Integer)fieldCustomSetting.Order__c;
            this.isRequired = field.Required__c;
            this.isDraftRequired = field.Draft_Required__c;
            
            
            if(field.Create_Blank_Space__c != true){//newlly added if(field1.Create_Blank_Space__c != true) 15-05-2018 
                Schema.SObjectField field1 = conVehicleFieldTypeMap.get(field.FieldApiName__c.trim());
                if(field1 != null){
                    Schema.DisplayType fldType = field1.getDescribe().getType();
                    this.fieldType = ''+fldType;
                    if(fieldType=='REFERENCE'){
                        this.lookUpName=''+field1.getDescribe().getReferenceTo().get(0);
                        
                        if(fieldsMapWithRefranceFieldName.containsKey(''+field.FieldApiName__c.trim()))
                        {
                            this.relatedFieldName = ''+fieldsMapWithRefranceFieldName.get(''+field.FieldApiName__c.trim());
                        }
                    }
                    
                }  
                
                if(field.ViewFieldApi__c == null || field.ViewFieldApi__c == ''){
                    field.ViewFieldApi__c = field.FieldApiName__c;
                }
                //newlly added 25-01-2018
                Schema.SObjectField field2 = conVehicleFieldTypeMap.get(field.ViewFieldApi__c.trim());
                if(field2 != null){
                    Schema.DisplayType viewFldType = field2.getDescribe().getType();
                    this.viewFieldType = ''+viewFldType ;
                    if(viewFieldType =='REFERENCE')
                        this.viewLookUpName=''+field2.getDescribe().getReferenceTo().get(0);
                }  
                
                //System.debug('field.FieldApiName__c 11## = '+fieldsMapAccessible.get('Total_Contract_Value_with_all_Options__c').getDescribe().isUpdateable());
                Schema.SObjectField sobjField = fieldsMapAccessible.get(field.FieldApiName__c);
                if(! fieldsMapAccessible.isEmpty() && sobjField != null){
                    this.isEditable= sobjField.getDescribe().isUpdateable();
                    
                    if(!field.Draft_Required__c && this.isEditable &&  fieldType != 'BOOLEAN'){
                        this.isDraftRequired = !(TM_ContractDetailPageController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().isNillable());  
                    }
                }else{
                    this.isEditable =true;
                }
                
                if(isRequired || isDraftRequired){
                    if(fieldCustomSetting.Required_Error_Message__c != null && fieldCustomSetting.Required_Error_Message__c != ''){
                        errorMessage = fieldCustomSetting.Required_Error_Message__c;
                        
                    }     
                    else{
                        errorMessage = 'You must enter a value';
                    }
                }
                
                // System.debug('In field setting constructor'+FieldDependancyHelper.parentMap);
                // System.debug('In childmap field setting constructor'+FieldDependancyHelper.childmap);
                Map<String,FieldDependancyHelper.DependantField > dependantFieldMap = FieldDependancyHelper.parentMap.get(field.FieldApiName__c.toLowerCase().trim());
                
                System.debug(LoggingLevel.NONE,'dependantFieldMap For'+ field.FieldApiName__c +' = '+FieldDependancyHelper.parentMap.get(field.FieldApiName__c.toLowerCase()));
                if(dependantFieldMap == null){
                    isParent = false;
                    dependantFieldMap = new Map<String,FieldDependancyHelper.DependantField>();
                }
                else{
                    isParent = true;
                }
                this.dependantFieldMap = dependantFieldMap;
                
                this.dependantFieldString = '';
                for(String strKey : dependantFieldMap.keyset()){
                    this.dependantFieldString += this.dependantFieldMap.get(strKey).value+'SPLITVAL'+this.dependantFieldMap.get(strKey).className+'SPLITVAL'+this.dependantFieldMap.get(strKey).apiName+'SPLITREC';
                }
                
                String fieldClassesName = FieldDependancyHelper.childmap.get(field.FieldApiName__c);
                //System.debug('Field Api Name = ' + field.FieldApiName__c +'     fieldClassesName = '+fieldClassesName);
                if(fieldClassesName == null){
                    fieldClassesName = '';
                }
                this.className = fieldClassesName;
                
                
            }  
        }
    }
}