public with sharing class CandidateNewCtrl {
    public Candidate__c candidate{get;set;}
    public Boolean isSuccess{get;Set;}
    public Boolean isWarning{get;Set;}
    // public Boolean isWarning1{get;Set;}
    private String lcaId;
    public List<LC_Candidate_Assignment__c> lcCandidateList{get;set;}
    
    public Attachment resume {
        get {
            if (resume == null)
                resume = new Attachment();
            return resume;
        }
        set;
    }
    
    public CandidateNewCtrl(ApexPages.standardController sc){
        String success = apexpages.currentpage().getparameters().get('success');
        lcaId = apexpages.currentpage().getparameters().get('lcaId');
        lcCandidateList = new List<LC_Candidate_Assignment__c>();
        
        if(success == '1'){
            isSuccess = true;
            //  isWarning = true;
        }else{
            
            candidate = (Candidate__c)sc.getRecord();
            lcCandidateList = [Select id,CLM_Labor_Category__c,Project_LCDB__c,Email__c From LC_Candidate_Assignment__c where id =: lcaId Limit 1];
            if(!lcCandidateList.isEmpty()){ 
                isWarning = false; 
                isSuccess = false;
                if(candidate.id == null){
                    if (Schema.sObjectType.Candidate__c.fields.Email__c.isAccessible() && Schema.sObjectType.Candidate__c.fields.Email__c.isCreateable()) 
                        candidate.Email__c = lcCandidateList[0].Email__c; 
                }else{
                    if (Schema.sObjectType.Candidate__c.fields.Email__c.isAccessible() && Schema.sObjectType.Candidate__c.fields.Email__c.isUpdateable()) 
                        candidate.Email__c = lcCandidateList[0].Email__c; 
                }
            } else{
                isWarning = true;
                isSuccess = true;
                // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Invalid URL'));
                
            }
            
        }
    }
    
    public pagereference save(){
        try{
            if(candidate.Id == null){
                if (Schema.sObjectType.Candidate__c.isCreateable())
                    DMLManager.upsertAsUser(candidate);
            }else{
                if (Schema.sObjectType.Candidate__c.isUpdateable())
                    DMLManager.upsertAsUser(candidate);
            } 
          /* if (Schema.sObjectType.Candidate__c.isAccessible() && Schema.sObjectType.Candidate__c.isCreateable()) {
                insert candidate;
           } */
            //resume.OwnerId = UserInfo.getUserId();
            //Integer count = [SELECT count() FROM Attachment WHERE ParentId = :candidate.Id]+1;
            // resume.name = 'Resume '+count;
            if(resume.body != null){
                if (Schema.sObjectType.Attachment.fields.ParentId.isAccessible() && Schema.sObjectType.Attachment.fields.ParentId.isCreateable()) 
                    resume.ParentId = candidate.Id; // the record the file is attached to
                //if (Schema.sObjectType.Attachment.fields.IsPrivate.isAccessible() && Schema.sObjectType.Attachment.fields.IsPrivate.isCreateable()) 
                //    resume.IsPrivate = true;
                // resume.name = 
                if (Schema.sObjectType.Attachment.isCreateable())
                    DMLManager.insertAsUser(resume);
                resume = new Attachment();    
            }
            
            if(lcaId != null ){
                List<LC_Candidate_Assignment__c> candidateAssignmentList = [Select id,CLM_Labor_Category__c,Project_LCDB__c,Email__c From LC_Candidate_Assignment__c where id =: lcaId Limit 1];
                if(!candidateAssignmentList.isEmpty()){
                    
                    LC_Candidate_Assignment__c lcCandidateAssignment = candidateAssignmentList[0];
                    system.debug('lcCandidateAssignment==' + lcCandidateAssignment);
                    Candidate_Application__c candidateApplication = new Candidate_Application__c();
                    if (Schema.sObjectType.Candidate_Application__c.fields.Candidate__c.isAccessible() && Schema.sObjectType.Candidate_Application__c.fields.Candidate__c.isCreateable()) 
                        candidateApplication.Candidate__c = candidate.id;
                    if (Schema.sObjectType.Candidate_Application__c.fields.Email__c.isAccessible() && Schema.sObjectType.Candidate_Application__c.fields.Email__c.isCreateable()) 
                        candidateApplication.Email__c = candidateAssignmentList[0].Email__c;
                    if (Schema.sObjectType.Candidate_Application__c.fields.Labor_Category__c.isAccessible() && Schema.sObjectType.Candidate_Application__c.fields.Labor_Category__c.isCreateable()) 
                        candidateApplication.Labor_Category__c = lcCandidateAssignment.CLM_Labor_Category__c; 
                    
                    if (Schema.sObjectType.Candidate_Application__c.isCreateable())
                        DMLManager.insertAsUser(candidateApplication);
                    if (Schema.sObjectType.LC_Candidate_Assignment__c.isDeletable())
                         DMLManager.deleteAsUser(lcCandidateAssignment);
                    
                }
            }
            System.debug('Success');
            PageReference paeRef = new PageReference ('/apex/CandidateNewPage?success=1');
            paeRef.setRedirect(true);
            return paeRef;
        }catch(Exception ex){
            System.debug('Failure = '+ex);
            return Null;
        }
    }
}