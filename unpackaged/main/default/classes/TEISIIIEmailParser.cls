public class TEISIIIEmailParser {
    
    private String emailSubject;
    public String taskOrderNumber;
    private Date questionDueDate;    
    private Datetime proposalDueDateTime;    
    private Contact con;
    private String classification;
    public boolean isAmendent;   //AMENDMENT  On 25-Sep-2019 by Swarna
    public boolean isCancel; // Cancellation  On 25-Sep-2019 by Swarna
    
    public void parse(Messaging.InboundEmail email){
        // On 25-Sep-2019 by Swarna -- start
        isAmendent = false;
        isCancel = false;
        // On 25-Sep-2019 by Swarna -- end
        emailSubject = email.subject;
        if(emailSubject.length() == 0){
            throw new ContractVehicleNotFoundException(MessageConstantController.TASK_ORDER_NOT_FOUND );
        }
        
        if(emailSubject.startsWith('Fwd:'))
            emailSubject = emailSubject.replace('Fwd:', '');
        if(emailSubject.startsWith('Re:'))
            emailSubject = emailSubject.replace('Re:', '');
        
        taskOrderNumber = emailSubject.substringBetween('PWS ',' ');
        
        System.debug('taskOrderNumber : ');
        if(taskOrderNumber == null){
            // On 24-Sep-2019 by Swarna -- start
            taskOrderNumber = emailSubject.substringBetween('PWS-','-'); 
            System.debug('taskOrderNumber - : '+taskOrderNumber);
            
            if(taskOrderNumber == null){
                taskOrderNumber = emailSubject.substringAfter('PWS ');
            }
            else{
                taskOrderNumber = 'PWS-'+taskOrderNumber;
            }
            System.debug('taskOrderNumber after : '+taskOrderNumber);
            //taskOrderNumber = taskOrderNumber.replaceAll('\\(.*\\)','');
        }
        else
        {
            taskOrderNumber = 'PWS '+taskOrderNumber;
        }
        // On 24-Sep-2019 by Swarna -- end
        System.debug('--- '+taskOrderNumber);
        
        String body = email.plainTextBody;
        System.debug('body ----- : '+body);
        // On 25-Sep-2019 by Swarna -- start
         if(emailSubject.toUpperCase().contains('AMENDMENT')){
            System.debug('AMENDMENT : '+emailSubject);
            isAmendent = true;
            return;
        }
        
        if(body.toLowerCase().contains('cancelling')){
            System.debug('cancelling : '+body);
            isCancel = true;
            return;
        }
        // On 25-Sep-2019 by Swarna -- end
        
        if(body!=null && body!=''){
            String bodyNormalize = body.normalizeSpace();
            String questionDateRaw = bodyNormalize.substringBetween('Questions is ', ',');
            // On 24-Sep-2019 by Swarna -- start
            System.debug('questionDateRaw 1 : '+questionDateRaw);
            if(questionDateRaw==null || questionDateRaw=='')
            {
                questionDateRaw = bodyNormalize.substringBetween('Questions Deadline: ', '.');
            }
            System.debug('questionDateRaw 2 : '+questionDateRaw);
            // On 24-Sep-2019 by Swarna -- end
            System.debug('questionDateRaw=='+questionDateRaw);
            String proposalDateRaw = bodyNormalize.substringBetween('submitted proposal is ', 'Looking forward to seeing you tomorrow.');
            // On 24-Sep-2019 by Swarna -- start
            System.debug('proposalDateRaw 1 : '+proposalDateRaw);
            if(proposalDateRaw==null || proposalDateRaw=='')
            {
                proposalDateRaw = bodyNormalize.substringBetween('Task Order Proposal Request (TOPR) Closing Date and Time ', '.');
            }
            System.debug('proposalDateRaw 2 : '+proposalDateRaw);
            // On 24-Sep-2019 by Swarna -- end
            proposalDateRaw = proposalDateRaw.replaceAll('\\.', '');
            System.debug('proposalDateRaw=='+proposalDateRaw);
            if(questionDateRaw != null && questionDateRaw != ''){
                DateTimeHelper dtHelper = new DateTimeHelper();
                DateTime questionDueDateTime = dtHelper.getDateTimeSpecialCase(questionDateRaw);
                questionDueDate = questionDueDateTime.date();
                System.debug('questionDueDate=='+questionDueDate);
            }
            
            if(proposalDateRaw != null && proposalDateRaw != ''){
                DateTimeHelper dtHelper = new DateTimeHelper();
                System.debug('proposalDateRaw B4 : '+proposalDateRaw);
                proposalDateRaw = proposalDateRaw.remove('(').remove(')');
                System.debug('proposalDateRaw After : '+proposalDateRaw);
                proposalDueDateTime = dtHelper.getDateTimeSpecialCase(proposalDateRaw);
                System.debug('proposalDueDateTime=='+proposalDueDateTime);
            }
            
            if(bodyNormalize.contains('CLASSIFICATION: '))
                classification = bodyNormalize.substringBetween('CLASSIFICATION: ', ' ');
            System.debug('classification=='+classification);
            
            //String contactInfo = body.substringAfter('Respectfully,');        
            //System.debug('contactInfo=='+contactInfo.split('\n'));        
            //createContact(contactInfo);
            
            // New changes New Contact method
            createContact(body,email);
        }
    }
    
    public Task_Order__c getTaskOrder(String toAddress){ 
        if(taskOrderNumber==null){
            return null;
        }        
        Task_Order__c taskOrder = new Task_Order__c();
        List<Contract_Vehicle__c> conVehicleList = [SELECT Id FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];        
        if(!conVehicleList.isEmpty()){
            taskOrder.Contract_Vehicle__c = conVehicleList[0].Id; // Contract Vehicle
            taskOrder.Name = taskOrderNumber;//Task Order Number
            
            if(classification != null)
                taskOrder.Classification__c = classification; // Classification
            
            if(questionDueDate != null){            
                taskOrder.Questions_Due_Date__c = questionDueDate; // Questions Due Date
            }
            
            if(proposalDueDateTime != null){            
                taskOrder.Due_Date__c = proposalDueDateTime.date(); // Proposal Due Date
                taskOrder.Proposal_Due_DateTime__c = proposalDueDateTime; // Proposal Due Date/Time
            }
            
            try{
                if(con.Id == null)
                    DMLManager.insertAsUser(con); 
            }catch(Exception e){
                System.debug(con);
            }
            
            if(con.Id!= null) {           
                if(con.Title != null){
                    if(con.Title == 'Contract Specialist')
                        taskOrder.Contract_Specialist__c = con.Id;
                    if(con.Title == 'Contracting Officer')
                        taskOrder.Contracting_Officer__c = con.Id;
                }
            }
            
            taskOrder.Release_Date__c = Date.today(); //RFP/TO Release Date
            taskOrder.Contract_Vehicle_picklist__c = 'TEIS III (US ARMY TOTAL ENGINEERING AND INTEGRATION SERVICES)';
            
            
            if(isCancel){
                taskOrder.Is_cancelled__c = true;
            }
            
            return taskOrder;
        }else{
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
        }
    }
    
    public Task_Order__c updateTaskOrder(Task_Order__c taskOrder, String toAddress){
        List<Contract_Vehicle__c> conVehicleList = [SELECT Id FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];
        
        if(!conVehicleList.isEmpty()){
            taskOrder.Contract_Vehicle__c = conVehicleList[0].Id; // Contract Vehicle
            
            if(classification != null)
                taskOrder.Classification__c = classification; // Classification
            
            if(questionDueDate != null){            
                taskOrder.Questions_Due_Date__c = questionDueDate; // Questions Due Date
            }
            
            if(proposalDueDateTime != null){            
                taskOrder.Due_Date__c = proposalDueDateTime.date(); // Proposal Due Date
                taskOrder.Proposal_Due_DateTime__c = proposalDueDateTime; // Proposal Due Date/Time
            }            
            
            try{
                if(taskOrder.Customer_Agency__c != null){
                    con.AccountId = taskOrder.Customer_Agency__c; // Set Customer Organization of the Task Order as an Account for Contact
                    System.debug('Customer Organization = ' + con.AccountId);
                    //DMLManager.upsertAsUser(con);
                    //upsert con;
                    if(con.Id == null){
                        // if Task Order with Customer Organization is not having a Contact or if a different Contact(i.e. a Contact with different Email Id) is found
                        DMLManager.insertAsUser(con);
                    }else{
                        // if Task Order with Customer Organization is having a Contact, then update that Contact
                        DMLManager.updateAsUser(con);
                    }
                }else{
                    if(con.Id == null)
                        DMLManager.insertAsUser(con);                    
                }                
            }catch(Exception e){
                System.debug(''+e.getMessage()+'\t'+e.getLineNumber());
            }
            
            if(con.Id != null) {           
                if(con.Title != null){
                    if(con.Title == 'Contract Specialist')
                        taskOrder.Contract_Specialist__c = con.Id;
                    if(con.Title == 'Contracting Officer')
                        taskOrder.Contracting_Officer__c = con.Id;
                }
            }  
            
            if(isCancel){
                taskOrder.Is_cancelled__c = true;
            }
            
            return taskOrder;
        }else{
            system.debug('##################################');
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
            
        }
    }
    
    /*private void createContact(String contactInfo){
        //System.debug(''+contactInfo);        
        try{
            con = new Contact();
            contactInfo = contactInfo.trim();
            String[] contactInfoSplit = contactInfo.split('\n');
            System.debug(''+contactInfoSplit);
            if(contactInfoSplit.size()>0){
                String email;
                //String[] contact1InfoSplit = contactInfoSplit[0].split(' ');
                for(String emailId : contactInfoSplit){
                    System.debug('emailId = '+emailId);
                    // if(emailId.contains('@') && emailId.contains('.com')){
                    if(emailId.contains('@')){
                        email = emailId.trim();
                        break;
                    }                    
                }
                
                System.debug('email = '+email);
                if(email!=null){
                    //con = new Contact();
                    List<Contact> contactList;  
                    contactList = [SELECT Id, Name, Title FROM Contact WHERE email =:email];                    
                    
                    if(contactList != null && !contactList.isEmpty() ){//for existing contact
                        System.debug('if = '+contactList[0].id);
                        con = contactList[0];               
                    }else{//for new contact;
                        System.debug('else');
                        con.Is_FedTom_Contact__c = true;
                        con.Email = email;
                        String[] namesplit = contactInfoSplit[2].split(' ');
                        if(namesplit!= null && !namesplit.isEmpty()){
                            if(nameSplit.size()==2){
                                con.firstName = nameSplit[0];
                                con.lastName = nameSplit[1];
                            }else if(nameSplit.size()==3){
                                con.firstName = nameSplit[0];
                                con.lastName = nameSplit[2];
                            }else {
                                con.lastName = nameSplit[0];
                            }
                        }
                        
                        for(String info : contactInfoSplit){
                            if(info.toLowerCase().contains('contract specialist') ){
                                con.title = 'Contract Specialist';
                            }else If(info.toLowerCase().contains('contracting officer')){
                                con.title = 'Contracting Officer';
                            }else{
                                if(info.contains('Ph: ')){
                                    try{
                                        con.Phone = info.substringBetween('Ph: ', 'Fax:').trim();
                                    }catch(Exception e){
                                        System.debug(''+e.getMessage()+'\t'+e.getLineNumber());
                                    }
                                }
                                if(info.contains('Fax: ')){
                                    try{
                                        con.Fax = info.substringBetween('Fax: ', 'E-Mail Address:').trim();
                                    }catch(Exception e){
                                        System.debug(''+e.getMessage()+'\t'+e.getLineNumber());
                                    }
                                }
                            }    
                        }    
                    }
                }
            }
            
            System.debug('con=='+con);
        }catch(Exception e){
            System.debug(''+e.getMessage()+'\t'+e.getLineNumber());
        }
        
    }*/
    
     private void createContact(String emailBody,Messaging.InboundEmail email){
        try{
            System.debug('emailBody : '+emailBody);
            System.debug('emailBody.toLowerCase().contains(contract specialist) '+emailBody.toLowerCase().contains('contract specialist'));
            con = new Contact();            
            List<Contact> contactList = [Select id, Email, Title from Contact WHERE Email = :email.fromAddress];
            if(contactList != null && !contactList.isEmpty() ){//for existing contact
                con = contactList[0]; 
            }else{//for new contact
                System.debug('New Contact : '+email.fromName);
                String emailFromName = email.fromName;        
                String[] nameSplited = emailFromName.split(' ');
                con.FirstName = nameSplited[0];
                con.LastName = nameSplited[1];
                con.Email =  email.fromAddress;
                con.Is_FedTom_Contact__c = true;
                String contactInfo = emailBody.substringAfter('Respectfully,');  
                System.debug('contactInfo  : '+contactInfo);      
                //System.debug('*********'+contactInfo.split('\n'));
                if(contactInfo!=null && contactInfo!='')
                {
                    contactInfo = contactInfo.trim();
                    String[] contactInfoSplit = contactInfo.split('\n');
                    if(contactInfoSplit.size()>0){                    
                        for(String info : contactInfoSplit){
                            //System.debug('info^^^^^^^^^^'+info);
                            if(info.toLowerCase().contains('contract specialist') ){
                                con.title = 'Contract Specialist';
                            }else If(info.toLowerCase().contains('contracting officer')){
                                con.title = 'Contracting Officer';
                            }                               
                        }
                    }
                }
                // On 25-Sep-2019 for new email template by Swarna -- end
                /*if(contactInfo=='')*/
                else
                {
                    System.debug('contactInfo empty');
                    if(emailBody.toLowerCase().contains('contract specialist'))
                    {
                        con.title = 'Contract Specialist';
                    }
                    else
                    {
                        con.title = 'Contracting Officer';
                    }
                }
                // On 25-Sep-2019 for new email template by Swarna -- end
            }                                                                                  
        }catch(Exception ex){
            System.debug('Create Contact Error ='+ex.getMessage()+'\tLine Number = '+ex.getLineNumber());
        }                
    }
    
}