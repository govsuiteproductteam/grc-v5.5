public class TM_ContractNewPageController {
    public static Map<String, Schema.SObjectField> fieldsMapAccessible{get;set;}
    public static Map<String,Schema.SObjectField> conVehicleFieldTypeMap{get;set;}
    public static Map<String,String> fieldsMapWithRefranceFieldName{get;set;} //newlly added 19-04
    public String isValidLiecence{get;set;}
    static{
        conVehicleFieldTypeMap =  Schema.SObjectType.Contract_Vehicle__c.fields.getMap();
        fieldsMapAccessible=Schema.getGlobalDescribe().get('TM_TOMA__Contract_Vehicle__c').getDescribe().SObjectType.getDescribe().fields.getMap();
        //Newlly added 19-04
         fieldsMapWithRefranceFieldName = new Map<String,String>();
        for(Schema.SObjectField sobjectField : fieldsMapAccessible.values())
        {
            String tempField = sobjectField.getDescribe().getName();
            System.debug('tempField  '+tempField);
            String typeOFField = sobjectField.getDescribe().getType()+'';
            if(typeOFField == 'REFERENCE' && tempField.endsWith('__c'))
            {
                fieldsMapWithRefranceFieldName.put(tempField, sobjectField.getDescribe().getRelationshipName()+'.Name');
            }
            else if(typeOFField == 'REFERENCE' && tempField == 'OwnerId'){
                fieldsMapWithRefranceFieldName.put(tempField, sobjectField.getDescribe().getRelationshipName()+'.Name');
            }
            
        }
    }
    
    public Contract_Vehicle__c contractVehicle{get; set;}
    public String recordId{get; set;}
    public Boolean isEdit{get; set;}
    public List<TabWrapper> tabWrapperList{get; set;}
    public List<SelectOption> recordTypeOptionList{get; set;}
    public Boolean isChangeRecordType{get; set;}
    public ApexPages.StandardController controller{get; set;}
    public Map<String,Map<String,FieldDependancyHelper.DependantField>> parentMap{get; set;}
    public Map<Integer,String> monthMap{get; set;}
    Public String allTab{get;set;} 
    public String mode{get;set;}
    public Boolean stopProcessing{get; set;}
    public String cloneFlag;
    public Boolean isRecClone{get; set;}//newly added for redirection on cancel @ 03/06/2019
    //public String 
    public String isCloneString;//newlly added
    private Boolean isRecCloned;//newlly added
    
     //for nooverride
    private Id toId;
    private String pageReferEdit;
     public String isValidFedTomLiecence{get;set;}
    //End
    
    public TM_ContractNewPageController (ApexPages.StandardController controller) {
        isValidLiecence = LincenseKeyHelper.checkLicensePermitionForFedCLM();
         isValidFedTomLiecence = LincenseKeyHelper.checkLicensePermitionForFedTOM();//newlly added for no override
        if(isValidLiecence == 'Yes'){
            stopProcessing = false;
            isRecClone = false;
            isRecCloned = false;
            putMonthMapData();
            mode = 'view';
            this.controller = controller;
            initMember();
            if(!stopProcessing){
                init();
                contractVehicle = (Contract_Vehicle__c)controller.getRecord();
                recordId= contractVehicle.Id;
                System.debug('recordId INIT'+recordId);
                fillRecordTypeOptions();
            }
        }
        else if(isValidFedTomLiecence == 'Yes'){//newlly added for override code
            System.debug('@@@@@@@@@@@ '+ApexPages.CurrentPage());
            toId = ApexPages.CurrentPage().getparameters().get('id');
            Schema.DescribeSObjectResult r = Contract_Vehicle__c.sObjectType.getDescribe();
            String keyPrefix = r.getKeyPrefix();
           // Boolean isLightningPage = UserInfo.getUiThemeDisplayed() == 'Theme4d';
          // System.debug('isLightningPage = '+isLightningPage);
            if(toId!=Null){
            //  if(isLightningPage){
                   // System.debug('isLightningPage = '+isLightningPage);
                   // pageReferEdit = '/one/one.app?source=alohaHeader#/sObject/'+toId+'/edit';
                    //  pageReferEdit = '/one/one.app#/sObject/'+toId+'/edit'+'&nooverride=1';
                   // pageReferEdit = '/one/one.app#/sObject/'+toId+'/edit;
                  //https://fedtomdev-dev-ed.lightning.force.com/one/one.app#/sObject/a0R37000002Dx25EAC/edit
                  //https://fedtomdev-dev-ed.lightning.force.com/one/one.app#/sObject/a003700000A2ab5AAB/edit
               // }else{
                   pageReferEdit = '/'+toId+'/e?retURL=/'+toId+'&nooverride=1'; 
                   //pageReferEdit = '/'+keyPrefix+'/e?retURL=/'+toId+'/o&nooverride=1&id='+toId;
                    // pageReferEdit = '/'+toId+'/e?retURL=/'+toId+'/o&nooverride=1'; 
               // pageReferEdit = '/apex/testcheckrendered?id='+toId;
               // }                              
                System.debug('@@@@ pageReferEdit '+pageReferEdit);
            }
            else if(toId==Null){
                string recId = ApexPages.CurrentPage().getparameters().get('RecordType');
                pageReferEdit = '/'+keyPrefix+'/e?retURL=/'+keyPrefix+'/o&nooverride=1';
                if(recId!=Null)
                {
                    pageReferEdit = pageReferEdit+'&RecordType='+recId;
                }
            }
        }
    }
    
    //newlly added for no override 02-05
    public PageReference setRedirectEdit(){
        System.debug('pageReferEdit '+pageReferEdit);
        if(pageReferEdit != null)
        {
            PageReference pg = new Pagereference(pageReferEdit);
            pg.setRedirect(true);
            return pg;
        }
        return null;
    }
    
    private void putMonthMapData(){
        monthMap = new Map<Integer,String>();
        monthMap.put(1,'JAN');
        monthMap.put(2,'FEB');
        monthMap.put(3,'MAR');
        monthMap.put(4,'APR');
        monthMap.put(5,'MAY');
        monthMap.put(6,'JUN');
        monthMap.put(7,'JUL');
        monthMap.put(8,'AUG');
        monthMap.put(9,'SEP');
        monthMap.put(10,'OCT');
        monthMap.put(11,'NOV');
        monthMap.put(12,'DEC');
    }
    
    private void getFieldDependancyData(){
        FieldDependancyHelper fieldDependancyHelper = new FieldDependancyHelper();
        fieldDependancyHelper.process(contractVehicle.RecordTypeId,true);
        
    }
    
    private void initMember(){
        isChangeRecordType = false;
        //System.debug('In getTabStructure');
        //contractVehicle = (Contract_Vehicle__c)controller.getRecord();
        String contractVehivleId = ApexPages.CurrentPage().getparameters().get('id');
        if(contractVehivleId != null){
            cloneFlag = ApexPages.CurrentPage().getparameters().get('clone');
            
            List<Contract_Vehicle__c> contractVehicleList = [Select id,RecordTypeId From Contract_Vehicle__c where id =: contractVehivleId  Limit 1];
            if(!contractVehicleList.isEmpty()){
                contractVehicle = contractVehicleList[0];
            }
            else{
                contractVehicle = new Contract_Vehicle__c();
            }
        }
        List<CLM_FIeld__c> fieldCustomSettingList = [Select Id,Draft_Required__c,FieldApiName__c,FieldLabel__c,HelpText__c,Is_Contract__c,
                                                     Order__c,RecordType_Id__c,  Required__c,TabNumber__c,ViewFieldApi__c ,Required_Error_Message__c,Create_Blank_Space__c,Use_Custom_lookup__c
                                                     From CLM_FIeld__c where Is_Contract__c = true order by TabNumber__c asc , Order__c asc];
        Set<String> fieldApiNameSet = new Set<String>();
        fieldApiNameSet.add('TM_TOMA__Is_Clone__c');
        Boolean isOwnerIdFound = false;
        Boolean isRecordTypeIdFound = false;                                                 
        if(!fieldCustomSettingList.isEmpty()){
            
            for(CLM_FIeld__c clmField : fieldCustomSettingList ){
                if(clmField.FieldApiName__c != null  && clmField.Create_Blank_Space__c != true){//newlly added 15-05-2018 && clmField.Create_Blank_Space__c != true
                    if(clmField.FieldApiName__c.equalsIgnoreCase('OwnerId')){
                        isOwnerIdFound = true;
                    }
                    if(clmField.FieldApiName__c.equalsIgnoreCase('RecordTypeId')){
                        isRecordTypeIdFound = true;
                    }
                    fieldApiNameSet.add(clmField.FieldApiName__c );
                    //newlly added 19-04
                    if(fieldsMapWithRefranceFieldName.containsKey(''+clmField.FieldApiName__c))
                    {
                        fieldApiNameSet.add(fieldsMapWithRefranceFieldName.get(''+clmField.FieldApiName__c));
                    }
                }
            }
            
        }
        else{
            stopProcessing = true;
        }
        if(!isOwnerIdFound )
            fieldApiNameSet.add('OwnerId');
        if(!isRecordTypeIdFound )
            fieldApiNameSet.add('RecordTypeId');
        //System.debug('fieldApiNameSet = '+fieldApiNameSet);   
        if (!Test.isRunningTest()) { 
            controller.addFields(new List<String>(fieldApiNameSet));
        }
        contractVehicle = (Contract_Vehicle__c)controller.getRecord();
        isCloneString = ApexPages.CurrentPage().getparameters().get('clone'); 
        if(isCloneString!=null && isCloneString =='1'){
            isRecCloned=true;
            isRecClone = true;
        }
        if(isRecCloned){
            // contractVehicle = (Contract_Vehicle__c)controller.getRecord();
            contractVehicle.TM_TOMA__Is_Clone__c=true;
            Map<String, Schema.SObjectField> fieldsMapAccessible = Schema.getGlobalDescribe().get('TM_TOMA__Contract_Vehicle__c').getDescribe().SObjectType.getDescribe().fields.getMap();
            
            if(fieldsMapAccessible != null && ! fieldsMapAccessible.isEmpty()){
                for(String fieldApi : fieldApiNameSet){
                    Schema.SObjectField objField =fieldsMapAccessible.get(fieldApi);
                    if(objField!= null && objField.getDescribe().isUnique() && objField.getDescribe().isAccessible()){
                        system.debug('fieldApi--'+fieldApi);
                        contractVehicle.put(fieldApi,null);
                    }
                }
            }
            
        }
    }
    
    private void init(){
       /* 
        getFieldDependancyData();
        this.parentMap = FieldDependancyHelper.parentMap;
       */ 
        
        isEdit = false;
        //To get contract Vehicle information from database as per record id
        
        //Declair and define Tabwrapper list to retun it to invoking function.
        tabWrapperList = new List<TabWrapper>();
        
        
        //if(!contractVehicleList.isEmpty()){
        //String recordTypeId = contractVehicle.recordTypeId;
        
        //newlly added cod for record type, if record type changes blank to other record type 
        //System.debug('contractVehicle RecordType '+ApexPages.CurrentPage().getparameters().get('RecordType'));
        //newlly uncomented 02-05
        string recId = ApexPages.CurrentPage().getparameters().get('RecordType');
        System.debug('recId '+recId);
        if(recId != null){
            System.debug('recId '+recId);
            contractVehicle.RecordTypeId = recId;
            System.debug('contractVehicle.RecordTypeId '+contractVehicle.RecordTypeId);
        }
        else if(recId == null && contractVehicle.RecordTypeId != null){// Added 25-06-2018 Due to Edit RecordType Chage Issue
            System.debug('Inside not null condition');
        }
       else{// newlly added code if we have onlly one record type active 18-05-2018
           System.debug('inside else ');
           /* List<RecordType> rtypes = [Select Name, Id From RecordType where sObjectType='TM_TOMA__Contract_Vehicle__c' and isActive=true];
            //Id devRecordTypeId = Schema.SObjectType.TM_TOMA__Subcontract__c.getRecordTypeInfosByName().get('Master Subcontract').getRecordTypeId();
           if(rtypes != null) 
           contractVehicle.RecordTypeId = rtypes[0].Id;*/
           System.debug('@@@@@@@@@ 21-05');
           List<Id> ValidRecTypeId = New List<Id>();
            List<String> recordTypeNameList = new List<String>();
            Schema.DescribeSObjectResult R = TM_TOMA__Contract_Vehicle__c.SObjectType.getDescribe();
            List<Schema.RecordTypeInfo> RT = R.getRecordTypeInfos();
            for( Schema.RecordTypeInfo recordType : RT )
            {
                if(recordType.isAvailable())
                { 
                    ValidRecTypeId.add(recordType.getRecordTypeId());
                    
                }
            }
            if(ValidRecTypeId != null && ! ValidRecTypeId.isEmpty()){
                String recordTypeAvailable = ValidRecTypeId[0];
                List<RecordType> tempRecList = [SELECT Id,Name FROM RecordType WHERE isActive = true AND Id=:recordTypeAvailable];
                if(tempRecList != null && ! tempRecList.isEmpty()){
                    contractVehicle.RecordTypeId = recordTypeAvailable; 
                }
            }
           
           
        }
        
        
        String recordTypeId =contractVehicle.RecordTypeId;
        if(recordTypeId == null){
            recordTypeId = '';
        }
        if(recordTypeId.length() > 15){
            recordTypeId = recordTypeId.substring(0,15);
        }
        
        
        getFieldDependancyData();
        this.parentMap = FieldDependancyHelper.parentMap;
        //All the custom setting for specific record type in order based on tab and order no.
        //we take the data in order to keep it in proper order while creaeting data set.
        List<CLM_FIeld__c> fieldCustomSettingList = [Select Id,Draft_Required__c,FieldApiName__c,FieldLabel__c,HelpText__c,Is_Contract__c,
                                                     Order__c,RecordType_Id__c,  Required__c,TabNumber__c,ViewFieldApi__c,Required_Error_Message__c,Create_Blank_Space__c,Use_Custom_lookup__c
                                                     From CLM_FIeld__c where RecordType_Id__c =: recordTypeId And Is_Contract__c = true order by TabNumber__c asc , Order__c asc];
        
        ////System.debug('fieldCustomSettingList = '+fieldCustomSettingList);
        //This map is to store data according to tabSequnce there are mutiple section and then according to section order there are multiple field in section
        //we create data set as Map<TabNumber,Map<SectionOrder,List<NumberOfFieldsIn section>>>
        
        Map<Integer,Map<Integer,List<CLM_FIeld__c>>> fieldCustomSettingMap = new Map<Integer,Map<Integer,List<CLM_FIeld__c>>>();
        for(CLM_FIeld__c fieldCustomSetting : fieldCustomSettingList){
            Map<Integer,List<CLM_FIeld__c>> fieldInternalMap = fieldCustomSettingMap.get((Integer)fieldCustomSetting.TabNumber__c);
            if(fieldInternalMap == null){
                fieldInternalMap = new Map<Integer,List<CLM_FIeld__c>>();
            }
            
            if(fieldCustomSetting.Order__c != null){
                //WE have section order in 10, 20 , 30... format and field order in 1001,1002,2001..., where last 2 digit specify squence 
                //and first remainig specify section order so thats why we divide field order with 100 to get section order.
                Integer sectionNumber = ((Integer)fieldCustomSetting.Order__c)/100;
                List<CLM_FIeld__c> internalFieldList = fieldInternalMap.get(sectionNumber);
                if(internalFieldList == null){
                    internalFieldList = new List<CLM_FIeld__c>();
                }
                internalFieldList.add(fieldCustomSetting);
                fieldInternalMap.put(sectionNumber,internalFieldList);
                fieldCustomSettingMap.put((Integer)fieldCustomSetting.TabNumber__c,fieldInternalMap);
            }
            //fieldApiNameSet.add(fieldCustomSetting.FieldApiName__c);
            
        }
        //To get all section custom setting for specific recordType and also it is in order to maintain data structure.
        List<CLM_Section__c> sectionCustomSettingList = [Select Id,BackgroundColor__c,Height__c,Inline_Api_Name_Component_Api__c,Is_Contract__c,Is_InlineVF_Page__c,
                                                         Is_Lightning_Component__c, No_Of_Column__c,Order__c,RecordType_Id__c,SectionName__c,TabNumber__c,Width__c,AppApi__c 
                                                         From CLM_Section__c where RecordType_Id__c =: recordTypeId And Is_Contract__c = true And Is_InlineVF_Page__c = false And 
                                                         Is_Lightning_Component__c = false order by TabNumber__c asc , Order__c asc];
        //To keep section List according to tabOrder
        ////System.debug('sectionCustomSettingList = '+sectionCustomSettingList);
        Map<Integer,List<CLM_Section__c>> sectionMap = new Map<Integer,List<CLM_Section__c>>();
        for(CLM_Section__c sectionCustomSetting : sectionCustomSettingList){
            List<CLM_Section__c> internalSectionList = sectionMap.get((Integer)sectionCustomSetting.TabNumber__c);
            if(internalSectionList == null){
                internalSectionList = new List<CLM_Section__c>();
            }
            internalSectionList.add(sectionCustomSetting);
            sectionMap.put((Integer)sectionCustomSetting.TabNumber__c,internalSectionList);
        }
        List<CLM_Tab__c> tabCustomSettingList = [Select Id,Is_Contract__c,Order__c,RecordType_Id__c,TabName__c,BackgroundColor__c
                                                 From CLM_Tab__c where RecordType_Id__c =: recordTypeId And Is_Contract__c = true order by Order__c asc];
        //Here we have apply a logic to  create a data set as we have data set that we have mutiple tab and inside one tab there are mutiple sectin
        //and inside section there are either  multiple fields or components or VF page.
        ////System.debug('tabCustomSettingList = '+tabCustomSettingList);
        Integer index = 1;
        allTab = '';
        //newly added for Fake tab @ 21/05/2019
        if(tabCustomSettingList.isEmpty()){
            tabCustomSettingList = new List<CLM_Tab__c>(); 
            tabCustomSettingList.add(new CLM_Tab__c(Order__c=0,TabName__c='Fake Tab',BackgroundColor__c='Red'));
        }  
        for(CLM_Tab__c tabCustomSetting : tabCustomSettingList){
            Integer tabOrderNumber = (Integer)tabCustomSetting.Order__c;
            if(tabOrderNumber != null){
                List<CLM_Section__c> internalSectionList = sectionMap.get((Integer)tabCustomSetting.Order__c);   
                Map<Integer,List<CLM_FIeld__c>> fieldInternalMap = fieldCustomSettingMap.get((Integer)tabCustomSetting.Order__c);
                if(fieldInternalMap == null){
                    fieldInternalMap = new Map<Integer,List<CLM_FIeld__c>>();
                }
                if(internalSectionList != null){
                    TabWrapper tabWrap = new TabWrapper(tabCustomSetting,internalSectionList,fieldInternalMap);
                    //if there is no section in tab then we don't want to show empty tab on page layout so we will not add in data set.
                    if(tabWrap.sectionWrapperList != null && !tabWrap.sectionWrapperList.isEmpty()){
                        tabWrap.sequence = index;
                        tabWrapperList.add(tabWrap);
                    }
                }
            }
            allTab += 'tabedit'+index+',';
            index++;
        }
        if(allTab.length() > 0){
            allTab = allTab.substring(0,allTab.length()-1);
        }
        //System.debug('tabWrapperList = '+tabWrapperList);
        
        
        /*for(TabWrapper tabWrapper : tabWrapperList){
//System.debug('Tab Name = '+tabWrapper.tab.TabName__c);
//System.debug('Tab Order = '+tabWrapper.tab.Order__c);
for(SectionWrapper sctionWrap : tabWrapper.sectionWrapperList){
//System.debug('sction Name = '+sctionWrap.section.SectionName__c);
//System.debug('sction Order = '+sctionWrap.section.Order__c);
for(FieldWrapper fieldWrap : sctionWrap.fieldWrapperList){
//System.debug('field Name = '+fieldWrap.field.FieldApiName__c);
//System.debug('field Order = '+fieldWrap.field.Order__c);
} 
}
}*/
    }
    
    private void fillRecordTypeOptions(){
        recordTypeOptionList = new List<SelectOption>();
        Schema.DescribeSObjectResult describeResult = Contract_Vehicle__c.SObjectType.getDescribe();
        List<Schema.RecordTypeInfo> recordTypeInfoList = describeResult .getRecordTypeInfos();
        for(Schema.RecordTypeInfo recordTypeInfo: recordTypeInfoList){
            if(recordTypeInfo.isAvailable() && recordTypeInfo.getName() != 'Master'){
                recordTypeOptionList.add(new SelectOption(recordTypeInfo.getRecordTypeId(),recordTypeInfo.getName()));
            }
        }
    }
    
    public void changeRecordType(){
        isChangeRecordType = true;
        mode = 'edit';
        init();
    }
    
    
    public pagereference saveContract1(){
        try{
            if(contractVehicle.Contract_Status_Backend__c != null || contractVehicle.Contract_Status_Backend__c != ''){
                contractVehicle.Contract_Status_Backend__c = 'Draft';
            }
            if(cloneFlag != null && cloneFlag == '1'){
                contractVehicle.id = null;
            }
            upsert contractVehicle;
            //System.debug('Successfully Updated');
            isChangeRecordType = false;
            mode = 'view';
            return new pagereference('/'+contractVehicle.Id);
        }
        catch(Exception ex){
            //System.debug('Exception = '+ex);
            mode = 'edit';
            String errorMessage = ''+ex;
            errorMessage  = errorMessage.substringAfter('first error:');
            ////System.debug(LoggingLevel.info,'errorMessage  = '+errorMessage  );
            Boolean found = false;
            if(ApexPages.hasMessages()){
                // //System.debug(LoggingLevel.info,'errorMessage  = '+errorMessage  );
                ApexPages.Message[] messageArray = ApexPages.getMessages();
                SObjectType accountType = Schema.getGlobalDescribe().get('TM_TOMA__Contract_Vehicle__c');
                Map<String,Schema.SObjectField> mfields = accountType.getDescribe().fields.getMap();
                
                ////System.debug(LoggingLevel.info,'mfields = '+mfields );
                String fieldApiName = errorMessage.subStringBetween(': [',']');
                ////System.debug(LoggingLevel.info,'fieldApiName = '+fieldApiName );
                Schema.SObjectField field = mfields.get(fieldApiName );
                ////System.debug(LoggingLevel.info,'field = '+field );
                if(field != null){
                    String fieldLabel = field.getDescribe().getLabel();
                    //System.debug(LoggingLevel.info,'fieldLabel = '+fieldLabel );
                    for(ApexPages.Message msg : messageArray ){
                        String msgFieldlabel = msg.getComponentLabel();
                        if(msgFieldlabel != null && msgFieldlabel == fieldLabel  ){
                            found = true;
                        }
                    }
                }
                
                
                
            }
            if(!found){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,''+errorMessage );
                ApexPages.addMessage(myMsg);
            }
            return null;
        }
    }
    
    public pagereference saveAndActivate(){
        //System.debug('saveAndActivate');
        try{
            contractVehicle.Contract_Status_Backend__c = 'Final';
            if(cloneFlag != null && cloneFlag == '1'){
                contractVehicle.id = null;
            }
            upsert contractVehicle;
            //System.debug('Successfully Updated');
            isChangeRecordType = false;
            mode = 'view';
            return new pagereference('/'+contractVehicle.Id);
        }
        catch(Exception ex){
            //System.debug('Exception = '+ex);
            mode = 'edit';
            String errorMessage = ''+ex;
            errorMessage  = errorMessage.substringAfter('first error:');
            ////System.debug(LoggingLevel.info,'errorMessage  = '+errorMessage  );
            Boolean found = false;
            if(ApexPages.hasMessages()){
                // //System.debug(LoggingLevel.info,'errorMessage  = '+errorMessage  );
                ApexPages.Message[] messageArray = ApexPages.getMessages();
                SObjectType accountType = Schema.getGlobalDescribe().get('TM_TOMA__Contract_Vehicle__c');
                Map<String,Schema.SObjectField> mfields = accountType.getDescribe().fields.getMap();
                
                ////System.debug(LoggingLevel.info,'mfields = '+mfields );
                String fieldApiName = errorMessage.subStringBetween(': [',']');
                ////System.debug(LoggingLevel.info,'fieldApiName = '+fieldApiName );
                Schema.SObjectField field = mfields.get(fieldApiName );
                ////System.debug(LoggingLevel.info,'field = '+field );
                if(field != null){
                    String fieldLabel = field.getDescribe().getLabel();
                    //System.debug(LoggingLevel.info,'fieldLabel = '+fieldLabel );
                    for(ApexPages.Message msg : messageArray ){
                        String msgFieldlabel = msg.getComponentLabel();
                        if(msgFieldlabel != null && msgFieldlabel == fieldLabel  ){
                            found = true;
                        }
                    }
                }
                
                
                
            }
            if(!found){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,''+errorMessage );
                ApexPages.addMessage(myMsg);
            }
            return null;
        }
    }
    
    public void changeOwner(){
        system.debug('Record updated = '+ contractVehicle.ownerId);
        update contractVehicle;
        system.debug('Record updated');
    }
    
    public PageReference cancelEdit(){
        //mode = 'view';
        
        mode = 'view';
        
        PageReference pageRef = new pagereference('/'+contractVehicle.Id);
        pageRef.setRedirect(true);
        //contractVehicle = (Contract_Vehicle__c)controller.getRecord();
        return pageRef;
        
    }
    
    public PageReference chandeMode(){
        mode = 'edit';
        return null;
    }
    public PageReference cancelCustomOnClone(){
        System.debug('recordId  recordId '+recordId+ 'contractVehicle.Id '+contractVehicle.Id );
        PageReference pageRef = new pagereference('/'+contractVehicle.Id);
        pageRef.setRedirect(true);
        return pageRef;
    }
    public class TabWrapper{
        public CLM_Tab__c tab {get; set;}
        public List<SectionWrapper> sectionWrapperList{get; set;}
        public Integer sequence {get; set;}
        public Map<Integer,SectionWrapper> sectionWrapperMap {get; set;} 
        
        public TabWrapper(CLM_Tab__c tabCustomSetting,List<CLM_Section__c> internalSectionList,Map<Integer,List<CLM_FIeld__c>> fieldInternalMap){
            this.tab = tabCustomSetting;
            this.sectionWrapperList = new List<SectionWrapper>();
            for(CLM_Section__c sectionCustomSetting : internalSectionList){
                if(fieldInternalMap == null){
                    fieldInternalMap = new Map<Integer,List<CLM_FIeld__c>>();
                }
                List<CLM_FIeld__c> internalFieldList = fieldInternalMap.get((Integer)sectionCustomSetting.Order__c);
                //if(internalFieldList != null){
                SectionWrapper sectionWrap = new SectionWrapper(sectionCustomSetting,internalFieldList);
                // if the section is a VF page or component or there are filed in section then only we will add it in tab otherwise we will not add it.
                // We don't want to show empty sections in tabs
                if((sectionWrap.fieldWrapperList != null && !sectionWrap.fieldWrapperList.isEmpty())||(sectionWrap.section.Is_InlineVF_Page__c || sectionWrap.section.Is_Lightning_Component__c)){
                    sectionWrapperList.add(sectionWrap);
                }
                //}
            }
        }
    }
    
    public class SectionWrapper{
        public CLM_Section__c section {get; set;}
        public String sectionId{get; set;}
        public Integer fieldColumn{get; set;}
        public List<FieldWrapper> fieldWrapperList{get; set;}
        public Integer sequence {get; set;}
        public Map<Integer,FieldWrapper> fieldWrapperMap {get; set;} 
        
        public SectionWrapper(CLM_Section__c sectionCustomSetting,List<CLM_FIeld__c> internalFieldList ){
            this.section = sectionCustomSetting;
            this.sequence = (Integer)sectionCustomSetting.Order__c;
            String pageOrComponentCompleteName = section.Inline_Api_Name_Component_Api__c;
            if(pageOrComponentCompleteName != null){
                String pageOrComponentName = pageOrComponentCompleteName.substringAfterLast('/');
                if(pageOrComponentName == null || pageOrComponentName == ''){
                    pageOrComponentName = pageOrComponentCompleteName.substringAfterLast(':');
                }
                if(pageOrComponentName == null || pageOrComponentName == ''){
                    pageOrComponentName = 'test'+section.Order__c;
                }
                sectionId = pageOrComponentName ;
            }
            fieldWrapperList = new List<FieldWrapper>();
            fieldWrapperMap = new Map<Integer,FieldWrapper>();
            if(internalFieldList != null){
                for(CLM_FIeld__c fieldCustomSetting : internalFieldList){
                    FieldWrapper fieldWrap = new FieldWrapper(fieldCustomSetting);
                    //System.debug(LoggingLevel.Info,'Field API '+dependantFieldMap);
                    fieldWrapperList.add(fieldWrap);        
                    fieldWrapperMap.put(sequence,fieldWrap);
                }
            }
            if(sectionCustomSetting.No_Of_Column__c != null && sectionCustomSetting.No_Of_Column__c != 0){
                fieldColumn = (Integer)( 12/sectionCustomSetting.No_Of_Column__c);
                
            }
            else{
                fieldColumn  = 6;
            }
        }
    }
    
    public class FieldWrapper{
        public CLM_FIeld__c field {get; set;}
        public Integer sequence {get; set;}
        public String fieldType{get;set;}
        public Boolean isRequired{get;set;}
        public Boolean isDraftRequired{get;set;}
        public Boolean isEditable{get;set;}
        public String lookUpName{get; set;}
        public String errorMessage{get;set;}
        public String className{get;set;}
        public Map<String,FieldDependancyHelper.DependantField> dependantFieldMap{get;set;}
        public String dependantFieldString{get;set;}
        public Boolean isParent{get; set;}
        public String refLabel{get;set;}
        
         //newlly added 19-04
        public String  relatedFieldName{get;set;}
        
        public FieldWrapper(CLM_FIeld__c fieldCustomSetting){
            this.field = fieldCustomSetting;
            this.sequence = (Integer)fieldCustomSetting.Order__c;
            this.isRequired = field.Required__c;
            this.isDraftRequired = field.Draft_Required__c;
            
            
              if(field.Create_Blank_Space__c != true){//newlly added if(field1.Create_Blank_Space__c != true) 15-05-2018
            Schema.SObjectField field1 = conVehicleFieldTypeMap.get(field.FieldApiName__c.trim());
            if(field1 != null){
                Schema.DisplayType fldType = field1.getDescribe().getType();
                this.fieldType = ''+fldType;
                if(fieldType=='REFERENCE')
                    this.lookUpName=''+field1.getDescribe().getReferenceTo().get(0);
                //newll added 19-04
                if(fieldsMapWithRefranceFieldName.containsKey(''+field.FieldApiName__c.trim()))
                {
                    this.relatedFieldName = ''+fieldsMapWithRefranceFieldName.get(''+field.FieldApiName__c.trim());
                }
            }  
            
            
            ////System.debug('field.FieldApiName__c 11## = '+fieldsMapAccessible.get('Total_Contract_Value_with_all_Options__c').getDescribe().isUpdateable());
            Schema.SObjectField sobjField = fieldsMapAccessible.get(field.FieldApiName__c);
            if(! fieldsMapAccessible.isEmpty() && sobjField != null){
                this.isEditable= sobjField.getDescribe().isUpdateable();
                
                if(!field.Draft_Required__c && this.isEditable &&  fieldType != 'BOOLEAN'){
                    this.isDraftRequired = !(TM_ContractDetailPageController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().isNillable());  
                }
            }else{
                this.isEditable =true;
            }
            
            if(isRequired || isDraftRequired){
                if(fieldCustomSetting.Required_Error_Message__c != null && fieldCustomSetting.Required_Error_Message__c != ''){
                    errorMessage = fieldCustomSetting.Required_Error_Message__c;
                    
                }     
                else{
                    errorMessage = 'You must enter a value';
                }
            }
            
            //System.debug('In field setting constructor'+FieldDependancyHelper.parentMap);
            //System.debug('In childmap field setting constructor'+FieldDependancyHelper.childmap);
            Map<String,FieldDependancyHelper.DependantField> dependantFieldMap = FieldDependancyHelper.parentMap.get(field.FieldApiName__c.toLowerCase());
            
            if(dependantFieldMap == null){
                isParent = false;
                dependantFieldMap = new Map<String,FieldDependancyHelper.DependantField>();
            }
            else{
                isParent = true;
            }
            this.dependantFieldMap = dependantFieldMap;
            
            this.dependantFieldString = '';
            for(String strKey : dependantFieldMap.keyset()){
                this.dependantFieldString += this.dependantFieldMap.get(strKey).value+'SPLITVAL'+this.dependantFieldMap.get(strKey).className+'SPLITVAL'+this.dependantFieldMap.get(strKey).apiName+'SPLITREC';
            }
            
            String fieldClassesName = FieldDependancyHelper.childmap.get(field.FieldApiName__c);
            //System.debug('Field Api Name = ' + field.FieldApiName__c +'     fieldClassesName = '+fieldClassesName);
            if(fieldClassesName == null){
                fieldClassesName = '';
            }
            this.className = fieldClassesName;
            
            if(field.ViewFieldApi__c == null || field.ViewFieldApi__c == ''){
                field.ViewFieldApi__c = field.FieldApiName__c;
            }
        }  
    }
    }
}