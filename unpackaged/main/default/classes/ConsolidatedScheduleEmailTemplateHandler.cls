global with sharing class ConsolidatedScheduleEmailTemplateHandler implements Messaging.InboundEmailHandler{
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        if(Schema.sObjectType.Task_Order__c.isAccessible() && Schema.sObjectType.Contract_Vehicle__c.isAccessible() && Schema.sObjectType.Contact.isAccessible()){
        List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:email.toAddresses.get(0)];
        if(!conVehicalList.isEmpty()){
            ConsolidatedScheduleEmailTemplateParser parser = new ConsolidatedScheduleEmailTemplateParser();
            parser.parse(email);//Email Parsing will be handle by parse method.
            
            //Task_Order__c taskOrder = getExistTaskOrder(email.subject.stripHtmlTags());//Checked for existing TaskOrder if alredy created before
            if(parser.getRFQId()!=null){
                Task_Order__c taskOrder = getExistTaskOrder(parser.getRFQId(),conVehicalList[0].id);
                if(taskOrder != null){//If email template exists (new -> According to RFQId)           
                    try{
                        Task_Order__c oldTaskOrder = taskOrder.clone(false,true,false,false);                         
                        Task_Order__c updatedTakOrder = parser.updateTaskOrder(taskOrder, email.toAddresses.get(0));
                        //insertContractMods(updatedTakOrder , oldTaskOrder);
                        //DMLManager.updateAsUser(updatedTakOrder);
                        EmailHandlerHelper.insertContractMods(updatedTakOrder,oldTaskOrder,email);
                        update updatedTakOrder;
                        EmailHandlerHelper.insertBinaryAttachment(updatedTakOrder,email);
                        EmailHandlerHelper.insertTextAttachment(updatedTakOrder,email);
                        EmailHandlerHelper.insertActivityHistory(updatedTakOrder,email);                
                        system.debug('Already Exists');
                    }catch(Exception e){
                        System.debug('error message = '+e.getMessage()+'\tline number = '+e.getLineNumber());
                    }            
                    
                }else{// If TaskOrder not exists          
                    taskOrder = parser.getTaskOrder(email.toAddresses.get(0));//This will be the new Task Order, We are just checking that the TO object is not null in next line
                    if(taskOrder != null){  
                        try{
                            DMLManager.insertAsUser(taskOrder);
                            EmailHandlerHelper.insertBinaryAttachment(taskOrder,email);
                            EmailHandlerHelper.insertTextAttachment(taskOrder,email);
                            EmailHandlerHelper.insertActivityHistory(taskOrder,email);
                        }catch(Exception e){
                            System.debug('error message = '+e.getMessage());
                        }
                    }
                }        
            }else{
                throw new ContractVehicleNotFoundException(MessageConstantController.TASK_ORDER_NOT_FOUND);
            }
        }else{
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
        }
        }
        return result;
    }
    
    private Task_Order__c getExistTaskOrder(String emailSubject,String contractId){
        List<Task_Order__c> taskOrderList = [SELECT Id,Name,Email_Reference__c, Task_Order_Title__c, Due_Date__c, Release_Date__c,Contracting_Officer__c,Contract_Specialist__c,ASB_Action__c,Description__c,Start_Date__c,Stop_Date__c,Contract_Negotiator__c,Proposal_Due_DateTime__c FROM Task_Order__c where Name =: emailSubject AND Contract_Vehicle__c =: contractId];
        System.debug('taskOrderList'+taskOrderList);
        if(!taskOrderList.isEmpty()){
            return taskOrderList[0];
        }
        return null;
    }  
    
    
}