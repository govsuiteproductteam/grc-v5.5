@IsTest
public class Test_SubContractDynamicFieldsController {
	 Public static testMethod void testSubContractDynamicFieldsController(){
        Test.startTest();
        List<RecordType> rtypes = [Select Name, Id From RecordType where sObjectType='TM_TOMA__SubContract__c' and isActive=true];
        string recId='';
        if(rtypes.size()>0)
        {
            recId=rtypes[0].Id;
        }
        
        
        CLM_Tab__c tabSetting =TestDataGenerator.createCLMTab(recId);
        tabSetting.TabName__c='Internal Organisation';
        tabSetting.Is_Contract__c = false;
        insert tabSetting;  
        CLM_Tab__c tabSetting1 = TestDataGenerator.createCLMTab(recId);
        tabSetting1.Is_Contract__c = false;
        tabSetting1.Order__c = 2;
        tabSetting1.TabName__c='Contract Description';
        insert tabSetting1;   
        CLM_Section__c sectionSetting = TestDataGenerator.createCLMSection(recId);
        sectionSetting.Is_Contract__c = false;
        insert sectionSetting;
        CLM_Section__c sectionSetting1 = TestDataGenerator.createCLMSection(recId);
        sectionSetting1.Is_Contract__c = false;
        sectionSetting1.No_Of_Column__c = null;
        sectionSetting1.Order__c = 20;
        sectionSetting1.Is_Lightning_Component__c= true;
        sectionSetting1.Inline_Api_Name_Component_Api__c='TestComponent';
        insert sectionSetting1;
        CLM_Section__c sectionSetting2 = TestDataGenerator.createCLMSection(recId);
        sectionSetting2.Is_Contract__c = false;
        sectionSetting2.Order__c = 30;
        sectionSetting2.Is_InlineVF_Page__c= true;
        sectionSetting2.Inline_Api_Name_Component_Api__c='TestVFPage';
        insert sectionSetting2;
        CLM_FIeld__c fieldSetting = TestDataGenerator.createCLMField(recId);
        fieldSetting.Is_Contract__c = false;
        fieldSetting.ViewFieldApi__c='LastModifiedById';
        fieldSetting.Order__c = 1001;
        insert fieldSetting;
        CLM_FIeld__c fieldSetting1 = TestDataGenerator.createCLMField(recId);
        fieldSetting1.Is_Contract__c = false;
        fieldSetting1.FieldApiName__c = 'OwnerId';
        fieldSetting1.ViewFieldApi__c='OwnerId';
        fieldSetting1.Order__c = 1002;
        insert fieldSetting1;
        CLM_FIeld__c fieldSetting2 = TestDataGenerator.createCLMField(recId);
        fieldSetting2.Is_Contract__c = false;
        fieldSetting2.FieldApiName__c = 'RecordTypeId';
        fieldSetting2.ViewFieldApi__c='RecordTypeId';
        fieldSetting2.Order__c = 1003;
        insert fieldSetting2;
                
        SubContract__c conVeh = TestDataGenerator.createSubContract('12435');
        conVeh.recordTypeId = recId;
        insert conVeh;
        String subConid = [Select id From SubContract__c Where Id=:conVeh.Id].Id;
         SubContractDynamicFieldsController ctrl = new SubContractDynamicFieldsController();
         List<String> fields = SubContractDynamicFieldsController.getAllFieldsApi('TM_TOMA__SubContract__c',subConid);
         SubContractDynamicFieldsController.getparentMap(recId);
         SubContractDynamicFieldsController.isLightningPage();
         SubContractDynamicFieldsController.getUserAccesibility();
         SubContractDynamicFieldsController.getDynamicCompoNames(recId);
         SubContractDynamicFieldsController.saveModifications(conVeh);
         SubContractDynamicFieldsController.getMyContract(subConid);
         SubContractDynamicFieldsController.updateRecTypeId(recId,subConid);
         SubContractDynamicFieldsController.createContract(subConid);
         SubContractDynamicFieldsController.updateOwnerId(subConid,UserInfo.getUserId());
         SubContractDynamicFieldsController.FieldWrapMap fldWrap =new SubContractDynamicFieldsController.FieldWrapMap();
         fldWrap.row = 1;
         fldWrap.fieldWrapList = null;
        
        Test.stopTest();
    }
}