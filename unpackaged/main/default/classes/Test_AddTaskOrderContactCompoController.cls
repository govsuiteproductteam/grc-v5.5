@IsTest
public class Test_AddTaskOrderContactCompoController {
    static testMethod void testAddTaskOrderContactCompoCont(){
        List<RecordType> rtypes = [Select Name, Id From RecordType where sObjectType='TM_GovSuite__FedCap_Opportunity__c' and isActive=true];
        string recId='';
        if(rtypes != null && ! rtypes.isEmpty())
        {
            recId=rtypes[0].Id;
        }
        System.debug(''+rtypes);
        Account acc = TestDataGenerator.createAccount('testAccount');
        insert acc;
        account acc1 = TestDataGenerator.createAccount('TestAcc1');
        insert acc1;
        Contact con = TestDataGenerator.createContact('Smith', acc);
        insert con;
        Contact con1 = TestDataGenerator.createContact('Smith1', acc);
        insert con1;
        Contact con2 = TestDataGenerator.createContact('Meyer', acc);
        insert con2;
        Contract_Vehicle__c contract = TestDataGenerator.createContractVehicle('999',acc);
        insert contract;
        
        Task_Order__c fedOpp = TestDataGenerator.createTaskOrder('TMtaskOrder', acc, contract);
        insert fedOpp;
        
        /*FedCap_Opportunity__c fedOpp = TestDataGenerator.createFedOpp('TestFedOpp', acc, recId, 'Closed Won');
        //fedOpp.Probability__c=10;
        fedOpp.Budget_Approved__c=true;
        fedOpp.Type__c= 'New Business';
        fedOpp.Closed_Reason__c='Test';
        fedOpp.Marketing_Budget__c=8787;
        fedOpp.Contract_Vehicle_owned__c =contract.Id; 
        insert fedOpp;*/
        Vehicle_Partner__c vehPar = TestDataGenerator.createVehiclePartner(acc, contract);
        insert vehPar;
        Teaming_Partner__c temPart = TestDataGenerator.createTeamingPartner(vehPar, fedOpp);
        insert temPart;
        
        
        Task_Order_Contact__c taskOrderCon = TestDataGenerator.createTaskOrderContact(fedOpp, con);
        insert taskOrderCon;
        AddTaskOrderContactCompoController.checkLicensePermition();
        List<Task_Order_Contact__c> taskOrderList = AddTaskOrderContactCompoController.getContractVehicalContact(fedOpp.Id);
        System.assertEquals(1, taskOrderList.size());
        string str = '['+taskOrderList[0].Id+'SPLITDATA'+true+'SPLITDATA'+false+'SPLITPARENT';
        AddTaskOrderContactCompoController.saveTaskOrderContact(str);
        taskOrderList = AddTaskOrderContactCompoController.getContractVehicalContact(fedOpp.Id);
        System.assertEquals(true, taskOrderList[0].Main_POC__c);
        AddTaskOrderContactCompoController.addTaskOrderContact(fedOpp.Id);
        AddTaskOrderContactCompoController.deleteTaskOrderContact(taskOrderCon.id);
        AddTaskOrderContactCompoController.addSearchTaskOrderContact(fedOpp.Id,'Smith1');
        taskOrderList = AddTaskOrderContactCompoController.getContractVehicalContact(fedOpp.Id);
        System.assertEquals(0, taskOrderList.size());
        str = '['+con1.Id+'SPLITDATA'+fedOpp.Id+'SPLITDATA'+false+'SPLITDATA'+true+'SPLITPARENT';
        AddTaskOrderContactCompoController.addNewTaskOrderContact(str);
        taskOrderList = AddTaskOrderContactCompoController.getContractVehicalContact(fedOpp.Id);
        System.assertEquals(1, taskOrderList.size());
        
    }
    
}