({
    doInit : function (component, event) {
        var recordTypeId=component.get("v.pageReference").state.recordTypeId;
        
        if(recordTypeId===null || recordTypeId===undefined || recordTypeId==="undefined" || recordTypeId===""){
            
            
            var action = component.get("c.getDefaultReocrdTypeId");
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    
                    component.set("v.pageUrl","/apex/TM_TOMA__TM_ContractNewPage?RecordType="+response.getReturnValue());
                }
                
            });
            
            // optionally set storable, abortable, background flag here
            
            // A client-side action could cause multiple events, 
            // which could trigger other events and 
            // other server-side action calls.
            // $A.enqueueAction adds the server-side action to the queue.
            $A.enqueueAction(action);
            
        }else{
            
            component.set("v.pageUrl","/apex/TM_TOMA__TM_ContractNewPage?RecordType="+component.get("v.pageReference").state.recordTypeId);
        }
        /* console.log("inside init"+"/TM_TOMA__TM_ContractNewPage?RecordType="+component.get("v.pageReference").state.recordTypeId);
        window.open("/apex/TM_TOMA__TM_ContractNewPage?RecordType="+component.get("v.pageReference").state.recordTypeId);
        */
        /*    var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
              "url": "/apex/TM_TOMA__TM_ContractNewPage?RecordType="+component.get("v.pageReference").state.recordTypeId,
              "isredirect": "true"
            });
            urlEvent.fire();*/
        
    }
})