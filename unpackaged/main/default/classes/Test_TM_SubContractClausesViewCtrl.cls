@IsTest
public class Test_TM_SubContractClausesViewCtrl {
public static testMethod void testSubContractClausesViewCtrl(){
        Account account = TestDataGenerator.createAccount('Test Account');
        insert account;
        Subcontract__c  subContractVehicle =  TestDataGenerator.createSubContract('12345');
        //subContractVehicle.Name = 'Test';
        insert subContractVehicle;
        Clauses__c clause = TestDataGenerator.getClause('AFFARS 5352.215-9009 Travel (AFMC)');
        insert clause;
        Clauses__c clause1 = TestDataGenerator.getClause('AFARS 5152.204-4007');
        clause1.Clause_Type__c='Agency';
        insert clause1;
        Clauses__c clause2 = TestDataGenerator.getClause('ACEARS 52.212-5000 Evaluation of subdivided items');
        clause2.Clause_Type__c='Center';
        insert clause2;
        Clauses__c clause3 = TestDataGenerator.getClause('ACEARS 52.212-5000 Evaluation');
        clause3.Clause_Type__c='Department';
        insert clause3;
        Clauses__c clause4 = TestDataGenerator.getClause('AFFARS 5352.215-9009');
        clause4.Clause_Type__c='FAR';
        insert clause4;
    
        Contract_Clauses__c conClaus = TestDataGenerator.getSubContractClause(subContractVehicle.id,clause.id);
        insert conClaus;
        Contract_Clauses__c conClaus1 = TestDataGenerator.getSubContractClause(subContractVehicle.id,clause1.id);
        insert conClaus1;
        Contract_Clauses__c conClaus2 = TestDataGenerator.getSubContractClause(subContractVehicle.id,clause2.id);
        insert conClaus2;
        Contract_Clauses__c conClaus3 = TestDataGenerator.getSubContractClause(subContractVehicle.id,clause3.id);
        insert conClaus3;
        ApexPages.StandardController sc = new ApexPages.StandardController(subContractVehicle);
        TM_SubContractClausesViewCtrl con = new TM_SubContractClausesViewCtrl(sc);
        System.assertEquals(1, con.agencyClauseDispList.size());
        System.assertEquals(1, con.centerClauseDispList.size());
        System.assertEquals(1, con.departmentClauseDispList.size());
        System.assertEquals(subContractVehicle.Id, con.subConVehical.Id);
        con.cancelClauses();
        String claString = con.getClauseDataJSON();
        System.assert(claString != '');
       
        con.getAgencyClauseOption();
        con.getCenterClauseOption();
        con.getDepartmentClauseSOption();
        //con.farSelected=con.farSelected+','+clause4.Id;
        con.saveClauses();
        con.getFarClauseOption();
        con.agencyClauseOptionLoad();
        System.assert(con.agencySelected == '');
        con.centerClauseOptionLoad();
        System.assert(con.centerSelected == '');
        con.departmentClauseOptionLoad();
        System.assert(con.departmentSelected == '');
    }
}