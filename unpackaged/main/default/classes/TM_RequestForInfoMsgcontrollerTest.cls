@isTest
public with sharing class TM_RequestForInfoMsgcontrollerTest{
    public static TestMethod void TestTM_RequestForInfoMsgcontroller(){
        List<Profile> profileList = [SELECT Id FROM Profile where (Name = 'Custom Partner Community User' OR Name = 'Custom Partner Community Login User' OR Name  = 'Custom Customer Community Plus Login User') Limit 1];
        if(profileList != null && profileList.size()>0){
            Account acc = TestDataGenerator.createAccount('TaskOrderCapture');
            insert acc;
            
            Contact con = TestDataGenerator.createContact('Avanti',acc);
            insert con;
            
            User usr = TestDataGenerator.createUser(con,TRUE,'Msgcontest1', 'atest1','Msgcontest1@gmail.com','Msgcontaskordercapture@demo.com','atest1',profileList[0],'America/Los_Angeles','en_US','UTF-8','en_US');
            insert usr;    
            
            Contract_Vehicle__c contractVehicle = TestDataGenerator.createContractVehicle('TMconVehicle',acc);
            insert contractVehicle;
            
            Task_Order__c taskOrder = TestDataGenerator.createTaskOrder('TMtaskOrder', acc, contractVehicle);
            insert taskOrder;
           
            
            Survey__c sur = TestDataGenerator.createSurvey();
            insert sur;
            
            
            Survey_Task__c surveyTask = TestDataGenerator.createSurveyTask(sur ,taskOrder);
            insert surveyTask ;
             
            
            Test.startTest();
            ApexPages.CurrentPage().getparameters().put('sid',sur.id);
            ApexPages.StandardController controller = new ApexPages.StandardController(sur);
            TM_RequestForInformationMsgcontroller requestInfoMsgController = new TM_RequestForInformationMsgcontroller(controller);
         
            PageReference pgRef = requestInfoMsgController.continueNew();
            System.assertNotEquals(pgRef, null);
            System.assertNotEquals(requestInfoMsgController.surveyId , null);
            Test.stopTest();
        }
    }
}