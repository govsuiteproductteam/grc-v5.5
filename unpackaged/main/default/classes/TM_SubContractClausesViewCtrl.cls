public class TM_SubContractClausesViewCtrl {
    public List<Contract_Clauses__c > farClauseDispList{get;set;}
    public List<Contract_Clauses__c > agencyClauseDispList{get;set;}
    public List<Contract_Clauses__c > centerClauseDispList{get;set;}
    public List<Contract_Clauses__c > departmentClauseDispList{get;set;}
    public transient Boolean isEdit{get;set;}
    private Id subConVehicalId;
    
    ///// Assign Veriables
    public String areThereAnyFARClauses{get;set;}
    public String areThereAnyAgencyClauses{get;set;}
    public String areThereAnyCenterClauses{get;set;}
    public String areThereAnyDepartmentClauses{get;set;}
    /////
    Public SubContract__c subConVehical{get;set;}
    //Selector
    public String agencySelectOption{get;set;}
    public String centerSelectOption{get;set;}
    public String departmentSelectSOption{get;set;}
    
    //Actual Ids
    public String farSelected{get;set;}
    public String agencySelected{get;set;}
    public String centerSelected{get;set;}
    public String departmentSelected{get;set;}
    
    //action function variables
    public String agency{get;set;}
    public String center{get;set;}
    public String department{get;set;}
    
    //Action function vari to hold values
    public String agencyValueHold{get;set;}
    public String centerValueHold{get;set;}
    public String departmentValueHold{get;set;}
    
    //Contract is contains Map
    private map<id,id> farClausetrackMap;
    private map<id,id> agencyClausetrackMap;
    private map<id,id> centerClausetrackMap;
    private map<id,id> departmentClausetrackMap;
    
    
    //Private Variables
    private String agencyValue;
    private String centerValue;
    private String departmentValue;
    private String subConVehicalIdParm;
    ApexPages.StandardController sc;
    ////
     public String isValidLiecence{get;set;}
    public TM_SubContractClausesViewCtrl (ApexPages.StandardController sc){
       // sc.addFields(new List<String>{'TM_TOMA__Are_there_any_FAR_clauses__c'});
           isValidLiecence =LincenseKeyHelper.checkLicensePermitionForFedCLM();
        if(isValidLiecence == 'Yes'){
           this.sc =sc;
        subConVehical = (TM_TOMA__SubContract__c)sc.getRecord();
        init();
        }
    }
    
    public void init(){
        subConVehical = [Select Id,Are_there_any_FAR_clauses__c,Are_there_any_Agency_Supplement_clauses__c,Are_there_any_Center_Clauses__c,Are_there_any_Department_clauses__c From SubContract__c where id =: subConVehical.id];
        areThereAnyFARClauses = subConVehical.Are_there_any_FAR_clauses__c;
        areThereAnyAgencyClauses = subConVehical.Are_there_any_Agency_Supplement_clauses__c;
        areThereAnyCenterClauses = subConVehical.Are_there_any_Center_Clauses__c;
        areThereAnyDepartmentClauses = subConVehical.Are_there_any_Department_clauses__c;
        
        farClauseDispList = new List<Contract_Clauses__c >();
        agencyClauseDispList = new List<Contract_Clauses__c >();
        centerClauseDispList = new List<Contract_Clauses__c >();
        departmentClauseDispList = new List<Contract_Clauses__c >();
        isEdit = false;
        if(subConVehical.Id!=Null){
            subConVehicalId = subConVehical.Id;
            for(Contract_Clauses__c clause : [SELECT id,Clause_Agency__c,Clause_Center__c,Clause_Department__c,Clauses__c, Clauses__r.Clause_Description__c,SubContract__c,Clauses__r.Clause_Type__c,Clauses__r.Sort_Number__c FROM Contract_Clauses__c WHERE SubContract__c=:subConVehical.Id ORDER BY Clauses__r.Sort_Number__c,Clauses__r.Name]){
                if(clause.Clauses__r.Clause_Type__c != NULL && clause.Clauses__r.Clause_Type__c == 'FAR'){
                    farClauseDispList.add(clause);
                }else if(clause.Clauses__r.Clause_Type__c != NULL && clause.Clauses__r.Clause_Type__c == 'Agency'){
                    agencyClauseDispList.add(clause);
                }else if(clause.Clauses__r.Clause_Type__c != NULL && clause.Clauses__r.Clause_Type__c == 'Center'){
                    centerClauseDispList .add(clause);
                }else if(clause.Clauses__r.Clause_Type__c != NULL && clause.Clauses__r.Clause_Type__c == 'Department'){
                    departmentClauseDispList .add(clause);
                }
            }
            setBlanks();            
            //subConVehical = (Contract_Vehicle__c)sc.getRecord();
            subConVehicalIdParm = (string)subConVehical.get('Id');
            getAllClauseData();            
            setDependentOptions();
        }
            
    }
    
    public void saveClauses(){
       
        List<Contract_Clauses__c > crantractsToInsert = new List<Contract_Clauses__c >();
        
        List<Contract_Clauses__c > checkValList = [SELECT id, Clauses__c, SubContract__c FROM Contract_Clauses__c WHERE SubContract__c=:subConVehical.id];
        map<String, Boolean> booleanMAp = new map<String, Boolean>();
        for(Contract_Clauses__c  contractTemp : checkValList){
            booleanMAp.put((''+contractTemp.SubContract__c).substring(0,15)+'~'+(''+contractTemp.Clauses__c).substring(0,15),true);
        }
        
        if(farSelected != Null && farSelected != ''){
            for(String id : farSelected.split(',')){
                if(id!='' && !farClausetrackMap.containsKey(id)){
                    if(!booleanMAp.containskey((''+subConVehical.id).substring(0,15)+'~'+id.substring(0,15))){
                        crantractsToInsert.add(new Contract_Clauses__c (Clauses__c = (id)id,SubContract__c=subConVehical.id));
                    }
                }else if(farClausetrackMap.containsKey(id)){
                    farClausetrackMap.remove(id);
                }
            }
        }
        
        if(agencySelected!=Null && agencySelected!=''){
            for(String id : agencySelected.split(',')){
                if(id!='' && !agencyClausetrackMap.containsKey(id)){
                    if(!booleanMAp.containskey((''+subConVehical.id).substring(0,15)+'~'+id.substring(0,15))){
                        
                        crantractsToInsert.add(new Contract_Clauses__c (Clauses__c = (id)id,SubContract__c=subConVehical.id));
                    }
                }else if(agencyClausetrackMap.containsKey(id)){
                    agencyClausetrackMap.remove(id);
                }
            }
        }
        if(centerSelected!=Null && centerSelected!=''){
            for(String id : centerSelected.split(',')){
                if(id!='' && !centerClausetrackMap.containsKey(id)){
                    if(!booleanMAp.containskey((''+subConVehical.id).substring(0,15)+'~'+id.substring(0,15))){
                        crantractsToInsert.add(new Contract_Clauses__c (Clauses__c = (id)id,SubContract__c=subConVehical.id));
                    }
                }else if(centerClausetrackMap.containsKey(id)){
                    centerClausetrackMap.remove(id);
                }
            }
        }
        if(departmentSelected!=Null && departmentSelected!=''){
            for(String id : departmentSelected.split(',')){
                if(id!='' && !departmentClausetrackMap.containsKey(id)){
                    if(!booleanMAp.containskey((''+subConVehical.id).substring(0,15)+'~'+id.substring(0,15))){
                        crantractsToInsert.add(new Contract_Clauses__c (Clauses__c = (id)id,SubContract__c=subConVehical.id));
                    }
                }else if(departmentClausetrackMap.containsKey(id)){
                    departmentClausetrackMap.remove(id);
                }
            }
        }
        try{
            System.debug('In Try');
            
            set<id> deleteContractClauseId = new set<id>();
            deleteContractClauseId.addAll(farClausetrackMap.values());
            deleteContractClauseId.addAll(agencyClausetrackMap.values());
            deleteContractClauseId.addAll(centerClausetrackMap.values());
            deleteContractClauseId.addAll(departmentClausetrackMap.values());
            if(!crantractsToInsert.isEmpty()){
                upsert crantractsToInsert;
            }
            if(!deleteContractClauseId.isEmpty()){
                Database.delete(new List<Id>(deleteContractClauseId), false);
            }
            subConVehical = (TM_TOMA__SubContract__c)sc.getRecord();
            update subConVehical;
        
        }catch(Exception ex){
            System.debug('exception '+ex.getMessage());
        }finally{
            init();
        }
    }
    
    public pagereference cancelClauses(){
        //init();
        pagereference p = new pagereference('/apex/TM_SubContractClausesViewPage?id='+subConVehical.id);
        p.setRedirect(true);
        return p;
    }
    
    public void getAllClauseData(){
        List<Contract_Clauses__c > contractClauseList = [SELECT id,Clause_Agency__c,Clause_Center__c,Clause_Department__c,Clauses__c,SubContract__c,Clauses__r.Clause_Type__c,Clauses__r.Sort_Number__c FROM Contract_Clauses__c WHERE SubContract__c=:subConVehicalIdParm ORDER BY Clauses__r.Sort_Number__c,Clauses__r.Name];
        
        set<String> agencyClauseSelected = new set<String>();
        set<String> centerClauseSelected = new set<String>();
        set<String> departmentClauseSelected = new set<String>();
        
        
        for(Contract_Clauses__c tcc : contractClauseList){
            if(tcc.Clauses__r.Clause_Type__c != NULL && tcc.Clauses__r.Clause_Type__c == 'FAR'){
                if(!farClausetrackMap.containsKey(tcc.Clauses__c)){
                    farSelected += tcc.Clauses__c+',';
                    //if(isCloneNotDraft){
                        farClausetrackMap.put(tcc.Clauses__c,tcc.id);
                    //}
                }
            }
            if(tcc.Clause_Agency__c != Null && tcc.Clause_Agency__c != ''){
                if(!agencyClausetrackMap.containsKey(tcc.Clauses__c)){
                    if(!agencyClauseSelected.contains(tcc.Clause_Agency__c)){
                        agencyClauseSelected.add(tcc.Clause_Agency__c);
                        agencyValue +=  tcc.Clause_Agency__c+',';
                    }
                    agencySelected += tcc.Clauses__c+',';
                    //if(isCloneNotDraft)
                        agencyClausetrackMap.put(tcc.Clauses__c,tcc.id);
                }
            }
            if(tcc.Clause_Center__c != Null && tcc.Clause_Center__c != ''){
                if(!centerClausetrackMap.containsKey(tcc.Clauses__c)){
                    if(!centerClauseSelected.contains(tcc.Clause_Center__c)){
                        centerClauseSelected.add(tcc.Clause_Center__c);
                        centerValue +=  tcc.Clause_Center__c+',';
                    }
                    centerSelected += tcc.Clauses__c+',';
                    //if(isCloneNotDraft)
                        centerClausetrackMap.put(tcc.Clauses__c,tcc.id);
                }
            }
            if(tcc.Clause_Department__c != Null && tcc.Clause_Department__c != ''){
                if(!departmentClausetrackMap.containsKey(tcc.Clauses__c)){
                    if(!departmentClauseSelected.contains(tcc.Clause_Department__c)){
                        departmentClauseSelected.add(tcc.Clause_Department__c);
                        departmentValue +=  tcc.Clause_Department__c+',';
                    }
                    departmentSelected += tcc.Clauses__c+',';
                    //if(isCloneNotDraft)
                        departmentClausetrackMap.put(tcc.Clauses__c,tcc.id);
                }
            }
        } 
        agencyValueHold = agencySelected;
        centerValueHold = centerSelected;
        departmentValueHold = departmentSelected;
    }
    
    public String getClauseDataJSON(){
        Integer count = 1;
        String clauseDataJSON = '{';
        for(Clauses__c clause : [SELECT name,Clause_Description__c,Agency__c,Center__c,Clause_Type__c,Department__c,Sort_Number__c FROM Clauses__c ORDER BY Sort_Number__c,Name]){
            clauseDataJSON += ''+clause.id+': \'{"Name":"'+String.escapeSingleQuotes((clause.Clause_Description__c!=Null?clause.Clause_Description__c:'').replace('"','\\"'))+'","Index":"'+count+'","Agency":"'+clause.Agency__c+'","Center":"'+clause.Center__c+'","Depatrment":"'+clause.Department__c +'","ClauseType":"'+clause.Clause_Type__c+'"}\',';
            count++;
        }
        clauseDataJSON += '}';
        return clauseDataJSON;
    }
    
    public void setBlanks(){
        agencySelectOption = '';
        centerSelectOption = '';
        departmentSelectSOption = '';
        farSelected = '';
        agencySelected ='';
        centerSelected  = '';
        departmentSelected = '';
        agencyValue ='';
        centerValue = '';
        departmentValue ='';
        
        agencyValueHold = '';
        centerValueHold = '';
        departmentValueHold = '';
        
        farClausetrackMap = new map<id,id>();
        agencyClausetrackMap = new map<id,id>();
        centerClausetrackMap = new map<id,id>();
        departmentClausetrackMap = new map<id,id>();
        
        agency = '';
        center = '';
        department = '';
    }
    
    public void setDependentOptions(){
        agencySelectOption='';
        centerSelectOption='';
        departmentSelectSOption='';
        Set<String> tempAgencySet = new Set<String>(agencyValue.split(','));
        for(Schema.PicklistEntry pl : Clauses__c.Agency__c.getDescribe().getPicklistValues()){
            String b = 'false';
            if(tempAgencySet.contains(pl.getValue())){
                b = 'true';
            }
            agencySelectOption += '{"type":"option", "selected":'+b+', "label":"'+pl.getLabel()+'","value":"'+pl.getValue()+'"},';            
        }
        
        Set<String> tempCenterSet = new Set<String>(centerValue.split(','));
        for(Schema.PicklistEntry pl : Clauses__c.Center__c.getDescribe().getPicklistValues()){
            String b = 'false';
            if(tempCenterSet.contains(pl.getValue())){
                b = 'true';
            }
            centerSelectOption += '{"type":"option", "selected":'+b+', "label":"'+pl.getLabel()+'","value":"'+pl.getValue()+'"},';            
        }
        
        Set<String> tempDepartmentSet = new Set<String>(departmentValue.split(','));
        for(Schema.PicklistEntry pl : Clauses__c.Department__c.getDescribe().getPicklistValues()){
            String b = 'false';
            if(tempDepartmentSet.contains(pl.getValue())){
                b = 'true';
            }
            departmentSelectSOption += '{"type":"option", "selected":'+b+', "label":"'+pl.getLabel()+'","value":"'+pl.getValue()+'"},';            
        }
    }
    
    
    public String getAgencyClauseOption (){
        String agencyClauseOption = '';
        if(agency != Null  && agency != ''){
            Set<String> agecySet = new set<String>(agency.split(','));
            for(Clauses__c clause : [SELECT name,Clause_Description__c,Agency__c,Center__c,Clause_Type__c,Department__c,Sort_Number__c FROM Clauses__c WHERE Agency__c IN :agecySet ORDER BY Sort_Number__c,name]){
                agencyClauseOption += '{"type":"option", "label":"'+(clause.Clause_Description__c!=Null?clause.Clause_Description__c:'').replace('"','\\"')+'","value":"'+clause.id+'"},';
            }
        }else{
            agencyClauseOption += '';
        }
        return agencyClauseOption;
    }
    
    public String getCenterClauseOption(){
        String centerClauseOption = '';
        if(center!= Null  && center!= ''){
            Set<String> agecySet = new set<String>(center.split(','));
            for(Clauses__c clause : [SELECT name,Clause_Description__c,Agency__c,Center__c,Clause_Type__c,Department__c,Sort_Number__c FROM Clauses__c WHERE Center__c IN :agecySet ORDER BY Sort_Number__c,name]){
                centerClauseOption += '{"type":"option", "label":"'+(clause.Clause_Description__c!=Null?clause.Clause_Description__c:'').replace('"','\\"')+'","value":"'+clause.id+'"},';
            }
        }else{
            centerClauseOption += '';
        }
        return centerClauseOption ;
    }
    
    public String getDepartmentClauseSOption(){
        String departmentClauseSOption= '';
        if(department!= Null  && department!= ''){
            Set<String> agecySet = new set<String>(department.split(','));
            for(Clauses__c clause : [SELECT name,Clause_Description__c,Agency__c,Center__c,Clause_Type__c,Department__c,Sort_Number__c FROM Clauses__c WHERE Department__c IN :agecySet ORDER BY Sort_Number__c,Name]){
                departmentClauseSOption+= '{"type":"option", "label":"'+(clause.Clause_Description__c!=Null?clause.Clause_Description__c:'').replace('"','\\"')+'","value":"'+clause.id+'"},';
            }
        }else{
            departmentClauseSOption+= '';
        }
        return departmentClauseSOption;
    }
    
    public String getFarClauseOption(){
        String farClauseOption = '';
        for(Clauses__c clause : [SELECT name,Clause_Description__c,Agency__c,Center__c,Clause_Type__c,Department__c,Sort_Number__c FROM Clauses__c WHERE Clause_Type__c='FAR' ORDER BY Sort_Number__c,Name]){
            farClauseOption += '{"type":"option", "label":"'+(clause.Clause_Description__c!=Null ?clause.Clause_Description__c:'' ).replace('"','\\"')+'","value":"'+clause.id+'"},';
        }
        return farClauseOption ;
    }
    
    public void agencyClauseOptionLoad(){
        agencySelected ='';
    }
    
    public void centerClauseOptionLoad(){
        centerSelected  = '';
    }
    
    public void departmentClauseOptionLoad(){
        departmentSelected = '';
    }
    
}