@isTest
public class Test_TM_NewAtglanceSubcontractController {
    static testMethod void testMethodSubcontractVehicalAtglanceController(){
        test.startTest();
        List<RecordType> rtypes = [Select Name, Id From RecordType where sObjectType='TM_TOMA__SubContract__c' and isActive=true];
        string recId='';
        if(rtypes.size()>0)
        {
            recId=rtypes[0].Id;
        }
        
        CLM_AtaGlanceField__c newOppAtAGlance1 = TestDataGenerator.createAtaGlanceField(recId);
        newOppAtAGlance1.Is_Contract__c=false;
        newOppAtAGlance1.FieldApiName__c='TM_TOMA__Subcontractor_Name__c';
        newOppAtAGlance1.FieldLabel__c='Subcontractor Name';
        newOppAtAGlance1.Order__c=2;
        insert newOppAtAGlance1; 
        CLM_AtaGlanceField__c newOppAtAGlance3 = TestDataGenerator.createAtaGlanceField(recId);
        newOppAtAGlance3.Is_Contract__c=false;
        newOppAtAGlance3.FieldApiName__c='TM_TOMA__SubContact_Total_Contract_Value__c';
        newOppAtAGlance3.FieldLabel__c='Subcontract Value';
        newOppAtAGlance3.Order__c=3;
        insert newOppAtAGlance3; 
        
        CLM_AtaGlanceField__c newOppAtAGlance4 = TestDataGenerator.createAtaGlanceField(recId);
        newOppAtAGlance4.Is_Contract__c=false;
        newOppAtAGlance4.FieldApiName__c='CreatedDate';
        newOppAtAGlance4.FieldLabel__c='CreatedDate';
        newOppAtAGlance4.Order__c=3;
        insert newOppAtAGlance4; 
        
        Account acc = TestDataGenerator.createAccount('testAccount');
        insert acc;
        Contact con = TestDataGenerator.createContact('Test Contact', acc);   
        insert con;
        
        SubContract__c subConVeh = TestDataGenerator.createSubContract('23745');
        subConVeh.recordTypeId = recId;
        subConVeh.Subcontract_Status_Backend__c= TestDataGenerator.getPicklistValue('TM_TOMA__SubContract__c','TM_TOMA__Subcontract_Status_Backend__c');
        insert subConVeh;
        test.stopTest();
        String licenceStatus=TM_NewAtglanceSubcontractController.getUserAccesibility();
        Map<String,String> stagePickListVal=TM_NewAtglanceSubcontractController.getstagePicklistvalue();
        TM_AtglanceWrap atglanceWrap=TM_NewAtglanceSubcontractController.getFields(subconVeh.Id);
        System.assert(atglanceWrap!=null);
        
    }
}