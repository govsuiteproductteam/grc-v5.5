({
	doInitHlpr : function(component, event, helper) {
        var spnr = component.find("sendpartnerspnr");
		var conWrapList = component.get("c.getParentWraList");
        var recordId=component.get("v.recordId");
        conWrapList.setParams({
            "recordId":recordId
        });
        conWrapList.setCallback(this,function(resultData){
            var status=resultData.getState();
            if(status=="SUCCESS"){
                component.set("v.conWrapList",resultData.getReturnValue().conWrapList);
                component.set("v.surveyWraList",resultData.getReturnValue().surveyWrapList);
                component.set("v.emailWraList",resultData.getReturnValue().emailWrapList);
                component.set("v.attachWraList",resultData.getReturnValue().attachWrapList);
                
                
                component.set("v.isDataLoaded",true);
                $A.util.addClass(spnr,"slds-hide");
            }else{
                $A.util.addClass(spnr,"slds-hide");
            }
        });
        $A.enqueueAction(conWrapList);
	},
    navigateToContactHlpr: function(component, event, helper) {
         var teamId = event.currentTarget.dataset.record;
        var urlEvent = $A.get("e.force:navigateToURL");
    urlEvent.setParams({
      "url": "/"+teamId
    });
    urlEvent.fire();
    },
    sendEmailHlpr: function(component, event, helper,isCustomMail){
        var send_email =component.find("send_email");
        $A.util.addClass(send_email,"cust_disable");
        var contactWrap = component.get("v.conWrapList");
        
        if(!$A.util.isEmpty(contactWrap) && ! $A.util.isUndefined(contactWrap)){
            
            var isContactSelected=false;
            for(var i=0;i<contactWrap.length;i++){
                if(contactWrap[i].isSelected)
                    isContactSelected = true;
            }
        
            if(isContactSelected){
                var avoidSurvey = component.find("noquestionairechk").get("v.value");
                var surveyStrId='';
                var isSurveySelected = false;
            var surveyList = component.get("v.surveyWraList");
            if(!$A.util.isEmpty(surveyList) && ! $A.util.isUndefined(surveyList)){
                for(var i=0;i<surveyList.length;i++){
                    if(surveyList[i].isSelected){
                        surveyStrId = surveyList[i].survey.Id;
                     isSurveySelected =true;   
                    }
                }
            }
           if(avoidSurvey)
        surveyStrId = '';
                
                if(avoidSurvey || isSurveySelected || isCustomMail){
                
                    var emailTempId="";
                    var isEmailTempSelected=false;
        var emailList = component.get("v.emailWraList");
            if(!$A.util.isEmpty(emailList) && ! $A.util.isUndefined(emailList)){
                for(var i=0;i<emailList.length;i++){
                    if(emailList[i].isSelected){
                        emailTempId = emailList[i].emailTemp.Id;
                     isEmailTempSelected =true;   
                    }
                }
            }
                    
                    if(isEmailTempSelected || isCustomMail){      
        var taskOrderId = component.get("v.recordId");
        var tempconWraListString = component.get("v.conWrapList");
        var conWraListString = JSON.stringify(tempconWraListString);
        var tempattachmentWraStrng = component.get("v.attachWraList");
        var attachmentWraStrng = JSON.stringify(tempattachmentWraStrng);
        
        var subjectToSet = component.find("customemailsub").get("v.value");
        var emailBodyToSet = component.find("customemailbody").get("v.value");
        if($A.util.isUndefined(subjectToSet))
            subjectToSet=null;
        if($A.util.isUndefined(emailBodyToSet))
            emailBodyToSet=null;
        console.log(subjectToSet+'###'+emailBodyToSet);
        var test= component.get("c.testFunc");
        test.setParams({
            "str1":conWraListString,
            "str2":attachmentWraStrng,
            "str3":taskOrderId,
            "str4":emailTempId,
            "bool5":avoidSurvey,
            "str6":surveyStrId,
            "str7":subjectToSet,
            "str8":emailBodyToSet
            
        });
        test.setCallback(this,function(resultData){
            $A.util.removeClass(send_email,"cust_disable");
            var objId = component.get("v.recordId");
        var urlEvent = $A.get("e.force:navigateToURL");
    urlEvent.setParams({
      "url": "/"+objId
    });
    urlEvent.fire();
    
        });
        $A.enqueueAction(test);
                }//end of emailtemplate condition
                    else{
                        $A.util.removeClass(send_email,"cust_disable");
                        //alert("Please Select Email Template");
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!",
                            "message": "Please select Email Template",
                            "type":"error"
                        });
                toastEvent.fire();
                    }
              } //end of survey and no questionaire if condition.
                else{
                    $A.util.removeClass(send_email,"cust_disable");
                    //alert("Please Select Any one Survey or mark No Questionaire");
                    var mailBody = component.find("mailpopup");
                     $A.util.addClass(mailBody,'hidethis');
                    var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!",
                            "message": "Please select any one Survey or mark no Questionnaire",
                            "type":"error"
                        });
                toastEvent.fire();
                }
            }else{
                $A.util.removeClass(send_email,"cust_disable");
               // alert("Please Select atleast one Contact");
                var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!",
                            "message": "Please select at least one Contact",
                            "type":"error"
                        });
                toastEvent.fire();
            }
        }else{
            $A.util.removeClass(send_email,"cust_disable");
        }
    },
    noQuestionnaireHlpr : function(component, event, helper){
        var spinner = component.find("emailtempspinnr");
        $A.util.removeClass(spinner, "slds-hide");
        
        var isChecked = event.getSource().get("v.value");
        
        var questionirclm = document.getElementsByClassName('hideonuncheck');
        if(questionirclm != null && questionirclm.length>0){
            for(var i=0;i<questionirclm.length;i++){
                questionirclm[i].style.display = isChecked ? 'none':'';
            }
        }
        if(isChecked){
            var surveyWrappr = component.get("v.surveyWraList");
            if(surveyWrappr != null && surveyWrappr.length>0){
                for(var i=0;i<surveyWrappr.length;i++){
                    surveyWrappr[i].isSelected=false;
                }
                component.set("v.surveyWraList",surveyWrappr);
            }
            
            var refreshedEmailTempList = component.get("c.selectEmailTemplete");
        refreshedEmailTempList.setParams({
            "surveyStrId":''
        });
        refreshedEmailTempList.setCallback(this,function(resultData){
            var status= resultData.getState();
            if(status=="SUCCESS"){
                component.set("v.emailWraList",resultData.getReturnValue());
                $A.util.addClass(spinner, "slds-hide");
            }else{
                $A.util.addClass(spinner, "slds-hide");
            }
        });
        $A.enqueueAction(refreshedEmailTempList);
        }else{
                $A.util.addClass(spinner, "slds-hide");
            }
    },
    refreshEmailTempListHlpr: function(component, event, helper){
        var spinner = component.find("emailtempspinnr");
        $A.util.removeClass(spinner, "slds-hide");
        var selectedSurveyId = event.getSource().get("v.class");
        var refreshedEmailTempList = component.get("c.selectEmailTemplete");
        console.log('selectedSurveyId'+selectedSurveyId);
        refreshedEmailTempList.setParams({
            "surveyStrId":selectedSurveyId
        });
        refreshedEmailTempList.setCallback(this,function(resultData){
            var status= resultData.getState();
            if(status=="SUCCESS"){
                component.set("v.emailWraList",resultData.getReturnValue());
                $A.util.addClass(spinner, "slds-hide");
            }else{
                $A.util.addClass(spinner, "slds-hide");
            }
        });
        $A.enqueueAction(refreshedEmailTempList);
    },
    openMailPopUpHlpr: function(component, event, helper){
        var contactWrap = component.get("v.conWrapList");
        var isContactSelected=false;
            for(var i=0;i<contactWrap.length;i++){
                if(contactWrap[i].isSelected)
                    isContactSelected = true;
            }
        if(isContactSelected){
        var mailBody = component.find("mailpopup");
        $A.util.removeClass(mailBody,'hidethis');
        component.find("customemailsub").set("v.value",null);
        component.find("customemailbody").set("v.value",null);
        }else{
            var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!",
                            "message": "Please select at least one Contact",
                            "type":"error"
                        });
                toastEvent.fire();
        }
    },
    closeMailPopUpHlpr: function(component, event, helper){
        var mailBody = component.find("mailpopup");
        $A.util.addClass(mailBody,'hidethis');
        
        var subErr = component.find("sub-err");
            $A.util.addClass(subErr,"slds-hide");
            var bodyErr = component.find("body-err");
            $A.util.addClass(bodyErr,"slds-hide");
    },
    goToFedCapOppHlpr : function(component, event, helper){
        var objId = component.get("v.recordId");
        var urlEvent = $A.get("e.force:navigateToURL");
    urlEvent.setParams({
      "url": "/"+objId
    });
    urlEvent.fire();
    },
    checkAllContactsHlpr: function(component, event, helper){
         var isChecked = component.find("chk_all_con").get("v.value");

        var conWrapList = component.get("v.conWrapList");
        if(! $A.util.isUndefined(conWrapList) && ! $A.util.isEmpty(conWrapList)){
            for(var i=0;i<conWrapList.length;i++){
                if(isChecked){
                    conWrapList[i].isSelected = true;
                }else{
                    conWrapList[i].isSelected = false;
                }
            }
            component.set("v.conWrapList",conWrapList);
        }
    },
    checkAllAttachmentsHlpr: function(component, event, helper){
         var isChecked = component.find("chk_all_attach").get("v.value");

        var attachWraList = component.get("v.attachWraList");
        if(! $A.util.isUndefined(attachWraList) && ! $A.util.isEmpty(attachWraList)){
            for(var i=0;i<attachWraList.length;i++){
                if(isChecked){
                    attachWraList[i].isSelected = true;
                }else{
                    attachWraList[i].isSelected = false;
                }
            }
            component.set("v.attachWraList",attachWraList);
        }
    },
    checkContactClick : function(component,event,helper){
        var isChkAll = true;
        var conWrapList = component.get("v.conWrapList");
        if(! $A.util.isUndefined(conWrapList)  && ! $A.util.isEmpty(conWrapList)){
            for(var i=0;i<conWrapList.length;i++){
                if(!conWrapList[i].isSelected)
                    isChkAll = false;
            }
            component.find("chk_all_con").set("v.value",isChkAll);
        }
    },
    attachClickSelectHlpr: function(component,event,helper){
        var isChkAll = true;
        var attachWraList = component.get("v.attachWraList");
        if(! $A.util.isUndefined(attachWraList)  && ! $A.util.isEmpty(attachWraList)){
            for(var i=0;i<attachWraList.length;i++){
                if(!attachWraList[i].isSelected)
                    isChkAll = false;
            }
            component.find("chk_all_attach").set("v.value",isChkAll);
        }
    }
})