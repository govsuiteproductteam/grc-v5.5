@isTest
public class TM_RecordTypeSelectionTest {
    public static testMethod void testRecordTypeSelecttion(){
        Account account = TestDataGenerator.createAccount('Test Account');
        insert account;
        Contract_Vehicle__c contractVehicle =  TestDataGenerator.createContractVehicle('12345', account);
        insert contractVehicle;
        
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(contractVehicle);
        TM_RecordTypeSelection recordTypeSelection = new TM_RecordTypeSelection(sc);
        PageReference pageRef = Page.TM_RecordTypeSelection;
        pageRef.getParameters().put('id', contractVehicle.Id);
        Test.setCurrentPage(pageRef);
        PageReference pageReference = recordTypeSelection.ContinueOnRcdTypeSelection();
        System.assertEquals('/apex/tm_toma__tm_contractnewpage?id='+contractVehicle.id,PageReference.getUrl());
        Test.stopTest();
    }
}