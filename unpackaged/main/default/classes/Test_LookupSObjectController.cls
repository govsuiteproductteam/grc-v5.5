@isTest
public class Test_LookupSObjectController {
    public static testMethod void  testLookUp(){
        Test.startTest();
        Account account = new Account(name='test Account');
        insert account;       	
        Contract contr=new Contract();
        contr.AccountId=account.id;
        insert contr;
        Test.stopTest();
        List<SearchResult> searchResultList =  LookupSObjectController.lookup('tes', 'Account');
        System.assert(searchResultList.size() == 1);
        List<SearchResult> searchResultList1 =  LookupSObjectController.getDataForLookup('Account');
        System.assert(searchResultList1.size() == 1);
        Account account1 = (Account)LookupSObjectController.getNameFromId('Account',account.id);
        System.assertEquals(account1.Name, account.Name);
        Account account2 = (Account)LookupSObjectController.getNameFromId(null,account.id);
        LookupSearch lookup=new LookupSearch(new TM_VFLookUpCtrl());
        LookupSearch lookup1=new LookupSearch(new TM_VFLookUpFilterCtrl());
        LookupSearch.searchQuery('test','test');
        LookupSearch.getLookupRecords('000', 'contract');
        LookupSearch.checkConditionValue1(''+contr.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(new Account());
        new TM_ContractModListController(sc);
        TM_DateTimeController dateTimeController=new TM_DateTimeController();
        dateTimeController.setnoOfColoumnVar(1);
        dateTimeController.getnoOfColoumnVar();
        new SearchResult('name',account.id,account);
        System.assert(account2 == null);
       
        
        
        
    }
}