@IsTest
public class Test_TM_SubContractAutoProcessClauses {
    public static testMethod void testSubContractAutoProcessClauses(){
        
        Account account = TestDataGenerator.createAccount('Test Account');
        insert account;
        Contract_Vehicle__c contractVehicle =  TestDataGenerator.createContractVehicle('12345', account);
        insert contractVehicle;
        Contract_Vehicle__c contractVehicle1 =  TestDataGenerator.createContractVehicle('1234567', account);
        insert contractVehicle1;
        Clauses__c clause = TestDataGenerator.getClause('AFFARS 5352.215-9009 Travel (AFMC)');
        insert clause;
        Clauses__c clause1 = TestDataGenerator.getClause('AFARS 5152.204-4007');
        insert clause1;
        Clauses__c clause2 = TestDataGenerator.getClause('ACEARS 52.212-5000 Evaluation of subdivided items');
        insert clause2;
        
        Contract_Clauses__c conClaus = TestDataGenerator.getContractClause(contractVehicle.id,clause.id);
        insert conClaus;
        Contract_Clauses__c conClaus1 = TestDataGenerator.getContractClause(contractVehicle.id,clause1.id);
        insert conClaus1;
        Contract_Clauses__c conClaus2 = TestDataGenerator.getContractClause(contractVehicle1.id,clause2.id);
        insert conClaus2;
        
        
        SubContract__c subCon = TestDataGenerator.createSubContract('2346');
        subCon.CSC_Contract__c = contractVehicle.id;
        insert subCon;
        SubContract__c subCon1= TestDataGenerator.createSubContract('54645');
        subCon1.CSC_Contract__c = contractVehicle1.id;
        insert subCon1;
        /*comented below code when this apex methode call from process builder*/
        List<Id> subConIdList = new List<Id>();
        subConIdList.add(subCon.Id);
        subConIdList.add(subCon1.Id);
        TM_SubContractAutoProcessClauses.copyClausesToSubContract(subConIdList);
        /*End*/
        List<Contract_Clauses__c> conClausesList =new List<Contract_Clauses__c>();
        conClausesList = [Select Id,Contract_Vehicle__c FROM Contract_Clauses__c where Contract_Vehicle__c =: subCon.CSC_Contract__c];
        List<Contract_Clauses__c> subConClausesList =new List<Contract_Clauses__c>();
        subConClausesList = [Select Id,SubContract__c FROM Contract_Clauses__c where SubContract__c =: subCon.Id];
        //System.assertEquals(conClausesList.size(), subConClausesList.size());
        
        conClausesList = [Select Id,Contract_Vehicle__c FROM Contract_Clauses__c where Contract_Vehicle__c =: subCon1.CSC_Contract__c];
        subConClausesList = [Select Id,SubContract__c FROM Contract_Clauses__c where SubContract__c =: subCon1.Id];
        //System.assertEquals(conClausesList.size(), subConClausesList.size());
    }
    
}