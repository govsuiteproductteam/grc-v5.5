@isTest
public with sharing class TestUpdateOrInsertUserAccessibility {
    public static TestMethod void updateOrInsertUserTest(){
        
        List<Folder> documentFolderList = [Select Id from Folder where Name='FedTom' Limit 1];      
        
        Document doc1 = TestDataGeneratorEncryption.createDocument('FedTomUserDoc1', documentFolderList[0].Id);
        insert doc1;
        Document doc2 = TestDataGeneratorEncryption.createDocument('FedCLMUserDoc1', documentFolderList[0].Id);
        insert doc2;
        Profile profile = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User user1 = TestDataGeneratorEncryption.createUser(profile.Id,'19');
        user1.TM_TOMA__FedCLM_User__c = true;
        user1.TM_TOMA__FedTOM_User__c = true;
        User user2 = TestDataGeneratorEncryption.createUser(profile.Id,'29');
        user2.TM_TOMA__FedCLM_User__c = true;
        user2.TM_TOMA__FedTOM_User__c = true;
        User user3 = TestDataGeneratorEncryption.createUser(profile.Id,'3');
        List<User> userList = [SELECT Id FROM User];
        System.debug('Size = '+userList.size() );
        List<user> userListTom = [SELECt Id FROM User WHERE TM_TOMA__FedTOM_User__c = true];
        List<user> userListCLM = [SELECt Id FROM User WHERE TM_TOMA__FedCLM_User__c = true];
        if((userListTom!= null) && (userListCLM!= null )){
            Integer fedTomCounter;
            Integer fedCLMCounter;                        
            fedTomCounter = userListTom.size()+1;                        
            fedCLMCounter = userListCLM.size()+1;
            Test.startTest();
            EncryptionDecryptionController controller = new EncryptionDecryptionController();
            try{                
                controller.fedCLMLicence = ''+fedCLMCounter;
                System.debug('controller.fedCLMLicence = '+controller.fedCLMLicence);
                controller.fedTOMLicence = ''+fedTomCounter;        
                System.debug('controller.fedTOMLicence = '+controller.fedTOMLicence);
                controller.createCryptoKeyDoc();
                insert user1;               
                user1.TM_TOMA__FedCLM_User__c = false;
                user1.TM_TOMA__FedCLM_User__c = false;
                update user1;                
                insert user2;
                try{
                    user1.TM_TOMA__FedCLM_User__c = true;
                    user1.TM_TOMA__FedCLM_User__c = true;
                    update user1;
                }catch(Exception e){
                    System.debug('e.getMessage() ='+e.getMessage());
                    Boolean expectedExceptionThrown =  e.getMessage().contains('Insert failed. First exception on row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, '+'FedTom cannot Update More than '+ controller.fedCLMLicence+' records or FedCLM cannot Update More than '+controller.fedTOMLicence+' records.') ? true : false;
                    System.AssertEquals(expectedExceptionThrown, true);
                }
                insert user3;
            }catch(Exception e){
                System.debug('e.getMessage() ='+e.getMessage());
                System.debug('Insert failed. First exception on row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, '+'FedTom cannot Update More than '+ controller.fedCLMLicence+' records or FedCLM cannot Update More than '+controller.fedTOMLicence+' records.: []');
                Boolean expectedExceptionThrown =  e.getMessage().trim().contains('Insert failed. First exception on row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, '+'FedTom cannot Update More than '+ controller.fedTOMLicence+' records or FedCLM cannot Update More than '+controller.fedCLMLicence+' records.: []') ? true : false;
                System.AssertEquals(expectedExceptionThrown, true);
            }
             try{
                    user1.TM_TOMA__FedCLM_User__c = true;
                    user1.TM_TOMA__FedCLM_User__c = true;
                    update user1;                 
                }catch(Exception e){
                    System.debug('e.getMessage() ='+e.getMessage());
                    Boolean expectedExceptionThrown =  e.getMessage().contains('Insert failed. First exception on row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, '+'FedTom cannot Update More than '+ controller.fedCLMLicence+' records or FedCLM cannot Update More than '+controller.fedTOMLicence+' records.') ? true : false;
                    System.AssertEquals(expectedExceptionThrown, true);
                }
            //insert user2;
            //insert user3;
            Test.stopTest();
        }
    }
    
}