({
    doCheckLicense : function(component, event, helper) {
        var action = component.get("c.checkLicensePermition1");
        action.setCallback(this, function(a) {
            if(a.getReturnValue() === 'Yes')
            {
                component.set("v.CheckLicense", 'Yes');
                component.set("v.checkSpinner1",'Yes');
                helper.getDistributionFieldSetWrapperData(component, event, helper);
                helper.getDistributionDataWrapList(component, event, helper);
                // helper.getAllRolePicklistValue(component, event, helper);
                component.set("v.checkSpinner1",'No');
            }
            else
            {
                component.set("v.CheckLicense", a.getReturnValue());
            }
        });
        $A.enqueueAction(action);  
    },
    getRolePicklist: function(component, event, helper) {
        
        helper.getAllRolePicklistValue(component, event, helper);
    },
    
    addDistributionRowInWrap : function(component, event, helper) {
        var distributionDataList=component.get("v.distributionDataWrapList");
        var action = component.get("c.addNewDistributionRowInWrap"); //call apex controller method
        action.setParams({
            "parentObjId": component.get("v.recordId")
        });
        action.setCallback(this,function(a){
            var state= a.getState();
            if(state=="SUCCESS"){
                var newDistRec=a.getReturnValue();
                distributionDataList.push(newDistRec);
                component.set("v.distributionDataWrapList",distributionDataList);
                var checksave = component.get("v.checkSavebutton");
                checksave=checksave+1;
                component.set("v.checkSavebutton",checksave);
            }
            else if (state == "ERROR") 
            {
                console.log(a.getError());            
            }
        });
        $A.enqueueAction(action);
    },
    removeRowFromWrap : function(component, event, helper) {
        var conf = confirm("Are you sure you want to delete this record?");
        if(conf== true)
        {
            var distributionList=component.get("v.distributionDataWrapList");
            var delDistId=component.get("v.delDistributionId");
            var distributionTablesize=component.get("v.distributionSizeFlag");
            var selectedItem = event.currentTarget;
            var removeCounter = selectedItem.id;
            if (removeCounter<distributionTablesize) {
                delDistId+=',';
                distributionTablesize-=1;
                component.set("v.distributionSizeFlag",distributionTablesize);
                delDistId=delDistId+distributionList[removeCounter].disRecord.Id;
                component.set("v.delDistributionId",delDistId);
            } 
            distributionList.splice(removeCounter, 1);
            component.set("v.distributionDataWrapList", distributionList);
        }
    },
    deleteRowFromWrap : function(component, event, helper){
        
        var conf = confirm("Are you sure you want to delete this record?");
        if(conf== true)
        {
            var distList1 = component.get("v.distributionDataWrapList");
            var selectedItem = event.currentTarget;
            var removeCounter = selectedItem.id;
            var tempCont = selectedItem.dataset.arg1;
            if (tempCont != undefined && tempCont != '') {
                var action = component.get("c.deleteDistribution");
                action.setParams({ 
                    "delId": tempCont
                });
                action.setCallback(this, function(response){
                    var state = response.getState();
                    if (state === "SUCCESS")
                    {
                        helper.getDistributionDataWrapList(component, event, helper);
                        alert('Record was successfully deleted.');
                        /* var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Success!",
                            "message": "Record was successfully deleted.",
                            "type":"success"
                        });
                        toastEvent.fire();*/
                        
                    }
                    else if (state === "ERROR") 
                    {
                        alert('An error occurred while deleting the record. Please refresh the page.');
                        /* var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!",
                            "message": "An error occurred while deleting the record. Please refresh the page.",
                            "type":"error"
                        });
                        toastEvent.fire();*/
                    }
                });
                $A.enqueueAction(action);
            } 
            else{
                var checksave = component.get("v.checkSavebutton");
                checksave=checksave-1;
                component.set("v.checkSavebutton",checksave);
                distList1.splice(removeCounter, 1);
                component.set("v.distributionDataWrapList", distList1);
            }
        }
        
    },
    editRow : function(component, event, helper) {
        var selectedItem = event.currentTarget;
        var checkCounter = selectedItem.id;
        var elements = document.getElementsByClassName(checkCounter+'distViewCls');
        // console.log('@@@'+elements.length);
        for(var i=0; i<elements.length; i++) {
            // console.log(elements[i].className);
            elements[i].className += " slds-hide";
            elements[i].classList.remove("slds-show");
        }
        var elements1 = document.getElementsByClassName(checkCounter+'distEditCls');
        //console.log('@@@'+elements1.length);
        for(var i=0; i<elements1.length; i++) {
            //  console.log(elements1[i].className);
            elements1[i].className += " slds-show";
            elements1[i].classList.remove("slds-hide");
        }
        var distributionDataWrapList1 = component.get("v.distributionDataWrapList");
        //alert(distributionDataWrapList1[checkCounter].checkEdit);
        distributionDataWrapList1[checkCounter].checkEdit = 'Yes';
        component.set("v.distributionDataWrapList",distributionDataWrapList1);
        
        
        var d4 = document.getElementById(checkCounter+'distEdit');
        d4.className += " slds-hide";
        d4.classList.remove("slds-show");
        var d5 = document.getElementById(checkCounter+'distEditUndo');
        d5.className += " slds-show";
        d5.classList.remove("slds-hide");
        
        var checksave = component.get("v.checkSavebutton");
        checksave=checksave+1;
        component.set("v.checkSavebutton",checksave);
        // var oldval = selectedItem.dataset.arg1;
        // document.getElementById(checkCounter+'vehEdit').dataset.arg1 = selectedItem.dataset.arg1;
        // document.getElementById(checkCounter+'vehEdit').dataset.arg3 = selectedItem.dataset.arg2;
        // var checksave = component.get("v.checkSavebutton");
        //            checksave=checksave+1;
        // component.set("v.checkSavebutton",checksave);
        
    },
    closeRow : function(component, event, helper) {
        var selectedItem = event.currentTarget;
        var checkCounter = selectedItem.id;
        var distributionDataWrapList1 = component.get("v.distributionDataWrapList");
        var distFList1 = component.get("v.distributionFieldSetWrapperData");
        
        var elements = document.getElementsByClassName(checkCounter+'distEditCls');
        // console.log('@@@'+elements.length);
        for(var i=0; i<elements.length; i++) {
            elements[i].className += " slds-hide";
            elements[i].classList.remove("slds-show");
            var oldVal = elements[i].dataset.arg2.split(":-");
            if(oldVal[1] == undefined || oldVal[1] == 'undefined' || oldVal[1] == null || oldVal[1] == 'null' || oldVal[1] == 'Null' || oldVal[1] == '' || oldVal[1] == '--None--'){
                distributionDataWrapList1[checkCounter].disRecord[oldVal[0]] = '';
                /*Add for undu functionality*/
                var lookupOldData = $A.get("e.c:distributionlookuppicklistolddataEvent");
                lookupOldData.setParams({
                    "index" : checkCounter,
                    "api" : oldVal[0],
                    "oldValueLookup": 'undefined'
                });
                lookupOldData.fire();
            }
            else{
                distributionDataWrapList1[checkCounter].disRecord[oldVal[0]] = oldVal[1];
                /*Add for undu functionality*/
                var lookupOldData = $A.get("e.c:distributionlookuppicklistolddataEvent");
                lookupOldData.setParams({
                    "index" : checkCounter,
                    "api" : oldVal[0],
                    "oldValue": oldVal[1]
                });
                lookupOldData.fire();
                
            }
        }
        distributionDataWrapList1[checkCounter].checkEdit = 'No';
        component.set("v.distributionDataWrapList",distributionDataWrapList1);
        var elements1 = document.getElementsByClassName(checkCounter+'distViewCls');
        // console.log('@@@'+elements1.length);
        for(var i=0; i<elements1.length; i++) {
            //console.log(elements1[i].className);
            elements1[i].className += " slds-show";
            elements1[i].classList.remove("slds-hide");
        }
        
        var d4 = document.getElementById(checkCounter+'distEdit');
        d4.className += " slds-show";
        d4.classList.remove("slds-hide");
        var d5 = document.getElementById(checkCounter+'distEditUndo');
        d5.className += " slds-hide";
        d5.classList.remove("slds-show");
        
        var checksave = component.get("v.checkSavebutton");
        checksave=checksave-1;
        component.set("v.checkSavebutton",checksave);
        
    },
    saveDistributionWraper : function(component, event, helper) {
        document.getElementById("saveButtonDistribution").disabled = true;
        document.getElementById("saveButtonDistribution1").disabled = true;
        setTimeout( 
            function () {
        component.set("v.checkSpinner1",'Yes');
        var deldistId=component.get("v.delDistributionId");
        var distributionFieldListWrap = component.get("v.distributionFieldSetWrapperData");
        var distributionListWrap = component.get("v.distributionDataWrapList");
        var disSizeFlag = component.get("v.distributionSizeFlag");
        var checkValidation = 'No';
        var distributionTableJSON = '[';
        if(distributionListWrap != undefined){
            for (var i=0; i<distributionListWrap.length; i++){
                //alert(distributionListWrap[i].checkEdit);
                if(distributionListWrap[i].checkEdit == 'Yes'){
                    distributionTableJSON += "id"+"SPLITFIELD"+distributionListWrap[i].disRecord.Id+"SPLITDATA";
                    if(distributionFieldListWrap != undefined)
                    {
                        for (var j=0; j<distributionFieldListWrap.length; j++){
                            if(distributionListWrap[i].disRecord[distributionFieldListWrap[j].fieldAPI] == '' || distributionListWrap[i].disRecord[distributionFieldListWrap[j].fieldAPI] == 'undefined' || distributionListWrap[i].disRecord[distributionFieldListWrap[j].fieldAPI] == undefined || distributionListWrap[i].disRecord[distributionFieldListWrap[j].fieldAPI] == '--None--' || distributionListWrap[i].disRecord[distributionFieldListWrap[j].fieldAPI] == null || distributionListWrap[i].disRecord[distributionFieldListWrap[j].fieldAPI] == 'null')
                            {
                                //alert(i.valueOf() +'  '+disSizeFlag.valueOf());
                                checkValidation = 'Yes';
                                /*if(i.valueOf() < disSizeFlag.valueOf())
                            {
                                helper.checkValidation(component, event, helper,i);
                            }*/
                            }
                            
                            distributionTableJSON += distributionFieldListWrap[j].fieldAPI+"SPLITFIELD"+distributionListWrap[i].disRecord[distributionFieldListWrap[j].fieldAPI]+"SPLITDATA";
                        }
                        distributionTableJSON += "SPLITPARENT";
                    }
                }
            }
            // alert(distributionTableJSON);
            var fieldApiName='';
            var columeLabelName='';
            for (var j=0; j<distributionFieldListWrap.length; j++){
                fieldApiName=fieldApiName+distributionFieldListWrap[j].fieldAPI+',';
                columeLabelName = columeLabelName+distributionFieldListWrap[j].label+',';
            }
            columeLabelName = columeLabelName.substring(0, columeLabelName.length-1);
            //alert(distributionTableJSON);
            if(checkValidation == 'No')
            {
                var action = component.get("c.saveDistributionRecordList");
                action.setParams({ 
                    "distributionList": distributionTableJSON,
                    "distributionDeletedId": deldistId,
                    "distributionFieldApiName": fieldApiName,
                    "parentRecId": component.get("v.recordId")
                });
                action.setCallback(this, function(response){
                    var state = response.getState();
                    if (state === "SUCCESS")
                    {
                         component.set("v.checkSavebutton",0);
                         setTimeout( 
                            function () { 
                             alert('Changes were saved successfully.');
                            },100);
                       // alert('Changes were saved successfully.');
                        /* var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Success!",
                            "message": "Changes were saved successfully.",
                            "type":"success"
                        });
                        toastEvent.fire();*/
                        
                        helper.getDistributionFieldSetWrapperData(component, event, helper);
                        helper.getDistributionDataWrapList(component, event, helper);
                       
                        component.set("v.checkSpinner1",'No');
                      
                                document.getElementById("saveButtonDistribution").disabled = false;
                                document.getElementById("saveButtonDistribution1").disabled = false;
                         /*  setTimeout( 
                            function () {   },500);*/
            }
                    else if (state === "ERROR") 
                    {
                        console.log(response.getError()); 
                        //component.set("v.checkSavebutton",0);
                        component.set("v.checkSpinner1",'No');
                        document.getElementById("saveButtonDistribution").disabled = false;
                        document.getElementById("saveButtonDistribution1").disabled = false;
                    }
                });
                $A.enqueueAction(action);
            }
            else {
                component.set("v.checkSpinner1",'No');
                document.getElementById("saveButtonDistribution").disabled = false;
               document.getElementById("saveButtonDistribution1").disabled = false;
                alert('Please enter value for all required fields '+columeLabelName);
                /* var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": "Please enter value for all required fields",
                    "type":"error"
                });
                toastEvent.fire();*/
            }
            
        }
        component.set("v.checkSpinner1",'No');
        document.getElementById("saveButtonDistribution").disabled = false;
        document.getElementById("saveButtonDistribution1").disabled = false;
                 },500);
    }
})