public class GSAReverseAuctionEmailParser implements IEmailParser {
    
    private String emailSubject;
    public String taskOrderNumber;
    private String taskOrderTitle;
    Private String awardType;
    private String agency;
    private String contactName;
    private String emailId;
    
    
    public void parse(Messaging.InboundEmail email){
        emailSubject = email.subject.trim();
        if(emailSubject.length() == 0){
            throw new ContractVehicleNotFoundException(MessageConstantController.TASK_ORDER_NOT_FOUND);
        }
        
        try{
            taskOrderNumber = emailSubject.substringBetween('Reverse Auctions: Invitation to bid for Auction: ',' ').trim();            
        }catch(Exception ex){
             System.debug(''+ex.getMessage()+'\t: '+ex.getLineNumber());
        }
        System.debug('Task Order Number = '+taskOrderNumber); 
        
        try{
            taskOrderTitle = emailSubject.substringAfter('Auction Name:').trim();            
        }catch(Exception ex){
             System.debug(''+ex.getMessage()+'\t: '+ex.getLineNumber());
        }
        System.debug('Task Order Title = '+taskOrderTitle);
        
        
        String body = email.plainTextBody;
        try{
            awardType = body.substringBetween('Award Type:','\n').trim();            
        }catch(Exception ex){
             System.debug(''+ex.getMessage()+'\t: '+ex.getLineNumber());
        }
        System.debug('Award Type = '+awardType);
        
        System.debug('Body = '+body);
        try{
            String rawContactInfo = body.substringBetween('This reverse auction is being conducted by:','Please go to');
            
            System.debug('Raw Contact Info ='+rawContactInfo);
            List<String> splitedData = rawContactInfo.split('\n');    
            if(splitedData.size()>0){                                                           
                for(Integer i=0;i<splitedData.size();i++){
                    try{
                        if(i==1){
                            contactName = splitedData[i].trim(); 
                        } 
                    }catch(Exception ex){
                        System.debug(''+ex.getMessage()+'\t: '+ex.getLineNumber()); 
                    }
                    System.debug('Contact Name = '+contactName);
                    try{
                        if(i==2){
                            agency = splitedData[i].trim();                             
                        } 
                    }catch(Exception ex){
                        System.debug(''+ex.getMessage()+'\t: '+ex.getLineNumber());
                    }
                    System.debug('Agency = '+agency);
                    try{
                        if(splitedData.get(i).contains('@')){
                            emailId = splitedData.get(i).stripHtmlTags();
                        }
                    }catch(Exception ex){
                        System.debug(''+ex.getMessage()+'\t: '+ex.getLineNumber());
                    }
                    System.debug('Email Id = '+emailId);
                }
            }                           
        } catch(Exception ex){
            System.debug('*** Here ***');
            System.debug(''+ex.getMessage()+'\t: '+ex.getLineNumber());
        } 
        
        
        
        
    }
    
    public Task_Order__c getTaskOrder(String toAddress){
        if(taskOrderNumber==null){
            return null;
        }   
        Task_Order__c taskOrder = new Task_Order__c();
        List<Contract_Vehicle__c> conVehicalList = [SELECT Id FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];
        if(!conVehicalList.isEmpty()){
            taskOrder.Contract_Vehicle__c = conVehicalList[0].Id; // Label = Contract Vehicle
            taskOrder.Name = taskOrderNumber; //Task Order Number
            
            if(taskOrderTitle !=null)           
                taskOrder.Task_Order_Title__c = taskOrderTitle;// Task Order Title
            
            if(agency !=null)           
                taskOrder.Agency__c = agency; // Agency 
            
            if(awardType !=null)           
                taskOrder.Award_Type__c = awardType; // Award Type 
            
            taskOrder.Release_Date__c = Date.today();//RFP/TO Release Date
            
            try{
                Contact contact = createContact(contactName,emailId);
                if(contact.Id == null)
                    DMLManager.insertAsUser(contact);
                
                taskOrder.Contracting_Officer__c = contact.Id;
            }catch(Exception e){
                System.debug('Error in contact - '+e.getMessage()+':'+e.getLineNumber());
            }
            return taskOrder;
        }else{
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
        }       
    }
    
    public Task_Order__c updateTaskOrder(Task_Order__c taskOrder, String toAddress){
        List<Contract_Vehicle__c> conVehicalList = [SELECT Id FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress]; 
        if(!conVehicalList.isEmpty()){
            taskOrder.Contract_Vehicle__c = conVehicalList[0].Id; // Label = Contract Vehicle
            //taskOrder.Name = taskOrderNumber; //Task Order Number
            
            if(taskOrderTitle !=null)           
                taskOrder.Task_Order_Title__c = taskOrderTitle;// Task Order Title
            
            if(agency !=null)           
                taskOrder.Agency__c = agency; // Agency 
            
            if(awardType !=null)           
                taskOrder.Award_Type__c = awardType; // Award Type 
            
            try{
                Contact contact = createContact(contactName,emailId);
                if(contact != null){
                    if(contact.Id == null){                        
                        DMLManager.insertAsUser(contact);
                    }
                    taskOrder.Contracting_Officer__c = contact.Id;  
                }
            }catch(Exception e){
                System.debug('Error in contact creation - '+e.getMessage()+':'+e.getLineNumber());
            }
            return taskOrder;
        }else{
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
        }         
    }
    
    
    private Contact createContact(String name, String email){
        try{
            List<Contact> contactList = new List<Contact>();
            System.debug('email='+email);           
            if(email != null && email != ''){
                // To remove the url part, everything including the "<" and ">"
                /* Ex. avery.e.williams.civ@mail.mil <aaron.m.west19.civ@mail.mil>, 
remove "<aaron.m.west19.civ@mail.mil>" from the above Email Id*/
                email = email.replaceAll('\\<[^\\<\\>]*[^\\>\\<]*\\>', '').replaceAll('\\s+','');
                contactList = [SELECT Id,email From Contact WHERE email=:email];
            }
            
            if(contactList.isEmpty()){
                Contact contact = new Contact();
                String[] nameSplit = name.split(' ');
                contact.FirstName = nameSplit[0];
                contact.lastName = nameSplit[1];                               
                contact.Email = email;
                contact.Title = 'Contracting Officer';
                contact.Is_FedTom_Contact__c = true;
                return contact;                
            }
            else{
                return contactList[0];
            }
        }catch(Exception ex){
            
        }
        return null;
    }
}