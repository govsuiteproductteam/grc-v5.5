public with sharing class HistoryController {
        @AuraEnabled
        public String apiName;
        @AuraEnabled
        public string oldInformation;
        @AuraEnabled
        public string updatedInformation;
         @AuraEnabled public String ceatedDate {get;set;}
        public HistoryController (string fName,string oldValue,string updatedValue,String d1)
        {
            system.debug('Inside FboHistoryController1 ');
            apiName=fName;
            oldInformation=oldValue;
            updatedInformation=updatedValue;
            ceatedDate=d1;
            
        }
    }