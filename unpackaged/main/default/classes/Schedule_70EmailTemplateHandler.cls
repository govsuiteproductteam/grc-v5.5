global class Schedule_70EmailTemplateHandler implements Messaging.InboundEmailHandler{
  global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:email.toAddresses.get(0)];
        if(!conVehicalList.isEmpty()){
            //*********
            Schedule_70EmailTemplateParser parser = new Schedule_70EmailTemplateParser();
            parser.parse(email);
            //*********
            List<Task_Order__c> updateTaskOrderList = new List<Task_Order__c>();//to maintain multiple updated record
            List<Task_Order__c> taskOrderList = new List<Task_Order__c>();//to maintain multiple taskorder record
            List<Task_Order__c> oldTaskOrderList = new List<Task_Order__c>();//for contract mod functionality while updating the record
            List<String> rfqIds = parser.getRFQIds();
            Integer size = rfqIds.size();
            for(Integer index = 0 ; index < size; index++){ 
                Task_Order__c taskOrder =  getExistTaskOrder(rfqIds[index], conVehicalList[0].id);              
                if(taskOrder != null){//If email template exists (new -> According to RFQId)           
                    try{
                        Task_Order__c oldTaskOrder = taskOrder.clone(false,true,false,false);      
                        System.debug('oldTaskOrder '+oldTaskOrder);
                        Task_Order__c updatedTakOrder = parser.updateTaskOrder(taskOrder, email.toAddresses.get(0),index);
                        System.debug('oldTaskOrder '+oldTaskOrder);
                        System.debug('updatedTakOrder '+updatedTakOrder);
                        updateTaskOrderList.add(updatedTakOrder);
                        oldTaskOrderList.add(oldTaskOrder);
                        //EmailHandlerHelper.insertContractMods(updatedTakOrder , oldTaskOrder,email);
                        
                        //update updatedTakOrder;
                        //EmailHandlerHelper.insertBinaryAttachment(updatedTakOrder,email);
                        //EmailHandlerHelper.insertTextAttachment(updatedTakOrder,email);
                        //EmailHandlerHelper.insertActivityHistory(updatedTakOrder,email);                
                        system.debug('Already Exists');
                    }catch(Exception e){
                        System.debug('error message = '+e.getMessage()+'\tline number = '+e.getLineNumber());
                    }      
                }else{//for new task order
                    taskOrder = parser.getTaskOrder(email.toAddresses.get(0),index);//This will be the new Task Order, We are just checking that the TO object is not null in next line
                    if(taskOrder != null){  
                        try{
                            taskOrderList.add(taskOrder);
                            //DMLManager.insertAsUser(taskOrder);
                            //EmailHandlerHelper.insertBinaryAttachment(taskOrder,email);
                            //EmailHandlerHelper.insertTextAttachment(taskOrder,email);
                            //EmailHandlerHelper.insertActivityHistory(taskOrder,email);
                        }catch(Exception e){
                            System.debug('error message = '+e.getMessage());
                        }
                    }
                }
            }
            try{
                if(!taskOrderList.isEmpty()){
                    insert taskOrderList;
                    EmailHandlerHelper1.insertBinaryAttachment(taskOrderList,email);
                    EmailHandlerHelper1.insertTextAttachment(taskOrderList,email);
                    EmailHandlerHelper1.insertActivityHistory(taskOrderList,email);
                }
                
            }catch(Exception ex){
                
            }
            try{
                if(!updateTaskOrderList.isEmpty()){
                    System.debug('size ='+updateTaskOrderList+'\t'+oldTaskOrderList);
                    update updateTaskOrderList;
                    EmailHandlerHelper1.insertContractMods(updateTaskOrderList , oldTaskOrderList,email);                    
                    EmailHandlerHelper1.insertBinaryAttachment(updateTaskOrderList,email);
                    EmailHandlerHelper1.insertTextAttachment(updateTaskOrderList,email);
                    EmailHandlerHelper1.insertActivityHistory(updateTaskOrderList,email);
                }
            }catch(Exception ex){
                
            }
        }else{
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
        }
        return result;
    }    
    
    private Task_Order__c getExistTaskOrder(String taskOrderNum,String contractId){
        List<Task_Order__c> taskOrderList = [SELECT Id,Name,Email_Reference__c, Task_Order_Title__c, Due_Date__c, Release_Date__c,Contracting_Officer__c,Contract_Specialist__c,ASB_Action__c,Description__c,Start_Date__c,Stop_Date__c,
                                             Contract_Negotiator__c,Proposal_Due_DateTime__c FROM Task_Order__c where Name =: taskOrderNum AND Contract_Vehicle__c =: contractId];
        System.debug('taskOrderList'+taskOrderList);
        if(!taskOrderList.isEmpty()){
            return taskOrderList[0];
        }
        return null;
    }  
    
    
}