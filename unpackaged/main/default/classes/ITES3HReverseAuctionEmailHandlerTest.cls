@isTest
public class ITES3HReverseAuctionEmailHandlerTest {
    
    public static testmethod void testCase1(){
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Contact con = TestDataGenerator.createContact('TestCon', acc );
        insert con;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('T-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'ites3h_reverse_auction@2yeat42pz5216f1sd41vz07qdcqq5xomj34xxh7ybtsennj2hy.61-zhsweao.na34.apex.salesforce.com';
        
        insert conVehi;
        
        //For new Task Order Creation  
        Messaging.InboundEmail email  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        List<StaticResource> srList = [SELECT Id,Body FROM StaticResource WHERE Name='ItesReverseAuctionRequestSubmitted'];
        email.plainTextBody = srList.get(0).Body.toString();
        email.fromAddress ='test@test.com';
        email.fromName = 'ABC XYZ';
        email.subject = 'ITES-3H CHESS Reverse Auction: Request Submitted';
        email.toaddresses = new List<String>();
        email.toaddresses.add('ites3h_reverse_auction@2yeat42pz5216f1sd41vz07qdcqq5xomj34xxh7ybtsennj2hy.61-zhsweao.na34.apex.salesforce.com');
        
        //To update existing task order 
        Messaging.InboundEmail email1  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env1 = new Messaging.InboundEnvelope();
        List<StaticResource> srList1 = [SELECT Id,Body FROM StaticResource WHERE Name='ItesReverseAuctionRequestSubmitted_Updated'];
        email1.plainTextBody = srList1.get(0).Body.toString();
        email1.fromAddress ='test@test.com';
        email1.fromName = 'ABC XYZ';
        email1.subject = 'ITES-3H CHESS Reverse Auction: Request Submitted';
        email1.toaddresses = new List<String>();
        email1.toaddresses.add('ites3h_reverse_auction@2yeat42pz5216f1sd41vz07qdcqq5xomj34xxh7ybtsennj2hy.61-zhsweao.na34.apex.salesforce.com');
        
        //For Task Order Amendment
        Messaging.InboundEmail email2  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env2 = new Messaging.InboundEnvelope();
        List<StaticResource> srList2 = [SELECT Id,Body FROM StaticResource WHERE Name='ItesReverseAuctionAmendment'];
        email2.plainTextBody = srList2.get(0).Body.toString();
        email2.fromAddress ='test@test.com';
        email2.fromName = 'ABC XYZ';
        email2.subject = 'ITES-3H CHESS Reverse Auction: Amendment';
        email2.toaddresses = new List<String>();
        email2.toaddresses.add('ites3h_reverse_auction@2yeat42pz5216f1sd41vz07qdcqq5xomj34xxh7ybtsennj2hy.61-zhsweao.na34.apex.salesforce.com');
        
        //For task order Q&A
        Messaging.InboundEmail email3  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env3 = new Messaging.InboundEnvelope();
        List<StaticResource> srList3 = [SELECT Id,Body FROM StaticResource WHERE Name='ItesReverseAuctionQnA'];
        email3.plainTextBody = srList3.get(0).Body.toString();
        email3.fromAddress ='test@test.com';
        email3.fromName = 'ABC XYZ';
        email3.subject = 'ITES-3H CHESS Reverse Auction: Response Received to a Question';
        email3.toaddresses = new List<String>();
        email3.toaddresses.add('ites3h_reverse_auction@2yeat42pz5216f1sd41vz07qdcqq5xomj34xxh7ybtsennj2hy.61-zhsweao.na34.apex.salesforce.com');
        
        //For task order cancellation
        Messaging.InboundEmail email4  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env4 = new Messaging.InboundEnvelope();
        List<StaticResource> srList4 = [SELECT Id,Body FROM StaticResource WHERE Name='ItesReverseAuctionCancellation'];
        email4.plainTextBody = srList4.get(0).Body.toString();
        email4.fromAddress ='test@test.com';
        email4.fromName = 'ABC XYZ';
        email4.subject = 'ITES-3H CHESS Reverse Auction: Cancellation';
        email4.toaddresses = new List<String>();
        email4.toaddresses.add('ites3h_reverse_auction@2yeat42pz5216f1sd41vz07qdcqq5xomj34xxh7ybtsennj2hy.61-zhsweao.na34.apex.salesforce.com');
        
        
        
        ITES3HReverseAuctionEmailHandler handler = new ITES3HReverseAuctionEmailHandler();
        Test.startTest();
        Messaging.InboundEmailResult result = handler.handleInboundEmail(email, env);
        List<Task_Order__c> taskOrderList = [SELECT Id FROM Task_Order__c];
        Messaging.InboundEmailResult result1 = handler.handleInboundEmail(email1, env1);
        List<Contract_Mods__c> contractModList = [SELECT Id FROM Contract_Mods__c];
        Messaging.InboundEmailResult result2 = handler.handleInboundEmail(email2, env2);
        Messaging.InboundEmailResult result3 = handler.handleInboundEmail(email3, env3);
        Messaging.InboundEmailResult result4 = handler.handleInboundEmail(email4, env4);
        Test.stopTest();
        
        System.assertEquals(1, taskOrderList.size());
        System.assertEquals(1, contractModList.size());
        
    }
    
    public static testmethod void testCase2(){
        
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Contact con = TestDataGenerator.createContact('TestCon', acc );
        insert con;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('T-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'ites3h_reverse_auction@2yeat42pz5216f1sd41vz07qdcqq5xomj34xxh7ybtsennj2hy.61-zhsweao.na34.apex.salesforce.com';
        
        insert conVehi;
        
        Messaging.InboundEmail email5  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env5 = new Messaging.InboundEnvelope();
        List<StaticResource> srList5 = [SELECT Id,Body FROM StaticResource WHERE Name='ItesReverseAuctionRequestSubmitted'];
        String body = srList5.get(0).Body.toString();
        body = body.replace('Description:', '');
        email5.plainTextBody =body;
        email5.fromAddress ='test@test.com';
        email5.fromName = 'ABC XYZ';
        email5.subject = 'ITES-3H CHESS Reverse Auction: Request Submitted';
        email5.toaddresses = new List<String>();
        email5.toaddresses.add('ites3h_reverse_auction@2yeat42pz5216f1sd41vz07qdcqq5xomj34xxh7ybtsennj2hy.61-zhsweao.na34.apex.salesforce.com');
        
        Messaging.InboundEmail email6  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env6 = new Messaging.InboundEnvelope();
        List<StaticResource> srList6 = [SELECT Id,Body FROM StaticResource WHERE Name='ItesReverseAuctionRequestSubmitted'];
        String body1 = srList6.get(0).Body.toString();
        body1 = body1.replace('Auction End Time', '');
        email6.plainTextBody =body1;
        email6.fromAddress ='test@test.com';
        email6.fromName = 'ABC XYZ';
        email6.subject = 'ITES-3H CHESS Reverse Auction: Request Submitted';
        email6.toaddresses = new List<String>();
        email6.toaddresses.add('ites3h_reverse_auction@2yeat42pz5216f1sd41vz07qdcqq5xomj34xxh7ybtsennj2hy.61-zhsweao.na34.apex.salesforce.com');
        
        Messaging.InboundEmail email7  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env7 = new Messaging.InboundEnvelope();
        List<StaticResource> srList7 = [SELECT Id,Body FROM StaticResource WHERE Name='ItesReverseAuctionRequestSubmitted'];
        String body2 = srList7.get(0).Body.toString();
        body2 = body2.replace('Email:', '');
        email7.plainTextBody =body2;
        email7.fromAddress ='test@test.com';
        email7.fromName = 'ABC XYZ';
        email7.subject = 'ITES-3H CHESS Reverse Auction: Request Submitted';
        email7.toaddresses = new List<String>();
        email7.toaddresses.add('ites3h_reverse_auction@2yeat42pz5216f1sd41vz07qdcqq5xomj34xxh7ybtsennj2hy.61-zhsweao.na34.apex.salesforce.com');
        
        Messaging.InboundEmail email8  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env8 = new Messaging.InboundEnvelope();
        List<StaticResource> srList8 = [SELECT Id,Body FROM StaticResource WHERE Name='ItesReverseAuctionRequestSubmitted'];
        String body3 = srList8.get(0).Body.toString();
        body3 = body3.replace('(', '');
        email8.plainTextBody =body3;
        email8.fromAddress ='test@test.com';
        email8.fromName = 'ABC XYZ';
        email8.subject = 'ITES-3H CHESS Reverse Auction: Request Submitted';
        email8.toaddresses = new List<String>();
        email8.toaddresses.add('ites3h_reverse_auction@2yeat42pz5216f1sd41vz07qdcqq5xomj34xxh7ybtsennj2hy.61-zhsweao.na34.apex.salesforce.com');
        
        Messaging.InboundEmail email9  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env9 = new Messaging.InboundEnvelope();
        List<StaticResource> srList9 = [SELECT Id,Body FROM StaticResource WHERE Name='ItesReverseAuctionRequestSubmitted'];
        String body4 = srList9.get(0).Body.toString();
        body4 = body4.replace('Phone:', '');
        email9.plainTextBody =body4;
        email9.fromAddress ='test@test.com';
        email9.fromName = 'ABC XYZ';
        email9.subject = 'ITES-3H CHESS Reverse Auction: Request Submitted';
        email9.toaddresses = new List<String>();
        email9.toaddresses.add('ites3h_reverse_auction@2yeat42pz5216f1sd41vz07qdcqq5xomj34xxh7ybtsennj2hy.61-zhsweao.na34.apex.salesforce.com');
        
        Messaging.InboundEmail email10  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env10 = new Messaging.InboundEnvelope();
        List<StaticResource> srList10 = [SELECT Id,Body FROM StaticResource WHERE Name='ItesReverseAuctionRequestSubmitted'];
        String body5 = srList8.get(0).Body.toString();
        body5 = body5.replace('Organization:', '');
        email10.plainTextBody =body5;
        email10.fromAddress ='test@test.com';
        email10.fromName = 'ABC XYZ';
        email10.subject = 'ITES-3H CHESS Reverse Auction: Request Submitted';
        email10.toaddresses = new List<String>();
        email10.toaddresses.add('ites3h_reverse_auction@2yeat42pz5216f1sd41vz07qdcqq5xomj34xxh7ybtsennj2hy.61-zhsweao.na34.apex.salesforce.com');
        
        Messaging.InboundEmail email11  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env11 = new Messaging.InboundEnvelope();
        List<StaticResource> srList11 = [SELECT Id,Body FROM StaticResource WHERE Name='ItesReverseAuctionRequestSubmitted'];
        String body6 = srList8.get(0).Body.toString();
        body6 = body6.replace('Activity:', '');
        email11.plainTextBody =body6;
        email11.fromAddress ='test@test.com';
        email11.fromName = 'ABC XYZ';
        email11.subject = 'ITES-3H CHESS Reverse Auction: Request Submitted';
        email11.toaddresses = new List<String>();
        email11.toaddresses.add('ites3h_reverse_auction@2yeat42pz5216f1sd41vz07qdcqq5xomj34xxh7ybtsennj2hy.61-zhsweao.na34.apex.salesforce.com');
        
        Messaging.InboundEmail email12  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env12 = new Messaging.InboundEnvelope();
        List<StaticResource> srList12 = [SELECT Id,Body FROM StaticResource WHERE Name='ItesReverseAuctionRequestSubmitted'];
        String body7 = srList12.get(0).Body.toString();
        body7 = body7.replace('Installation:', '');
        email12.plainTextBody =body7;
        email12.fromAddress ='test@test.com';
        email12.fromName = 'ABC XYZ';
        email12.subject = 'ITES-3H CHESS Reverse Auction: Request Submitted';
        email12.toaddresses = new List<String>();
        email12.toaddresses.add('ites3h_reverse_auction@2yeat42pz5216f1sd41vz07qdcqq5xomj34xxh7ybtsennj2hy.61-zhsweao.na34.apex.salesforce.com');
        
        Messaging.InboundEmail email13  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env13 = new Messaging.InboundEnvelope();
        List<StaticResource> srList13 = [SELECT Id,Body FROM StaticResource WHERE Name='ItesReverseAuctionRequestSubmitted'];
        String body8 = srList13.get(0).Body.toString();
        body8 = body8.replace('Contracting Office:', '');
        email13.plainTextBody =body8;
        email13.fromAddress ='test@test.com';
        email13.fromName = 'ABC XYZ';
        email13.subject = 'ITES-3H CHESS Reverse Auction: Request Submitted';
        email13.toaddresses = new List<String>();
        email13.toaddresses.add('ites3h_reverse_auction@2yeat42pz5216f1sd41vz07qdcqq5xomj34xxh7ybtsennj2hy.61-zhsweao.na34.apex.salesforce.com');
        
        Messaging.InboundEmail email14  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env14 = new Messaging.InboundEnvelope();
        List<StaticResource> srList14 = [SELECT Id,Body FROM StaticResource WHERE Name='ItesReverseAuctionRequestSubmitted'];
        String body9 = srList12.get(0).Body.toString();
        body9 = body9.replace('Major End Item:', '');
        email14.plainTextBody =body9;
        email14.fromAddress ='test@test.com';
        email14.fromName = 'ABC XYZ';
        email14.subject = 'ITES-3H CHESS Reverse Auction: Request Submitted';
        email14.toaddresses = new List<String>();
        email14.toaddresses.add('ites3h_reverse_auction@2yeat42pz5216f1sd41vz07qdcqq5xomj34xxh7ybtsennj2hy.61-zhsweao.na34.apex.salesforce.com');
        
        Messaging.InboundEmail email15  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env15 = new Messaging.InboundEnvelope();
        List<StaticResource> srList15 = [SELECT Id,Body FROM StaticResource WHERE Name='ItesReverseAuctionRequestSubmitted'];
        String body10 = srList15.get(0).Body.toString();
        body10 = body10.replace('\n', '');
        email15.plainTextBody =body10;
        email15.fromAddress ='test@test.com';
        email15.fromName = 'ABC XYZ';
        email15.subject = 'ITES-3H CHESS Reverse Auction: Request Submitted';
        email15.toaddresses = new List<String>();
        email15.toaddresses.add('ites3h_reverse_auction@2yeat42pz5216f1sd41vz07qdcqq5xomj34xxh7ybtsennj2hy.61-zhsweao.na34.apex.salesforce.com');
        
        ITES3HReverseAuctionEmailHandler handler = new ITES3HReverseAuctionEmailHandler();
        Test.startTest();
        Messaging.InboundEmailResult result5 = handler.handleInboundEmail(email5, env5);
        Messaging.InboundEmailResult result6 = handler.handleInboundEmail(email6, env6);
        Messaging.InboundEmailResult result7 = handler.handleInboundEmail(email7, env7);
        Messaging.InboundEmailResult result8 = handler.handleInboundEmail(email8, env8);
        Messaging.InboundEmailResult result9 = handler.handleInboundEmail(email9, env9);
        Messaging.InboundEmailResult result10 = handler.handleInboundEmail(email10, env10);
        Messaging.InboundEmailResult result11 = handler.handleInboundEmail(email11, env11);
        Messaging.InboundEmailResult result12 = handler.handleInboundEmail(email12, env12);
        Messaging.InboundEmailResult result13 = handler.handleInboundEmail(email13, env13);
        Messaging.InboundEmailResult result14 = handler.handleInboundEmail(email14, env14);
        Messaging.InboundEmailResult result15 = handler.handleInboundEmail(email15, env15);
        
        List<Task_Order__c> taskOrderList = [SELECT Id FROM Task_Order__c];
        List<Contract_Mods__c> contractModList = [SELECT Id FROM Contract_Mods__c];
        Test.stopTest();
        System.assertEquals(1, taskOrderList.size());
        System.assertEquals(4, contractModList.size());
        
    }
    
}