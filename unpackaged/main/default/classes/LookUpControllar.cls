global with sharing class LookUpControllar {
    public List<EmailTemplate> results{get;set;} // search results
    public string searchString{get;set;} // search keyword
    public String textBox{get;set;}
    public String textParam{get;set;}
    private Set<ID> emailTempleteIdList;
    
    public LookUpControllar(){
        if(Schema.sObjectType.EmailTemplate.isAccessible() && Schema.sObjectType.Questionaries_Email_Template__c.isAccessible()){
            emailTempleteIdList = setEmailTempleteIdList();
            textBox = getParameter('txt');
            textParam = getParameter('paramId');
            searchString = getParameter('lksearch');        
            runSearch(); 
        } 
    }
    
    public PageReference search(){
        runSearch();
        return null;
    }
    
    private void runSearch() {
        results = performSearch(searchString);               
    }
    
    private List<EmailTemplate > performSearch(string searchString) {        
        //String soql = 'select id, name from EmailTemplate where folder.name = \'TOMA Email Templates\' AND TemplateType =\'visualforce\' AND id NOT In :emailTempleteIdList';
        String soql = 'select id, name from EmailTemplate where folder.name = \'TOMA Email Templates\' AND id NOT In :emailTempleteIdList';
        if(searchString != '' && searchString != null)
        soql = soql +  ' AND name LIKE \'%' + String.escapeSingleQuotes(searchString) +'%\'';
        soql = soql + ' limit 25';
        return database.query(soql); 
    }
    
    public string getParameter(String getVal) {
        String temp = System.currentPageReference().getParameters().get(getVal);
        if(temp!=Null){
            return String.escapeSingleQuotes(temp);
        }
        return Null;
    }   
    
    private Set<Id> setEmailTempleteIdList(){
        Set<Id> tempIdSet = new Set<Id>();
        String SurveyId = getParameter('SurveyId');
        for(Questionaries_Email_Template__c qet : [SELECT Email_Templete__c From Questionaries_Email_Template__c WHERE Teaming_Request_Questionnaire__c=:SurveyId]){
            tempIdSet.add(qet.Email_Templete__c);
        }        
        return tempIdSet;
    }
}