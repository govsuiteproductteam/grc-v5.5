public class CIO_SP3_SBEmailParser {
    private String originalHtmlString;
    private Map<String,Integer> monthMap = new Map<String,Integer>{'january'=>1,'february'=>2,'march'=>3,'april'=>4,'may'=>5,'june'=>6,'july'=>7,'august'=>8,'september'=>9,'october'=>10,'november'=>11,'december'=>12};
    private Set<String> setSalutation = new Set<String>{'Mr.','Ms.','Mrs.','Dr.','Prof.'};
    private Id accId;
    public Task_Order__c omaParser(String parsehtmlString, String toAddress){
        Task_Order__c taskOrder = new Task_Order__c();
        List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];
        if(!conVehicalList.isEmpty()){
            taskOrder.Contract_Vehicle__c = conVehicalList[0].Id;
            if(!conVehicalList.isEmpty() && conVehicalList[0].Account_Name__c!=Null){
                accId = conVehicalList[0].Account_Name__c;
                taskOrder.Task_Order_Contracting_Organization__c = accId ;
                //System.debug('accId '+accId);
            }else{
                accId = null;
            }
        }                
        originalHtmlString = parsehtmlString.replace(' ', '');
        taskOrder.Questions_Due_Date__c = getDate(parseData('Questions Deadline'));
        
        taskOrder.Due_Date__c = getDate(parseData('Submission Deadline'));
        
        taskOrder.Release_Date__c = date.today();
        
        taskOrder.Task_Order_Title__c = truncateString(parseData('Title'),253);
        
        taskOrder.Description__c = truncateString(parseData('Description'),10000);
        
        taskOrder.Customer_Comments__c =  truncateString(parseData('Customer Comments:'),253);
        
        List<Id> contactIdSet = getContactId(parseContact('Customer CS','NITAAC CSR'));
        System.debug('contactIdSet Contract_Specialist__c '+contactIdSet);
        
        if(contactIdSet!=Null && !contactIdSet.isEmpty()){
            taskOrder.Contract_Specialist__c = contactIdSet[0];
        }
        
        List<Id> customerContactIdSet = getContactId(parseContact('Customer CO/KO','Customer COR'));
        System.debug('contactIdSet Contracting_Officer__c '+customerContactIdSet);
        if(customerContactIdSet !=Null && !customerContactIdSet.isEmpty()){
            taskOrder.Contracting_Officer__c = customerContactIdSet[0];      
        }
        
        taskOrder.Contract_Vehicle_picklist__c = 'CIOSP3 SB - CHIEF INFORMATION OFFICER SOLUTIONS AN';
        
        return taskOrder;
    }
    
    private String parseData(String labelName){
        String data;
        Integer index = originalHtmlString.indexof(labelName)+labelName.length();
        Integer strIndex = originalHtmlString.indexOf('</p>', index) +4;        
        Integer strIndex1 = originalHtmlString.indexOf('</p>', strIndex);
        data = originalHtmlString.substring(strIndex ,strIndex1);
        data = parseHashHtml(data,false);
        return data;
    }
    
    private String parseContact(String labelName, String labelName1){
        String data;
        Integer index = originalHtmlString.indexof(labelName)+labelName.length();
        Integer index2 = originalHtmlString.indexof(labelName1);
        if(index != -1 && index2!= -1){
            data = originalHtmlString.substring(index ,index2);
            data = parseHashHtml(data,true);
            System.debug('contact string data '+data);
            return data;
        }
        return '';
    }
    
    public String parseHashHtml(String parseString, Boolean isN){
         if(parseString!=Null && parseString!=''){
            string HTML_TAG_PATTERN = '<.*?>';
            pattern myPattern = pattern.compile(HTML_TAG_PATTERN);
            matcher myMatcher = myPattern.matcher(parseString);
            if(isN){
               return myMatcher.replaceAll(' ');
            }else{
               return myMatcher.replaceAll('').replaceAll('\n','');
            }
         }else{
             return Null;
         }
     }
    
    public Date getDate(String parseString){
        date created;
        try{
            List<String>arrayList = parseString.trim().split(' ');              
            created = date.newInstance(Integer.valueOf(arrayList[3].trim()), monthMap.get(arrayList[1].toLowerCase()), Integer.valueOf(arrayList[2].replace(',','').trim()));
        }catch(Exception e){
            created = Null;
        }
        return created;
    }
    
    private List<Id> getContactId(String parseString){
        System.debug('parseString = '+parseString);
        if(parseString!=Null && parseString!=''){
            LIST<Id>idList = new List<Id>();
            List<String> conInfoList = parseString.split('\n');    
            System.debug('conInfoList = '+conInfoList );    
            String unBreakString = '';
            for(String s : conInfoList){
                System.debug('s = '+s);    
                if(s != Null && s != ''){
                    unBreakString += s+' ';
                }
            }              
            List<String> emails = getEmails(unBreakString.replaceAll('[ ]+', ' ').trim());
            if(!emails.isEmpty()){
                List<Contact> Contacts = [SELECT Id, FirstName, LastName , Email FROM Contact WHERE Email IN :emails];
                if(!Contacts.isEmpty()){
                    for(String email : emails){
                        Boolean flag = false;
                        for(Contact c : Contacts){
                            if(c.email == email && unBreakString.contains(c.FirstName) && unBreakString.contains(c.LastName)){
                                idList.add(c.Id);
                                flag = true;
                            }
                        }
                        if(!flag && Contacts!=Null && Contacts.size() > 0){
                            idList.add(Contacts[0].Id);
                        }
                    }
                    return idList;
                }else{
                    Contacts = createContact(unBreakString,emails[0]);                    
                    if(Contacts != null){
                        return new List<Id>((new Map<Id,Contact>(Contacts)).keySet());
                    }
                }
            }
        }
        return new List<Id>();
    }
    private string truncateString(String trunString, Integer maxLength){
        if(trunString!=Null){
            if(trunString.length()>=maxLength){
                return trunString.subString(0,maxLength);
            }else{
                return trunString;
            }
                
        }else{
            return Null;
        }
    }
    private List<Contact> createContact(String contactParse, String emailPrimary){
        List<String> conInfo = contactParse.split('Email: '+emailPrimary);
        system.debug('list of different info '+conInfo);
        List<Contact> conList = new List<Contact>();
        for(String str : conInfo){
            if(str!=Null && str.trim()!=''){
                system.debug('list of different str '+str);
                Contact cont = new Contact();
                Integer index = str.indexof('Phone:');
                String nameString = str.substring(0, index);
                String lName='';
                for(String nameCre : nameString.split(' ')){                
                    if(setSalutation.contains(nameCre)){
                        cont.Salutation = nameCre;
                    }else if(cont.FirstName==Null || cont.FirstName==''){
                        cont.FirstName = truncateString(nameCre,79);
                    }else{
                        lName += nameCre+' ';
                    }
                }
                cont.LastName = truncateString(lName.trim(),79);
                if(cont.LastName==Null || cont.LastName==''){
                    cont.LastName = cont.FirstName;
                    cont.FirstName = '';
                }
                system.debug('list of different LastName  '+ cont.LastName);
                if(str.contains('Email:')){
                    cont.Phone = str.substring(index+7, str.indexof('Email:')-1);
                    system.debug('list of different cont.Phone '+ cont.Phone );
                    cont.Email = str.substring(str.indexof('Email:')+6);
                    system.debug('list of different cont.Email '+ cont.Email );
                    
                }else{
                    cont.Phone = str.substring(index+6).trim();
                    system.debug('list of different cont.Phone '+ cont.Phone );
                    cont.Email = emailPrimary;
                    system.debug('list of different cont.Email '+ cont.Email );
                }
                cont.AccountId = accId;
                System.debug('cont.AccountId '+cont.AccountId);
                conList.add(cont);
                System.debug('cont '+cont);
            }
        }
        if(!conList.isEmpty()){
            try{
                insert conList;// Email;
                System.debug('conList '+conList);
                return conList;
            }catch(Exception e){
                System.debug('catch Exception '+ e.getMessage()+' Line '+e.getLineNumber()+' cause '+e.getCause());
            }
        }        
        return NULL;
    }
    
    private List<string> getEmails(String parseString){
        List<String>emails = new List<String>();
        String regex = '([a-zA-Z0-9_\\-\\.]+)@(((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3}))';       
        Pattern myPattern = Pattern.compile(regex );    
        Matcher myMatcher = myPattern.matcher(parseString);
        while(myMatcher.find()){
            emails.add(myMatcher.group());
        }
        return emails;
    }
    
}