public class AttachmentWrapper{
        @AuraEnabled
        public boolean isSelected{get;set;}
        @AuraEnabled
        public boolean isFile{get;set;}
        @AuraEnabled
        public Id attachmentId {get;set;}
        @AuraEnabled
        public String name{get;set;}
        @AuraEnabled
        Public Integer length{get;set;}
        public AttachmentWrapper(Id attachmentId,String name,Integer length){
            this.attachmentId = attachmentId;
            this.name = name;
            this.length = length;
            this.isSelected = false;
            this.isFile=false;
        }
        public AttachmentWrapper(Id attachmentId,String name,Integer length,Boolean isFile){
            this.attachmentId = attachmentId;
            this.name = name;
            this.length = length;
            this.isSelected = false;
            this.isFile=isFile;
        }
    }