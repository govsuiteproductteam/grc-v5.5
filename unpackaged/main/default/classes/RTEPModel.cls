public class RTEPModel {
    public string rtepNumber{get;set;}
    public string rtepTitle;
    public Date marketResearchDueDate;
    public string rtepStatus;
    public Date tepDueDate;
    
    public RTEPModel(string rtepNumber,string rtepTitle,String marketResearchDueDate, String rtepStatus,String tepDueDate){
        try{
            if(rtepNumber != null && rtepNumber.length()>0)
                this.rtepNumber = rtepNumber;
            if(rtepTitle != null && rtepTitle.length()>0)
                this.rtepTitle = rtepTitle;
            if(marketResearchDueDate != null && marketResearchDueDate.length()==10)
                this.marketResearchDueDate = Date.parse(marketResearchDueDate);        
            if(rtepStatus != null && rtepStatus.length()>0)
                this.rtepStatus = rtepStatus;
            if(tepDueDate != null && tepDueDate.length()==10)
                this.tepDueDate = Date.parse(tepDueDate);        
        }Catch(Exception e){
            System.debug('Exception '+e.getLineNumber() +' '+e.getMessage());
        }
    }
}