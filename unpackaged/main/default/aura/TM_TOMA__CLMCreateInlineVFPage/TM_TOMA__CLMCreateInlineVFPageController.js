({
	doInit : function(component, event, helper) {
		var compoName = component.get("v.compoName");
        var oppId = "?id="+component.get("v.recId");
        
        if(typeof compoName != 'undefined'){
            component.set("v.pageUrl",'/apex/'+compoName+oppId);
        }
        
	}
})