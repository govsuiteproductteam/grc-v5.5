@IsTest
public class Test_TM_VhiclePartnerPageCompController {
    static testMethod void testVhiclePartnerPageCompController(){
        account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        account acc1 = TestDataGenerator.createAccount('TestAcc1');
        insert acc1;
        Contact con = TestDataGenerator.createContact('Smith', acc1);
        insert con;
        Contact con1 = TestDataGenerator.createContact('Smith1', acc);
        insert con1;
        Contact con2 = TestDataGenerator.createContact('Meyer', acc);
        insert con2;
        Contract_Vehicle__c conVeh = TestDataGenerator.createContractVehicle('1234567', acc);
        insert conVeh;
        Vehicle_Partner__c vehPart = TestDataGenerator.createVehiclePartner(acc, conVeh);
        insert vehPart;
        Vehicle_Partner__c vehPart1 = TestDataGenerator.createVehiclePartner(acc1, conVeh);
        insert vehPart1;
        Contract_Vehicle_Contact__c conVehContact= TestDataGenerator.createContractVehicleContact(con, conVeh);
        insert conVehContact;
        Contract_Vehicle_Contact__c conVehContact1= TestDataGenerator.createContractVehicleContact(con1, conVeh);
        insert conVehContact1;
        
        TM_VhiclePartnerPageCompController.checkLicensePermition();
        TM_VhiclePartnerPageCompController.getContractVehicalContact(conVeh.Id);
        List<VehiclePartnerWrapper1> vehPartnerList = TM_VhiclePartnerPageCompController.getVehiclePartnerList(conVeh.Id);
        System.debug(vehPartnerList.size() == 2);
        vehPartnerList = TM_VhiclePartnerPageCompController.getSearchVehiclePartnerList(conVeh.Id,'Test');
        System.debug(vehPartnerList.size() != Null);
        TM_VhiclePartnerPageCompController.deleteContractVehicalContact(conVehContact1.Id);
        vehPartnerList = TM_VhiclePartnerPageCompController.getVehiclePartnerList(conVeh.Id);
        System.debug(vehPartnerList.size() == 1);
        
        string dataStr ='['+conVehContact.Id+'SPLITDATA'+true+'SPLITDATA'+false+'SPLITPARENT';
        TM_VhiclePartnerPageCompController.saveContractVehicleContact(dataStr);
        
        string accountIds = 'undefined'+' '+acc.Id+' '+acc1.Id;
        List<ContactWrapper1> conWrapList = TM_VhiclePartnerPageCompController.showContact(accountIds, conVeh.Id);
        System.assertEquals(2, conWrapList.size());
        conWrapList = TM_VhiclePartnerPageCompController.showSearchContact(accountIds, conVeh.Id, 'Smith1');
        System.assertEquals(1, conWrapList.size());
        
        dataStr = '['+conVeh.Id+'SPLITDATA'+con2.Id+'SPLITDATA'+true+'SPLITDATA'+false+'SPLITPARENT';
        TM_VhiclePartnerPageCompController.addPartner(dataStr);
        vehPartnerList = TM_VhiclePartnerPageCompController.getVehiclePartnerList(conVeh.Id);
        System.debug(vehPartnerList.size() == 2);
        
        
    }
    
}