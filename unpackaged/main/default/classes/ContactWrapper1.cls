public with sharing class ContactWrapper1 {
     @AuraEnabled
  public Contact con {get;set;}
     @AuraEnabled
        public Boolean conStatus {get;set;}
     @AuraEnabled
        public Boolean Main_POC {get;set;}
     @AuraEnabled
        public Boolean Receives_Mass_Emails {get;set;}
        
        public ContactWrapper1(Contact con){
            this.con = con;
            conStatus=false;
            Main_POC= false;
            Receives_Mass_Emails= false;
        }
}