public class AddTeamingPartnerCNTRL {
    @AuraEnabled 
    public static List<VehiclePartnerwrapper> vehiclePartnerWrapList{get;set;}
    
     @AuraEnabled
    public static String getUserAccesibility()
    {
        return LincenseKeyHelper.checkLicensePermitionForFedTOM();
    }
    
    @AuraEnabled
    public static MotherWrapper getExistingPartners(String oppId){
        system.debug('HELLLOOO');
        return new MotherWrapper([SELECT Id,Name,Task_Order__r.Name,Partner__c,Contract_Vehicle_Id__c,Role__c,Status__c,Vehicle_Partner__r.Name,Vehicle_Partner__c,Vehicle_Partner__r.Partner__c FROM Teaming_Partner__c WHERE Task_Order__c=:oppId],getPicklistValues('TM_TOMA__Teaming_Partner__c','TM_TOMA__Status__c'));
        
    }
    @AuraEnabled
    public Static List<AllPartnerwrapper> matchAccWrapList(String searchString){
        List<AllPartnerwrapper> allPartners = new List<AllPartnerwrapper>();
        if(searchString != null && searchString !=''){
            searchString = '%'+searchString+'%';
        List<Account> accList = [SELECT Id,Name FROM Account WHERE Name LIKE :searchString ORDER BY LastViewedDate DESC LIMIT 50];
            if(accList != null &&! accList.isEmpty()){
                for(Account acc : accList){
                    allPartners.add(new AllPartnerwrapper(acc));
                }
            }
        }
        return allPartners;
    }
    @AuraEnabled
    public static List<Teaming_Partner__c> getTeamingPart(String allPartnerData,String oppId){
        system.debug('allPartnerData'+allPartnerData+'oppId'+oppId);
        String conVehicleId;
        List<Task_Order__c> fedCapOpp = [SELECT Id,Contract_Vehicle__c FROM Task_Order__c WHERE Id=:oppId];
        if(fedCapOpp != null && ! fedCapOpp.isEmpty()){
            conVehicleId = fedCapOpp[0].Contract_Vehicle__c;
        }
        if(allPartnerData != null && allPartnerData !='' && conVehicleId != null && conVehicleId != ''){
            List<AllPartnerwrapper> accWrap = (List<AllPartnerwrapper>) System.JSON.deserialize(allPartnerData,List<AllPartnerwrapper>.class);
            if(accWrap != null && ! accWrap.isEmpty()){
                List<Account> accPartnerList = new List<Account>();
                for(AllPartnerwrapper allPart : accWrap){
                    if(allPart.isSelected){
                        accPartnerList.add(allPart.acc);
                    }
                }
                
                if(accPartnerList != null && ! accPartnerList.isEmpty()){
                    System.debug('accPartnerList'+accPartnerList);
                    List<Vehicle_Partner__c> vehiclePartnerList = new List<Vehicle_Partner__c>();
                    for(Account accTemp :accPartnerList){
                        Vehicle_Partner__c vehicle = new Vehicle_Partner__c();
                        vehicle.Partner__c=accTemp.Id;
                        vehicle.Contract_Vehicle__c = conVehicleId;
                        vehicle.Role__c = 'Subcontractor';//Sub-Contractor
                        vehiclePartnerList.add(vehicle);
                    }
                    System.debug('vehiclePartnerList'+vehiclePartnerList);
                    if(vehiclePartnerList != null && ! vehiclePartnerList.isEmpty()){
                    try{
                        insert vehiclePartnerList;
                        List<Teaming_Partner__c> teamingPartnerList = new List<Teaming_Partner__c>();
                        for(Vehicle_Partner__c vehi :vehiclePartnerList){
                            Teaming_Partner__c team = new Teaming_Partner__c();
                            team.Task_Order__c = oppId;
                            team.Vehicle_Partner__c = vehi.Id;
                            team.Status__c='New';
                            teamingPartnerList.add(team);
                        }
                            System.debug('teamingPartnerList'+teamingPartnerList);
                            if(teamingPartnerList != null && ! teamingPartnerList.isEmpty()){
                                try{
                                    insert teamingPartnerList;
                                    system.debug('returned Data'+[SELECT Id,Name,Task_Order__r.Name,Partner__c,Contract_Vehicle_Id__c,Role__c,Status__c,Vehicle_Partner__r.Name FROM Teaming_Partner__c WHERE Task_Order__c=:oppId]);
                                    return [SELECT Id,Name,Task_Order__r.Name,Partner__c,Contract_Vehicle_Id__c,Role__c,Status__c,Vehicle_Partner__r.Name FROM Teaming_Partner__c WHERE Task_Order__c=:oppId];
                                }catch(Exception e){
                                    system.debug('Exception'+e);
                                    return [SELECT Id,Name,Task_Order__r.Name,Partner__c,Contract_Vehicle_Id__c,Role__c,Status__c,Vehicle_Partner__r.Name FROM Teaming_Partner__c WHERE Task_Order__c=:oppId];
                                }
                            }
                        
                    }catch(Exception e){
                        System.debug('Exception'+e);
                        return [SELECT Id,Name,Task_Order__r.Name,Partner__c,Contract_Vehicle_Id__c,Role__c,Status__c,Vehicle_Partner__r.Name FROM Teaming_Partner__c WHERE Task_Order__c=:oppId];
                     }
                        
                    }
                }
            }
        }
        return [SELECT Id,Name,Task_Order__r.Name,Partner__c,Contract_Vehicle_Id__c,Role__c,Status__c,Vehicle_Partner__r.Name FROM Teaming_Partner__c WHERE Task_Order__c=:oppId];
    }
    @AuraEnabled
    public static List<AllPartnerwrapper> showAllPartners(){
        List<AllPartnerwrapper> allPartnerList =new List<AllPartnerwrapper>();
        List<Account> tempAccList = [SELECT Id,Name FROM Account LIMIT 50];
        if(tempAccList != null &&! tempAccList.isEmpty()){
            for(Account acc : tempAccList){
                allPartnerList.add(new AllPartnerwrapper(acc));
            }
        }
        return allPartnerList;
    }
     @AuraEnabled
    public static List<Teaming_Partner__c> dummyUpdateStatus(List<String> statusVal,String oppId){
        system.debug('str1'+statusVal+'str2'+oppId);
        return updateStatus(statusVal,oppId);
        
    }
    
    public static List<Teaming_Partner__c> updateStatus(List<String> statusVal,String oppId){
          system.debug('statusVal'+statusVal+' oppId'+oppId);
        if(statusVal != null && ! statusVal.isEmpty()){
            Map<String,String> idVsStatusMap = new Map<String,String>();
            for(String str : statusVal){
                if(str != null && str != '' && str.contains('#####')){
                    idVsStatusMap.put(str.split('#####')[0],str.split('#####')[1]);
                }
            }
            system.debug('idVsStatusMap'+idVsStatusMap);
        List<Teaming_Partner__c>tempList =[SELECT Id,Status__c FROM Teaming_Partner__c WHERE Id=:idVsStatusMap.keySet()];
        if(tempList != null && ! tempList.isEmpty()){
            for(Teaming_Partner__c team : tempList){
                team.Status__c = idVsStatusMap.get(team.Id);
            }
            
            try{
                update tempList;
            }catch(Exception e){
                system.debug('Exception '+e);
            }
        }
        }
        return [SELECT Id,Name,Task_Order__r.Name,Partner__c,Contract_Vehicle_Id__c,Role__c,Status__c,Vehicle_Partner__r.Name FROM Teaming_Partner__c WHERE Task_Order__c=:oppId];
      
    }
       @AuraEnabled
    public static List<Teaming_Partner__c> deleteTeam(String teamId,String oppId){
        if(teamId != null && teamId !=''){
            List<Teaming_Partner__c> teamToDelete = [SELECT Id FROM Teaming_Partner__c WHERE Id=:teamId];
            system.debug('teamToDelete '+teamToDelete);
            if(teamToDelete != null && ! teamToDelete.isEmpty()){
                try{
                    delete teamToDelete;
                    return [SELECT Id,Name,Task_Order__r.Name,Partner__c,Contract_Vehicle_Id__c,Role__c,Status__c,Vehicle_Partner__r.Name FROM Teaming_Partner__c WHERE Task_Order__c=:oppId];
                }catch(Exception e){
                    system.debug('Exception during deletion'+e);
                }
            }
        }
        return [SELECT Id,Name,Task_Order__r.Name,Partner__c,Contract_Vehicle_Id__c,Role__c,Status__c,Vehicle_Partner__r.Name FROM Teaming_Partner__c WHERE Task_Order__c=:oppId];
        
    }
    
    @AuraEnabled 
    public static List<VehiclePartnerwrapper> getTeamingPartnerList(String tOrderId){
        system.debug('tOrderId '+tOrderId);
        If(tOrderId != null && tOrderId !=''){
             Task_Order__c taskOrder = [Select Id,Contract_Vehicle__c From Task_Order__c where id =: tOrderId ];
           Id conVehicleId = taskOrder.Contract_Vehicle__c;
            List<Vehicle_Partner__c> vehiclePartnerList = new List<Vehicle_Partner__c>();
            List<Teaming_Partner__c> teamingPartnerList = new List<Teaming_Partner__c>();
        
            
            if(conVehicleId != null){
                List<Vehicle_Partner__c> inputVehiclePartnerList = [SELECT id, name , Contract_Vehicle__c,Point_of_Contact__r.name,Role__c,Partner__r.name FROM Vehicle_Partner__c WHERE Contract_Vehicle__c =: conVehicleId];    
                Map<Id,Vehicle_Partner__c> vehiclePartnerMap = new Map<ID,Vehicle_Partner__c>(inputVehiclePartnerList );
                
                List<Teaming_Partner__c> teminPartList = [Select Id,Vehicle_Partner__c From Teaming_Partner__c where Task_Order__c= : tOrderId ];
                Set<Id> vehiclePartnerIdSet = new Set<Id>();
                for(Teaming_Partner__c teamingPartner : teminPartList ){
                    vehiclePartnerIdSet.add(teamingPartner.Vehicle_Partner__c );
                }
                
               /* for(Id vehicalePartnerId : vehiclePartnerIdSet){
                   vehiclePartnerMap.remove(vehicalePartnerId );
                    
                }*/
                
                for(Id vehPartnerId : vehiclePartnerMap.keySet() ){
                    if(!vehiclePartnerIdSet.contains(vehPartnerId )){
                         Vehicle_Partner__c vehiclePartner = vehiclePartnerMap.get(vehPartnerId );
                         if(vehiclePartner != null){
                            vehiclePartnerList.add(vehiclePartner );
                         }
                    }
                }
                
                system.debug('vehiclePartnerList == '+ vehiclePartnerList );
                 try{
                   if(vehiclePartnerList != null && vehiclePartnerList .size()>0){
                       vehiclePartnerWrapList = new List<VehiclePartnerwrapper>();
                             for(Vehicle_Partner__c vehPartner : vehiclePartnerList){
                                 
                                 VehiclePartnerwrapper vehPartWrap = new VehiclePartnerwrapper();
                                 vehPartWrap.vehiclePartner = vehPartner ;
                                 vehPartWrap.status = false;
                                 vehiclePartnerWrapList.add(vehPartWrap);
                             }
                   }
                   
                 }
                 catch(Exception e){
                      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, e.getMessage()));
                 }
             }  
            // teamingPartnerList = [select id,name, Vehicle_Partner__r.name,Vehicle_Partner__r.point_of_contact__c, Vehicle_Partner__r.role__c FROM Teaming_Partner__c];
             system.debug('vehiclePartnerWrapList ==='+vehiclePartnerWrapList );
        return vehiclePartnerWrapList;
        }
        return null;
    }
    
   @AuraEnabled
    public static List<Teaming_Partner__c> addPartner(List<String> vehiclPartnerIds,String oppId){
        if(vehiclPartnerIds != null && vehiclPartnerIds.size()>0){
        List<Teaming_Partner__c> teamingPartnerList = new List<Teaming_Partner__c>();
        List<TeamingPartnerwrapper> teamingPartnerwraList = new List<TeamingPartnerwrapper>();
            for(String vehPartner : vehiclPartnerIds){
                TeamingPartnerwrapper teamingPartnerW = new TeamingPartnerwrapper(new Teaming_Partner__c (),false);
                teamingPartnerW.teamingPartner.Vehicle_Partner__c = vehPartner;
                teamingPartnerW.teamingPartner.Task_Order__c = oppId ;
                teamingPartnerW.flag = true;
                teamingPartnerW.teamingPartner.Status__c = 'New';
                teamingPartnerwraList.add(teamingPartnerW );
            }
            
            if(teamingPartnerwraList.size()>0){
                 for(TeamingPartnerwrapper teamingPartnerW: teamingPartnerwraList){
                     teamingPartnerList.add(teamingPartnerW.teamingPartner);
                 }
             }
            try{
                if(teamingPartnerList.size()>0){
                    system.debug('teamingPartnerList'+teamingPartnerList);
                 upsert teamingPartnerList;
                    shareOpportunity(teamingPartnerList,oppId);
                    return [SELECT Id,Name,Task_Order__r.Name,Partner__c,Contract_Vehicle_Id__c,Role__c,Status__c,Vehicle_Partner__r.Name FROM Teaming_Partner__c WHERE Task_Order__c=:oppId];
                 //teamingPartnerList;
                
                }
            }
            catch(exception e){
                 system.debug('Exception'+e);
            }
        }
        return [SELECT Id,Name,Task_Order__r.Name,Partner__c,Contract_Vehicle_Id__c,Role__c,Status__c,Vehicle_Partner__r.Name FROM Teaming_Partner__c WHERE Task_Order__c=:oppId];
    }
        
        private static void shareOpportunity(List<Teaming_Partner__c> teamingPartnerList,String oppId){
        List<Task_Order__Share> task_OrderList =  new  List<Task_Order__Share>();
    
         Set<Id> contactIdSet = new Set<Id>();
         Set<Id> accountIdSet = new Set<Id>();
         Map<Id,Teaming_Partner__c> teaminPartnerMap = new Map<Id,Teaming_Partner__c>(teamingPartnerList);
         List<Task_Order__c> tempFedList= [SELECT Contract_Vehicle__c FROM Task_Order__c WHERE Id=:oppId];
            String conVehicleId = '';
            if(tempFedList != null && ! tempFedList.isEmpty()){
                conVehicleId = tempFedList[0].Contract_Vehicle__c;
            }
         //Need to change logic for this contact.
         
         for(Teaming_Partner__c teamingPartner : [Select Id,Vehicle_Partner__r.Point_Of_Contact__c,Vehicle_Partner__r.Partner__c From Teaming_Partner__c where id IN: teaminPartnerMap.keySet()] ){
             //contactIdSet.add(teamingPartner.Vehicle_Partner__r.Point_Of_Contact__c);
             accountIdSet.add(teamingPartner.Vehicle_Partner__r.Partner__c);
         }
         
         List<Task_Order_Contact__c> taskOrderContactList = new List<Task_Order_Contact__c>();
       //  System.debug('query '+[Select Id,Contact__c,Main_POC__c,Receives_Mass_Emails__c From Contract_Vehicle_Contact__c where contact__r.AccountId IN : accountIdSet and Contract_Vehicle__c =: conVehicleId AND (Main_POC__c =true OR Receives_Mass_Emails__c = true)]);
         for(Contract_Vehicle_Contact__c contractVehicleContract : [Select Id,Contact__c,Main_POC__c,Receives_Mass_Emails__c From Contract_Vehicle_Contact__c where contact__r.AccountId IN : accountIdSet and Contract_Vehicle__c =: conVehicleId AND (Main_POC__c =true OR Receives_Mass_Emails__c = true) AND Contact__c NOT IN (Select Contact__c From Task_Order_Contact__c where Task_Order__c =: oppId)]){
         
             contactIdSet.add(contractVehicleContract.Contact__c );
              
             Task_Order_Contact__c taskOrderContact = new Task_Order_Contact__c();
             taskOrderContact.Contact__c = contractVehicleContract.Contact__c;
             taskOrderContact.Task_Order__c = oppId ;
             taskOrderContact.Main_POC__c = contractVehicleContract.Main_POC__C;
             taskOrderContact.Receives_Mass_Emails__c = contractVehicleContract.Receives_Mass_Emails__c ;
             taskOrderContactList.add(taskOrderContact);
             System.debug(' taskOrderContact '+taskOrderContact);
         }
         System.debug('taskOrderContactList '+taskOrderContactList);
         if(!taskOrderContactList.isEmpty()){            
             DMLManager.insertAsUser(taskOrderContactList);             
         }
         
         List<User> userList = [Select Id From User where contactId IN : contactIdSet];
         for(User usr :userList ){
             Task_Order__Share toshare= new  Task_Order__Share();
             toshare.ParentId = oppId  ;             
             toshare.UserOrGroupId = usr.id;             
             toshare.Accesslevel='Read';                                      
             task_OrderList.add(toshare);
         }
         
         if(!task_OrderList.isEmpty()){
            // insert task_OrderList;
             DMLManager.insertAsUser(task_OrderList);
         }
    }
        
    public static List<PickFieldwrap> getPicklistValues(String ObjectApi_name,String Field_name){ 
        List<PickFieldwrap> lstPickvals=new List<PickFieldwrap>();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectApi_name);//From the Object Api name retrieving the SObject
        Sobject Object_name = targetType.newSObject();
        Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
        List<Schema.PicklistEntry> pick_list_values = field_map.get(Field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
        for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
            lstPickvals.add(new PickFieldwrap(''+a.getLabel(),''+a.getValue()));//add the value  to our final list
        }
        system.debug('PICKKLLLIIISSSTTT'+lstPickvals);
        return lstPickvals;
    }
    
    /*public class TeamingPartnerwrapper {
       public Teaming_Partner__c teamingPartner{get;set;}
       public boolean flag {get;set;} 
       
        public TeamingPartnerwrapper(Teaming_Partner__c teamingPartner ,boolean status){
           this.teamingPartner =teamingPartner;
           this.flag = flag ;
          
           
       } 
   } 
   
   public class VehiclePartnerwrapper {
       @AuraEnabled
       public Vehicle_Partner__c vehiclePartner{get;set;}
       @AuraEnabled
       public boolean status {get;set;}
      
       
     //  public VehiclePartnerwrapper (Vehicle_Partner__c vehiclePartner,boolean status){
        //   this.vehiclePartner=vehiclePartner;
        //   this.status = status;
           
      // } 
   }
    
    
    public Class MotherWrapper{
        @AuraEnabled
        List<Teaming_Partner__c> teamingPartnerList{get;set;}
        @AuraEnabled
        List<PickFieldwrap> pickListVals{get;set;}
        
        public MotherWrapper(List<Teaming_Partner__c> teamingPartnerList,List<PickFieldwrap> pickListVals){
            this.teamingPartnerList = teamingPartnerList == null ? new List<Teaming_Partner__c>() : teamingPartnerList;
            this.pickListVals = pickListVals == null ? new List<PickFieldwrap>() : pickListVals;
        }
    }
    public Class AllPartnerwrapper{
        @AuraEnabled
        public Account acc{get;set;}
        @AuraEnabled
        public Boolean isSelected{get;set;}
        public AllPartnerwrapper(Account acc){
            this.acc =acc;
            this.isSelected = false;
        }
    }*/
}