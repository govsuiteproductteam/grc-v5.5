({
	getContractVehical : function(component, event, helper) {
		 var action = component.get("c.getContractVehicle");
        
        action.setParams({
            contractVehicleId : component.get("v.recordId")
        });
        
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
            // alert(a.getReturnValue().OwnerId);
             //   component.set("v.recordTypeId", a.getReturnValue().RecordTypeId);
                component.set("v.contractVehicle", a.getReturnValue());
               // component.set("checkSpinner1",'No');
              //alert('@@@'+component.get("v.contractVehicle").Name); 
            } else if (a.getState() === "ERROR") {
                $A.log("Errors", a.getError());
            }
        });
        
        $A.enqueueAction(action);
       

	},
    deleteContractVehical : function(component, event, helper) {
        // alert("deletebid");
		 var action = component.get("c.delContractVehicle");
        var msg ='Contract Vehical "'+component.get("v.contractVehicle").Name+'" Deleted Succesfully;'
       action.setParams({
         contractVehicleId : component.get("v.recordId")
      });
        
        action.setCallback(this, function(a) {
           if (a.getState() === "SUCCESS") {
              //window.location="https://tm-lightning-dev-ed.lightning.force.com/one/one.app?source=aloha#/sObject/a00/home?t=1468404871989";
             var toastEvent = $A.get("e.force:showToast");
                   toastEvent.setParams({
                        "title": "Success!",
                        "message": msg,
                        "type":"success"
                    });
                    toastEvent.fire(); 
          window.history.back();
          } else if (a.getState() === "ERROR") {
            $A.log("Errors", a.getError());
           }
      });
        
    $A.enqueueAction(action);
     // alert(msg);

	},
      showSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');
        var evt = spinner.get("e.toggle");
        evt.setParams({ isVisible : true });
        evt.fire();    
    },
    
    hideSpinner : function (component, event, helper) {
       var spinner = component.find('spinner');
       var evt = spinner.get("e.toggle");
       evt.setParams({ isVisible : false });
       evt.fire();    
    },
    doInitAtAGlanceData : function (component, event, helper) {
        component.set("v.checkSpinner1","Yes");
                            component.find("atAGlanceContainer").set("v.body", []);
                            // alert('Inside doInitAtAGlanceData1');
                            var action = component.get("c.getMetadata");
                                //alert(component.get("v.recordId"));
                                action.setParams({
                                recordId : component.get("v.recordId")
                                });
                                action.setCallback(this, function(a) {
                                if (a.getState() === "SUCCESS") {
                                var contractVehicle=component.get("v.contractVehicle");
                               // alert("SUCCESS "+a.getReturnValue());
                                var AtAGlanceWrap = a.getReturnValue();
                                    
                             if(AtAGlanceWrap.length > 0){
                                var viewFieldDataMap=JSON.parse(JSON.stringify(helper.viewPageFieldsMap));
                             //  alert("length "+AtAGlanceWrap.length);
                                for(var j=0; j<AtAGlanceWrap.length;j++ ){
                               // alert(AtAGlanceWrap[j].atAGlance.FieldLabel__c);
                            var fieldDataMap={};
                           // alert(AtAGlanceWrap[j].atAGlance.Is_Component__c)
                            
                            if(AtAGlanceWrap[j].atAGlance.TM_TOMA__Is_Component__c === true){
                                fieldDataMap["recordId"]=component.getReference("v.recordId");
                                fieldDataMap["colSpan"]=AtAGlanceWrap[j].atAGlance.TM_TOMA__ColSpan__c;
                                //fieldDataMap["isComponent"]=true;
                                
                            }else{
                                //fieldDataMap["isComponent"]=false;
                               // alert("else"+AtAGlanceWrap[j].atAGlance.FieldLabel__c);
                                fieldDataMap["aura:id"] = component.getGlobalId();
                                fieldDataMap["label"] = AtAGlanceWrap[j].atAGlance.TM_TOMA__FieldLabel__c;
                                fieldDataMap["access"] = "Global";
                                
                                //Component Specific Attributes
                                if(AtAGlanceWrap[j].fieldType == "STRING"){
                                    
                                    fieldDataMap["value"] = contractVehicle[AtAGlanceWrap[j].atAGlance.TM_TOMA__FieldApiName__c];
                                }else if(AtAGlanceWrap[j].fieldType == "DATETIME"){
                                    //fieldDataMap["class"] = "slds-input";
                                    fieldDataMap["value"] = contractVehicle[AtAGlanceWrap[j].atAGlance.TM_TOMA__FieldApiName__c];
                                    fieldDataMap["displayDatePicker"] = "true";
                                }
                                    /*else if(AtAGlanceWrap[j].fieldType == "REFERENCE"){
                                    //componentAttributesConfigMap["class"] = "slds-input";
                                    //componentAttributesConfigMap["value"] = fieldWrapperList[i].Bid[fieldWrapperList[i].fieldPath];
                                  //  alert(fieldWrapperList[i].Bid[fieldWrapperList[i].lookUpType]);
                                    fieldDataMap["sObjectType"] = fieldWrapperList[i].contractVehicle[fieldWrapperList[i].lookUpType];
                                    //componentAttributesConfigMap["lookUpId"] = "0033600000GR2SgAAL";
                                }*/
                                
                                 else if(AtAGlanceWrap[j].fieldType == "REFERENCE"){
                                                    if(contractVehicle[AtAGlanceWrap[j].atAGlance.TM_TOMA__FieldApiName__c] != undefined){
                                                        fieldDataMap["lookUpId"] =contractVehicle[AtAGlanceWrap[j].atAGlance.TM_TOMA__FieldApiName__c];
                                                    }else{
                                                        fieldDataMap["lookUpId"] = "";
                                                    }
                                //  alert("lookUpName "+AtAGlanceWrap[j].lookUpName);
                                                    fieldDataMap["sObjectType"] =AtAGlanceWrap[j].lookupName;
                                                }    
                                
                                else if(AtAGlanceWrap[j].fieldType == "PICKLIST"){
                                    fieldDataMap["class"] = "slds-input";
                                    fieldDataMap["value"] = contractVehicle[AtAGlanceWrap[j].atAGlance.TM_TOMA__FieldApiName__c];
                                }else if(AtAGlanceWrap[j].fieldType == "MULTIPICKLIST"){
                                    fieldDataMap["class"] = "slds-input";
                                   // fieldDataMap["fieldName"] = fieldWrapperList[i].fieldPath;
                                    fieldDataMap["value"] = contractVehicle[AtAGlanceWrap[j].atAGlance.TM_TOMA__FieldApiName__c];
                                }
                                    else if(AtAGlanceWrap[j].fieldType == "BOOLEAN"){
                                    fieldDataMap["class"] = "slds-input";
                                    fieldDataMap["value"] = contractVehicle[AtAGlanceWrap[j].atAGlance.TM_TOMA__FieldApiName__c];
                                }else if(AtAGlanceWrap[j].fieldType == "ID"){
                                    fieldDataMap["class"] = "slds-input";
                                    fieldDataMap["value"] = contractVehicle[AtAGlanceWrap[j].atAGlance.TM_TOMA__FieldApiName__c];
                                }else if(AtAGlanceWrap[j].fieldType == "DATE"){
         //  alert(contractVehicle[AtAGlanceWrap[j].atAGlance.FieldApiName__c]);
                                    fieldDataMap["class"] = "slds-input";
                                    fieldDataMap["value"] = contractVehicle[AtAGlanceWrap[j].atAGlance.TM_TOMA__FieldApiName__c];
                                    fieldDataMap["displayDatePicker"] = "true";
                                }else if(AtAGlanceWrap[j].fieldType == "CURRENCY"){
            //    alert(contractVehicle[AtAGlanceWrap[j].atAGlance.FieldApiName__c]);
                                    fieldDataMap["class"] = "slds-input";
                                    fieldDataMap["value"] = contractVehicle[AtAGlanceWrap[j].atAGlance.TM_TOMA__FieldApiName__c];
                                }else if(AtAGlanceWrap[j].fieldType == "TEXTAREA"){
                                    fieldDataMap["class"] = "slds-input";
                                    fieldDataMap["value"] = contractVehicle[AtAGlanceWrap[j].atAGlance.TM_TOMA__FieldApiName__c];
                                }else if(AtAGlanceWrap[j].fieldType == "PERCENT"){
                                    fieldDataMap["class"] = "slds-input";
                                    fieldDataMap["value"] = contractVehicle[AtAGlanceWrap[j].atAGlance.TM_TOMA__FieldApiName__c];
                                }else if(AtAGlanceWrap[j].fieldType == "DOUBLE"){
                                    fieldDataMap["class"] = "slds-input";
                                    fieldDataMap["value"] = contractVehicle[AtAGlanceWrap[j].atAGlance.TM_TOMA__FieldApiName__c];
                                }
                                fieldDataMap["class"] ="bidbox1";//"atAGlanceField";
                            }
                         /*   if(AtAGlanceWrap[j].atAGlance.Is_Component__c === true){
                                $A.createComponents([["aura:HTML", { tag: "div",HTMLAttributes: {"id": "mainDiv","class":AtAGlanceWrap[j].atAGlance.ColSpan__c===undefined? "slds-col--padded slds-size--6-of-12":"slds-col--padded slds-size--"+AtAGlanceWrap[j].atAGlance.ColSpan__c+"-of-12","style":"color: "+AtAGlanceWrap[j].atAGlance.Color__c} }],
                                                     ["c:"+AtAGlanceWrap[j].atAGlance.FieldApiName__c,{"recordId":component.getReference("v.recordId"), "colSpan":AtAGlanceWrap[j].atAGlance.ColSpan__c}]],
                                                    function(data,status){
                                                        if(component.isValid() && status=="SUCCESS"){
                                                            // var modalBody=component.find("modalDataDiv").get("v.body");
                                                            // modalBody.push(data);
                                                            //component.find("atAGlanceContainer").get("v.body");
                                                            data[0].set("v.body",data[1]);
                                                            var body = component.find("atAGlanceContainer").get("v.body");
                                                            body.push(data[0]);
                                                            component.find("atAGlanceContainer").set("v.body", body);  
                                                        }
                                                    });
                            }
                            else{*/
                                $A.createComponent(
                                    "c:innerHeaderComponent",{"componentName":AtAGlanceWrap[j].atAGlance.TM_TOMA__Is_Component__c === true ? "TM_TOMA:"+AtAGlanceWrap[j].atAGlance.TM_TOMA__FieldApiName__c:viewFieldDataMap[AtAGlanceWrap[j].fieldType ],"componentBody":fieldDataMap,"colSpan":AtAGlanceWrap[j].atAGlance.TM_TOMA__ColSpan__c,"imgId":AtAGlanceWrap[j].atAGlance.TM_TOMA__Document_Id__c ,"fieldColor":AtAGlanceWrap[j].atAGlance.TM_TOMA__Color__c,"fieldLabel":AtAGlanceWrap[j].atAGlance.TM_TOMA__FieldLabel__c,"isComponent":AtAGlanceWrap[j].atAGlance.TM_TOMA__Is_Component__c}
                                    ,function(dynamicCmp){
                                       // alert("Inside Function ");
                                        var body = component.find("atAGlanceContainer").get("v.body");
                                        body.push(dynamicCmp);
                                        component.find("atAGlanceContainer").set("v.body", body);
                                         component.set("v.checkAtAGlance","Yes"); 
                                        component.set("v.checkSpinner1","No");
                                    })
                          //  }
                        }  
                          }
                                    else{
                                        component.set("v.checkSpinner1","No");
                                        component.set("v.checkAtAGlance","No");
                                    }

                    }
                });
                $A.enqueueAction(action);
  
    },    
   /*  componentTypeConfigObj: {
        "DATETIME" : "ui:inputDateTime",
        "REFERENCE" : "TM_TOMA:GenericLookUpComponent",
        "STRING" : "ui:inputText",
        "PICKLIST":"TM_TOMA:RD_DynamicFieldsCreatePicklist",
        "MULTIPICKLIST":"TM_TOMA:RD_DynamicFieldsCreateMultiPicklistCMP",
        "BOOLEAN" :"ui:inputCheckbox",
        "ID" : "ui:inputText",
        "DATE" : "ui:inputDate",
        "CURRENCY" : "ui:inputCurrency",
        "TEXTAREA" : "ui:inputTextArea",
        "PERCENT" : "ui:inputText",
        "DOUBLE" : "ui:inputNumber"
    },*/
     viewPageFieldsMap: {
						"DATETIME" : "ui:outputDateTime",
						"REFERENCE" : "c:TM_ShowLookupField",// "ui:outputText"
						"STRING" : "ui:outputText",
						"PICKLIST":"ui:outputText",
						"BOOLEAN" :"ui:outputCheckbox",
						"ID" : "ui:outputText",
						"DATE" : "ui:outputDate",
						"CURRENCY" : "ui:outputCurrency",
						"TEXTAREA" : "ui:outputTextArea",
						"PERCENT" : "ui:outputText",
						"DOUBLE" : "ui:outputNumber",
                        "MULTIPICKLIST":"ui:outputTextArea"
					}
})