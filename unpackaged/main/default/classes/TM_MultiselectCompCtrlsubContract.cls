public with sharing class TM_MultiselectCompCtrlsubContract {
    public String selectedValues{get;set;}
    public String picApiName{get;set;}
    public String oriFieldId{get;set;}
    public String dependantDataNew{get; set;}
    
    public String selectedIds{get;set;}
    public String jsonStr{get;set;}
        
    public List<String> getPickListOption(){
        jsonStr='{';
        List<String> pickListOption = new List<String>();
        Set<String> selectedVals;
        if(selectedValues!=Null){
             selectedVals = new Set<String>(selectedValues.split(';'));
        }else{
            selectedVals = new Set<String>();
        }
        selectedIds = '';
        //Map<String,String> jsIdValueMap = new Map<String,String>();
        Map<String,Schema.SObjectField> M =  Schema.SObjectType.SubContract__c.fields.getMap();
        if(M.containsKey(picApiName.trim())){
            Schema.SObjectField field = M.get(picApiName.trim());
            Schema.DescribeFieldResult fldType = field.getDescribe();
            List<Schema.PicklistEntry> ple = fldType.getPicklistValues();
            Integer i = 0;
            for( Schema.PicklistEntry f : ple){
                pickListOption.add(f.getValue().trim());
                jsonStr += '"'+f.getValue().trim()+'":"menu-'+oriFieldId+'-'+i+'",';
                if(selectedVals.contains(f.getValue().trim())){
                    selectedIds+='menu-'+oriFieldId+'-'+i+';';
                }
                i++;
            }
            
            jsonStr = jsonStr.removeEnd(',') + '}';
        }
        return pickListOption;
    }

}