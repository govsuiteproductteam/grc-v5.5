({
	doInit : function(component, event, helper) {
		var compoName = component.get("v.compoName");
        console.log("compoName"+compoName);
        if(typeof compoName != 'undefined'){
            $A.createComponent(compoName,{"recordId":component.get("v.recordId") },function(compo){
                var body=component.get("v.body");
                body.push(compo);
                component.set("v.body",body);
            });
        }
        
	}
})