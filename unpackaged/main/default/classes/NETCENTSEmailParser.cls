public class NETCENTSEmailParser {
    
    public String taskOrderNumber;
    public String taskOrderTitle;
    public String proposalDueDate;
    public String proposalDueDateRaw;
    private Datetime proposalDueDateTime;    
    public Date startDate;
    public String programSummary;
    private Contact con;
    private String quantity;
    
    public void parse(Messaging.InboundEmail email){
        try{
            String emailBody = email.plainTextBody;
            if(String.isNotBlank(emailBody)){
                // RFQ Information
                String rfqInfo = emailBody.substringBetween('RFQ Information:','To respond to this request:');
                //System.debug('rfqInfo = '+rfqInfo);
                if(String.isNotBlank(rfqInfo)){
                    String[] rfqInfoSplit = rfqInfo.split('\n');
                    //System.debug('rfqInfoSplit=='+rfqInfoSplit);
                    if(rfqInfoSplit != null && !rfqInfoSplit.isEmpty()){
                        for(String eachRFQInfo : rfqInfoSplit){
                            //System.debug('each RFQ Info = ' + eachRFQInfo);
                            eachRFQInfo = eachRFQInfo.trim();
                            if(eachRFQInfo.contains('RFQID: ')){
                                taskOrderNumber = getRefinedData(eachRFQInfo);
                                System.debug('Task Order Number = ' + taskOrderNumber);
                            }else if(eachRFQInfo.contains('RFQ Name: ')){
                                taskOrderTitle = getRefinedData(eachRFQInfo);
                                System.debug('Task Order Title = ' + taskOrderTitle);
                            }else if(eachRFQInfo.contains('Vendor Response Due Date: ')){
                                proposalDueDateRaw = getRefinedData(eachRFQInfo);
                                DateTimeHelper dtHelper = new DateTimeHelper();
                                proposalDueDateTime =  dtHelper.getDateTimeSpecialCase(proposalDueDateRaw);
                                System.debug('proposalDueDateTime=='+proposalDueDateTime);
                                System.debug('Proposal Due Date = ' + proposalDueDate);
                            }else if(eachRFQInfo.contains('Desired Delivery Date: ')){     
                                //startDate = eachRFQInfo.split(': ')[1];
                                startDate = getDate(getRefinedData(eachRFQInfo));
                                System.debug('Start Date = ' + startDate);
                            }
                        }
                    }
                    if(emailBody.contains('Quantity:')){
                        try{
                            quantity = emailBody.substringBetween('Quantity:', 'Description:').trim();    
                        }catch(Exception e){
                            
                        }                        
                    }
                }
                
                String programSummaryRaw = emailBody.substringBetween('Description:','To respond to this request:');
                if(String.isNotBlank(programSummaryRaw)) {
                    programSummary = programSummaryRaw.normalizeSpace();
                }
                System.debug('Program Summary = ' + programSummary);
                
                // Customer Information
                String contactInfo = emailBody.substringBetween('Customer Information:','RFQ Information:');
                //System.debug('contactInfo = '+contactInfo);
                if(String.isNotBlank(contactInfo)) {
                    createContact(contactInfo.trim());
                }
            }
        }catch(Exception e){
            System.debug(''+e.getMessage()+'\t'+e.getLineNumber());
        }
        
    }
    
    public String getRefinedData(String rawData){
        String[] rawDataSplit = rawData.split(': ');
        if(rawDataSplit != null && rawDataSplit.size() == 2){
            return rawDataSplit[1].trim();
        }
        return '';
    }
    
    public Task_Order__c getTaskOrder(String toAddress){ 
        if(taskOrderNumber==null){
            return null;
        }        
        Task_Order__c taskOrder = new Task_Order__c();
        List<Contract_Vehicle__c> conVehicalList = [SELECT Id FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];        
        if(!conVehicalList.isEmpty()){
            taskOrder.Contract_Vehicle__c = conVehicalList[0].Id; // Contract Vehicle
            taskOrder.Name = taskOrderNumber; //Task Order Number
            
            if(taskOrderTitle != null)
                taskOrder.Task_Order_Title__c = taskOrderTitle; //Task Order Title
            
            if(proposalDueDateTime !=null){            
                taskOrder.Due_Date__c = proposalDueDateTime.date(); // Due Date
                taskOrder.Proposal_Due_DateTime__c = proposalDueDateTime; // Proposal Due Date/Time
            }
            
            if(startDate != null)
                taskOrder.Start_Date__c = startdate; // Start Date
            
            if(programSummary != null)
                taskOrder.Description__c = programSummary; // Description
            
            if(quantity != null)
                taskOrder.Quantity__c = quantity;
            
            taskOrder.Release_Date__c = Date.today(); // RFP/TO Release Date
            
            taskOrder.Contract_Vehicle_picklist__c = 'NETCENTS - AIR FORCE NETWORK CENTRIC SOLUTIONS';
            
            try{
                if(con.id == null)
                    DMLManager.insertAsUser(con); 
            }catch(Exception e){
                System.debug(''+e.getMessage()+'\t'+e.getLineNumber());
            }
            
            if(con.id!= null) {           
                taskOrder.Buyer__c = con.Id;
            }
            
            return taskOrder;
        }else{
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
        }
    }
    
    public Task_Order__c updateTaskOrder(Task_Order__c taskOrder, String toAddress){
        List<Contract_Vehicle__c> conVehicalList = [SELECT Id FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];
        
        if(!conVehicalList.isEmpty()){
            taskOrder.Contract_Vehicle__c = conVehicalList[0].Id; // Contract Vehicle
            
            if(taskOrderTitle != null)
                taskOrder.Task_Order_Title__c = taskOrderTitle; //Task Order Title
            
            if(proposalDueDateTime !=null){            
                taskOrder.Due_Date__c = proposalDueDateTime.date(); // Due Date
                taskOrder.Proposal_Due_DateTime__c = proposalDueDateTime; // Proposal Due Date/Time
            }
            
            if(startDate != null)
                taskOrder.Start_Date__c = startdate; // Start Date
                
            if(quantity != null)
                taskOrder.Quantity__c = quantity;
            
            if(programSummary != null)
                taskOrder.Description__c = programSummary; // Description
            
            
            try{
                if(taskOrder.Customer_Agency__c != null){
                    con.AccountId = taskOrder.Customer_Agency__c; // Set Customer Organization of the Task Order as an Account for Contact
                    System.debug('Customer Organization = ' + con.AccountId);
                    //DMLManager.upsertAsUser(con);
                    //upsert con;
                    if(con.Id == null){
                        // if Task Order with Customer Organization is not having a Contact or if a different Contact(i.e. a Contact with different Email Id) is found
                        DMLManager.insertAsUser(con);
                    }else{
                        // if Task Order with Customer Organization is having a Contact, then update that Contact
                        DMLManager.updateAsUser(con);
                    }
                }else{
                    if(con.Id == null)
                        DMLManager.insertAsUser(con);
                }  
            }catch(Exception e){
                System.debug(''+e.getMessage()+'\t'+e.getLineNumber());
            }
            
            if(con.Id != null) {           
                taskOrder.Buyer__c = con.Id;
            }  
            
            return taskOrder;
        }else{
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
        }
    }
    
    private void createContact(String contactInfo){
        try{
            con = new Contact();
            String[] contactInfoSplit = contactInfo.split('\n');
            //System.debug('contactInfoSplit='+contactInfoSplit);
            if(contactInfoSplit != null && contactInfoSplit.size()>0){
                String contactEmail;
                for(String eachContactInfo : contactInfoSplit){
                    //System.debug('eachContactInfo = '+eachContactInfo);
                    eachContactInfo = eachContactInfo.trim();
                    if(String.isNotBlank(eachContactInfo) && eachContactInfo.contains('Email: ') && eachContactInfo.contains('@')){
                        //contactEmail = eachContactInfo.split(': ')[1].trim();
                        contactEmail = getRefinedData(eachContactInfo);
                        break;
                    }                    
                }
                
                System.debug('email = '+contactEmail);
                if(contactEmail!=null){
                    //con = new Contact();
                    List<Contact> contactList;  
                    contactList = [SELECT Id, Name, Title FROM Contact WHERE email =:contactEmail];                    
                    
                    if(contactList != null && !contactList.isEmpty() ){//for existing contact
                        System.debug('if = '+contactList[0].id);
                        con = contactList[0];               
                    }else{//for new contact;
                        System.debug('else');
                        con.Is_FedTom_Contact__c = true;
                        con.Email = contactEmail;
                        //for(String eachConInfo : contactInfoSplit){
                        try{
                            if(contactInfoSplit[0].contains('Name: ')){
                                String[] nameSplit = contactInfoSplit[0].replace('Name: ','').split(' ');
                                if(nameSplit != null && !nameSplit.isEmpty()){
                                    if(nameSplit .size()==2){
                                        con.firstName = nameSplit[0].trim();
                                        con.lastName = nameSplit[1].trim();
                                    }else if(nameSplit.size()==3){
                                        con.firstName = nameSplit[0].trim();
                                        con.lastName = nameSplit[2].trim();
                                    }else {
                                        con.lastName = nameSplit[0].trim();
                                    }
                                }
                            }
                        }catch(Exception e){
                            System.debug(''+e.getMessage()+'\t'+e.getLineNumber());
                        }
                        
                        try{
                            if(contactInfoSplit[contactInfoSplit.size()-1].contains('Phone: ')){
                                //con.Phone = contactInfoSplit[contactInfoSplit.size()-1].split(': ')[1];
                                con.Phone = getRefinedData(contactInfoSplit[contactInfoSplit.size()-1]);
                            }
                        }catch(Exception e){
                            System.debug(''+e.getMessage()+'\t'+e.getLineNumber());
                        }
                        
                        try{  
                            String conAddressRaw = contactInfo.substringBetween('Address:', 'Email:');
                            String conAddress;
                            //System.debug('con Address=='+conAddressRaw);
                            if(String.isNotBlank(conAddressRaw)){
                                conAddress = conAddressRaw.normalizeSpace();
                                if(String.isNotBlank(conAddress)){
                                    con.MailingStreet = conAddress.replaceAll('\\s*<.*>\\s*', '');
                                }
                            }
                        }catch(Exception e){
                            System.debug(''+e.getMessage()+'\t'+e.getLineNumber());
                        }
                        
                    }
                }
            }
            //System.debug('contact info = '+con);  
        }catch(Exception e){
            System.debug(''+e.getMessage()+'\t'+e.getLineNumber());
        }
        
    }
    
    private Date getDate(String dateStr){
        try{
            String[] splitDateRaw = dateStr.split(' ',2);
            return Date.parse(splitDateRaw[0]);
        }catch(Exception e){
            return null;
        }
    }
    
}