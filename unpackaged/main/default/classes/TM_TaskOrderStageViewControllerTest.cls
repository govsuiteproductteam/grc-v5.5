/*
Developer:Amit
Description:Test method for Stage process displayed on Task Order
*/
@isTest
public with sharing class TM_TaskOrderStageViewControllerTest
{
    public static testMethod void taskOrderViewTest()
    {
        
        Account acc = TestDataGenerator.createAccount('TaskOrderCapture');        
        insert acc;
        Contract_Vehicle__c contractVehicle = TestDataGenerator.createContractVehicle('TMconVehicle',acc);
        insert contractVehicle;
        Task_Order__c taskOrder = TestDataGenerator.createTaskOrder('TMtaskOrder', acc, contractVehicle);
        insert taskOrder;
        
        Test.startTest();
        ApexPages.CurrentPage().getParameters().put('id',null);
        ApexPages.StandardController controller = new ApexPages.StandardController(taskOrder); 
        TM_TaskOrderStageViewController surController = new TM_TaskOrderStageViewController(controller);
        system.assertNotEquals(surController.stageNameList.size(), 0);
        Test.stopTest();
    }
}