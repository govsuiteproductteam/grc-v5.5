public class SubContractDynamicFieldsController {
    private static Map<String,Map<String,FieldDependancyHelper.DependantField>> tempparentMap=null;
    public static Map<String,Map<String,FieldDependancyHelper.DependantField>> getparentMap(String recordId){
        if(tempparentMap==null){
            tempparentMap=new Map<String,Map<String,FieldDependancyHelper.DependantField>>();
            getFieldDependancyData(recordId);
            tempparentMap.putAll(FieldDependancyHelper.parentMap);
        }
        return tempparentMap;
    }
    
    @AuraEnabled
    public static List<String> getAllFieldsApi(String sobjectname,String recId){
        String whereField = FLSController.getQueryWithFLS('Id','TM_TOMA__SubContract__c');
        List<String> fldListToReturn = new List<String>();
        String allfields='';
        if(whereField != ''){
            String query = 'SELECT '+FLSController.getQueryWithFLS('Id,RecordTypeId','TM_TOMA__SubContract__c')+' FROM TM_TOMA__SubContract__c WHERE Id=:recId';
            
            List<SubContract__c> fedOppList =Database.query(query); //[SELECT Id,RecordTypeId FROM Contract_Vehicle__c WHERE Id=:recordId];
            if(fedOppList != null && ! fedOppList.isEmpty()){
                String recordTypeId = fedOppList[0].RecordTypeId; 
                if(recordTypeId != null && recordTypeId != ''){
                    //code to get field depandancy data  
                    getFieldDependancyData(recordTypeId);
                    SubContractDynamicFieldsController.parentMap = FieldDependancyHelper.parentMap;
                    
                    if(recordTypeId.length()>15){
                        recordTypeId =recordTypeId.substring(0,15);
                    }
                    Map<String, Schema.SObjectField> fieldsMapAccessible = Schema.getGlobalDescribe().get('TM_TOMA__SubContract__c').getDescribe().SObjectType.getDescribe().fields.getMap();
                    
                    Set<String> accessiblefields = new Set<String>();
                    
                    Set<String> reqFieldsSet = new Set<String>{'OwnerId','RecordTypeId','Name','Id'};
                        for(String s : reqFieldsSet){
                            Schema.Sobjectfield field = fieldsMapAccessible.get(s);
                            if(field != null && field.getDescribe().isAccessible())
                                accessiblefields.add(s);
                        }
                    List<CLM_FIeld__c> fieldCustSettList = new List<CLM_FIeld__c>();
                    String whereField0 = FLSController.getQueryWithFLS('TM_TOMA__RecordType_Id__c','TM_TOMA__CLM_FIeld__c');
                    if(whereField0 != ''){
                        String query1 = 'SELECT '+FLSController.getQueryWithFLS('Id,TM_TOMA__FieldApiName__c,TM_TOMA__ViewFieldApi__c','TM_TOMA__CLM_FIeld__c')+' FROM TM_TOMA__CLM_FIeld__c WHERE TM_TOMA__RecordType_Id__c =: recordTypeId';
                        fieldCustSettList = Database.query(query1); //[SELECT Id,FieldApiName__c,ViewFieldApi__c FROM CLM_FIeld__c WHERE RecordType_Id__c =: recordTypeId];
                    }
                    
                    
                    List<CLM_AtaGlanceField__c> atAGlanceFieldsList = new List<CLM_AtaGlanceField__c>();
                    String whereField1 = FLSController.getQueryWithFLS('TM_TOMA__RecordType_Id__c','TM_TOMA__CLM_AtaGlanceField__c');
                    if(whereField1 != ''){
                        String query2 = 'SELECT '+FLSController.getQueryWithFLS('Id,TM_TOMA__FieldApiName__c','TM_TOMA__CLM_AtaGlanceField__c')+' FROM TM_TOMA__CLM_AtaGlanceField__c WHERE TM_TOMA__RecordType_Id__c =:recordTypeId';
                        atAGlanceFieldsList = Database.query(query2); //[SELECT Id,FieldApiName__c FROM CLM_AtaGlanceField__c WHERE RecordType_Id__c =:recordTypeId];
                    }
                    List<CLM_Tab__c> tabFieldsList = New List<CLM_Tab__c>();
                    String whereField2 = FLSController.getQueryWithFLS('TM_TOMA__RecordType_Id__c','TM_TOMA__CLM_Tab__c');
                    if(whereField2 != ''){
                        String query3 = 'SELECT '+FLSController.getQueryWithFLS('Id,TM_TOMA__TabStatus__c','TM_TOMA__CLM_Tab__c')+' FROM TM_TOMA__CLM_Tab__c WHERE TM_TOMA__RecordType_Id__c =:recordTypeId';
                        tabFieldsList = Database.query(query3); //[SELECT Id,TabStatus__c FROM CLM_Tab__c WHERE RecordType_Id__c =:recordTypeId];
                    }
                    if(fieldCustSettList != null && ! fieldCustSettList.isEmpty()){
                        for(CLM_FIeld__c fieldCustSett :fieldCustSettList){
                            if(fieldCustSett.FieldApiName__c != null && fieldCustSett.FieldApiName__c != ''){
                                accessiblefields.add(fieldCustSett.FieldApiName__c);
                            }
                            if(fieldCustSett.ViewFieldApi__c != null && fieldCustSett.ViewFieldApi__c != ''){
                                accessiblefields.add(fieldCustSett.ViewFieldApi__c);
                            }
                            
                        }
                    }
                    
                    
                    if(atAGlanceFieldsList != null && ! atAGlanceFieldsList.isEmpty()){
                        for(CLM_AtaGlanceField__c atAGlanceCustSett :atAGlanceFieldsList){
                            if(atAGlanceCustSett.FieldApiName__c != null && atAGlanceCustSett.FieldApiName__c != ''){
                                accessiblefields.add(atAGlanceCustSett.FieldApiName__c);
                            }
                        }
                    }
                    
                    
                    if(tabFieldsList != null && ! tabFieldsList.isEmpty()){
                        for(CLM_Tab__c tabCustSett :tabFieldsList){
                            if(tabCustSett.TabStatus__c != null && tabCustSett.TabStatus__c != ''){
                                accessiblefields.add(tabCustSett.TabStatus__c);
                            }
                        }
                    }
                    List<String> tempApiList = new List<String>();
                    for(String fldApi : accessiblefields){
                        if(fieldsMapAccessible.get(fldApi) != null)
                            tempApiList.add(fieldsMapAccessible.get(fldApi).getDescribe().getName());
                    }
                    return tempApiList;
                    
                }
            }
        }
        
        return null;
    }
    
    public static Map<String, Schema.SObjectField> fieldsMapAccessible{get;set;}
    @AuraEnabled
    public static Boolean noTabs{get;set;}
    
    public static Map<String,Map<String,FieldDependancyHelper.DependantField>> parentMap{get; set;}
    
    static{
        fieldsMapAccessible=Schema.getGlobalDescribe().get('TM_TOMA__SubContract__c').getDescribe().SObjectType.getDescribe().fields.getMap();
        noTabs=false;        
    }
    
    private static void getFieldDependancyData(string recordId){
        FieldDependancyHelper fieldDependancyHelper = new FieldDependancyHelper();
        fieldDependancyHelper.process(recordId,false);
        
    }
    
    @AuraEnabled
    public static Boolean isLightningPage(){
        return (UserInfo.getUiThemeDisplayed() == 'Theme4d');
    }
    @AuraEnabled
    public static String getUserAccesibility()
    {
        return LincenseKeyHelper.checkLicensePermitionForFedCLM();
    }
    @AuraEnabled
    public static List<String> getDynamicCompoNames(String recordTypeId){
        if(recordTypeId.length()>14)
            recordTypeId = recordTypeId.substring(0,15);
        system.debug('recordTypeId'+recordTypeId);
        //List<TM_InlineVFPageSetting__c> inLineVFList= new List<TM_InlineVFPageSetting__c>();
        List<String> ComponentNameList = New List<String>();
        //inLineVFList=[SELECT Id,Height__c,InlineApiName__c,InlineLabel__c,Is_Lightning_Component__c,Order__c,RecordType_Id__c,TabNumber__c,Width__c FROM TM_InlineVFPageSetting__c WHERE RecordType_Id__c =: recordTypeId AND Is_Lightning_Component__c = true];
        /*system.debug('dyyynnnaaammmiiiccc components'+inLineVFList);
if(! inLineVFList.isEmpty()){
for(TM_InlineVFPageSetting__c compoName:inLineVFList){
ComponentNameList.add(compoName.InlineApiName__c);
}
}*/
        return(ComponentNameList.size()>0 ? ComponentNameList : new List<String>());
    }
    @AuraEnabled
    public static Map<String,SubContract__c> saveModifications(SubContract__c contractObj){
        system.debug('biiiiiidddddddddd'+contractObj);
        if(contractObj != null){
            
            try{
                update contractObj;
                system.debug('success No Error..!!!'+contractObj);
                
            }catch(DmlException e){
                system.debug('Execption in updating bid on line No.'+e.getLineNumber());
                system.debug(e);
                return new Map<String,SubContract__c>{e.getDmlFields(0)+'#####'+e.getDmlMessage(0)=>contractObj};
                    }
        }
        return new Map<String,SubContract__c>{'NOERROR'=>contractObj};
            }
    
    @AuraEnabled
    public static SubContract__c getMyContract(String contractId){
        system.debug('RDRDRDRDRD'+contractId);
        if(contractId != null && contractId !=''){
            List<SubContract__c> bidList = New List<SubContract__c>();
            String query='SELECT '+getAllFields('TM_TOMA__SubContract__c',contractId)+' FROM TM_TOMA__SubContract__c WHERE '+' id = :contractId ';
            bidList=Database.query(query);
            if(bidList != null && ! bidList.isEmpty()){
                return bidList[0];
            }
        }
        
        return New SubContract__c();
    }
    @AuraEnabled
    public static SubContract__c updateRecTypeId(String rectypeId,String contractId){
        system.debug('bid---'+rectypeId+'bid---'+contractId);
        List<SubContract__c> contractList = New List<SubContract__c>();
        if(rectypeId != null){
            String query='SELECT '+getAllFields('TM_TOMA__SubContract__c',contractId)+' FROM TM_TOMA__SubContract__c WHERE '+' id = :contractId ';
            contractList =Database.query(query);
            if(!contractList.isEmpty()){
                contractList[0].RecordTypeId = rectypeId;
                try{
                    update contractList[0];
                    return contractList[0];
                }catch(Exception e){system.debug('Exception'+e);}
                
                
            }
        }
        
        return new SubContract__c();
    }
    
    @AuraEnabled
    public static String updateOwnerId(String contractId,String ownerId){
        system.debug('contractId'+contractId);
        system.debug('ownerId'+ownerId);
        if(contractId != null && contractId != ''){
            try{
                SubContract__c newBid = new SubContract__c(id=contractId,OwnerId=ownerId);
                update newBid;
            }catch(Exception e){
                system.debug('Exception'+e);
                return ('ERRORINUPDATE#####'+e.getDmlFields(0)+'#####'+e.getMessage());
            }
            
            return 'SUCCESS';
            
        }
        return 'ERROR';
    }
    
    
    public static String getAllFields(String sobjectname,String recId){
        String whereField = FLSController.getQueryWithFLS('Id','TM_TOMA__SubContract__c');
        String allfields='';
        if(whereField != ''){
            String query = 'SELECT '+FLSController.getQueryWithFLS('Id,RecordTypeId','TM_TOMA__SubContract__c')+' FROM TM_TOMA__SubContract__c WHERE Id=:recId';
            
            List<SubContract__c> fedOppList =Database.query(query); //[SELECT Id,RecordTypeId FROM Contract_Vehicle__c WHERE Id=:recordId];
            if(fedOppList != null && ! fedOppList.isEmpty()){
                String recordTypeId = fedOppList[0].RecordTypeId;
                if(recordTypeId != null && recordTypeId != ''){
                    if(recordTypeId.length()>15){
                        recordTypeId =recordTypeId.substring(0,15);
                    }
                    Map<String, Schema.SObjectField> fieldsMapAccessible = Schema.getGlobalDescribe().get('TM_TOMA__SubContract__c').getDescribe().SObjectType.getDescribe().fields.getMap();
                    
                    Set<String> accessiblefields = new Set<String>();
                    
                    Set<String> reqFieldsSet = new Set<String>{'OwnerId','RecordTypeId','Name','Id'};
                        for(String s : reqFieldsSet){
                            Schema.Sobjectfield field = fieldsMapAccessible.get(s);
                            if(field != null && field.getDescribe().isAccessible())
                                accessiblefields.add(s);
                        }
                    List<CLM_FIeld__c> fieldCustSettList = new List<CLM_FIeld__c>();
                    String whereField0 = FLSController.getQueryWithFLS('TM_TOMA__RecordType_Id__c','TM_TOMA__CLM_FIeld__c');
                    if(whereField0 != ''){
                        String query1 = 'SELECT '+FLSController.getQueryWithFLS('Id,TM_TOMA__FieldApiName__c,TM_TOMA__ViewFieldApi__c,TM_TOMA__RecordType_Id__c','TM_TOMA__CLM_FIeld__c')+' FROM TM_TOMA__CLM_FIeld__c WHERE TM_TOMA__RecordType_Id__c =: recordTypeId';
                        fieldCustSettList = Database.query(query1); //[SELECT Id,FieldApiName__c,ViewFieldApi__c FROM CLM_FIeld__c WHERE RecordType_Id__c =: recordTypeId];
                    }
                    
                    
                    List<CLM_AtaGlanceField__c> atAGlanceFieldsList = new List<CLM_AtaGlanceField__c>();
                    String whereField1 = FLSController.getQueryWithFLS('TM_TOMA__RecordType_Id__c','TM_TOMA__CLM_AtaGlanceField__c');
                    if(whereField1 != ''){
                        String query2 = 'SELECT '+FLSController.getQueryWithFLS('Id,TM_TOMA__FieldApiName__c,TM_TOMA__RecordType_Id__c','TM_TOMA__CLM_AtaGlanceField__c')+' FROM TM_TOMA__CLM_AtaGlanceField__c WHERE TM_TOMA__RecordType_Id__c =:recordTypeId';
                        atAGlanceFieldsList = Database.query(query2); //[SELECT Id,FieldApiName__c FROM CLM_AtaGlanceField__c WHERE RecordType_Id__c =:recordTypeId];
                    }
                    List<CLM_Tab__c> tabFieldsList = New List<CLM_Tab__c>();
                    String whereField2 = FLSController.getQueryWithFLS('TM_TOMA__RecordType_Id__c','TM_TOMA__CLM_Tab__c');
                    if(whereField2 != ''){
                        String query3 = 'SELECT '+FLSController.getQueryWithFLS('Id,TM_TOMA__TabStatus__c,TM_TOMA__RecordType_Id__c','TM_TOMA__CLM_Tab__c')+' FROM TM_TOMA__CLM_Tab__c WHERE TM_TOMA__RecordType_Id__c =:recordTypeId';
                        tabFieldsList = Database.query(query3); //[SELECT Id,TabStatus__c FROM CLM_Tab__c WHERE RecordType_Id__c =:recordTypeId];
                    }
                    if(fieldCustSettList != null && ! fieldCustSettList.isEmpty()){
                        for(CLM_FIeld__c fieldCustSett :fieldCustSettList){
                            if(fieldCustSett.FieldApiName__c != null && fieldCustSett.FieldApiName__c != ''){
                                accessiblefields.add(fieldCustSett.FieldApiName__c);
                            }
                            if(fieldCustSett.ViewFieldApi__c != null && fieldCustSett.ViewFieldApi__c != ''){
                                accessiblefields.add(fieldCustSett.ViewFieldApi__c);
                            }
                            
                        }
                    }
                    
                    
                    if(atAGlanceFieldsList != null && ! atAGlanceFieldsList.isEmpty()){
                        for(CLM_AtaGlanceField__c atAGlanceCustSett :atAGlanceFieldsList){
                            if(atAGlanceCustSett.FieldApiName__c != null && atAGlanceCustSett.FieldApiName__c != ''){
                                accessiblefields.add(atAGlanceCustSett.FieldApiName__c);
                            }
                        }
                    }
                    
                    
                    if(tabFieldsList != null && ! tabFieldsList.isEmpty()){
                        for(CLM_Tab__c tabCustSett :tabFieldsList){
                            if(tabCustSett.TabStatus__c != null && tabCustSett.TabStatus__c != ''){
                                accessiblefields.add(tabCustSett.TabStatus__c);
                            }
                        }
                    }
                    
                    for(String fieldname : accessiblefields)
                    {
                        Schema.Sobjectfield field = fieldsMapAccessible.get(fieldname);
                        if(field != null){
                            if(field.getDescribe().isAccessible()){
                                allfields += fieldname+',';
                            }
                        }
                    }
                    if(allfields != '')
                        allfields = allfields.subString(0,allfields.length()-1);
                }
            }
        }
        
        return allfields;
    }
    
    
    
    private static Map<Integer,CLMTabWraper> tabWrapMap;
    private static Map<String,Schema.SObjectField> M;
    
    @AuraEnabled
    public static Boolean isEditMode{get;set;}
    @AuraEnabled
    public static Map<String,String> fieldDatatypeMap{get;set;}   
    
    @AuraEnabled
    public static SubContractWrapper createContract(String recordId) {
        System.debug('*********createContract*******');
        isEditMode = true;
        fieldDatatypeMap = new Map<String,String>();
        SubContractWrapper contractWrapper  = init(recordId);
        
        contractWrapper.doUserHaveEditPermission = true;
        if(Schema.sObjectType.SubContract__c.isAccessible() && Schema.SObjectType.SubContract__c.isUpdateable()){
            List<UserRecordAccess> recAccessList = [SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId =: UserInfo.getUserId() AND RecordId =: recordId];
            if(recAccessList != null && ! recAccessList.isEmpty()){
                contractWrapper.doUserHaveEditPermission = recAccessList[0].HasEditAccess == null ? true : recAccessList[0].HasEditAccess == true ? true:false;
            }
        }else{
            contractWrapper.doUserHaveEditPermission = false;
        }
        system.debug('bid data=$$$==='+contractWrapper.contractObj);
        return contractWrapper;
    }
    
    private static SubContractWrapper Init(String recordId) {
        M = new Map<String,Schema.SObjectField>();
        List<CLMTabListAndDataWrapper> tabNamesWrpList = new List<CLMTabListAndDataWrapper>();
        System.debug('recordID = '+recordId);
        
        String query='SELECT '+getAllFields('TM_TOMA__SubContract__c',recordId)+' FROM TM_TOMA__SubContract__c WHERE '+' id = :recordId '+' LIMIT 1 ';
        
        SubContract__c  contractObj = database.query(query);
        
        
        Set<String> fieldApiName = new Set<String>{'RecordTypeId'};
            Map<Id, List<CLM_FIeld__c>> mapRecTypeIdToCustSetObj = new Map<Id, List<CLM_FIeld__c>>();
        
        M =  Schema.SObjectType.SubContract__c.fields.getMap();
        
        for(CLM_FIeld__c  itrCustSeting : [SELECT FieldApiName__c,ViewFieldApi__c, RecordType_Id__c, Order__c, TabNumber__c, FieldLabel__c,Required__c,Is_Contract__c,Draft_Required__c,Create_Blank_Space__c,Required_Error_Message__c  FROM CLM_FIeld__c Where 	Is_Contract__c = false ORDER BY TabNumber__c,Order__c Limit 10000]) { 
            List<CLM_FIeld__c> custSetList = mapRecTypeIdToCustSetObj.get(itrCustSeting.RecordType_Id__c);
            if(custSetList == Null) {
                custSetList = new List<CLM_FIeld__c>();
            }
            
            if(!fieldDatatypeMap.containsKey(itrCustSeting.FieldApiName__c.trim())){
                System.debug('*********************itrCustSeting.FieldApiName__c = '+itrCustSeting.FieldApiName__c);
                Schema.SObjectField field = M.get(itrCustSeting.FieldApiName__c.trim());
                if(field != null){
                    
                    Schema.DisplayType fldType = field.getDescribe().getType();
                    
                    fieldDatatypeMap.put(itrCustSeting.FieldApiName__c.trim(), ''+fldType);
                    
                }
            }
            
            if(itrCustSeting.ViewFieldApi__c != null && !fieldDatatypeMap.containsKey(itrCustSeting.ViewFieldApi__c.trim())){
                System.debug('*********************itrCustSeting.ViewFieldApi__c = '+itrCustSeting.ViewFieldApi__c);
                Schema.SObjectField viewField = M.get(itrCustSeting.ViewFieldApi__c.trim());
                
                if(viewField != null){
                    Schema.DisplayType viewFldType = viewField.getDescribe().getType();
                    fieldDatatypeMap.put(itrCustSeting.ViewFieldApi__c.trim(), ''+viewFldType);
                }
                
            }
            custSetList.add(itrCustSeting);
            mapRecTypeIdToCustSetObj.put(itrCustSeting.RecordType_Id__c, custSetList);
            fieldApiName.add(itrCustSeting.FieldApiName__c);                                                      
        }
        
        
        for(CLM_Tab__c statusTab : [SELECT TabStatus__c FROM CLM_Tab__c WHERE TabStatus__c!=Null]){
            if(statusTab.TabStatus__c!=Null){
                fieldApiName.add(statusTab.TabStatus__c);
            }
        }
        
        
        String contractRecTypeId = '';
        if(contractObj.RecordTypeId != null && (''+contractObj.RecordTypeId).length()>=15)
            contractRecTypeId =(''+contractObj.RecordTypeId).substring(0,15);
        System.debug(''+contractRecTypeId);
        tabWrapMap = new Map<Integer,CLMTabWraper>();
        List<CLMTabWraper> tabWrapList = new List<CLMTabWraper>();
        
        Integer firstTab=0;
        for(CLM_Tab__c tab : [SELECT Id, Order__c, RecordType_Id__c, TabName__c, TabStatus__c,BackgroundColor__c FROM CLM_Tab__c WHERE RecordType_Id__c = :contractRecTypeId ORDER BY Order__c]){//AND TabName__c NOT IN:tabsToExclude
            if(tab.BackgroundColor__c == '' || tab.BackgroundColor__c == null)//newlly added 20-08-2018 for tab css 
            {
                tab.BackgroundColor__c='#0070d2';
            }
            CLMTabWraper tabWrap = new CLMTabWraper(tab);
            if(firstTab == 0){
                tabWrap.isLoadedInDom=true;
                firstTab++;
            }else{
                tabWrap.isLoadedInDom=false;
            }
            
            tabWrapList.add(tabWrap);
            tabNamesWrpList.add(new CLMTabListAndDataWrapper(tab));
            tabWrapMap.put((Integer)tab.Order__c, tabWrap);
        }
        
        List<CLM_Section__c> tempSecList = new List<CLM_Section__c>();
        tempSecList = [SELECT Id, Order__c,RecordType_Id__c, SectionName__c,AppApi__c,Is_Contract__c, TabNumber__c, Is_InlineVF_Page__c, Is_Lightning_Component__c,Inline_Api_Name_Component_Api__c,Height__c,Width__c,No_Of_Column__c FROM CLM_Section__c WHERE RecordType_Id__c = :contractRecTypeId AND Is_Contract__c = false ORDER BY TabNumber__c asc , Order__c asc];//,Enabled_for_Lightning__c
        
        //if there is no tab just section and fields then we are adding a fake Tab
        if(tabWrapMap.isEmpty() && ! tempSecList.isEmpty()){
            CLMTabWraper fakeTab = new CLMTabWraper(new CLM_Tab__c(TabName__c = 'NO TAB',RecordType_Id__c = contractRecTypeId,Order__c=1,BackgroundColor__c = '#4f85bb'));
            noTabs = true;
            tabWrapMap.put(0,fakeTab);
            tabWrapList.add(tabWrapMap.get(0));
        }
        
        
        for(CLM_Section__c sec : tempSecList){
            CLMSectionWrap secWrap = new CLMSectionWrap(sec);
            if(! sec.Is_Lightning_Component__c && ! sec.Is_InlineVF_Page__c){
                if(mapRecTypeIdToCustSetObj.containsKey(contractObj.RecordTypeId)){
                    List<CLM_FIeld__c> custFieldList = mapRecTypeIdToCustSetObj.get(contractObj.RecordTypeId);              
                    if(custFieldList != null && !custFieldList.isEmpty()){    
                        
                        List<CLMFieldWrap> fieldWrapList = getFieldWrapperListSectionWise(custFieldList,(Integer)sec.TabNumber__c,(Integer)sec.Order__c,contractObj.RecordTypeId,contractObj);//newlly added contractObj for getting value for Date datetime and parcent field 18-09-2018
                        System.debug('hello fieldWrapList'+fieldWrapList);
                        secWrap.fieldWrapList = fieldWrapList;
                    }
                }
                system.debug('tabWrapMap tabWrapMap'+tabWrapMap);
            }//end of if
            //else if it is a component or Inline vf page
            else {
                CLMComponentWrap compoWrap = New CLMComponentWrap(sec);
                secWrap.ComponentOrPageWrap = compoWrap;
            }
            if(tabWrapMap.containsKey((Integer)sec.TabNumber__c)){
                CLMTabWraper tabWrap  = tabWrapMap.get((Integer)sec.TabNumber__c);
                tabWrap.sectionWrapList.add(secWrap);
                tabWrap.sectionWrapMap.put((Integer)sec.Order__c, secWrap);
            }
        }
        if(mapRecTypeIdToCustSetObj.containsKey(contractObj.RecordTypeId)){
            List<CLM_FIeld__c> custFieldList = mapRecTypeIdToCustSetObj.get(contractObj.RecordTypeId);
            Integer key;
            Integer count;
            Integer tempSec;
            Integer tempTab;
            Map<Integer,List<CLMFieldWrap>> fieldWrapMap = new Map<Integer,List<CLMFieldWrap>>();
            
            for(CLM_FIeld__c cusField : custFieldList){
                string tempRec = contractObj.RecordTypeId+'';
                CLMFieldWrap fieldWra = new CLMFieldWrap(cusField,tempRec,'TM_TOMA__SubContract__c','Detail');
                Integer secSeq = (Integer)cusField.Order__c / 100;
                if(tempSec == Null || tempSec != secSeq || tempTab == Null || tempTab != (Integer)cusField.TabNumber__c){
                    key = 1;
                    count = 1;
                    tempSec = secSeq;
                    tempTab = (Integer)cusField.TabNumber__c;
                }
                List<CLMFieldWrap> fieldWrapList = new List<CLMFieldWrap>();
                if(tabWrapMap.containsKey((Integer)cusField.TabNumber__c)){
                    CLMTabWraper tabWrap = tabWrapMap.get((Integer)cusField.TabNumber__c);
                    
                    if(tabWrap.sectionWrapMap.containsKey(secSeq)){
                        if(fieldWrapMap.containsKey(key)){
                            fieldWrapMap.get(key).add(fieldWra);
                        }else{
                            fieldWrapMap.put(key, new List<CLMFieldWrap>{fieldWra});
                        }
                        fieldWrapList.add(fieldWra);
                    }
                }
                
                
                
                CLMTabWraper tabWrap = tabWrapMap.get((Integer)cusField.TabNumber__c);                    
                if(tabWrap != null && tabWrap.sectionWrapMap.containsKey(secSeq)){
                    CLMSectionWrap seqWrap = tabWrap.sectionWrapMap.get(secSeq);
                }
                if(math.mod(count, 2) == 0){
                    key++;
                }
                count++;
            }
        }
        
        SubContractWrapper contractWrapper = new SubContractWrapper();
        contractWrapper.contractObj = contractObj;
        contractWrapper.tabWrapList = tabWrapList;
        contractWrapper.hasNoTabs = noTabs;
        contractWrapper.allTabsNameWrpList = tabNamesWrpList;
        return contractWrapper;
    }
    
    private static List<CLMFieldWrap> getFieldWrapperListSectionWise(List<CLM_FIeld__c> custFieldList, Integer secTabNo, Integer sectionOrder,Id recTypeID,SubContract__c contractObjData){
        List<CLMFieldWrap> fieldWrapList = new List<CLMFieldWrap>();
        
        for(CLM_FIeld__c cusField : custFieldList){            
            if(cusField.TabNumber__c == secTabNo && checkSection((Integer)cusField.Order__c,sectionOrder)){
                string tempRecID = recTypeID+'';
                CLMFieldWrap fieldWra = new CLMFieldWrap(cusField,tempRecID,'TM_TOMA_SubContract__c','Detail');
                fieldWra.fieldType = fieldDatatypeMap.get(cusField.FieldApiName__c.trim());
                fieldWra.viewFieldType = cusField.ViewFieldApi__c != null && cusField.ViewFieldApi__c != '' ? fieldDatatypeMap.get(cusField.ViewFieldApi__c.trim()) : fieldDatatypeMap.get(cusField.FieldApiName__c.trim());
                if(fieldWra.fieldType != null && fieldWra.fieldType != '' && fieldWra.fieldType == 'REFERENCE'){
                    // code for lookup Name
                    System.debug(''+fieldWra.fieldType);
                    Schema.SObjectField field = M.get(cusField.FieldApiName__c.trim());
                    System.debug(''+field.getDescribe().getReferenceTo().get(0));
                    fieldWra.lookUpName = ''+field.getDescribe().getReferenceTo().get(0);
                    fieldWra.lookUpParentLabel = ''+field.getDescribe().getReferenceTo().get(0).getDescribe().getLabel();
                }else if(fieldWra.fieldType != null && (fieldWra.fieldType == 'DATE' || fieldWra.fieldType =='DATETIME' || fieldWra.fieldType=='PERCENT')){//newlly added for Date DateTime and parcent field value 18-09-2018
                    if(fieldWra.isAccessible == true){//newlly added 25-10-2018 for datetime and percent field level security issue
                        if(fieldWra.fieldType =='DATETIME'){
                            fieldWra.fldValue =json.serialize(contractObjData.get(cusField.FieldApiName__c.trim()));
                        }else{
                            fieldWra.fldValue = ''+contractObjData.get(cusField.FieldApiName__c.trim());
                        }
                    }
                    
                }
                fieldWrapList.add(fieldWra);
            }
        }
        return fieldWrapList;
    }
    
    private static boolean checkSection(Integer fieldOrder,Integer sectionOrder){
        Integer secOrderFromField = fieldOrder/100;        
        if(sectionOrder == secOrderFromField){
            return true;
        }
        return false;
    }
    
    
    Public class FieldWrapMap{
        @AuraEnabled
        public Integer row{get;set;} 
        @AuraEnabled
        public List<CLMFieldWrap> fieldWrapList{get;set;}
        
    }   
}