@isTest
public class Test_TM_SubContractRecordTypeSelection {
  public static testMethod void testSubContractRecordTypeSelection(){
      
        SubContract__c subContractVehicle =  TestDataGenerator.createSubContract('12345');
        insert subContractVehicle;
        
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(subContractVehicle);
        TM_SubContractRecordTypeSelection recordTypeSelection = new TM_SubContractRecordTypeSelection(sc);
        PageReference pageRef = Page.TM_SubContractRecordTypeSelection;
        pageRef.getParameters().put('id', subContractVehicle.Id);
        Test.setCurrentPage(pageRef);
        PageReference pageReference = recordTypeSelection.ContinueOnRcdTypeSelection();
        System.assertEquals('/apex/tm_toma__tm_subcontractnewpage?id='+subContractVehicle.id,PageReference.getUrl());
        Test.stopTest();
    }
}