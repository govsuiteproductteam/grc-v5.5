global With Sharing class TM_TaskOrderAttachmentController{
    public List<AttachmentModel> modelList{get;set;}
    public Attachment attachment{get;set;}
    public Note note{get;set;}
    public String taskOrderId{get;set;}
    public Boolean isAttachFile{get;set;}
    public Boolean isNewNote{get;set;}
    public String taskOrderName{get;set;}
    public String type{get;set;}
    public String deleteRecordId{get;set;}
    public String profileName{get;set;}
    public boolean isShare{get;set;}
    public String profileLicence{get;set;}
    private Map<String,Folder__c> attachmenttoFolderMap;
    private Map<String,Folder__c> noteToFolderMap;
    
    public TM_TaskOrderAttachmentController(ApexPages.StandardController sc){
        if(Schema.sObjectType.User.isAccessible() && Schema.sObjectType.Task_Order__c.isAccessible() &&  Schema.sObjectType.attachment.isAccessible() && Schema.sObjectType.Note.isAccessible() && Schema.sObjectType.Folder__c.isAccessible() ){
            User u = [select Profile.Name , Profile.UserLicense.Name from User where id=:userinfo.getUserId() LIMIT 1];
            profileName = u.profile.Name;
            profileLicence = u.Profile.UserLicense.Name;
            
            isShare = false;
            taskOrderId = sc.getId();
            List<Task_Order__c> taskOrderList = [select id, OwnerId, Name from Task_Order__c where id=:taskOrderId LIMIT 1000];
            if(taskOrderList.size()>0){
                taskOrderId = taskOrderList[0].Id;
                taskOrderName = taskOrderList[0].Name;
                if( ((profileLicence == 'Salesforce Platform' || profileLicence == 'Salesforce') && taskOrderList[0].OwnerId==userinfo.getUserId()) || profileName == 'System Administrator'){
                    isShare = true;
                }
            }
            
            isAttachFile = false;
            isNewNote = false;
            
            
            if(ApexPages.CurrentPage().getParameters().get('attachFile') != null){
                isAttachFile = Boolean.ValueOf(ApexPages.CurrentPage().getParameters().get('attachFile'));
            }
            if(ApexPages.CurrentPage().getParameters().get('newNote') != null){
                isNewNote = Boolean.ValueOf(ApexPages.CurrentPage().getParameters().get('newNote'));
            }        
            init();
        }
    }
    
    public void init(){
        if(Schema.sObjectType.Folder__c.isAccessible() && Schema.sObjectType.Attachment.isAccessible() && Schema.sObjectType.Note.isAccessible()){
            attachmenttoFolderMap = new Map<String,Folder__c>();
            noteToFolderMap = new Map<String,Folder__c>();
            modelList = new List<AttachmentModel>();
            deleteRecordId = '';
            type = '';
            attachment = new Attachment();
            note = new Note();
            
            List<Folder__c> folderList;
            
            if(profileName == 'System Administrator'){
                folderList = [select id,(select id,Name,LastModifiedDate,CreatedById,CreatedBy.Name from Attachments),
                                    (select id,Title,IsPrivate,LastModifiedDate,CreatedById,CreatedBy.Name from Notes),
                                    UserRecordAccess.HasReadAccess, UserRecordAccess.HasEditAccess, UserRecordAccess.HasDeleteAccess, 
                                    UserRecordAccess.MaxAccessLevel, UserRecordAccess.HasTransferAccess, UserRecordAccess.HasAllAccess
                                     from Folder__c 
                                    where Task_Order__c =: taskOrderId ORDER BY LastModifiedDate DESC LIMIT 1000];
            }else{
                folderList = [select id,(select id,Name,LastModifiedDate,CreatedById,CreatedBy.Name from Attachments),
                                    (select id,Title,IsPrivate,LastModifiedDate,CreatedById,CreatedBy.Name from Notes),
                                    UserRecordAccess.HasReadAccess, UserRecordAccess.HasEditAccess, UserRecordAccess.HasDeleteAccess, 
                                    UserRecordAccess.MaxAccessLevel, UserRecordAccess.HasTransferAccess, UserRecordAccess.HasAllAccess
                                     from Folder__c 
                                    where Id in:fetchShareFolders() AND Task_Order__c =:taskOrderId ORDER BY LastModifiedDate DESC LIMIT 1000];
            }
            system.debug('@@@@ folder='+folderList.size());
            for(Folder__c folder:folderList)
            {
                for(Attachment att:folder.Attachments){
                    attachmenttoFolderMap.put(att.Id,folder);
                    AttachmentModel am = new AttachmentModel(folder, att, null);
                    modelList.add(am);
                }
                for(Note no:folder.Notes){
                    noteToFolderMap.put(no.Id,folder);
                    AttachmentModel am = new AttachmentModel(folder, null, no);
                    modelList.add(am);
                }
            }
       }
    }
    
    public void confirmDeleteNotesAndAttachment(){
        if(Schema.sObjectType.Attachment.isAccessible() && Schema.sObjectType.Note.isAccessible()){
            if(deleteRecordId != null && deleteRecordId.trim().length()>0 ){
                if(type == 'Attachment' && attachmentToFolderMap.containsKey(deleteRecordId))
                   // delete attachmentToFolderMap.get(deleteRecordId);
                    DMLManager.deleteAsUser(attachmentToFolderMap.get(String.escapeSingleQuotes(deleteRecordId)));
                    
                else if(type == 'Note' && noteToFolderMap.containsKey(deleteRecordId))
                   // delete noteToFolderMap.get(deleteRecordId);
                    DMLManager.deleteAsUser(noteToFolderMap.get(String.escapeSingleQuotes(deleteRecordId)));
                
                init();
            }
        }
    }
    
    public PageReference newNote(){
        if(Schema.sObjectType.Folder__c.isAccessible()){
            Folder__c folder = new Folder__c();
            folder.Task_Order__c = taskOrderId;
           // insert folder;
            DMLManager.insertAsUser(folder);
            
            note.ParentId = folder.Id;
           // insert note;
            DMLManager.insertAsUser(note);
            
           // String returnURL =  profileName == 'System Administrator'  && !note.IsPrivate ? '/p/share/CustomObjectSharingEdit?parentId='+folder.Id+'&retURL=/'+taskOrderId : '/'+taskOrderId;
            String returnURL = (profileLicence == 'Salesforce Platform' || profileLicence == 'Salesforce')  && !note.IsPrivate ? '/p/share/CustomObjectSharingEdit?parentId='+folder.Id+'&retURL=/'+taskOrderId : '/'+taskOrderId;
            
            PageReference pg = new PageReference(returnURL);
            return pg.setRedirect(true);
        }    
        return null; 
    }
    
    public PageReference uploadAttachment(){
        if(Schema.sObjectType.Attachment.isAccessible() && Schema.sObjectType.Folder__c.isAccessible()){
            if(attachment.Body != null && Attachment.Name != null){
                Folder__c folder = new Folder__c();
                folder.Task_Order__c = taskOrderId;
               // insert folder;
                DMLManager.insertAsUser(folder);
                
                attachment.ParentId = folder.Id;
                //insert attachment;
                DMLManager.insertAsUser(attachment);
                
                //String returnURL = profileName == 'System Administrator' ? '/p/share/CustomObjectSharingEdit?parentId='+folder.Id+'&retURL=/'+taskOrderId : '/'+taskOrderId;
                String returnURL = (profileLicence == 'Salesforce Platform' || profileLicence == 'Salesforce') ? '/p/share/CustomObjectSharingEdit?parentId='+folder.Id+'&retURL=/'+taskOrderId : '/'+taskOrderId;
                PageReference pg = new PageReference(returnURL);
                return pg.setRedirect(true);
            }else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select a file to upload.'));
                return null;
            }
        }
        return null;
    }
    
    public static Set<String> fetchShareFolders(){
        if(Schema.sObjectType.Folder__c.isAccessible()){
            Set<String> folderIds = new Set<String>();
            for(Folder__Share folderShare:[select id,ParentId from Folder__Share where UserOrGroupId=:UserInfo.getUserId()]){
                folderIds.add(folderShare.ParentId);
            }
            return folderIds;
        }
        return null;
    }
   /****************************** Wrapper class for Attachment and Note object ***********************************/ 
    public class AttachmentModel{
        public Folder__c folder{get;set;}
        public Attachment att{get;set;}
        public Note note{get;set;}
        
        public String createdById{get;set;}
        public String createdByName{get;set;}
        public DateTime lastModifiedDate{get;set;}
        public String title{get;set;}
        public String type{get;set;}
        public String recordId{get;set;}
        
        public AttachmentModel(Folder__c folder,Attachment attachment, Note newNote){
            this.folder = folder;
            this.att = attachment;
            this.note = newNote;
            
            if(attachment!= null){
                this.recordId = attachment.Id;
                this.title = attachment.Name;
                this.createdById = attachment.CreatedById;
                this.createdByName = attachment.CreatedBy.Name;
                this.lastModifiedDate = attachment.LastModifiedDate;
                this.type = 'Attachment';
            }else{
                this.recordId = newNote.Id;
                this.title = newNote.Title;
                this.createdById = newNote.CreatedById;
                this.createdByName = newNote.CreatedBy.Name;
                this.lastModifiedDate = newNote.LastModifiedDate;
                this.type = 'Note';
            }
        }
    }
}