@isTest
public class DESP_IIIEmailHandlerTest {
    public static testMethod void testDESP_IIIForTaskOrder(){
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Contact con = TestDataGenerator.createContact('TestCon', acc);
        insert con;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('T-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'desp_iii@2peodqbelym0wls6emi3f2awkkv5g71k0ebiefz8awihxzqjd0.37-pvateac.na31.apex.salesforce.com';
        
        insert conVehi;
        
        Messaging.InboundEmail email  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        email.plainTextBody = 'Hello,\n'+
            '\n'+
            'The DESP III is requesting a proposal in response to the attached\n'+
            'Contractual Engineering Task, dated 6 February 2016 for the subject\n'+
            'services. This will be issued as a new task order in accordance with the\n'+
            'DESP III basic Indefinite Delivery/Indefinite Quantity (ID/IQ) contracts.\n'+
            'Thank you\n'+
            '\n'+
            'Kimberlene S.C. Butler\n'+
            'A-OPC/ Contract Specialist\n'+
            'Design & Engineering Support Program (DESP) AFMC OL H/PZIEA\n'+
            'Kimberlene.butler@us.af.mil DSN 586-8610 Comm (801) 586-8610';
        email.fromAddress ='test@test.com';
        email.fromName = 'ABC XYZ';
        email.subject = 'DESP III Request for Order Proposal, Navy RC-26 Radar';
        email.toaddresses = new List<String>();
        email.toaddresses.add('desp_iii@2peodqbelym0wls6emi3f2awkkv5g71k0ebiefz8awihxzqjd0.37-pvateac.na31.apex.salesforce.com');
        
        DESP_IIIEmailHandler handler = new DESP_IIIEmailHandler();
        Test.startTest();
        Messaging.InboundEmailResult result = handler.handleInboundEmail(email, env);
        Test.stopTest();
        
        List<Task_Order__c> taskOrderList = [SELECT Id, Name, Task_Order_Title__c, Contract_Vehicle_picklist__c, Release_Date__c FROM Task_Order__c];
        Task_Order__c taskOrder = taskOrderList.get(0);
        System.assertEquals(taskOrderList.size(), 1);
        System.assertEquals('DESP III Request for Order Proposal, Navy RC-26 Radar', taskOrder.Task_Order_Title__c);
        System.assertEquals('DESP III - DESIGN AND ENGINEERING SUPPORT PROGRAM', taskOrder.Contract_Vehicle_picklist__c);
        System.assertEquals(Date.today(), taskOrder.Release_Date__c);
    }
    
    public static testMethod void testDESP_IIIForAttachments(){
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Contact con = TestDataGenerator.createContact('TestCon', acc);
        insert con;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('T-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'desp_iii@2peodqbelym0wls6emi3f2awkkv5g71k0ebiefz8awihxzqjd0.37-pvateac.na31.apex.salesforce.com';
        
        insert conVehi;
        
        Messaging.InboundEmail email  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        email.plainTextBody = 'Hello,\n\n'+
            'The DESP III is requesting a proposal in response to the attached\n'+
            'Contractual Engineering Task, dated 6 February 2016 for the subject\n'+
            'services. This will be issued as a new task order in accordance with the\n'+
            'DESP III basic Indefinite Delivery/Indefinite Quantity (ID/IQ) contracts.\n'+
            'Thank you\n\n'+
            'Kimberlene1\n'+
            'A-OPC/ Contracting Officer\n'+
            'Design & Engineering Support Program (DESP) AFMC OL H/PZIEA\n'+
            'Kimberlene1@us.af.mil DSN 586-8610 Comm (801) 586-8610';
        email.fromAddress ='test@test.com';
        email.fromName = 'ABC XYZ';
        email.subject = 'Fwd: DESP III Request for Order Proposal, Navy RC-26 Radar';
        email.toaddresses = new List<String>();
        email.toaddresses.add('desp_iii@2peodqbelym0wls6emi3f2awkkv5g71k0ebiefz8awihxzqjd0.37-pvateac.na31.apex.salesforce.com');
        
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        attachment.body = blob.valueOf('my attachment text');
        attachment.fileName = 'textfileone.txt';
        attachment.mimeTypeSubType = 'text/plain';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
            
            Messaging.InboundEmail.TextAttachment attachmenttext = new Messaging.InboundEmail.TextAttachment();
        attachmenttext.body = 'my attachment text';
        attachmenttext.fileName = 'textfiletwo3.txt';
        attachmenttext.mimeTypeSubType = 'texttwo/plain';
        email.textAttachments =   new Messaging.inboundEmail.TextAttachment[] { attachmenttext };
            
            DESP_IIIEmailHandler handler = new DESP_IIIEmailHandler();
        Test.startTest();
        Messaging.InboundEmailResult result = handler.handleInboundEmail(email, env);
        Test.stopTest();
        
        List<Task_Order__c> taskOrderList = [SELECT Id, Name, Task_Order_Title__c, Contract_Vehicle_picklist__c, Release_Date__c FROM Task_Order__c];
        Task_Order__c taskOrder = taskOrderList.get(0);
        System.assertEquals(taskOrderList.size(), 1);
        List<Attachment> attachments=[SELECT Id, Name FROM Attachment WHERE parent.id =: taskOrder.id];
        System.assertEquals(2, attachments.size());
    }
}