({
    doCheckLicense : function(component, event, helper) {
        var action = component.get("c.checkLicensePermition1");
        action.setCallback(this, function(a) {
           //alert('doCheckLicense');
            if(a.getReturnValue() === 'Yes')
            {
               // alert('inside if');
                component.set("v.CheckLicense", 'Yes');
                 helper.getContractVehical(component, event, helper);
                 helper.doInitAtAGlanceData(component, event, helper);
            }
            else
            {
                // alert('inside else');
                component.set("v.CheckLicense", a.getReturnValue());
            }
            
        });
         $A.enqueueAction(action);  
    },
    /*  changeStatfulButton : function(component, event, helper) {
        var statefullbutton = component.find("FollowId");
        document.getElementById("FollowId").style.display="none";
        document.getElementById("FollowingId").style.display="block";
        //$A.util.toggleClass(editModal1, "slds-modal slds-fade-in-open");
        //$A.util.toggleClass(editModalBack1, "slds-backdrop slds-backdrop--open");
    },*/
/*    doChangeOwnerId : function(component, event, helper){
        // helper.showSpinner(component,event);
         component.set("v.checkSpinner1","Yes");
    var contractVehicle1 = component.get("v.contractVehicle");
    //alert(contractVehicle1.OwnerId);
         var action = component.get("c.changeOwnerIdFromHeader");
        
        action.setParams({
            recordId : component.get("v.recordId"),
            ownId : contractVehicle1.OwnerId
        });
        
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                
                 var contractVehicleOwnId = a.getReturnValue()
             //   var bid2 = component.get("v.bid");
             //   bid2.OwnerId=bidOwnId;
             //   component.get("v.bid",bid2);
               
                //alert(a.getReturnValue());
                // var compEvents = component.getEvent("headerChangeOwnerEventFired");
                //compEvents.setParams({ "newOwnerId" : contractVehicleOwnId });
               // compEvents.fire();
               // helper.hideSpinner(component,event);
                component.set("v.checkSpinner1","No");
                
                var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "Owner Updated successfully.",
                        "type":"success"
                    });
                    toastEvent.fire(); 
               
            } else if (a.getState() === "ERROR") {
                $A.log("Errors", a.getError());
            }
             var ownerModal = component.find("ownerModalId");
                var ownerModalBack = component.find("ownerModalbagroundId");
                $A.util.removeClass(ownerModal, "slds-modal slds-fade-in-open");
                $A.util.addClass(ownerModal, "slds-modal slds-fade-in-close");
                $A.util.removeClass(ownerModalBack, "slds-backdrop slds-backdrop--open");
                $A.util.addClass(ownerModalBack, "slds-backdrop slds-backdrop--close");
        });
        
        $A.enqueueAction(action);
    },    */
    
    doInitChatter: function(component, event, helper) {
        var action = component.get("c.checkChatter");
         action.setCallback(this, function(a) {
             if (a.getState() === "SUCCESS") {
                 if(a.getReturnValue() === false){
                    //alert('NO');
                     component.set("v.chatterCheck", 'NO');
                 }
                 else{
                     //alert('Yes');
                      component.set("v.chatterCheck", 'Yes');
                 }
             }
         }); 
        $A.enqueueAction(action);
    },
    
    //Edit Modal and Button methode
    showEditModal : function(component, event, helper) {
        var editModal1 = component.find("editModalId");
        var editModalBack1 = component.find("editModalbagroundId");
        //$A.util.toggleClass(editModal1, "slds-modal slds-fade-in-open");
        // $A.util.toggleClass(editModalBack1, "slds-backdrop slds-backdrop--open");
        $A.util.removeClass(editModal1, "slds-modal slds-fade-in-close");
        $A.util.addClass(editModal1, "slds-modal slds-fade-in-open");
        $A.util.removeClass(editModalBack1, "slds-backdrop slds-backdrop--close");
        $A.util.addClass(editModalBack1, "slds-backdrop slds-backdrop--open");
    },
    closeEditPopup : function(component, event, helper) {
        var editModal = component.find("editModalId");
        var editModalBack = component.find("editModalbagroundId");
        // $A.util.toggleClass(editModal, "slds-modal slds-fade-in-close");
        //$A.util.toggleClass(editModalBack, "slds-backdrop slds-backdrop--close");
        $A.util.removeClass(editModal, "slds-modal slds-fade-in-open");
        $A.util.addClass(editModal, "slds-modal slds-fade-in-close");
        $A.util.removeClass(editModalBack, "slds-backdrop slds-backdrop--open");
        $A.util.addClass(editModalBack, "slds-backdrop slds-backdrop--close");
    },
    
    //Case Modal and Button methode
    showCaseModal : function(component, event, helper) {
        var caseModal1 = component.find("caseModalId");
        var caseModalBack1 = component.find("caseModalbagroundId");
        //$A.util.toggleClass(caseModal1, "slds-modal slds-fade-in-open");
        //$A.util.toggleClass(caseModalBack1, "slds-backdrop slds-backdrop--open");
        $A.util.removeClass(caseModal1, "slds-modal slds-fade-in-close");
        $A.util.addClass(caseModal1, "slds-modal slds-fade-in-open");
        $A.util.removeClass(caseModalBack1, "slds-backdrop slds-backdrop--close");
        $A.util.addClass(caseModalBack1, "slds-backdrop slds-backdrop--open");
    },
    closeCasePopup : function(component, event, helper) {
        var caseModal = component.find("caseModalId");
        var caseModalBack = component.find("caseModalbagroundId");
        //$A.util.toggleClass(caseModal, "slds-modal slds-fade-in-close");
        //$A.util.toggleClass(caseModalBack, "slds-backdrop slds-backdrop--close");
        $A.util.removeClass(caseModal, "slds-modal slds-fade-in-open");
        $A.util.addClass(caseModal, "slds-modal slds-fade-in-close");
        $A.util.removeClass(caseModalBack, "slds-backdrop slds-backdrop--open");
        $A.util.addClass(caseModalBack, "slds-backdrop slds-backdrop--close");
    },
    
    //Note Modal and Button methode
    showNoteModal : function(component, event, helper) {
        var noteModal1 = component.find("noteModalId");
        var noteModalBack1 = component.find("noteModalbagroundId");
        // $A.util.toggleClass(noteModal1, "slds-modal slds-fade-in-open");
        // $A.util.toggleClass(noteModalBack1, "slds-backdrop slds-backdrop--open");
        $A.util.removeClass(noteModal1, "slds-modal slds-fade-in-close");
        $A.util.addClass(noteModal1, "slds-modal slds-fade-in-open");
        $A.util.removeClass(noteModalBack1, "slds-backdrop slds-backdrop--close");
        $A.util.addClass(noteModalBack1, "slds-backdrop slds-backdrop--open");
    },
    closeNotePopup : function(component, event, helper) {
        var noteModal = component.find("noteModalId");
        var noteModalBack = component.find("noteModalbagroundId");
        //$A.util.toggleClass(noteModal, "slds-modal slds-fade-in-close");
        //$A.util.toggleClass(noteModalBack, "slds-backdrop slds-backdrop--close");
        $A.util.removeClass(noteModal, "slds-modal slds-fade-in-open");
        $A.util.addClass(noteModal, "slds-modal slds-fade-in-close");
        $A.util.removeClass(noteModalBack, "slds-backdrop slds-backdrop--open");
        $A.util.addClass(noteModalBack, "slds-backdrop slds-backdrop--close");
    },
    doInit : function(component, event, helper) {
        //alert('hi '+component.get("v.recordId"));
        helper.getContractVehical(component, event, helper);
    },
   /* doInitNewOwnerId : function(component, event, helper) {
        var OwnId=event.getParam("newOwnerId");
        var bid1=component.get("v.bid");
        bid1.OwnerId=OwnId;
        alert(bid1.OwnerId);
        component.set("v.Bid",bid1);
    },*/
                                
 //Code For Follow/Following And UnFOllowing Button (StatfulButton)
 changeStatfulButton : function(component, event, helper) {
                                var flag = component.get("v.flag");
                                //alert(flag);
                                var button = component.find("statefullbuttonId");
                                if(flag=="false")
                                {
                                $A.util.removeClass(button, "slds-not-selected");
                                $A.util.addClass(button, "slds-is-selected"); 
                                component.set("v.flag","true");
                                }
                                else
                                {
                                $A.util.removeClass(button, "slds-is-selected");
                                $A.util.addClass(button, "slds-not-selected"); 
                                component.set("v.flag","false");
                                }
                                
                                //  $A.util.toggleClass(button, "slds-is-selected");
                                
                                /*  var span1 = component.find("spanFollowId");
        $A.util.toggleClass(span1, "slds-text-selected");
        var span2 = component.find("spanFollowingId");
        $A.util.toggleClass(span2, "slds-text-not-selected");*/
  },
//span action to hide DropDown after clicking on span
spanAction : function(component, event, helper) {
                                var dropDown = component.find("dropDownId");
                                $A.util.removeClass(dropDown, "slds-dropdown-trigger slds-dropdown-trigger--click slds-is-open");        
                                
                                },
                                //show drop down on click
                                showDropDown : function(component, event, helper) {
                                var dropDown = component.find("dropDownId");
                                $A.util.toggleClass(dropDown, "slds-dropdown-trigger slds-dropdown-trigger--click slds-is-open");
 },
                                
 //Delete Modal and Button methode
closeDeletePopup : function(component, event, helper) {
                                var deleteModal = component.find("deleteModalId");
                                var deleteModalBack = component.find("deleteModalbagroundId");
                                //$A.util.toggleClass(deleteModal, "slds-modal slds-fade-in-open");
                                //$A.util.toggleClass(deleteModalBack, "slds-backdrop slds-backdrop--open");
                                $A.util.removeClass(deleteModal, "slds-modal slds-fade-in-open");
                                $A.util.addClass(deleteModal, "slds-modal slds-fade-in-close");
                                $A.util.removeClass(deleteModalBack, "slds-backdrop slds-backdrop--open");
                                $A.util.addClass(deleteModalBack, "slds-backdrop slds-backdrop--close");
                                },
showDeletPopUp : function(component, event, helper) {
                                
                                var deleteModal1 = component.find("deleteModalId");
                                var deleteModalBack1 = component.find("deleteModalbagroundId");
                                //$A.util.toggleClass(deleteModal1, "slds-modal slds-fade-in-close");
                                //$A.util.toggleClass(deleteModalBack1, "slds-backdrop slds-backdrop--close");
                                $A.util.removeClass(deleteModal1, "slds-modal slds-fade-in-close");
                                $A.util.addClass(deleteModal1, "slds-modal slds-fade-in-open");
                                $A.util.removeClass(deleteModalBack1, "slds-backdrop slds-backdrop--close");
                                $A.util.addClass(deleteModalBack1, "slds-backdrop slds-backdrop--open");
                                },
                                
                                //Clone Modal and Button methode
showClonePopUp : function(component, event, helper) {
                                var cloneModal = component.find("cloneModalId");
                                var cloneModalBack = component.find("cloneModalbagroundId");
                                // $A.util.toggleClass(cloneModal, "slds-modal slds-fade-in-open");
                                //$A.util.toggleClass(cloneModalBack, "slds-backdrop slds-backdrop--open");
                                $A.util.removeClass(cloneModal, "slds-modal slds-fade-in-close");
                                $A.util.addClass(cloneModal, "slds-modal slds-fade-in-open");
                                $A.util.removeClass(cloneModalBack, "slds-backdrop slds-backdrop--close");
                                $A.util.addClass(cloneModalBack, "slds-backdrop slds-backdrop--open");
              /*     $A.createComponents([["aura:HTML", { tag: "div",HTMLAttributes: {"id": "mainDiv"} }],
                                                ["TM_TOMA:TM_DynamicCompoCloneFedCapOpp",{"recordId":component.getReference("v.recordId"), "recordTypeId":component.getReference("v.recordTypeId"),"closePopUp":component.getReference("c.closeclonePopup")}]],
                                                function(dataClone,status){
                                                    if(component.isValid() && status=="SUCCESS"){
                                                        // var modalBody=component.find("modalDataDiv").get("v.body");
                                                        // modalBody.push(data);
                                                        //component.find("atAGlanceContainer").get("v.body");
                                                        dataClone[0].set("v.body",dataClone[1]);
                                                        var body = component.find("cloneDiv").get("v.body");
                                                        body.push(dataClone[0]);
                                                        component.find("cloneDiv").set("v.body", body);  
                                                    }
                                                }); */
                        },
               closeclonePopup : function(component, event, helper) {
                                var cloneModal = component.find("cloneModalId");
                                var cloneModalBack = component.find("cloneModalbagroundId");
                                //  $A.util.toggleClass(cloneModal, "slds-modal slds-fade-in-close");
                                //  $A.util.toggleClass(cloneModalBack, "slds-backdrop slds-backdrop--close");
                                $A.util.removeClass(cloneModal, "slds-modal slds-fade-in-open");
                                $A.util.addClass(cloneModal, "slds-modal slds-fade-in-close");
                                $A.util.removeClass(cloneModalBack, "slds-backdrop slds-backdrop--open");
                                $A.util.addClass(cloneModalBack, "slds-backdrop slds-backdrop--close");
                                
                                var destructPopUp= component.find("cloneDiv");
                                destructPopUp.set("v.body",[]);
                                },
  
                    
                                //Owner Modal and Button methode
showOwnerPopUp : function(component, event, helper) {
                                var dropDown = component.find("dropDownId");
                                $A.util.removeClass(dropDown, "slds-dropdown-trigger slds-dropdown-trigger--click slds-is-open");   
                                var ownerModal = component.find("ownerModalId");
                                var ownerModalBack = component.find("ownerModalbagroundId");
                                // $A.util.toggleClass(ownerModal, "slds-modal slds-fade-in-open");
                                // $A.util.toggleClass(ownerModalBack, "slds-backdrop slds-backdrop--open");
                                $A.util.removeClass(ownerModal, "slds-modal slds-fade-in-close");
                                $A.util.addClass(ownerModal, "slds-modal slds-fade-in-open");
                                $A.util.removeClass(ownerModalBack, "slds-backdrop slds-backdrop--close");
                                $A.util.addClass(ownerModalBack, "slds-backdrop slds-backdrop--open");
                                },
closeOwnerPopup : function(component, event, helper) {
                                var ownerModal = component.find("ownerModalId");
                                var ownerModalBack = component.find("ownerModalbagroundId");
                                // $A.util.toggleClass(ownerModal, "slds-modal slds-fade-in-close");
                                // $A.util.toggleClass(ownerModalBack, "slds-backdrop slds-backdrop--close");
                                $A.util.removeClass(ownerModal, "slds-modal slds-fade-in-open");
                                $A.util.addClass(ownerModal, "slds-modal slds-fade-in-close");
                                $A.util.removeClass(ownerModalBack, "slds-backdrop slds-backdrop--open");
                                $A.util.addClass(ownerModalBack, "slds-backdrop slds-backdrop--close");
                                },
deleteButton : function(component, event, helper) {
                                // alert("deleteButton");
                                helper.deleteContractVehical(component, event, helper);
                                },
                                
                                
 doInitAtAGlanceData1 : function(component, event,helper) {
 helper.doInitAtAGlanceData(component, event, helper);                                      
 }
        })