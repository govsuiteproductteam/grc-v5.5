public class DESP_IIIEmailParser {
    
    private String emailSubject = '';
    private String name;
    private String taskOrderTitle;
    private String emailId;
    private String contactEmailId = '';
    private String contactName = '';
    private String contactTitle = '';
    private Contact con;
    
    public void parse(Messaging.InboundEmail email){
        String emailPlainText = email.plainTextBody;
        System.debug('\n***********Plain Text**********\n');
        if(String.isNotBlank(emailPlainText)){
            System.debug(emailPlainText);
            
            // Get Task Order Title from Email Subject
            emailSubject = email.subject;
            if(emailSubject.startsWith('Fwd:'))
                emailSubject = emailSubject.replace('Fwd:', '');
            if(emailSubject.startsWith('Re:'))
                emailSubject = emailSubject.replace('Re:', '');
            
            emailSubject = emailSubject.stripHtmlTags();
            if(String.isNotBlank(emailSubject))
                emailSubject = EmailHandlerHelper.checkValidString(emailSubject, 255);
            
            if(String.isNotBlank(emailSubject)){
                taskOrderTitle = emailSubject;
            }
            //End Subject
            
            //New Logic Date 06/04/2018
            // Get Contact Details 
            createContact(emailPlainText,email); 
            // End Contact Details
              
             
            //Old Logic
            //Integer index = emailPlainText.indexOf('Thank you');
            //String contactDetails = emailPlainText.substring(index);
            //if(index > 0){                                   
            //parseContactDetails(contactDetails);
            // }
           
        }
    }
    
    public Task_Order__c getTaskOrder(String toAddress){
        Task_Order__c taskOrder = new Task_Order__c();
        List<Contract_Vehicle__c> conVehicleList = [SELECT Id FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];
        
        if(!conVehicleList.isEmpty()){
            taskOrder.Contract_Vehicle__c = conVehicleList[0].Id;
            taskOrder.Task_Order_Title__c = taskOrderTitle;
            taskOrder.Release_Date__c = Date.today();
            taskOrder.Contract_Vehicle_picklist__c = 'DESP III - DESIGN AND ENGINEERING SUPPORT PROGRAM';
            
            try{
                if(con.id == null)
                    DMLManager.insertAsUser(con); 
            }catch(Exception e){
                System.debug(''+e.getMessage()+'\t'+e.getLineNumber());
            }
            
            if(con.Id != null) {           
                if(con.Title != null){
                    if(con.Title == 'Contract Specialist')
                        taskOrder.Contract_Specialist__c = con.Id;
                    if(con.Title == 'Contracting Officer')
                        taskOrder.Contracting_Officer__c = con.Id;
                }
            }
        }else{
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
        }
        
        return taskOrder;
    }
    
    /*public void parseContactDetails(String contactDetails){
        try{
            con = new Contact();
            if(String.isNotBlank(contactDetails)){
                //contactDetails = contactDetails.replaceAll('>\n', '').replaceAll('> ', '');
                contactDetails = contactDetails.replaceAll('> ', '');
                system.debug('contactDetails=='+contactDetails);
                List<String> contactDetailsSplit = contactDetails.split('\n');
                System.debug('contactDetailsSplit=='+contactDetailsSplit);
                if(contactDetailsSplit != null && contactDetailsSplit.size()>0){
                    String contactEmailIdRaw = contactDetailsSplit[5];
                    if(String.isNotBlank(contactEmailIdRaw)){
                        String[] contactEmailIdRawSplit = contactEmailIdRaw.split(' ');
                        if(contactEmailIdRawSplit != null){
                            if(contactEmailIdRawSplit[0].contains('@')){
                                contactEmailId = contactEmailIdRawSplit[0];
                            }
                        }
                    }
                    System.debug('contactEmailId='+contactEmailId);
                    if(String.isNotBlank(contactEmailId)){
                        //con = new Contact();
                        List<Contact> contactList = [Select id, Email, Title from Contact WHERE Email = :contactEmailId];
                        system.debug(contactList);
                        if(contactList != null && !contactList.isEmpty() ){//for existing contact
                            con = contactList[0];   
                        }else{//for new contact;
                            System.debug('else');
                            con.Is_FedTom_Contact__c = true;
                            con.Email = contactEmailId;
                            String[] namesplit = contactDetailsSplit[2].split(' ');
                            if(namesplit!= null && !namesplit.isEmpty()){
                                if(nameSplit.size()==2){
                                    con.firstName = nameSplit[0];
                                    con.lastName = nameSplit[1];
                                }else if(nameSplit.size()==3){
                                    con.firstName = nameSplit[0];
                                    con.lastName = nameSplit[2];
                                }else {
                                    con.lastName = nameSplit[0];
                                }
                            }
                            
                            if(contactDetails.contains('Contract Specialist')){
                                contactTitle = 'Contract Specialist';
                            }else if(contactDetails.contains('Contracting Officer')){
                                contactTitle = 'Contracting Officer';
                            }
                            System.debug('contactTitle=='+contactTitle);
                            con.Title = contactTitle;                            
                        }
                    }
                }
            }
        }catch(Exception e){
            System.debug(''+e.getMessage()+'\tLine Number = '+e.getLineNumber());
        }
        
    }*/
    
    private void createContact(String emailBody,Messaging.InboundEmail email){
        try{
            con = new Contact();            
            List<Contact> contactList = [Select id, Email, Title from Contact WHERE Email = :email.fromAddress];
            if(contactList != null && !contactList.isEmpty() ){//for existing contact
                con = contactList[0]; 
            }else{//for new contact
                String emailFromName = email.fromName;        
                String[] nameSplited = emailFromName.split(' ');
                con.FirstName = nameSplited[0];
                con.LastName = nameSplited[1];
                con.Email =  email.fromAddress;
                String contactRawString = emailBody.substringAfter('Thank you');
                System.debug('Raw String = '+contactRawString);
                
                if(contactRawString.trim().contains('Contract Specialist')){
                    contactTitle = 'Contract Specialist';
                }else if(contactRawString.trim().contains('Contracting Officer')){
                    contactTitle = 'Contracting Officer';
                }
                System.debug('contactTitle=='+contactTitle);
                con.Title = contactTitle.trim(); 
            }                                                                                  
        }catch(Exception ex){
            System.debug('Create Contact Error ='+ex.getMessage()+'\tLine Number = '+ex.getLineNumber());
        }                
    }
    
}