public class SurveyWrapper{
        @AuraEnabled
        public boolean isSelected{get;set;}
        @AuraEnabled
        public Survey__c survey {get;set;}
        
        public SurveyWrapper(Survey__c survey){
            this.survey = survey;
            this.isSelected = false;
        }
    }