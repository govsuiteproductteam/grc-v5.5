global class NETCENTSEmailHandler implements Messaging.InboundEmailHandler {
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:email.toAddresses.get(0)];
        if(!conVehicalList.isEmpty()){
            NETCENTSEmailParser parser = new NETCENTSEmailParser();
            parser.parse(email);//Email Parsing will be handle by parse method.
            if(parser.taskOrderNumber!=null && parser.taskOrderNumber.trim().length()>0){
                Task_Order__c taskOrder = EmailHandlerHelper.getExistTaskOrder(parser.taskOrderNumber, conVehicalList[0].ID);
                if(taskOrder == null){ // New Task Order
                    taskOrder = parser.getTaskOrder(email.toAddresses.get(0));  // Map Values for New Task Order
                    if(taskOrder != null){  
                        try{
                            DMLManager.insertAsUser(taskOrder); // Insert New Task Order
                            EmailHandlerHelper.insertBinaryAttachment(taskOrder, email);
                            EmailHandlerHelper.insertTextAttachment(taskOrder, email);
                            EmailHandlerHelper.insertActivityHistory(taskOrder, email);
                        }catch(Exception e){
                            System.debug('error message = '+e.getMessage());
                        }
                    }
                } else{ // For Existing Task Order 
                    Task_Order__c oldTaskOrder = taskOrder.clone(false,true,false,false);       
                    Task_Order__c updatedTaskOrder = parser.updateTaskOrder(taskOrder, email.toAddresses.get(0)); // Map updated values for Existed/Old Task Order
                    try{
                        EmailHandlerHelper.insertContractMods(updatedTaskOrder, oldTaskOrder, email); // Insert Contract Mods with Old & Updated values of Updated Task Order
                        update updatedTaskOrder;
                        EmailHandlerHelper.insertBinaryAttachment(updatedTaskOrder, email);
                        EmailHandlerHelper.insertTextAttachment(updatedTaskOrder, email);
                        EmailHandlerHelper.insertActivityHistory(updatedTaskOrder, email); 
                        system.debug('Task Order Already Exists');
                    }catch(Exception e){
                        System.debug('error message = '+e.getMessage()+'\tline number = '+e.getLineNumber());
                    }
                }
            }
        } else{
            System.debug('Contract Vehicle not found.');
        }
        return new Messaging.InboundEmailresult();
    }
}