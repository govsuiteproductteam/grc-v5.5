global with sharing class TM_RequestForInformationMsgcontroller {
    
    public string surveyId{get;set;}
    public List<Survey_Task__c> surveyTaskList{get;set;}
    public Survey_Task__c surveyTsk {get;set;}
    
    public TM_RequestForInformationMsgcontroller(ApexPages.StandardController controller) {
       if(Schema.sObjectType.Survey_Task__c.isAccessible()){
         surveyId = ApexPages.CurrentPage().getparameters().get('sid');
         try{
            if(surveyId != null){
                surveyTsk = new Survey_Task__c();
               
                surveyTaskList = new List<Survey_Task__c>();
                surveyTaskList = [SELECT id, Survey__c, Task_Order__c,Task_Order__r.Name, Contract_Number__c, Contract_Vehicle__c,Primary_Requirement__c,Task_Order_Contract_Type__c,Task_Order_Number__c FROM Survey_Task__c WHERE id =:surveyId  ];
                System.debug('surveyTaskList ==='+surveyTaskList );
                
                if(surveyTaskList.size()>0){
                 surveyTsk   = surveyTaskList[0];
                }
              
               
            }
        }
        catch(exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, e.getMessage()));
        }
      }  
    }
    
    public PageReference continueNew(){
        PageReference pg = new PageReference ('/apex/TM_SurveyResponsePage?sid='+surveyId);
        return pg;
    }
}