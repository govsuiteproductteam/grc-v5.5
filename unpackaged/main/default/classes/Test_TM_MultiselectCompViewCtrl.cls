@isTest
public class Test_TM_MultiselectCompViewCtrl {
    public static testMethod void testMultiselectCompViewCtrl(){
        TM_MultiselectCompViewCtrl multiCompView = new TM_MultiselectCompViewCtrl();
        multiCompView.selectedValues = 'Alaskan Native Corporation;Foreign-Owned Business';
        System.assertEquals(2, multiCompView.getValueList().size());
    }
    
}