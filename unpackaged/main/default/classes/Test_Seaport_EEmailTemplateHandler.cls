@isTest
public with sharing class Test_Seaport_EEmailTemplateHandler {
    public Static TestMethod void Seaport_EEmailTemplateHandlerTest (){
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Contact con = TestDataGenerator.createContact('avan', acc );
        insert con;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('T-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'seaport_e@1-1upouchqu9w4n7yrn4q3a9lwonud72chn6yx8smy58wtfrsbr3.61-fcydeae.na34.apex.salesforce.com';
        
        insert conVehi;
        
        //for new Task Order Creation  
        Messaging.InboundEmail email  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        email.plainTextBody = 'You have been invited to participate in the following event.\n'+  
            
            'Event Information:\n'+
            
            '*  Category:    SeaPort Enhanced\n'+
            '*  Name:        N00024-15-R-3403:1\n'+
            '*  Description: CODE 15 TRAINING, TRAINERS, AND HUMAN SYSTEMS INTEGRATION (Zone 1, Unrestricted, Activity: NUWC, NEWPORT DIVISION, UIC: N66604)\n'+
            'adsgd\n'+
            '*  Start Time:  2/9/2016 10:30:00 AM (GMT-05:00) Eastern Time (US & Canada)\n'+
            '*  Stop Time:   3/25/2016 2:00:00 PM (GMT-05:00) Eastern Time (US & Canada)\n'+
            '*  Creator:     CAROLYN GILLMAN (carolyn.gillman@navy.mil)\n'+
            
            '*  Contract Negotiator:     CAROLYN GILLMAN (carolyn.gillman@navy.mil)\n'+
            '*  Contracting Officer:     CAROLYN GILLMAN (carolyn.gillman@navy.mil)\n'+
            '*  Zone:                    1 - Northeast Zone\n'+
            '*  Set Aside:               SDVOSB\n'+
            '--------------------------------------------- \n'+
            'You may check in through the following link: https://auction.seaport.navy.mil/Bid/Login.aspx?RC=1_69560&pcode=N. You may also copy and paste the link into your web browser. \n'+
            
            'Thank you for your business with SeaPort.\n'+
            
            'Please do not reply directly to this e-mail. Contact SeaportSupport@aquilent.com for technical support.\n';
        email.fromAddress ='test@test.com';
        email.fromName = 'ABC XYZ';
        email.subject = 'RE: Task Order Request ID11150045 for JSP Support Services';
        email.toaddresses = new List<String>();
        email.toaddresses.add('seaport_e@1-1upouchqu9w4n7yrn4q3a9lwonud72chn6yx8smy58wtfrsbr3.61-fcydeae.na34.apex.salesforce.com');
        
        //For Updating existing task order with existing Contact
        Messaging.InboundEmail email1  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env1 = new Messaging.InboundEnvelope();
        email1.plainTextBody = 'You have been invited to participate in the following event.\n'+  
            
            'Event Information:\n'+
            
            '*  Category:    SeaPort Enhanced\n'+
            '*  Name:        N00024-15-R-3403:1\n'+
            '*  Description: CODE 15 TRAINING, TRAINERS, AND HUMAN SYSTEMS INTEGRATION (Zone 1, Unrestricted, Activity: NUWC, NEWPORT DIVISION, UIC: N66604)\n'+
            '*  Start Time:  2/9/2016 10:30:00 AM (GMT-05:00) Eastern Time (US & Canada)\n'+
            '*  Stop Time:   3/25/2016 2:00:00 PM (GMT-05:00) Eastern Time (US & Canada)\n'+
            '*  Creator:     CAROLYN GILLMAN (carolyn.gillman@navy.mil)\n'+
            
            '*  Contract Negotiator:     CAROLYN GILLMAN (carolyn.gillman@navy.mil)\n'+
            '*  Contracting Officer:     CAROLYN GILLMAN (carolyn.gillman@navy.mil)\n'+
            '*  Zone:                    1 - Northeast Zone\n'+
            '*  Set Aside:               SDVOSB\n'+
            '--------------------------------------------- \n'+
            'You may check in through the following link: https://auction.seaport.navy.mil/Bid/Login.aspx?RC=1_69560&pcode=N. You may also copy and paste the link into your web browser. \n'+
            
            'Thank you for your business with SeaPort.\n'+
            
            'Please do not reply directly to this e-mail. Contact SeaportSupport@aquilent.com for technical support.\n';
        email1.fromAddress ='test@test.com';
        email1.fromName = 'ABC XYZ';
        email1.subject = 'Task Order Request ID11150045 for JSP Support Services';
        email1.toaddresses = new List<String>();
        email1.toaddresses.add('seaport_e@1-1upouchqu9w4n7yrn4q3a9lwonud72chn6yx8smy58wtfrsbr3.61-fcydeae.na34.apex.salesforce.com');
        
        //For Updating existing task order with new Contact
        Messaging.InboundEmail email2  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env2 = new Messaging.InboundEnvelope();
        email2.plainTextBody = 'You have been invited to participate in the following event.\n'+  
            
            'Event Information:\n'+
            
            '*  Category:    SeaPort Enhanced\n'+
            '*  Name:        N00024-15-R-3403:1\n'+
            '*  Description: CODE 15 TRAINING, TRAINERS, AND HUMAN SYSTEMS INTEGRATION (Zone 1, Unrestricted, Activity: NUWC, NEWPORT DIVISION, UIC: N66604)\n'+
            '*  Start Time:  2/9/2016 10:30:00 AM (GMT-05:00) Eastern Time (US & Canada)\n'+
            '*  Stop Time:   3/25/2016 2:00:00 PM (GMT-05:00) Eastern Time (US & Canada)\n'+
            '*  Creator:     CAROLYN GILLMAN (carolyn.gillman@navy.mil)\n'+
            
            '*  Contract Negotiator:     CAROLYN GILLMAN (\n'+
            'carolyn.gillman@navy\n'+
            '.mil)\n'+
            '*  Contracting Officer:     CAROLYN GILLMAN (carolyn.gillman@navy.mil)\n'+
            '*  Zone:                    1 - Northeast Zone\n'+
            '*  Set Aside:               SDVOSB\n'+
            '--------------------------------------------- \n'+
            'You may check in through the following link: https://auction.seaport.navy.mil/Bid/Login.aspx?RC=1_69560&pcode=N. You may also copy and paste the link into your web browser. \n'+
            
            'Thank you for your business with SeaPort.\n'+
            
            'Please do not reply directly to this e-mail. Contact SeaportSupport@aquilent.com for technical support.\n';
        email2.fromAddress ='test1@test.com';
        email2.fromName = 'DEF XYZ';
        email2.subject = 'Task Order Request ID11150045 for JSP Support Services';
        email2.toaddresses = new List<String>();
        email2.toaddresses.add('seaport_e@1-1upouchqu9w4n7yrn4q3a9lwonud72chn6yx8smy58wtfrsbr3.61-fcydeae.na34.apex.salesforce.com');        
        
        Seaport_EEmailTemplateHandler seaportHandler = new Seaport_EEmailTemplateHandler();
        
        Test.startTest();
        Messaging.InboundEmailResult result = seaportHandler.handleInboundEmail(email, env);
        Messaging.InboundEmailResult result1 = seaportHandler.handleInboundEmail(email1, env1);
        Messaging.InboundEmailResult result2 = seaportHandler.handleInboundEmail(email2, env2);
        Test.stopTest();
        
        List<Task_Order__c> tOrderList = [SELECT Id FROM Task_Order__c];
        System.assertEquals(tOrderList.size(), 1);
    }
}