@isTest
public class TM_ContractNewPageControllerTest {
    public static testMethod void testContractNewPage(){
        Test.startTest();
        List<RecordType> rtypes = [Select Name, Id From RecordType where sObjectType='TM_TOMA__Contract_Vehicle__c' and isActive=true];
        string recId='';
        if(rtypes.size()>0)
        {
            recId=rtypes[0].Id;
        }
        
        Document document;
        document = new Document();
        document.Body = Blob.valueOf('');
        document.ContentType = 'txt';
        document.DeveloperName = 'Test_Dependent_Field_Document_contract';
        document.IsPublic = true;
        document.Name = 'Test Dependent Field Document contract';
        document.FolderId = [select id from folder where name = 'Dependent Field Doc'].id;
        insert document;
        DependentFieldSettingCtrl depFieldSett = new DependentFieldSettingCtrl();
        depFieldSett.recordTypeId=recId;
        depFieldSett.getsObjectTypeOptions();
        depFieldSett.objectType = 'Contract';
        depFieldSett.selectSObject();
        depFieldSett.go();
        depFieldSett.masterLabel = 'TM_TOMA__Task_Order_Types__c';
        depFieldSett.addMaster();
        depFieldSett.masterValue = 'All Type';
        depFieldSett.listIndex = 0;
        depFieldSett.dependentSields = 'TM_TOMA__Task_Order_Types__c';
        depFieldSett.addNewValue();
        depFieldSett.masterValue = 'All Type';
        depFieldSett.addField();
        depFieldSett.dependentSields = 'TM_TOMA__Ceiling__c';
        depFieldSett.addField();
        depFieldSett.dependentSields = 'TM_TOMA__Ceiling__c';
        depFieldSett.addField();
        
        depFieldSett.save();
        
        CLM_Tab__c tabSetting = TestDataGenerator.createCLMTab(recId);
        tabSetting.TabName__c='Internal Organisation';
        insert tabSetting;  
        CLM_Tab__c tabSetting1 = TestDataGenerator.createCLMTab(recId);
        tabSetting1.Order__c = 2;
        tabSetting1.TabName__c='Contract Description';
        insert tabSetting1;   
        CLM_Section__c sectionSetting = TestDataGenerator.createCLMSection(recId);
        insert sectionSetting;
        CLM_Section__c sectionSetting1 = TestDataGenerator.createCLMSection(recId);
        sectionSetting1.Order__c = 20;
        sectionSetting1.Is_Lightning_Component__c= true;
        sectionSetting1.Inline_Api_Name_Component_Api__c='TestComponent';
        insert sectionSetting1;
        CLM_Section__c sectionSetting2 = TestDataGenerator.createCLMSection(recId);
        sectionSetting2.Order__c = 30;
        sectionSetting2.Is_Contract__c = true;
        sectionSetting2.Is_InlineVF_Page__c= false;
        sectionSetting2.Inline_Api_Name_Component_Api__c='TestVFPage';
        insert sectionSetting2;
        CLM_FIeld__c fieldSetting = TestDataGenerator.createCLMField(recId);
        fieldSetting.ViewFieldApi__c='';
        fieldSetting.Required__c=true;
        fieldSetting.Required_Error_Message__c='Enter value for this field';
        fieldSetting.Order__c = 1001;
        insert fieldSetting;
        CLM_FIeld__c fieldSetting1 = TestDataGenerator.createCLMField(recId);
        fieldSetting1.FieldApiName__c = 'OwnerId';
        fieldSetting1.ViewFieldApi__c='OwnerId';
        fieldSetting1.Order__c = 1002;
        insert fieldSetting1;
        CLM_FIeld__c fieldSetting2 = TestDataGenerator.createCLMField(recId);
        fieldSetting2.FieldApiName__c = 'RecordTypeId';
        fieldSetting2.ViewFieldApi__c='RecordTypeId';
        fieldSetting2.Order__c = 1003;
        insert fieldSetting2;
        
        Account acc = TestDataGenerator.createAccount('testAccount');
        insert acc;
        
        Contract_Vehicle__c conVeh = TestDataGenerator.createContractVehicle('Test contract vehicle', acc);
        conVeh.recordTypeId = recId;
        conVeh.Contract_Type__c= TestDataGenerator.getPicklistValue('TM_TOMA__Contract_Vehicle__c','TM_TOMA__Contract_Type__c');
        conVeh.Contract_Status_Backend__c= TestDataGenerator.getPicklistValue('TM_TOMA__Contract_Vehicle__c','TM_TOMA__Contract_Status_Backend__c');
        insert conVeh;
        
        PageReference pageRef = Page.TM_ContractNewPage;        
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('RecordType', recId);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(conVeh);
        TM_ContractNewPageController contractNewPageController = new TM_ContractNewPageController(sc);
        System.assert(contractNewPageController.tabWrapperList.size()>0);
        contractNewPageController.saveContract1();
        contractNewPageController.saveAndActivate();
        System.assertEquals(contractNewPageController.contractVehicle.Contract_Status_Backend__c, 'Final');
        contractNewPageController.chandeMode();
        contractNewPageController.cancelEdit();
        contractNewPageController.changeOwner();
        contractNewPageController.changeRecordType();
        contractNewPageController.contractVehicle.Ceiling__c = 123123123123121233.123123123;
        contractNewPageController.saveContract1();
        System.assertEquals(contractNewPageController.mode, 'edit');
        contractNewPageController.contractVehicle.Ceiling__c = 123123123123121233.123123123;
        contractNewPageController.saveAndActivate();
        System.assertEquals(contractNewPageController.mode, 'edit');
        
        pageRef = Page.TM_ContractNewPage;        
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('RecordType', null); 
        // pageRef.getParameters().put('clone','1');
        // System.debug('pageRef'+pageRef);
        ApexPages.currentPage().getParameters().put('id', conVeh.Id);
        ApexPages.currentPage().getParameters().put('clone', '1');
        
        sc = new ApexPages.StandardController(conVeh);
        contractNewPageController = new TM_ContractNewPageController(sc);
        contractNewPageController.saveContract1();
        contractNewPageController.saveAndActivate();
        System.assertEquals('Final',contractNewPageController.contractVehicle.Contract_Status_Backend__c);

        Test.stopTest();
    }
}