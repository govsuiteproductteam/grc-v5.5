public class ContractWrapper_new {
        @AuraEnabled
        public List<CLMTabWraper> tabWrapList{get;set;}
        @AuraEnabled
        public Boolean hasNoTabs{get;set;}  
         @AuraEnabled
        public Boolean doUserHaveEditPermission{get;set;}
        @AuraEnabled
        public List<CLMTabListAndDataWrapper> allTabsNameWrpList{get;set;}
    }