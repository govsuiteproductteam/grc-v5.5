@isTest
public class TM_VFLookUpFilterCtrlTest {
    
    public static testMethod void testVFLookupFilterSearchLayout(){
        Contract_Vehicle__c contractVehicle = new Contract_Vehicle__c(Name='Test-CV');
        insert contractVehicle;
        
        Test.startTest();
        String lookupSearchLayoutInfo = '{"size":1,"totalSize":1,"done":true,"queryLocator":null,"entityTypeName":"SearchLayout","records":[{"attributes":{"type":"SearchLayout","url":"/services/data/v36.0/tooling/sobjects/SearchLayout/Account.Lookup"},"EntityDefinitionId":"Account","FieldsDisplayed":{"applicable":true,"fields":[{"apiName":"Name","label":"Account Name","sortable":true},{"apiName":"Parent.Name","label":"Parent Account","sortable":true},{"apiName":"BillingState","label":"Billing State/Province","sortable":true},{"apiName":"Website","label":"Website","sortable":true}]},"LayoutType":"Lookup"}]}';
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(lookupSearchLayoutInfo, 200));
        TM_VFLookUpFilterCtrl vfLookUpFilterCtrl = new TM_VFLookUpFilterCtrl();
        vfLookUpFilterCtrl.searchString = '*';
        vfLookUpFilterCtrl.recObjName = 'TM_TOMA__Contract_Vehicle__c';        
        vfLookUpFilterCtrl.refObjName = 'Account';
        vfLookUpFilterCtrl.recordflApi = 'TM_TOMA__Contracting_Agency__c';
        vfLookUpFilterCtrl.recdId = contractVehicle.Id;
        vfLookUpFilterCtrl.lookupFilter();
        Test.StopTest();
        
        
        System.assert(true);
    }
    
    public static testMethod void testVFLookupFilter(){
        List<Profile> profileList = [SELECT Id FROM Profile where Name = 'System Administrator' Limit 1];
        User usr = TestDataGenerator.createUser(null,TRUE,'test1', 'atest1','test1FielterLookup@gmail.com','tastFielterLookup@demo.com','atest1',profileList[0],'America/Los_Angeles','en_US','UTF-8','en_US');
        insert usr;
        
        List<RecordType> accRecordtypeList = [Select Name, Id From RecordType where sObjectType='Account' and isActive=true];
        String accRecordTypeId = '';
        if(accRecordtypeList != null && !accRecordtypeList.isEmpty())
        {
            accRecordTypeId = accRecordtypeList[0].Id;
        }
        List<RecordType> cvRecordtypeList = [Select Name, Id From RecordType where sObjectType='TM_TOMA__Contract_Vehicle__c' and isActive=true];
        String cvRecordTypeId = '';
        if(cvRecordtypeList != null && !cvRecordtypeList.isEmpty())
        {
            cvRecordTypeId = cvRecordtypeList[0].Id;
        }
        List<Account> accList = new List<Account>();
        Account acc1 = TestDataGenerator.createAccount('Abc');
        if(accRecordTypeId != '')
        acc1.RecordTypeId = accRecordTypeId;
        
        acc1.OwnerId = usr.Id;
        accList.add(acc1);
        Account acc2 = TestDataGenerator.createAccount('Xyz');
        acc2.Industry='Energy';
        acc2.ParentId = acc1.Id;
        acc2.RecordTypeId = accRecordTypeId;
        acc2.OwnerId = usr.Id;
        accList.add(acc2);
        Account acc3 = TestDataGenerator.createAccount('Asd');
        acc3.RecordTypeId = accRecordTypeId;
        acc3.OwnerId = usr.Id;
        accList.add(acc3);
        Account acc4 = TestDataGenerator.createAccount('Lmn');
        acc4.RecordTypeId = accRecordTypeId;
        acc4.OwnerId = usr.Id;
        accList.add(acc4);
        insert accList;
        List<Contract_Vehicle__c> contractVehicleList = new List<Contract_Vehicle__c>(); 
        Contract_Vehicle__c contractVehicle = new Contract_Vehicle__c(Name='Test-CV', RecordTypeId = cvRecordTypeId, OwnerId = usr.Id);
        contractVehicleList.add(contractVehicle);
        upsert contractVehicleList;
        Contract_Vehicle__c contractVehicle1 = new Contract_Vehicle__c(Name='Test-CV1', Account_Name__c = acc1.Id, Contracting_Agency__c = acc2.Id,
                                                                       Master_Contract_Vehicle__c = contractVehicle.Id, RecordTypeId = cvRecordTypeId, 
                                                                       OwnerId = usr.Id, Contract_Type__c = 'Multiple Contract Award Vehicle,Single Award Contract Vehicle');
        contractVehicleList.add(contractVehicle1);
        upsert contractVehicleList;
        
        Test.startTest();
        String lookupFilterItems = '"filterItems":[{"field":"Account.RecordTypeId","operation":"equals","value":"Federal","valueField":null},'+
            +'{"field":"Account.ParentId","operation":"equals","value":"","valueField":"$Source.TM_TOMA__Account_Name__c"},'+
            +'{"field":"$User.Id","operation":"equals","value":"","valueField":"TM_TOMA__Contract_Vehicle__c.OwnerId"},'+
            +'{"field":"$Source.RecordTypeId","operation":"equals","value":"Contract_Vehicle","valueField":null},'+
            +'{"field":"$Source.Name","operation":"contains","value":"","valueField":"Account.RecordTypeId"},'+
            +'{"field":"Account.CreatedDate","operation":"equals","value":"3/14/2018 4:36 AM","valueField":null},'+
            +'{"field":"Account.CreatedDate","operation":"equals","value":"","valueField":"$Source.CreatedDate"},'+
            +'{"field":"$Source.CreatedDate","operation":"equals","value":"","valueField":"Account.CreatedDate"},'+
            +'{"field":"$Source.TM_TOMA__Description__c","operation":"equals","value":"","valueField":"Account.Description"},'+
            +'{"field":"Account.AccountSource","operation":"notContain","value":"Phone Inquiry,Partner Referral","valueField":null},'+
            +'{"field":"Account.AccountSource","operation":"notContain","value":"Phone Inquiry","valueField":null},'+
            +'{"field":"Account.AccountSource","operation":"equals","value":"Web,Other","valueField":null},'+
            +'{"field":"Account.AccountSource","operation":"equals","value":"Web","valueField":null},'+
            +'{"field":"$Source.OwnerId","operation":"equals","value":"","valueField":"$User.LastModifiedById"},'+
            +'{"field":"$User.Id","operation":"equals","value":"","valueField":"$Source.OwnerId"},'+
            +'{"field":"$UserRole.Id","operation":"notEqual","value":"","valueField":"$Source.OwnerId"},'+
            +'{"field":"$Profile.Id","operation":"startsWith","value":"","valueField":"$Source.OwnerId"},'+
            +'{"field":"Account.Name","operation":"startsWith","value":"A","valueField":null},'+
            +'{"field":"$Source.Name","operation":"contains","value":"CV","valueField":null},'+
            +'{"field":"Account.OwnerId","operation":"equals","value":"","valueField":"$User.Id"},'+
            +'{"field":"Account.OwnerId","operation":"equals","value":"","valueField":"$UserRole.LastModifiedById"},'+
            +'{"field":"Account.OwnerId","operation":"equals","value":"","valueField":"$Profile.LastModifiedById"},'+
            +'{"field":"$User.LastModifiedById","operation":"notEqual","value":"","valueField":null},'+
            +'{"field":"$UserRole.LastModifiedById","operation":"startsWith","value":"G","valueField":null},'+
            +'{"field":"$Profile.LastModifiedById","operation":"equals","value":"","valueField":null},'+
            +'{"field":"Account.AccountSource","operation":"notContain","value":"","valueField":"$Source.TM_TOMA__Contract_Type__c"},'+
            +'{"field":"Account.AccountSource","operation":"equals","value":"","valueField":"$Source.TM_TOMA__Contract_Type__c"},'+
            +'{"field":"Account.Name","operation":"notContain","value":"","valueField":"$Source.Name"},'+
            +'{"field":"Account.AnnualRevenue","operation":"equals","value":"","valueField":null},'+
            +'{"field":"$Source.OwnerId","operation":"equals","value":"","valueField":"Account.OwnerId"}]';
        
        String lookupFilterInfo = '{"size":1,"totalSize":1,"done":true,"queryLocator":null,"entityTypeName":"FieldDefinition",'+
            '"records":[{"attributes":{"type":"FieldDefinition","url":"/services/data/v36.0/tooling/sobjects/FieldDefinition/01I36000002N9e5.00N3600000KIcv1"},"DeveloperName":"Contracting_Agency","Metadata":{"caseSensitive":null,"customDataType":null,"defaultValue":null,"deleteConstraint":"SetNull","deprecated":null,"description":null,"displayFormat":null,"displayLocationInDecimal":null,"encrypted":null,"escapeMarkup":null,"externalDeveloperName":null,"externalId":false,"formula":null,"formulaTreatBlanksAs":null,"globalPicklist":null,"inlineHelpText":"Account record type = federal","isConvertLeadDisabled":null,"isFilteringDisabled":null,"isNameField":null,"isSortingDisabled":null,"label":"Contracting Organization","length":null,'+
            '"lookupFilter":{"booleanFilter":null,'+ lookupFilterItems +','+
            '"active":true,"description":null,"errorMessage":null,"infoMessage":null,"isOptional":false},"maskChar":null,"maskType":null,"picklist":null,"populateExistingRows":null,"precision":null,"readOnlyProxy":null,"referenceTargetField":null,"referenceTo":"Account","relationshipLabel":"Contract Vehicles (Contracting Agency)","relationshipName":"Contracting_Agency","relationshipOrder":null,"reparentableMasterDetail":null,"required":false,"restrictedAdminField":null,"scale":null,"startingNumber":null,"stripMarkup":null,"summarizedField":null,"summaryFilterItems":null,"summaryForeignKey":null,"summaryOperation":null,"trackFeedHistory":false,"trackHistory":false,"trackTrending":false,"type":"Lookup","unique":null,"urls":null,"visibleLines":null,"writeRequiresMasterRead":null},"EntityDefinition":{"attributes":{"type":"EntityDefinition","url":"/services/data/v36.0/tooling/sobjects/EntityDefinition/01I36000002N9e5"},"FullName":"Contract_Vehicle__c"}}]}';
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(lookupFilterInfo, 200));
        TM_VFLookUpFilterCtrl vfLookUpFilterCtrl = new TM_VFLookUpFilterCtrl();
        vfLookUpFilterCtrl.searchString = '*';
        vfLookUpFilterCtrl.recObjName = 'TM_TOMA__Contract_Vehicle__c';        
        vfLookUpFilterCtrl.refObjName = 'Account';
        vfLookUpFilterCtrl.recordflApi = 'TM_TOMA__Contracting_Agency__c';
        vfLookUpFilterCtrl.recdId = contractVehicle1.Id;
        // Main method
        //vfLookUpFilterCtrl.lookupFilter();
        
        TM_VFLookUpFilterCtrl.checkConditionValue1(''+contractVehicle1.Id);
        //vfLookUpFilterCtrl.getIsOptionalRendered();
        //vfLookUpFilterCtrl.getIsOptionalrequiredRendered();
        vfLookUpFilterCtrl.clearSearchlookupFilter();
        //vfLookUpFilterCtrl.getJsonString();
        vfLookUpFilterCtrl.getResent();
        vfLookUpFilterCtrl.recordId = acc2.Id;
        vfLookUpFilterCtrl.getRecordLabel();
        Test.StopTest();
        
        System.assert(true);
    }
    
    
    
    // Field - Parent and ValueField - Parent
    public static testMethod void testVFLookupFilterFirstCase(){
        List<Profile> profileList = [SELECT Id FROM Profile where Name = 'System Administrator' Limit 1];
        User usr = TestDataGenerator.createUser(null,TRUE,'test1', 'atest1','test1@gmail.com','taskordercapture@demo.com','atest1',profileList[0],'America/Los_Angeles','en_US','UTF-8','en_US');
        insert usr;
        
        List<Account> accList = new List<Account>();
        Account acc1 = TestDataGenerator.createAccount('Abc');
        acc1.OwnerId = usr.Id;
       // acc1.Child_Organization_Won_Opportunities__c = 10;
        //acc1.BidRollup__c = 10000;
        acc1.AnnualRevenue = 20000;
      //   acc1.Organization_Won_Opportunities__c = 8;
        accList.add(acc1);
        Account acc2 = TestDataGenerator.createAccount('Xyz');
        acc2.OwnerId = usr.Id;
        accList.add(acc2);
        Account acc3 = TestDataGenerator.createAccount('Asd');
        acc3.OwnerId = usr.Id;
        accList.add(acc3);
        Account acc4 = TestDataGenerator.createAccount('Lmn');
        acc4.OwnerId = usr.Id;
        accList.add(acc4);
        insert accList;
        List<sObject> parentRecordList = (List<sObject>)accList;
        
        List<LookupFilterToolingAPI.FilterItem> filterItemList = new List<LookupFilterToolingAPI.FilterItem>();
        
       LookupFilterToolingAPI.FilterItem filterItem1 = new LookupFilterToolingAPI.FilterItem();
        filterItem1.field='Account.OwnerId';
        filterItem1.operation='equals';
        filterItem1.value='';
        filterItem1.valueField='$User.Id';
        filterItemList.add(filterItem1);
        
        LookupFilterToolingAPI.FilterItem filterItem2 = new LookupFilterToolingAPI.FilterItem();
        filterItem2.field='$Profile.CreatedById';
        filterItem2.operation='equals';
        filterItem2.value='';
        filterItem2.valueField='Account.OwnerId';
        filterItemList.add(filterItem2);
        
        LookupFilterToolingAPI.FilterItem filterItem3 = new LookupFilterToolingAPI.FilterItem();
        filterItem3.field='Account.OwnerId';
        filterItem3.operation='notEqual';
        filterItem3.value='';
        filterItem3.valueField='$Profile.LastModifiedById';
        filterItemList.add(filterItem3);
        
        LookupFilterToolingAPI.FilterItem filterItem4 = new LookupFilterToolingAPI.FilterItem();
        filterItem4.field='Account.NaicsCode';
        filterItem4.operation='notEqual';
        filterItem4.value='';
        filterItem4.valueField='Account.NaicsDesc';
        filterItemList.add(filterItem4);
        
        LookupFilterToolingAPI.FilterItem filterItem5 = new LookupFilterToolingAPI.FilterItem();
        filterItem5.field='Account.CreatedById';
        filterItem5.operation='startsWith';
        filterItem5.value='';
        filterItem5.valueField='Account.OwnerId';
        filterItemList.add(filterItem5);
        
        LookupFilterToolingAPI.FilterItem filterItem6 = new LookupFilterToolingAPI.FilterItem();
        filterItem6.field='Account.CreatedById';
        filterItem6.operation='contains';
        filterItem6.value='';
        filterItem6.valueField='Account.OwnerId';
        filterItemList.add(filterItem6);
        
        LookupFilterToolingAPI.FilterItem filterItem7 = new LookupFilterToolingAPI.FilterItem();
        filterItem7.field='Account.Name';
        filterItem7.operation='notContain';
        filterItem7.value='';
        filterItem7.valueField='Account.Site';
        filterItemList.add(filterItem7);              
        
        LookupFilterToolingAPI.FilterItem filterItem8 = new LookupFilterToolingAPI.FilterItem();
        filterItem8.field='Account.AnnualRevenue';
        filterItem8.operation='lessThan';
        filterItem8.value='';
        filterItem8.valueField='Account.AnnualRevenue';
        filterItemList.add(filterItem8); 
        
        LookupFilterToolingAPI.FilterItem filterItem9 = new LookupFilterToolingAPI.FilterItem();
        filterItem9.field='Account.AnnualRevenue';
        filterItem9.operation='lessOrEqual';
        filterItem9.value='';
        filterItem9.valueField='Account.AnnualRevenue';
        filterItemList.add(filterItem9);
        
        LookupFilterToolingAPI.FilterItem filterItem10 = new LookupFilterToolingAPI.FilterItem();
        filterItem10.field='Account.AnnualRevenue';
        filterItem10.operation='greaterThan';
        filterItem10.value='';
        filterItem10.valueField='Account.TM_TOMA__Organization_Won_Opportunities__c';
        filterItemList.add(filterItem10); 
        
        LookupFilterToolingAPI.FilterItem filterItem11 = new LookupFilterToolingAPI.FilterItem();
        filterItem11.field='Account.AnnualRevenue';
        filterItem11.operation='greaterOrEqual';
        filterItem11.value='';
        filterItem11.valueField='Account.TM_TOMA__Organization_Won_Opportunities__c';
        filterItemList.add(filterItem11); 
        
        LookupFilterToolingAPI.FilterItem filterItem12 = new LookupFilterToolingAPI.FilterItem();
        filterItem12.field='Account.TM_TOMA__Branch__c';
        filterItem12.operation='includes';
        filterItem12.value='';
        filterItem12.valueField='Account.AccountSource';
        filterItemList.add(filterItem12); 
        
        LookupFilterToolingAPI.FilterItem filterItem13 = new LookupFilterToolingAPI.FilterItem();
        filterItem13.field='Account.Type';
        filterItem13.operation='excludes';
        filterItem13.value='';
        filterItem13.valueField='Account.AccountSource';
        filterItemList.add(filterItem13); 
        
        LookupFilterToolingAPI.FilterItem filterItem14 = new LookupFilterToolingAPI.FilterItem();
        filterItem14.field='$UserRole.LastModifiedById';
        filterItem14.operation='equals';
        filterItem14.value='';
        filterItem14.valueField='Account.LastModifiedById';
        filterItemList.add(filterItem14);
        
        LookupFilterToolingAPI.FilterItem filterItem15 = new LookupFilterToolingAPI.FilterItem();
        filterItem15.field='$User.Id';
        filterItem15.operation='startsWith';
        filterItem15.value='';
        filterItem15.valueField='Account.CreatedById';
        filterItemList.add(filterItem15);
        
        LookupFilterToolingAPI.FilterItem filterItem16 = new LookupFilterToolingAPI.FilterItem();
        filterItem16.field='Account.Name';
        filterItem16.operation='startsWith';
        filterItem16.value='';
        filterItem16.valueField='$UserRole.Name';
        filterItemList.add(filterItem16);
        
        LookupFilterToolingAPI.FilterItem filterItem17 = new LookupFilterToolingAPI.FilterItem();
        filterItem17.field='Account.CreatedById';
        filterItem17.operation='equals';
        filterItem17.value='';
        filterItem17.valueField='Account.OwnerId';
        filterItemList.add(filterItem17);
        
        LookupFilterToolingAPI.FilterItem filterItem18 = new LookupFilterToolingAPI.FilterItem();
        filterItem18.field='$UserRole.LastModifiedById';
        filterItem18.operation='notEqual';
        filterItem18.value='';
        filterItem18.valueField='Account.OwnerId';
        filterItemList.add(filterItem18);
        
        LookupFilterToolingAPI.FilterItem filterItem19 = new LookupFilterToolingAPI.FilterItem();
        filterItem19.field='$UserRole.Name';
        filterItem19.operation='contains';
        filterItem19.value='';
        filterItem19.valueField='Account.Name';
        filterItemList.add(filterItem19);
        
       LookupFilterToolingAPI.FilterItem filterItem20 = new LookupFilterToolingAPI.FilterItem();
        filterItem20.field='Account.CreatedById';
        filterItem20.operation='contains';
        filterItem20.value='';
        filterItem20.valueField='$User.Id';
        filterItemList.add(filterItem20);
        
        LookupFilterToolingAPI.FilterItem filterItem21 = new LookupFilterToolingAPI.FilterItem();
        filterItem21.field='$UserRole.LastModifiedById';
        filterItem21.operation='notContain';
        filterItem21.value='';
        filterItem21.valueField='Account.OwnerId';
        filterItemList.add(filterItem21); 
        
        LookupFilterToolingAPI.FilterItem filterItem22 = new LookupFilterToolingAPI.FilterItem();
        filterItem22.field='Account.OwnerId';
        filterItem22.operation='notContain';
        filterItem22.value='';
        filterItem22.valueField='$Profile.LastModifiedById';
        filterItemList.add(filterItem22); 
        
        LookupFilterToolingAPI.FilterItem filterItem23 = new LookupFilterToolingAPI.FilterItem();
        filterItem23.field='$User.Id';
        filterItem23.operation='equals';
        filterItem23.value='';
        filterItem23.valueField='Account.CreatedById';
        filterItemList.add(filterItem23);
        
       LookupFilterToolingAPI.FilterItem filterItem24 = new LookupFilterToolingAPI.FilterItem();
        filterItem24.field='Account.CreatedById';
        filterItem24.operation='notEqual';
        filterItem24.value='';
        filterItem24.valueField='$User.';//Id
        filterItemList.add(filterItem24);
        
        LookupFilterToolingAPI.FilterItem filterItem25 = new LookupFilterToolingAPI.FilterItem();
        filterItem25.field='$Profile.Id';
        filterItem25.operation='equals';
        filterItem25.value='';
        filterItem25.valueField='Account.CreatedById';
        filterItemList.add(filterItem25);
        
        LookupFilterToolingAPI.FilterItem filterItem26 = new LookupFilterToolingAPI.FilterItem();
        filterItem26.field='$UserRole.Id';
        filterItem26.operation='notEqual';
        filterItem26.value='';
        filterItem26.valueField='Account.CreatedById';
        filterItemList.add(filterItem26);
        
        LookupFilterToolingAPI.FilterItem filterItem27 = new LookupFilterToolingAPI.FilterItem();
        filterItem27.field='Account.CreatedById';
        filterItem27.operation='equals';
        filterItem27.value='';
        filterItem27.valueField='$User.';//Id
        filterItemList.add(filterItem27);
        
        LookupFilterToolingAPI.FilterItem filterItem28 = new LookupFilterToolingAPI.FilterItem();
        filterItem28.field='Account.NaicsCode';
        filterItem28.operation='notEqual';
        filterItem28.value='';
        filterItem28.valueField='Account.';//NaicsCode
        filterItemList.add(filterItem28);
        
        LookupFilterToolingAPI.FilterItem filterItem29 = new LookupFilterToolingAPI.FilterItem();
        filterItem29.field='$User.Id';
        filterItem29.operation='startsWith';
        filterItem29.value='';
        filterItem29.valueField='Account.';//OwnerId
        filterItemList.add(filterItem29);
        
        LookupFilterToolingAPI.FilterItem filterItem30 = new LookupFilterToolingAPI.FilterItem();
        filterItem30.field='Account.Name';
        filterItem30.operation='startsWith';
        filterItem30.value='';
        filterItem30.valueField='$UserRole.';//Name
        filterItemList.add(filterItem30);
        
        LookupFilterToolingAPI.FilterItem filterItem31 = new LookupFilterToolingAPI.FilterItem();
        filterItem31.field='Account.CreatedById';
        filterItem31.operation='startsWith';
        filterItem31.value='';
        filterItem31.valueField='Account.';//OwnerId
        filterItemList.add(filterItem31);
        
        LookupFilterToolingAPI.FilterItem filterItem32 = new LookupFilterToolingAPI.FilterItem();
        filterItem32.field='$UserRole.Name';
        filterItem32.operation='contains';
        filterItem32.value='';
        filterItem32.valueField='Account.';//Name
        filterItemList.add(filterItem32);
        
        LookupFilterToolingAPI.FilterItem filterItem33 = new LookupFilterToolingAPI.FilterItem();
        filterItem33.field='Account.CreatedById';
        filterItem33.operation='contains';
        filterItem33.value='';
        filterItem33.valueField='$User.';//Id
        filterItemList.add(filterItem33);
        
        LookupFilterToolingAPI.FilterItem filterItem34 = new LookupFilterToolingAPI.FilterItem();
        filterItem34.field='$UserRole.LastModifiedById';
        filterItem34.operation='notContain';
        filterItem34.value='';
        filterItem34.valueField='Account.';//OwnerId
        filterItemList.add(filterItem34); 
        
        LookupFilterToolingAPI.FilterItem filterItem35 = new LookupFilterToolingAPI.FilterItem();
        filterItem35.field='Account.OwnerId';
        filterItem35.operation='notContain';
        filterItem35.value='';
        filterItem35.valueField='$Profile.';//OwnerId
        filterItemList.add(filterItem35);
        
        LookupFilterToolingAPI.FilterItem filterItem36 = new LookupFilterToolingAPI.FilterItem();
        filterItem36.field='Account.AnnualRevenue';
        filterItem36.operation='lessThan';
        filterItem36.value='';
        filterItem36.valueField='Account.';//Organization_Won_Opportunities__c
        filterItemList.add(filterItem36); 
        
        LookupFilterToolingAPI.FilterItem filterItem37 = new LookupFilterToolingAPI.FilterItem();
        filterItem37.field='Account.AnnualRevenue';
        filterItem37.operation='lessOrEqual';
        filterItem37.value='';
        filterItem37.valueField='Account.';//Organization_Won_Opportunities__c
        filterItemList.add(filterItem37);
        
        LookupFilterToolingAPI.FilterItem filterItem38 = new LookupFilterToolingAPI.FilterItem();
        filterItem38.field='Account.AnnualRevenue';
        filterItem38.operation='greaterThan';
        filterItem38.value='';
        filterItem38.valueField='Account.';//Organization_Won_Opportunities__c
        filterItemList.add(filterItem38); 
        
        LookupFilterToolingAPI.FilterItem filterItem39 = new LookupFilterToolingAPI.FilterItem();
        filterItem39.field='Account.AnnualRevenue';
        filterItem39.operation='greaterOrEqual';
        filterItem39.value='';
        filterItem39.valueField='Account.Organization_Won_Opportunities__c';
        filterItemList.add(filterItem39);
        
        LookupFilterToolingAPI.FilterItem filterItem40 = new LookupFilterToolingAPI.FilterItem();
        filterItem40.field='Account.TM_TOMA__Branch__c';
        filterItem40.operation='includes';
        filterItem40.value='';
        filterItem40.valueField='Account.';//Sub_Branch__c
        filterItemList.add(filterItem40); 
        
        LookupFilterToolingAPI.FilterItem filterItem41 = new LookupFilterToolingAPI.FilterItem();
        filterItem41.field='Account.Type';
        filterItem41.operation='excludes';
        filterItem41.value='';
        filterItem41.valueField='Account.';
        filterItemList.add(filterItem41); 
        
        for(Integer i = 0; i < filterItemList.size(); i++){
            FirstCaseLookupFilter.filter(parentRecordList, filterItemList[i], 'Account');
        }
    }
    
    // Field - Child & ValueField - Parent
    public static testMethod void testVFLookupFilterSixthCase(){
        List<Profile> profileList = [SELECT Id FROM Profile where Name = 'System Administrator' Limit 1];
        User usr = TestDataGenerator.createUser(null,TRUE,'test1', 'atest1','test1@gmail.com','taskordercapture@demo.com','atest1',profileList[0],'America/Los_Angeles','en_US','UTF-8','en_US');
        insert usr;
        
        List<RecordType> accRecordtypeList = [Select Name, Id From RecordType where sObjectType='Account' and isActive=true];
        String accRecordTypeId = '';
        if(accRecordtypeList != null && !accRecordtypeList.isEmpty())
        {
            accRecordTypeId = accRecordtypeList[0].Id;
        }
        System.debug('accRecordTypeId='+accRecordTypeId);
        
        List<Account> accList = new List<Account>();
        Account acc1 = TestDataGenerator.createAccount('Abc');
        if(accRecordTypeId != '')
        acc1.RecordTypeId = accRecordTypeId;
        
        acc1.OwnerId = usr.Id;
        acc1.AnnualRevenue = 20000;
        accList.add(acc1);
        insert accList;
        
        Contract_Vehicle__c contractVehicle = new Contract_Vehicle__c(Name='Test-CV',OwnerId=usr.Id,Contract_Amount__c=10000,
                                                                      Initial_Contract_Funding_at_Award__c=30000);
        insert contractVehicle;
        
        List<sObject> parentRecordList = (List<sObject>)accList;
        
        List<LookupFilterToolingAPI.FilterItem> filterItemList = new List<LookupFilterToolingAPI.FilterItem>();
        
        LookupFilterToolingAPI.FilterItem filterItem1 = new LookupFilterToolingAPI.FilterItem();
        filterItem1.field='$Source.OwnerId';
        filterItem1.operation='equals';
        filterItem1.value='';
        filterItem1.valueField='$User.LastModifiedById';
        filterItemList.add(filterItem1);
        
        LookupFilterToolingAPI.FilterItem filterItem2 = new LookupFilterToolingAPI.FilterItem();
        filterItem2.field='$Source.LastModifiedById';
        filterItem2.operation='equals';
        filterItem2.value='';
        filterItem2.valueField='Account.OwnerId';
        filterItemList.add(filterItem2);
        
        LookupFilterToolingAPI.FilterItem filterItem3 = new LookupFilterToolingAPI.FilterItem();
        filterItem3.field='$Source.OwnerId';
        filterItem3.operation='notEqual';
        filterItem3.value='';
        filterItem3.valueField='$Profile.LastModifiedById';
        filterItemList.add(filterItem3);
        
        LookupFilterToolingAPI.FilterItem filterItem4 = new LookupFilterToolingAPI.FilterItem();
        filterItem4.field='$Source.OwnerId';
        filterItem4.operation='notEqual';
        filterItem4.value='';
        filterItem4.valueField='$UserRole.LastModifiedById';
        filterItemList.add(filterItem4);
        
        LookupFilterToolingAPI.FilterItem filterItem5 = new LookupFilterToolingAPI.FilterItem();
        filterItem5.field='$Source.OwnerId';
        filterItem5.operation='startsWith';
        filterItem5.value='';
        filterItem5.valueField='$User.Id';
        filterItemList.add(filterItem5);
        
        LookupFilterToolingAPI.FilterItem filterItem6 = new LookupFilterToolingAPI.FilterItem();
        filterItem6.field='$Source.TM_TOMA__Contracting_Agency__c';
        filterItem6.operation='contains';
        filterItem6.value='';
        filterItem6.valueField='Account.Name';
        filterItemList.add(filterItem6);
        
        LookupFilterToolingAPI.FilterItem filterItem7 = new LookupFilterToolingAPI.FilterItem();
        filterItem7.field='$Source.TM_TOMA__End_User_Organization__c';
        filterItem7.operation='notContain';
        filterItem7.value='';
        filterItem7.valueField='Account.Name';
        filterItemList.add(filterItem7);
        
        LookupFilterToolingAPI.FilterItem filterItem8 = new LookupFilterToolingAPI.FilterItem();
        filterItem8.field='$Source.OwnerId';
        filterItem8.operation='includes';
        filterItem8.value='';
        filterItem8.valueField='Account.OwnerId';
        filterItemList.add(filterItem8);
        
        LookupFilterToolingAPI.FilterItem filterItem9 = new LookupFilterToolingAPI.FilterItem();
        filterItem9.field='$Source.TM_TOMA__End_User_Organization__c';
        filterItem9.operation='excludes';
        filterItem9.value='';
        filterItem9.valueField='Account.Name';
        filterItemList.add(filterItem9);
        
        LookupFilterToolingAPI.FilterItem filterItem10 = new LookupFilterToolingAPI.FilterItem();
        filterItem10.field='$Source.OwnerId';
        filterItem10.operation='contains';
        filterItem10.value='';
        filterItem10.valueField='$User.Id';
        filterItemList.add(filterItem10);
        
        LookupFilterToolingAPI.FilterItem filterItem11 = new LookupFilterToolingAPI.FilterItem();
        filterItem11.field='$Source.LastModifiedById';
        filterItem11.operation='notContain';
        filterItem11.value='';
        filterItem11.valueField='$User.Id';
        filterItemList.add(filterItem11);
        
        LookupFilterToolingAPI.FilterItem filterItem12 = new LookupFilterToolingAPI.FilterItem();
        filterItem12.field='$Source.OwnerId';
        filterItem12.operation='startsWith';
        filterItem12.value='';
        filterItem12.valueField='Account.OwnerId';
        filterItemList.add(filterItem12);
        
        LookupFilterToolingAPI.FilterItem filterItem13 = new LookupFilterToolingAPI.FilterItem();
        filterItem13.field='$Source.OwnerId';
        filterItem13.operation='excludes';
        filterItem13.value='';
        filterItem13.valueField='$User.Id';
        filterItemList.add(filterItem13);
        
        LookupFilterToolingAPI.FilterItem filterItem14 = new LookupFilterToolingAPI.FilterItem();
        filterItem14.field='$Source.OwnerId';
        filterItem14.operation='includes';
        filterItem14.value='';
        filterItem14.valueField='$User.Id';
        filterItemList.add(filterItem14);
        
        LookupFilterToolingAPI.FilterItem filterItem15 = new LookupFilterToolingAPI.FilterItem();
        filterItem15.field='$Source.OwnerId';
        filterItem15.operation='notEqual';
        filterItem15.value='';
        filterItem15.valueField='Account.LastModifiedById';
        filterItemList.add(filterItem15);
        
        LookupFilterToolingAPI.FilterItem filterItem16 = new LookupFilterToolingAPI.FilterItem();
        filterItem16.field='$Source.OwnerId';
        filterItem16.operation='equals';
        filterItem16.value='';
        filterItem16.valueField='Account.LastModifiedById';
        filterItemList.add(filterItem16);
        
        LookupFilterToolingAPI.FilterItem filterItem17 = new LookupFilterToolingAPI.FilterItem();
        filterItem17.field='$Source.CreatedDate';
        filterItem17.operation='lessThan';
        filterItem17.value='';
        filterItem17.valueField='Account.LastModifiedDate';
        filterItemList.add(filterItem17); 
        
        LookupFilterToolingAPI.FilterItem filterItem18 = new LookupFilterToolingAPI.FilterItem();
        filterItem18.field='$Source.TM_TOMA__Contract_Amount__c';
        filterItem18.operation='lessThan';
        filterItem18.value='';
        filterItem18.valueField='Account.AnnualRevenue';
        filterItemList.add(filterItem18); 
        
        LookupFilterToolingAPI.FilterItem filterItem19 = new LookupFilterToolingAPI.FilterItem();
        filterItem19.field='$Source.CreatedDate';
        filterItem19.operation='lessOrEqual';
        filterItem19.value='';
        filterItem19.valueField='Account.LastModifiedDate';
        filterItemList.add(filterItem19);
        
        LookupFilterToolingAPI.FilterItem filterItem20 = new LookupFilterToolingAPI.FilterItem();
        filterItem20.field='$Source.TM_TOMA__Contract_Amount__c';
        filterItem20.operation='lessOrEqual';
        filterItem20.value='';
        filterItem20.valueField='Account.AnnualRevenue';
        filterItemList.add(filterItem20);
        
        LookupFilterToolingAPI.FilterItem filterItem21 = new LookupFilterToolingAPI.FilterItem();
        filterItem21.field='$Source.LastModifiedDate';
        filterItem21.operation='greaterThan';
        filterItem21.value='';
        filterItem21.valueField='Account.CreatedDate';
        filterItemList.add(filterItem21); 
        
        LookupFilterToolingAPI.FilterItem filterItem22 = new LookupFilterToolingAPI.FilterItem();
        filterItem22.field='$Source.TM_TOMA__Initial_Contract_Funding_at_Award__c';
        filterItem22.operation='greaterThan';
        filterItem22.value='';
        filterItem22.valueField='Account.AnnualRevenue';
        filterItemList.add(filterItem22); 
        
        LookupFilterToolingAPI.FilterItem filterItem23 = new LookupFilterToolingAPI.FilterItem();
        filterItem23.field='$Source.LastModifiedDate';
        filterItem23.operation='greaterOrEqual';
        filterItem23.value='';
        filterItem23.valueField='Account.CreatedDate';
        filterItemList.add(filterItem23); 
        
        LookupFilterToolingAPI.FilterItem filterItem24 = new LookupFilterToolingAPI.FilterItem();
        filterItem24.field='$Source.TM_TOMA__Initial_Contract_Funding_at_Award__c';
        filterItem24.operation='greaterOrEqual';
        filterItem24.value='';
        filterItem24.valueField='Account.AnnualRevenue';
        filterItemList.add(filterItem24); 
        
        for(Integer i = 0; i < filterItemList.size(); i++){
            SixthCaseLookupFilter.filter(parentRecordList, filterItemList[i], 'Account', '');
            SixthCaseLookupFilter.filter(parentRecordList, filterItemList[i], 'Account', contractVehicle.OwnerId);
            SixthCaseLookupFilter.filter(parentRecordList, filterItemList[i], 'Account', ''+contractVehicle.Initial_Contract_Funding_at_Award__c);
        }
    }
    
}