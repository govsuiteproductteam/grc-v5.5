@IsTest
public class Test_TM_VFLookUpCtrl {
    public static testMethod void testVFLookUpCtrl(){
        account acc = TestDataGenerator.createAccount('Test Acc');
        insert acc;
        Contract_Vehicle__c conVechicle = TestDataGenerator.createContractVehicle('1234', acc);
        conVechicle.Name = 'Test Contract';
        insert conVechicle;
        TM_VFLookUpCtrl vfLookup = new TM_VFLookUpCtrl();
        vfLookup.refObjName = 'TM_TOMA__Contract_Vehicle__c';
        vfLookup.recordId = conVechicle.id;
        vfLookup.fieldIdVar='';
        vfLookup.recordName='';
       // System.assert(vfLookup.getJsonString() != null);
        string lab = vfLookup.getRecordLabel();
        //System.assertEquals('Test Contract', lab);
        System.assertEquals('', lab);
        vfLookup.getResent();
        vfLookup.refObjName = 'Group';
      //  vfLookup.getJsonString();
        vfLookup.getRecordLabel();
        //to cover code of catch
        vfLookup.refObjName = 'droup';
        vfLookup.getResent();
        vfLookup.refObjName = 'group';
        vfLookup.getResent();
        vfLookup.recordId = null;
        vfLookup.getRecordLabel();
        
    }
}