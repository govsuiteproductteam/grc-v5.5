public class RTEPRecordParser {
    private static String rtepNumber;
    private static String rtepTitle;
    private static String rtepStatus;
    private static String tepDueDate;
    private static String rtepDescription;
    private static String requringActivity;
    private static String acquisitionTrackerID;
    private static String marketResearchDueDate;
    private static String competitiveType;
    private static String competitiveProcedure;
    private static String coName;
    private static String csName;
    private static String uploadedDocument;
    private static String uploadsUrls = '';
    private static String revisionUrls = '';
    private static List<Contract_Mods__c> cmList = new List<Contract_Mods__c>();
    private static List<Attachment__c> attachList = new List<Attachment__c>();
    //public static Map<String,String> uploadLinksMap;//file link v/s file name
    //public static Map<String,String> revisionLinksMap;//file link v/s file name
    //private List<String> revisionURLList = new List<String>();
    
    Public static String parse(String htmlBody ){
        //StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'RTEPRecordDetails' LIMIT 1];
        //String htmlBody = sr.Body.toString();
        
        /**********parseRTEPDetails**********/
        String rtepNumberInput = htmlBody.substringBetween(RTEPRecordModel.RTEP_NUMBER_LABEL, RTEPRecordModel.RTEP_TITLE_LABEL);
        if(rtepNumberInput != null){
            rtepNumberInput  = rtepNumberInput.stripHtmlTags().Trim();
            System.debug('Dummy RTEP Number = '+rtepNumberInput);
            rtepNumber = rtepNumberInput;
        }
        System.debug('****'+rtepNumber);
        
        String rtepTitleInput = htmlBody.substringBetween(RTEPRecordModel.RTEP_TITLE_LABEL, RTEPRecordModel.RTEP_STATUS_LABEL);
        if(rtepTitleInput != null){
            rtepTitle = rtepTitleInput.stripHtmlTags().Trim();
        }
        System.debug('****'+rtepTitle);
        
        /*if(htmlBody.contains(RTEPRecordModel.TEP_DUE_DATE_LABEL)){
            rtepStatus = htmlBody.substringBetween(RTEPRecordModel.RTEP_STATUS_LABEL, RTEPRecordModel.TEP_DUE_DATE_LABEL).stripHtmlTags().Trim();
            System.debug('****'+rtepStatus);         
            tepDueDate = htmlBody.substringBetween(RTEPRecordModel.TEP_DUE_DATE_LABEL, RTEPRecordModel.RTEP_DESCRIPTION_LABEL).stripHtmlTags().Trim();
        }else{
            rtepStatus = htmlBody.substringBetween(RTEPRecordModel.RTEP_STATUS_LABEL, RTEPRecordModel.MARKET_RESEARCH_DUE_DATE_LABEL).stripHtmlTags().Trim();
        }*/
        if(htmlBody.contains(RTEPRecordModel.TEP_DUE_DATE_LABEL)){
            String rtepStatusInput = htmlBody.substringBetween(RTEPRecordModel.RTEP_STATUS_LABEL, RTEPRecordModel.TEP_DUE_DATE_LABEL);
            if(rtepStatusInput != null){
                rtepStatus = rtepStatusInput.stripHtmlTags().Trim();
                System.debug('****'+rtepStatus);     
            }   
            String tepDueDateInput =  htmlBody.substringBetween(RTEPRecordModel.TEP_DUE_DATE_LABEL, RTEPRecordModel.RTEP_DESCRIPTION_LABEL);
            if(tepDueDateInput != null){
                tepDueDate = tepDueDateInput.stripHtmlTags().Trim();
            }
        }else if(htmlBody.contains(RTEPRecordModel.MARKET_RESEARCH_DUE_DATE_LABEL)){
            String rtepStatusInput = htmlBody.substringBetween(RTEPRecordModel.RTEP_STATUS_LABEL, RTEPRecordModel.MARKET_RESEARCH_DUE_DATE_LABEL);
            if(rtepStatusInput != null){ 
                rtepStatus = htmlBody.substringBetween(RTEPRecordModel.RTEP_STATUS_LABEL, RTEPRecordModel.MARKET_RESEARCH_DUE_DATE_LABEL).stripHtmlTags().Trim();
            }
            String marketResearchDueDateInput = htmlBody.substringBetween(RTEPRecordModel.MARKET_RESEARCH_DUE_DATE_LABEL, RTEPRecordModel.RTEP_DESCRIPTION_LABEL);
            if(marketResearchDueDateInput != null){
                marketResearchDueDate = marketResearchDueDateInput.stripHtmlTags().Trim();
            }
            
        }
        System.debug('****'+tepDueDate);
        String rtepDescriptionInput = htmlBody.substringBetween(RTEPRecordModel.RTEP_DESCRIPTION_LABEL, RTEPRecordModel.RECURINNG_ACTIVITY_LABEL);
        if(rtepDescriptionInput != null){
            rtepDescription = rtepDescriptionInput.stripHtmlTags().Trim();
        }
        System.debug('****'+rtepDescription);
        String requringActivityInput = htmlBody.substringBetween(RTEPRecordModel.RECURINNG_ACTIVITY_LABEL, RTEPRecordModel.ACQUISITION_TRACKER_LABEL);
        if(requringActivityInput != null){
            requringActivity = requringActivityInput.stripHtmlTags().Trim();
        }
        System.debug('****'+requringActivity);
        
        String acquisitionTrackerIDInput = htmlBody.substringBetween(RTEPRecordModel.ACQUISITION_TRACKER_LABEL, RTEPRecordModel.COMPETITIVE_TYPE_LABEL);
        if(acquisitionTrackerIDInput != null){
            acquisitionTrackerID = acquisitionTrackerIDInput.stripHtmlTags().Trim();
        }
        System.debug('****'+acquisitionTrackerID);
        
        String competitiveTypeInput = htmlBody.substringBetween(RTEPRecordModel.COMPETITIVE_TYPE_LABEL, RTEPRecordModel.COMPETITIVE_PROCEDURE_LABEL);
        if(competitiveTypeInput != null){
            competitiveType = competitiveTypeInput.stripHtmlTags().Trim();
        }
        System.debug('****'+competitiveType);
        
        String competitiveProcedureInput = htmlBody.substringBetween(RTEPRecordModel.COMPETITIVE_PROCEDURE_LABEL, 'POC Information');
        if(competitiveProcedureInput  != null){
            competitiveProcedure = competitiveProcedureInput.stripHtmlTags().Trim();
        }
        System.debug('****'+competitiveProcedure);      
        
        
        /**********parsePOCInformation**********/
        String coNameInput = htmlBody.substringBetween(RTEPRecordModel.CO_NAME_LABEL, RTEPRecordModel.CS_NAME_LABEL);
        if(coNameInput != null){
            coName = coNameInput.stripHtmlTags().Trim();
        }
        System.debug('****'+coName);
        
        String csNameInput = htmlBody.substringBetween(RTEPRecordModel.CS_NAME_LABEL, 'Uploads');
        if(csNameInput != null){
            csName = csNameInput.stripHtmlTags().Trim();
        }
        System.debug('****'+csName);
        
        Task_Order__c taskOrder =  createTaskOrder();
        
        /**********parseUploads**********/
        uploadedDocument = htmlBody.substringBetween(RTEPRecordModel.UPLOADED_DOCUMENT_LABEL, 'Revisions');
        if(uploadedDocument  != null){
            uploadedDocument  = uploadedDocument.Trim();
            parseUploadFile(taskOrder );
        }
        
        /**********ParseRevision**********/
  
        String revision = htmlBody.substringBetween('Revisions', '</body>');
        if(revision != null){
            revision = revision.trim();
            parseRevision(revision,taskOrder );
        }
        
        /**********Create Task Order**********/
        
        //Add Logic herre
        Set<string> modNameSet = new Set<String>();
        
        for(Contract_Mods__c cm : cmList){
            modNameSet.add(cm.Modification_Name__c);
        }
        System.debug(LoggingLevel.Info,'Mod Name Set = '+modNameSet );
        List<Contract_Mods__c > contractModeList = new List<Contract_Mods__c >();
        if(Schema.sObjectType.Contract_Mods__c.isAccessible())
        	contractModeList = [Select Id,Modification_Name__c from Contract_Mods__c where Modification_Name__c IN: modNameSet And TaskOrder__c =: taskOrder.Id];
        Map<String,Contract_Mods__c > modNameMap = new Map<String,Contract_Mods__c >();
        for(Contract_Mods__c contractMod : contractModeList){
            modNameMap.put(contractMod.Modification_Name__c,contractMod);
        }
        for(Contract_Mods__c cm : cmList){
            Contract_Mods__c  conMod = modNameMap.get(cm.Modification_Name__c);
            if(conMod != null){
                cm.id = conMod.id;
            }
            else{
                cm.TaskOrder__c = taskOrder.Id;
            }
        }
        try{
            //upsert cmList;
            DMLManager.upsertAsUser(cmList);
        }
        catch(Exception ex){
        }
        System.debug('attachList = '+attachList  );
        
        Set<string> attachSet = new Set<String>();
        for(Attachment__c  attach : attachList ){
            attachSet.add(attach.Name);
        }
        System.debug('attachSet = '+attachSet);
        List<Attachment__c> attachmentList = new List<Attachment__c>();
        if(Schema.sObjectType.Attachment__c.isAccessible())
        	attachmentList = [Select Id,Name from Attachment__c where Name IN: attachSet and Task_Order__c=: taskOrder.Id];
        Map<String,Attachment__c> attachmentMap = new Map<String,Attachment__c>();
        for(Attachment__c attachment: attachmentList ){
            attachmentMap.put(attachment.Name,attachment);
        }
        System.debug('attachmentMap = '+attachmentMap );
        for(Attachment__c attach : attachList ){
            Attachment__c attachment = attachmentMap.get(attach.Name);
            if(attachment != null){
                attach.id = attachment.id;
            }
            else{
                attach.Task_Order__c = taskOrder.Id;
            }
        }
         try{
            //database.upsert(attachList) ;
            DMLManager.upsertAsUser(attachList);
        }
        catch(Exception ex){
        }
        
        System.debug('task Order Number = '+taskOrder.Name);
        System.debug('task Order Number = '+taskOrder.Due_Date__c);
        System.debug('uploadsUrls'+uploadsUrls);
        System.debug('revisionUrls'+revisionUrls);
        return uploadsUrls+'SPLITURL'+revisionUrls;
    }
    
    private static void parseUploadFile(Task_Order__c taskOrder ){        
        if(uploadedDocument.contains('</tr>')){
            List<String> uploadTableList = uploadedDocument.split('</tr>');
            for(String data : uploadTableList){
                String link = '';
                String fileName = '';
                System.debug('data = '+data);
                if(data.contains('<a')){
                    String linkInfo = data.substringBetween('<a', '/a>');
                    System.debug('linkInfo = '+linkInfo);
                    if(linkInfo.contains('href')){
                        if(linkInfo.contains('href=\''))
                            link = linkInfo.substringBetween('href=\'','\'').trim();
                        if(linkInfo.contains('href="'))
                            link = linkInfo.substringBetween('href="','"').trim();
                        System.debug('link = '+link);
                        fileName = linkInfo.substringBetween('<span','</span>');
                        fileName = '<span '+fileName+'</span>';
                        fileName = fileName.stripHtmlTags().trim();
                        System.debug('fileName = '+fileName);
                        //uploadLinksMap.put(link, fileName);
                        link = 'www.voa.va.gov/ECTOS/'+link; 
                        Attachment__c attach = new Attachment__C();
                        attach.Name = fileName ;
                        attach.URL__C = link;
                        attachList.add(attach);
                        String ext = getExtension(fileName);
                        if(uploadsUrls != ''){
                            uploadsUrls += 'SPLITURL'+fileName+'SPACESPLIT'+ext+'SPACESPLIT'+link+'SPACESPLIT'+taskOrder.id; 
                        }else{
                            uploadsUrls += fileName+'SPACESPLIT'+ext+'SPACESPLIT'+link+'SPACESPLIT'+taskOrder.id; 
                        }
                    }
                    //System.debug('linkInfo = '+linkInfo);
                }
            }
        }
    }
    
    private static void parseRevision(String revision,Task_Order__c taskOrder ){
        //System.debug('revision'+revision);
        if(revision.contains('<tbody>')){
            revision = revision.substringBetween('<tbody>', '</tbody>');
            System.debug('*******revision'+revision);
            //Rivision table content multiple rows separated by <tr> and every <tr> has column separated by <td>
            if(revision.contains('</tr>')){
                List<String> revisionList = revision.split('</tr>');
                for(String revisionRecord : revisionList){                
                    createContractMod(revisionRecord);
                }        
            }
        }
    }
    
    private static Task_Order__c createTaskOrder(){
        List<Task_Order__c> taskOrderList;
        if(Schema.sObjectType.Task_Order__c.isAccessible())
        taskOrderList = [Select Id,Name,Task_Order_Title__c, RTEP_Status__c,Due_Date__c ,Description__c,Requiring_Activity__c,Acquisition_Tracker_ID__c ,
                                             Contract_Type__c,Competitive_Procedure__c,Contracting_Officer__c,Contract_Specialist__c,Release_Date__c,Market_Research_Due_Date__c 
                                             FRom Task_Order__c where Name =: rtepNumber and Contract_Vehicle__r.SINs__c = 'ATOMS RETURN ALL' Limit 1];
        Task_Order__c  taskOrder;
        if(taskOrderList != null && !taskOrderList.isEmpty()){
            taskOrder = taskOrderList[0];
        }
        else{
            taskOrder = new Task_Order__c ();
        }
        Task_Order__c  oldTaskOrder = taskOrder.clone(true,true,false,false);
        Boolean isChanged = false;
        if(rtepNumber  != null){
            taskOrder.Name = rtepNumber; 
        }
        if(taskOrder.Task_Order_Title__c != rtepTitle){
            taskOrder.Task_Order_Title__c = rtepTitle;
            isChanged = true;
        }
        if(taskOrder.RTEP_Status__c !=  rtepStatus){
            taskOrder.RTEP_Status__c =  rtepStatus ;
            isChanged = true;
        }
        
        DateTimeHelper dtTime = new DateTimeHelper();
        if(tepDueDate  != null){
            DateTime dueDateTime = dtTime.getDateTime(tepDueDate);
            if(dueDateTime != null && taskOrder.Due_Date__c !=  dueDateTime.date()){
                taskOrder.Due_Date__c =  dueDateTime.date() ; 
                isChanged = true;
            }
        }
        else{
            if(taskOrder.Due_Date__c != null){
                taskOrder.Due_Date__c = null;
                isChanged = true;
            }
        }
        
        if(tepDueDate  != null){            
            DateTime dueDateTime = dtTime.getDateTimeSpecialCase(tepDueDate+' EST');
            if(dueDateTime != null && taskOrder.Proposal_Due_DateTime__c !=  dueDateTime){
                taskOrder.Proposal_Due_DateTime__c =  dueDateTime; 
                isChanged = true;
            }
        }
        else{
            if(taskOrder.Proposal_Due_DateTime__c != null){
                taskOrder.Proposal_Due_DateTime__c = null;
                isChanged = true;
            }
        }
        
       //Newly Added Start
        if(marketResearchDueDate  != null){            
            DateTime dueDateTime = dtTime.getDateTime(marketResearchDueDate);
            if(dueDateTime != null && taskOrder.Market_Research_Due_Date__c !=  dueDateTime.date()){
                taskOrder.Market_Research_Due_Date__c =  dueDateTime.date() ; 
                isChanged = true;
            }
        }else{
            if(taskOrder.Market_Research_Due_Date__c != null){
                taskOrder.Market_Research_Due_Date__c = null;
                isChanged = true;
            }
        }
        //End
        //"taskOrder.Description__c" is updated to 10000 character, changes as per updated field length
        //String trimmedDescription =  '';
        if(rtepDescription != null && rtepDescription.length()>10000)
            rtepDescription = rtepDescription.substring(0, 10000);
        
        if(taskOrder.Description__c !=  rtepDescription){            
            taskOrder.Description__c =  rtepDescription;
            isChanged = true;
        }
        
        if(taskOrder.Requiring_Activity__c !=  requringActivity){
            taskOrder.Requiring_Activity__c =  requringActivity;
            isChanged = true;
        }
        if(taskOrder.Acquisition_Tracker_ID__c !=  acquisitionTrackerID){
            taskOrder.Acquisition_Tracker_ID__c =  acquisitionTrackerID ;
            isChanged = true;
        }
        if(taskOrder.Contract_Type__c !=  competitiveType){
            taskOrder.Contract_Type__c =  competitiveType;
            isChanged = true;
        }
        if(competitiveProcedure != null){
            competitiveProcedure = competitiveProcedure.trim();
            competitiveProcedure = competitiveProcedure.replaceAll('\n','');
        }
        
        if(taskOrder.Competitive_Procedure__c !=   competitiveProcedure){
            taskOrder.Competitive_Procedure__c =   competitiveProcedure;
            isChanged = true;
        }
        
        if(coName != null){
            Id coContactactId = insertContact(coName);
            if(taskOrder.Contracting_Officer__c != coContactactId ){
                taskOrder.Contracting_Officer__c = coContactactId ;
                isChanged = true;
            }
        }
        if(csName != null){
            Id csContactId = insertContact(csName);
            if(taskOrder.Contract_Specialist__c != csContactId ){
                taskOrder.Contract_Specialist__c = csContactId ;
                isChanged = true;
            }
        }
        
        
        
        try{
            if(taskOrder.Contract_Vehicle__c == null){
                if(Schema.sObjectType.Contract_Vehicle__c.isAccessible()){
                    List<Contract_Vehicle__c> contractVehicleList = [Select Id,SINs__c From Contract_Vehicle__c where SINs__c =: 'ATOMS RETURN ALL'];
                    if(!contractVehicleList.isEmpty())
                    	taskOrder.Contract_Vehicle__c = contractVehicleList.get(0).Id;//temp
                }
            }
            if(isChanged ){
                try{
                    if(taskOrder.id != null){
                        DMLManager.upsertAsUser(taskOrder);
                        //upsert taskOrder;
                        GSAParseHelper.insertContractModForVoa(taskOrder,oldTaskOrder);
                    }
                    else{
                        taskOrder.Release_Date__c = Date.today();
                        DMLManager.upsertAsUser(taskOrder);
                        //upsert taskOrder;
                    }
                    
                }
                catch(Exception ex){
                }
            }
        }
        catch(Exception ex){
        }
        // uploadedDocument = 
        return taskOrder;
    }
    
    /*private string parseContactName(String conName){
Contact Details is coming like given below Example
Ginty, Matthew
Matthew.Ginty@va.gov
732-440-9700

List<String> resultConName = conName.trim().split('\n');
if(resultConName.size()>0){
if(resultConName[0].contains(',')){
List<String> splitname = resultConName[0].trim().split(',');
if(splitname.size() == 2){
String finalName = splitname[1] +' '+ splitname[0];
return finalName;
}
}
}
return '';
}

private Id insertContact(String conName){     
List<Contact> conList = [SELECT id , name FROM Contact where name =: conName];
if(conList != null && !conList.isEmpty()){
return conList[0].id;
}else{
Contact con = new Contact();
con.LastName = conName;     
insert con;     
return con.id;
}      
}*/
    
    private static Id insertContact(String conDetail){ 
        List<String> resultConName = conDetail.trim().split('\n');
        String lastName;
        String firstName;      
        String mobNum ;
        String email;
        if(resultConName.size()>0){
            if(resultConName[0].contains(',')){
                List<String> splitname = resultConName[0].trim().split(',');
                if(splitname.size() == 2){
                    firstName = splitname[1];
                    lastName = splitname[0];
                }
            }else{
                lastName = resultConName[0];
            }
            if(resultConName[1] !=  null && resultConName[1].trim().length()>0){
                email = resultConName[1].trim();
            } 
            
            if(resultConName[2] != null && resultConName[2].trim().length()>0){
                mobNum = resultConName[2].trim();
            } 
            
            String emailStr = getContactEmail(email).trim();
            system.debug('email=='+ email );
            List<Contact> conList;
            if(Schema.sObjectType.Contact.isAccessible())
            	conList = [SELECT id , name FROM Contact where email=: emailStr];
            if(conList!=null && !conList.isEmpty()){
                return conList[0].id;
            }else{
                Contact con = new Contact();
                if(firstName != null){
                    con.firstName = firstName;
                }
                con.LastName = lastName ;  
                con.email = emailStr;
                try{
                    con.phone = mobNum ;
                }catch(Exception e){
                    
                }
                con.Is_FedTom_Contact__c = true;
                DMLManager.insertAsUser(con);
                //insert con;     
                return con.id;
            }   
        }
        return null; 
    }
    
    
    private static String getContactEmail(String parseString){
        String regex = '([a-zA-Z0-9_\\-\\.]+)@(((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3}))';       
        Pattern myPattern = Pattern.compile(regex );    
        Matcher myMatcher = myPattern.matcher(parseString);
        if(myMatcher.find()){
            return myMatcher.group();
        }
        return null;
    }
    
    private static List<Contract_Mods__c> createContractMod(String revisionRecord){        
        String revNum = '';
        String revTitle = '';
        String revisionPostedDate = '';
        String tepDueDateChanged = '';
        String link = '';
        String fileName = '';
        if(revisionRecord.contains('</td>')){
            List<String> recordInfo = revisionRecord.split('</td>');
            System.debug('recordInfo size = '+recordInfo.size());            
            if(recordInfo.size() == 7){
                revNum = recordInfo[0]+'</td>';
                revNum = revNum.stripHtmlTags().trim();
                System.debug('********************revNum = '+revNum.stripHtmlTags().trim());
                revTitle = recordInfo[1]+'</td>';
                revTitle = revTitle.stripHtmlTags().trim();
                System.debug('********************revTitle = '+revTitle.stripHtmlTags().trim());
                revisionPostedDate = recordInfo[2]+'</td>';
                revisionPostedDate = revisionPostedDate.stripHtmlTags().trim();
                System.debug('********************revTitle = '+revisionPostedDate.stripHtmlTags().trim());
                tepDueDateChanged = recordInfo[3]+'</td>';
                tepDueDateChanged = tepDueDateChanged.stripHtmlTags().trim();
                System.debug('********************revTitle = '+tepDueDateChanged.stripHtmlTags().trim());
                // System.debug('num'+data.stripHtmlTags().trim());
                if(recordInfo[4].contains('<a href')){
                    
                    if(recordInfo[4].contains('href=\''))
                        link = recordInfo[4].substringBetween('href=\'','\'').trim();
                    if(recordInfo[4].contains('href="'))
                        link = recordInfo[4].substringBetween('href="','"').trim();
                    System.debug('*link = '+link);
                     
                    fileName = recordInfo[4].substringBetween('<a','</a>');
                    fileName = '<a '+fileName+'</a>';
                    fileName = fileName.stripHtmlTags().trim();
                    System.debug('*fileName = '+fileName);
                    Attachment__c attach = new Attachment__C();
                    if(fileName.length() < 79){
                        attach.Name = fileName ;
                    }
                    else{
                        attach.Name = fileName.subString(0,79) ;
                    }
                    link = 'www.voa.va.gov/ECTOS/'+link;    
                    attach.URL__C = link;
                    attachList.add(attach);
                    //revisionLinksMap.put(link, fileName);     
                    String ext = getExtension(fileName);           
                         
                    if(revisionUrls != ''){
                        revisionUrls += 'SPLITURL'+fileName+'SPACESPLIT'+ext+'SPACESPLIT'+link; 
                    }else{
                        revisionUrls += fileName+'SPACESPLIT'+ext+'SPACESPLIT'+link; 
                    }
                    
                }
                
                Contract_Mods__c contractMod = new Contract_Mods__c();
                contractMod.RecordTypeId = Schema.SObjectType.Contract_Mods__c.getRecordTypeInfosByName().get('Contract Mod').getRecordTypeId();
                if(revNum != null && revNum.length()>0)
                    contractMod.Modification_Name__c = revNum;
                if(revTitle != null && revTitle.length()>0) 
                    contractMod.Modification__c = revTitle;         
                //contractMod.Task_Order_Title__c = 'tt = '+ recordInfo[3];
                if(revisionPostedDate != null && revisionPostedDate.length()>0){
                    //contractMod.Task_Order_Title__c = tepDueDateChanged;
                    try{
                        DateTime dtTime = new DateTimeHelper().getDateTime(revisionPostedDate );
                        if(dtTime != null){
                            contractMod.New_Proposal_Due_Date_Time__c = dtTime ;
                        }
                        //if(DateTime.parse(tepDueDateChanged) !=null)
                         //   contractMod.New_Proposal_Due_Date_Time__c = DateTime.parse(tepDueDateChanged);
                    }
                    catch(Exception ex){
                    }
                }                
                if(contractMod != null)
                cmList.add(contractMod);
            }
            if(recordInfo.size() == 3){
                
                if(recordInfo[0].contains('<a href')){
                    if(recordInfo[0].contains('href=\''))
                        link = recordInfo[0].substringBetween('href=\'','\'').trim();
                    if(recordInfo[0].contains('href="'))
                        link = recordInfo[0].substringBetween('href="','"').trim();
                    link = recordInfo[0].substringBetween('href=\'','\'').trim();
                    link = 'www.voa.va.gov/ECTOS/'+link;
                    System.debug('*link = '+link);
                    fileName = recordInfo[0].substringBetween('<a','</a>');
                    fileName = '<a '+fileName+'</a>';
                    fileName = fileName.stripHtmlTags().trim();
                    System.debug('*fileName = '+fileName.stripHtmlTags().trim());
                    Attachment__c attach = new Attachment__C();
                    if(fileName.length() < 79){
                        attach.Name = fileName ;
                    }
                    else{
                        attach.Name = fileName.subString(0,79) ;
                    }
                    
                    attach.URL__C = link;
                    attachList.add(attach);
                    //revisionLinksMap.put(link, fileName);          
                }
            }            
        }
        return cmList;
    }
    
    private static String getExtension(String fileName){
        if(fileName.contains('.'))
            return fileName.substringAfterLast('.');
        return '';
    }
    
}