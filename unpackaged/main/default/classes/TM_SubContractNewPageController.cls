public class TM_SubContractNewPageController {
    // newlly added 19-04
    public static Map<String, Schema.SObjectField> fieldsMapAccessible{get;set;}
    public static Map<String,Schema.SObjectField> conVehicleFieldTypeMap{get;set;}
    public static Map<String,String> fieldsMapWithRefranceFieldName{get;set;} //newlly added 12-04
    static{
        conVehicleFieldTypeMap =  Schema.SObjectType.SubContract__c.fields.getMap();
        fieldsMapAccessible=Schema.getGlobalDescribe().get('TM_TOMA__Subcontract__c').getDescribe().SObjectType.getDescribe().fields.getMap();
        //newlly added 19-04
        fieldsMapWithRefranceFieldName = new Map<String,String>();
        for(Schema.SObjectField sobjectField : fieldsMapAccessible.values())
        {
            String tempField = sobjectField.getDescribe().getName();
            String typeOFField = sobjectField.getDescribe().getType()+'';
            if(typeOFField == 'REFERENCE')
            {
                fieldsMapWithRefranceFieldName.put(tempField, sobjectField.getDescribe().getRelationshipName()+'.Name');
            }
            
        }
    }
    //End
    
    public List<TabWrapper> tabWrapperList{get; set;}//TM_SubContractDetailPageController.TabWrapper
    private String recordTypeId;
    Public String allTab{get;set;} 
    public Map<String,Map<String,FieldDependancyHelper.DependantField>> parentMap{get; set;}   
    public String cloneFlag ;
    public String isValidLiecence{get;set;}
    public String isCloneString;//newlly added
    private Boolean isRecCloned;//newlly added
    public SubContract__c subContractVehicle;//contractVehicle
    private ApexPages.StandardController controller;
    public Boolean isRecClone{get; set;}//newly added for redirection on cancel @ 03/06/2019
    
    //for nooverride
    private Id toId;
    private String pageReferEdit;
    public String isValidFedTomLiecence{get;set;}
    //End
    
    public TM_SubContractNewPageController(ApexPages.StandardController controller){ 
        isValidLiecence = LincenseKeyHelper.checkLicensePermitionForFedCLM();
        isValidFedTomLiecence = LincenseKeyHelper.checkLicensePermitionForFedTOM();//newlly added for no override
        if(isValidLiecence == 'Yes'){
            isRecCloned=false;//newlly added
            isRecClone = false;
            this.controller = controller;
            recordTypeId = apexpages.currentpage().getparameters().get('RecordType');
            
            if(recordTypeId == null){
                String subContractId = apexpages.currentpage().getparameters().get('id');
                cloneFlag = ApexPages.CurrentPage().getparameters().get('clone');
                if(subContractId  != null){
                    SubContract__c subContract = [Select Id,RecordTypeId From SubContract__c where id=: subContractId ];
                    
                    if(subContract.RecordTypeId != null){
                        recordTypeId  = ((String)subContract.RecordTypeId);
                    }
                }
                else{
                   /* List<RecordType> rtypes = [Select Name, Id From RecordType where sObjectType='TM_TOMA__Subcontract__c' and isActive=true];
                    //Id devRecordTypeId = Schema.SObjectType.TM_TOMA__Subcontract__c.getRecordTypeInfosByName().get('Master Subcontract').getRecordTypeId();
                   if(rtypes != null) 
                    recordTypeId = rtypes[0].Id;*/
                    System.debug('@@@@@@@@@ 21-05');
                      List<Id> ValidRecTypeId = New List<Id>();
                    List<String> recordTypeNameList = new List<String>();
                    Schema.DescribeSObjectResult R = TM_TOMA__Subcontract__c.SObjectType.getDescribe();
                    List<Schema.RecordTypeInfo> RT = R.getRecordTypeInfos();
                    for( Schema.RecordTypeInfo recordType : RT )
                    {
                        if(recordType.isAvailable())
                        { 
                            ValidRecTypeId.add(recordType.getRecordTypeId());
                            
                        }
                    }
                    if(ValidRecTypeId != null && ! ValidRecTypeId.isEmpty()){
                        String recordTypeAvailable = ValidRecTypeId[0];
                        List<RecordType> tempRecList = [SELECT Id,Name FROM RecordType WHERE isActive = true AND Id=:recordTypeAvailable];
                        if(tempRecList != null && ! tempRecList.isEmpty()){
                            recordTypeId = recordTypeAvailable; 
                        }
                    }
                    
                }
            }
            
            
            List<CLM_FIeld__c> fieldCustomSettingList = [Select Id,Draft_Required__c,FieldApiName__c,FieldLabel__c,HelpText__c,Is_Contract__c,
                                                         Order__c,RecordType_Id__c,  Required__c,TabNumber__c,ViewFieldApi__c ,Required_Error_Message__c,Create_Blank_Space__c,Use_Custom_lookup__c
                                                         From CLM_FIeld__c where  Is_Contract__c = false order by TabNumber__c asc , Order__c asc];
            Set<String> fieldApiNameSet = new Set<String>();
            fieldApiNameSet.add('TM_TOMA__Is_Clone__c');
            for(CLM_FIeld__c clmField : fieldCustomSettingList ){
                if(clmField.Create_Blank_Space__c != true){//newlly added 15-05-2018 && clmField.Create_Blank_Space__c != true
                    fieldApiNameSet.add(clmField.FieldApiName__c );
                    //newlly added 19-04
                    if(fieldsMapWithRefranceFieldName.containsKey(''+clmField.FieldApiName__c))
                    {
                        fieldApiNameSet.add(fieldsMapWithRefranceFieldName.get(''+clmField.FieldApiName__c));
                    }
                }
            }
            if(!Test.isRunningTest())
                controller.addFields(new List<String>(fieldApiNameSet));
            //contractVehicle = (Contract_Vehicle__c)controller.getRecord();
            init();
            controller.getRecord().put('RecordTypeId',recordTypeId);
            controller.getRecord().put('OwnerId',UserInfo.getUserId());
            
            isCloneString = ApexPages.CurrentPage().getparameters().get('clone'); 
            if(isCloneString!=null && isCloneString =='1'){
                isRecCloned=true;
                isRecClone = true;
            }
            if(isRecCloned){
                subContractVehicle = (Subcontract__c)controller.getRecord();
                subContractVehicle.TM_TOMA__Is_Clone__c=true;
                //newlly added 19-04
               // Map<String, Schema.SObjectField> fieldsMapAccessible = Schema.getGlobalDescribe().get('TM_TOMA__Subcontract__c').getDescribe().SObjectType.getDescribe().fields.getMap();
                
                if(fieldsMapAccessible != null && ! fieldsMapAccessible.isEmpty()){
                    for(String fieldApi : fieldApiNameSet){
                        Schema.SObjectField objField =fieldsMapAccessible.get(fieldApi);
                        if(objField!= null && objField.getDescribe().isUnique() && objField.getDescribe().isAccessible()){
                            system.debug('fieldApi--'+fieldApi);
                            subContractVehicle.put(fieldApi,null);
                        }
                    }
                }
                
            } 
            
        }
         else if(isValidFedTomLiecence == 'Yes'){
            toId = ApexPages.CurrentPage().getparameters().get('id');
            Schema.DescribeSObjectResult r = Subcontract__c.sObjectType.getDescribe();
            String keyPrefix = r.getKeyPrefix();
            //Boolean isLightningPage = UserInfo.getUiThemeDisplayed() == 'Theme4d';
           // System.debug('isLightningPage = '+isLightningPage);
            if(toId!=Null){
            //   if(isLightningPage){
             //       System.debug('isLightningPage = '+isLightningPage);
             //       pageReferEdit = '/one/one.app?source=alohaHeader#/sObject/'+toId+'/edit';
             //   }else{
                     pageReferEdit = '/'+toId+'/e?retURL=/'+toId+'&nooverride=1'; 
            //    }                              
                System.debug('pageReferEdit '+pageReferEdit);
            }
            else if(toId==Null){
                string recId = ApexPages.CurrentPage().getparameters().get('RecordType');
                pageReferEdit = '/'+keyPrefix+'/e?retURL=/'+keyPrefix+'/o&nooverride=1';
                if(recId!=Null)
                {
                    pageReferEdit = pageReferEdit+'&RecordType='+recId;
                }
            }
        }
    }
    
     //newlly added for no override 02-05
    public PageReference setRedirectEdit(){
        System.debug('pageReferEdit '+pageReferEdit);
        if(pageReferEdit != null)
        {
            PageReference pg = new Pagereference(pageReferEdit);
            pg.setRedirect(true);
            return pg;
        }
        return null;
    }
    //End
    
    private void init(){
        
        getFieldDependancyData();
        this.parentMap = FieldDependancyHelper.parentMap;
        //System.debug('parentMap :- '+parentMap);
        //isEdit = false;
        //To get contract Vehicle information from database as per record id
        
        //Declair and define Tabwrapper list to retun it to invoking function.
        tabWrapperList = new List<TabWrapper>();//TM_SubContractDetailPageController.TabWrapper
        
        
        //if(!contractVehicleList.isEmpty()){
        //String recordTypeId = contractVehicle.recordTypeId;
        if(recordTypeId != null){
            if(recordTypeId.length() > 15){
                recordTypeId = recordTypeId.substring(0,15);
            }
        }
        //All the custom setting for specific record type in order based on tab and order no.
        //we take the data in order to keep it in proper order while creaeting data set.
        //newlly added Create_Blank_Space__c 16-05-2018
        List<CLM_FIeld__c> fieldCustomSettingList = [Select Id,Draft_Required__c,FieldApiName__c,FieldLabel__c,HelpText__c,Is_Contract__c,
                                                     Order__c,RecordType_Id__c,  Required__c,TabNumber__c,ViewFieldApi__c,Required_Error_Message__c,Create_Blank_Space__c,Use_Custom_lookup__c
                                                     From CLM_FIeld__c where RecordType_Id__c =: recordTypeId And Is_Contract__c = false order by TabNumber__c asc , Order__c asc];
        
        //System.debug('fieldCustomSettingList = '+fieldCustomSettingList);
        //This map is to store data according to tabSequnce there are mutiple section and then according to section order there are multiple field in section
        //we create data set as Map<TabNumber,Map<SectionOrder,List<NumberOfFieldsIn section>>>
        
        Map<Integer,Map<Integer,List<CLM_FIeld__c>>> fieldCustomSettingMap = new Map<Integer,Map<Integer,List<CLM_FIeld__c>>>();
        for(CLM_FIeld__c fieldCustomSetting : fieldCustomSettingList){
            Map<Integer,List<CLM_FIeld__c>> fieldInternalMap = fieldCustomSettingMap.get((Integer)fieldCustomSetting.TabNumber__c);
            if(fieldInternalMap == null){
                fieldInternalMap = new Map<Integer,List<CLM_FIeld__c>>();
            }
            
            if(fieldCustomSetting.Order__c != null){
                //WE have section order in 10, 20 , 30... format and field order in 1001,1002,2001..., where last 2 digit specify squence 
                //and first remainig specify section order so thats why we divide field order with 100 to get section order.
                Integer sectionNumber = ((Integer)fieldCustomSetting.Order__c)/100;
                List<CLM_FIeld__c> internalFieldList = fieldInternalMap.get(sectionNumber);
                if(internalFieldList == null){
                    internalFieldList = new List<CLM_FIeld__c>();
                }
                internalFieldList.add(fieldCustomSetting);
                fieldInternalMap.put(sectionNumber,internalFieldList);
                fieldCustomSettingMap.put((Integer)fieldCustomSetting.TabNumber__c,fieldInternalMap);
            }
            //fieldApiNameSet.add(fieldCustomSetting.FieldApiName__c);
            
        }
        //To get all section custom setting for specific recordType and also it is in order to maintain data structure.
        List<CLM_Section__c> sectionCustomSettingList = [Select Id,BackgroundColor__c,Height__c,Inline_Api_Name_Component_Api__c,Is_Contract__c,Is_InlineVF_Page__c,
                                                         Is_Lightning_Component__c, No_Of_Column__c,Order__c,RecordType_Id__c,SectionName__c,TabNumber__c,Width__c,AppApi__c 
                                                         From CLM_Section__c where RecordType_Id__c =: recordTypeId And Is_Contract__c = false And Is_InlineVF_Page__c = false And 
                                                         Is_Lightning_Component__c = false order by TabNumber__c asc , Order__c asc];
        //To keep section List according to tabOrder
        //System.debug('sectionCustomSettingList = '+sectionCustomSettingList);
        Map<Integer,List<CLM_Section__c>> sectionMap = new Map<Integer,List<CLM_Section__c>>();
        for(CLM_Section__c sectionCustomSetting : sectionCustomSettingList){
            List<CLM_Section__c> internalSectionList = sectionMap.get((Integer)sectionCustomSetting.TabNumber__c);
            if(internalSectionList == null){
                internalSectionList = new List<CLM_Section__c>();
            }
            internalSectionList.add(sectionCustomSetting);
            sectionMap.put((Integer)sectionCustomSetting.TabNumber__c,internalSectionList);
        }
        List<CLM_Tab__c> tabCustomSettingList = [Select Id,Is_Contract__c,Order__c,RecordType_Id__c,TabName__c,BackgroundColor__c
                                                 From CLM_Tab__c where RecordType_Id__c =: recordTypeId And Is_Contract__c = false order by Order__c asc];
        //Here we have apply a logic to  create a data set as we have data set that we have mutiple tab and inside one tab there are mutiple sectin
        //and inside section there are either  multiple fields or components or VF page.
        //System.debug('tabCustomSettingList = '+tabCustomSettingList);
        Integer index = 1;
        allTab = '';
         //newly added for Fake tab @ 21/05/2019
        if(tabCustomSettingList.isEmpty()){
            tabCustomSettingList = new List<CLM_Tab__c>(); 
            tabCustomSettingList.add(new CLM_Tab__c(Order__c=0,TabName__c='Fake Tab',BackgroundColor__c='Red'));
        }  
        for(CLM_Tab__c tabCustomSetting : tabCustomSettingList){
            Integer tabOrderNumber = (Integer)tabCustomSetting.Order__c;
            if(tabOrderNumber != null){
                List<CLM_Section__c> internalSectionList = sectionMap.get((Integer)tabCustomSetting.Order__c);   
                Map<Integer,List<CLM_FIeld__c>> fieldInternalMap = fieldCustomSettingMap.get((Integer)tabCustomSetting.Order__c);
                if(fieldInternalMap == null){
                    fieldInternalMap = new Map<Integer,List<CLM_FIeld__c>>();
                }
                if(internalSectionList != null){
                    //TM_SubContractDetailPageController.TabWrapper tabWrap = new TM_SubContractDetailPageController.TabWrapper(tabCustomSetting,internalSectionList,fieldInternalMap);
                   TabWrapper tabWrap = new TabWrapper(tabCustomSetting,internalSectionList,fieldInternalMap);
                    //if there is no section in tab then we don't want to show empty tab on page layout so we will not add in data set.
                    if(tabWrap.sectionWrapperList != null && !tabWrap.sectionWrapperList.isEmpty()){
                        tabWrap.sequence = index;
                        tabWrapperList.add(tabWrap);
                    }
                }
            }
            allTab += 'tabedit'+index+',';
            index++;
        }
        if(allTab.length() > 0){
            allTab = allTab.substring(0,allTab.length()-1);
        }
        System.debug('tabWrapperList = '+tabWrapperList);       
    }
     public PageReference cancelCustomOnClone(){
       // System.debug('recordId  recordId '+recordId+ 'subContractVehicle.Id '+subContractVehicle.Id );
        PageReference pageRef = new pagereference('/'+subContractVehicle.Id);
        pageRef.setRedirect(true);
        return pageRef;
    }
    public pagereference saveContract1(){
        System.debug('saveContract1');
        
        try{
            subContractVehicle = (SubContract__c)controller.getRecord();
           // if(subContractVehicle.Subcontract_Status_Backend__c != null || subContractVehicle.Subcontract_Status_Backend__c != null){
                subContractVehicle.Subcontract_Status_Backend__c = 'Draft';
            //}
            if(cloneFlag != null && cloneFlag == '1'){
                subContractVehicle.id = null;
            }
            upsert subContractVehicle;
            //return controller.save();
            return new pagereference('/'+subContractVehicle.Id);
        }
        catch(Exception ex){
            System.debug('Exception = '+ex);
            // mode = 'edit';
            String errorMessage = ''+ex;
            errorMessage  = errorMessage.substringAfter('first error:');
            //System.debug(LoggingLevel.info,'errorMessage  = '+errorMessage  );
            Boolean found = false;
            if(ApexPages.hasMessages()){
                // System.debug(LoggingLevel.info,'errorMessage  = '+errorMessage  );
                ApexPages.Message[] messageArray = ApexPages.getMessages();
                SObjectType accountType = Schema.getGlobalDescribe().get('TM_TOMA__SubContract__c');
                Map<String,Schema.SObjectField> mfields = accountType.getDescribe().fields.getMap();
                
                //System.debug(LoggingLevel.info,'mfields = '+mfields );
                String fieldApiName = errorMessage.subStringBetween(': [',']');
                //System.debug(LoggingLevel.info,'fieldApiName = '+fieldApiName );
                Schema.SObjectField field = mfields.get(fieldApiName );
                //System.debug(LoggingLevel.info,'field = '+field );
                if(field != null){
                    String fieldLabel = field.getDescribe().getLabel();
                    System.debug(LoggingLevel.info,'fieldLabel = '+fieldLabel );
                    for(ApexPages.Message msg : messageArray ){
                        String msgFieldlabel = msg.getComponentLabel();
                        if(msgFieldlabel != null && msgFieldlabel == fieldLabel  ){
                            found = true;
                        }
                    }
                }
                
                
                
            }
            if(!found){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,''+errorMessage );
                ApexPages.addMessage(myMsg);
            }
            return Null;
        }
    }
    
    public pagereference saveAndActivate(){
        System.debug('saveAndActivate');
        subContractVehicle = (SubContract__c)controller.getRecord();
        try{
            subContractVehicle.Subcontract_Status_Backend__c = 'Final';
            if(cloneFlag != null && cloneFlag == '1'){
                subContractVehicle.id = null;
            }
            upsert subContractVehicle;
            // return controller.save();
            return new pagereference('/'+subContractVehicle.Id);
        }
        catch(Exception ex){
            System.debug('Exception = '+ex+' '+ ex.getLineNumber());
            System.debug('Exception = '+ex);
            // mode = 'edit';
            String errorMessage = ''+ex;
            errorMessage  = errorMessage.substringAfter('first error:');
            //System.debug(LoggingLevel.info,'errorMessage  = '+errorMessage  );
            Boolean found = false;
            if(ApexPages.hasMessages()){
                // System.debug(LoggingLevel.info,'errorMessage  = '+errorMessage  );
                ApexPages.Message[] messageArray = ApexPages.getMessages();
                SObjectType accountType = Schema.getGlobalDescribe().get('TM_TOMA__SubContract__c');
                Map<String,Schema.SObjectField> mfields = accountType.getDescribe().fields.getMap();
                
                //System.debug(LoggingLevel.info,'mfields = '+mfields );
                String fieldApiName = errorMessage.subStringBetween(': [',']');
                //System.debug(LoggingLevel.info,'fieldApiName = '+fieldApiName );
                Schema.SObjectField field = mfields.get(fieldApiName );
                //System.debug(LoggingLevel.info,'field = '+field );
                if(field != null){
                    String fieldLabel = field.getDescribe().getLabel();
                    System.debug(LoggingLevel.info,'fieldLabel = '+fieldLabel );
                    for(ApexPages.Message msg : messageArray ){
                        String msgFieldlabel = msg.getComponentLabel();
                        if(msgFieldlabel != null && msgFieldlabel == fieldLabel  ){
                            found = true;
                        }
                    }
                }
                
                
                
            }
            if(!found){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,''+errorMessage );
                ApexPages.addMessage(myMsg);
            }
            return Null;
        }
    }
    
    private void getFieldDependancyData(){
        FieldDependancyHelper fieldDependancyHelper = new FieldDependancyHelper();
        fieldDependancyHelper.process(recordTypeId,false);
        
    }
    
     public class TabWrapper{
        public CLM_Tab__c tab {get; set;}
        public List<SectionWrapper> sectionWrapperList{get; set;}
        public Integer sequence {get; set;}
        public Map<Integer,SectionWrapper> sectionWrapperMap {get; set;} 
        
        public TabWrapper(CLM_Tab__c tabCustomSetting,List<CLM_Section__c> internalSectionList,Map<Integer,List<CLM_FIeld__c>> fieldInternalMap){
            this.tab = tabCustomSetting;
            this.sectionWrapperList = new List<SectionWrapper>();
            for(CLM_Section__c sectionCustomSetting : internalSectionList){
                if(fieldInternalMap == null){
                    fieldInternalMap = new Map<Integer,List<CLM_FIeld__c>>();
                }
                List<CLM_FIeld__c> internalFieldList = fieldInternalMap.get((Integer)sectionCustomSetting.Order__c);
                //if(internalFieldList != null){
                SectionWrapper sectionWrap = new SectionWrapper(sectionCustomSetting,internalFieldList);
                // if the section is a VF page or component or there are filed in section then only we will add it in tab otherwise we will not add it.
                // We don't want to show empty sections in tabs
                if((sectionWrap.fieldWrapperList != null && !sectionWrap.fieldWrapperList.isEmpty())||(sectionWrap.section.Is_InlineVF_Page__c || sectionWrap.section.Is_Lightning_Component__c)){
                    sectionWrapperList.add(sectionWrap);
                }
                //}
            }
        }
    }
    
    public class SectionWrapper{
        public CLM_Section__c section {get; set;}
        public String sectionId{get; set;}
        public Integer fieldColumn{get; set;}
        public List<FieldWrapper> fieldWrapperList{get; set;}
        public Integer sequence {get; set;}
        public Map<Integer,FieldWrapper> fieldWrapperMap {get; set;} 
        
        public SectionWrapper(CLM_Section__c sectionCustomSetting,List<CLM_FIeld__c> internalFieldList ){
            this.section = sectionCustomSetting;
            this.sequence = (Integer)sectionCustomSetting.Order__c;
            String pageOrComponentCompleteName = section.Inline_Api_Name_Component_Api__c;
            if(pageOrComponentCompleteName != null){
                String pageOrComponentName = pageOrComponentCompleteName.substringAfterLast('/');
                if(pageOrComponentName == null || pageOrComponentName == ''){
                    pageOrComponentName = pageOrComponentCompleteName.substringAfterLast(':');
                }
                if(pageOrComponentName == null || pageOrComponentName == ''){
                    pageOrComponentName = 'test'+section.Order__c;
                }
                sectionId = pageOrComponentName ;
            }
            fieldWrapperList = new List<FieldWrapper>();
            fieldWrapperMap = new Map<Integer,FieldWrapper>();
            if(internalFieldList != null){
                for(CLM_FIeld__c fieldCustomSetting : internalFieldList){
                    FieldWrapper fieldWrap = new FieldWrapper(fieldCustomSetting);
                    
                    fieldWrapperList.add(fieldWrap);        
                    fieldWrapperMap.put(sequence,fieldWrap);
                }
            }
            if(sectionCustomSetting.No_Of_Column__c != null && sectionCustomSetting.No_Of_Column__c != 0){
                fieldColumn = (Integer)( 12/sectionCustomSetting.No_Of_Column__c);
                
            }
            else{
                fieldColumn  = 6;
            }
        }
    }
    
    public class FieldWrapper{
        public CLM_FIeld__c field {get; set;}
        public Integer sequence {get; set;}
        public String fieldType{get;set;}
        public String viewFieldType{get; set;}//newlly added
        public Boolean isRequired{get;set;}
        public Boolean isDraftRequired{get;set;}
        public Boolean isEditable{get;set;}
        public String lookUpName{get; set;}
        public String viewLookUpName{get; set;}//newlly added
        public String errorMessage{get;set;}
        public String className{get;set;}
        public Map<String,FieldDependancyHelper.DependantField> dependantFieldMap{get;set;}
        public String dependantFieldString{get;set;}
        public Boolean isParent{get; set;}
        public String refLabel{get;set;}
        
        //newlly added 19-04
        public String  relatedFieldName{get;set;}
        
        public FieldWrapper(CLM_FIeld__c fieldCustomSetting){
            this.field = fieldCustomSetting;
            this.sequence = (Integer)fieldCustomSetting.Order__c;
            this.isRequired = field.Required__c;
            this.isDraftRequired = field.Draft_Required__c;
            
            
            if(field.Create_Blank_Space__c != true){//newlly added if(field1.Create_Blank_Space__c != true) 15-05-2018
            Schema.SObjectField field1 = conVehicleFieldTypeMap.get(field.FieldApiName__c.trim());
            if(field1 != null){
                Schema.DisplayType fldType = field1.getDescribe().getType();
                this.fieldType = ''+fldType;
                if(fieldType=='REFERENCE'){
                    this.lookUpName=''+field1.getDescribe().getReferenceTo().get(0);
                    // newlly added 19-04
                    if(fieldsMapWithRefranceFieldName.containsKey(''+field.FieldApiName__c.trim()))
                    {
                        this.relatedFieldName = ''+fieldsMapWithRefranceFieldName.get(''+field.FieldApiName__c.trim());
                    }
                }
            }  
            
            if(field.ViewFieldApi__c == null || field.ViewFieldApi__c == ''){
                field.ViewFieldApi__c = field.FieldApiName__c;
            }
            
            //newlly added
            Schema.SObjectField field2 = conVehicleFieldTypeMap.get(field.ViewFieldApi__c.trim());
            if(field2 != null){
                Schema.DisplayType viewFldType = field2.getDescribe().getType();
                this.viewFieldType = ''+viewFldType ;
                if(viewFieldType =='REFERENCE')
                   this.viewLookUpName=''+field2.getDescribe().getReferenceTo().get(0);
            }  
            // End
            
            //System.debug('field.FieldApiName__c 11## = '+fieldsMapAccessible.get('Total_Contract_Value_with_all_Options__c').getDescribe().isUpdateable());
            Schema.SObjectField sobjField = fieldsMapAccessible.get(field.FieldApiName__c);
            if(! fieldsMapAccessible.isEmpty() && sobjField != null){
                this.isEditable= sobjField.getDescribe().isUpdateable();
                
                if(!field.Draft_Required__c && this.isEditable &&  fieldType != 'BOOLEAN'){
                    this.isDraftRequired = !(TM_SubContractDetailPageController.fieldsMapAccessible.get(field.FieldApiName__c).getDescribe().isNillable());  
                }
            }else{
                this.isEditable =true;
            }
            
            if(isRequired || isDraftRequired){
                if(fieldCustomSetting.Required_Error_Message__c != null && fieldCustomSetting.Required_Error_Message__c != ''){
                    errorMessage = fieldCustomSetting.Required_Error_Message__c;
                    
                }     
                else{
                    errorMessage = 'You must enter a value';
                }
            }
            
            System.debug('In field setting constructor'+FieldDependancyHelper.parentMap);
            System.debug('In childmap field setting constructor'+FieldDependancyHelper.childmap);
            Map<String,FieldDependancyHelper.DependantField> dependantFieldMap = FieldDependancyHelper.parentMap.get(field.FieldApiName__c.toLowerCase());
            if(dependantFieldMap == null){
                isParent = false;
                dependantFieldMap = new Map<String,FieldDependancyHelper.DependantField>();
            }
            else{
                isParent = true;
            }
            this.dependantFieldMap = dependantFieldMap;
            
            this.dependantFieldString = '';
            for(String strKey : dependantFieldMap.keyset()){
                this.dependantFieldString += this.dependantFieldMap.get(strKey).value+'SPLITVAL'+this.dependantFieldMap.get(strKey).className+'SPLITVAL'+this.dependantFieldMap.get(strKey).apiName+'SPLITREC';
            }
            
            String fieldClassesName = FieldDependancyHelper.childmap.get(field.FieldApiName__c);
            System.debug('Field Api Name = ' + field.FieldApiName__c +'     fieldClassesName = '+fieldClassesName);
            if(fieldClassesName == null){
                fieldClassesName = '';
            }
            this.className = fieldClassesName;
            
            
        }  
    }
    }
}