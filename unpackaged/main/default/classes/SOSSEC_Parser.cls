public with sharing class SOSSEC_Parser {
    private String emailSubject='';
    public String taskOrderNumber='';
    public String taskOrderTitle='';
    
    public void parse(Messaging.InboundEmail email){
        emailSubject = email.subject;
        System.debug(''+emailSubject.length());
        if(emailSubject.length() == 0){
            throw new ContractVehicleNotFoundException(MessageConstantController.TASK_ORDER_NOT_FOUND );
        }
         System.debug(''+emailSubject);
        if(emailSubject.startsWith('Fwd:'))
            emailSubject = emailSubject.replace('Fwd:', '');
        if(emailSubject.startsWith('Re:'))
            emailSubject = emailSubject.replace('Re:', '');
        //taskOrderTitle=emailSubject.length() >254 ? emailSubject.substring(0,254):emailSubject;
        //taskOrderNumber = emailSubject.length() >79 ? emailSubject.substring(0,79):emailSubject;
        taskOrderTitle = truncateString(emailSubject,254);
        taskOrderNumber = truncateString(emailSubject,79);
    }
    
    
    public Task_Order__c getTaskOrder(String toAddress){
        if(taskOrderNumber==null){
            return null;
        }
        if(Schema.sObjectType.Task_Order__c.isAccessible() && Schema.sObjectType.Contract_Vehicle__c.isAccessible()){
            List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];        
            if(!conVehicalList.isEmpty()){
                Task_Order__c taskOrder = new Task_Order__c();
                taskOrder.Contract_Vehicle__c = conVehicalList[0].Id;
                taskOrder.Name = taskOrderNumber;//Task Order Number
                if(emailSubject != null && emailSubject.trim() != ''){      
                    taskOrder.Task_Order_Title__c = truncateString(emailSubject,254);
                    //taskOrder.Task_Order_Title__c = emailSubject.length() >254 ? emailSubject.substring(0,254):emailSubject;//Task Order Title
                }
                taskOrder.Release_Date__c = Date.today();
                return taskOrder;
            }else{
                throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
            }
        }else{
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
        }
        return Null;
    }
    
    public Task_Order__c updateTaskOrder(Task_Order__c taskOrder, String toAddress){
        if(Schema.sObjectType.Task_Order__c.isAccessible() && Schema.sObjectType.Contract_Vehicle__c.isAccessible() && Schema.sObjectType.Contact.isAccessible()){ 
            List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];
            
            if(!conVehicalList.isEmpty()){
                taskOrder.Contract_Vehicle__c = conVehicalList[0].Id;
                if(emailSubject != null && emailSubject.trim() != ''){                    
                    taskOrder.Task_Order_Title__c = emailSubject.length() >254 ? emailSubject.substring(0,254):emailSubject;//Task Order Title
                }
                if(taskOrder.Release_Date__c == Null){
                    taskOrder.Release_Date__c = Date.today();
                }
                return taskOrder;
            }else{
                throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
            }
        }
        return taskOrder;
    }
    
    private static string truncateString(String trunString, Integer maxLength){
        if(trunString!=Null){
            String tempTString = trunString.trim();
            if(tempTString.length()>=maxLength){
                return tempTString.subString(0,maxLength);
            }else{
                return tempTString;
            }
            
        }else{
            return Null;
        }
    }
}