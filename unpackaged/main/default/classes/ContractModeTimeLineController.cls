public with sharing class ContractModeTimeLineController {
   // private static List<String> apiStringList=new List<String>{'Contracting_Officer__c','Contract_Specialist__c','Description__c','Due_Date__c','Start_Date__c','Stop_Date__c','Task_Order_Number__c','Task_Order_Title__c','Proposal_Due_Date_Time__c','Questions_Due_Date__c','Release_Date__c','Contract_Negotiator__c','ASB_Action__c','Buyer__c','Category_Backend__c','Category__c','Customer_Agency__c','Delivery__c','Reference__c','Primary_Requirement__c','Requirement__c','RTEP_Status__c','Requiring_Activity__c','Acquisition_Tracker_ID__c','Contract_Type__c','Competitive_Procedure__c','Market_Research_Due_Date__c','Classification__c','Key_Personnel_Labor_Categories__c','Incumbent__c','POP_Notes__c','Task_Order_Place_of_Performance__c'};       
    private static List<String> apiStringList= new List<String>{'TM_TOMA__Contracting_Officer__c','TM_TOMA__Contract_Specialist__c','TM_TOMA__Description__c','TM_TOMA__Due_Date__c','TM_TOMA__Start_Date__c','TM_TOMA__Stop_Date__c','TM_TOMA__Task_Order_Number__c','TM_TOMA__Task_Order_Title__c',
                                                            'TM_TOMA__Proposal_Due_Date_Time__c','TM_TOMA__Questions_Due_Date__c','TM_TOMA__Release_Date__c','TM_TOMA__Contract_Negotiator__c','TM_TOMA__ASB_Action__c','TM_TOMA__Buyer__c','TM_TOMA__Category_Backend__c','TM_TOMA__Category__c',
                                                            'TM_TOMA__Customer_Agency__c','TM_TOMA__Delivery__c','TM_TOMA__Reference__c','TM_TOMA__Primary_Requirement__c','TM_TOMA__Requirement__c','TM_TOMA__RTEP_Status__c','TM_TOMA__Requiring_Activity__c','TM_TOMA__Acquisition_Tracker_ID__c',
                                                            'TM_TOMA__Contract_Type__c','TM_TOMA__Competitive_Procedure__c','TM_TOMA__Market_Research_Due_Date__c','TM_TOMA__Classification__c','TM_TOMA__Key_Personnel_Labor_Categories__c','TM_TOMA__Incumbent__c','TM_TOMA__POP_Notes__c','TM_TOMA__Task_Order_Place_of_Performance__c',
                                                            'TM_TOMA__Individual_Receiving_Shipment__c','TM_TOMA__Task_Order_Contracting_Activity__c','TM_TOMA__Agency__c','TM_TOMA__Agency_City__c','TM_TOMA__Agency_State__c','TM_TOMA__Agency_Street_Address__c','TM_TOMA__Agency_Zip__c','TM_TOMA__GPAT__c','TM_TOMA__SEWP_Agency_ID__c','TM_TOMA__Reseller_Requirements__c',
                                                            'TM_TOMA__Installation__c','TM_TOMA__Contracting_Office_Name__c','TM_TOMA__Award_Type__c','TM_TOMA__Expedited_Shipment_Picklist__c','TM_TOMA__Fair_Opportunity_Picklist__c','TM_TOMA__Outside_System_Order_Picklist__c','TM_TOMA__Stage__c','TM_TOMA__ITES_Format__c','TM_TOMA__ITES_3H_Activity__c'};
    private static List<HistoryController> apiList;                                                                  
    public static Contract_Mods__c contractMode;
    private static Map<String,String> labelMap;
    private static Map<String,String> typeMap;
    
    static{
        labelMap=new Map<String,String>();
        typeMap=new Map<String,String>();
        List<Schema.SObjectField> distributionFieldApiList =Contract_Mods__c.SObjectType.getDescribe().fields.getMap().values();
        if(distributionFieldApiList.size() > 0){
            for(Schema.SObjectField sobjectField : distributionFieldApiList){
                String tempField = sobjectField.getDescribe().getName();
                String fieldLabel = sobjectField.getDescribe().getLabel();
                String fieldType = string.valueOf(sobjectField.getDescribe().getType());
                typeMap.put(tempField, fieldType);
                labelMap.put(tempField,fieldLabel);
                
            }
            
        }
    }
    
    @AuraEnabled
    public static List<Contract_Mods__c> getContractModeList(String tskId){
        //System.debug(''+tskId);
        List<Contract_Mods__c> contractModeList=new List<Contract_Mods__c>();
        
        if(Schema.sObjectType.Contract_Mods__c.isAccessible())
            contractModeList = [SELECT Id, Name, createdDate, RecordTypeId FROM Contract_Mods__c WHERE TaskOrder__c = :tskId AND RecordType.DeveloperName = 'Update' ORDER BY createdDate];
        
        //System.debug('contractModeList   >>>>>'+contractModeList.size());
        return contractModeList;
    }
    
    @AuraEnabled
    public static List<HistoryController> timelineRec(Id timelineId,Id oppId)
    {
        ContractModeTimeLineController cmd=new ContractModeTimeLineController();
        
        apiList = new List<HistoryController> ();
        if(Schema.sObjectType.Contract_Mods__c.isAccessible()){
            
            contractMode= [Select CreatedDate,New_Acquisition_Tracker_ID__c,Acquisition_Tracker_ID__c,New_Buyer__c,Buyer__c,Category_Backend__c,New_Category_Backend__c,    New_Category__c,Category__c,New_Classification__c,Classification__c,    New_Competitive_Procedure__c,Competitive_Procedure__c,New_Contracting_Officer__c,Contracting_Officer__c,New_Contract_Negotiator__c,Contract_Negotiator__c,  New_Contract_Specialist__c,Contract_Specialist__c,Contract_Type__c, New_Contract_Type__c,
                           New_Customer_Agency__c,Customer_Agency__c,New_Delivery__c,Delivery__c,New_Description__c,Description__c, New_Incumbent__c,Incumbent__c,New_Key_Personnel_Labor_Categories__c,Key_Personnel_Labor_Categories__c,New_Market_Research_Due_Date__c,Market_Research_Due_Date__c,New_POP_Notes__c,POP_Notes__c,    New_Primary_Requirement__c,Primary_Requirement__c,  New_Requirement__c,Requirement__c,  New_Due_Date__c,Due_Date__c,New_Proposal_Due_Date_Time__c,
                           Proposal_Due_Date_Time__c,   New_Questions_Due_Date__c,Questions_Due_Date__c,    New_Reference__c,Reference__c,  New_Requiring_Activity__c,Requiring_Activity__c,New_Release_Date__c,Release_Date__c,New_RTEP_Status__c,RTEP_Status__c,New_Start_Date__c,Start_Date__c,New_Stop_Date__c,Stop_Date__c,New_ASB_Action__c,ASB_Action__c,New_Task_Order_Number__c,Task_Order_Number__c,New_Task_Order_Place_of_Performance__c,Task_Order_Place_of_Performance__c,
                           New_Task_Order_Title__c,Task_Order_Title__c,TaskOrder__c,Individual_Receiving_Shipment__c,New_Individual_Receiving_Shipment__c,Task_Order_Contracting_Activity__c,New_Task_Order_Contracting_Activity__c,
                           Agency__c,Agency_City__c,Agency_State__c,Agency_Street_Address__c,New_Agency__c,New_Agency_City__c,New_Agency_State__c,New_Agency_Street_Address__c,New_Agency_Zip__c,
                           New_GPAT__c,Agency_Zip__c,GPAT__c,SEWP_Agency_ID__c,New_SEWP_Agency_ID__c,Reseller_Requirements__c,
                           New_Reseller_Requirements__c,Installation__c,New_Installation__c,Contracting_Office_Name__c,New_Contracting_Office_Name__c,
                           Buyer__r.Name,Contracting_Officer__r.Name,Contract_Specialist__r.Name,Contract_Negotiator__r.Name,
                           New_Buyer__r.Name,New_Contracting_Officer__r.Name,New_Contract_Specialist__r.Name,New_Contract_Negotiator__r.Name,Customer_Agency__r.Name,New_Customer_Agency__r.Name,Award_Type__c,New_Award_Type__c,
                           Expedited_Shipment_Picklist__c, New_Expedited_Shipment_Picklist__c,Fair_Opportunity_Picklist__c,New_Fair_Opportunity_Picklist__c,
                           Outside_System_Order_Picklist__c,New_Outside_System_Order_Picklist__c,Stage__c,New_Stage__c,ITES_Format__c,New_ITES_Format__c,ITES_3H_Activity__c,New_ITES_3H_Activity__c 
                           FROM Contract_Mods__c WHERE id=:timelineId And RecordType.DeveloperName = 'Update'];
            
            Map<String,String> apiVSFieldValueMap = new Map<String,String>();
            if(contractMode!=null){
                apiVSFieldValueMap.put('TM_TOMA__Customer_Agency__c',contractMode.Customer_Agency__r.Name);
                apiVSFieldValueMap.put('TM_TOMA__Contracting_Officer__c',contractMode.Contracting_Officer__r.Name);
                apiVSFieldValueMap.put('TM_TOMA__Contract_Specialist__c',contractMode.Contract_Specialist__r.Name);
                apiVSFieldValueMap.put('TM_TOMA__Buyer__c',contractMode.Buyer__r.Name);
                apiVSFieldValueMap.put('TM_TOMA__Contract_Negotiator__c',contractMode.Contract_Negotiator__r.Name);
                //new
                apiVSFieldValueMap.put('TM_TOMA__New_Customer_Agency__c',contractMode.New_Customer_Agency__r.Name);
                apiVSFieldValueMap.put('TM_TOMA__New_Contracting_Officer__c',contractMode.New_Contracting_Officer__r.Name);
                apiVSFieldValueMap.put('TM_TOMA__New_Contract_Specialist__c',contractMode.New_Contract_Specialist__r.Name);
                apiVSFieldValueMap.put('TM_TOMA__New_Buyer__c',contractMode.New_Buyer__r.Name);
                apiVSFieldValueMap.put('TM_TOMA__New_Contract_Negotiator__c',contractMode.New_Contract_Negotiator__r.Name);
            }
            
            String createdDate='';
            if(contractMode!=null){
                DateTime crtDate=(DateTime)contractMode.get('CreatedDate');
                //createdDate=crtDate.formatGmt('MMMMM dd, yyyy hh:mm a');
                createdDate=crtDate.format('MMMMM dd, yyyy hh:mm a');
                
                for(String api : apiStringList){
                    try{
                    System.debug(''+api);
                    if(contractMode.get(api)!=Null || contractMode.get(api.replace('TM_TOMA__','TM_TOMA__New_'))!=Null){
                       System.debug(''+ typeMap.get(api));
                        if(typeMap.get(api)=='DATETIME'){
                            DateTime dt=(DateTime)contractMode.get(api);
                            String Oldv=dt.format('MMMMM dd, yyyy hh:mm a');
                            DateTime dt1=(DateTime)contractMode.get(api.replace('TM_TOMA__','TM_TOMA__New_'));
                            String newv=dt1.format('MMMMM dd, yyyy hh:mm a');
                            //System.debug('date 2  fmt >>>>'+newv);
                            apiList.add(new HistoryController(labelMap.get(api),Oldv,newv,createdDate));   
                            
                        }else if(typeMap.get(api)=='DATE'){
                            DateTime dt=(DateTime)contractMode.get(api);
                            String Oldv=dt.formatGmt('MMMMM dd, yyyy');
                            DateTime dt1=(DateTime)contractMode.get(api.replace('TM_TOMA__','TM_TOMA__New_'));
                            String newv=dt1.formatGmt('MMMMM dd, yyyy');
                            //System.debug('date 2  fmt >>>>'+newv);
                            apiList.add(new HistoryController(labelMap.get(api),Oldv,newv,createdDate)); 
                            
                        }else if(typeMap.get(api)=='REFERENCE'){     
                            System.debug('api = '+api);
                            System.debug('api = '+apiVSFieldValueMap.get(api));
                            System.debug('api = '+apiVSFieldValueMap.get(api.replace('TM_TOMA__','TM_TOMA__New_')));
                            
                            apiList.add(new HistoryController(labelMap.get(api),apiVSFieldValueMap.get(api),apiVSFieldValueMap.get(api.replace('TM_TOMA__','TM_TOMA__New_')),createdDate));
                        }else{
                            apiList.add(new HistoryController(labelMap.get(api),String.valueof(contractMode.get(api)),String.valueof(contractMode.get(api.replace('TM_TOMA__','TM_TOMA__New_'))),createdDate));    
                        }
                        
                    }
                    }catch(exception e){
                        
                    }
                }
            }
        }
        return  apiList;
    }
    
    @AuraEnabled
    public static String getCheckLicensePermition(){
        String permission = LincenseKeyHelper.checkLicensePermitionForFedTOM();
        //System.debug('permission>>>>'+permission);
        return permission;        
    }
    
    /*public with sharing class HistoryController {
@AuraEnabled
public String apiName;
@AuraEnabled
public string oldInformation;
@AuraEnabled
public string updatedInformation;
@AuraEnabled public String ceatedDate {get;set;}
public HistoryController (string fName,string oldValue,string updatedValue,String d1)
{
system.debug('Inside FboHistoryController1 ');
apiName=fName;
oldInformation=oldValue;
updatedInformation=updatedValue;
ceatedDate=d1;

}
}*/
    
}