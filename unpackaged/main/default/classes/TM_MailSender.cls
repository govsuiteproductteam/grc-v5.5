global class TM_MailSender {
    
    @InvocableMethod(label='Send Email' description='Send Email to Contract Vehicle Contact.')
    global static void sendMail(List<Id> taskOrderIdList){
        if(Schema.sObjectType.Task_Order__c.isAccessible() && Schema.sObjectType.Task_Order_Contact__c.isAccessible() && Schema.sObjectType.Attachment.isAccessible() && Schema.sObjectType.EmailTemplate.isAccessible() ){ 
        Id taskOrderId = taskOrderIdList[0];
        try{
            List<Task_Order__c> taskOrderList = [SELECT Id,Name,(SELECT Id,contact__c FROM Task_Order_Contacts__r where contact__c != null AND (Main_POC__c = true OR Receives_Mass_Emails__c = true)) From Task_Order__c WHERE Id =:taskOrderId LIMIT 1];
            if(!taskOrderList.isEmpty())
            {
                List<Task_Order_Contact__c> taskOrderContactList = taskOrderList.get(0).Task_Order_Contacts__r;
                if(taskOrderContactList != null){
                    List<Id> contactIdList = new List<Id>();
                    for(Task_Order_Contact__c taskOrderContact : taskOrderContactList){
                        if(taskOrderContact.contact__c != null){
                            contactIdList.add(taskOrderContact.contact__c);
                            //sendMail(contactIdList,taskOrderId);
                        }
                    }
                    if(!contactIdList.isEmpty()){
                        sendMail(contactIdList,taskOrderId);
                    }
                    
                }
            }
        }catch(Exception e){
            System.debug(''+e.getMessage()+'\t'+e.getLineNumber());
        }
    }
    }
    
   /* public static void sendMail(List<Id> taskOrderIdList){
    Id taskOrderId = taskOrderIdList[0];
    try{
        List<Task_Order__c> taskOrderList = [SELECT Id,Name,Contract_Vehicle__c From Task_Order__c WHERE Id =:taskOrderId LIMIT 1];
        if(!taskOrderList.isEmpty())
        {
            Id contractVehicleId = taskOrderList.get(0).Contract_Vehicle__c;
            if(contractVehicleId != null){
                List<Contract_Vehicle__c> contractVehicleList = [SELECT Id,(SELECT Id,contact__c  FROM Contract_Vehicle_Contacts__r where contact__c != null ANd (Main_POC__c = true OR Receives_Mass_Emails__c = true)),(select id,name,BodyLength from Attachments) FROM Contract_Vehicle__c WHERE Id =:contractVehicleId];
                if(!contractVehicleList.isEmpty()){
                    List<Contract_Vehicle_Contact__c> contractVehicleContactList = contractVehicleList.get(0).Contract_Vehicle_Contacts__r;
                    if(!contractVehicleContactList.isEmpty()){
                        //Map<Id,Contract_Vehicle_Contact__c> contractVehicleMap = new Map<Id,Contract_Vehicle_Contact__c>(contractVehicleContactList);
                        List<Id> contactIdList = new List<Id>();
                        for(Contract_Vehicle_Contact__c cvContact : contractVehicleContactList){
                            if(cvContact.contact__c != null){
                                contactIdList.add(cvContact.contact__c);
                                //sendMail(contactIdList,taskOrderId);
                            }
                        }
                        if(!contactIdList.isEmpty()){
                            sendMail(contactIdList,taskOrderId);
                        }
                    }
                }
            }
        }
    }catch(Exception e){
        System.debug(''+e.getMessage()+'\t'+e.getLineNumber());
    }
}*/
    
    private static void sendMail(List<Id> contactIdList,Id taskOrderId){
        try{
            List<Attachment> attachmentList = [select Name, Body, BodyLength from Attachment where parentId=:taskOrderId];
            List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
            for (Attachment attachment :attachmentList)
            {
                Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                efa.setFileName(attachment.Name);
                efa.setBody(attachment.Body);
                //attachment.body = null;
                fileAttachments.add(efa);
            }
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            List<EmailTemplate> emailTemplateList = [SELECT Id FROM EmailTemplate WHERE developerName='IDIQ_RFI_Template_3' LIMIT 1];// temp as discussed
            if(!emailTemplateList.isEmpty()){
                Id emailTemplateId = emailTemplateList.get(0).Id;
                for(Id contactId : contactIdList) {
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setTargetObjectId(contactId);
                    mail.setTemplateId(emailTemplateId);
                    mail.setSaveAsActivity(true); 
                    if(!fileAttachments.isEmpty())
                        mail.setFileAttachments(fileAttachments);
                    mails.add(mail);
                }
            }
            if(!mails.isEmpty())
                Messaging.sendEmail(mails);
        }catch(Exception e){
            System.debug(''+e.getMessage()+'\t'+e.getLineNumber());
        }
    }
}