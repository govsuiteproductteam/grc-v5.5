@isTest
public class AFCAPIVEmailTemplateHandlerTest {
    public static testmethod void testCase1(){
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Contact con = TestDataGenerator.createContact('avan', acc );
        insert con;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('T-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'afcapiv@4goo5mqiigfoiz612urcnlljjptepva1g93g0ma5asazu5d2n.37-pvateac.na31.apex.salesforce.com';
        
        system.assertEquals(con.accountId, acc.id);
        
        insert conVehi;
        
        //for new Task Order Creation  
        Messaging.InboundEmail email  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        List<StaticResource> srList = [SELECT Id,Body FROM StaticResource WHERE Name='AFCAPIVEmailTest'];
        
        email.plainTextBody = srList.get(0).Body.toString();
        email.fromAddress ='test@test.com';
        email.fromName = 'ABC XYZ';
        email.subject = 'Request for Proposal (RFP) FA8051-17-R-1001 : Engineering Support Services, Muwaffaq-Salti Air Base (MSAB), Jordan';
        email.toaddresses = new List<String>();
        email.toaddresses.add('afcapiv@4goo5mqiigfoiz612urcnlljjptepva1g93g0ma5asazu5d2n.37-pvateac.na31.apex.salesforce.com');
        AFCAPIVEmailTemplateHandler  aFCAPIVEmailTemplateHandler = new AFCAPIVEmailTemplateHandler();
        //For Updating existing task order with existing Contact
        Messaging.InboundEmail email1  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env1 = new Messaging.InboundEnvelope();
        email1.plainTextBody = '';
        email1.fromAddress ='test@test.com';
        email1.fromName = 'ABC XYZ';
        email1.subject = 'Amendment 01 to Request for Proposal (RFP) FA8051-17-R-1001 : Engineering Support Services, Muwaffaq-Salti Air Base (MSAB), Jordan';
        email1.toaddresses = new List<String>();
        email1.toaddresses.add('afcapiv@4goo5mqiigfoiz612urcnlljjptepva1g93g0ma5asazu5d2n.37-pvateac.na31.apex.salesforce.com');
        
        //For Updating existing task order with new Contact
        Messaging.InboundEmail email2  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env2 = new Messaging.InboundEnvelope();
        email2.plainTextBody = '';
        email2.fromAddress ='test@test.com';
        email2.fromName = 'ABC XYZ';
        email2.subject = 'Test';
        email2.toaddresses = new List<String>();
        email2.toaddresses.add('afcapiv@4goo5mqiigfoiz612urcnlljjptepva1g93g0ma5asazu5d2n.37-pvateac.na31.apex.salesforce.com');
        
        Messaging.InboundEmailResult result = aFCAPIVEmailTemplateHandler.handleInboundEmail(email, env);
        Messaging.InboundEmailResult result1 = aFCAPIVEmailTemplateHandler.handleInboundEmail(email1, env1);
        try{
        Messaging.InboundEmailResult result2 = aFCAPIVEmailTemplateHandler.handleInboundEmail(email2, env2);
        }
        catch(Exception ex){
        }
    }
}