({
    checkAccess : function(component, event, helper) {
        var isLiecenceValid =  component.get("c.checkLicensePermition");
        isLiecenceValid.setCallback(this,function(validityData){
            var state= validityData.getState();
            if(state=="SUCCESS"){
                console.log("Stages Status"+validityData.getReturnValue());
                if(validityData.getReturnValue() == 'Yes'){
                    component.set("v.CheckLicense",true);
                    helper.gettaskOrderContactList1(component, event, helper);
                }/*else if(validityData.getReturnValue() == 'No'){
                    component.set("v.CheckLicense",false);
                }*/else{
                    component.set("v.CheckLicense",false);
                    component.set("v.errorMsg",validityData.getReturnValue());
                }
            }
            else if (state == "ERROR") 
                {
                    console.log(validityData.getError());            
                }
        });
        $A.enqueueAction(isLiecenceValid);
    },
    RefreshTaskOrderContactPage : function(component, event, helper) {
        var isLiecenceValid =  component.get("c.checkLicensePermition");
        isLiecenceValid.setCallback(this,function(validityData){
            var state= validityData.getState();
            if(state=="SUCCESS"){
                console.log("Stages Status"+validityData.getReturnValue());
                if(validityData.getReturnValue() == 'Yes'){
                    component.set("v.CheckLicense",true);
                    helper.gettaskOrderContactList1(component, event, helper);
                }else if(validityData.getReturnValue() == 'No'){
                    component.set("v.CheckLicense",false);
                }else{
                    component.set("v.CheckLicense",true);
                }
            }
           else if (state == "ERROR") 
                {
                    console.log(validityData.getError());            
                }
        });
        $A.enqueueAction(isLiecenceValid);
    },
    gettaskOrderContactList: function(component, event, helper) {
        helper.gettaskOrderContactList1(component, event, helper);
    },
    editTaskOrderContact : function (component, event, helper) {
        var selectedItem = event.currentTarget;
        var tempId = selectedItem.id;
        /*  var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": tempId,
            "slideDevName": "related"
        });
        navEvt.fire();*/
         var editRecordEvent = $A.get("e.force:editRecord");
         editRecordEvent.setParams({
             "recordId": tempId
         });
         editRecordEvent.fire();
     },
    deleteTaskOrderCon : function (component, event, helper) {
        
        var selectedItem = event.currentTarget;
        var tempId = selectedItem.id;
        //var recName = selectedItem.dataset.arg2;
        var action = component.get("c.deleteTaskOrderContact");
        action.setParams({
            taskOrderConId: selectedItem.id
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS")
            {
                helper.gettaskOrderContactList1(component, event, helper);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "Task Order Contact Was Deleted",
                    "type":"success"
                });
                toastEvent.fire();
            }
            else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": "Error occurred while deleting the record.Please refresh the page.",
                    "type":"error"
                });
                toastEvent.fire();
                console.log(a.getError()); 
            }
            
        })
        $A.enqueueAction(action); 
    },
    addTaskorderContactList : function (component, event, helper) {
        var action = component.get("c.addTaskOrderContact");
        action.setParams({
            taskOrderId: component.get("v.recordId")
        });
        action.setCallback(this, function(a) {
            var state= a.getState();
            if(state=="SUCCESS"){
            if(a.getReturnValue() == undefined || a.getReturnValue() == '' || a.getReturnValue() == null)
            {
                component.set("v.conWraList",);
                component.set("v.conWraListTemp",);
            }
            else{
                component.set("v.conWraList", a.getReturnValue());
                component.set("v.conWraListTemp", a.getReturnValue());
            }
            
            var Modal = component.find("taskOrderContactAddModal");
            var ModalBack = component.find("taskOrderContactAddModalbagroundId");
            $A.util.removeClass(Modal, "slds-modal slds-fade-in-close");
            $A.util.addClass(Modal, "slds-modal slds-fade-in-open");
            $A.util.removeClass(ModalBack, "slds-backdrop slds-backdrop--close");
            $A.util.addClass(ModalBack, "slds-backdrop slds-backdrop--open");
            }
            else if (state == "ERROR") 
            {
                var Modal = component.find("taskOrderContactAddModal");
                var ModalBack = component.find("taskOrderContactAddModalbagroundId");
                $A.util.removeClass(Modal, "slds-modal slds-fade-in-close");
                $A.util.addClass(Modal, "slds-modal slds-fade-in-open");
                $A.util.removeClass(ModalBack, "slds-backdrop slds-backdrop--close");
                $A.util.addClass(ModalBack, "slds-backdrop slds-backdrop--open");
                console.log(a.getError());            
            }
        })
        $A.enqueueAction(action); 
    },
    maincheckboxContact : function (component, event, helper) {
        var obj = component.find("select-all").get("v.value");
        if(obj === true) {
            var tempList = component.get("v.conWraList");
            if(tempList != undefined){
                for(var i=0;i<tempList.length;i++)
                {
                    tempList[i].status=true;
                }
                component.set("v.conWraList",tempList);
            }
        }
        else{
            var tempList = component.get("v.conWraList");
            if(tempList != undefined){
                for(var i=0;i<tempList.length;i++)
                {
                    tempList[i].status=false;
                }
                component.set("v.conWraList",tempList);
            }
        }
    },
    maincheckboxChildContact : function (component, event, helper) {
        var checkbox = event.getSource();
        if(checkbox.get("v.value") === false) {
            component.find("select-all").set("v.value",false);
        }
        else
        {
            var check;
            var tempList = component.get("v.conWraList");
            if(tempList != undefined){
                for(var i=0;i<tempList.length;i++)
                {
                    if(tempList[i].status === false)
                    {
                        check=false;
                        break;
                    }
                    else
                    {
                        check=true;
                    }
                }
                if(check === true)
                {
                    component.find("select-all").set("v.value",true);
                }
                else
                {
                    component.find("select-all").set("v.value",false);
                }
            }
        }
    },
    cancel : function (component, event, helper) {
        var conModal = component.find("taskOrderContactAddModal");
        var conModalBack = component.find("taskOrderContactAddModalbagroundId");
        $A.util.removeClass(conModal, "slds-modal slds-fade-in-open");
        $A.util.addClass(conModal, "slds-modal slds-fade-in-close");
        $A.util.removeClass(conModalBack, "slds-backdrop slds-backdrop--open");
        $A.util.addClass(conModalBack, "slds-backdrop slds-backdrop--close");
        component.find("select-all").set("v.value",false);
        component.set("v.searchContact",'');
        var tempList = component.get("v.conWraList");
        if(tempList != undefined){
            for(var i=0;i<tempList.length;i++)
            {
                tempList[i].status = false;
            }
            component.set("v.conWraList",tempList); 
        }
    },
    AccountRecord : function (component, event, helper) {
        var selectedItem = event.currentTarget;
        var tempId = selectedItem.id;
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": tempId,
            "slideDevName": "detail"
        });
        navEvt.fire();
    },
    ContactRecord : function (component, event, helper) {
        var selectedItem = event.currentTarget;
        var tempId = selectedItem.id;
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": tempId,
            "slideDevName": "detail"
        });
        navEvt.fire();
    },
    addTaskorderContact1 : function (component, event, helper) {
        document.getElementById("addContractVehicleContacts").disabled = true;
        var tempList = component.get("v.conWraList");
        var partnerDataString='[';
        if(tempList != undefined){
            for(var i=0;i<tempList.length;i++)
            {
                if(tempList[i].status === true)
                {
                    var conId = tempList[i].taskOrderContact.TM_TOMA__Contact__r.Id;
                    var FedCapId = component.get("v.recordId");
                    var mainPOC = tempList[i].taskOrderContact.TM_TOMA__Main_POC__c;
                    var receivesMassEmails = tempList[i].taskOrderContact.TM_TOMA__Receives_Mass_Emails__c;
                    partnerDataString += conId+'SPLITDATA'+FedCapId+'SPLITDATA'+mainPOC+'SPLITDATA'+receivesMassEmails+'SPLITPARENT';
                }
            }
            if(partnerDataString.length>1){
                var action = component.get("c.addNewTaskOrderContact");
                action.setParams({
                    "taskOrderConList": partnerDataString
                });
                action.setCallback(this, function(a) {
                    var state= a.getState();
                    if(state=="SUCCESS"){
                        document.getElementById("addContractVehicleContacts").disabled = false;
                        helper.gettaskOrderContactList1(component, event, helper);
                        component.find("select-all").set("v.value",false);
                        var conModal = component.find("taskOrderContactAddModal");
                        var conModalBack = component.find("taskOrderContactAddModalbagroundId");
                        $A.util.removeClass(conModal, "slds-modal slds-fade-in-open");
                        $A.util.addClass(conModal, "slds-modal slds-fade-in-close");
                        $A.util.removeClass(conModalBack, "slds-backdrop slds-backdrop--open");
                        $A.util.addClass(conModalBack, "slds-backdrop slds-backdrop--close");
                    }
                    else if (state == "ERROR") 
                    {
                        document.getElementById("addContractVehicleContacts").disabled = false;
                        console.log(a.getError());            
                    }
                })
                $A.enqueueAction(action);
            }
            else
            {
                document.getElementById("addContractVehicleContacts").disabled = false;
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": "Please select at least one Contact.",
                    "type":"error"
                });
                toastEvent.fire();
                // alert('Please select at least one Contact.');
            }
        }  
        else
        {
            //alert('Please select at least one Contact.');
            document.getElementById("addContractVehicleContacts").disabled = false;
             var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": "Please select at least one Contact.",
                    "type":"error"
                });
                toastEvent.fire();
        }
    },
    onCheckPOC: function (component, event, helper) {
        var temp = component.get("v.conWraList");
        if(temp != undefined)
        {
            for(var i=0;i<temp.length;i++)
            {
                if(event.getSource().get("v.text") === i)
                {
                    if(event.getSource().get("v.value") !== true)
                    {
                        temp[i].taskOrderContact.TM_TOMA__Main_POC__c=false;
                    }
                    else{
                        temp[i].taskOrderContact.TM_TOMA__Main_POC__c=true;
                    }
                }
            }
            component.set("v.conWraList",temp);
        }
    },
    onCheckEmails: function (component, event, helper) {
        var temp = component.get("v.conWraList");
        if(temp != undefined)
        {
            for(var i=0;i<temp.length;i++)
            {
                if(event.getSource().get("v.text") === i)
                {
                    if(event.getSource().get("v.value") !== true)
                    {
                        temp[i].taskOrderContact.TM_TOMA__Receives_Mass_Emails__c=false;
                    }
                    else{
                        temp[i].taskOrderContact.TM_TOMA__Receives_Mass_Emails__c=true;
                    }
                }
            }
            component.set("v.conWraList",temp);
        }
    },
    editInlinePoc : function(component, event, helper) {
        var selectedItem = event.currentTarget;
        var checkCounter = selectedItem.id;
        var d = document.getElementById(checkCounter+'editPocInlineOut');
        d.className += " slds-hide";
        d.classList.remove("slds-show");
        var d1 = document.getElementById(checkCounter+'editPocInlineIn');
        d1.className += " slds-show";
        d1.classList.remove("slds-hide");
        var d2 = document.getElementById(checkCounter+'editEmailInlineOut');
        d2.className += " slds-hide";
        d2.classList.remove("slds-show");
        var d3 = document.getElementById(checkCounter+'editEmailInlineIn');
        d3.className += " slds-show";
        d3.classList.remove("slds-hide");
        document.getElementById(checkCounter+'edit').dataset.arg3 = selectedItem.dataset.arg2;
        document.getElementById(checkCounter+'edit').dataset.arg1 = selectedItem.dataset.arg1;
        var d4 = document.getElementById(checkCounter+'edittaskEditDiv');
        d4.className += " slds-hide";
        d4.classList.remove("slds-show");
        var d5 = document.getElementById(checkCounter+'edittaskEditUndoDiv');
        d5.className += " slds-show";
        d5.classList.remove("slds-hide");
        var checksave = component.get("v.checkSavebutton");
        checksave=checksave+1;
        component.set("v.checkSavebutton",checksave);
    },
    closeInlinePoc : function(component, event, helper) {
        var selectedItem = event.currentTarget;
        var checkCounter = selectedItem.id;
        var oldval = selectedItem.dataset.arg1;
        var oldval1 = selectedItem.dataset.arg3;
        //event.currentTarget.dataset.caseID;
        var d = document.getElementById(checkCounter+'PocInlineOut');
        d.className += " slds-show";
        d.classList.remove("slds-hide");
        var d1 = document.getElementById(checkCounter+'PocInlineIn');
        d1.className += " slds-hide";
        d1.classList.remove("slds-show");
        
        var d2 = document.getElementById(checkCounter+'EmailInlineOut');
        d2.className += " slds-show";
        d2.classList.remove("slds-hide");
        var d11 = document.getElementById(checkCounter+'EmailInlineIn');
        d11.className += " slds-hide";
        d11.classList.remove("slds-show");
        
        var d4 = document.getElementById(checkCounter+'taskEditDiv');
        d4.className += " slds-show";
        d4.classList.remove("slds-hide");
        var d5 = document.getElementById(checkCounter+'taskEditUndoDiv');
        d5.className += " slds-hide";
        d5.classList.remove("slds-show");
        var checksave = component.get("v.checkSavebutton");
        checksave=checksave-1;
        component.set("v.checkSavebutton",checksave);
        
        var index = selectedItem.dataset.arg2;
        var temp = component.get("v.taskOrderContactList");
        if(temp != undefined){
            for(var i=0;i<temp.length;i++)
            {
                if(index === i+'')
                {
                    temp[i].TM_TOMA__Main_POC__c = (oldval == 'true');
                    temp[i].TM_TOMA__Receives_Mass_Emails__c = (oldval1 == 'true');
                }
            }
            component.set("v.taskOrderContactList",temp);
        }
    },
    editInlineEmail : function(component, event, helper) {
        var selectedItem = event.currentTarget;
        var checkCounter = selectedItem.id;
        var d = document.getElementById(checkCounter+'editEmailInlineOut');
        d.className += " slds-hide";
        d.classList.remove("slds-show");
        var d1 = document.getElementById(checkCounter+'editEmailInlineIn');
        d1.className += " slds-show";
        d1.classList.remove("slds-hide");
        // var oldval = selectedItem.dataset.arg1;
        document.getElementById(checkCounter+'editEmail').dataset.arg1 = selectedItem.dataset.arg1;
    },
    closeInlineEmail : function(component, event, helper) {
        var selectedItem = event.currentTarget;
        var checkCounter = selectedItem.id;
        var oldval = selectedItem.dataset.arg1;
        //event.currentTarget.dataset.caseID;
        var d = document.getElementById(checkCounter+'InlineOut');
        d.className += " slds-show";
        d.classList.remove("slds-hide");
        var d1 = document.getElementById(checkCounter+'InlineIn');
        d1.className += " slds-hide";
        d1.classList.remove("slds-show");
        var index = selectedItem.dataset.arg2;
        var temp = component.get("v.taskOrderContactList");
        if(temp != undefined){
            for(var i=0;i<temp.length;i++)
            {
                if(index === i+'')
                {
                    temp[i].TM_TOMA__Receives_Mass_Emails__c = (oldval == 'true');
                }
            }
            component.set("v.taskOrderContactList",temp);
        }
    },
    saveTaskorderContactList : function(component, event, helper) {
        document.getElementById("saveTaskOrderContactBtn").disabled = true;
        var taskOrderList=component.get("v.taskOrderContactList");
        var taskOrderContactDataString = '[';
        if(taskOrderList != undefined){
            for (var i=0; i<taskOrderList.length; i++){        
                
                var vTId = taskOrderList[i].Id;
                var mainPoc = taskOrderList[i].TM_TOMA__Main_POC__c;
                var massEmail  = taskOrderList[i].TM_TOMA__Receives_Mass_Emails__c;
                taskOrderContactDataString += vTId+'SPLITDATA'+mainPoc+'SPLITDATA'+massEmail+'SPLITPARENT';
            }
        }
        var action = component.get("c.saveTaskOrderContact");
        action.setParams({ 
            "taskOrderContactList": taskOrderContactDataString
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS")
            {
                helper.gettaskOrderContactList1(component, event, helper);
                component.set("v.checkSavebutton",0);
                document.getElementById("saveTaskOrderContactBtn").disabled = false;
                // alert("Data save successfully");
                /*  var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "Data Updated successfully.",
                        "type":"success"
                    });
                    toastEvent.fire(); */
                }
                else if (state == "ERROR") 
                {
                    component.set("v.checkSavebutton",0);
                    document.getElementById("saveTaskOrderContactBtn").disabled = false;
                    console.log(response.getError());            
                }
            });
        $A.enqueueAction(action);
    },
    doInitMatchingResultContact : function(component, event, helper) {
        var contactStr = component.get("v.searchContact")
        if(typeof contactStr === 'undefined' || contactStr.length < 2)
        {
           // component.find("select-all").set("v.value",false);
            
            var tempList = component.get("v.conWraListTemp");
            if(tempList != undefined){
                for(var i=0;i<tempList.length;i++)
                {
                    if(tempList[i].status == false) 
                    {
                        component.find("select-all").set("v.value",false);
                    }
                }
                component.set("v.conWraList",tempList); 
            }
            component.set("v.conWraList",component.get("v.conWraListTemp"));
        }
        else{
            var action = component.get("c.addSearchTaskOrderContact");
            action.setParams({
                taskOrderId: component.get("v.recordId"),
                searchString: contactStr
            });
            action.setCallback(this, function(a) {
                var state= a.getState();
                if(state=="SUCCESS"){
                    if(a.getReturnValue() == undefined || a.getReturnValue() == null || a.getReturnValue() == '')
                    {
                        component.set("v.conWraList", );
                    }
                    else
                    {
                        component.set("v.conWraList", a.getReturnValue());
                    }
                }
                else if (state == "ERROR") 
                {
                    console.log(a.getError());            
                }
                
            })
            $A.enqueueAction(action); 
        }
    }
})