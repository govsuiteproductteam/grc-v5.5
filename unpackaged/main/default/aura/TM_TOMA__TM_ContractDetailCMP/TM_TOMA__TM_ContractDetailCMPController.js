({
	doInit : function(component, event, helper) {
		
      var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location),
                sURLVariables = sPageURL.split('?'),
                sParameterName,
                i;
          //alert(sPageURL);
             //alert(sURLVariables);
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                //alert(sParameterName);
                if (sParameterName[0] === 'frameHeight') {
                    //alert(sParameterName[1]);
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        
        //set the src param value to my src attribute
        //component.set("v.src", getUrlParameter('src'));
        //var frameHeight = getUrlParameter('frameHeight');
        var frameHeight = getUrlParameter('frameHeight');
        
        component.set("v.frameHeight", frameHeight);
        component.set("v.pageUrl","/apex/TM_TOMA__TM_ContractDetailPage?id="+component.get("v.recordId"));
        //alert('cmp frameHeight = '+frameHeight); 
        //if(frameHeight != null && frameHeight != undefined && frameHeight != 'undefined' && frameHeight != '' ){
            //alert('In if satement');
			component.set("v.pageUrl","/apex/TM_TOMA__TM_ContractDetailPage?id="+component.get("v.recordId")+"&frameHeight="+frameHeight);
			//var currentEle = document.getElementById('myIFrame');
			//currentEle.height = frameHeight+'px';
    	//}
        //else{
            	//alert('In else satement');
        //	component.set("v.pageUrl","/apex/TM_ContractDetailPage?id="+component.get("v.recordId")+"&reload=1");
        //}
	},
    
    resizeIframe : function(component, event, helper){
        alert('Hi');
        var vfPageHeight = helper.getCookie('vfPageHeight');
        alert('vfPageHeight' + vfPageHeight);
        var currentEle = document.getElementById('myIFrame');
        alert(currentEle.height);
        alert(currentEle.contentWindow.body);
        currentEle.height = 'vfPageHeight'+ 'px';
    }
})