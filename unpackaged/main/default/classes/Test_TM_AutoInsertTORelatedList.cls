@isTest
public class Test_TM_AutoInsertTORelatedList {
    public static testMethod void insertTeamingPartnerAndTOContactTest(){
        Test.startTest();
         Account account = TestDataGenerator.createAccount('Test Account');
        insert account;
        Contact contact = TestDataGenerator.createContact('test', account);
        insert contact;
        Contract_Vehicle__c contractVehicle = TestDataGenerator.createContractVehicle('Test cv', account);
        insert contractVehicle;
        Vehicle_Partner__c vehiclePartner = TestDataGenerator.createVehiclePartner(account, contractVehicle);
        insert vehiclePartner;
        Contract_Vehicle_Contact__c cvContact = TestDataGenerator.createContractVehicleContact(contact, contractVehicle);
        cvContact.Main_POC__c = true;
        cvContact.Receives_Mass_Emails__c = true;
        insert cvContact;
        Task_Order__c taskOrder = TestDataGenerator.createTaskOrder('Test To-1001', account, contractVehicle);        
        insert taskOrder;
        Test.stopTest();
        //Remove below line after process Builder is created for the same functionality
        TM_AutoInsertTORelatedList.insertTeamingPartnerAndTOContact(new List<Id>{taskOrder.Id});
        Integer teamingPartnerCount = [SELECT Count() FROM Teaming_Partner__c];
        System.assert(teamingPartnerCount >0 );
        Integer taskOrderContactCount = [SELECT Count() FROM Task_Order_Contact__c];
        System.assert(taskOrderContactCount > 0);
    }
}