global with sharing class TM_CommunityHomePageController {
   
    
    public List<Task_Order__c> taskOrderList {get;set;}
    public List<TaskOrderWrapper> taskOrderWraList {get;set;}
    public List<Task_Order__c> tOrderOpenActivityList {get;set;}
    //public Task_Order__c taskOrderObj {get;set;}
   // public List<TaskOrderWrapper> taskOrderOpnActivityWraList {get;set;}
    public List <ContentDocument> contentDocumentList {get;set;}
    public List<Task_Order__c> selecteTaskOrderList{get;set;}
    public List<Task> lst {get;set;}
    //public Id usrId{get;set;}
   
    public List<Report> reportListQuarter {get;set;}
    public report report1_byQuarter {get;set;}
    public report report2_byStage {get;set;}
    private List<Task_Order_Questionnaire__c> communityUrlList;
    public String returnOpenActivityUrl {get;set;}
    public String returnDocUrl {get;set;}
    public String returnTaskOrderUrl {get;set;}
    public List<report> reportListStage {get;set;}
    
    public TM_CommunityHomePageController () {
       if(Schema.sObjectType.Report.isAccessible() && Schema.sObjectType.Contact.isAccessible() && Schema.sObjectType.User.isAccessible() && Schema.sObjectType.Task.isAccessible() && Schema.sObjectType.Task_Order__c.isAccessible() && Schema.sObjectType.ContentDocument.isAccessible() && Schema.sObjectType.Teaming_Partner__c.isAccessible()){ 
           init();
       }
    }
    
   // init() method is used to display Report, list of task orders, open task and documents.
   
    public void init(){
           reportListQuarter = new List<Report>();
           reportListStage = new List<Report>();
           
           Id myFolderId = [select id from folder where developername = 'FedTOM_Reports' limit 1].id;
           system.debug('myFolderId '+myFolderId);
            if(myFolderId != null){
                reportListQuarter = [select id, name, description from report where ownerid = : myFolderid AND name = 'TO Released by Quarter' ];
                system.debug('reportListQuarter ===='+ reportListQuarter );
                
                reportListStage = [select id, name, description from report where ownerid = : myFolderid AND name = 'TO Released by Stage' ];
                
                if(reportListQuarter != null && reportListQuarter.size()>0){
                    report1_byQuarter = new report();
                    report1_byQuarter  = reportListQuarter[0];
                    system.debug('report 1 ===='+ report1_byQuarter);
                 }
                 if(reportListStage != null && reportListStage.size() > 0){    
                    report2_byStage  = new report();
                    report2_byStage  = reportListStage[0];
                    system.debug('report 2===='+ report2_byStage  );
                    
                }  
            }  
            taskOrderList = new List<Task_order__c>();
            taskOrderList =[SELECT id, name  From Task_Order__c where createdDate = LAST_N_DAYS:7];
            system.debug('taskOrderList===='+ taskOrderList);
            
            if(taskOrderList != null && taskOrderList.size()>0 ){
                taskOrderWraList = new List<TaskOrderWrapper>();
                Integer ind = 1;
                for(Task_Order__c taskOrder : taskOrderList ){
                    TaskOrderWrapper tskOrderWra = new TaskOrderWrapper(this ,taskOrder );
                    tskOrderWra.taskOrder = taskOrder ;
                    tskOrderWra.index = ind;
                    ind++;
                    taskOrderWraList.add(tskOrderWra);
                }
            }
            
            tOrderOpenActivityList = new List<Task_Order__c>();
           
            lst = [SELECT Id ,ActivityDate , Description,  OwnerId, Owner.Name, Priority, Status,
                                                        Subject FROM Task WHERE OwnerId =:UserInfo.getUserId() AND Status != 'Completed'];
            
            /*tOrderOpenActivityList = [SELECT id, name, (SELECT Id ,ActivityDate , Description,  IsTask, OwnerId, Owner.Name, Priority, Status,
                                                        Subject   FROM OpenActivities WHERE OwnerId =:UserInfo.getUserId())   FROM task_Order__c ];   
                                                        
           system.debug('tOrderOpenActivityList ===='+ tOrderOpenActivityList );
           if(tOrderOpenActivityList != null && tOrderOpenActivityList.size()>0){
               taskOrderOpnActivityWraList = new List<TaskOrderWrapper>();
               for(Task_Order__c tskOrder : tOrderOpenActivityList ){
                   TaskOrderWrapper tskOrderWra = new TaskOrderWrapper(this ,tskOrder );
                   taskOrderOpnActivityWraList.add(tskOrderWra);
                   system.debug('taskOrderOpnActivityWraList=='+taskOrderOpnActivityWraList);
               }
           }
           */
           
            /*Set<Id> contentDocId = new Set<Id>();
            for(FeedItem cd : [SELECT ContentFileName, RelatedRecordId FROM FeedItem WHERE Type = 'ContentPost'])
            {
                contentDocId.add(cd.RelatedRecordId);
            }
            System.debug(contentDocId);*/        
        
           //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'maiusr ='+ contentDocId));
           //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'usr = '+[Select Id,title,FirstPublishLocationId from ContentVersion]));
          // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'usrNew = '+Database.query('Select Id,title,FirstPublishLocationId from ContentVersion' )));
           //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'usr1 = '+[Select Id,title from ContentDocument where id='06961000000KJfs']));
           contentDocumentList = new List<ContentDocument>();
           contentDocumentList = [SELECT id, ArchivedById, ArchivedDate, ContentModifiedDate, ContentSize, Description,FileExtension, FileType, IsArchived, LastReferencedDate, LastViewedDate, LatestPublishedVersionId,
                                         OwnerId, Owner.Name, ParentId, PublishStatus, Title FROM ContentDocument Order by createdDate ASC LIMIT 10000];
          
           //contentDocumentList = new List<ContentVersion>();
           //contentDocumentList = [SELECT id,title,Description,ContentSize,FileType,FirstPublishLocationId FROM ContentVersion where FirstPublishLocationId In (Select Id From Task_Order__c) Order by createdDate ASC LIMIT 20];       
            
           communityUrlList = [SELECT id, Customer_Community_URL__c, Partner_Community_URL__c FROM Task_Order_Questionnaire__c LIMIT 1];
           User usr = [SELECT id, name, contactId, contact.AccountId, Profile.UserLicense.Name  FROM user WHERE id =: UserInfo.getUserId() ];
           
           if(usr.Profile.UserLicense.Name.containsIgnoreCase('partner')){
               if(communityUrlList[0].Partner_Community_URL__c  != null){
                   returnOpenActivityUrl = communityUrlList[0].Partner_Community_URL__c + '/';
                   returnDocUrl = communityUrlList[0].Partner_Community_URL__c + '/';
                   returnTaskOrderUrl = communityUrlList[0].Partner_Community_URL__c + '/';
               }
           }
           else if(usr.Profile.UserLicense.Name.containsIgnoreCase('Customer')){
               if(communityUrlList[0].Customer_Community_URL__c != null){
                   returnOpenActivityUrl = communityUrlList[0].Customer_Community_URL__c + '/';
                   returnDocUrl = communityUrlList[0].Customer_Community_URL__c + '/';
                   returnTaskOrderUrl = communityUrlList[0].Customer_Community_URL__c + '/';
               }
           }
           else{
                
                string pathUrl = URL.getCurrentRequestUrl().getPath();
                if(pathUrl.indexOf('/apex') > 1){
                    pathUrl = pathUrl.subString(1,pathUrl.indexOf('/apex'));
                }
                returnOpenActivityUrl = '/'+pathUrl +'/';   
                returnDocUrl = '/'+pathUrl  +'/';
                returnTaskOrderUrl = '/'+pathUrl  +'/';
           }
         
          //  ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'usr = '+usr ));
             selecteTaskOrderList = new List<Task_Order__c>();
             selecteTaskOrderList = [Select ID,Name  From Task_Order__c where id In (select Task_Order__c FROM Teaming_Partner__c WHERE status__c = 'Selected') ];                 
         //    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'teamingPartnerList = '+teamingPartnerList.size() ));
    }
    
  /***************************** Task Order Wrapper classes *************************************/
    public class TaskOrderWrapper{
        public Task_Order__c taskOrder{get;set;}
        public Integer index {get;set;}
        //public List<OpenActivityWrapper> openActivityWraList{get;set;}
        TM_CommunityHomePageController commHomeController;
        
        public TaskOrderWrapper(TM_CommunityHomePageController commHomeController, Task_Order__c taskOrder ){
            this.taskOrder = taskOrder ;
            this.commHomeController = commHomeController; 
           // openActivityWraList = new List<OpenActivityWrapper>();
           // Integer i = 1;
           /* List<OpenActivity> openActivityList = taskOrder.openActivities;
            for(OpenActivity opnActivity : openActivityList ){
                OpenActivityWrapper opnActWra = new OpenActivityWrapper (opnActivity , i);
                i++;
                openActivityWraList.add(opnActWra );
                system.debug('openActivityWraList=='+ openActivityWraList);
            } */
        }
    }
    
  /*  public class OpenActivityWrapper{
        public OpenActivity opnActivity {get;set;}
        public Integer srNo {get;set;}
        
        public OpenActivityWrapper(OpenActivity opnActivity, Integer srNo){
            this.opnActivity = opnActivity;
            this.srNo = srNo;
        }
    } */
   /**************************************************************************************/

}