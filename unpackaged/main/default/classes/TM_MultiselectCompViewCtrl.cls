public class TM_MultiselectCompViewCtrl {
    public String selectedValues{get;set;}
    //public List<String> valueList{get;set;}
    public List<String> getValueList(){
        List<String> valueList = new List<String>();
        if(selectedValues!=Null && selectedValues!=''){
            for(String val : selectedValues.split(';')){
                valueList.add(val);
            }
        }
        return valueList;
    }
}