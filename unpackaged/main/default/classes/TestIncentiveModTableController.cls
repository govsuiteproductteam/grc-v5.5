@isTest
public class TestIncentiveModTableController {
    public static testMethod void incentiveModTableTest(){
        Account account = TestDataGenerator.createAccount('Test Account');
        insert account;
        Contract_Vehicle__c contractVehicle =  TestDataGenerator.createContractVehicle('12345', account);
        insert contractVehicle;

       // Id recordTypeId = Schema.SObjectType.Value_Table__c.getRecordTypeInfosByName().get('Bookings').getRecordTypeId();
        Id incentiveAwardFeeRecordTypeId = Schema.SObjectType.Value_Table__c.getRecordTypeInfosByName().get('Incentive Award Fee').getRecordTypeId();
       
        Value_Table__c valueTable1 = TestDataGenerator.createValueTable(incentiveAwardFeeRecordTypeId,contractVehicle.Id);
        insert valueTable1;
        
        Value_Table__c valueTable2 = TestDataGenerator.createValueTable(incentiveAwardFeeRecordTypeId,contractVehicle.Id);
        insert valueTable2;
        
        Value_Table__c valueTable3 = TestDataGenerator.createValueTable(incentiveAwardFeeRecordTypeId,contractVehicle.Id);
        insert valueTable3;
        
        Id valueTableRecordId = valueTable2.Id;
        Test.startTest();
        List<Value_Table__c> inceTableList = new List<Value_Table__c>();
        inceTableList.add(valueTable1);
        String inceTableListStr = JSON.serialize(inceTableList);
        TM_IncentiveModTableController.saveConValModFeeTable(inceTableListStr, contractVehicle.Id, 'abc,'+valueTableRecordId);
        List<Value_Table__c> valueTableList = [SELECT Id FROM Value_Table__c WHERE id =: valueTableRecordId];
        System.assert(valueTableList.size() == 0);
        Contract_Vehicle__c contractVehicle1 =  TM_IncentiveModTableController.getContractVehical(contractVehicle.Id);
        System.assert(contractVehicle1 != null);
        List<Value_Table__c> valueTableList1 = TM_IncentiveModTableController.getValueTableIncModList(contractVehicle.Id);
        System.assert(valueTableList1.size() == 2);
        Value_Table__c newValueTable = TM_IncentiveModTableController.newRowModFeeTable(contractVehicle.Id);
        System.assert(newValueTable != null);
        TM_IncentiveModTableController.isLightningPage();
        Test.stopTest();
    }

}