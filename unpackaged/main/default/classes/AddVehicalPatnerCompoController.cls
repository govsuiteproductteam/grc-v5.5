public with sharing class AddVehicalPatnerCompoController {
    
    
    @AuraEnabled
    public static String checkLicensePermition()
    {
        return LincenseKeyHelper.checkLicensePermitionForFedTOM();
    }
    
    
    @AuraEnabled
    public static List<Vehicle_Partner__c> getVehicalPatner(Id contractVehicalId)
    {
        if(Schema.sObjectType.Vehicle_Partner__c.isAccessible()){
            List<Vehicle_Partner__c> vehicalPatnerList = new  List<Vehicle_Partner__c>();
            if(contractVehicalId!=Null)
            {
                vehicalPatnerList = [select Id,Name,Contract_Vehicle__c,Partner__c,Partner__r.Name,Role__c from Vehicle_Partner__c where Contract_Vehicle__c =: contractVehicalId ORDER BY Partner__r.Name ASC Limit 1000];
            }
            
            return vehicalPatnerList;
        }
        else{
            return null;
        }
    }
 
    
    @AuraEnabled
    public static Map<String,String> getRoleMap()
    {
        Map<String,String> optionsMap=new Map<String,String>();
        if(Schema.sObjectType.Vehicle_Partner__c.isAccessible()){
            Schema.DescribeFieldResult feildResult = Vehicle_Partner__c.Role__c.getDescribe();
            LIST<Schema.PicklistEntry> ple = feildResult.getPicklistValues();
            for(Schema.PicklistEntry f : ple)
            {
                optionsMap.put(f.getLabel(), f.getValue());
            }
            return optionsMap;
        }
        else{
            return null;
        }
    }
    
    @AuraEnabled
    public static Vehicle_Partner__c addNewVehicalPartner(Id contractVehicalId)
    {
        if(Schema.sObjectType.Vehicle_Partner__c.isAccessible() && Schema.sObjectType.Account.isAccessible()){
            id tempOppId;
            list<Account> oaccList=[Select Id from Account limit 1];
            if(oaccList.size()>0){
                tempOppId = oaccList[0].Id;
            }
            else{
                tempOppId=null;
            }
            if(Schema.SObjectType.Vehicle_Partner__c.isCreateable()){
                Vehicle_Partner__c newVehicalpart = new Vehicle_Partner__c (Contract_Vehicle__c = contractVehicalId,Role__c='',Partner__c=tempOppId);
                return newVehicalpart; 
            }
            else{
                return null;
            }
        }
        else{
            return null;
        }
    }
    @AuraEnabled
    public static void deleteVehicalPatner(string delId){
        if(delId!=Null)
        {
            //Code Added to del Teaming Partner related to the particular Vehicle Partner
            List<Teaming_Partner__c> teamingPartnerList = [SELECT Id FROM Teaming_Partner__c WHERE Vehicle_Partner__c =: delId];
            List<Vehicle_Partner__c> delList = [Select Id from Vehicle_Partner__c where Id =: delId];
            try{
                DMLManager.deleteAsUser(teamingPartnerList);
                DMLManager.deleteAsUser(delList);
            }
            
            catch(Exception e)
            {
                System.debug(' Error :-  '+e.getLineNumber()+' '+e.getMessage());
            }
        }
    }
    
    @AuraEnabled
    public static void saveVehicalPatner(string vehicalPatnerList)
    {
        
        List<Vehicle_Partner__c> vehiclePartnerList = new List<Vehicle_Partner__c>();
        if(vehicalPatnerList != null && vehicalPatnerList.length()>1)
        {
            vehicalPatnerList =vehicalPatnerList.substring(1);
            List<String> vehicalPatnerstrList = vehicalPatnerList.split('SPLITPARENT');
            if(vehicalPatnerstrList.size() > 0)
            {
                for(String temp : vehicalPatnerstrList){
                    Vehicle_Partner__c vehiclePartner = new Vehicle_Partner__c();
                    vehiclePartner = getVehicalPatner(temp);
                    vehiclePartnerList.add(vehiclePartner);
                }
            }         
            
            if(!vehiclePartnerList.isEmpty()){
                try{
                    DMLManager.upsertAsUser(vehiclePartnerList);
                }catch(Exception e){
                }
            }
        }
        
    }
    
    private static Vehicle_Partner__c getVehicalPatner(String vehicalPatnerStr){
        if(vehicalPatnerStr != null && vehicalPatnerStr.trim().length() > 0){
            String[] vehicalPatnerStrList = vehicalPatnerStr.split('SPLITDATA');
            if(vehicalPatnerStrList.size() > 0){
                if(Schema.SObjectType.Vehicle_Partner__c.isCreateable()){
                    Vehicle_Partner__c vech = new Vehicle_Partner__c();
                    if(vehicalPatnerStrList[0] != 'undefined'){
                        vech.id = vehicalPatnerStrList[0];
                    } 
                    if(vech.id == null && vehicalPatnerStrList.size() > 1){
                        vech.Contract_Vehicle__c = vehicalPatnerStrList[1];
                    }
                    if(vech.id == null && vehicalPatnerStrList.size() > 2){
                        vech.Partner__c = vehicalPatnerStrList[2];
                    }
                    if(vehicalPatnerStrList.size() > 3){
                        vech.Role__c = vehicalPatnerStrList[3];
                    }
                    return vech;
                }
            }
        }
        return null;
    }
}