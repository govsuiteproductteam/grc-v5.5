global with sharing class AllientSBEmailParser {
    private String []parsedData1;  
    private String []parsedData2;
    
    public String parse(String emailBody){
        System.debug('inside parse method');
        String [] email = emailBody.split('</table>');
  //     String data = emailBody.substringBetween('<table border="1" cellspacing="3" cellpadding="0">', '</table>');
    //    System.debug('data = '+ data);
        String email1 =  email[0];
        String email2 =  email[1];
      
        //System.debug('******data 1 = '+ email[0]);
        //System.debug('*******data 2 = '+ email[1]);
        
        String data1 = email1.substringBetween('</tr>', '</tbody>');
        
        parsedData1 =  data1.split('</td>');
        
       /* for(String d : parsedData1){
            System.debug('*******parse data = '+ d.stripHtmlTags());
        }*/
        //System.debug('*******data 1 = '+ data1.stripHtmlTags());
        //System.debug('*******data 1 = '+ data1.escapeHtml3());
        //System.debug('*******data 1 = '+ data1.escapeHtml4());
        return emailBody;
    }
    
    public String getRFQId(){
        String rFQId = parsedData1[0];
        String cleanRFQId = rFQId.stripHtmlTags();
        return checkValidString(cleanRFQId,80);
    }
    
    public String getAction(){
        String action = parsedData1[1];
        String cleanAction = action.stripHtmlTags();
        return checkValidString(cleanAction,255);
    }
    
    public Date getDate(){
        String dateValue = parsedData1[2];
        String []dateSplit = dateValue.split('/');
        dateValue = dateSplit[0];
        dateValue += '/' + dateSplit[1];
        dateValue += '/' + dateSplit[2].substring(0, 4);
        return Date.parse(dateValue.stripHtmlTags());
    }
    
    public Date getQuotesDueBy(){
       String dateValue = parsedData1[3];
        String []dateSplit = dateValue.split('/');
        dateValue = dateSplit[0];
        dateValue += '/' + dateSplit[1];
        dateValue += '/' + dateSplit[2].substring(0, 4);
        return Date.parse(dateValue.stripHtmlTags());
      
    }
    
    public String getRFQTitle(){
       String rFQTitle = parsedData1[4];
        String cleanRFQTitle = rFQTitle.stripHtmlTags();
        return checkValidString(cleanRFQTitle,255);
    }

    
    private String checkValidString(String str, Integer endLimit){
        if(str.length() > endLimit){
            String validString = str.substring(0,endLimit);
            return validString;
        }
        return str;
    }
    
    public Task_Order__c setTaskOrder(String toAddress){
        if(Schema.sObjectType.Task_Order__c.isAccessible() && Schema.sObjectType.Contract_Vehicle__c.isAccessible()){
        Task_Order__c taskOrder = new Task_Order__c();
        List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];
        if(!conVehicalList.isEmpty()){
            taskOrder.Contract_Vehicle__c = conVehicalList[0].Id;
            taskOrder.Task_Order_Contracting_Organization__c = conVehicalList[0].Account_Name__c;
           }                
       
        taskOrder.Name = getRFQId();
        System.debug('####'+getAction());
        taskOrder.ASB_Action__c = getAction();
         System.debug('#####'+taskOrder.ASB_Action__c);
        taskOrder.Release_Date__c = getDate();
        taskOrder.Due_Date__c = getQuotesDueBy();
         
        taskOrder.Task_Order_Title__c = getRFQTitle();
        taskOrder.Contract_Vehicle_picklist__c = 'ALLIANT SB - ALLIANT SMALL BUSINESS';
       /* taskOrder.Due_Date__c = getDate(parseData('Submission Deadline'));
        taskOrder.Release_Date__c = date.today();
        taskOrder.Task_Order_Title__c = parseData('Title');
        taskOrder.Description__c = parseData('Description');
        taskOrder.Customer_Comments__c = parseData('Customer Comments:');
        List<Id>contactIdSet = getContactId(parseContact('Customer CO/KO','Customer COR'));
        taskOrder.Contracting_Officer__c = contactIdSet[0];        */
        return taskOrder;
        }else{
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
        }
    }
}