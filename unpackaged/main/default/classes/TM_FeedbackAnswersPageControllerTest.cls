@isTest
public with sharing class TM_FeedbackAnswersPageControllerTest{
    public static TestMethod void TestTM_FeedbackAnswersPageController(){
        Test.startTest();
        List<Profile> profileList = [SELECT Id FROM Profile where (Name = 'Custom Partner Community User' OR Name = 'Custom Partner Community Login User' OR  Name  = 'Custom Customer Community Plus Login User') Limit 1];
        if(profileList != null && profileList.size()>0){
        
            Account acc = TestDataGenerator.createAccount('TaskOrderCapture');
            insert acc;
            
            Contact con = TestDataGenerator.createContact('Avanti',acc);
            insert con;
            
            User usr = TestDataGenerator.createUser(con,TRUE,'test12345', 'a1test','test12345678@gmail.com','taskordercap123454@demo.com','a1test',profileList[0],'America/Los_Angeles','en_US','UTF-8','en_US');
            insert usr;    
            
            Contract_Vehicle__c contractVehicle = TestDataGenerator.createContractVehicle('TMconVehicle',acc);
            insert contractVehicle;
            
            Task_Order__c taskOrder = TestDataGenerator.createTaskOrder('TMtaskOrder', acc, contractVehicle);
            insert taskOrder;
            
            Survey__c sur = TestDataGenerator.createSurvey();
            insert sur;
           
            Survey_Task__c surveyTask = TestDataGenerator.createSurveyTask(sur ,taskOrder);
            insert surveyTask ;
            
            Question__c que = TestDataGenerator.createQuestion('What',sur);
            insert que;
            
            Feedback__c feedBack = TestDataGenerator.createFeedback(con, surveyTask , usr);
            insert feedBack;
            
            Survey_Answer__c surveyAns = TestDataGenerator.createSurveyAnswer(feedBack , que , surveyTask  );
            surveyAns.Option_1__c = 'opt1';
            surveyAns.Option_2__c = 'opt2';
            surveyAns.Option_3__c = 'opt3';
            surveyAns.Option_4__c = 'opt4';
            surveyAns.Option_5__c = 'opt5';
            surveyAns.Option_6__c = 'opt6';
            surveyAns.Option_7__c = 'opt7';
            surveyAns.Option_8__c = 'opt8';
            surveyAns.Option_9__c = 'opt9';      
            surveyAns.Option_10__c = 'opt10';     
            insert surveyAns;
            
            
            
            
            ApexPages.CurrentPage().getparameters().put('id',feedBack.id);
            ApexPages.StandardController controller = new ApexPages.StandardController(feedBack);
            TM_FeedbackAnswersPageController feedbackAnsController = new TM_FeedbackAnswersPageController(controller);
            feedbackAnsController.init();
            system.assertNotEquals(feedbackAnsController.feedbackList.size() , 0);
            Test.stopTest();
        }
    }
}