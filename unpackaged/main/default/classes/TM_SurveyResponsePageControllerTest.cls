@isTest
public with sharing class TM_SurveyResponsePageControllerTest{
    public static TestMethod void TestTM_SurveyResponsePageController(){
        List<Profile> profileList = [SELECT Id FROM Profile where (Name = 'Custom Partner Community User' OR Name = 'Custom Partner Community Login User' OR Name  = 'Custom Customer Community Plus Login User') Limit 1];
        if(profileList != null && profileList.size()>0){
        
            Account acc = TestDataGenerator.createAccount('TaskOrderCapture');
            insert acc;
            
            Contact con = TestDataGenerator.createContact('Avanti',acc);
            insert con;
            
            User usr = TestDataGenerator.createUser(con,TRUE,'srptest1', 'atest1','srptest1@gmail.com','srptaskordercapture@demo.com','atest1',profileList[0],'America/Los_Angeles','en_US','UTF-8','en_US');
            insert usr;    
            
            Contract_Vehicle__c contractVehicle = TestDataGenerator.createContractVehicle('TMconVehicle',acc);
            insert contractVehicle;
            
            Task_Order__c taskOrder = TestDataGenerator.createTaskOrder('TMtaskOrder', acc, contractVehicle);
            insert taskOrder;
            
            Survey__c sur = TestDataGenerator.createSurvey();
            insert sur;
           
            Survey_Task__c surveyTask = TestDataGenerator.createSurveyTask(sur ,taskOrder);
            insert surveyTask ;
            
            List<Question__c > queList = new List<Question__c >();
            Question__c que1 = TestDataGenerator.createQuestion('What',sur);
            que1.Option_1__c ='opt1';
            que1.Option_2__c ='opt2';
            que1.Option_3__c ='opt3';
            que1.Option_4__c ='opt4';
            que1.Option_5__c ='opt5';
            que1.Option_6__c ='opt6';
            que1.Option_7__c ='opt7'; 
            que1.Option_8__c ='opt8';
            que1.Option_9__c ='opt9';   
            que1.Option_10__c ='opt10';  
            que1.Question_Type__c = 'Radio';
            que1.Is_mandatory__c = true;
            que1.Answer__c = '';
            queList.add(que1);
            
            Question__c que2 = TestDataGenerator.createQuestion('What',sur);
            que2.Option_1__c ='opt1';
            que2.Option_2__c ='opt2';
            que2.Option_3__c ='opt3';
            que2.Option_4__c ='opt4';
            que2.Option_5__c ='opt5';
            que2.Option_6__c ='opt6';
            que2.Option_7__c ='opt7'; 
            que2.Option_8__c ='opt8';
            que2.Option_9__c ='opt9';   
            que2.Option_10__c ='opt10';  
            que2.Question_Type__c = 'Text';
            que2.Is_mandatory__c = true;
            que2.Answer__c = '';
            queList.add(que2);
            
            Question__c que3 = TestDataGenerator.createQuestion('What',sur);
            que3.Option_1__c ='opt1';
            que3.Option_2__c ='opt2';
            que3.Option_3__c ='opt3';
            que3.Option_4__c ='opt4';
            que3.Option_5__c ='opt5';
            que3.Option_6__c ='opt6';
            que3.Option_7__c ='opt7'; 
            que3.Option_8__c ='opt8';
            que3.Option_9__c ='opt9';   
            que3.Option_10__c ='opt10';  
            que3.Question_Type__c = 'Descriptive';
            que3.Is_mandatory__c = true;
            que3.Answer__c = '';
            queList.add(que3);
            
            Question__c que4 = TestDataGenerator.createQuestion('What',sur);
            que4.Option_1__c ='opt1';
            que4.Option_2__c ='opt2';
            que4.Option_3__c ='opt3';
            que4.Option_4__c ='opt4';
            que4.Option_5__c ='opt5';
            que4.Option_6__c ='opt6';
            que4.Option_7__c ='opt7'; 
            que4.Option_8__c ='opt8';
            que4.Option_9__c ='opt9';   
            que4.Option_10__c ='opt10';  
            que4.Question_Type__c = 'Picklist';
            que4.Is_mandatory__c = false;
            que4.Answer__c = '';
            queList.add(que4);
            
            Question__c que5 = TestDataGenerator.createQuestion('What',sur);
            que5.Option_1__c ='opt1';
            que5.Option_2__c ='opt2';
            que5.Option_3__c ='opt3';
            que5.Option_4__c ='opt4';
            que5.Option_5__c ='opt5';
            que5.Option_6__c ='opt6';
            que5.Option_7__c ='opt7'; 
            que5.Option_8__c ='opt8';
            que5.Option_9__c ='opt9';   
            que5.Option_10__c ='opt10';  
            que5.Question_Type__c = 'Checkbox';
            que5.Is_mandatory__c = false;
            que5.Answer__c = '';
            queList.add(que5);
            
            insert queList;
            
            Feedback__c feedBack = TestDataGenerator.createFeedback(con, surveyTask , usr);
            insert feedBack;
            
            List<Survey_Answer__c > surAnsList = new List<Survey_Answer__c >();
            Survey_Answer__c surveyAns1 = TestDataGenerator.createSurveyAnswer(feedBack , que1 , surveyTask  );
            surveyAns1.Option_1__c = 'opt1';
            surveyAns1.Option_2__c = 'opt2';
            surveyAns1.Option_3__c = 'opt3';
            surveyAns1.Option_4__c = 'opt4';
            surveyAns1.Option_5__c = 'opt5';
            surveyAns1.Option_6__c = 'opt6';
            surveyAns1.Option_7__c = 'opt7';
            surveyAns1.Option_8__c = 'opt8';
            surveyAns1.Option_9__c = 'opt9';      
            surveyAns1.Option_10__c = 'opt10';
            surveyAns1.Answer_Descriptive__c = 'ans1';
            surAnsList.add(surveyAns1 ); 
            
            Survey_Answer__c surveyAns2 = TestDataGenerator.createSurveyAnswer(feedBack , que2 , surveyTask  );
            surveyAns2.Option_1__c = 'opt1';
            surveyAns2.Option_2__c = 'opt2';
            surveyAns2.Option_3__c = 'opt3';
            surveyAns2.Option_4__c = 'opt4';
            surveyAns2.Option_5__c = 'opt5';
            surveyAns2.Option_6__c = 'opt6';
            surveyAns2.Option_7__c = 'opt7';
            surveyAns2.Option_8__c = 'opt8';
            surveyAns2.Option_9__c = 'opt9';      
            surveyAns2.Option_10__c = 'opt10';
            surveyAns1.Answer_Descriptive__c = 'ans2';
            surAnsList.add(surveyAns2 ); 
            
            Survey_Answer__c surveyAns3 = TestDataGenerator.createSurveyAnswer(feedBack , que3 , surveyTask  );
            surveyAns3.Option_1__c = 'opt1';
            surveyAns3.Option_2__c = 'opt2';
            surveyAns3.Option_3__c = 'opt3';
            surveyAns3.Option_4__c = 'opt4';
            surveyAns3.Option_5__c = 'opt5';
            surveyAns3.Option_6__c = 'opt6';
            surveyAns3.Option_7__c = 'opt7';
            surveyAns3.Option_8__c = 'opt8';
            surveyAns3.Option_9__c = 'opt9';      
            surveyAns3.Option_10__c = 'opt10';
            surveyAns1.Answer_Descriptive__c = 'ans3';
            surAnsList.add(surveyAns3); 
            
            Survey_Answer__c surveyAns4 = TestDataGenerator.createSurveyAnswer(feedBack , que4 , surveyTask  );
            surveyAns4.Option_1__c = 'opt1';
            surveyAns4.Option_2__c = 'opt2';
            surveyAns4.Option_3__c = 'opt3';
            surveyAns4.Option_4__c = 'opt4';
            surveyAns4.Option_5__c = 'opt5';
            surveyAns4.Option_6__c = 'opt6';
            surveyAns4.Option_7__c = 'opt7';
            surveyAns4.Option_8__c = 'opt8';
            surveyAns4.Option_9__c = 'opt9';      
            surveyAns4.Option_10__c = 'opt10';
            surveyAns1.Answer_Descriptive__c = 'ans4';
            surAnsList.add(surveyAns4); 
            
            Survey_Answer__c surveyAns5 = TestDataGenerator.createSurveyAnswer(feedBack , que5 , surveyTask  );
            surveyAns5.Option_1__c = 'opt1';
            surveyAns5.Option_2__c = 'opt2';
            surveyAns5.Option_3__c = 'opt3';
            surveyAns5.Option_4__c = 'opt4';
            surveyAns5.Option_5__c = 'opt5';
            surveyAns5.Option_6__c = 'opt6';
            surveyAns5.Option_7__c = 'opt7';
            surveyAns5.Option_8__c = 'opt8';
            surveyAns5.Option_9__c = 'opt9';      
            surveyAns5.Option_10__c = 'opt10';
            surveyAns1.Answer_Descriptive__c = 'ans5';
            surAnsList.add(surveyAns5); 
                
            insert surAnsList;
            
            Test.startTest();
            ApexPages.CurrentPage().getparameters().put('sid', surveyTask.id);
            ApexPages.StandardController controller = new  ApexPages.StandardController(surveyTask );
            TM_SurveyResponsePageController surveyRespController = new TM_SurveyResponsePageController (controller );
            
           surveyRespController.saveFeedback();
            surveyRespController.queWraList[0].question =  que1;
            surveyRespController.queWraList[0].question.Question_Type__c = 'Radio';
            surveyRespController.queWraList[0].optionsList[3].options = que1.Option_3__c;
            surveyRespController.queWraList[0].optionsList[4].options = que1.Option_4__c;
            surveyRespController.queWraList[0].radioSelected = que1.Option_1__c;
            surveyRespController.queWraList[0].question.Is_Mandatory__c = TRUE;
          
            
            surveyRespController.queWraList[1].question =  que2;
            surveyRespController.queWraList[0].question.Question_Type__c = 'Text';
            surveyRespController.queWraList[0].question.Is_Mandatory__c = TRUE;
            surveyRespController.queWraList[0].question.Answer__c = 'testText';
            
            
            surveyRespController.queWraList[1].question =  que3;
            surveyRespController.queWraList[0].question.Question_Type__c = 'Descriptive';
            surveyRespController.queWraList[0].question.Is_Mandatory__c = TRUE;
            surveyRespController.queWraList[0].question.Answer__c = 'descAns';
            
            
            surveyRespController.queWraList[1].question =  que4;
            surveyRespController.queWraList[0].question.Question_Type__c = 'Picklist';
            surveyRespController.queWraList[1].optionsList[0].options = que4.Option_1__c;
            surveyRespController.queWraList[1].optionsList[1].options = que4.Option_2__c;
            surveyRespController.queWraList[0].question.Is_Mandatory__c = TRUE;
            surveyRespController.queWraList[0].picklistSelected = que1.Option_1__c;
           
            
            surveyRespController.queWraList[1].question =  que5;
            surveyRespController.queWraList[0].optionsList[0].checkboxChecked = true;
            surveyRespController.queWraList[0].question.Question_Type__c = 'Checkbox';
            surveyRespController.queWraList[1].optionsList[0].options = que5.Option_1__c;
            surveyRespController.queWraList[1].optionsList[1].options  = que5.Option_2__c;
            surveyRespController.queWraList[0].question.Is_Mandatory__c = TRUE;
           
          // surveyRespController.init();
            surveyRespController.saveFeedback();
            pageReference  pgRe = surveyRespController.callafterfinish();
            system.assertNotEquals(pgRe , null);
              
            PageReference pgRef = surveyRespController.submit();
            system.assertEquals(pgRef, null);
            system.assertNotEquals(surveyRespController.queWralist.size(), 0);
            Test.stopTest();
        }
    }
    
}