/***************************************************************
   Description : TestDataGenerator for the TaskOrder Survey App
*****************************************************************/
  public with sharing class TestDataGenerator
  {
         public static Account createAccount(String accName)
         {
               Account acc = new Account();
               acc.Name = accName;
               return acc;
         
         }
  
         public Static Contact createContact(String conLastName, Account acc)
         {
               Contact con = new Contact();
               con.LastName = conLastName;
               con.AccountId = acc.id;
               return con;
          
         }
            public static User createUser (Contact con,boolean usractive, string usrLastName,string usrAlias, string usrEmail, string usrUsername,string usrCommunityNickname, Profile ProfileId, string usrTimeZoneSidKey, string usrLocaleSidKey, string usrEmailEncodingKey,string usrLanguageLocaleKey)
           {
                    User usr = new User ();   
               if(con != null)
                    usr.ContactId = con.Id; 
                    usr.IsActive =  usractive;
                    usr.LastName = usrLastName;
                    usr.Alias = usrAlias;
                    usr.Email = usrEmail;
                    usr.Username = usrUsername;
                    usr.CommunityNickname = usrCommunityNickname;
                    usr.ProfileId  = ProfileId.Id ;
                    usr.TimeZoneSidKey = usrTimeZoneSidKey;
                    usr.LocaleSidKey = usrLocaleSidKey;
                    usr.EmailEncodingKey = usrEmailEncodingKey;
                    usr.LanguageLocaleKey = usrLanguageLocaleKey;      
                    return usr;
            }   
            
         public static Contract_Vehicle__c createContractVehicle(string ContractVehicleNumber, Account acc)
         {
                Contract_Vehicle__c contractVehicle = new Contract_Vehicle__c();
                contractVehicle.Name = ContractVehicleNumber;
                contractVehicle.Account_Name__c = acc.id;
                return contractVehicle ;
         }
  
         public static Contract_Vehicle_Contact__c createContractVehicleContact(Contact con,Contract_Vehicle__c contractVehicle)
         {
                 Contract_Vehicle_Contact__c contractVehicleCon = new Contract_Vehicle_Contact__c();
                 contractVehicleCon.Contact__c = con.id;
                 contractVehicleCon.Contract_Vehicle__c = contractVehicle.id;
                 return contractVehicleCon;
         }
         
         public static Task_Order__c createTaskOrder(String TaskOrderNumber, Account acc, Contract_Vehicle__c contractVehicle)
         {
              Task_Order__c taskOrder = new Task_Order__c();
              taskOrder.Name = TaskOrderNumber;
              taskOrder.Contracting_Organization__c = acc.id;
              taskOrder.Contract_Vehicle__c = contractVehicle.id;
             // taskOrder.Contractor__c = acc.id;
              taskOrder.Customer_Agency__c= acc.id;
              taskOrder.Task_Order_Contracting_Organization__c = acc.id;
              //taskOrder.Teaming_Partner__c = acc.id;
              return taskOrder;
         
         }
         
         public static Folder__c createFolder(Task_Order__c taskOrder)
         {
               Folder__c folder = new Folder__c();
               folder.Task_Order__c = taskOrder.id;
               return folder;
         }
         
         public static Vehicle_Partner__c createVehiclePartner(Account acc,Contract_Vehicle__c contractVehicle)
         {
               Vehicle_Partner__c vehiclePartner = new Vehicle_Partner__c();
               vehiclePartner.Contract_Vehicle__c = contractVehicle.id;
               vehiclePartner.Partner__c = acc.id;
               
               return vehiclePartner;
         
         }
         
          public static Task_Order_Contact__c createTaskOrderContact(Task_Order__c taskOrder, Contact con)
          {          
              Task_Order_Contact__c taskOrderCon = new Task_Order_Contact__c();
              taskOrderCon.Contact__c = con.id;
              taskOrderCon.Task_Order__c = taskOrder.id;       
              return  taskOrderCon;       
          }
        
            public Static Survey__c createSurvey()
            {
                Survey__c survey  = new Survey__c();
                return survey; 
            }
        
      public Static Questionaries_Email_Template__c addQuestionarieEmailTemplate(Id surveyId, Id emailTempleteId){
          return new Questionaries_Email_Template__c(Email_Templete__c=emailTempleteId,Teaming_Request_Questionnaire__c = surveyId);
      }
         
        
        public static Survey_Task__c createSurveyTask (Survey__c survey,Task_Order__c taskOrder)
         {
              Survey_Task__c surveyTask = new Survey_Task__c();
              surveyTask.Survey__c = survey.id;
              surveyTask.Task_Order__c = taskOrder.id;    
               return surveyTask;
          }  
          
          
           public static Question__c createQuestion(String QuestionsName, Survey__c survey)
            {
              Question__c question = new Question__c();
              question.Name = QuestionsName;
              question.Survey__c = survey.id;
              return question;       
           }  
           
           public Static Teaming_Partner__c createTeamingPartner(Vehicle_Partner__c vehiclePartner,Task_Order__c taskOrder)
           {   
                Teaming_Partner__c teamPartner = new Teaming_Partner__c ();
                teamPartner.Vehicle_Partner__c = vehiclePartner.id;
                teamPartner.Task_Order__c = taskOrder.id;     
                return teamPartner;
            } 
            
          
            
            public static Feedback__c createFeedback(Contact  con, Survey_Task__c surveyTask, User user)
            {
                 Feedback__c  feedback = new  Feedback__c();
                 feedback.Contact__c = con.id;
                 feedback.Survey_Task__c = surveyTask.id;
                 feedback.User__c = user.id;
                 return feedback ;
            }
            
           
           public static Survey_Answer__c createSurveyAnswer(Feedback__c feedback , Question__c question, Survey_Task__c surveyTask)
              {
                  Survey_Answer__c  surveyAns = new Survey_Answer__c();
                  surveyAns.Feedback__c  = feedback.id;
                  surveyAns.Question__c  = question.id;
                  surveyAns.Survey_Taken__c = surveyTask.id;
                  return surveyAns ;
              }  
              
           public static Task createTask(String sub, date tskActivityDate, string strStatus, string strPriority  ){
               Task tsk = new Task();
               tsk.Subject = sub;
               tsk.ActivityDate = tskActivityDate;
               tsk.Status = strStatus  ; 
               tsk.Priority = strPriority ;
               return tsk;
           } 
           
           // Custom setting
           public static  Task_Order_Questionnaire__c createTaskOrderQue (String customerUrl, String partnerUrl){
               Task_Order_Questionnaire__c taskOrderQue = new Task_Order_Questionnaire__c();
               taskOrderQue.Customer_Community_URL__c = customerUrl;
               taskOrderQue.Partner_Community_URL__c = partnerUrl;
               return taskOrderQue;
           }
           
           
           
          public static Project__c createProject (Contract_Vehicle__c contractVehicle ,String status){
               Project__c project = new Project__c ();
               project.Contract_Vehicle__c = contractVehicle.id;
                 project.Status__c = status;
                // project.Labor_Category_Assignment_Required__c = laborCatAssReq;
                 project.Reason__c = 'TestReason';
                 project.Number__c = '1234123';
               return project;
           }
           
           public Static CLM_Labor_Category__c createClmLabCategory(Contract_Vehicle__c contractVehicle , String reqEducationLevel , Project__c proj){
               CLM_Labor_Category__c clmLaborCatgory = new CLM_Labor_Category__c ();
               clmLaborCatgory.Contract_Vehicle__c = contractVehicle.id;
               clmLaborCatgory.Req_Education_Level__c = reqEducationLevel ;
               clmLaborCatgory.Project__c = proj.id;
               return clmLaborCatgory ;
           }
           
           public Static LC_Candidate_Assignment__c createLcCandidateAssign ( Project__c  proj,CLM_Labor_Category__c clmLaborCat ){
               LC_Candidate_Assignment__c lcCandidateAssign = new LC_Candidate_Assignment__c();
               lcCandidateAssign.CLM_Labor_Category__c = clmLaborCat.id;
               lcCandidateAssign.Project_LCDB__c = proj.id;
               return lcCandidateAssign ;
           }
           
           public Static Candidate__c createCandidate (String status, String TestFirstName, String TestLastName,  String emailID){
               Candidate__c candidate = new Candidate__c ();
             //  candidate.Candidate_availability__c = 'Available';
               candidate.Education_Level__c = 'Masters Degree';
               candidate.Status__c = status;
               candidate.Years_of_experience__c = 2;
               candidate.Firstname__c = TestFirstName;
               candidate.Lastname__c = TestLastName;
               candidate.Primary_phone__c = '1235684541';
               candidate.Email__c = emailID;
               return candidate ;
           }
           
           public Static Candidate_Application__c createCandidateApp(CLM_Labor_Category__c clmLaborCatgory, Candidate__c candidate ){
               Candidate_Application__c candidateApp = new Candidate_Application__c ();
               candidateApp.Labor_Category__c  = clmLaborCatgory.id;
               candidateApp.Candidate__c = candidate.id;
             //  candidateApp.Project_LCDB__c = proj.id;
               return candidateApp;
           }
      
      /*New methode add in class during migretion of Fed CLM Lightning*/
      
       public static Value_Table__c createValueTable(id recordTypeId,Id contractVehicleId){
        Value_Table__c valuetable = new Value_Table__c();
        valueTable.RecordTypeId = recordTypeId;
        valueTable.Start_Date__c = Date.today();
        valueTable.End_Date__c = Date.today();
        valueTable.Contract_Vehicle__c = contractVehicleId;
        return valueTable;
    }
      public static CLM_AtaGlanceField__c createAtaGlanceField(String recordTypeId){
        recordTypeId = recordTypeId.substring(0,15);
        CLM_AtaGlanceField__c AtaGlanceField = new CLM_AtaGlanceField__c();
        AtaGlanceField.Color__c='Red';
        AtaGlanceField.ColSpan__c=2;
        AtaGlanceField.Document_Id__c='';
        AtaGlanceField.FieldApiName__c='Name';
        AtaGlanceField.FieldLabel__c='Name';
        AtaGlanceField.Is_Component__c=false;
        AtaGlanceField.Name='RecOppAtAGlaance';
        AtaGlanceField.Order__c=1;
        AtaGlanceField.RecordType_Id__c=recordTypeId;
        
        return AtaGlanceField;
    }
       public static String getPicklistValue(String ObjectApi_name,String Field_name){ 
        List<String> lstPickvals=new List<String>();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectApi_name);//From the Object Api name retrieving the SObject
        Sobject Object_name = targetType.newSObject();
        Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
        List<Schema.PicklistEntry> pick_list_values = field_map.get(Field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
        for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
            lstPickvals.add(''+a.getValue());//add the value  to our final list
        }
        if(lstPickvals !=null && ! lstPickvals.isEmpty())
            return lstPickvals[0];
        else return 'Test';
    }
      public static SubContract__c createSubContract(string subContractNumber)
    {
        SubContract__c subContract = new SubContract__c();
        subContract.Sub_Contract_Number__c = subContractNumber;
        return subContract ;
    }
         public static Clauses__c getClause(String clauseDescription){
        Clauses__c tempClause = new Clauses__c();
        tempClause.Agency__c = TestDataGenerator.getPicklistValue('TM_TOMA__Clauses__c', 'TM_TOMA__Agency__c');
        tempClause.Center__c = TestDataGenerator.getPicklistValue('TM_TOMA__Clauses__c', 'TM_TOMA__Center__c');
        tempClause.Clause_Description__c = clauseDescription;
        tempClause.Clause_Type__c = TestDataGenerator.getPicklistValue('TM_TOMA__Clauses__c', 'TM_TOMA__Clause_Type__c');
        tempClause.Department__c = TestDataGenerator.getPicklistValue('TM_TOMA__Clauses__c', 'TM_TOMA__Department__c');
        return tempClause;
    } 
       public static Contract_Clauses__c getContractClause(id contractId,id clauseId){
        Contract_Clauses__c tempContractClause = new Contract_Clauses__c();
        tempContractClause.Contract_Vehicle__c = contractId;
        tempContractClause.Clauses__c = clauseId;
        return tempContractClause;
    }
    
     public static CLM_Tab__c createCLMTab(String recordTypeId){
        recordTypeId = recordTypeId.substring(0, 15);
        CLM_Tab__c clmTab = new CLM_Tab__c();
        clmTab.Name = 'Test 2-10001Last ModifiedX7gk9';
        clmTab.BackgroundColor__c = 'Red';
        clmTab.TabName__c = 'Test Tab';
        clmTab.RecordType_Id__c = recordTypeId;
        clmTab.Is_Contract__c = true;
        clmTab.Order__c = 1;
        return clmTab;
    }
    
    public static CLM_Section__c createCLMSection(String recordTypeId){
        recordTypeId = recordTypeId.substring(0, 15);
        CLM_Section__c clmSection = new CLM_Section__c();
        clmSection.Name = 'Test 2-10001Last ModifiedX7gk9';
        clmSection.BackgroundColor__c = 'Red';
        clmSection.Height__c = 100;
        clmSection.RecordType_Id__c = recordTypeId;
        clmSection.No_Of_Column__c = 2;
        clmSection.Is_Contract__c = true;
        clmSection.Order__c = 10;
        clmSection.SectionName__c = 'Test';
        clmSection.TabNumber__c = 1;
        clmSection.Width__c=100;
        return clmSection;
    }
    
    public static CLM_FIeld__c createCLMField(String recordTypeId){
        recordTypeId = recordTypeId.substring(0, 15);
        CLM_FIeld__c clmField = new CLM_FIeld__c();
        clmField.Name = 'Test 2-10001Last ModifiedX7gk9';
        clmField.FieldApiName__c = 'LastModifiedById';
        clmField.FieldLabel__c = 'Last Modified By ID';
        clmField.Order__c = 1;
        clmField.RecordType_Id__c = recordTypeId;
        clmField.TabNumber__c = 1;
        clmField.Is_Contract__c = true;
        return clmField;
    }
    
    public static Opportunity createOpportunity(Id accountId){
        Opportunity oppt = new Opportunity(Name ='Test Opp',
                                           AccountID = accountId,
                                           StageName = 'Customer Won',
                                           Amount = 3000,
                                           CloseDate = System.today()
                                         );

   
        return oppt;
    }
    
    public static Contract_Clauses__c  getSubContractClause(id subContractId,id clauseId){
        Contract_Clauses__c tempSubContractClause = new Contract_Clauses__c ();
        tempSubContractClause.SubContract__c = subContractId;
        tempSubContractClause.Clauses__c = clauseId;
        return tempSubContractClause;
    }
     public static Customer_Contacts__c createCustomerContact(Contact con,Contract_Vehicle__c contractVehicle)
    {
        Customer_Contacts__c customerCon = new Customer_Contacts__c();
        customerCon.Name__c = con.Id;
        customerCon.Contract_Vehicle__c = contractVehicle.id;
        return customerCon;
    }
    
    public static Attachment createAttachment(String parentId){
        Blob b = Blob.valueOf('Test Data');
        
        Attachment attachment = new Attachment();
        attachment.ParentId = parentId;
        attachment.Name = 'Test Attachment for Parent';
        attachment.Body = b;
        return attachment;
    }
      
       public static Contract_Mods__c  getContractMods(id RecordTypeId,id fedCapId,String taskOrdertitle){
        Contract_Mods__c con=new Contract_Mods__c();
        con.RecordTypeId=RecordTypeId;
        con.taskOrder__c=fedCapId;
        con.Task_Order_Title__c=taskOrdertitle;
        return con;
    }
       public static Contract_Mods__c  updateContractMods(Contract_Mods__c conId,String taskOrdertitle){
        Contract_Mods__c con=conId;
        con.New_Task_Order_Title__c=taskOrdertitle;
        return con;
    }
}