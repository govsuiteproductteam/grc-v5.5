public class TM_RedirectForContratController {
    @AuraEnabled
    public static String getDefaultReocrdTypeId(){
        List<Id> ValidRecTypeId = New List<Id>();
        List<String> recordTypeNameList = new List<String>();
        Schema.DescribeSObjectResult R = TM_TOMA__Contract_Vehicle__c.SObjectType.getDescribe();
        List<Schema.RecordTypeInfo> RT = R.getRecordTypeInfos();
        for( Schema.RecordTypeInfo recordType : RT )
        {
            if(recordType.isAvailable())
            { 
                /* Check if the recordtype is visible for the current user or not. */
                if(recordType.getName() !='Master'){
                    //bidRecTypeId = (''+recordType.getRecordTypeId()).substring(0,15);
                    //bidObj.RecordTypeId = bidRecTypeId;
                    ValidRecTypeId.add(recordType.getRecordTypeId());
                }
            }
        }
        system.debug('ValidRecTypeId'+ValidRecTypeId);
        //checking object level recordtype Permission.Active/Inactive
        if(ValidRecTypeId != null && ! ValidRecTypeId.isEmpty()){
            String recordTypeAvailable = ValidRecTypeId[0];
            List<RecordType> tempRecList = [SELECT Id,Name FROM RecordType WHERE isActive = true AND Id=:recordTypeAvailable];
            if(tempRecList != null && ! tempRecList.isEmpty()){
                return recordTypeAvailable;
            }else{
                return null;
            }
        }else{
            return null;
        }
    }
    
}