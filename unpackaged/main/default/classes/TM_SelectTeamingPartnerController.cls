global with sharing class TM_SelectTeamingPartnerController {
    
    public Id tOrderId {get;set;}
    public Id ConVehicleId {get;set;}
    public List<Vehicle_Partner__c> vehiclePartnerList {get;set;}
    public Contract_Vehicle__c conVehicle {get;set;}
    public List<VehiclePartnerwrapper> vehiclePartnerWrapList{get;set;} 
    public List<Teaming_Partner__c> teamingPartnerList {get;set;}
    public String isValidUser{get; set;}
    
    public TM_SelectTeamingPartnerController(ApexPages.StandardController controller) {
        isValidUser = LincenseKeyHelper.checkLicensePermitionForFedTOM();
        if(isValidUser != 'Yes')
            return;
     if(Schema.sObjectType.Vehicle_Partner__c.isAccessible() && Schema.sObjectType.Teaming_Partner__c.isAccessible() && Schema.sObjectType.Task_Order__c.isAccessible() && Schema.sObjectType.Task_Order__share.isAccessible()){
        
        tOrderId = ApexPages.CurrentPage().getparameters().get('tid');
        if(tOrderId != null){
            Task_Order__c taskOrder = [Select Id,Contract_Vehicle__c From Task_Order__c where id =: tOrderId ];
            conVehicleId = taskOrder.Contract_Vehicle__c;
            vehiclePartnerList = new List<Vehicle_Partner__c>();
            teamingPartnerList = new List<Teaming_Partner__c>();
        
            
            if(conVehicleId != null){
                List<Vehicle_Partner__c> inputVehiclePartnerList = [SELECT id, name , Contract_Vehicle__c,Point_of_Contact__r.name,Role__c,Partner__r.name FROM Vehicle_Partner__c WHERE Contract_Vehicle__c =: conVehicleId];    
                Map<Id,Vehicle_Partner__c> vehiclePartnerMap = new Map<ID,Vehicle_Partner__c>(inputVehiclePartnerList );
                
                List<Teaming_Partner__c> teminPartList = [Select Id,Vehicle_Partner__c From Teaming_Partner__c where Task_Order__c= : tOrderId ];
                Set<Id> vehiclePartnerIdSet = new Set<Id>();
                for(Teaming_Partner__c teamingPartner : teminPartList ){
                    vehiclePartnerIdSet.add(teamingPartner.Vehicle_Partner__c );
                }
                
               /* for(Id vehicalePartnerId : vehiclePartnerIdSet){
                   vehiclePartnerMap.remove(vehicalePartnerId );
                    
                }*/
                
                for(Id vehPartnerId : vehiclePartnerMap.keySet() ){
                    if(!vehiclePartnerIdSet.contains(vehPartnerId )){
                         Vehicle_Partner__c vehiclePartner = vehiclePartnerMap.get(vehPartnerId );
                         if(vehiclePartner != null){
                            vehiclePartnerList.add(vehiclePartner );
                         }
                    }
                }
                
                system.debug('vehiclePartnerList == '+ vehiclePartnerList );
                 try{
                   if(vehiclePartnerList != null && vehiclePartnerList .size()>0){
                       vehiclePartnerWrapList = new List<VehiclePartnerwrapper>();
                             for(Vehicle_Partner__c vehPartner : vehiclePartnerList){
                                 
                                 VehiclePartnerwrapper vehPartWrap = new VehiclePartnerwrapper();
                                 vehPartWrap.vehiclePartner = vehPartner ;
                                 vehPartWrap.status = false;
                                 vehiclePartnerWrapList.add(vehPartWrap);
                             }
                   }
                   
                 }
                 catch(Exception e){
                      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, e.getMessage()));
                 }
             }  
            // teamingPartnerList = [select id,name, Vehicle_Partner__r.name,Vehicle_Partner__r.point_of_contact__c, Vehicle_Partner__r.role__c FROM Teaming_Partner__c];
             //system.debug('teamingPartnerList ==='+teamingPartnerList );
         }
         
       }   
    }
    
    // addTeamingPartner() method is used to add add teaming partner and share the task order.
    public PageReference addTeamingPartner(){
        if(Schema.sObjectType.Teaming_Partner__c.isAccessible() ){
            List<Teaming_Partner__c> teamingPartnerList = new List<Teaming_Partner__c>();
            List<TeamingPartnerwrapper> teamingPartnerwraList = new List<TeamingPartnerwrapper>();
             
             if(vehiclePartnerWrapList != null && vehiclePartnerWrapList.size()>0){
                  for(VehiclePartnerwrapper vehPartnerW : vehiclePartnerWrapList ){
                     if(vehPartnerW.status == TRUE){                         
                          TeamingPartnerwrapper teamingPartnerW = new TeamingPartnerwrapper(new Teaming_Partner__c (),false);
                          teamingPartnerW.teamingPartner.Vehicle_Partner__c = vehPartnerW.vehiclePartner.id;
                          teamingPartnerW.teamingPartner.Task_Order__c = tOrderId ;
                          teamingPartnerW.flag = vehPartnerW.status;
                          teamingPartnerW.teamingPartner.Status__c = 'New';
                          teamingPartnerwraList.add(teamingPartnerW );                          
                     }
                   }  
             } 
             
             
            if(teamingPartnerwraList.size()>0){
                 for(TeamingPartnerwrapper teamingPartnerW: teamingPartnerwraList){
                     teamingPartnerList.add(teamingPartnerW.teamingPartner);
                 }
             } 
             
            
            try{
                if(teamingPartnerList.size()>0){
                // upsert teamingPartnerList;
                 DMLManager.upsertAsUser(teamingPartnerList);
                 shareTaskOrder(teamingPartnerList);
                }
            }
            catch(exception e){
                 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, e.getMessage()));
            }
        
            PageReference Pg = new PageReference('/'+tOrderId );
            Pg.setRedirect(true);
            return pg; 
        }
        return null;
    }
    
    private void shareTaskOrder(List<Teaming_Partner__c> teamingPartnerList){
        List<Task_Order__share> task_OrderList =  new  List<Task_Order__share>();
    
         Set<Id> contactIdSet = new Set<Id>();
         Set<Id> accountIdSet = new Set<Id>();
         Map<Id,Teaming_Partner__c> teaminPartnerMap = new Map<Id,Teaming_Partner__c>(teamingPartnerList);
         
         //Need to change logic for this contact.
         
         for(Teaming_Partner__c teamingPartner : [Select Id,Vehicle_Partner__r.Point_Of_Contact__c,Vehicle_Partner__r.Partner__c From Teaming_Partner__c where id IN: teaminPartnerMap.keySet()] ){
             //contactIdSet.add(teamingPartner.Vehicle_Partner__r.Point_Of_Contact__c);
             accountIdSet.add(teamingPartner.Vehicle_Partner__r.Partner__c);
         }
         
         List<Task_Order_Contact__c> taskOrderContactList = new List<Task_Order_Contact__c>();
         System.debug('query '+[Select Id,Contact__c,Main_POC__c,Receives_Mass_Emails__c From Contract_Vehicle_Contact__c where contact__r.AccountId IN : accountIdSet and Contract_Vehicle__c =: conVehicleId AND (Main_POC__c =true OR Receives_Mass_Emails__c = true)]);
         for(Contract_Vehicle_Contact__c contractVehicleContract : [Select Id,Contact__c,Main_POC__c,Receives_Mass_Emails__c From Contract_Vehicle_Contact__c where contact__r.AccountId IN : accountIdSet and Contract_Vehicle__c =: conVehicleId AND (Main_POC__c =true OR Receives_Mass_Emails__c = true) AND Contact__c NOT IN (Select Contact__c From Task_Order_Contact__c where Task_Order__c =: tOrderId)]){         
             contactIdSet.add(contractVehicleContract.Contact__c );
              
             Task_Order_Contact__c taskOrderContact = new Task_Order_Contact__c();
             taskOrderContact.Contact__c = contractVehicleContract.Contact__c;
             taskOrderContact.Task_Order__c = tOrderId ;
             taskOrderContact.Main_POC__c = contractVehicleContract.Main_POC__C;
             taskOrderContact.Receives_Mass_Emails__c = contractVehicleContract.Receives_Mass_Emails__c ;
             taskOrderContactList.add(taskOrderContact);
             System.debug(' taskOrderContact '+taskOrderContact);
         }
         System.debug('taskOrderContactList '+taskOrderContactList);
         if(!taskOrderContactList.isEmpty()){            
             DMLManager.insertAsUser(taskOrderContactList);             
         }
         
         List<User> userList = [Select Id From User where contactId IN : contactIdSet];
         for(User usr :userList ){
             Task_Order__share toshare= new  Task_Order__share();
             toshare.ParentId = tOrderId  ;             
             toshare.UserOrGroupId = usr.id;             
             toshare.Accesslevel='Read';                                      
             task_OrderList.add(toshare);
         }
         
         if(!task_OrderList.isEmpty()){
            // insert task_OrderList;
             DMLManager.insertAsUser(task_OrderList);
         }
    }
    
     public pageReference cancel(){
          return new Pagereference('/'+tOrderId );
     }
   
   /********************************** Wrapper class ***************************************/
   public class TeamingPartnerwrapper {
       public Teaming_Partner__c teamingPartner{get;set;}
       public boolean flag {get;set;} 
       
        public TeamingPartnerwrapper(Teaming_Partner__c teamingPartner ,boolean status){
           this.teamingPartner =teamingPartner;
           this.flag = flag ;
          
           
       } 
   } 
   
   public class VehiclePartnerwrapper {
       public Vehicle_Partner__c vehiclePartner{get;set;}
       public boolean status {get;set;}
      
       
     /*  public VehiclePartnerwrapper (Vehicle_Partner__c vehiclePartner,boolean status){
           this.vehiclePartner=vehiclePartner;
           this.status = status;
           
       } */
   }
  /********************************************************************************************/
       
}