@isTest
public with sharing class TM_CommunityHomePageControllerTest{
    public static TestMethod void TestTM_CommunityHomePageController(){
        List<Profile> profileList = [SELECT Id, UserLicense.Name  FROM Profile where (UserLicense.Name = 'Partner' OR UserLicense.Name= 'Customer') Limit 1];
        if(profileList != null && profileList.size()>0){
        
            Account acc = TestDataGenerator.createAccount('TaskOrderCapture');
            insert acc;
            
            List<Contact> conList = new List<Contact>();
            Contact con = TestDataGenerator.createContact('Avanti',acc);     
            conList.add(con);
            Contact con1 = TestDataGenerator.createContact('avan', acc);
            conList.add(con1);
            insert conList;
            
            /*List<User> userList = new List<User>();
            User usr = TestDataGenerator.createUser(con,TRUE,'test1', 'atest1','test1@gmail.com','taskordercapture@demo.com','atest1',profileList[0], 'America/Los_Angeles','en_US','UTF-8','en_US');
            userList.add(usr);
            User usr1 = TestDataGenerator.createUser(con1,TRUE,'test2', 'atest2','test2@gmail.com','taskordercapture123@demo.com','atest2',profileList[0], 'America/Los_Angeles','en_US','UTF-8','en_US');
            userList.add(usr1); 
            insert userList;*/
            
           
            
            Contract_Vehicle__c contractVehicle = TestDataGenerator.createContractVehicle('TMconVehicle',acc);
            insert contractVehicle;
            
            Task_Order__c taskOrder = TestDataGenerator.createTaskOrder('TMtaskOrder', acc, contractVehicle);
            insert taskOrder;
            
            Vehicle_Partner__c vehiclePartner = TestDataGenerator.createVehiclePartner(acc,contractVehicle );
            insert vehiclePartner;
            
            Teaming_Partner__c teamPartner = TestDataGenerator.createTeamingPartner(vehiclePartner , taskOrder);
            insert teamPartner;
            
            Task tsk = TestDataGenerator.createTask('testSubject', system.today(), 'Not Started', 'Normal');
            insert tsk;
          
            Task_Order_Questionnaire__c  taskOrderQue  = TestDataGenerator.createTaskOrderQue ('https://idiqcapture-developer-edition.na31.force.com/Customer', 'https://idiqcapture-developer-edition.na31.force.com/partner');           
            insert taskOrderQue  ;
            
            
            
            Test.StartTest();
            TM_CommunityHomePageController communityHomePageController = new TM_CommunityHomePageController();
            communityHomePageController.init();
           
            system.assertNotEquals(communityHomePageController.taskOrderWraList.size(), 0);        
            Test.StopTest();
    }
  }
  
      
}