@RestResource(urlMapping='/EGOSRecordConnectPost/*')
global with sharing class EGOSRecordConnectPost {
    @HttpPost
    global static String doPost(){
        String urls = '';
        try{            
            RestRequest req = RestContext.request;
            String htmlBody = req.requestBody.toString();
            Map<String,String> headerMap = req.headers;
            String taskOrderNumber = headerMap.get('taskOrderNumber'); 
            System.debug('raw taskOrderNumber = '+taskOrderNumber);
            if(taskOrderNumber == null || taskOrderNumber =='')
                return urls;
            String releaseDate = headerMap.get('releaseDate');    
            System.debug('releaseDate = '+releaseDate);
            Integer timezone;
            try{
                timezone = Integer.valueOf(headerMap.get('timezone'));
            }catch(Exception e){}
            
            String responseStatus = headerMap.get('responseStatus');
            System.debug('responseStatus ='+responseStatus);
            String status = headerMap.get('status');
            System.debug('status ='+status);
           
            System.debug('Hi This is Body = '+htmlBody);                        
            if(responseStatus == 'No Bid' || status == 'Cancelled'){
                System.debug('if');
                List<Task_Order__c> taskOrderList = getExistingTaskOrder(taskOrderNumber);
                if(!taskOrderList.isEmpty()){
                    urls = EGOSRecordParser.parse(htmlBody,taskOrderNumber.remove('"').trim(),timezone,releaseDate.trim(),responseStatus,status);
                    List<RecordType> recordTypeList  = [SELECT Id,Name FROM RecordType WHERE DeveloperName='Contract_Mod' AND sObjectType='TM_TOMA__Contract_Mods__c'];                                
                    if(recordTypeList != null && !recordTypeList.isEmpty()){
                        if(status == 'Cancelled'){
                            try{
                                Contract_Mods__c contractMod = new Contract_Mods__c();
                                contractMod.TM_TOMA__TaskOrder__c = taskOrderList[0].id;
                                contractMod.TM_TOMA__Modification_Name__c = 'Cancelled';
                                contractMod.TM_TOMA__Modification__c = 'Cancelled';
                                contractMod.RecordTypeId = recordTypeList[0].Id;							
                                insert contractMod;
                            }catch(Exception e){
                                
                            }
                        }
                    }
                }
            }else{
                System.debug('else');
                urls = EGOSRecordParser.parse(htmlBody,taskOrderNumber.remove('"').trim(),timezone,releaseDate.trim(),responseStatus,status);
            }
            
            System.debug('urls = '+urls); 
            System.debug('urls = '+urls);            
        }
        catch(Exception e){
            System.debug(''+e);
        }
        return urls;
    } 
    
    private static List<Task_Order__c> getExistingTaskOrder(String taskOrderNumber){
        //where condition, coz we dont want to process TO after the IS Cancelled is checked AND/or stage is "No Bid" 
        List<Task_Order__c> taskOrderList = [SELECT Id FROM Task_Order__c WHERE Name =:taskOrderNumber AND Is_Cancelled__c = false AND Stage__c != 'No Bid' LIMIT 1];               
        return taskOrderList;
    }
}