@RestResource(urlMapping='/GSAConnectPOST/*')
global with sharing class GSAConnectPOST{
    static String statusTemp;
    
    @HttpPost
    global static String doPost11()
    {
        statusTemp = '';
        String urls = '';
        String result = '';
        try{
            System.debug('Inside doPost11');
            RestRequest req = RestContext.request;
            String strBody = req.requestBody.toString();
            //System.debug('Hi This is Body = '+strBody);                        
            GSAHTMLParser parser = new GSAHTMLParser();
            result = Parser.parse(strBody);
            System.debug('**result =' +result);
            //result = parser.upsertGSADataInTaskOrder(strBody);
            
        }
        catch(Exception ee){}
        //String retValue = 'rfqId=RFQ1071729##contractNumber=GS-10F-0101S'; 
        
        return result;
    }
    
    /*private void createGSAData(String htmlBody){
        
    }*/
    
}