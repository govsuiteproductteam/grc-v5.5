public class RTEPRecordModel {
    public static final String RTEP_NUMBER_LABEL = 'RTEP Number:';
    public String rtepNumber;
    
    public static final String RTEP_TITLE_LABEL = 'RTEP Title:';
    public String rtepTitle;
    
    public static final String RTEP_STATUS_LABEL = 'RTEP Status:';
    public String rtepStatus;
    
    public static final String TEP_DUE_DATE_LABEL = 'TEP Due Date:';
    public static final String MARKET_RESEARCH_DUE_DATE_LABEL = 'Market Research Due Date:';
    public String tepDueDate;
    
    public static final String RTEP_DESCRIPTION_LABEL = 'RTEP Description:';
    public String rtepDescription;
    
    public static final String RECURINNG_ACTIVITY_LABEL = 'Requiring Activity:';
    public String requringActivity;
    
    public static final String ACQUISITION_TRACKER_LABEL = 'Acquisition Tracker ID:';
    public String acquisitionTrackerID;
    
    public static final String COMPETITIVE_TYPE_LABEL = 'Contract Type:';
    public static String competitiveType;
    
    public static final String COMPETITIVE_PROCEDURE_LABEL = 'Competitive Procedure:';
    public String competitiveProcedure;
    
    public static final String CO_NAME_LABEL = 'CO Name:';
    public String coName;
    
    public static final String CS_NAME_LABEL = 'CS Name:';
    public String csName;
    
    public static final String UPLOADED_DOCUMENT_LABEL = 'Uploaded Documents:';
    public String uploadedDocument;
    
    public Task_Order__c taskOrderMapping(){
        Task_Order__c  taskOrder = new Task_Order__c ();
        if(rtepNumber  != null){
            taskOrder.Name = rtepNumber; 
        }
        if(rtepTitle   != null){
            taskOrder.Task_Order_Title__c = rtepTitle;
        }
        if(rtepStatus != null){
            taskOrder.RTEP_Status__c =  rtepStatus ;
        }
        if(tepDueDate  != null){
            DateTimeHelper dtTime = new DateTimeHelper();
            DateTime dueDateTime = dtTime.getDateTime(tepDueDate);
            if(dueDateTime != null){
                taskOrder.Due_Date__c =  dueDateTime.date() ; 
            }
        }
        if(rtepDescription != null){
            taskOrder.Description__c =  rtepDescription;
        }
        if(requringActivity != null){
            taskOrder.Requiring_Activity__c =  requringActivity;
        }
        if(acquisitionTrackerID != null){
            taskOrder.Acquisition_Tracker_ID__c =  acquisitionTrackerID ;
        }
        if(competitiveType != null){
            taskOrder.Contract_Type__c =  competitiveType;
        }
        if(competitiveProcedure != null){
            taskOrder.Competitive_Procedure__c =   competitiveProcedure;
        }
        if(coName != null){
            taskOrder.Contracting_Officer__c = insertContact(coName);
        }
        if(csName != null){
            taskOrder.Contract_Specialist__c = insertContact(csName);
        }
        // uploadedDocument = 
        return taskOrder;
    }
    
   /* private static string parseContactName(String conName){
        Contact Details is coming like given below Example
          Ginty, Matthew
          Matthew.Ginty@va.gov
          732-440-9700
                
        List<String> resultConName = conName.trim().split('\n');
        if(resultConName.size()>0){
            if(resultConName[0].contains(',')){
                List<String> splitname = resultConName[0].trim().split(',');
                if(splitname.size() == 2){
                    String finalName = splitname[1] + splitname[0];
                    return finalName;
                }
            }
            
           if(resultConName[1] !=  null){
                String email = resultConName[1].trim();
            }
            
            if(resultConName[2] != null){
                String mobNum = resultConName[2].trim();
            } 
        }
        return '';
    } */
    
    private static Id insertContact(String conDetail){ 
        List<String> resultConName = conDetail.trim().split('\n');
        String lastName;
        String firstName;      
        String mobNum ;
        String email;
        if(resultConName.size()>0){
            if(resultConName[0].contains(',')){
                List<String> splitname = resultConName[0].trim().split(',');
                if(splitname.size() == 2){
                    firstName = splitname[1];
                    lastName = splitname[0];
                }
            }else{
               lastName = resultConName[0];
            }
            if(resultConName[1] !=  null){
                email = resultConName[1].trim();
            } 
            
            if(resultConName[2] != null){
                 mobNum = resultConName[2].trim();
            } 
                
            String emailStr = getContactEmail(email).trim();
            system.debug('email=='+ email );
            List<Contact> conList = [SELECT id , name FROM Contact where email=: emailStr];
            if(!conList.isEmpty()){
                return conList[0].id;
            }else{
                Contact con = new Contact();
                
                if(firstName != null){
                        con.firstName = firstName;
                }
                con.LastName = lastName ;  
                con.email = emailStr;
                con.phone = mobNum ;
                
                DMLManager.insertAsUser(con);
                //insert con;     
                return con.id;
            }   
          }
          return null; 
    }
    
    
     public static String getContactEmail(String parseString){
        String regex = '([a-zA-Z0-9_\\-\\.]+)@(((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3}))';       
        Pattern myPattern = Pattern.compile(regex );    
        Matcher myMatcher = myPattern.matcher(parseString);
        if(myMatcher.find()){
             return myMatcher.group();
        }
        return null;
    }
    
    
}