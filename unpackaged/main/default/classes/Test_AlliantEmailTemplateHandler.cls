@isTest
public with sharing class Test_AlliantEmailTemplateHandler {
    public Static TestMethod void alliantEmailTemplateHandlerTest (){
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Contact con = TestDataGenerator.createContact('avan', acc );
        insert con;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('T-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'alliant@311ht5x423bxlmqumtqebb3nskbyjthbv9sjkmd1tlsxvkfk32.61-zhsweao.na34.apex.salesforce.com';
        
        insert conVehi;
        
      //for new Task Order Creation  
        Messaging.InboundEmail email  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        email.plainTextBody = 'Hello All,'+
            'You are invited to respond the attached Task Order Request (TOR) ID11150045 for Joint Service Provider (JSP) Support Services. Question shall be submitted by email to Frederick.Thomas@gsa.gov and Justin.Breadshaw@gsa.gov by Tuesday, February 16th 2016 at 12:00 PM EST. Responses are due by Wednesday, March 9th 2016 at 1:00 PM EST.\n'+
            'Please see attached documents.\n'+
            'Fred Thomas\n'+
            'Contracting Officer\n'+
            'Assisted Acquisition Services\n'+
            'FAS/National Capital Region\n'+
            'Frederick.thomas@gsa.gov\n'+
            'NCR Acquisition Customer Survey\n';
        email.fromAddress ='test@test.com';
        email.fromName = 'ABC XYZ';
        email.subject = 'RE: Task Order Request ID11150045 for JSP Support Services';
        email.toaddresses = new List<String>();
        email.toaddresses.add('alliant@311ht5x423bxlmqumtqebb3nskbyjthbv9sjkmd1tlsxvkfk32.61-zhsweao.na34.apex.salesforce.com');
        
        //For Updating existing task order with existing Contact
        Messaging.InboundEmail email1  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env1 = new Messaging.InboundEnvelope();
        email1.plainTextBody = 'Hello All,'+
            'You are invited to respond the attached Task Order Request (TOR) ID11150045 for Joint Service Provider (JSP) Support Services. Question shall be submitted by email to Frederick.Thomas@gsa.gov and Justin.Breadshaw@gsa.gov by Tuesday, February 15th 2016 at 12:00 PM EST. Responses are due by Wednesday, March 9th 2016 at 1:00 PM EST.\n'+
            'Please see attached documents.\n'+
            'Fred Thomas\n'+
            'Contracting Specialist\n'+
            'Assisted Acquisition Services\n'+
            'FAS/National Capital Region\n'+
            'Frederick.thomas@gsa.gov\n'+
            'NCR Acquisition Customer Survey\n';
        email1.fromAddress ='test@test.com';
        email1.fromName = 'ABC XYZ';
        email1.subject = 'Task Order Request ID11150045 for JSP Support Services';
        email1.toaddresses = new List<String>();
        email1.toaddresses.add('alliant@311ht5x423bxlmqumtqebb3nskbyjthbv9sjkmd1tlsxvkfk32.61-zhsweao.na34.apex.salesforce.com');
        
         //For Updating existing task order with new Contact
        Messaging.InboundEmail email2  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env2 = new Messaging.InboundEnvelope();
        email2.plainTextBody = 'Hello All,'+
            'You are invited to respond the attached Task Order Request (TOR) ID11150045 for Joint Service Provider (JSP) Support Services. Question shall be submitted by email to Frederick.Thomas@gsa.gov and Justin.Breadshaw@gsa.gov by Tuesday, February 15th 2016 at 12:00 PM EST. Responses are due by Wednesday, March 9th 2016 at 1:00 PM EST.\n'+
            'Please see attached documents.\n'+
            'Fred Thomas\n'+
            'Contracting Specialist\n'+
            'Assisted Acquisition Services\n'+
            'FAS/National Capital Region\n'+
            'Frederick.thomas@gsa.gov\n'+
            'NCR Acquisition Customer Survey\n';
        email2.fromAddress ='test1@test.com';
        email2.fromName = 'DEF XYZ';
        email2.subject = 'Task Order Request ID11150045 for JSP Support Services';
        email2.toaddresses = new List<String>();
        email2.toaddresses.add('alliant@311ht5x423bxlmqumtqebb3nskbyjthbv9sjkmd1tlsxvkfk32.61-zhsweao.na34.apex.salesforce.com');  
        
        //for Blank
               
        
        
        AlliantEmailTemplateHandler alliantHandler = new AlliantEmailTemplateHandler();
        
        Test.startTest();
        Messaging.InboundEmailResult result = alliantHandler.handleInboundEmail(email, env);
        Messaging.InboundEmailResult result1 = alliantHandler.handleInboundEmail(email1, env1);
        Messaging.InboundEmailResult result2 = alliantHandler.handleInboundEmail(email2, env2);
        // Messaging.InboundEmailResult result3 = alliantHandler.handleInboundEmail(email3, env3);
        Test.stopTest();
        
        List<Task_Order__c> tOrderList = [SELECT Id FROM Task_Order__c];
        System.assertEquals(tOrderList.size(), 1);
    }
    
    
    public Static TestMethod void alliantEmailTemplateHandlerTest2()
    {
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Contact con = TestDataGenerator.createContact('avan', acc );
        insert con;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('T-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'alliant@311ht5x423bxlmqumtqebb3nskbyjthbv9sjkmd1tlsxvkfk32.61-zhsweao.na34.apex.salesforce.com';
        
        insert conVehi;
        Messaging.InboundEmail email3  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env3 = new Messaging.InboundEnvelope();
        email3.plainTextBody = '';
         email3.fromAddress ='test1@test.com';
        email3.fromName = 'DEF XYZ';
        email3.subject = ' ';
        email3.toaddresses = new List<String>();
        email3.toaddresses.add('alliant@311ht5x423bxlmqumtqebb3nskbyjthbv9sjkmd1tlsxvkfk32.61-zhsweao.na34.apex.salesforce.com'); 
        
        AlliantEmailTemplateHandler alliantHandler = new AlliantEmailTemplateHandler();
        Test.startTest();
       
        try
        {
            Messaging.InboundEmailResult result3 = alliantHandler.handleInboundEmail(email3, env3);
        }
        catch(Exception e)
        {
            Boolean expectedExceptionThrown = e.getMessage().contains('Task Order Number not found with the email');
            System.AssertEquals(expectedExceptionThrown, true);
        }
        Test.stopTest();
        
    }
    
    
}