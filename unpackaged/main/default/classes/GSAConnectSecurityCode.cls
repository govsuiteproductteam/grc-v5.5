@RestResource(urlMapping='/GSAConnectSecurityCode/*')
global with sharing class GSAConnectSecurityCode {
	@HttpPost
    global static String doPost()
    {
        List<Document> documentList = [SELECT Id,Body FROM Document WHERE DeveloperName='EbuySecurityCode'];
        return documentList[0].body.toString();
    }
}