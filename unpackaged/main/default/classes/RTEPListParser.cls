public class RTEPListParser {
    private static String urls='';
    Public static String parse(String htmlBody){       
        //StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'RTEPList' LIMIT 1];
        //String htmlBody = sr.Body.toString();
        
        List<RTEPModel> rtepModelList = new List<RTEPModel>();
        String data = htmlBody.substringBetween('</tfoot>', '</tbody>');
        //System.debug(''+data);
        List<String> rtepRecordList = data.split('</tr>');
        //System.debug('Size = '+rtepRecordList.size());
        for(String rtepHtml : rtepRecordList){  
            //System.debug('rtepHtml = '+rtepHtml);
            List<String> recordDetail = rtepHtml.split('</td>');            
            //System.debug('recordDetail size  = '+recordDetail.size());
            if(recordDetail!=null && recordDetail.size() > 5 ){
                String rtepNumber = recordDetail.get(0).stripHtmlTags().trim();
                System.debug('recordDetail.get(0) = '+recordDetail.get(0));
                appendUrl(recordDetail.get(0));
                String rtepTitle = recordDetail.get(1).stripHtmlTags().trim();
                String marketResearchDueDate = recordDetail.get(2).stripHtmlTags().trim();
                //System.debug('marketResearchDueDate = '+marketResearchDueDate +'\t length = '+marketResearchDueDate.trim().length());
                String rtepStatus = recordDetail.get(3).stripHtmlTags().trim();
                String tepDueDate = recordDetail.get(4).stripHtmlTags().trim();
                RTEPModel model = new RTEPModel(rtepNumber,rtepTitle,marketResearchDueDate,rtepStatus,tepDueDate);
                rtepModelList.add(model);
            }
        }
        //System.debug(''+data);
        System.debug('urls = '+urls);
        return urls;
    }
    
    private static void appendUrl(String data){
        if(data.contains('<a')){
            //Boolean cond1 = data.contains('T4NG-0185');
            if(data.contains('href=') ){
                if(urls != ''){
                    if(data.contains('<a href=\''))
                        urls += 'SPLITURL'+data.substringBetween('href=\'', '\'');
                    if(data.contains('<a href="'))
                        urls += 'SPLITURL'+data.substringBetween('href="', '"');
                }else{
                    if(data.contains('<a href=\''))
                        urls = data.substringBetween('<a href=\'', '\'');
                    if(data.contains('<a href="'))
                        urls = data.substringBetween('<a href="', '"');
                }
                
            }
        }
    }
}