@isTest
public class EGOSAttachementTest {

    public static testMethod void testEGOSAttachementCase1(){
        Account acc = new Account(Name='Acc-123');
        insert acc;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('CV-123', acc);
        conVehi.SINs__c = 'CIO-CS';
        insert conVehi;
        Task_Order__c taskOrder = TestDataGenerator.createTaskOrder('SN-39841', acc, conVehi);
        insert taskOrder;
        Attachment attachmentObj = TestDataGenerator.createAttachment(taskOrder.Id);
        Test.startTest();        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        req.requestBody = attachmentObj.Body;
        req.httpMethod = 'POST';
        req.addHeader('taskOrderId', taskOrder.Id);
        req.addHeader('fileName', 'Test Attachment for Parent');
        RestContext.request = req;
        RestContext.response = res;
        EGOSAttachement.doPost();
        Test.stopTest();
        List<Attachment> attachList = [Select Id From Attachment where parentId =: taskOrder.Id];
        System.assertEquals(1, attachList.size());
    }
    
    public static testMethod void testEGOSAttachementCase2(){
        Account acc = new Account(Name='Acc-123');
        insert acc;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('CV-123', acc);
        conVehi.SINs__c = 'CIO-CS';
        insert conVehi;
        Task_Order__c taskOrder = TestDataGenerator.createTaskOrder('SN-39841', acc, conVehi);
        insert taskOrder;
        Attachment attachmentObj1 = TestDataGenerator.createAttachment(taskOrder.Id);
        insert attachmentObj1;
        Attachment attachmentObj2 = TestDataGenerator.createAttachment(taskOrder.Id);
        Test.startTest();        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        req.requestBody = attachmentObj2.Body;
        req.httpMethod = 'POST';
        req.addHeader('taskOrderId', taskOrder.Id);
        req.addHeader('fileName', 'Test Attachment for Parent.pdf');
        RestContext.request = req;
        RestContext.response = res;
        EGOSAttachement.doPost();
        Test.stopTest();
        List<Attachment> attachList = [Select Id From Attachment where parentId =: taskOrder.Id];
        System.assertEquals(1, attachList.size());
    }
    
    public static testMethod void testEGOSAttachementCase3(){
        Account acc = new Account(Name='Acc-123');
        insert acc;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('CV-123', acc);
        conVehi.SINs__c = 'CIO-CS';
        insert conVehi;
        Task_Order__c taskOrder = TestDataGenerator.createTaskOrder('SN-39841', acc, conVehi);
        insert taskOrder;
        Test.startTest();        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        req.requestBody = Blob.valueof('');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        EGOSAttachement.doPost();
        Test.stopTest();
        List<Attachment> attachList = [Select Id From Attachment where parentId =: taskOrder.Id];
        System.assertEquals(0, attachList.size());
    }
    
}