@isTest
global class MockHttpResponseGenerator implements HttpCalloutMock {
    private String testResponse;
    private Integer testStatusCode;
    
    public MockHttpResponseGenerator(String testResponse, Integer testStatusCode){
        this.testResponse = testResponse;
        this.testStatusCode = testStatusCode;
    }
    
    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        res.setBody(testResponse);
        res.setStatusCode(testStatusCode);
        return res;
    }
}