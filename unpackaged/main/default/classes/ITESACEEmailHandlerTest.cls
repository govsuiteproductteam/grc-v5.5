@isTest
public class ITESACEEmailHandlerTest {

    public static testmethod void testCase1(){
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Contact con = TestDataGenerator.createContact('TestCon', acc );
        insert con;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('T-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'itesace@2jsbj75qdzvapqv37seip6je473909llnk5fecjylquk0vn7qx.61-zhsweao.na34.apex.salesforce.com';
        
        insert conVehi;
        
         //For new Task Order Creation
        Messaging.InboundEmail email  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        List<StaticResource> srList = [SELECT Id,Body FROM StaticResource WHERE Name='ITESACENew'];
        email.plainTextBody = srList.get(0).Body.toString();
        //System.debug('Body = '+email.plainTextBody);
        email.fromAddress ='test@test.com';
        email.fromName = 'ABC XYZ';
        email.subject = '[HDWE-18-1219]: OrderTrak Request Approved - Please Process Attached Order';
        email.toaddresses = new List<String>();
        email.toaddresses.add('itesace@2jsbj75qdzvapqv37seip6je473909llnk5fecjylquk0vn7qx.61-zhsweao.na34.apex.salesforce.com');
        
        
        //To update existing task order 
        Messaging.InboundEmail email1  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env1 = new Messaging.InboundEnvelope();
        List<StaticResource> srList1 = [SELECT Id,Body FROM StaticResource WHERE Name='ITESACEUpdated'];
        email1.plainTextBody = srList1.get(0).Body.toString();
        //System.debug('Body = '+email1.plainTextBody);
        email1.fromAddress ='test@test.com';
        email1.fromName = 'ABC XYZ';
        email1.subject = 'Fwd:[HDWE-18-1219]: OrderTrak Request Approved - Please Process Attached Order';
        email1.toaddresses = new List<String>();
        email1.toaddresses.add('itesace@2jsbj75qdzvapqv37seip6je473909llnk5fecjylquk0vn7qx.61-zhsweao.na34.apex.salesforce.com');
        
        
        ITESACEEmailHandler handler = new ITESACEEmailHandler();
        Test.startTest();
        Messaging.InboundEmailResult result = handler.handleInboundEmail(email, env);
        List<Task_Order__c> taskOrderList = [SELECT Id FROM Task_Order__c];
        Messaging.InboundEmailResult result1 = handler.handleInboundEmail(email1, env1);
        List<Contract_Mods__c> contractModList = [SELECT Id FROM Contract_Mods__c];
        Test.stopTest();
        
        System.assertEquals(1, taskOrderList.size());
        System.assertEquals(1, contractModList.size());
    }
}