({
    doInitHelper  : function(component, event, helper) {
        var stageValue = component.get("v.recordId"); 
        var fieldMap = component.get("c.getFields");
        fieldMap.setParams({            
            "recoredTyeId" :  stageValue    
        });
        fieldMap.setCallback(this, function(response) {
            var state = response.getState();
            if(state ==="SUCCESS")
            {
                if(response.getReturnValue()!=null){
                    //component.set("v.OppAtaGlanceField",response.getReturnValue());
                    var glanceWrap=response.getReturnValue();
                    //console.log(glanceWrap["atglanceList"]+"glanceWrap > "+JSON.stringify(glanceWrap));
                    var glanceList=glanceWrap["atglanceList"];
                    //  console.log("response.getReturnValue() > "+glanceList.length);
                    if(glanceList!=null && glanceList.length>0){
                        component.set("v.OppAtaGlanceWrap",glanceWrap);
                        component.set("v.check",true); 
                        
                        //    console.log("ve = "+component.get("v.OppAtaGlanceWrap"));
                    }else{
                        var headerId=component.find("headerId");
                        
                        $A.util.removeClass(headerId,"slds-page-header");
                        $A.util.addClass(headerId,"headerCSS");
                    }
                    
                    /*  for(var i  in pqr){
                        console.log("Color__c >"+pqr[i]["Color__c"]+"ColSpan__c "+pqr[i]["ColSpan__c"]+" Document_Id__c"+pqr[i]["Document_Id__c"]+"FieldApiName__c"+pqr[i]["FieldApiName__c"]+"FieldLabel__c"+pqr[i]["FieldLabel__c"]+"Is_Component__c"+pqr[i]["Is_Component__c"]+"Order__c"+pqr[i]["Order__c"]);
                    }*/
                }
                
                /* var valueMap=response.getReturnValue();  
                for(var i in valueMap) {
                    if (valueMap.hasOwnProperty(i)) {
                        if(i==="tab"){
                            var vc=valueMap[i].split(",");
                            component.set("v.TabFields",vc);
                            // component.get("v.TabFields").push(vc);
                        }else if(i==="reType"){
                            var vc=valueMap[i].split(",");
                            component.set("v.RecorTypeFields",vc);
                            //component.get("v.RecorTypeFields").push(vc);
                        }else if(i==="atglace"){
                            console.log("atglance fields > "+valueMap[i]);
                            var vc=valueMap[i].split(",");
                            component.set("v.AtglanceFieldstring",valueMap[i]);
                            component.set("v.AtglanceFields",vc);
                            if(component.get("v.AtglanceFields") !='undefined' && component.get("v.AtglanceFields") !=null){
                                component.set("v.check",true);
                            }
                            component.get("v.AtglanceFields").push(vc);
                        }
                    }
                }*/
                // component.find("recordLoader").reloadRecord();
            }else{
                console.log("Error");
            }
        });
        $A.enqueueAction(fieldMap);
    }
})