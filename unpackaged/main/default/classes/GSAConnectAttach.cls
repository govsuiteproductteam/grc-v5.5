@RestResource(urlMapping='/GSAConnectAttach/*')
global with sharing class GSAConnectAttach{
    static String statusTemp;
    
    @HttpPost
    global static String doPost()
    {
        statusTemp = '';
        String urls = '';
        String result = '';
        try{
            System.debug('Inside doPost11');
            RestRequest req = RestContext.request;
            
            Blob inputBody = req.requestBody;
           // String EncodingUtil.base64Encode(inputBody );

            Map<String,String> headerMap = req.headers;
            
            System.debug('Hi This is Body = '+inputBody );    
            String fileName = headerMap.get('fileName');    
            String taskOrderId = headerMap.get('taskOrderId');        
            System.debug('fileName  =  '+fileName );
            System.debug('taskOrderId =  '+taskOrderId );
            if(fileName != null && taskOrderId != null){
                result = GSAAttacher.attachDocTO(inputBody ,fileName ,taskOrderId ); 
            } 
            else{
                result = 'Empty Data';
            }         
                        
            
        }
        catch(Exception ee){
            system.debug('Exception = '+ee);
            result = 'Failed';
        }
        //String retValue = 'rfqId=RFQ1071729##contractNumber=GS-10F-0101S'; 
        
        return result;
    }
    
    private void createGSAData(String htmlBody){
        
    }
    
}