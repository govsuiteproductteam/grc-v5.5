@IsTest
public class Test_ContrctNewDynamicFieldsController {
    Public static testMethod void testContrctEditDynamicFieldsController(){
        Test.startTest();
        List<RecordType> rtypes = [Select Name, Id From RecordType where sObjectType='TM_TOMA__Contract_Vehicle__c' and isActive=true];
        string recId='';
        if(rtypes.size()>0)
        {
            recId=rtypes[0].Id;
        }
        
        CLM_Tab__c tabSetting = TestDataGenerator.createCLMTab(recId);
        tabSetting.TabName__c='Internal Organisation';
        insert tabSetting;  
        CLM_Tab__c tabSetting1 = TestDataGenerator.createCLMTab(recId);
        tabSetting1.Order__c = 2;
        tabSetting1.TabName__c='Contract Description';
        insert tabSetting1;   
        CLM_Section__c sectionSetting = TestDataGenerator.createCLMSection(recId);
        insert sectionSetting;
        CLM_Section__c sectionSetting1 = TestDataGenerator.createCLMSection(recId);
        sectionSetting1.Order__c = 20;
        sectionSetting1.Is_Lightning_Component__c= true;
        sectionSetting1.Inline_Api_Name_Component_Api__c='TestComponent';
        insert sectionSetting1;
        CLM_Section__c sectionSetting2 = TestDataGenerator.createCLMSection(recId);
        sectionSetting2.Order__c = 30;
        sectionSetting2.Is_InlineVF_Page__c= true;
        sectionSetting2.Inline_Api_Name_Component_Api__c='TestVFPage';
        insert sectionSetting2;
        CLM_FIeld__c fieldSetting = TestDataGenerator.createCLMField(recId);
        fieldSetting.ViewFieldApi__c='LastModifiedById';
        fieldSetting.Order__c = 1001;
        insert fieldSetting;
        CLM_FIeld__c fieldSetting1 = TestDataGenerator.createCLMField(recId);
        fieldSetting1.FieldApiName__c = 'OwnerId';
        fieldSetting1.ViewFieldApi__c='OwnerId';
        fieldSetting1.Order__c = 1002;
        insert fieldSetting1;
        CLM_FIeld__c fieldSetting2 = TestDataGenerator.createCLMField(recId);
        fieldSetting2.FieldApiName__c = 'RecordTypeId';
        fieldSetting2.ViewFieldApi__c='RecordTypeId';
        fieldSetting2.Order__c = 1003;
        insert fieldSetting2;
        
        Account acc = TestDataGenerator.createAccount('testAccount');
        insert acc;
        
        TM_TOMA__Contract_Vehicle__c conVeh = TestDataGenerator.createContractVehicle('Test contract vehicle', acc);
        conVeh.recordTypeId = recId;
        conVeh.TM_TOMA__Contract_Type__c= TestDataGenerator.getPicklistValue('TM_TOMA__Contract_Vehicle__c','TM_TOMA__Contract_Type__c');
        insert conVeh;
        
        String subConid = [Select id From TM_TOMA__Contract_Vehicle__c Where Id=:conVeh.Id].Id;
        
        ContrctNewDynamicFieldsController ctrl = new ContrctNewDynamicFieldsController();
        List<String> fields = ContrctNewDynamicFieldsController.getAllFieldsApi('TM_TOMA__Contract_Vehicle__c',subConid);
        ContrctNewDynamicFieldsController.createContract(recId);
        ContrctNewDynamicFieldsController.isLightningPage();
        ContrctNewDynamicFieldsController.getUserAccesibility();
        ContrctNewDynamicFieldsController.getDynamicCompoNames(recId);
        ContrctNewDynamicFieldsController.getDefaultReocrdTypeId();
        ContrctNewDynamicFieldsController.getMyContract(subConid);
        ContrctNewDynamicFieldsController.saveModifications(conVeh);
        Contract_Vehicle__c contractVechile=ContrctNewDynamicFieldsController.getMyContract(subConid);
        ContrctNewDynamicFieldsController.FieldWrapMap fldWrap =new ContrctNewDynamicFieldsController.FieldWrapMap();
        fldWrap.row = 1;
        fldWrap.fieldWrapList = null;
        System.assert(contractVechile!=null);
        Test.stopTest();
    }
    
    
}