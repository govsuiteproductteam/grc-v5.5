@IsTest
public class Test_TM_ContractVehicleAutoProcessClause {
public static testMethod void testContractVehicleAutoProcessClauses(){
        
        Account account = TestDataGenerator.createAccount('Test Account');
        insert account;
        Contract_Vehicle__c contractVehicle =  TestDataGenerator.createContractVehicle('12345', account);
        insert contractVehicle;
        Contract_Vehicle__c contractVehicle1 =  TestDataGenerator.createContractVehicle('1234567', account);
        insert contractVehicle1;
        Clauses__c clause = TestDataGenerator.getClause('AFFARS 5352.215-9009 Travel (AFMC)');
        insert clause;
        Clauses__c clause1 = TestDataGenerator.getClause('AFARS 5152.204-4007');
        insert clause1;
        Clauses__c clause2 = TestDataGenerator.getClause('ACEARS 52.212-5000 Evaluation of subdivided items');
        insert clause2;
        
        Contract_Clauses__c conClaus = TestDataGenerator.getContractClause(contractVehicle.id,clause.id);
        insert conClaus;
        Contract_Clauses__c conClaus1 = TestDataGenerator.getContractClause(contractVehicle.id,clause1.id);
        insert conClaus1;
        Contract_Clauses__c conClaus2 = TestDataGenerator.getContractClause(contractVehicle1.id,clause2.id);
        insert conClaus2;
        
        
        Contract_Vehicle__c ContractVehicle2 = TestDataGenerator.createContractVehicle('1234567', account);
        ContractVehicle2.Master_Contract_Vehicle__c = contractVehicle.id;
        insert ContractVehicle2;
        Contract_Vehicle__c ContractVehicle3= TestDataGenerator.createContractVehicle('1234568', account);
        ContractVehicle3.Master_Contract_Vehicle__c = ContractVehicle1.id;
        insert ContractVehicle3;
        
        /*List<Id> ConIdList = new List<Id>();
        ConIdList.add(ContractVehicle2.Id);
        ConIdList.add(ContractVehicle3.Id);
        TM_ContractVehicleAutoProcessClauses.copyClausesToContractVehicle(ConIdList);*/
        
        List<Contract_Clauses__c> masterConClausesList =new List<Contract_Clauses__c>();
        masterConClausesList = [Select Id,Contract_Vehicle__c FROM Contract_Clauses__c where Contract_Vehicle__c =: ContractVehicle2.Master_Contract_Vehicle__c];
        List<Contract_Clauses__c> ConClausesList =new List<Contract_Clauses__c>();
        ConClausesList = [Select Id,Contract_Vehicle__c FROM Contract_Clauses__c where Contract_Vehicle__c =: ContractVehicle2.Id];
        System.assertEquals(conClausesList.size(), ConClausesList.size());
        
        conClausesList = [Select Id,Contract_Vehicle__c FROM Contract_Clauses__c where Contract_Vehicle__c =: ContractVehicle3.Master_Contract_Vehicle__c];
        ConClausesList = [Select Id,Contract_Vehicle__c FROM Contract_Clauses__c where Contract_Vehicle__c =: ContractVehicle3.Id];
        System.assertEquals(conClausesList.size(), ConClausesList.size());
    }
}