public class Seaport_EEmailTemplateParser {
    private String emailSubject='';
    private String taskOrderNumber='';
    public Map<String,String> parseMap;
	private Contact con;    
    
    public void parse(Messaging.InboundEmail email){
        emailSubject = email.subject;
        emailSubject = emailSubject.stripHtmlTags();
        System.debug('emailSubject = '+emailSubject);
        String emailBody = email.plainTextBody.remove('*');
        
        parseMap = new Map<String,String>();        
        initMap(emailbody);                
        for(String key : parseMap.keySet()){
            System.debug('Key = '+key+'\tValue = '+parseMap.get(key));
        }
        
        taskOrderNumber = getTrimedEmailSubject(checkValidString(emailSubject, 79));        
    }
    
    private String checkValidString(String str, Integer endLimit){
        if(str!=Null){
            if(str.length() > endLimit){
                String validString = str.substring(0,endLimit);
                return validString;
            }else{
            return str;
            }
        }
        else{
            return Null;
        }
    }

   
    
    public Task_Order__c getTaskOrder(String toAddress){        
        
        List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c =: toAddress];        
        if(!conVehicalList.isEmpty()){
            if(parseMap.containsKey('Name') && parseMap.get('Name') != null && parseMap.get('Name') != ''){
                Task_Order__c taskOrder = new Task_Order__c();
                if(conVehicalList.size()>0){
                    taskOrder.Contract_Vehicle__c = conVehicalList[0].Id;
                }
                
                taskOrder.Name = checkValidString(parseMap.get('Name'),79) ;//Task Order Number
                if(parseMap.containsKey('Description')){
                    taskOrder.Description__c = checkValidString(parseMap.get('Description'),10000);
                }
                
                if(parseMap.containsKey('Start Time')){
                    Date startDate = getDate(parseMap.get('Start Time'));
                    if(startDate != null){
                        taskOrder.Start_Date__c = startdate;
                    }
                }
                if(parseMap.containsKey('Stop Time')){
                    Date StopDate = getDate(parseMap.get('Stop Time'));
                    if(StopDate != null){
                        taskOrder.Stop_Date__c = StopDate;
                    }
                }         
                
                //taskOrder.Stop_Date__c = getDate(parseMap.get('Stop Time'));
                if(parseMap.containsKey('Contract Negotiator')){
                    createContact(parseMap.get('Contract Negotiator'),'Contract Negotiator');
                    insertNewContact();
                    taskOrder.Contract_Negotiator__c = con.Id;
                        
                }
                if(parseMap.containsKey('Contracting Officer')){
                    System.debug('**before'+parseMap.get('Contracting Officer'));
                    createContact(parseMap.get('Contracting Officer'),'Contracting Officer');  
                    insertNewContact();
                    taskOrder.Contracting_Officer__c = con.Id;                        
                    System.debug('taskOrder.Contracting_Officer__c = '+taskOrder.Contracting_Officer__c);
                }    
                taskOrder.Contract_Vehicle_picklist__c = 'SeaPort-e - ENGINEERING TECHNICAL AND SUPPORT SERV';
                if(parseMap.containsKey('Zone')){
                    taskOrder.Zone__c = checkValidString(parseMap.get('Zone'),255);       
                }
                
                if(parseMap.containsKey('Set Aside')){
                    taskOrder.Set_Aside__c = checkValidString(parseMap.get('Set Aside'),255);       
                }
                
                //taskOrder.Email_Reference__c = checkValidString(getTrimedEmailSubject(emailSubject) , 255);        
                return taskOrder;
            }
        }
        else{
            throw new ContractVehicleNotFoundException('Contract Vehicle not found with to email address');
        }
        return null;
    }
    
    public Task_Order__c updateTaskOrder(Task_Order__c taskOrder, String toAddress){        
        List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];        
        if(conVehicalList.size()>0){
            taskOrder.Contract_Vehicle__c = conVehicalList[0].Id;
        }
        taskOrder.Name = checkValidString(parseMap.get('Name'),79) ;//Task Order Number
        System.debug('Description $$$$$$$$$$$$$$$$$$= ');
        if(parseMap.containsKey('Description')){
            System.debug('Description $$$$$$$$= '+parseMap.get('Description'));
            taskOrder.Description__c = checkValidString(parseMap.get('Description'),10000);
        }
        if(parseMap.containsKey('Start Time')){
            Date startDate = getDate(parseMap.get('Start Time'));
            if(startDate != null){
                taskOrder.Start_Date__c = startdate;
            }
        }
        if(parseMap.containsKey('Stop Time')){
            Date StopDate = getDate(parseMap.get('Stop Time'));
            if(StopDate != null){
                taskOrder.Stop_Date__c = StopDate;
            }
        }         
        
        //taskOrder.Stop_Date__c = getDate(parseMap.get('Stop Time'));
        if(parseMap.containsKey('Contract Negotiator')){
            createContact(parseMap.get('Contract Negotiator'),'Contract Negotiator');            
            if(taskOrder.Customer_Agency__c != null){                
                con.AccountId = taskOrder.Customer_Agency__c;                             
                //DMLManager.upsertAsUser(con);
                if(con.Id == null){
                    // if Task Order with Customer Organization is not having a Contact or if a different Contact(i.e. a Contact with different Email Id) is found
                    //DMLManager.insertAsUser(con);
                    insert con;
                }else{
                    // if Task Order with Customer Organization is having a Contact, then update that Contact
                    //DMLManager.updateAsUser(con);
                    update con;
                }
            }else{
                if(con.id ==null)
                    DMLManager.insertAsUser(con);                    
            }      
            taskOrder.Contract_Negotiator__c = con.Id;
                
        }
        if(parseMap.containsKey('Contracting Officer')){
            System.debug('**before'+parseMap.get('Contracting Officer'));
            createContact(parseMap.get('Contracting Officer'),'Contracting Officer');  
            if(taskOrder.Customer_Agency__c != null){
                con.AccountId = taskOrder.Customer_Agency__c;                
                //DMLManager.upsertAsUser(con);
                if(con.Id == null){
                    // if Task Order with Customer Organization is not having a Contact or if a different Contact(i.e. a Contact with different Email Id) is found
                    //DMLManager.insertAsUser(con);
                    insert con;
                }else{
                    // if Task Order with Customer Organization is having a Contact, then update that Contact
                    //DMLManager.updateAsUser(con);
                    update con;
                }
            }else{
                if(con.id ==null)
                    DMLManager.insertAsUser(con);                    
            }                  
            taskOrder.Contracting_Officer__c = con.Id;            
            System.debug('taskOrder.Contracting_Officer__c = '+taskOrder.Contracting_Officer__c);
        }    
        //taskOrder.Contract_Vehicle_picklist__c = 'SeaPort-e - ENGINEERING TECHNICAL AND SUPPORT SERV';
        if(parseMap.containsKey('Zone')){
            taskOrder.Zone__c = checkValidString(parseMap.get('Zone'),255);       
        }
        
        if(parseMap.containsKey('Set Aside')){
            taskOrder.Set_Aside__c = checkValidString(parseMap.get('Set Aside'),255);       
        }
        
        //taskOrder.Email_Reference__c = checkValidString(getTrimedEmailSubject(emailSubject) , 255);        
        return taskOrder;    
    }
    
    private String getTrimedEmailSubject(String subject){
        if(subject.startsWith('RE:')){
            subject = subject.replaceFirst('RE:', '');            
        }
        if(subject.startsWith('Fwd:') ){
            subject = subject.replaceFirst('Fwd:', '');            
        }   
        return subject.trim();
    }
    
    private void initMap(String emailbody){
        List<String> emailBodySplitted = emailbody.split('\n');
        //for(String line : emailBodySplitted){
        for(integer i = 0; i<emailBodySplitted.size();i++){            
            String line = emailBodySplitted.get(i);
            System.debug('Line ='+line);
            if(line.contains(':')){
                String[] splitLine = line.split(':',2);             
                
                if(splitLine[0].remove('*').trim() == 'Contract Negotiator' || splitLine[0].remove('*').trim() =='Contracting Officer'){
                    String nextLine = emailBodySplitted.get(i+1).trim();
                    if(nextLine.length()>0 && !nextLine.contains(':')){
                        String nextToNextLine = emailBodySplitted.get(i+2).trim();
                        if(nextToNextLine.length()>0 && !nextToNextLine.contains(':')){
                            parseMap.put(splitLine[0].remove('*').trim(), splitLine[1].trim()+nextLine+nextToNextLine);
                            continue;                            
                        }
                        parseMap.put(splitLine[0].remove('*').trim(), splitLine[1].trim()+nextLine);
                        continue;
                    }
                }          
               
                if(emailbody.contains('Description:') && emailbody.contains('Start Time:')){
                  String description = emailBody.substringBetween('Description:', 'Start Time:');
                    if(description != null && description != '')
                        parseMap.put('Description', description.normalizeSpace());
                }
                //System.debug('splitLine[1] ='+splitLine[1]);
                parseMap.put(splitLine[0].remove('*').trim(), splitLine[1].trim());
            }
        }
    }
    
    private Date getDate(String dateStr){
        try{
            String[] splitDateRaw = dateStr.split(' ',2);
            return Date.parse(splitDateRaw[0]);
        }catch(Exception e){
            return null;
        }
    }
    
    /*private Id createContact(String contactInfo,String type){
        try{
            System.debug('contactInfo '+contactInfo);
            String[] contactInfoSplit = contactInfo.split('\\('); 
            System.debug('contactInfoSplit 11 = '+contactInfoSplit +'\tsize = '+contactInfoSplit.size());
            String name = '';
            if(contactInfoSplit.size()>0){
                name = contactInfoSplit[0];
            }
            String email='';
            if(contactInfoSplit.size()>1){
                System.debug('contactInfoSplit11 = '+contactInfoSplit[1]);
                if(contactInfoSplit[1].contains(')')){
                    email = contactInfoSplit[1].remove(')');
                    System.debug('**email = '+email);
                }
                
            }
            if(email.contains('<') ){
                System.debug('email 1111 = '+email );
                Integer index1 = email.indexOf('<');
                Integer index2 = email.indexOf('>');
                if(index1 != -1 ){
                    email = email.subString(0,index1);
                    System.debug('email 22 = '+email );
                }
            }
            System.debug('email 11 = '+email );
            List<Contact> conOfficerList = [SELECT id , name FROM Contact Where email =:email];
            System.debug('conOfficerList 11 = '+conOfficerList );
            if(conOfficerList != null && !conOfficerList.isEmpty() ){
                System.debug('if = '+conOfficerList[0].id);
                return conOfficerList[0].id;               
            }else{
                Contact con = new Contact();               
                con.LastName = name;
                con.Email = email;
                con.Is_FedTom_Contact__c = true;
                con.Title = type;
                DMLManager.insertAsUser(con);    
                System.debug('new Contact = '+con.id);
                return con.id;               
            }
        }catch(Exception e){            
            return null;
        }
        return null;
    }*/
    
     private void createContact(String contactInfo,String type){
        try{
            System.debug('contactInfo '+contactInfo);
            String[] contactInfoSplit = contactInfo.split('\\('); 
            System.debug('contactInfoSplit 11 = '+contactInfoSplit +'\tsize = '+contactInfoSplit.size());
            String name = '';
            if(contactInfoSplit.size()>0){
                name = contactInfoSplit[0];
            }
            String email='';
            if(contactInfoSplit.size()>1){
                System.debug('contactInfoSplit11 = '+contactInfoSplit[1]);
                if(contactInfoSplit[1].contains(')')){
                    email = contactInfoSplit[1].remove(')');
                    System.debug('**email = '+email);
                }
                
            }
            if(email.contains('<') ){
                System.debug('email 1111 = '+email );
                Integer index1 = email.indexOf('<');
                Integer index2 = email.indexOf('>');
                if(index1 != -1 ){
                    email = email.subString(0,index1);
                    System.debug('email 22 = '+email );
                }
            }
            System.debug('email 11 = '+email );
            List<Contact> conOfficerList = [SELECT id , name FROM Contact Where email =:email];
            System.debug('conOfficerList 11 = '+conOfficerList );
            if(conOfficerList != null && !conOfficerList.isEmpty() ){
                System.debug('if = '+conOfficerList[0].id);
                con = conOfficerList[0];               
            }else{
                con = new Contact();               
                con.LastName = name;
                con.Email = email;
                con.Is_FedTom_Contact__c = true;
                con.Title = type;
                //DMLManager.insertAsUser(con);    
                //System.debug('new Contact = '+con.id);
                //return con.id;               
            }
        }catch(Exception e){            
            //return null;
        }
        //return null;
    }
    
    private void insertNewContact(){
        try{               
            if(con.Id == null)
                DMLManager.insertAsUser(con);                         
        }catch(Exception e){
            System.debug('Error in contact - '+e.getMessage()+':'+e.getLineNumber());
        }
    }
    
}