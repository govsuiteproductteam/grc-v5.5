public class CLMSectionWrap {
    @AuraEnabled
    public List<CLMFieldWrap> fieldWrapList{get;set;}
    @AuraEnabled
    public CLMComponentWrap ComponentOrPageWrap{get;set;}
    //public List<FieldWrapMap> fieldWrapMap1{get;set;}
    //public Map<Integer,List<CLMFieldWrap>> fieldWrapMap{get;set;}
    @AuraEnabled
    public List<Integer> fieldSeqList{get;set;}
    @AuraEnabled
    public CLM_Section__c section{get;set;}
    @AuraEnabled
    public Integer order{get;set;}
    @AuraEnabled
    public String secId{get;set;}
   //   @AuraEnabled
    // public Integer fieldColumn{get; set;}
    
    public CLMSectionWrap(CLM_Section__c section){
        this.section = section;
        this.order = (Integer)section.Order__c;
        fieldWrapList = new List<CLMFieldWrap>();
        ComponentOrPageWrap = New CLMComponentWrap();
        //this.fieldWrapMap = new Map<Integer,List<FieldWrap>>();
        this.fieldSeqList = new List<Integer>();
        this.secId = (section.SectionName__c).replaceAll('[^a-z^A-z^0-9]', '');
       /*   if(section.No_Of_Column__c != null && section.No_Of_Column__c != 0){
                fieldColumn = (Integer)( 12/section.No_Of_Column__c);
                
            }
            else{
                fieldColumn  = 6;
            }*/
    }
}