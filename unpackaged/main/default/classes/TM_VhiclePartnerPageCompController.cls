public with sharing class TM_VhiclePartnerPageCompController {
    
    
    @AuraEnabled
    public static String checkLicensePermition()
    {
        return LincenseKeyHelper.checkLicensePermitionForFedTOM();
    }
    
    @AuraEnabled
    public static List<Contract_Vehicle_Contact__c> getContractVehicalContact(Id contractVehicalId)
    {
        if(Schema.sObjectType.Contract_Vehicle_Contact__c.isAccessible()){
            List<Contract_Vehicle_Contact__c> contVechConList = new  List<Contract_Vehicle_Contact__c>();
            if(contractVehicalId!=Null)
            {
                contVechConList = [select Id,Name,Contact__c,Contact__r.Account.Id,Contact__r.Account.Name,Contact__r.FirstName,Contact__r.Name,Contact__r.LastName,Contact__r.Email,Contract_Vehicle__c,Main_POC__c,Receives_Mass_Emails__c from Contract_Vehicle_Contact__c where Contract_Vehicle__c =: contractVehicalId ORDER BY Contact__r.Account.Name ASC Limit 1000];
            }
            
            return contVechConList;
        }
        else{
            return null;
        }
    }
    @AuraEnabled
    public static List<VehiclePartnerWrapper1> getVehiclePartnerList(Id contractVehicalId)
    {
        List<VehiclePartnerWrapper1> vehiclePartnerWraList;
        List<Vehicle_Partner__c> vehiclePartnerList;
        if(Schema.sObjectType.Contact.isAccessible() && Schema.sObjectType.User.isAccessible() && Schema.sObjectType.Contract_Vehicle_Contact__c.isAccessible() && Schema.sObjectType.Vehicle_Partner__c.isAccessible()){  
            if(contractVehicalId != null){
                vehiclePartnerList = new List<Vehicle_Partner__c>();
                vehiclePartnerList =[SELECT id, name, Contract_Vehicle__c, Partner__c, Partner__r.Name, Point_of_Contact__c, Role__c FROM Vehicle_Partner__c WHERE Contract_Vehicle__c =: contractVehicalId ORDER BY Partner__r.Name ASC Limit 1000];
                system.debug('vehiclePartnerList ==='+vehiclePartnerList );
            }
            
            if(vehiclePartnerList != null && vehiclePartnerList.size() > 0){
                vehiclePartnerWraList = new List<VehiclePartnerWrapper1>();
                for(Vehicle_Partner__c vehiclePartner : vehiclePartnerList ){
                    VehiclePartnerWrapper1 vehiclePartnerWra = new VehiclePartnerWrapper1(vehiclePartner);
                    vehiclePartnerWraList.add(vehiclePartnerWra);
                    system.debug('vehiclePartnerWraList=='+vehiclePartnerWraList);
                }
            }
            return vehiclePartnerWraList;   
        }
        else{
            return null;
        }
        
    }
    
    @AuraEnabled
    public static List<VehiclePartnerWrapper1> getSearchVehiclePartnerList(Id contractVehicalId,string searchString)
    {
        if(searchString != '' && searchString != Null){
            searchString+='%';
            List<VehiclePartnerWrapper1> vehiclePartnerWraList;
            List<Vehicle_Partner__c> vehiclePartnerList;
            if(Schema.sObjectType.Contact.isAccessible() && Schema.sObjectType.User.isAccessible() && Schema.sObjectType.Contract_Vehicle_Contact__c.isAccessible() && Schema.sObjectType.Vehicle_Partner__c.isAccessible()){  
                if(contractVehicalId != null){
                    vehiclePartnerList = new List<Vehicle_Partner__c>();
                    vehiclePartnerList =[SELECT id, name, Contract_Vehicle__c, Partner__c, Partner__r.Name, Point_of_Contact__c, Role__c FROM Vehicle_Partner__c WHERE Contract_Vehicle__c =: contractVehicalId AND (Partner__r.Name LIKE :searchString OR Role__c LIKE :searchString) ORDER BY Partner__r.Name ASC Limit 1000];
                    system.debug('vehiclePartnerList ==='+vehiclePartnerList );
                }
                
                if(vehiclePartnerList != null && vehiclePartnerList.size() > 0){
                    vehiclePartnerWraList = new List<VehiclePartnerWrapper1>();
                    for(Vehicle_Partner__c vehiclePartner : vehiclePartnerList ){
                        VehiclePartnerWrapper1 vehiclePartnerWra = new VehiclePartnerWrapper1(vehiclePartner);
                        vehiclePartnerWraList.add(vehiclePartnerWra);
                        system.debug('vehiclePartnerWraList=='+vehiclePartnerWraList);
                    }
                }
                return vehiclePartnerWraList;   
            }
            else{
                return null;
            }
        }
        else{
            return null;
        }
    }
    
    
    @AuraEnabled
    public static void deleteContractVehicalContact(Id contractVehicalConId)
    {
        if(contractVehicalConId!=Null)
        {
            System.debug('contractVehicalConId!=Null '+contractVehicalConId);
            if(Schema.sObjectType.Contract_Vehicle_Contact__c.isAccessible()){
                List<Contract_Vehicle_Contact__c> contVechConList = [select Id from Contract_Vehicle_Contact__c where id =: contractVehicalConId];
                if(contVechConList.size() > 0)
                {
                    System.debug('contractVehicalConId '+contVechConList.size());
                    DMLManager.deleteAsUser(contVechConList);
                }
            }
        }
        
    }
    @AuraEnabled
    public static List<ContactWrapper1> showContact(string accountIdsList,id contractVehicleId)
    {
        List<ContactWrapper1> conWraList = new List<ContactWrapper1>();
        List<Contract_Vehicle_Contact__c> conVehicleContactList = new List<Contract_Vehicle_Contact__c>();
        List<Contact> conList = new List<Contact>();
        List<Contact> conInputList = new List<Contact>();
        string[] accIds;
        if(accountIdsList!=Null)
        {
            accIds=accountIdsList.split(' ');
            accIds.remove(0);
            
            if(accIds.size()>0)
            {
                if(Schema.sObjectType.Contract_Vehicle_Contact__c.isAccessible() && Schema.sObjectType.Contact.isAccessible()){
                    Set<Id> conVehicleContactSet = new Set<Id>();
                    if(contractVehicleId!=Null)
                        conVehicleContactList = [SELECT id, name, Contact__c, Contract_Vehicle__c, Main_POC__c, Receives_Mass_Emails__c  FROM Contract_Vehicle_Contact__c WHERE Contract_Vehicle__c  =: contractVehicleId ORDER BY Name ASC Limit 1000]; 
                    
                    if(conVehicleContactList != null){
                        for(Contract_Vehicle_Contact__c conVehicleContact : conVehicleContactList ){
                            conVehicleContactSet.add(conVehicleContact.Contact__c);
                        }
                    }
                    
                    conInputList  = [SELECT id, name, Email, AccountId, Account.Name FROM Contact WHERE AccountId IN : accIds ORDER BY Name ASC Limit 1000];
                    Map<id, Contact> contactMap = new Map<id,Contact>(conInputList  );
                    for(Id contactId : contactMap.keyset() ){
                        if(!conVehicleContactSet.contains(contactId)){
                            Contact con =  contactMap.get(contactId);
                            
                            
                            if(con != null){
                                conList.add(con);
                            }
                        }
                    }
                    if(conList != null && conList.size() > 0){
                        conWraList = new List<ContactWrapper1>();
                        for(Contact con : conList){
                            ContactWrapper1 conWra = new ContactWrapper1(con);
                            conWraList.add(conWra);
                            system.debug('conWraList=='+ conWraList);
                        }
                    }
                }
            }
            return conWraList;
        }
        else{
            return null;
        }
    }
    
    @AuraEnabled
    public static List<ContactWrapper1> showSearchContact (string accountIdsList,id contractVehicleId,string searchString)
    {
        if(searchString != '' && searchString != Null){
            searchString+='%';
            List<ContactWrapper1> conWraList = new List<ContactWrapper1>();
            List<Contract_Vehicle_Contact__c> conVehicleContactList = new List<Contract_Vehicle_Contact__c>();
            List<Contact> conList = new List<Contact>();
            List<Contact> conInputList = new List<Contact>();
            string[] accIds;
            if(accountIdsList!=Null)
            {
                accIds=accountIdsList.split(' ');
                accIds.remove(0);
                
                if(accIds.size()>0)
                {
                    if(Schema.sObjectType.Contact.isAccessible() &&  Schema.sObjectType.Contract_Vehicle_Contact__c.isAccessible() ){ 
                    Set<Id> conVehicleContactSet = new Set<Id>();
                    if(contractVehicleId!=Null)
                        conVehicleContactList = [SELECT id, name, Contact__c, Contract_Vehicle__c, Main_POC__c, Receives_Mass_Emails__c  FROM Contract_Vehicle_Contact__c WHERE Contract_Vehicle__c  =: contractVehicleId ORDER BY Name ASC Limit 1000]; 
                    
                    if(conVehicleContactList != null){
                        for(Contract_Vehicle_Contact__c conVehicleContact : conVehicleContactList ){
                            conVehicleContactSet.add(conVehicleContact.Contact__c);
                        }
                    }
                    
                    conInputList  = [SELECT id, name,FirstName,LastName, Email, AccountId, Account.Name FROM Contact WHERE AccountId IN : accIds AND (FirstName LIKE :searchString OR LastName LIKE :searchString OR name LIKE :searchString OR Account.Name LIKE :searchString OR Email LIKE :searchString) ORDER BY name ASC Limit 1000];
                    Map<id, Contact> contactMap = new Map<id,Contact>(conInputList  );
                    for(Id contactId : contactMap.keyset() ){
                        if(!conVehicleContactSet.contains(contactId)){
                            Contact con =  contactMap.get(contactId);
                            
                            
                            if(con != null){
                                conList.add(con);
                            }
                        }
                    }
                    if(conList != null && conList.size() > 0){
                        conWraList = new List<ContactWrapper1>();
                        for(Contact con : conList){
                            ContactWrapper1 conWra = new ContactWrapper1(con);
                            conWraList.add(conWra);
                            system.debug('conWraList=='+ conWraList);
                        }
                    }
                        }
                }
                return conWraList;
            }
            else{
                return null;
            }
        }
        else{
            return null;
        }
    }
    
    
    @AuraEnabled
    public static void addPartner(string partnerData){
        System.debug('partnerData '+partnerData);
        if(Schema.sObjectType.Contact.isAccessible() &&  Schema.sObjectType.Contract_Vehicle_Contact__c.isAccessible() ){  
            List<Contract_Vehicle_Contact__c> conVehicleContactList = new List<Contract_Vehicle_Contact__c>();
            if(partnerData != null && partnerData.length()>1)
            {
                partnerData =partnerData.substring(1);
                List<String> contactVehicleContactStrList = partnerData.split('SPLITPARENT');
                if(contactVehicleContactStrList.size() > 0)
                {
                    for(String temp : contactVehicleContactStrList){
                        Contract_Vehicle_Contact__c conVehicleContact = new Contract_Vehicle_Contact__c();
                        conVehicleContact = getContractVehicleCon(temp);
                        conVehicleContactList.add(conVehicleContact);
                    }
                }
                try{
                    if(conVehicleContactList.size() > 0){
                        // upsert conVehicleContactList;
                        DMLManager.upsertAsUser(conVehicleContactList);
                    }
                }
                catch(Exception e){
                    System.debug('Exception '+e);
                }
            }
        }
    }
    private static Contract_Vehicle_Contact__c getContractVehicleCon(String contVehContStr){
        if(contVehContStr != null && contVehContStr.trim().length() > 0){
            String[] contVehContList = contVehContStr.split('SPLITDATA');
            if(contVehContList.size() > 0){
                if(Schema.sObjectType.Contract_Vehicle_Contact__c.isCreateable() ){ 
                Contract_Vehicle_Contact__c conVehCon = new Contract_Vehicle_Contact__c();
                if(!contVehContList[0].equalsIgnoreCase('undefined')){
                    conVehCon.Contract_Vehicle__c = contVehContList[0];
                }   
                if(contVehContList.size() > 1){
                    conVehCon.Contact__c = contVehContList[1];
                }
                if(contVehContList.size() > 2){
                    conVehCon.Main_POC__c = Boolean.valueOf(contVehContList[2]);
                }
                if(contVehContList.size() > 3){
                    conVehCon.Receives_Mass_Emails__c = Boolean.valueOf(contVehContList[3]);
                }
                return conVehCon;
                    }
            }
        }
        return null;
    }
    
    @AuraEnabled
    public static void saveContractVehicleContact(string conVehicleContactList)
    {
        if(Schema.sObjectType.Contract_Vehicle_Contact__c.isAccessible() ){ 
        List<Contract_Vehicle_Contact__c> contractVehicleConList = new List<Contract_Vehicle_Contact__c>();
        if(conVehicleContactList != null && conVehicleContactList.length()>1)
        {
            conVehicleContactList =conVehicleContactList.substring(1);
            List<String> conVehicleConstrList = conVehicleContactList.split('SPLITPARENT');
            if(conVehicleConstrList.size() > 0)
            {
                for(String temp : conVehicleConstrList){
                    System.debug(''+temp);
                    Contract_Vehicle_Contact__c contractVehicleCon = new Contract_Vehicle_Contact__c();
                    contractVehicleCon = getContractVehicleContact(temp);
                    System.debug(''+contractVehicleCon);
                    contractVehicleConList.add(contractVehicleCon);
                }
            }         
            
            if(!contractVehicleConList.isEmpty()){
                try{
                    DMLManager.updateAsUser(contractVehicleConList);
                }catch(Exception e){
                    System.debug('Exception '+e);
                }
            }
        }
        }
    }
    
    private static Contract_Vehicle_Contact__c getContractVehicleContact(String contractVehicleConStr){
        if(contractVehicleConStr != null && contractVehicleConStr.trim().length() > 0){
            String[] contractVehicleConStrList = contractVehicleConStr.split('SPLITDATA');
            if(contractVehicleConStrList.size() > 0){
                if(Schema.sObjectType.Contract_Vehicle_Contact__c.isCreateable() ){ 
                Contract_Vehicle_Contact__c contractVehicleContact = new Contract_Vehicle_Contact__c();
                if(contractVehicleConStrList[0] != 'undefined'){
                    contractVehicleContact.id = contractVehicleConStrList[0];
                } 
                if(contractVehicleConStrList.size() > 1){
                    contractVehicleContact.Main_POC__c = Boolean.valueOf(contractVehicleConStrList[1]);
                }
                if(contractVehicleConStrList.size() > 2){
                    contractVehicleContact.Receives_Mass_Emails__c = Boolean.valueOf(contractVehicleConStrList[2]);
                }
                return contractVehicleContact;
            }
            }
        }
        return null;
    }
    
    
    
    
    
    
    /*    public class VehiclePartnerWrapper1{
public Vehicle_Partner__c vehiclePartner{get;set;}
public Boolean status {get;set;}

public VehiclePartnerWrapper1(Vehicle_Partner__c vehiclePartner){
this.vehiclePartner = vehiclePartner;
}
}

public class ContactWrapper1{
public Contact con {get;set;}
public Boolean conStatus {get;set;}
public Boolean Main_POC {get;set;}
public Boolean Receives_Mass_Emails {get;set;}

public ContactWrapper1(Contact con){
this.con = con;
}
}*/
}