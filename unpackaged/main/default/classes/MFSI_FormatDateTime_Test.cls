@istest
public with sharing class MFSI_FormatDateTime_Test {
    static testMethod void controllerTest(){
        MFSI_FormatDateTime m = new MFSI_FormatDateTime();
        m.dateTimeValue = system.now();
        String localFormat = m.getTimeZoneValue();
        String localFormatActual = system.now().format();
        system.assertEquals(localFormatActual, localFormat);
        m.dateTimeValue = Null;
        system.assertEquals(Null, m.getTimeZoneValue());
    }
}