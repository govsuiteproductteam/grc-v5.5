public with sharing class TaskOrderContactWrap{
         @AuraEnabled
        public Task_Order_Contact__c taskOrderContact{get; set;}
         @AuraEnabled
        public Boolean status{get; set;}
        
    public TaskOrderContactWrap(Task_Order_Contact__c taskOrderContact){
        this.taskOrderContact = taskOrderContact;
        this.taskOrderContact.Main_POC__c = false;
        this.taskOrderContact.Receives_Mass_Emails__c = false;
        status = false;
    }
    }