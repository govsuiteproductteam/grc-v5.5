@isTest
public with sharing class Test_LookUpControllar {
    public Static TestMethod void LookUpControllar(){
        PageReference pageRef = Page.LookUpPage;
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('frm','');
        System.currentPageReference().getParameters().put('txt','abc');
        System.currentPageReference().getParameters().put('paramId','');
        System.currentPageReference().getParameters().put('lksearch','');
        LookUpControllar con = new LookUpControllar();
        System.assertEquals(con.getParameter('txt'), 'abc');
        System.assertEquals(con.search(), Null);
    }
}