@isTest
public class Test_TM_ContractValueTableController {
  public static testMethod void contractValueTableTest(){
        Account account = TestDataGenerator.createAccount('Test Account');
        insert account;
        Contract_Vehicle__c contractVehicle =  TestDataGenerator.createContractVehicle('12345', account);
        insert contractVehicle;

       // Id recordTypeId = Schema.SObjectType.Value_Table__c.getRecordTypeInfosByName().get('Bookings').getRecordTypeId();
        Id contractValueRecordTypeId = Schema.SObjectType.Value_Table__c.getRecordTypeInfosByName().get('Contract Value').getRecordTypeId();
        Value_Table__c valueTable1 = TestDataGenerator.createValueTable(contractValueRecordTypeId,contractVehicle.Id);
        insert valueTable1;
        
        Value_Table__c valueTable2 = TestDataGenerator.createValueTable(contractValueRecordTypeId,contractVehicle.Id);
        insert valueTable2;
        
        Value_Table__c valueTable3 = TestDataGenerator.createValueTable(contractValueRecordTypeId,contractVehicle.Id);
        insert valueTable3;
        
        Id valueTableRecordId = valueTable2.Id;
        Test.startTest();
        List<Value_Table__c> contractValueTableList = new List<Value_Table__c>();
        contractValueTableList.add(valueTable1);
        String contractValueTableListStr = JSON.serialize(contractValueTableList);
        TM_ContractValueTableController.saveConValTable(contractValueTableListStr, contractVehicle.Id, 'abc,'+valueTableRecordId,contractVehicle);
        List<Value_Table__c> valueTableList = [SELECT Id FROM Value_Table__c WHERE id =: valueTableRecordId];
        System.assert(valueTableList.size() == 0);
        
        //List<ValueTableWrapper> valueTableWrapperList = new List<ValueTableWrapper>{new ValueTableWrapper(valueTable1)};
        Value_Table__c newValueTable = TM_ContractValueTableController.newRowContValTable(contractVehicle.Id);
        System.assert(newValueTable != null);
        /*  TM_ContractValueTableController.addBookingRow(valueTableWrapperList);
       List<Value_Table__c> valueTableList1 = TM_ContractValueTableController.setValueTableBooking(bookingTableList);
       System.assert(valueTableList1.size() != 0);
       List<Value_Table__c> valueTableList2 = TM_ContractValueTableController.getValueTableBooking();
       System.assert(valueTableList2 != null);
       Value_Table__c newValueTable1 = TM_ContractValueTableController.newRowBookingTable();
       System.assert(newValueTable1 != null);*/
        
        List<Value_Table__c> valueTableList3 = TM_ContractValueTableController.getValueTableContValList(contractVehicle.Id);
        System.assert(valueTableList3.size() != 0);
        
        Contract_Vehicle__c contractVehicle1 = TM_ContractValueTableController.getContractVehical(contractVehicle.Id);
        System.assert(contractVehicle1 != null);
        TM_ContractValueTableController.isLightningPage();
        Test.stopTest();
    }
}