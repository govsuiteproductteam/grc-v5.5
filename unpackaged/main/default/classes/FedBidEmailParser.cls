public class FedBidEmailParser implements IEmailParser{
    
    public String taskOrderNumber;
    private String taskOrderTitle;
    private String solicitationNumber;
    private String agency;
    private String contractVehicle;
    private String shipTo;
    private String setAside;
    private String paymentTerms;
    private String primaryRequirement;
    private DateTime proposalDateTime;
    private Date questionsDueDate;
    private String fedBidCategory;
    private String fedBidSubCategory;
    private String fedBidSellerCommunity;
    public String modificationName;
    public String modificationText;
    //public Boolean isRepostAmendment;
    public Boolean isExtendedAmendment;
    public Boolean hasQuestionDueDateAsText;
    
    public void parse(Messaging.InboundEmail inboundEmail){
        // To strip hyperlink from Task Order Number(Buy No)
       String emailHTMLBodyStripped = inboundEmail.htmlBody.stripHTMLTags().remove('*');

        String emailBody = inboundEmail.plainTextBody.remove('*');
        String subject = inboundEmail.subject;
        //isRepostAmendment = false;
        isExtendedAmendment = false;
        hasQuestionDueDateAsText = false;
        
        if(subject.contains('REPOST')){
            parseRepost(emailBody, emailHTMLBodyStripped);
            //isRepostAmendment = true;
        }else if(emailBody.contains('Buy No.:')){
            parseNew(emailBody, emailHTMLBodyStripped);
        }else if(emailBody.contains('Buy #:')){
            // If email subject contains 'extended' text, then that template is processed like an "Amendment" template & Contract Mod Modification name is 'Modification'
            if(subject.toLowerCase().contains('extended'))
                isExtendedAmendment = true;
            parseAmendment(emailBody, emailHTMLBodyStripped);
        }
    }
    
    private void parseNew(String emailBody, String emailHTMLBodyStripped){
        //taskOrderNumber = getRefinedSubstring(emailBody, 'Buy No.:', 'Solicitation No.:');
        taskOrderNumber = getRefinedSubstring(emailHTMLBodyStripped, 'Buy No.:', 'Solicitation No.:');
        
        if(taskOrderNumber == null){
            throw new ContractVehicleNotFoundException(MessageConstantController.TASK_ORDER_NOT_FOUND );
        }
        System.debug('taskOrderNumber = '+taskOrderNumber);  
        
        solicitationNumber = getRefinedSubstring(emailBody.stripHtmlTags(), 'Solicitation No.:','Buy Description:');
        System.debug('solicitationNumber = '+solicitationNumber);        
        
        // This method is commonly used to parse "FedBid New" & "FedBid REPOST" templates, bcoz these templates are almost identical
        parseDataForNewAndRepost(emailBody);
    }
    
    private void parseAmendment(String emailBody, String emailHTMLBodyStripped){
        //taskOrderNumber = getRefinedSubstring(emailBody, 'Buy #:','Solicitation #:');
        taskOrderNumber = getRefinedSubstring(emailHTMLBodyStripped, 'Buy #:','Solicitation #:');
        if(taskOrderNumber == null){
            throw new ContractVehicleNotFoundException(MessageConstantController.TASK_ORDER_NOT_FOUND );
        }
        System.debug('taskOrderNumber = '+taskOrderNumber);  
        
        solicitationNumber = getRefinedSubstring(emailBody.stripHtmlTags(), 'Solicitation #:','Description:');
        System.debug('solicitationNumber = '+solicitationNumber);        
        
        taskOrderTitle = getRefinedSubstring(emailBody, 'Description:','End Date/Time:').trim();
        System.debug('taskOrderTitle = '+taskOrderTitle);        
        
        DateTimeHelper dtHelper = new DateTimeHelper();
        try{
            String proposalDateTimeRaw = getRefinedSubstring(emailBody,'End Date/Time:','Question Deadline:');
            if(proposalDateTimeRaw != '')
                proposalDateTime = dtHelper.getDateTimeSpecialCase(proposalDateTimeRaw);
        }catch(Exception e){
            System.debug('Proposal Date Time Error : '+e.getMessage()+':'+e.getLineNumber());
        }   
        System.debug('proposalDateTime = '+proposalDateTime);        
        
        try{
            String questionsDueDateTimeRaw = getRefinedSubstring(emailBody,'Question Deadline:','Buyer:');
            System.debug('questionsDueDateTimeRaw='+questionsDueDateTimeRaw);
            DateTime questionsDueDateTime;
            if(questionsDueDateTimeRaw != '' && !questionsDueDateTimeRaw.toLowerCase().contains('no seller question deadline set')){
                questionsDueDateTime = dtHelper.getDateTimeSpecialCase(questionsDueDateTimeRaw);
            }else{
                hasQuestionDueDateAsText = true;
            }
            if(questionsDueDateTime != null)
                questionsDueDate = questionsDueDateTime.date();
        }catch(Exception e){
            System.debug('Questions Due Date Error : '+e.getMessage()+':'+e.getLineNumber());
        }   
        System.debug('questionsDueDate = '+questionsDueDate);        
        
        agency = getRefinedSubstring(emailBody,'Buyer:','\n');
        System.debug('agency = '+agency);        
        
        // Modification Name
        if(isExtendedAmendment){
            modificationName = 'Modification';
        }else{
            modificationName = getModificationName(taskOrderNumber, 'Modification ');
        }
        System.debug('modificationName='+modificationName);
        
        // Modification Text
        modificationText = 'Buy #: ' + getRefinedSubstring(emailBody,'Buy #:', 'You now have');
        System.debug('modificationText='+modificationText);
        
        // Remove "_#" from Task Order Number
        taskOrderNumber = taskOrderNumber.replaceAll('_([0-9]+)','').trim();
    }
    
    private void parseRepost(String emailBody, String emailHTMLBodyStripped){
        
        //taskOrderNumber = getRefinedSubstring(emailBody,'Buy No.:','Solicitation No.:');
        taskOrderNumber = getRefinedSubstring(emailHTMLBodyStripped,'Buy No.:','Solicitation No.:');
        
        if(taskOrderNumber == null){
            throw new ContractVehicleNotFoundException(MessageConstantController.TASK_ORDER_NOT_FOUND );
        }
        System.debug('taskOrderNumber = '+taskOrderNumber);  
        
        // Modification Name
        modificationName = getModificationName(taskOrderNumber, 'Repost ');
        System.debug('modificationName='+modificationName);
        
        // Modification Text
        modificationText = 'Repost Reason: ' + getRefinedSubstring(emailBody,'Repost Reason:', 'Buy Description:');
        System.debug('modificationText='+modificationText);
        
        // Remove "_#" from Task Order Number
        taskOrderNumber = taskOrderNumber.replaceAll('_([0-9]+)','').trim();
        
        solicitationNumber = getRefinedSubstring(emailBody.stripHtmlTags(),'Solicitation No.:','Repost Reason:');
        System.debug('solicitationNumber = '+solicitationNumber);        
        
        // This method is commonly used to parse "FedBid New" & "FedBid REPOST" templates, bcoz these templates are almost identical
        parseDataForNewAndRepost(emailBody);
        
    }
    
    public Task_Order__c getTaskOrder(String toAddress){ 
        if(taskOrderNumber==null){
            return null;
        }        
        Task_Order__c taskOrder = new Task_Order__c();
        List<Contract_Vehicle__c> conVehicalList = [SELECT Id FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];  
        System.debug(''+conVehicalList);
        if(!conVehicalList.isEmpty()){
            taskOrder.Contract_Vehicle__c = conVehicalList[0].Id; // Label = Contract Vehicle
            taskOrder.Name = taskOrderNumber; //Task Order Number
            
            if(taskOrderTitle !=null)
                taskOrder.Task_Order_Title__c = taskOrderTitle;// Task Order Title
            
            if(solicitationNumber !=null)
                taskOrder.Solicitation_Number__c = solicitationNumber; // Solicitation Number
            
            if(agency !=null)
                taskOrder.Agency__c = agency; // Agency
            
            if(shipTo !=null)
                taskOrder.Ship_To__c = shipTo; // Ship To
            
            if(contractVehicle !=null)
                taskOrder.Contract_Vehicle_picklist__c = contractVehicle; // Contract Vehicle
            
            if(setAside !=null)
                taskOrder.Set_Aside__c = setAside; // Set Aside
            
            if(paymentTerms !=null)
                taskOrder.Payment_Terms__c = paymentTerms; // Payment Terms
            
            if(primaryRequirement !=null)
                taskOrder.Requirement__c = primaryRequirement; // Requirement
            
            if(proposalDateTime !=null){            
                taskOrder.Due_Date__c = proposalDateTime.date(); // Due Date
                taskOrder.Proposal_Due_DateTime__c = proposalDateTime; // Proposal Due Date/Time
            }
            
            if(hasQuestionDueDateAsText){
                taskOrder.Questions_Due_Date__c = null; // Questions Due Date
            }else if(questionsDueDate !=null){          
                taskOrder.Questions_Due_Date__c = questionsDueDate; // Questions Due Date
            }
            
            if(fedBidCategory !=null)           
                taskOrder.FedBid_Category__c = fedBidCategory; // FedBid Category
            
            if(fedBidSubCategory !=null)           
                taskOrder.FedBid_Subcategory__c = fedBidSubCategory; // FedBid Sub Category
            
            if(fedBidSellerCommunity !=null)           
                taskOrder.FedBid_Seller_Community__c = fedBidSellerCommunity; // FedBid Seller Community
            
            taskOrder.Release_Date__c = Date.today();//RFP/TO Release Date
            
            return taskOrder;
        }else{
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
        }
    }
    
    public Task_Order__c updateTaskOrder(Task_Order__c taskOrder, String toAddress){
        List<Contract_Vehicle__c> conVehicalList = [SELECT Id FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];  
        System.debug(''+conVehicalList);
        if(!conVehicalList.isEmpty()){
            taskOrder.Contract_Vehicle__c = conVehicalList[0].Id; // Label = Contract Vehicle
            //taskOrder.Name = taskOrderNumber; //Task Order Number
            
            if(taskOrderTitle !=null)           
                taskOrder.Task_Order_Title__c = taskOrderTitle;// Task Order Title
            
            if(solicitationNumber !=null)
                taskOrder.Solicitation_Number__c = solicitationNumber; // Solicitation Number
            
            if(agency !=null)
                taskOrder.Agency__c = agency; // Agency
            
            if(shipTo !=null)
                taskOrder.Ship_To__c = shipTo; // Ship To
            
            if(contractVehicle !=null)
                taskOrder.Contract_Vehicle_picklist__c = contractVehicle; // Contract Vehicle
            
            if(setAside !=null)
                taskOrder.Set_Aside__c = setAside; // Set Aside
            
            if(paymentTerms !=null)
                taskOrder.Payment_Terms__c = paymentTerms; // Payment Terms
            
            if(primaryRequirement !=null)
                taskOrder.Requirement__c = primaryRequirement; // Requirement
            
            if(proposalDateTime !=null){            
                taskOrder.Due_Date__c = proposalDateTime.date(); // Due Date
                taskOrder.Proposal_Due_DateTime__c = proposalDateTime; // Proposal Due Date/Time
            }
            
            if(hasQuestionDueDateAsText){
                taskOrder.Questions_Due_Date__c = null; // Questions Due Date
            }else if(questionsDueDate !=null){          
                taskOrder.Questions_Due_Date__c = questionsDueDate; // Questions Due Date
            }
            
            if(fedBidCategory !=null)           
                taskOrder.FedBid_Category__c = fedBidCategory; // FedBid Category
            
            if(fedBidSubCategory !=null)           
                taskOrder.FedBid_Subcategory__c = fedBidSubCategory; // FedBid Sub Category
            
            if(fedBidSellerCommunity !=null)           
                taskOrder.FedBid_Seller_Community__c = fedBidSellerCommunity; // FedBid Seller Community
            
            return taskOrder;
        }else{
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
        }    
    }
    
    // This method is commonly used for "FedBid New" & "FedBid REPOST" templates, bcoz these templates are almost identical 
    private void parseDataForNewAndRepost(String emailBody){
        taskOrderTitle = getRefinedSubstring(emailBody,'Buy Description:','Buyer:');
        System.debug('taskOrderTitle = '+taskOrderTitle);        
        
        agency = getRefinedSubstring(emailBody,'Buyer:','Ship To:');
        System.debug('agency = '+agency);        
        
        shipTo = getRefinedSubstring(emailBody,'Ship To:','Contract Vehicle:');
        System.debug('shipTo = '+shipTo);        
        
        contractVehicle = getRefinedSubstring(emailBody,'Contract Vehicle:','Set-Aside Requirement:');
        System.debug('contractVehicle = '+contractVehicle);        
        
        setAside = getRefinedSubstring(emailBody,'Set-Aside Requirement:','Payment Term:').trim();
        System.debug('setAside = '+setAside);        
        
        paymentTerms = getRefinedSubstring(emailBody,'Payment Term:','Purchase Description:');
        System.debug('paymentTerm = '+paymentTerms);        
        
        primaryRequirement = getRefinedSubstring(emailBody,'Purchase Description:','End Date:');
        System.debug('primaryRequirement = '+primaryRequirement);        
        
        String proposalDateTimeRaw = getRefinedSubstring(emailBody,'End Date:','End Time:')+
            ' ' + getRefinedSubstring(emailBody,'End Time:','Seller Question Deadline:');
        
        System.debug('proposalDateTimeRaw='+proposalDateTimeRaw);
        
        DateTimeHelper dtHelper = new DateTimeHelper();
        if(proposalDateTimeRaw != ''){
            try{
                proposalDateTime = dtHelper.getDateTimeSpecialCase(proposalDateTimeRaw.trim());
            }catch(Exception e){
                System.debug('Proposal Date Error : '+e.getMessage()+':'+e.getLineNumber());
            }
        }
        System.debug('proposalDateTime='+proposalDateTime);
        
        String questionsDueDateTimeRaw = getRefinedSubstring(emailBody,'Seller Question Deadline:','Category:');
        DateTime questionsDueDateTime;
        if(questionsDueDateTimeRaw != '' && !questionsDueDateTimeRaw.contains('no seller question deadline set')){
            questionsDueDateTime = dtHelper.getDateTimeSpecialCase(questionsDueDateTimeRaw);
        }else{
            hasQuestionDueDateAsText = true;
        }
        if(questionsDueDateTime != null)
            questionsDueDate = questionsDueDateTime.date();
        System.debug('questionsDueDate = '+questionsDueDate);        
        
        fedBidCategory = getRefinedSubstring(emailBody,'Category:','Subcategory:').normalizeSpace();
        System.debug('fedBidCategory = '+fedBidCategory);        
        
        fedBidSubCategory = getRefinedSubstring(emailBody,'Subcategory:','Seller Community:');
        System.debug('fedBidSubCategory = '+fedBidSubCategory);        
        
        fedBidSellerCommunity = getRefinedSubstring(emailBody,'Seller Community:','\n').trim();
        System.debug('fedBidSellerCommunity = '+fedBidSellerCommunity);        
    }
    
    public String getRefinedSubstring(String originalText, String startText, String endText){
        String refinedSubstring = '';
        try{
            /* The word "NEW" may be placed before any of the email template fields.
Ex. "End Time: 14:00
NEW Seller Question Deadline: No Seller Question Deadline Set"
In above example Email Fields are - "End Time:" and "NEW Seller Question Deadline:"
FedTOM Fields are - "End Time:" nad "Seller Question Deadline:"
So, check for "NEW" word in Email Fields and if exist, then append "NEW" word to respective startText or EndText */
            if(originalText.contains('NEW ' + startText))
                startText = 'NEW ' + startText;
            
            if(originalText.contains('NEW ' + endText))
                endText = 'NEW ' + endText;
            
            if(originalText.contains(startText))
                refinedSubstring = originalText.substringBetween(startText, endText);
            
            if(refinedSubstring != null)
                return refinedSubstring.trim();
        }catch(Exception e){
            System.debug('Refined Substring Error : '+e.getMessage()+':'+e.getLineNumber());
            return '';
        }
        
        return '';
    }
    
    // Retrieve "Modication Name" from "Task Order Number"
    private String getModificationName(String modificationNameRaw, String modificationNamePrefix){
        if(modificationNameRaw.contains('_')){
            try{
                String[] modificationNameRawSplit = modificationNameRaw.split('_');
                if(modificationNameRawSplit.size() > 1)
                    return modificationNamePrefix + modificationNameRawSplit[1];
            }catch(Exception e){
                System.debug('Modification Name Error : '+e.getMessage()+':'+e.getLineNumber());
            }
        }
        return '';
    }
    
}