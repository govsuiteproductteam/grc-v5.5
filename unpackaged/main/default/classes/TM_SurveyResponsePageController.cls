global with sharing class TM_SurveyResponsePageController {    
   
    public List<Question__c> queList {get;set;}
    public List<QuestionWrapper> queWraList{get;set;}
    public Id surveyTaskId {get;set;}
    public List<Survey_Task__c> surveyTaskList {get;set;}
    public Survey_Task__c surveyTask {get;set;}
    public List<Survey__c> surveyList{get;set;}
    public Survey__c survey {get;set;}
    public List<Survey_Answer__c > surveyAnsList{get;set;}
    public List<Survey_Answer__c> surveyAnsInsertList;
    
    public Boolean isSuccess{get; set;}
    public boolean alreadySubmitted{get;set;}
    public user usr {get;set;}
    public Boolean isContinueClicked {get;set;}
    public Boolean isOnLoad{get; set;}
    public Boolean hideFirstPanel{get;set;}
    
    public String firstName {get;set;}
    public String lastName {get;set;}
    public String companyName {get;set;}
    public String emailAddr{get;set;}
    //public Profile p;
    public String isValidUser{get; set;}
    
    public TM_SurveyResponsePageController(ApexPages.StandardController controller) {
      isValidUser = LincenseKeyHelper.checkLicensePermitionForFedTOM();
        if(isValidUser != 'Yes')
            return;
        
        if(Schema.sObjectType.Survey_Task__c.isAccessible() && Schema.sObjectType.Survey__c.isAccessible() && Schema.sObjectType.Feedback__c.isAccessible() && Schema.sObjectType.Question__c.isAccessible()){ 
       //p = [SELECT Id, name , Profile.UserLicense.Name FROM Profile where Name = 'FedTom Packaging Profile' Limit 1];
        if(UserInfo.getUserType() == 'Guest'){
                  isOnLoad = true;
                  hideFirstPanel = true;
                  init();
                  isSuccess = true; 
            }else{
                 init();
                    isSuccess = true;
                    isOnLoad = false;
                    hideFirstPanel = false;
            }  
     
      }
      
    }
    
    public void init(){
       // ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.WARNING,'Cobnttact List = '+[Select Id from Contact where id = '0033700000Gz3MmAAJ']));
        try{
            if(ApexPages.CurrentPage().getparameters().get('sid') != null){
                surveyTaskId = String.escapeSingleQuotes(ApexPages.CurrentPage().getparameters().get('sid'));
            }
            if(surveyTaskId != null){
                surveyTaskList = new List<Survey_Task__c>();
                surveyTaskList = [SELECT id, survey__c, Task_Order__c , createdDate FROM Survey_Task__c where id =: surveyTaskId Limit 1];
                system.debug('surveyTaskList=='+surveyTaskList);
                
                if(surveyTaskList != null && surveyTaskList.size()>0){
                     
                    surveyTask = surveyTaskList[0];
                    surveyTask.Survey__c = surveyTaskList[0].Survey__c;
                    surveyTask.Task_Order__c = surveyTaskList[0].Task_Order__c;
                    
                    surveyList = [SELECT id, Name, Description__c  FROM Survey__c WHERE id =: surveyTaskList[0].Survey__c Limit 1000]; 
                    system.debug('surveyList ==='+surveyList ); 
                    
                    survey = surveyList[0];
                    //survey.id = surveyList[0].id;
                    
                } 
                
                try{
                    List<Feedback__c> feedbackList = new List<Feedback__c>();
                    feedbackList = [SELECT id, name,Contact__c FROM Feedback__c WHERE User__c =: UserInfo.getUserId() AND Survey_Task__c =: surveyTask.id Limit 1000 ];
                    system.debug('feedbackList =='+ feedbackList  );
                    
                    if(feedbackList.size() == 0){
                        alreadySubmitted = true;
                    }
                    else{
                     
                        if(UserInfo.getUserType() != 'Guest'){
                            alreadySubmitted = false;
                            hideFirstPanel  = false;
                            isOnLoad = false;
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.WARNING,'A survey for this task order and partner has already been submitted; you cannot submit the same survey twice.'));                    
                        }else{
                            alreadySubmitted = true;
                        }
                    }
                }
                catch(Exception e){
                     ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, e.getMessage()));
                }
                
                if(surveyList != null && surveyList.size()>0){
                    queList = new List<question__c>();
                    queList = [SELECT id ,Question__c, Option_1__c, Option_2__c, Option_3__c, Option_4__c, Option_5__c, Option_6__c, Option_7__c, Option_8__c, Option_9__c, Option_10__c, Answer__c, Descriptive_Answer__c, Is_mandatory__c, Keyword__c, Question_Type__c, Sequence__c, Survey__c FROM Question__c WHERE Survey__c =: survey.id order by Sequence__c Asc Limit 1000];
                }
                
               try{
                   if(queList != null && queList.size() > 0){
                      queWraList = new List<QuestionWrapper>();
                      for(Question__c que : queList){
                          QuestionWrapper queWra = new QuestionWrapper(que);
                          queWra.srNo = queWraList.size()+1;
                          queWralist.add(queWra);
                         
                      } 
                       system.debug('queWraList=='+ queWraList);
                   }
                }
                catch(Exception e){
                    System.debug(''+e.getMessage()+''+e.getLineNumber());
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, ''+e.getLineNumber()));
                }
            }
        }
        catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, e.getMessage()));
            
        }
    }
    
    // saveFeedback() method used to save questions with the options for each selected "Question type".
     public void saveFeedback(){
      
            try{
                 surveyAnsList = new List<Survey_Answer__c >();
                if(queWralist != null && queWralist.size()> 0){
                    for(QuestionWrapper queWra : queWralist){
                   
                        String strApi = 'Option_';
                        
                          /************************* for "Checkbox" Question type *******************/
                          if(queWra.question.Question_Type__c == 'Checkbox'){
                              Survey_Answer__c surveyAns = new Survey_Answer__c();
                              surveyAns.Question__c = queWra.question.id;
                               Boolean flag = false;
                                for(Integer i = 0;  i < 10 ;i++){
                                    Integer j = i+1;
                                    if(queWra.optionsList.size()>=j && queWra.optionsList[i].checkboxChecked == true){
                                      flag  = true;
                                      surveyAns.put(strApi+j+'__c',queWra.optionsList[i].options.replaceAll('\\\'','')) ;
                                    
                                    }
                                   // i++;
                                }
                              
                              if(flag){
                                  surveyAnsList.add(surveyAns );
                                  system.debug('surveyAnsList==='+surveyAnsList);
                              }
                         }   
                            
                         /********************* for "Radiobox" Question type ************************/
                        if(queWra.question.Question_Type__c == 'Radio'){
                              Survey_Answer__c surveyAns = new Survey_Answer__c();
                              surveyAns.Question__c = queWra.question.id;
                              Boolean flag = false;
                                 system.debug(' Insert if @@@ '   );
                             for(Integer i=0;  i < 10 ;i++){
                                    Integer j = i+1;
                                   system.debug(' Insert for loop ' + i);
                                  if(queWra.question.get('Option_'+j+'__c') != null && queWra.radioSelected != null && queWra.radioSelected != ''){   
                                    if( ((String)queWra.question.get('Option_'+j+'__c')).replaceAll('\'','').replaceAll('\\\\','') == queWra.radioSelected.replaceAll('\'','').replaceAll('\\\\','') ){
                                        system.debug(' Insert for loop ### ');
                                        flag = true;
                                       surveyAns.put(strApi+j+'__c',String.escapeSingleQuotes(queWra.optionsList[i].options.replaceAll('\'','').replaceAll('\\\\',''))) ;
                                    }
                                  }
                               // i++;
                            } 
                            if(flag){
                               surveyAnsList.add(surveyAns);
                               system.debug('surveyAnsList==='+surveyAnsList);
                           }
                      } 
                      
                       /****************** for "Descriptive' Question type  *************************/ 
                       if(queWra.question.Question_Type__c == 'Descriptive'){
                           Survey_Answer__c surveyAns = new Survey_Answer__c();
                           surveyAns.Question__c = queWra.question.id;
                           surveyAns.Answer_Descriptive__c = String.escapeSingleQuotes(queWra.answerDesc);
                           Boolean flag = false;
                           if(surveyAns.Answer_Descriptive__c != null && surveyAns.Answer_Descriptive__c != ''){
                               flag = true;
                               surveyAnsList.add(surveyAns);
                           }
                       }
                       
                       /******************** for "Text" Question type ********************************/
                       if(queWra.question.Question_type__c == 'Text'){
                           Survey_Answer__c surveyAns = new Survey_Answer__c();
                           surveyAns.Question__c = queWra.Question.id;
                           surveyAns.Answer_Descriptive__c = String.escapeSingleQuotes(queWra.answerDesc);
                           if(surveyAns.Answer_Descriptive__c != null && surveyAns.Answer_Descriptive__c != ''){
                               surveyAnsList.add(surveyAns);
                                system.debug('surveyAnsList==='+surveyAnsList);
                           }
                       }
                       
                       /***************** for "Picklist" Question type ****************************/
                       if(queWra.question.Question_Type__c == 'Picklist'){
                              Survey_Answer__c surveyAns = new Survey_Answer__c();
                              surveyAns.Question__c = queWra.question.id;
                              Boolean flag = false;
                                 for(Integer i=0;  i < 10 ;i++){
                                   Integer j = i+1;
                                      if(queWra.question.get('Option_'+j+'__c') != null && queWra.picklistSelected!= null && queWra.picklistSelected!= ''){   
                                            if( ((String)queWra.question.get('Option_'+j+'__c')).replaceAll('\'','').replaceAll('\\\\','') == queWra.picklistSelected.replaceAll('\'','').replaceAll('\\\\','')){
                                                flag = true;
                                               surveyAns.put(strApi+j+'__c',String.escapeSingleQuotes(queWra.optionsList[i].options.replaceAll('\'','').replaceAll('\\\\',''))) ;
                                            }
                                      }
                               // i++;
                            }  
                           if(flag){
                               surveyAnsList.add(surveyAns);
                               system.debug('surveyAnsList==='+surveyAnsList);
                           }
                      } 
                }
              }
           }
           catch(Exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, e.getMessage()));
            }
    }
    
    // submit() method used to save survey response of user. 
     public PageReference submit(){
       if(Schema.sObjectType.feedback__c.isAccessible() && Schema.sObjectType.Task_Order__c.isAccessible() && Schema.sObjectType.User.isAccessible() && Schema.sObjectType.Survey_Task__c.isAccessible()  ){
            saveFeedback();
            system.debug('surveyAnsList@@@@@@'+surveyAnsList);
            
            try{
                if(surveyAnsList != null && surveyAnsList.size() >0){
                    feedback__c feedback = new feedback__c ();
                    if (Schema.sObjectType.feedback__c.fields.Task_Order__c.isAccessible() && Schema.sObjectType.feedback__c.fields.Task_Order__c.isCreateable()) {
                        feedback.Task_Order__c =  surveyTask.Task_Order__c;
                     }
                     
                    if (Schema.sObjectType.feedback__c.fields.Survey_Task__c.isAccessible()&& Schema.sObjectType.feedback__c.fields.Survey_Task__c.isCreateable()) { 
                        feedback.Survey_Task__c  = surveyTask.id;
                    }
                    Id userIds = UserInfo.getUserId();
                    List<User> userList = [SELECT id, Name, ContactId , Contact.AccountId from User Where id =: userIds  Limit 1000];
                    system.debug('userList ##'+ userList);
                  
                      //  Profile p = [SELECT Id, name , Profile.UserLicense.Name FROM Profile where Name = 'IDIQ Profile' Limit 1];
                        if(UserInfo.getUserType() == 'Guest'){
                        system.debug('&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&');
                            if(con  != null ){
                                  if (Schema.sObjectType.feedback__c.fields.Contact__c.isAccessible() && Schema.sObjectType.feedback__c.fields.Contact__c.isCreateable()) {
                                      feedback.Contact__c = con.id;
                                  }
                                  if(con.AccountId != null){
                                   if (Schema.sObjectType.feedback__c.fields.Company_Name__c.isAccessible() && Schema.sObjectType.feedback__c.fields.Company_Name__c.isCreateable()) {
                                      feedback.Company_Name__c =  con.AccountId;
                                  }
                                 }
                             }
                            if (Schema.sObjectType.feedback__c.fields.User__c.isAccessible() && Schema.sObjectType.feedback__c.fields.User__c.isCreateable()) {
                                feedback.User__c = userIds ;
                            }
                            
                            if(Schema.sObjectType.feedback__c.fields.Date_Response_Received__c.isAccessible() && Schema.sObjectType.feedback__c.fields.Date_Response_Received__c.isCreateable()){
                                feedback.Date_Response_Received__c = DateTime.now();
                            }
                            if (Schema.sObjectType.feedback__c.fields.Date_Teaming_Request_sent__c.isAccessible() && Schema.sObjectType.feedback__c.fields.Date_Teaming_Request_sent__c.isCreateable()) {
                                feedback.Date_Teaming_Request_sent__c = surveyTask.createdDate;
                            }
                        }else{
                            if(userList.size()>0 && userList[0].contactId != null){
                                if (Schema.sObjectType.feedback__c.fields.Contact__c.isAccessible() && Schema.sObjectType.feedback__c.fields.Contact__c.isCreateable()) {
                                    feedback.Contact__c = userList[0].contactId;
                                }
                                if (Schema.sObjectType.feedback__c.fields.Company_Name__c.isAccessible() && Schema.sObjectType.feedback__c.fields.Company_Name__c.isCreateable()) {
                                    feedback.Company_Name__c =  userList[0].Contact.AccountId;
                                }
                            }
                            
                            if (Schema.sObjectType.feedback__c.fields.User__c.isAccessible() && Schema.sObjectType.feedback__c.fields.User__c.isCreateable()) {
                                feedback.User__c = userIds ;
                            }
                            
                            if (Schema.sObjectType.feedback__c.fields.Date_Response_Received__c.isAccessible() && Schema.sObjectType.feedback__c.fields.Date_Response_Received__c.isCreateable()) {
                                feedback.Date_Response_Received__c = DateTime.now();
                            }
                            
                            if (Schema.sObjectType.feedback__c.fields.Date_Teaming_Request_sent__c.isAccessible() && Schema.sObjectType.feedback__c.fields.Date_Teaming_Request_sent__c.isCreateable()) {
                                feedback.Date_Teaming_Request_sent__c = surveyTask.createdDate;
                            }
                       }
                       system.debug('feedback ###' + feedback);
                       
                       if (Schema.sObjectType.feedback__c.isCreateable()) {
                           insert feedback;
                       }
                      // DMLManager.insertAsUser(feedback);
                       
                       surveyAnsInsertList = new List<Survey_Answer__c>();
                       for(Survey_Answer__c surAns : surveyAnsList ){
                           surAns.feedback__c = feedback.id;
                           surveyAnsInsertList.add(surAns);
                       }
                       
                       if(surveyAnsInsertList != null && surveyAnsInsertList.size()>0){
                           //upsert surveyAnsInsertList;
                           DMLManager.upsertAsUser(surveyAnsInsertList);
                           system.debug('surveyAnsInsertList==='+surveyAnsInsertList);
                       }
                }
            }
            catch(exception e){
                ApexPages.addMessage(new Apexpages.Message(Apexpages.SEVERITY.ERROR, e.getMessage()+ ' error on line number==='+ e.getLineNumber()));
               // system.debug('error on line number==='+ e.getLineNumber());
            }
         }
        return null;
    } 
    
        public pageReference callafterfinish(){
           // Profile p = [SELECT Id, name , Profile.UserLicense.Name FROM Profile where Name = 'IDIQ Profile' Limit 1];
           if(UserInfo.getUserType() == 'Guest'){
                 return new PageReference('/'+'TM_TOMA__TM_SuccessMsgPage');
           }else{
                return new PageReference('/'+'home/home.jsp');
           }
           
         /*  if(p.id != userInfo.getProfileId()){
                return new PageReference('/'+'home/home.jsp');
            }else{
                if(p.id == userInfo.getProfileId()){
                   return new PageReference('/'+'TM_TOMA__TM_SuccessMsgPage');
                    
                }
            }*/
            return null;
        }
        
        private contact con;
        private Task_Order_Contact__c taskOrderCon;
        public void continueNew(){
                //taskOrderCon = new Task_Order_Contact__c ();
                List<Survey_Task__c> surveyTaskList = [SELECT id, Task_Order__c, Survey__c FROM  Survey_Task__c Where id =:surveyTaskId];
                system.debug('surveyTaskList =='+ surveyTaskList );
                Id taskOrderID = surveyTaskList[0].Task_Order__c;
               /* system.debug('taskOrderID=='+ taskOrderID);
                List<Task_Order_Contact__c> taskOrderConList = [Select id, name, Contact__c,Contact__r.email, Contact__r.FirstName,Contact__r.LastName ,Contact__r.AccountId,Task_Order__c FROM Task_Order_Contact__c Where Task_Order__c =:taskOrderID AND Contact__r.email=: emailAddr];
                system.debug('taskOrderConList=='+ taskOrderConList );
                */
           List<Contact> conList = [Select Id, firstName,lastName,Contact.AccountId from Contact where id IN (Select Contact__c from Task_Order_Contact__c where Contact__r.email =: emailAddr AND task_order__c =: taskOrderID )];
           
           system.debug('conList=='+  conList);
            if(conList != null && conList.size()>0){
               Boolean isSet = false;
               for(Contact con1 : conList){
                   if(con1.FirstName == firstName && con1.LastName == lastName){
                       isSet = true;
                       con = con1;
                       system.debug('con=='+con);
                   }
               } 
               if(!isSet){
                   con  = conList[0];
               }
               
                isContinueClicked = true;
                isOnLoad = true;
                List<Feedback__c>fdbackConList = [SELECT id, name,Contact__c FROM Feedback__c WHERE Survey_Task__c =: surveyTask.id AND Contact__c  =:con.id   Limit 1000];
                if(!fdbackConList.isEmpty()){
                  // alreadysubmitted = false;
                   ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.WARNING,'A survey for this task order and partner has already been submitted; you cannot submit the same survey twice.'));                    
                   isContinueClicked = false;
                   isOnLoad = false;
                }
               
            }else{
                ApexPages.addMessage(new Apexpages.Message(Apexpages.SEVERITY.ERROR, 'You do not have permission to access this survey. Please make sure you are entering the same email address and contact information where the survey was received.'));
                isContinueClicked = false;
                isOnLoad = false;
            }
          
       
        }
   /****************************** Question and Option Wrapper classes *******************************/
    
    public class QuestionWrapper{
        public Question__c question {get;set;}
        public Integer srNo {get;set;}
        public String picklistSelected{get;set;}
        public String radioSelected {get;set;}
        public List<OptionWrapper> optionsList {get;set;}
        public List<SelectOption> selectOptionPicklist {get;set;}
        public List<SelectOption> selectOptionRadio {get;set;}
        public String answerDesc{get;set;}
           
        public QuestionWrapper(Question__c question ){
            this.question = question ;
            Integer i=1;
                     
             optionsList = new List<OptionWrapper>();
             while(i <= 10 && question.get('Option_'+i+'__c')!=null){
                  String optStr = (String)question.get('Option_'+i+'__c');
                  if(optStr.length() > 0){                      
                      optionsList.add(new OptionWrapper( (String)question.get('Option_'+i+'__c'),i));
                  }
                  i++;
              }
              for(optionWrapper optW : optionsList){
                  system.debug('optW== '+optW.options + ' sr number==== '+ optW.sr );
              }
          
            if(question.Question_Type__c == 'Picklist' && optionsList.size() > 0){
                selectOptionPicklist = new List<SelectOption>();
                for(OptionWrapper optWra : optionsList){
                    selectOptionPicklist.add(new selectOption(optWra.options, optWra.options));                    
                }
            }
            
             if(question.Question_Type__c == 'Radio' && optionsList.size() > 0){
                selectOptionRadio  = new List<SelectOption>();
                for(OptionWrapper optWra : optionsList){
                    selectOptionRadio.add(new selectOption(optWra.options, optWra.options));
                }
            }
            
        }
    }
    
   
    public class OptionWrapper{
        public String options {get;set;}
        public Integer Sr {get;set;}
        public Boolean checkboxChecked {get;set;}
      
        
        public OptionWrapper(String options, Integer sr){
            this.options = options;
            this.Sr = Sr;
            checkboxChecked = false;
           
        }
    }
}