({
    getConVehicleContactList1 : function(component, event, helper) {
        var action = component.get("c.getContractVehicalContact");
        action.setParams({
            contractVehicalId: component.get("v.recordId")
        });
        action.setCallback(this, function(a) {
            var state= a.getState();
            if(state=="SUCCESS"){
                
                component.set("v.conVehicleContactList", a.getReturnValue());
                component.set("v.checkSavebutton",0);
            }
            else if (state == "ERROR") 
            {
                console.log(a.getError());            
            }
        })
        $A.enqueueAction(action); 
    },
    getVehiclePartnerWraList1 : function(component, event, helper) {
        var action = component.get("c.getVehiclePartnerList");
        action.setParams({
            contractVehicalId: component.get("v.recordId")
        });
        action.setCallback(this, function(a) {
            var state= a.getState();
            if(state=="SUCCESS"){
                var temp =a.getReturnValue();
                component.set("v.vehiclePartnerWraList", a.getReturnValue());
                component.set("v.vehiclePartnerWraListTemp", a.getReturnValue());
                component.set("v.conWraList",);
            }
            else if (state == "ERROR") 
                {
                    console.log(a.getError());            
                }
            })
            $A.enqueueAction(action); 
    }
})