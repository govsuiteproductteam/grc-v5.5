public class TM_PageLayoutField {
    @TestVisible private TM_PageLayoutController plController;
    
    public transient String fieldApi{get;set;}
    public transient String fieldLabel{get;set;}
    public transient String helpText{get;set;}
    public transient Boolean isRequired{get;set;}
    public transient Boolean useCustomLookup{get;set;}//newlly adde 27-06-2018
    public transient Boolean isDraftReq{get;set;}
    public transient String ViewFieldApi{get;set;}
    public transient Integer tabFieldSeq{get;set;}
    public transient Integer secFieldSeq{get;set;}
    public transient Integer fieldSequenceUpdate{get;set;}
    
    public transient Boolean createBlankSpace{get;set;}//newlly added 16-05-2018
    
     //New veriables 14/11/2016
    public transient String glanceFieldBackColor{get;set;}
    public transient Boolean glanceIsComponent{get;set;}
    public transient Integer glanceColSpan{get;set;}
    public transient String glanceDocId{get;set;}
    
    public transient String glanceAppApi{get;set;}
    public transient String errorMsg{get;set;}
    
    //Delete Veriables
    public transient Integer tabSequenceFieldDel{get;set;}
    public transient Integer secSequenceFieldDel{get;set;}
    public transient Integer fieldSequenceFieldDel{get;set;}
    
    
    
    public String glanceClass{get;set;}
    
    //
    //private Map<String,String> vfpNameNdNameSpMap;
    //public String vfpageApi{get;set;}
    
    public TM_PageLayoutField(TM_PageLayoutController plController){
        this.plController = plController;
        glanceClass ='';
        /*vfpNameNdNameSpMap = new Map<String,String>();
        vfpageApi = '';
        for(ApexPage apexPage : [select masterlabel, NamespacePrefix, Markup from ApexPage where ControllerType='1' LIMIT 50]){            
            if(apexPage.Markup.replace(' ','').containsIgnoreCase('standardcontroller="opportunity"')){
                vfpageApi += '"'+apexPage.masterlabel+'",';
                vfpNameNdNameSpMap.put(apexPage.masterlabel,apexPage.NamespacePrefix);
            }
        }*/
    }
    
    public void addField(){  
         if(createBlankSpace != true){//newlly added 16-05-2018 if(createBlankSpace != true) for Create Blank Field code
            //old code
        if(fieldApi!=Null && fieldApi!='' && fieldLabel!=Null && fieldLabel!=''){            
            if(fieldSequenceUpdate!=Null && fieldSequenceUpdate>0){                
                plController.tabLanding = tabFieldSeq;
                tabFieldSeq -= 1;
                secFieldSeq -= 1;
                fieldSequenceUpdate -= 1;
                try{
                    if(plController.tabWrapperList != Null){
                        TM_PageLayoutController.FieldWrapper tempFieldWrap = plController.tabWrapperList[tabFieldSeq].sectionWrapperList[secFieldSeq].fieldObjList[fieldSequenceUpdate];
                        tempFieldWrap.vfCustomField.FieldApiName__c = fieldApi;
                        tempFieldWrap.vfCustomField.FieldLabel__c = fieldLabel;
                        tempFieldWrap.vfCustomField.HelpText__c = helpText;
                        tempFieldWrap.vfCustomField.Required__c = isRequired;
                        tempFieldWrap.vfCustomField.Draft_Required__c = isDraftReq;
                        tempFieldWrap.vfCustomField.ViewFieldApi__c = ViewFieldApi != Null ? ViewFieldApi : fieldApi;
                        tempFieldWrap.vfCustomField.Required_Error_Message__c = errorMsg;
                        //newlly added 16-05-2018
                        tempFieldWrap.vfCustomField.Create_Blank_Space__c = createBlankSpace;
                        //newlly added 27-06-2018
                        tempFieldWrap.vfCustomField.Use_Custom_lookup__c = useCustomLookup;
                        System.debug(tempFieldWrap.vfCustomField.ViewFieldApi__c);
                    }
                }catch(Exception e){}
                
            }else{
                //newlly added 16-05-2018 Create_Blank_Field__c = createBlankSpace
                //newlly added 27-06-2018 Use_Custom_lookup__c=useCustomLookup for new lookup
                CLM_FIeld__c field = new CLM_FIeld__c(FieldApiName__c = fieldApi, FieldLabel__c = fieldLabel, HelpText__c = helpText, Required__c=isRequired, Draft_Required__c = isDraftReq, ViewFieldApi__c = ViewFieldApi,Required_Error_Message__c = errorMsg,Create_Blank_Space__c = createBlankSpace,Use_Custom_lookup__c=useCustomLookup);
                plController.addField(field,tabFieldSeq,secFieldSeq);
            }
        }
             //end old code
        }
         else{//newlly added 16-05-2018 for Create Blank Field code
            if(fieldSequenceUpdate!=Null && fieldSequenceUpdate>0){                
                    plController.tabLanding = tabFieldSeq;
                    tabFieldSeq -= 1;
                    secFieldSeq -= 1;
                    fieldSequenceUpdate -= 1;
                    try{
                        if(plController.tabWrapperList != Null){
                            TM_PageLayoutController.FieldWrapper tempFieldWrap = plController.tabWrapperList[tabFieldSeq].sectionWrapperList[secFieldSeq].fieldObjList[fieldSequenceUpdate];
                            tempFieldWrap.vfCustomField.FieldApiName__c = fieldApi;
                            tempFieldWrap.vfCustomField.FieldLabel__c = fieldLabel;
                            tempFieldWrap.vfCustomField.HelpText__c = helpText;
                            tempFieldWrap.vfCustomField.Required__c = isRequired;
                            tempFieldWrap.vfCustomField.Draft_Required__c = isDraftReq;
                            tempFieldWrap.vfCustomField.ViewFieldApi__c = ViewFieldApi != Null ? ViewFieldApi : fieldApi;
                            tempFieldWrap.vfCustomField.Required_Error_Message__c = errorMsg;
                            //newlly added 16-05-2018
                            tempFieldWrap.vfCustomField.Create_Blank_Space__c = createBlankSpace;
                            System.debug(tempFieldWrap.vfCustomField.ViewFieldApi__c);
                        }
                    }catch(Exception e){}
                    
                }else{
                    //newlly added 16-05-2018 Create_Blank_Space__c = createBlankSpace
                    CLM_FIeld__c field = new CLM_FIeld__c(FieldApiName__c = fieldApi, FieldLabel__c = fieldLabel, HelpText__c = helpText, Required__c=isRequired, Draft_Required__c = isDraftReq, ViewFieldApi__c = ViewFieldApi,Required_Error_Message__c = errorMsg,Create_Blank_Space__c = createBlankSpace);
                    plController.addField(field,tabFieldSeq,secFieldSeq);
                }
        }
    }
    
    public void deleteField(){
        if(tabSequenceFieldDel != Null && secSequenceFieldDel != Null && fieldSequenceFieldDel != Null){
            plController.deleteField(tabSequenceFieldDel, secSequenceFieldDel, fieldSequenceFieldDel);
        }
    }
    
    public void deleteGlance(){
        if(fieldSequenceFieldDel != Null){
            plController.deleteGlance(fieldSequenceFieldDel);
        }
    }
    
    //Section Rearrange
     public void fieldRearrange(){
         if(tabFieldSeq!=Null && tabFieldSeq>0 && secFieldSeq!=Null && secFieldSeq>0){
            tabFieldSeq -= 1;
            secFieldSeq -= 1;
            plController.tabRearrangeList.clear();
             try{
                 for(TM_PageLayoutController.FieldWrapper tempFieldWrap : plController.tabWrapperList[tabFieldSeq].sectionWrapperList[secFieldSeq].fieldObjList){
                     plController.tabRearrangeList.add(tempFieldWrap.vfCustomField.FieldLabel__c);
                 }
             }catch(Exception e){}
         }
    }
    
    public void glanceRearrange(){
        plController.tabRearrangeList.clear();
        glanceClass = 'glance';
        for(TM_PageLayoutController.AtaGlanceWrapper tempGlanceWrap : plController.ataGlanceWrapperList){
            if(!tempGlanceWrap.ataGlanceField.Is_Component__c){
                plController.tabRearrangeList.add(tempGlanceWrap.ataGlanceField.FieldLabel__c);
            }else{
                plController.tabRearrangeList.add(tempGlanceWrap.ataGlanceField.FieldApiName__c);
            }
        }
    }
    
    public void applyFieldRearrange(){
        if(plController.tabRearrangeString != Null && tabFieldSeq!=Null && secFieldSeq!=Null && secFieldSeq>0){
            plController.tabLanding = tabFieldSeq;
            tabFieldSeq -= 1;
            secFieldSeq -= 1;
            Integer i=1;
            try{
                for(String reFieldName : plController.tabRearrangeString.split('SPLITXCOMMA')){//newlly added split('SPLITXCOMMA') in place of split(',') 17-05-2018
                    if(reFieldName!=''){
                        if(reFieldName.contains('&amp;')){
                            reFieldName.replace('&amp;','&');
                        }
                        for(TM_PageLayoutController.FieldWrapper tempFieldWrap : plController.tabWrapperList[tabFieldSeq].sectionWrapperList[secFieldSeq].fieldObjList){
                            if(tempFieldWrap.vfCustomField.FieldLabel__c.replace(' ','')+''+tempFieldWrap.fieldOrder == reFieldName.replace(' ','')){
                                tempFieldWrap.fieldOrder = i;
                                break;
                            }
                        }
                        i++;
                    }                
                }
                plController.tabWrapperList[tabFieldSeq].sectionWrapperList[secFieldSeq].fieldObjList.sort();
                plController.tabRearrangeList.clear();
                //plController.tabLanding = 1;
            }catch(Exception e){}
        }
    }
    
    public void applyGlanceRearrange(){
        if(plController.tabRearrangeString != Null){
            Integer i=1;
            try{
                System.debug('allRearr '+plController.tabRearrangeString);
                for(String reFieldName : plController.tabRearrangeString.split('SPLITXCOMMA')){//newlly added split('SPLITXCOMMA') in place of split(',') 17-05-2018
                    if(reFieldName!=''){
                        if(reFieldName.contains('&amp;')){
                            reFieldName.replace('&amp;','&');
                        }
                        for(TM_PageLayoutController.AtaGlanceWrapper tempGlanceWrap : plController.ataGlanceWrapperList){
                            if(!tempGlanceWrap.ataGlanceField.Is_Component__c){
                                if(tempGlanceWrap.ataGlanceField.FieldLabel__c.replace(' ','') +''+ tempGlanceWrap.glanceOrder == reFieldName.replace(' ','')){
                                    tempGlanceWrap.glanceOrder = i;
                                    break;
                                }
                            }else{
                                if(tempGlanceWrap.ataGlanceField.FieldApiName__c.replace(' ','') +''+ tempGlanceWrap.glanceOrder == reFieldName.replace(' ','')){
                                    tempGlanceWrap.glanceOrder = i;
                                    break;
                                }
                            }
                        }
                        i++;
                    }
                }
                plController.ataGlanceWrapperList.sort();
                plController.tabRearrangeList.clear();
            }catch(Exception e){System.debug('@@@ '+e.getMessage());}
        }
    }
    
    public void addGlance(){
        if(fieldApi!=Null && fieldApi!='' && ((fieldLabel!=Null && fieldLabel!='') || glanceIsComponent)){
            if(fieldSequenceUpdate!=Null && fieldSequenceUpdate>0){
                fieldSequenceUpdate -= 1;
                try{
                    if(plController.ataGlanceWrapperList!=Null){
                        TM_PageLayoutController.AtaGlanceWrapper glanceWrap = plController.ataGlanceWrapperList[fieldSequenceUpdate];
                        glanceWrap.ataGlanceField.FieldApiName__c=fieldApi;
                        glanceWrap.ataGlanceField.ColSpan__c=glanceColSpan;
                        glanceWrap.ataGlanceField.Is_Component__c=glanceIsComponent;
                        if(!glanceIsComponent){
                        	glanceWrap.ataGlanceField.FieldLabel__c = fieldLabel;
                            glanceWrap.ataGlanceField.Color__c = glanceFieldBackColor;
                            glanceWrap.ataGlanceField.Document_Id__c = glanceDocId;
                        }else{
                            glanceWrap.ataGlanceField.AppApi__c = glanceAppApi;
                        }
                    }
                }catch(Exception e){}
                
            }else{
                CLM_AtaGlanceField__c glance = new CLM_AtaGlanceField__c();
                glance.FieldApiName__c=fieldApi;
                glance.ColSpan__c=glanceColSpan;
                glance.Is_Component__c=glanceIsComponent;
                if(!glanceIsComponent){
                    glance.FieldLabel__c = fieldLabel;
                    glance.Color__c = glanceFieldBackColor;
                    glance.Document_Id__c = glanceDocId;
                }else{
                    glance.AppApi__c = glanceAppApi;
                }
                plController.addGlance(glance);
            }
        }
    }/**/
}