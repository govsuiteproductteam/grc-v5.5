@IsTest
public class DependentFieldSettingCtrlTest {
    public static testmethod void testDependentFieldSettingCtrl(){
        DependentFieldSettingCtrl depFieldSett = new DependentFieldSettingCtrl();
        depFieldSett.getsObjectTypeOptions();
        List<RecordType> rtypes = [Select Name, Id From RecordType where sObjectType='TM_TOMA__Contract_Vehicle__c' and isActive=true];
        if(rtypes.size()>0)
        {
            depFieldSett.recordTypeId=rtypes[0].Id;
        }
        Document document;
        document = new Document();
        document.Body = Blob.valueOf('');
        document.ContentType = 'txt';
        document.DeveloperName = 'Test_Dependent_Field_Document_contract';
        document.IsPublic = true;
        document.Name = 'Test Dependent Field Document contract';
        document.FolderId = [select id from folder where name = 'Dependent Field Doc'].id;
        insert document;
        depFieldSett.objectType = 'Contract';
        depFieldSett.selectSObject();
        depFieldSett.go();
        depFieldSett.masterLabel = 'TM_TOMA__Task_Order_Types__c';
        depFieldSett.addMaster();
        depFieldSett.masterValue = 'All Type';
        depFieldSett.listIndex = 0;
        depFieldSett.dependentSields = 'TM_TOMA__Task_Order_Types__c';
        depFieldSett.addNewValue();
        depFieldSett.masterValue = 'All Type';
        depFieldSett.addField();
        depFieldSett.dependentSields = 'TM_TOMA__Ceiling__c';
        depFieldSett.addField();
        depFieldSett.dependentSields = 'TM_TOMA__Ceiling__c';
        depFieldSett.addField();
        
        depFieldSett.save();
        
        depFieldSett.masterValue = 'All Type';
        depFieldSett.listIndex = 0;
        depFieldSett.fieldApeIndex = 0;
        depFieldSett.removeField();
        depFieldSett.listIndex = 0;
        depFieldSett.removeValue();
        depFieldSett.listIndex = 0;
        depFieldSett.removeValue();
        depFieldSett.listIndex = 0;
        depFieldSett.removeFieldStructure();
        
        if(rtypes.size()>0)
        {
        depFieldSett.recordCloinId=rtypes[1].Id;
        }
        depFieldSett.cloneLayout();
        System.assertEquals('Save Clone', depFieldSett.btnLabel);
        
        depFieldSett.masterLabel = 'TM_TOMA__Task_Order_Types__c';
        depFieldSett.addMaster();
        depFieldSett.masterLabel = '';
        depFieldSett.addMaster();
        depFieldSett.masterLabel = 'TM_TOMA__Test';
        depFieldSett.addMaster();
        System.assertEquals(depFieldSett.message,'The Field '+depFieldSett.masterLabel+' not belongs to '+depFieldSett.objectType+'.');
        depFieldSett.masterLabel = 'TM_TOMA__Task_Order_Types__c';
        depFieldSett.addNewValue();
        depFieldSett.masterValue = '';
        depFieldSett.addNewValue();
        System.assertEquals(depFieldSett.message,'Please enter value.');
        depFieldSett.getContractFields();
        
        System.assert(depFieldSett.getRecordTypeOptions()!= null);

        depFieldSett.saveclone();
        depFieldSett.go();
        
    }
    public static testmethod void testDependentFieldSettingCtrl1(){
        DependentFieldSettingCtrl depFieldSett = new DependentFieldSettingCtrl();
        List<RecordType> rtypes = [Select Name, Id From RecordType where sObjectType='TM_TOMA__SubContract__c' and isActive=true];
        if(rtypes.size()>0)
        {
            depFieldSett.recordTypeId=rtypes[0].Id;
        }
        Document document;
        document = new Document();
        document.Body = Blob.valueOf('');
        document.ContentType = 'txt';
        document.DeveloperName = 'Test_Dependent_Field_Document_subContract';
        document.IsPublic = true;
        document.Name = 'Test Dependent Field Document subContract';
        document.FolderId = [select id from folder where name = 'Dependent Field Doc'].id;
        insert document;
        
        depFieldSett.objectType = 'Sub Contract';
        depFieldSett.selectSObject();
        depFieldSett.go();
        depFieldSett.masterLabel = 'TM_TOMA__Task_Order_Types__c';
        depFieldSett.addMaster();
        depFieldSett.masterValue = 'All Type';
        depFieldSett.listIndex = 0;
        
        depFieldSett.addNewValue();
        depFieldSett.addField();
        depFieldSett.save();
        depFieldSett.masterLabel = 'TM_TOMA__Task_Order_Types__c';
        depFieldSett.addMaster();
        //System.assertEquals('The Field '+depFieldSett.masterLabel+' is already Exist.', depFieldSett.message);
        depFieldSett.removeFieldStructure();
        depFieldSett.masterValue = 'All Type';
        
        depFieldSett.getContractFields();
        
        System.assert(depFieldSett.getRecordTypeOptions()!= null);
    }
}