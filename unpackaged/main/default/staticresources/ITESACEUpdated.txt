Greetings,
The order for request, HDWE-18-1219 - Operations Bags, Keyboards12, Electricity, & Mice has been approved.
Please place the order. Any questions should be directed toClanton, Rosarea L CIV USARMY Gov CEIT
(US) at (601) 629-3757.
Note: No Action is required by the Customer or Request Approver. This email is for use by the vendor to
actually place the attached order. If you see any issues with your order, please contact the POC listed
above.
https://apps.usace.army.mil/sites/OrderTrak/SitePages/OrderDetails.aspx?OrderID=31578

OrderTrak Notification
Please do not respond/reply to this email. Points of Contact for this request are displayed on
the https://apps.usace.army.mil/sites/OrderTrak/
 
 
You're receiving this message because you're a member of the Sales-ITES ACE Orders group. If you
don't want to receive any messages or events from this group, stop following it in your inbox.