public with sharing class TM_ContractModListController {
    public List<Contract_Mods__c> contractModeList{get;set;}
    public TM_ContractModListController(ApexPages.StandardController controller){
        if(Schema.sObjectType.Contract_Mods__c.isAccessible())
            contractModeList = [SELECT Id, Name, createdDate, RecordTypeId FROM Contract_Mods__c WHERE TaskOrder__c = :controller.getRecord().Id AND RecordType.DeveloperName = 'Update' ORDER BY createdDate];
    }
}