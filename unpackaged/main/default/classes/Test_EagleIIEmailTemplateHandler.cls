@isTest
public with sharing class Test_EagleIIEmailTemplateHandler {
    public Static TestMethod void testEagleIIEmailTemplateHandler (){
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Contact con = TestDataGenerator.createContact('TestContact', acc );
        insert con;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('T-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'eagleii@d-n3hb4l1jktiefjd99s91te1u1fvra7ge5wrm5mlo32z1w9gc2.61-8ji7eae.na34.apex.salesforce.com';        
        insert conVehi;    
        
        Messaging.InboundEmail email  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        email.plainTextBody = 'Good Afternoon,\n'+
            
            'Please find attached Request for Quote (RFQ) HSSS01-15-Q-0300 and its corresponding attachments. Please review the Instructions to Offers very carefully, as there is important information regarding this requirement. In order to be considered for this requirement, a representative from every interested vendor must attend at mandatory site visit September 10, 2015 at 9:00 AM EST. If you wish to attend, please submit a completed SSF 3237: USSS Facility Access Form to the contract specialist at gabriella.marino@associates.usss.dhs.gov NO LATER THAN SEPTEMBER 9, 2015 at 12:00 NOON EST. The site visit will be at the J.J. Rowley Training Center in Laurel, MD. \n'+          
            'Responses are due to the Contract Specialist *no later than September 18,\n2015 at 10:00 AM EST. \n'+
            
            'Please let me know if you have any questions.\n'+ 
            'Kind regards,\n'+
            
            'Gabriella Marino\n'+
            'Contract Specialist\n'+
            'United States Secret Service\n'+
            'Washington, DC\n'+
            '202- 406-9714\n';
        email.fromAddress ='test@test.com';
        email.fromName = 'ABC XYZ';
        email.subject = 'RE: Request for Quote: HSSS01-15-Q-0300 A/V and VTC';
        email.toaddresses = new List<String>();
        email.toaddresses.add('eagleii@d-n3hb4l1jktiefjd99s91te1u1fvra7ge5wrm5mlo32z1w9gc2.61-8ji7eae.na34.apex.salesforce.com');             
        
        Messaging.InboundEmail email1  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env1 = new Messaging.InboundEnvelope();
        email1.plainTextBody = 'Good Afternoon,\n'+
            
            'Please find attached Request for Quote (RFQ) HSSS01-15-Q-0300 and its corresponding attachments. Please review the Instructions to Offers very carefully, as there is important information regarding this requirement. In order to be considered for this requirement, a representative from every interested vendor must attend at mandatory site visit September 10, 2015 at 9:00 AM EST. If you wish to attend, please submit a completed SSF 3237: USSS Facility Access Form to the contract specialist at gabriella.marino@associates.usss.dhs.gov NO LATER THAN SEPTEMBER 9, 2015 at 12:00 NOON EST. The site visit will be at the J.J. Rowley Training Center in Laurel, MD. \n'+          
            'Responses are due to the Contract Specialist *no later than September 17,\n2015 at 10:00 AM EST. \n'+
            
            'Please let me know if you have any questions.\n'+ 
            'Kind regards,\n'+
            
            'Gabriella Marino\n'+
            'Contract Specialist\n'+
            'United States Secret Service\n'+
            'Washington, DC\n'+
            '202- 406-9714\n';
        email1.fromAddress ='test@test.com';
        email1.fromName = 'ABC XYZ';
        email1.subject = 'Request for Quote: HSSS01-15-Q-0300 A/V and VTC';
        email1.toaddresses = new List<String>();
        email1.toaddresses.add('eagleii@d-n3hb4l1jktiefjd99s91te1u1fvra7ge5wrm5mlo32z1w9gc2.61-8ji7eae.na34.apex.salesforce.com');  
        
        Messaging.InboundEmail email2  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env2 = new Messaging.InboundEnvelope();
        email2.plainTextBody = 'Good Afternoon,\n'+
            
            'Please find attached Request for Quote (RFQ) HSSS01-15-Q-0300 and its corresponding attachments. Please review the Instructions to Offers very carefully, as there is important information regarding this requirement. In order to be considered for this requirement, a representative from every interested vendor must attend at mandatory site visit September 10, 2015 at 9:00 AM EST. If you wish to attend, please submit a completed SSF 3237: USSS Facility Access Form to the contract specialist at gabriella.marino@associates.usss.dhs.gov NO LATER THAN SEPTEMBER 9, 2015 at 12:00 NOON EST. The site visit will be at the J.J. Rowley Training Center in Laurel, MD. \n'+          
            'Responses are due to the Contract Specialist *no later than September 17,\n2015 at 10:00 AM EST. \n'+
            
            'Please let me know if you have any questions.\n'+ 
            'Kind regards,\n'+
            
            'Gabriella Marino\n'+
            'Contract Specialist\n'+
            'United States Secret Service\n'+
            'Washington, DC\n'+
            '202- 406-9714\n';
        email2.fromAddress ='test1@test.com';
        email2.fromName = 'ABC XYZ';
        email2.subject = 'Request for Quote: HSSS01-15-Q-0300 A/V and VTC';
        email2.toaddresses = new List<String>();
        email2.toaddresses.add('eagleii@d-n3hb4l1jktiefjd99s91te1u1fvra7ge5wrm5mlo32z1w9gc2.61-8ji7eae.na34.apex.salesforce.com');  
        
         
        
        
        EagleIIEmailTemplateHandler eagleIIEmailTemplateHandler = new EagleIIEmailTemplateHandler();
        
        Test.startTest();
        Messaging.InboundEmailResult result = eagleIIEmailTemplateHandler.handleInboundEmail(email, env);
        Messaging.InboundEmailResult result1 = eagleIIEmailTemplateHandler.handleInboundEmail(email1, env1);
        Messaging.InboundEmailResult result2 = eagleIIEmailTemplateHandler.handleInboundEmail(email2, env2);
        Test.stopTest();
        
        List<Task_Order__c> tOrderList = [SELECT Id FROM Task_Order__c];
        System.assertEquals(tOrderList.size(), 1);
    }
    
    public Static TestMethod void testEagleIIEmailTemplateHandler2 (){
        
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Contact con = TestDataGenerator.createContact('TestContact', acc );
        insert con;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('T-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'eagleii@d-n3hb4l1jktiefjd99s91te1u1fvra7ge5wrm5mlo32z1w9gc2.61-8ji7eae.na34.apex.salesforce.com';        
        insert conVehi; 
        
        Messaging.InboundEmail email3  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env3 = new Messaging.InboundEnvelope();
        email3.plainTextBody = null;
        email3.fromAddress ='test1@test.com';
        email3.fromName = 'ABC XYZ';
        email3.subject = 'Request for Quote: HSSS01-15-Q-0300 A/V and VTC';
        email3.toaddresses = new List<String>();
        email3.toaddresses.add('eagleii@d-n3hb4l1jktiefjd99s91te1u1fvra7ge5wrm5mlo32z1w9gc2.61-8ji7eae.na34.apex.salesforce.com');  
        
         EagleIIEmailTemplateHandler eagleIIEmailTemplateHandler = new EagleIIEmailTemplateHandler();
        
        Test.startTest(); 
        try{
            Messaging.InboundEmailResult result3 = eagleIIEmailTemplateHandler.handleInboundEmail(email3, env3);
        }
        catch(Exception e)
        {
           Boolean check = (e.getMessage().contains('Attempt to de-reference a null object'))?true:false;
            System.assertEquals(check, true);
        }
        Test.stopTest();
    }
}