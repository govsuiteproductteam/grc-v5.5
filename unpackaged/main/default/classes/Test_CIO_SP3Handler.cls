@isTest
public with sharing class Test_CIO_SP3Handler {
    public Static TestMethod void CIO_SP3_EmailHandler (){
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Contact con = TestDataGenerator.createContact('TestContact', acc );
        insert con;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('T-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'cio_sp3@bjrnsewhbq18kt4e3qjb53ih0w30tfapiiggxmzooymtpyaiz.61-zhsweao.na34.apex.salesforce.com';        
        insert conVehi;    
        //Schema.SObjectType.sObjectApiName.getRecordTypeInfosByName().get('Record Type Name').getRecordTypeId();
        Messaging.InboundEmail email  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        email.plainTextBody = 'RFI-34112-O Released.'+
            'RFI-34112-O titled RFI - DOC-SSI-16-RFI-VAT0204 from Department of Commerce, Patent & Trademark Office, Office of the Chief Financial Officer has been released. '+
            
            'Responses are due by 02/11/2016 2:00 PM Eastern Time. '+
            
            'Please visit e-GOS at https://cio.egos.nih.gov for more information. '+
            '______________________________________________________ '+
            
            'nitaac.nih.gov'+
            'Phone: 1.888.773.6542'+
            'NITAACsupport@nih.gov'+
            
            '6011 Executive Boulevard, Suite 501'+
            'Rockville, Maryland 20852 ';
        email.fromAddress ='test@test.com';
        email.subject = 'RFI-34112-O test1111 Cancelled';
        email.toaddresses = new List<String>();
        email.toaddresses.add('cio_sp3@bjrnsewhbq18kt4e3qjb53ih0w30tfapiiggxmzooymtpyaiz.61-zhsweao.na34.apex.salesforce.com');             
        
        Messaging.InboundEmail email1  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env1 = new Messaging.InboundEnvelope();
        email1.plainTextBody = 'RFI-34112-O Released.'+
            'RFI-34112-O titled RFI - DOC-SSI-16-RFI-VAT0204 from Department of Commerce, Patent & Trademark Office, Office of the Chief Financial Officer has been released. '+
            
            'Responses are due by 02/11/2016 2:00 PM Eastern Time. '+
            
            'Please visit e-GOS at https://cio.egos.nih.gov for more information. '+
            '______________________________________________________ '+
            
            'nitaac.nih.gov'+
            'Phone: 1.888.773.6542'+
            'NITAACsupport@nih.gov'+
            
            '6011 Executive Boulevard, Suite 501'+
            'Rockville, Maryland 20852 ';
        email1.fromAddress ='test@test.com';
        email1.subject = 'RE: RFI-34112-O test1111 Cancelled';
        email1.toaddresses = new List<String>();
        email1.toaddresses.add('cio_sp3@bjrnsewhbq18kt4e3qjb53ih0w30tfapiiggxmzooymtpyaiz.61-zhsweao.na34.apex.salesforce.com');
        
        Messaging.InboundEmail email2  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env2 = new Messaging.InboundEnvelope();
        email2.plainTextBody = 'RFI-34112-O Released.'+
            'RFI-34112-O titled RFI - DOC-SSI-16-RFI-VAT0204 from Department of Commerce, Patent & Trademark Office, Office of the Chief Financial Officer has been released. '+
            
            'Responses are due by 13/13/2016 2:00 PM Eastern Time. '+
            
            'Please visit e-GOS at https://cio.egos.nih.gov for more information. '+
            '______________________________________________________ '+
            
            'nitaac.nih.gov'+
            'Phone: 1.888.773.6542'+
            'NITAACsupport@nih.gov'+
            
            '6011 Executive Boulevard, Suite 501'+
            'Rockville, Maryland 20852 ';
        email2.fromAddress ='test1@test.com';
        email2.subject = 'Fwd: RFI-34112-O test1111 Cancelled';
        email2.toaddresses = new List<String>();
        email2.toaddresses.add('cio_sp3@bjrnsewhbq18kt4e3qjb53ih0w30tfapiiggxmzooymtpyaiz.61-zhsweao.na34.apex.salesforce.com');
        
        Messaging.InboundEmail email3  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env3 = new Messaging.InboundEnvelope();
        email3.plainTextBody = 'RFI-34333 Released.'+
            'RFI-34112-O titled RFI - DOC-SSI-16-RFI-VAT0204 from Department of Commerce, Patent & Trademark Office, Office of the Chief Financial Officer has been released. '+
            
            'Responses are due by 02/11/2016 2:00 PM Eastern Time.\n'+
            
            'Please visit e-GOS at https://cio.egos.nih.gov for more information. '+
            '______________________________________________________ '+
            
            'nitaac.nih.gov'+
            'Phone: 1.888.773.6542'+
            'NITAACsupport@nih.gov'+
            
            '6011 Executive Boulevard, Suite 501'+
            'Rockville, Maryland 20852 ';
        email3.fromAddress ='test@test.com';
        email3.subject = 'RFI 34112 test1111 Cancelled';
        email3.toaddresses = new List<String>();
        email3.toaddresses.add('cio_sp3@bjrnsewhbq18kt4e3qjb53ih0w30tfapiiggxmzooymtpyaiz.61-zhsweao.na34.apex.salesforce.com');
        
        Messaging.InboundEmail email4  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env4 = new Messaging.InboundEnvelope();
        email4.plainTextBody = 'An amendment for RFI-34119-O titled RFI - RFI1063421 Audit Resolution and Tracking System from Executive Branch, Department of Health and Human Services, Office of the Secretary has been released. Please visit e-GOS at https://cio.egos.nih.gov to view the Amendment. Amendment Description: The due date for RFI submission has changed:From: Wednesday February 24, 2016 at 3:00pm To: Wednesday March 2, 2016 at 3:00pm\n Amendment number: 1 ______________________________________________________ nitaac.nih.gov Phone: 1.888.773.6542 NITAACsupport@nih.gov 6011 Executive Boulevard, Suite 501 Rockville, Maryland 20852';
        email4.fromAddress ='test@test.com';
        email4.subject = '  Amendment for RFI-34115-O Released.';
        email4.toaddresses = new List<String>();
        email4.toaddresses.add('cio_sp3@bjrnsewhbq18kt4e3qjb53ih0w30tfapiiggxmzooymtpyaiz.61-zhsweao.na34.apex.salesforce.com');
        
        /*Messaging.InboundEmail email5  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env5 = new Messaging.InboundEnvelope();
        email5.plainTextBody = 'An amendment for RFI-34119-O titled RFI - RFI1063421 Audit Resolution and Tracking System from Executive Branch, Department of Health and Human Services, Office of the Secretary has been released. Please visit e-GOS at https://cio.egos.nih.gov to view the Amendment. Amendment Description: The due date for RFI submission has changed:From: Wednesday February 24, 2016 at 3:00pm To: Wednesday March 2, 2016 at 3:00pm\n Amendment number: 1 ______________________________________________________ nitaac.nih.gov Phone: 1.888.773.6542 NITAACsupport@nih.gov 6011 Executive Boulevard, Suite 501 Rockville, Maryland 20852';
        email5.fromAddress ='test@test.com';
        email5.subject = '  Amendment for Released.';
        email5.toaddresses = new List<String>();
        email5.toaddresses.add('cio_sp3@bjrnsewhbq18kt4e3qjb53ih0w30tfapiiggxmzooymtpyaiz.61-zhsweao.na34.apex.salesforce.com');*/
        
        CIO_SP3Handler cioEmailHandler = new CIO_SP3Handler();
        
        Test.startTest();
        Messaging.InboundEmailResult result = cioEmailHandler.handleInboundEmail(email, env);
        Messaging.InboundEmailResult result1 = cioEmailHandler.handleInboundEmail(email1, env1);
        Messaging.InboundEmailResult result2 = cioEmailHandler.handleInboundEmail(email2, env2);
        Messaging.InboundEmailResult result3 = cioEmailHandler.handleInboundEmail(email3, env3);
        Messaging.InboundEmailResult result4 = cioEmailHandler.handleInboundEmail(email4, env4);
        //Messaging.InboundEmailResult result5 = cioEmailHandler.handleInboundEmail(email5, env5);
        Test.stopTest();       
        
        List<Task_Order__c> tOrderList = [SELECT Id FROM Task_Order__c];
        System.assertEquals(tOrderList.size(), 3);
    }
}