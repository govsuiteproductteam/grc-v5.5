global with sharing class TM_TaskOrderStageViewController{

    public Set<String> stageNameSet{get;set;}
    public List<String> stageNameList{get;set;}
    public Task_Order__c taskOrder{get;set;}
    
    public TM_TaskOrderStageViewController(ApexPages.StandardController sc){
      if(Schema.sObjectType.Task_Order__c.isAccessible()){ 
        if(!test.isRunningTest())
        sc.addFields(new String[]{'Stage__C'});
        init();
        taskOrder = (Task_Order__c)sc.getRecord();
      }
    }
    
    /*Load members*/
    public void init(){
        stageNameSet = new Set<String>();
        stageNameList = new List<String>();
        fetchStageName();    
    }
    
    /*Get values of Stage picklist in stageNameSet to display in Vf page*/
    public void fetchStageName(){
    
            Schema.DescribeFieldResult fieldResult = Task_Order__c.Stage__c.getDescribe();
            List<Schema.PicklistEntry> pickListEntryList = fieldResult.getPicklistValues();
            for(Schema.PicklistEntry pickListEntry : pickListEntryList)
            {
                stageNameSet.add(pickListEntry.label);
                system.debug('stageNameSet==='+stageNameSet);
            }
            stageNameList.add(''); //Blank value to first element to adjust arrow tab css
            stageNameList.addAll(stageNameSet);
            stageNameList.add(''); //Blank value to first element to adjust arrow tab css
       
    }
}