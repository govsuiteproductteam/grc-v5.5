@RestResource(urlMapping='/GSAConnectDeleteDuplicate/*')
global with sharing class GSAConnectDeleteDuplicate{    
    @HttpPost
    global static String doPost()
    {       
        try{
            System.debug('Inside doPost');
            RestRequest req = RestContext.request;
            String strBody = req.requestBody.toString();
            List<String> taskOrderNameList = new List<String>();
            for(AggregateResult ar :  [select name,TM_TOMA__Contract_Vehicle__c,OwnerId from TM_TOMA__Task_Order__c WHERE CreatedDate=TODAY group by name,TM_TOMA__Contract_Vehicle__c,OwnerId having count(name)>1]){           
                taskOrderNameList.add((String)ar.get('name'));
            }
            List<Task_Order__c> taskOrderList = new List<Task_Order__c>();
            Map<String,Task_Order__c> duplicateTOMap = new Map<String,Task_Order__c>();
            for(Task_Order__c to : [SELECT Id,Name,Contract_Vehicle__c FROM Task_Order__c WHERE Name In : taskOrderNameList]){
                duplicateTOMap.put(to.name+to.Contract_Vehicle__c, to);                               
            }
            for(String Key : duplicateTOMap.keyset()){
                taskOrderList.add(duplicateTOMap.get(Key));
            }
            Delete taskOrderList;
            
            deleteDuplicateContractMod();
            
        }
        catch(Exception ee){}
        
        
        return '';
    } 
    
    private static void deleteDuplicateContractMod(){       
        List<String> modificationList = new List<String>();
        List<String> taskOrderIdList = new List<String>();
        for(AggregateResult ar :  [select Modification_Name__c,TaskOrder__c,CreatedById from Contract_Mods__c WHERE CreatedDate=TODAY AND TM_Toma__Modification_Name__c!= null  group by Modification_Name__c,TaskOrder__c,CreatedById having count(name)>1]){
            modificationList.add((String)ar.get('TM_Toma__Modification_Name__c'));
            taskOrderIdList.add((String)ar.get('TM_Toma__TaskOrder__c'));            
        }
        System.debug(''+modificationList);
        System.debug(''+taskOrderIdList);
        Map<String,Contract_Mods__c> duplicateCMMap = new Map<String,Contract_Mods__c>();
        for(Contract_Mods__c cm : [SELECT Id,Modification_Name__c,TaskOrder__c FROM Contract_Mods__c WHERE TaskOrder__c IN:taskOrderIdList AND Modification_Name__c IN:modificationList ]){
            duplicateCMMap.Put(cm.Modification_Name__c+''+cm.TaskOrder__c,cm);
        }
        system.debug(''+duplicateCMMap);
        List<Contract_Mods__c> contractModList = new List<Contract_Mods__c>();
        for(String key : duplicateCMMap.keySet()){
            contractModList.add(duplicateCMMap.get(key));
        }
        system.debug(''+contractModList);
        delete contractModList;
    }
    
}