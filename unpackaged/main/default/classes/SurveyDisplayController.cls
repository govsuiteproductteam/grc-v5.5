/*
Developer    :  Avanti
Description  :  Apex Controller which display all the questions regarding survey.
VF Page      :  SurveyDisplayPage
*/

global with sharing class SurveyDisplayController 
{       
    public Id SurveyId {get;set;}
    public Survey__c survey{get;set;}
    Public List<Survey__c> surveyList{get;set;}
    Public List<Survey__c> surveySendList{get;set;}
    public List<Question__c> queList{get;set;}
    public List<QuestionWrapper> question_WraList{get;set;}
    public boolean isSend{get;set;}
    public String emailTemplateStr {get;set;}
    //Questionaries_Email_Template__c variables
    //public String emailTempleteIdsString{get;set;}// String contaning the ids seprated by '~' Charector
    public List<Questionaries_Email_Template__c> qetList{get;set;}
    public List<EmailTemplate> EmailTemplateList{get;set;}
    public Id emailTempIdDelete{get;set;}
    //BootStrap Model
    public String templeteId{get;set;}
    public Questionaries_Email_Template__c qetTemp{get;set;}
    public String isValidLiecence{get;set;}
    //end
    
    public SurveyDisplayController(ApexPages.StandardController controller){
        if(Schema.sObjectType.Survey__c.isAccessible() && Schema.sObjectType.Question__c.isAccessible() && Schema.sObjectType.Questionaries_Email_Template__c.isAccessible() &&  Schema.sObjectType.Survey__c.isAccessible() && Schema.sObjectType.EmailTemplate.isAccessible()){    
            isValidLiecence = LincenseKeyHelper.checkLicensePermitionForFedTOM();
            if(isValidLiecence == 'Yes'){
                survey= new survey__c();
                qetTemp = new Questionaries_Email_Template__c();// Questionaries_Email_Template__c temprary object 
                this.survey= (Survey__c)Controller.getRecord();      
                displayData();
                setQetIdEmailTempleteWrapperList();
            }
        }
    }
    
    //Save Questionaries_Email_Template__c
    public void saveQET(){
        if(Schema.sObjectType.Questionaries_Email_Template__c.isAccessible()){
            try{
                Questionaries_Email_Template__c qet = new Questionaries_Email_Template__c();
                qet.Teaming_Request_Questionnaire__c = SurveyId;
                system.debug('emailTemplateStr===='+ emailTemplateStr);
                if(emailTemplateStr != null){
                    emailTemplateStr = (Id)String.escapeSingleQuotes(templeteId.trim());
                    qet.Email_Templete__c = emailTemplateStr;
                }        
                //insert qet;
                DMLManager.insertAsUser(qet);
                
            }catch(Exception e){}
            setQetIdEmailTempleteWrapperList();
            qetTemp = Null;
            qetTemp = new Questionaries_Email_Template__c();
            templeteId = '';
        }
    }
    
    // getDelete() method used  to delete respective email template.
    public void qetDelete(){
        if(Schema.sObjectType.Questionaries_Email_Template__c.isAccessible()){
            if(emailTempIdDelete!=Null && ''+emailTempIdDelete!=''){
                try{
                    //  Delete [SELECT Id FROM Questionaries_Email_Template__c WHERE Email_Templete__c = :emailTempIdDelete AND Teaming_Request_Questionnaire__c = :SurveyId LIMIT 1];
                    DMLManager.deleteAsUser([SELECT Id FROM Questionaries_Email_Template__c WHERE Email_Templete__c = :emailTempIdDelete AND Teaming_Request_Questionnaire__c = :SurveyId LIMIT 1]);
                    emailTempIdDelete='';
                }catch(Exception ex){}
                setQetIdEmailTempleteWrapperList();
            }
        }
    }
    
    // displayData() method used to display all questions along with options.
    public void displayData(){
        if(Schema.sObjectType.Survey__c.isAccessible() && Schema.sObjectType.Question__c.isAccessible()  ){
            SurveyId = ApexPages.currentPage().getParameters().get('id');
            
            survey = new Survey__c(); 
            surveyList =[SELECT Id,Name,Description__c , isSend__c, isActive__c  FROM Survey__c WHERE Id=:SurveyId ];
            
            if(surveyList.size()>0)
            {
                survey = surveyList[0];
                
            }
            if(survey.Id!=null)
            {
                queList = new List<Question__c>();
                queList = [SELECT Id,Question__c,Question_Type__c,Option_1__c,Option_2__c,Option_3__c,Option_4__c,Option_5__c,Option_6__c, 
                           Option_7__c,Option_8__c ,Option_9__c ,Is_Mandatory__c ,Option_10__c  FROM Question__c WHERE Survey__r.id=:survey.id Order by sequence__c ASC Limit 1000 ];
                
            }
            if(queList != null && queList.size()>0)
            {
                question_WraList= new List<QuestionWrapper>();                   
                for(Question__c que : queList )
                {
                    QuestionWrapper qw = new QuestionWrapper (que);
                    
                    qw.srNo = question_WraList.size()+1;
                    question_WraList.add(qw);
                }
            }
        }   
    }
    
    private void setQetIdEmailTempleteWrapperList(){
        //emailTempleteIdsString = '';
        System.debug('SurveyId '+SurveyId);
        qetList = [SELECT Email_Templete__c From Questionaries_Email_Template__c WHERE Teaming_Request_Questionnaire__c=:SurveyId];
        if(EmailTemplateList == Null){
            EmailTemplateList = new List<EmailTemplate>();
        }else{
            EmailTemplateList.clear();
        }               
        SET<Id> tempIdList = new SET<id>();
        for(Questionaries_Email_Template__c temp : qetList){
            tempIdList.add((Id)temp.Email_Templete__c);
        }
        System.debug('tempIdList '+tempIdList);
        for(EmailTemplate eTemp : [SELECT Id, Name FROM EmailTemplate WHERE Id In :tempIdList]){
            //emailTempleteIdsString = emailTempleteIdsString+'~'+eTemp.Id;
            EmailTemplateList.add(eTemp);
        }
        System.debug('EmailTemplateList '+EmailTemplateList);
    }
    
    // myEdit() method is redirected back to the SurveyPage for editing questions with the respective options.
    public PageReference myEdit(){
        
        PageReference returnPage = new PageReference('/apex/SurveyPage?id='+ SurveyId);
        returnPage.setRedirect(true);
        return returnPage ;
    }
    
    // cloneSurvey() method is redirected back to the SurveyPage to clone the Survey.
    public PageReference cloneSurvey(){
        
        PageReference pgclone = new PageReference ('/apex/SurveyPage?id='+SurveyId+'&clone=1');
        pgclone.setRedirect(true);
        return pgclone ;
    }  
    
    Public PageReference Cancel(){
        
        return new Pagereference('/'+ (''+SurveyId).substring(0,3) );
    }  
    
    
    
    
    /************************************************ Wrapper Classes *********************************************************/
    public class Surveywrapper
    {
        public Survey__c survey{get;set;}
        public Survey__c sur{get;set;}
    }
    
    public class OptionWrapper
    {
        public String opt{get; set;}
        public Integer Sr{get; set;}
        
        public OptionWrapper(String str , Integer n){
            this.opt=str;
            this.Sr=n;
        }
    }  
    
    public class QuestionWrapper{
        
        public Question__c question {get;set;}
        public integer srNo {get; set;}
        public list<OptionWrapper> lstOptions{get;set;}
        
        
        public QuestionWrapper (Question__c q){
            this.question = q;
            Integer i=1;
            lstOptions = new list<OptionWrapper>();
            while(i <= 10 && question.get('Option_'+i+'__c')!=null){
                String optStr = (String)question.get('Option_'+i+'__c');
                if(optStr.length() > 0){
                    
                    lstOptions.add(new OptionWrapper( (String)question.get('Option_'+i+'__c'),i));
                }
                i++;
            }
            
        }
    }
    
    /**************************************************************************************************************/ 
    
    /*public class QetIdEmailTempleteWrapper{
public Id qetId{get;set;}
public EmailTemplate empTpl{get;set;}

public QetIdEmailTempleteWrapper(Id qetId, EmailTemplate empTpl){
this.qetId = qetId;
this.empTpl = empTpl;
}
}*/
    
}