public with sharing class GSAParseHelper{
    public static void insertContractMods(Task_Order__c newTaskOrder,Task_Order__c oldTaskOrder){
        if(Schema.sObjectType.Task_Order__c.isAccessible() && Schema.sObjectType.Contract_Vehicle__c.isAccessible() && Schema.sObjectType.Contract_Mods__c.isAccessible()){ 
        List<Task_Order__c> taskOrderList = [SELECT Id,Name,Task_Order_Title__c ,Release_Date__c ,Due_Date__c,Proposal_Due_DateTime__c ,Delivery__c ,Description__c ,Reference__c ,
                                             Category__c,Category_Backend__c ,Buyer__c ,Customer_Agency__c ,Contract_Vehicle__c,Task_Order_Contracting_Activity__c,Individual_Receiving_Shipment__c
                                             FROM Task_Order__c where Name =: newTaskOrder.id];
                                             
        if(taskOrderList.size() > 0 ){
            newTaskOrder = taskOrderList[0];
        }
        if(newTaskOrder != null && oldTaskOrder != null){
            Contract_Mods__c cm = new Contract_Mods__c();
            cm.RecordTypeId = Schema.SObjectType.Contract_Mods__c.getRecordTypeInfosByName().get('Update').getRecordTypeId();
            
            //cm.Task_Order_Number__c = taskOrder.Name;
            cm.TaskOrder__c = newTaskOrder.Id;
            Boolean flag = false;
            
            System.debug('Task_Order_Title__c = '+ oldTaskOrder.Task_Order_Title__c );
            System.debug('Task_Order_Title__c = '+ newTaskOrder.Task_Order_Title__c);
            if(newTaskOrder.Task_Order_Title__c != oldTaskOrder.Task_Order_Title__c && !(newTaskOrder.Task_Order_Title__c == null && oldTaskOrder.Task_Order_Title__c == '')
                                               && !(newTaskOrder.Task_Order_Title__c == '' && oldTaskOrder.Task_Order_Title__c == null)){
                cm.Task_Order_Title__c = oldTaskOrder.Task_Order_Title__c;
                cm.New_Task_Order_Title__c = newTaskOrder.Task_Order_Title__c ;
                System.debug('In Task Order Title');
                flag = true;
            }
            System.debug('Due_Date__c = '+ oldTaskOrder.Due_Date__c );
            System.debug('Due_Date__c = '+ newTaskOrder.Due_Date__c );
            if(newTaskOrder.Due_Date__c != oldTaskOrder.Due_Date__c){
                cm.Due_Date__c = oldTaskOrder.Due_Date__c;
                cm.New_Due_Date__c = newTaskOrder.Due_Date__c;
                System.debug('In Proposal Due Date');
                flag = true;
            }
            
            System.debug('Proposal_Due_DateTime__c= '+ oldTaskOrder.Proposal_Due_DateTime__c);
            System.debug('Proposal_Due_DateTime__c= '+ newTaskOrder.Proposal_Due_DateTime__c);
            if(newTaskOrder.Proposal_Due_DateTime__c!= oldTaskOrder.Proposal_Due_DateTime__c){
                cm.Proposal_Due_Date_Time__c= oldTaskOrder.Proposal_Due_DateTime__c;
                cm.New_Proposal_Due_Date_Time__c= newTaskOrder.Proposal_Due_DateTime__c;
                System.debug('In Proposal Due Date');
                flag = true;
            }
            
            System.debug('Release_Date__c = '+ oldTaskOrder.Release_Date__c );
            System.debug('Release_Date__c = '+ newTaskOrder.Release_Date__c );
            if(newTaskOrder.Release_Date__c!= oldTaskOrder.Release_Date__c){
                cm.Release_Date__c = oldTaskOrder.Release_Date__c;
                cm.New_Release_Date__c  = newTaskOrder.Release_Date__c;
                System.debug('In Release Date');
                flag = true;
            }
            
            //System.debug('Description__c  = '+ oldTaskOrder.Description__c );
            //System.debug('Description__c Len = '+ oldTaskOrder.Description__c.length());
            //System.debug('Description__c  = '+ newTaskOrder.Description__c );
            //System.debug('Description__c Len = '+ newTaskOrder.Description__c.length());            
            if( newTaskOrder.Description__c != oldTaskOrder.Description__c && !(newTaskOrder.Description__c == null && oldTaskOrder.Description__c == '')
                                               && !(newTaskOrder.Description__c == '' && oldTaskOrder.Description__c == null)){
                cm.Description__c= oldTaskOrder.Description__c;
                cm.New_Description__c= newTaskOrder.Description__c;
                System.debug('In Description');
                flag = true;
            }
            
            System.debug('Delivery__c = '+ oldTaskOrder.Delivery__c);
            System.debug('Delivery__c = '+ newTaskOrder.Delivery__c);
            if( newTaskOrder.Delivery__c != oldTaskOrder.Delivery__c  && !(newTaskOrder.Delivery__c == null && oldTaskOrder.Delivery__c == '')
                                               && !(newTaskOrder.Delivery__c == '' && oldTaskOrder.Delivery__c == null)){
                cm.Delivery__c= oldTaskOrder.Delivery__c;
                cm.New_Delivery__c= newTaskOrder.Delivery__c;
                System.debug('In Delivery__c');
                flag = true;
            }
             
            System.debug('Reference__c = '+ oldTaskOrder.Reference__c);
            System.debug('Reference__c = '+ newTaskOrder.Reference__c);
            if( newTaskOrder.Reference__c != oldTaskOrder.Reference__c && !(newTaskOrder.Reference__c == null && oldTaskOrder.Reference__c == '')
                                               && !(newTaskOrder.Reference__c == '' && oldTaskOrder.Reference__c == null)){
                cm.Reference__c= oldTaskOrder.Reference__c;
                cm.New_Reference__c = newTaskOrder.Reference__c;
                System.debug('In Reference__c ');
                flag = true;
            }
            
            System.debug('Category__c = '+ oldTaskOrder.Category__c);
            System.debug('Category__c = '+ newTaskOrder.Category__c);
            if( newTaskOrder.Category__c != oldTaskOrder.Category__c && !(newTaskOrder.Category__c == null && oldTaskOrder.Category__c == '')
                                               && !(newTaskOrder.Category__c == '' && oldTaskOrder.Category__c == null)){
                cm.Category__c= oldTaskOrder.Category__c;
                cm.New_Category__c = newTaskOrder.Category__c;
                System.debug('In Category__c');
                flag = true;
            }
            System.debug('Category_Backend__c = '+ oldTaskOrder.Category_Backend__c );
            System.debug('Category_Backend__c = '+ newTaskOrder.Category_Backend__c );
            if( newTaskOrder.Category_Backend__c != oldTaskOrder.Category_Backend__c && !(newTaskOrder.Category_Backend__c == null && oldTaskOrder.Category_Backend__c == '')
                                               && !(newTaskOrder.Category_Backend__c == '' && oldTaskOrder.Category_Backend__c == null)){
                cm.Category_Backend__c = oldTaskOrder.Category_Backend__c ;
                cm.New_Category_Backend__c  = newTaskOrder.Category_Backend__c ;
                System.debug('In Category_Backend__c ');
                flag = true;
            }
            
            System.debug('Buyer__c = '+ oldTaskOrder.Buyer__c);
            System.debug('Buyer__c = '+ newTaskOrder.Buyer__c);
            if( newTaskOrder.Buyer__c != oldTaskOrder.Buyer__c){
                cm.Buyer__c= oldTaskOrder.Buyer__c;
                cm.New_Buyer__c = newTaskOrder.Buyer__c;
                System.debug('In Buyer__c');
                flag = true;
            }
            
            System.debug('Customer_Agency__c = '+ oldTaskOrder.Customer_Agency__c);
            System.debug('Customer_Agency__c = '+ newTaskOrder.Customer_Agency__c);
            if( newTaskOrder.Customer_Agency__c != oldTaskOrder.Customer_Agency__c){
                cm.Customer_Agency__c= oldTaskOrder.Customer_Agency__c;
                cm.New_Customer_Agency__c = newTaskOrder.Customer_Agency__c;
                System.debug('In Customer_Agency__c');
                flag = true;
            }
            
            System.debug('Task_Order_Contracting_Activity__c = '+ oldTaskOrder.Task_Order_Contracting_Activity__c );
            System.debug('New Task_Order_Contracting_Activity__c = '+ newTaskOrder.Task_Order_Contracting_Activity__c );
            if( newTaskOrder.Task_Order_Contracting_Activity__c != oldTaskOrder.Task_Order_Contracting_Activity__c && !(newTaskOrder.Task_Order_Contracting_Activity__c == null && oldTaskOrder.Task_Order_Contracting_Activity__c == '')
                                               && !(newTaskOrder.Task_Order_Contracting_Activity__c == '' && oldTaskOrder.Task_Order_Contracting_Activity__c == null)){
                cm.Task_Order_Contracting_Activity__c = oldTaskOrder.Task_Order_Contracting_Activity__c ;
                cm.New_Task_Order_Contracting_Activity__c  = newTaskOrder.Task_Order_Contracting_Activity__c ;
                System.debug('In Task_Order_Contracting_Activity__c ');
                flag = true;
            }
            
            System.debug('Individual_Receiving_Shipment__c = '+ oldTaskOrder.Individual_Receiving_Shipment__c );
            System.debug('New Individual_Receiving_Shipment__c = '+ newTaskOrder.Individual_Receiving_Shipment__c );
            if( newTaskOrder.Individual_Receiving_Shipment__c != oldTaskOrder.Individual_Receiving_Shipment__c && !(newTaskOrder.Individual_Receiving_Shipment__c == null && oldTaskOrder.Individual_Receiving_Shipment__c == '')
                                               && !(newTaskOrder.Individual_Receiving_Shipment__c == '' && oldTaskOrder.Individual_Receiving_Shipment__c == null)){
                cm.Individual_Receiving_Shipment__c = oldTaskOrder.Individual_Receiving_Shipment__c ;
                cm.New_Individual_Receiving_Shipment__c  = newTaskOrder.Individual_Receiving_Shipment__c ;
                System.debug('In Individual_Receiving_Shipment__c ');
                flag = true;
            }
            
          
            if(flag &&  cm.TaskOrder__c != null){
            
                System.debug('Insert Contract Modes');
                DMLManager.insertAsUser(cm);
            }
        }
        }
    }   
    
    public static void insertContractModForVoa(Task_Order__c newTaskOrder,Task_Order__c oldTaskOrder){
        //List<Task_Order__c> taskOrderList = [SELECT Id,Name,Task_Order_Title__c ,Release_Date__c ,Due_Date__c,Proposal_Due_DateTime__c ,Delivery__c ,Description__c ,Reference__c ,
        //                                            Category__c,Category_Backend__c ,Buyer__c ,Customer_Agency__c ,Contract_Vehicle__c  FROM Task_Order__c where Name =: newTaskOrder.id];
                                             
        //if(taskOrderList.size() > 0 ){
        //    newTaskOrder = taskOrderList[0];
        //}
       if(Schema.sObjectType.Task_Order__c.isAccessible() && Schema.sObjectType.Contract_Vehicle__c.isAccessible() && Schema.sObjectType.Contract_Mods__c.isAccessible()){  
        if(newTaskOrder != null && oldTaskOrder != null){
            Contract_Mods__c cm = new Contract_Mods__c();
            cm.RecordTypeId = Schema.SObjectType.Contract_Mods__c.getRecordTypeInfosByName().get('Update').getRecordTypeId();
            
            //cm.Task_Order_Number__c = taskOrder.Name;
            cm.TaskOrder__c = newTaskOrder.Id;
            Boolean flag = false;
            
            /*System.debug('Name = '+ oldTaskOrder.Name );
            System.debug('Name = '+ newTaskOrder.Name );
            if(newTaskOrder.Name != oldTaskOrder.Name && !(newTaskOrder.Name == null && oldTaskOrder.Name == '')
                                               && !(newTaskOrder.Name == '' && oldTaskOrder.Name == null)){
                cm.Name__c = oldTaskOrder.Name ;
                cm.New_Name__c = newTaskOrder.Name ;
                System.debug('In Task Order Title');
                flag = true;
            }*/
            
            System.debug('Task_Order_Title__c = '+ oldTaskOrder.Task_Order_Title__c );
            System.debug('Task_Order_Title__c = '+ newTaskOrder.Task_Order_Title__c);
            if(newTaskOrder.Task_Order_Title__c != oldTaskOrder.Task_Order_Title__c && !(newTaskOrder.Task_Order_Title__c == null && oldTaskOrder.Task_Order_Title__c == '')
                                               && !(newTaskOrder.Task_Order_Title__c == '' && oldTaskOrder.Task_Order_Title__c == null)){
                cm.Task_Order_Title__c = oldTaskOrder.Task_Order_Title__c;
                cm.New_Task_Order_Title__c = newTaskOrder.Task_Order_Title__c ;
                System.debug('In Task Order Title');
                flag = true;
            }
            
            System.debug('RTEP_Status__c = '+ oldTaskOrder.RTEP_Status__c);
            System.debug('RTEP_Status__c = '+ newTaskOrder.RTEP_Status__c);
            if(newTaskOrder.RTEP_Status__c != oldTaskOrder.RTEP_Status__c  && !(newTaskOrder.RTEP_Status__c == null && oldTaskOrder.RTEP_Status__c == '')
                                               && !(newTaskOrder.RTEP_Status__c == '' && oldTaskOrder.RTEP_Status__c == null)){
                cm.RTEP_Status__c= oldTaskOrder.RTEP_Status__c ;
                cm.New_RTEP_Status__c = newTaskOrder.RTEP_Status__c;
                System.debug('In RTEP_Status__c ');
                flag = true;
            }
            
            System.debug('Due_Date__c = '+ oldTaskOrder.Due_Date__c );
            System.debug('Due_Date__c = '+ newTaskOrder.Due_Date__c );
            if(newTaskOrder.Due_Date__c != oldTaskOrder.Due_Date__c){
                cm.Due_Date__c = oldTaskOrder.Due_Date__c;
                cm.New_Due_Date__c = newTaskOrder.Due_Date__c;
                System.debug('In Proposal Due Date');
                flag = true;
            }
            
           /* System.debug('Proposal_Due_DateTime__c= '+ oldTaskOrder.Proposal_Due_DateTime__c);
            System.debug('Proposal_Due_DateTime__c= '+ newTaskOrder.Proposal_Due_DateTime__c);
            if(newTaskOrder.Proposal_Due_DateTime__c!= oldTaskOrder.Proposal_Due_DateTime__c){
                cm.Proposal_Due_Date_Time__c= oldTaskOrder.Proposal_Due_DateTime__c;
                cm.New_Proposal_Due_Date_Time__c= newTaskOrder.Proposal_Due_DateTime__c;
                System.debug('In Proposal Due Date');
                flag = true;
            }*/
            
            System.debug('Release_Date__c = '+ oldTaskOrder.Release_Date__c );
            System.debug('Release_Date__c = '+ newTaskOrder.Release_Date__c );
            if(newTaskOrder.Release_Date__c!= oldTaskOrder.Release_Date__c){
                cm.Release_Date__c = oldTaskOrder.Release_Date__c;
                cm.New_Release_Date__c  = newTaskOrder.Release_Date__c;
                System.debug('In Release Date');
                flag = true;
            }
            
            System.debug('Description__c  = '+ oldTaskOrder.Description__c );
            System.debug('Description__c Len = '+ oldTaskOrder.Description__c.length());
            System.debug('Description__c  = '+ newTaskOrder.Description__c );
            System.debug('Description__c Len = '+ newTaskOrder.Description__c.length());
            if(newTaskOrder.Description__c.equals(oldTaskOrder.Description__c)){
                System.debug('Inside here my bro');
            }
            if(newTaskOrder.Description__c != oldTaskOrder.Description__c && !(newTaskOrder.Description__c == null && oldTaskOrder.Description__c == '')
                                               && !(newTaskOrder.Description__c == '' && oldTaskOrder.Description__c == null)){
                cm.Description__c= oldTaskOrder.Description__c;
                cm.New_Description__c= newTaskOrder.Description__c;
                System.debug('In Description');
                flag = true;
            }
            
            System.debug('Requiring_Activity__c = '+ oldTaskOrder.Requiring_Activity__c);
            System.debug('Requiring_Activity__c = '+ newTaskOrder.Requiring_Activity__c);
            if( newTaskOrder.Requiring_Activity__c != oldTaskOrder.Requiring_Activity__c && !(newTaskOrder.Requiring_Activity__c == null && oldTaskOrder.Requiring_Activity__c == '')
                                               && !(newTaskOrder.Requiring_Activity__c == '' && oldTaskOrder.Requiring_Activity__c == null)){
                cm.Requiring_Activity__c= oldTaskOrder.Requiring_Activity__c;
                cm.New_Requiring_Activity__c= newTaskOrder.Requiring_Activity__c;
                System.debug('In Requiring_Activity__c ');
                flag = true;
            }
             
            System.debug('Acquisition_Tracker_ID__c = '+ oldTaskOrder.Acquisition_Tracker_ID__c);
            System.debug('Acquisition_Tracker_ID__c = '+ newTaskOrder.Acquisition_Tracker_ID__c);
            if( newTaskOrder.Acquisition_Tracker_ID__c != oldTaskOrder.Acquisition_Tracker_ID__c && !(newTaskOrder.Acquisition_Tracker_ID__c == null && oldTaskOrder.Acquisition_Tracker_ID__c == '')
                                               && !(newTaskOrder.Acquisition_Tracker_ID__c == '' && oldTaskOrder.Acquisition_Tracker_ID__c == null)){
                cm.Acquisition_Tracker_ID__c= oldTaskOrder.Acquisition_Tracker_ID__c;
                cm.New_Acquisition_Tracker_ID__c = newTaskOrder.Acquisition_Tracker_ID__c;
                System.debug('In Acquisition Tracker ID');
                flag = true;
            }
            
            System.debug('Contract_Type__c = '+ oldTaskOrder.Contract_Type__c);
            System.debug('Contract_Type__c = '+ newTaskOrder.Contract_Type__c);
            if( newTaskOrder.Contract_Type__c != oldTaskOrder.Contract_Type__c && !(newTaskOrder.Contract_Type__c == null && oldTaskOrder.Contract_Type__c == '')
                                               && !(newTaskOrder.Contract_Type__c == '' && oldTaskOrder.Contract_Type__c == null)){
                cm.Contract_Type__c = oldTaskOrder.Contract_Type__c;
                cm.New_Contract_Type__c = newTaskOrder.Contract_Type__c;
                System.debug('In Contract_Type__c ');
                flag = true;
            }
            System.debug('Competitive_Procedure__c  = '+ oldTaskOrder.Competitive_Procedure__c);
            System.debug('Competitive_Procedure__c = '+ newTaskOrder.Competitive_Procedure__c);
            if( newTaskOrder.Competitive_Procedure__c != oldTaskOrder.Competitive_Procedure__c ){
                cm.Competitive_Procedure__c= oldTaskOrder.Competitive_Procedure__c ;
                cm.New_Competitive_Procedure__c  = newTaskOrder.Competitive_Procedure__c;
                System.debug('In Competitive_Procedure__c');
                flag = true;
            }
            
            System.debug('Contracting_Officer__c = '+ oldTaskOrder.Contracting_Officer__c);
            System.debug('Contracting_Officer__c = '+ newTaskOrder.Contracting_Officer__c);
            if( newTaskOrder.Contracting_Officer__c != oldTaskOrder.Contracting_Officer__c){
                cm.Contracting_Officer__c= oldTaskOrder.Contracting_Officer__c;
                cm.New_Contracting_Officer__c = newTaskOrder.Contracting_Officer__c;
                System.debug('In New_Contracting_Officer__c ');
                flag = true;
            }
            
            System.debug('Contract_Specialist__c = '+ oldTaskOrder.Contract_Specialist__c);
            System.debug('Contract_Specialist__c = '+ newTaskOrder.Contract_Specialist__c);
            if( newTaskOrder.Contract_Specialist__c!= oldTaskOrder.Contract_Specialist__c){
                cm.Contract_Specialist__c= oldTaskOrder.Contract_Specialist__c;
                cm.New_Contract_Specialist__c = newTaskOrder.Contract_Specialist__c;
                System.debug('In Contract_Specialist__c');
                flag = true;
            }
            
            System.debug('Market_Research_Due_Date__c = '+ oldTaskOrder.Market_Research_Due_Date__c);
            System.debug('Market_Research_Due_Date__c = '+ newTaskOrder.Market_Research_Due_Date__c);
            if(newTaskOrder.Market_Research_Due_Date__c != oldTaskOrder.Market_Research_Due_Date__c){
                cm.Market_Research_Due_Date__c= oldTaskOrder.Market_Research_Due_Date__c;
                cm.New_Market_Research_Due_Date__c = newTaskOrder.Market_Research_Due_Date__c;
                System.debug('In Market_Research_Due_Date__c');
                flag = true;
            }
          
            if(flag &&  cm.TaskOrder__c != null){
            
                System.debug('Insert Contract Modes');
                DMLManager.insertAsUser(cm);
            }
        }
       }
    }   
}