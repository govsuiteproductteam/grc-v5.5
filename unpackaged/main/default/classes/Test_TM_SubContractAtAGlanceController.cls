@IsTest
public class Test_TM_SubContractAtAGlanceController {
 static testMethod void testMethodSubContractAtAGlanceController(){
      Test.startTest();
          List<RecordType> rtypes = [Select Name, Id From RecordType where sObjectType='TM_TOMA__SubContract__c' and isActive=true];
        string recId='';
        if(rtypes.size()>0)
        {
            recId=rtypes[0].Id;
        }
        CLM_AtaGlanceField__c newOppAtAGlance = TestDataGenerator.createAtaGlanceField(recId);
        newOppAtAGlance.Is_Contract__c=false;
        insert newOppAtAGlance;
        CLM_AtaGlanceField__c newOppAtAGlance1 = TestDataGenerator.createAtaGlanceField(recId);
        newOppAtAGlance1.Is_Contract__c=false;
        newOppAtAGlance1.FieldApiName__c='TM_TOMA__Sub_Contract_Number__c';
        newOppAtAGlance1.FieldLabel__c='Sub Contract Number';
        newOppAtAGlance1.Order__c=2;
        insert newOppAtAGlance1; 
        CLM_AtaGlanceField__c newOppAtAGlance3 = TestDataGenerator.createAtaGlanceField(recId);
        newOppAtAGlance3.Is_Contract__c=false;
        newOppAtAGlance3.FieldApiName__c='TM_TOMA__CSC_Contract__c';
        newOppAtAGlance3.FieldLabel__c='Program Name';
        newOppAtAGlance3.Order__c=3;
        insert newOppAtAGlance3; 
        CLM_AtaGlanceField__c newOppAtAGlance2 = TestDataGenerator.createAtaGlanceField(recId);
        newOppAtAGlance2.Is_Contract__c=false;
        newOppAtAGlance2.FieldApiName__c='TestComponent';
        newOppAtAGlance2.FieldLabel__c='TestComponent';
        newOppAtAGlance2.Is_Component__c=true;
        newOppAtAGlance2.Order__c=4;
        insert newOppAtAGlance2; 
        
        Account acc = TestDataGenerator.createAccount('testAccount');
        insert acc;
        Contact con = TestDataGenerator.createContact('Test Contact', acc);   
        insert con;
     
        SubContract__c subConVeh = TestDataGenerator.createSubContract('23745');
        subConVeh.recordTypeId = recId;
        subConVeh.Subcontract_Status_Backend__c= TestDataGenerator.getPicklistValue('TM_TOMA__SubContract__c','TM_TOMA__Subcontract_Status_Backend__c');
        insert subConVeh;
        
       
        PageReference pageRef = Page.TM_SubContractAtAGlancePage;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id', subConVeh.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(subConVeh);
        TM_SubContractAtAGlanceController subContractAtAGlanceController = new TM_SubContractAtAGlanceController(sc);
        System.assertEquals(subContractAtAGlanceController.subConVehicle.Sub_Contract_Number__c, '23745');
        System.assert(subContractAtAGlanceController.conAtAGlanceList.size()>0);
        TM_SubContractAtAGlanceController.getAllFields('TM_TOMA__SubContract__c');
        Test.stopTest();
  }
}