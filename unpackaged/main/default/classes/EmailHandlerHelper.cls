public class EmailHandlerHelper{
    
    public static void insertBinaryAttachment(Task_Order__c taskOrder,Messaging.InboundEmail email){
        Messaging.InboundEmail.BinaryAttachment[] binaryAttachmentList = email.binaryAttachments;
        System.debug('Binary Attachment Size Internal ');
        if(binaryAttachmentList != null && binaryAttachmentList.size() > 0 ){
            System.debug('Binary Attachment Size = '+binaryAttachmentList.size());
            
            List<Attachment> attachmentList = new List<Attachment>();
            for(Messaging.InboundEmail.BinaryAttachment binaryAttachment : binaryAttachmentList ){
                System.debug('Binary Attachment Size Internal 11 ');
                Attachment attach = new Attachment();
                attach.parentId = taskOrder.id;
                attach.body = binaryAttachment.body;
                attach.Name =  binaryAttachment.fileName;
                attachmentList.add(attach);
            }
            System.debug('attachmentList = '+attachmentList.size());
            if(!attachmentList.isEmpty()){
                System.debug('Before Insert');
                DMLManager.insertAsUser(attachmentList);
                System.debug('After Insert');
            }
        }
        
    }
    
    public static  void insertTextAttachment(Task_Order__c taskOrder,Messaging.InboundEmail email){
        Messaging.InboundEmail.TextAttachment[] textAttachmentList = email.textAttachments;
        System.debug('Binary Attachment Size Internal ');
        if(textAttachmentList != null && textAttachmentList .size() > 0 ){
            System.debug('Binary Attachment Size = '+textAttachmentList .size());
            
            List<Attachment> attachmentList = new List<Attachment>();
            for(Messaging.InboundEmail.TextAttachment textAttachment : textAttachmentList ){
                System.debug('Binary Attachment Size Internal 11 ');
                Attachment attach = new Attachment();
                attach.parentId = taskOrder.id;
                attach.body = Blob.valueOf(textAttachment.body);
                attach.Name =  textAttachment.fileName;
                attachmentList.add(attach);
            }
            System.debug('attachmentList = '+attachmentList.size());
            if(!attachmentList.isEmpty()){
                System.debug('Before Insert');
                DMLManager.insertAsUser(attachmentList);
                System.debug('After Insert');
            }
        }
        
    }
    
    public static  void insertActivityHistory(Task_Order__c taskOrder,Messaging.InboundEmail email){
        
        Task newTask = new Task();
        String emailSubject = email.Subject;
        if(emailSubject.length()>255){
            emailSubject = emailSubject.substring(0,245);//245 beacause we are apending "Email : " and the field limit is 255 
        }
        newTask.Subject = 'Email : '+emailSubject;
        //String testBodyString = email.plainTextBody;
        String taskBody = 'From : ';
        if(email.fromAddress != null){
            taskBody += email.fromAddress;
        }
        taskBody += '\nCC:';
        if(email.ccAddresses != null){
            taskBody += email.ccAddresses;
        }
        taskBody +='\n'+ 'BCC:\n'+
            'Subject: '+email.Subject+'\n'+ 'Body: '+email.plainTextBody;
        newTask.description = taskBody;
        newTask.OwnerId =UserInfo.getUserId();
        newTask.WhatId = taskOrder.id;
        newTask.priority = 'Normal';
        newTask.status = 'Completed'; 
        newTask.TaskSubtype = 'Email';
        try{
            DMLManager.insertAsUser(newTask);
        }
        catch(Exception ex){
            System.debug('Error on task creation = '+ex);
        }
    }
    public static void insertContractMods(Task_Order__c newTaskOrder,Task_Order__c oldTaskOrder,Messaging.InboundEmail email){
        List<Task_Order__c> taskOrderList = [SELECT Id,Name,Email_Reference__c, Task_Order_Title__c, Due_Date__c, Release_Date__c,Contracting_Officer__c,Contract_Specialist__c,ASB_Action__c,Description__c,Start_Date__c,Stop_Date__c,
                                             Contract_Negotiator__c,Proposal_Due_DateTime__c,Primary_Requirement__c,Requirement__c,Classification__c,Key_Personnel_Labor_Categories__c,Incumbent__c,
                                             Contract_Type__c,Task_Order_Place_of_Performance__c,POP_Notes__c,Buyer__c,
                                             Agency__c,Agency_City__c,Agency_State__c,Agency_Street_Address__c,Agency_Zip__c,GPAT__c,Reseller_Requirements__c,SEWP_Agency_ID__c,
                                             Installation__c,Contracting_Office_Name__c,Solicitation_Number__c,Ship_To__c,Payment_Terms__c,FedBid_Category__c,FedBid_Subcategory__c,
                                             FedBid_Seller_Community__c,Contract_Vehicle_picklist__c,Set_Aside__c,Quantity__c,ITES_3H_Activity__c,Award_Type__c,TO_Request_Status__c,Zone__c,
                                             Delivery__c,Fair_Opportunity_Picklist__c,Outside_System_Order_Picklist__c,
                                             Will_a_brand_name_justification_Picklist__c,Expedited_Shipment_Picklist__c,
                                             Stage__c,ITES_Format__c  
                                             FROM Task_Order__c where Name =: newTaskOrder.id];
        System.debug(''+taskOrderList);
        if(taskOrderList.size() > 0 ){
            newTaskOrder = taskOrderList[0];
        }
        if(newTaskOrder != null && oldTaskOrder != null){
            Contract_Mods__c cm = new Contract_Mods__c();
            cm.RecordTypeId = Schema.SObjectType.Contract_Mods__c.getRecordTypeInfosByName().get('Update').getRecordTypeId();
            
            //cm.Task_Order_Number__c = taskOrder.Name;
            cm.TaskOrder__c = newTaskOrder.Id;
            Boolean flag = false;
            System.debug('Task_Order_Title__c = '+ oldTaskOrder.Task_Order_Title__c);
            System.debug('Task_Order_Title__c = '+ newTaskOrder.Task_Order_Title__c);
            if(newTaskOrder.Task_Order_Title__c != oldTaskOrder.Task_Order_Title__c){
                cm.Task_Order_Title__c = oldTaskOrder.Task_Order_Title__c;
                cm.New_Task_Order_Title__c = newTaskOrder.Task_Order_Title__c ;
                System.debug('In Task Order Title');
                flag = true;
            }
            System.debug('Due_Date__c = '+ oldTaskOrder.Due_Date__c );
            System.debug('Due_Date__c = '+ newTaskOrder.Due_Date__c );
            if(newTaskOrder.Due_Date__c != oldTaskOrder.Due_Date__c){
                cm.Due_Date__c = oldTaskOrder.Due_Date__c;
                cm.New_Due_Date__c = newTaskOrder.Due_Date__c;
                System.debug('In Proposal Due Date');
                flag = true;
            }
            
            System.debug('Proposal_Due_DateTime__c= '+ oldTaskOrder.Proposal_Due_DateTime__c);
            System.debug('Proposal_Due_DateTime__c= '+ newTaskOrder.Proposal_Due_DateTime__c);
            if(newTaskOrder.Proposal_Due_DateTime__c!= oldTaskOrder.Proposal_Due_DateTime__c){
                cm.Proposal_Due_Date_Time__c= oldTaskOrder.Proposal_Due_DateTime__c;
                cm.New_Proposal_Due_Date_Time__c= newTaskOrder.Proposal_Due_DateTime__c;
                System.debug('In Proposal Due Date');
                flag = true;
            }
            
            System.debug('Questions_Due_Date__c = '+ oldTaskOrder.Questions_Due_Date__c);
            System.debug('Questions_Due_Date__c = '+ newTaskOrder.Questions_Due_Date__c);
            if(newTaskOrder.Questions_Due_Date__c != oldTaskOrder.Questions_Due_Date__c){
                cm.Questions_Due_Date__c = oldTaskOrder.Questions_Due_Date__c;
                cm.New_Questions_Due_Date__c = newTaskOrder.Questions_Due_Date__c;
                System.debug('In Proposal Due Date');
                flag = true;
            }
            
            System.debug('Release_Date__c = '+ oldTaskOrder.Release_Date__c );
            System.debug('Release_Date__c = '+ newTaskOrder.Release_Date__c );
            if(newTaskOrder.Release_Date__c!= oldTaskOrder.Release_Date__c){
                cm.Release_Date__c = oldTaskOrder.Release_Date__c;
                cm.New_Release_Date__c  = newTaskOrder.Release_Date__c;
                System.debug('In Release Date');
                flag = true;
            }
            System.debug('Contracting_Officer__c= '+ oldTaskOrder.Contracting_Officer__c);
            System.debug('Contracting_Officer__c= '+ newTaskOrder.Contracting_Officer__c);
            if(newTaskOrder.Contracting_Officer__c != oldTaskOrder.Contracting_Officer__c){
                cm.Contracting_Officer__c = oldTaskOrder.Contracting_Officer__c;
                cm.New_Contracting_Officer__c = newTaskOrder.Contracting_Officer__c;
                System.debug('In Contracting Officer');
                flag = true;
            }
            System.debug('Contract_Specialist__c = '+ oldTaskOrder.Contract_Specialist__c );
            System.debug('Contract_Specialist__c = '+ newTaskOrder.Contract_Specialist__c );
            if(newTaskOrder.Contract_Specialist__c != oldTaskOrder.Contract_Specialist__c){
                cm.Contract_Specialist__c = oldTaskOrder.Contract_Specialist__c;
                cm.New_Contract_Specialist__c = newTaskOrder.Contract_Specialist__c;
                System.debug('In Contract Specialist');
                flag = true;
            }
            
            System.debug('Contract_Negotiator__c = '+ oldTaskOrder.Contract_Negotiator__c);
            System.debug('Contract_Negotiator__c = '+ newTaskOrder.Contract_Negotiator__c);
            if(newTaskOrder.Contract_Negotiator__c != oldTaskOrder.Contract_Negotiator__c){
                cm.Contract_Negotiator__c= oldTaskOrder.Contract_Negotiator__c;
                cm.New_Contract_Negotiator__c = newTaskOrder.Contract_Negotiator__c;
                System.debug('In Contract Specialist');
                flag = true;
            }
            
            System.debug('Buyer__c = '+ oldTaskOrder.Buyer__c);
            System.debug('Buyer__c = '+ newTaskOrder.Buyer__c);
            if(newTaskOrder.Buyer__c != oldTaskOrder.Buyer__c){
                cm.Buyer__c = oldTaskOrder.Buyer__c;
                cm.New_Buyer__c = newTaskOrder.Buyer__c;
                System.debug('In Buyer');
                flag = true;
            }
            
            System.debug('ASB_Action__c  = '+ oldTaskOrder.ASB_Action__c );
            System.debug('ASB_Action__c = '+ newTaskOrder.ASB_Action__c );
            if(newTaskOrder.ASB_Action__c!= oldTaskOrder.ASB_Action__c){
                cm.ASB_Action__c = oldTaskOrder.ASB_Action__c;
                cm.New_ASB_Action__c = newTaskOrder.ASB_Action__c;
                System.debug('In ASB Action');
                flag = true;
            }
            
            System.debug('Description__c  = '+ oldTaskOrder.Description__c );
            System.debug('Description__c  = '+ newTaskOrder.Description__c );
            if( newTaskOrder.Description__c != oldTaskOrder.Description__c){
                cm.Description__c= oldTaskOrder.Description__c;
                cm.New_Description__c= newTaskOrder.Description__c;
                System.debug('In Description');
                flag = true;
            }
            
            System.debug('Start_Date__c = '+ oldTaskOrder.Start_Date__c );
            System.debug('Start_Date__c = '+ newTaskOrder.Start_Date__c );
            if(newTaskOrder.Start_Date__c!= oldTaskOrder.Start_Date__c){
                cm.Start_Date__c = oldTaskOrder.Start_Date__c ;
                cm.New_Start_Date__c = newTaskOrder.Start_Date__c ;
                System.debug('In Start Date');
                flag = true;
            }
            
            System.debug('Stop_Date__c = '+ oldTaskOrder.Stop_Date__c );
            System.debug('Stop_Date__c = '+ newTaskOrder.Stop_Date__c);
            if(newTaskOrder.Stop_Date__c != oldTaskOrder.Stop_Date__c){
                cm.Stop_Date__c= oldTaskOrder.Stop_Date__c;
                cm.New_Stop_Date__c= newTaskOrder.Stop_Date__c;
                System.debug('In Stop Date');
                flag = true;
            }
            
            //****
            System.debug('Classification__c = '+ oldTaskOrder.Classification__c );
            System.debug('Classification__c = '+ newTaskOrder.Classification__c);                
            if(newTaskOrder.Classification__c != oldTaskOrder.Classification__c){
                cm.Classification__c= oldTaskOrder.Classification__c;
                cm.New_Classification__c= newTaskOrder.Classification__c;
                System.debug('in Classification');
                flag = true;
            }
            
            System.debug('Task_Order_Place_of_Performance__c = '+ oldTaskOrder.Task_Order_Place_of_Performance__c );
            System.debug('Task_Order_Place_of_Performance__c = '+ newTaskOrder.Task_Order_Place_of_Performance__c);                
            if(newTaskOrder.Task_Order_Place_of_Performance__c != oldTaskOrder.Task_Order_Place_of_Performance__c){
                cm.Task_Order_Place_of_Performance__c= oldTaskOrder.Task_Order_Place_of_Performance__c;
                cm.New_Task_Order_Place_of_Performance__c= newTaskOrder.Task_Order_Place_of_Performance__c;
                System.debug('in Task_Order_Place_of_Performance__c');
                flag = true;
            }
            
            System.debug('Incumbent__c = '+ oldTaskOrder.Incumbent__c );
            System.debug('Incumbent__c = '+ newTaskOrder.Incumbent__c);                
            if(newTaskOrder.Incumbent__c != oldTaskOrder.Incumbent__c){
                cm.Incumbent__c= oldTaskOrder.Incumbent__c;
                cm.New_Incumbent__c= newTaskOrder.Incumbent__c;
                System.debug('in Incumbent__c');
                flag = true;
            }
            
            System.debug('Key_Personnel_Labor_Categories__c = '+ oldTaskOrder.Key_Personnel_Labor_Categories__c );
            System.debug('Key_Personnel_Labor_Categories__c = '+ newTaskOrder.Key_Personnel_Labor_Categories__c);                
            if(newTaskOrder.Key_Personnel_Labor_Categories__c != oldTaskOrder.Key_Personnel_Labor_Categories__c){
                cm.Key_Personnel_Labor_Categories__c= oldTaskOrder.Key_Personnel_Labor_Categories__c;
                cm.New_Key_Personnel_Labor_Categories__c= newTaskOrder.Key_Personnel_Labor_Categories__c;
                System.debug('in Key_Personnel_Labor_Categories__c');
                flag = true;
            }
            
            System.debug('Contract_Type__c = '+ oldTaskOrder.Contract_Type__c );
            System.debug('Contract_Type__c = '+ newTaskOrder.Contract_Type__c);                
            if(newTaskOrder.Contract_Type__c != oldTaskOrder.Contract_Type__c){
                cm.Contract_Type__c= oldTaskOrder.Contract_Type__c;
                cm.New_Contract_Type__c= newTaskOrder.Contract_Type__c;
                System.debug('in Contract_Type__c');
                flag = true;
            }
            
            System.debug('POP_Notes__c = '+ oldTaskOrder.POP_Notes__c );
            System.debug('POP_Notes__c = '+ newTaskOrder.POP_Notes__c);                
            if(newTaskOrder.POP_Notes__c != oldTaskOrder.POP_Notes__c){
                cm.POP_Notes__c= oldTaskOrder.POP_Notes__c;
                cm.New_POP_Notes__c= newTaskOrder.POP_Notes__c;
                System.debug('in POP_Notes__c');
                flag = true;
            }
            //******
            // To show Primary_Requirement__c on Contract Mod.
            // Added by Ramandeep
            System.debug('Primary_Requirement__c = '+ oldTaskOrder.Primary_Requirement__c );
            System.debug('Primary_Requirement__c = '+ newTaskOrder.Primary_Requirement__c);
            
            if(oldTaskOrder.Primary_Requirement__c !=Null && newTaskOrder.Primary_Requirement__c !=Null){
                if(newTaskOrder.Primary_Requirement__c.trim() != oldTaskOrder.Primary_Requirement__c.trim()){
                    cm.Primary_Requirement__c= oldTaskOrder.Primary_Requirement__c;
                    cm.New_Primary_Requirement__c= newTaskOrder.Primary_Requirement__c;
                    System.debug('In Primary_Requirement__c IF');
                    flag = true;
                }
            }else{
                if(newTaskOrder.Primary_Requirement__c != oldTaskOrder.Primary_Requirement__c){
                    cm.Primary_Requirement__c= oldTaskOrder.Primary_Requirement__c;
                    cm.New_Primary_Requirement__c= newTaskOrder.Primary_Requirement__c;
                    System.debug('In Primary_Requirement__c ELSE');
                    flag = true;
                }  
            }
            
            System.debug('Requirement__c = '+ oldTaskOrder.Requirement__c );
            System.debug('Requirement__c = '+ newTaskOrder.Requirement__c);
            if(oldTaskOrder.Requirement__c !=Null && newTaskOrder.Requirement__c !=Null){
                if(newTaskOrder.Requirement__c.trim() != oldTaskOrder.Requirement__c.trim()){
                    cm.Requirement__c= oldTaskOrder.Requirement__c;
                    cm.New_Requirement__c= newTaskOrder.Requirement__c;
                    System.debug('In Requirement__c');
                    flag = true;
                }
            }else{
                if(newTaskOrder.Requirement__c != oldTaskOrder.Requirement__c){
                    cm.Requirement__c= oldTaskOrder.Requirement__c;
                    cm.New_Requirement__c= newTaskOrder.Requirement__c;
                    System.debug('In Requirement__c');
                    flag = true;
                }
            }
            
            // New Changes - 7Feb2018
            System.debug('Agency__c = '+ oldTaskOrder.Agency__c );
            System.debug('Agency__c = '+ newTaskOrder.Agency__c);                
            if(newTaskOrder.Agency__c != oldTaskOrder.Agency__c){
                cm.Agency__c= oldTaskOrder.Agency__c;
                cm.New_Agency__c= newTaskOrder.Agency__c;
                System.debug('in Agency__c');
                flag = true;
            }
            
            System.debug('Agency_City__c = '+ oldTaskOrder.Agency_City__c );
            System.debug('Agency_City__c = '+ newTaskOrder.Agency_City__c);                
            if(newTaskOrder.Agency_City__c != oldTaskOrder.Agency_City__c){
                cm.Agency_City__c= oldTaskOrder.Agency_City__c;
                cm.New_Agency_City__c= newTaskOrder.Agency_City__c;
                System.debug('in Agency_City__c');
                flag = true;
            }
            
            System.debug('Agency_State__c = '+ oldTaskOrder.Agency_State__c );
            System.debug('Agency_State__c = '+ newTaskOrder.Agency_State__c);                
            if(newTaskOrder.Agency_State__c != oldTaskOrder.Agency_State__c){
                cm.Agency_State__c= oldTaskOrder.Agency_State__c;
                cm.New_Agency_State__c= newTaskOrder.Agency_State__c;
                System.debug('in Agency_State__c');
                flag = true;
            } 
            
            System.debug('Agency_Street_Address__c = '+ oldTaskOrder.Agency_Street_Address__c );
            System.debug('Agency_Street_Address__c = '+ newTaskOrder.Agency_Street_Address__c);                
            if(newTaskOrder.Agency_Street_Address__c != oldTaskOrder.Agency_Street_Address__c){
                cm.Agency_Street_Address__c= oldTaskOrder.Agency_Street_Address__c;
                cm.New_Agency_Street_Address__c= newTaskOrder.Agency_Street_Address__c;
                System.debug('in Agency_Street_Address__c');
                flag = true;
            }
            
            System.debug('Agency_Zip__c = '+ oldTaskOrder.Agency_Zip__c );
            System.debug('Agency_Zip__c = '+ newTaskOrder.Agency_Zip__c);                
            if(newTaskOrder.Agency_Zip__c != oldTaskOrder.Agency_Zip__c){
                cm.Agency_Zip__c= oldTaskOrder.Agency_Zip__c;
                cm.New_Agency_Zip__c= newTaskOrder.Agency_Zip__c;
                System.debug('in Agency_Zip__c');
                flag = true;
            }
            
            System.debug('GPAT__c = '+ oldTaskOrder.GPAT__c );
            System.debug('GPAT__c = '+ newTaskOrder.GPAT__c);                
            if(newTaskOrder.GPAT__c != oldTaskOrder.GPAT__c){
                cm.GPAT__c= oldTaskOrder.GPAT__c;
                cm.New_GPAT__c= newTaskOrder.GPAT__c;
                System.debug('in GPAT__c');
                flag = true;
            }
            
            System.debug('Reseller_Requirements__c = '+ oldTaskOrder.Reseller_Requirements__c );
            System.debug('Reseller_Requirements__c = '+ newTaskOrder.Reseller_Requirements__c);                
            if(newTaskOrder.Reseller_Requirements__c != oldTaskOrder.Reseller_Requirements__c){
                cm.Reseller_Requirements__c= oldTaskOrder.Reseller_Requirements__c;
                cm.New_Reseller_Requirements__c= newTaskOrder.Reseller_Requirements__c;
                System.debug('in Reseller_Requirements__c');
                flag = true;
            } 
            
            System.debug('SEWP_Agency_ID__c = '+ oldTaskOrder.SEWP_Agency_ID__c );
            System.debug('SEWP_Agency_ID__c = '+ newTaskOrder.SEWP_Agency_ID__c);                
            if(newTaskOrder.SEWP_Agency_ID__c != oldTaskOrder.SEWP_Agency_ID__c){
                cm.SEWP_Agency_ID__c= oldTaskOrder.SEWP_Agency_ID__c;
                cm.New_SEWP_Agency_ID__c= newTaskOrder.SEWP_Agency_ID__c;
                System.debug('in SEWP_Agency_ID__c');
                flag = true;
            }
            // New Changes - 7Feb2018

            // New Changes - 15Feb2018
            System.debug('Installation__c = '+ oldTaskOrder.Installation__c );
            System.debug('Installation__c = '+ newTaskOrder.Installation__c);                
            if(newTaskOrder.Installation__c != oldTaskOrder.Installation__c){
                cm.Installation__c= oldTaskOrder.Installation__c;
                cm.New_Installation__c= newTaskOrder.Installation__c;
                System.debug('in Installation__c');
                flag = true;
            }
            
            System.debug('Contracting_Office_Name__c = '+ oldTaskOrder.Contracting_Office_Name__c );
            System.debug('Contracting_Office_Name__c = '+ newTaskOrder.Contracting_Office_Name__c);                
            if(newTaskOrder.Contracting_Office_Name__c != oldTaskOrder.Contracting_Office_Name__c){
                cm.Contracting_Office_Name__c= oldTaskOrder.Contracting_Office_Name__c;
                cm.New_Contracting_Office_Name__c= newTaskOrder.Contracting_Office_Name__c;
                System.debug('in Contracting_Office_Name__c');
                flag = true;
            }
            // New Changes - 15Feb2018

            // New Changes - 22Feb2018
            System.debug('Solicitation_Number__c = '+ oldTaskOrder.Solicitation_Number__c );
            System.debug('Solicitation_Number__c = '+ newTaskOrder.Solicitation_Number__c);                
            if(newTaskOrder.Solicitation_Number__c != oldTaskOrder.Solicitation_Number__c){
                cm.Solicitation_Number__c= oldTaskOrder.Solicitation_Number__c;
                cm.New_Solicitation_Number__c= newTaskOrder.Solicitation_Number__c;
                System.debug('in Solicitation_Number__c');
                flag = true;
            }
            
            System.debug('Ship_To__c = '+ oldTaskOrder.Ship_To__c );
            System.debug('Ship_To__c = '+ newTaskOrder.Ship_To__c);                
            if(newTaskOrder.Ship_To__c != oldTaskOrder.Ship_To__c){
                cm.Ship_To__c= oldTaskOrder.Ship_To__c;
                cm.New_Ship_To__c= newTaskOrder.Ship_To__c;
                System.debug('in Ship_To__c');
                flag = true;
            }
            
            System.debug('Payment_Terms__c = '+ oldTaskOrder.Payment_Terms__c );
            System.debug('Payment_Terms__c = '+ newTaskOrder.Payment_Terms__c);                
            if(newTaskOrder.Payment_Terms__c != oldTaskOrder.Payment_Terms__c){
                cm.Payment_Terms__c= oldTaskOrder.Payment_Terms__c;
                cm.New_Payment_Terms__c= newTaskOrder.Payment_Terms__c;
                System.debug('in Payment_Terms__c');
                flag = true;
            }
            System.debug('FedBid_Category__c flag = '+ flag);
            System.debug('FedBid_Category__c = '+ oldTaskOrder.FedBid_Category__c );
            System.debug('FedBid_Category__c = '+ newTaskOrder.FedBid_Category__c);                
            if(newTaskOrder.FedBid_Category__c != oldTaskOrder.FedBid_Category__c){
                cm.FedBid_Category__c= oldTaskOrder.FedBid_Category__c;
                cm.New_FedBid_Category__c= newTaskOrder.FedBid_Category__c;
                System.debug('in FedBid_Category__c');
                flag = true;
            }
            
            System.debug('FedBid_Subcategory__c = '+ oldTaskOrder.FedBid_Subcategory__c );
            System.debug('FedBid_Subcategory__c = '+ newTaskOrder.FedBid_Subcategory__c);                
            if(newTaskOrder.FedBid_Subcategory__c != oldTaskOrder.FedBid_Subcategory__c){
                cm.FedBid_Subcategory__c= oldTaskOrder.FedBid_Subcategory__c;
                cm.New_FedBid_Subcategory__c= newTaskOrder.FedBid_Subcategory__c;
                System.debug('in FedBid_Subcategory__c');
                flag = true;
            }
            
            System.debug('FedBid_Seller_Community__c = '+ oldTaskOrder.FedBid_Seller_Community__c );
            System.debug('FedBid_Seller_Community__c = '+ newTaskOrder.FedBid_Seller_Community__c);                
            if(newTaskOrder.FedBid_Seller_Community__c != oldTaskOrder.FedBid_Seller_Community__c){
                cm.FedBid_Seller_Community__c= oldTaskOrder.FedBid_Seller_Community__c;
                cm.New_FedBid_Seller_Community__c= newTaskOrder.FedBid_Seller_Community__c;
                System.debug('in FedBid_Seller_Community__c');
                flag = true;
            }
            
            System.debug('Contract_Vehicle_picklist__c = '+ oldTaskOrder.Contract_Vehicle_picklist__c );
            System.debug('Contract_Vehicle_picklist__c = '+ newTaskOrder.Contract_Vehicle_picklist__c);                
            if(newTaskOrder.Contract_Vehicle_picklist__c != oldTaskOrder.Contract_Vehicle_picklist__c){
                cm.Contract_Vehicle_picklist__c= oldTaskOrder.Contract_Vehicle_picklist__c;
                cm.New_Contract_Vehicle__c= newTaskOrder.Contract_Vehicle_picklist__c;
                System.debug('in Contract_Vehicle_picklist__c');
                flag = true;
            }
            
            System.debug('Set_Aside__c = '+ oldTaskOrder.Set_Aside__c );
            System.debug('Set_Aside__c = '+ newTaskOrder.Set_Aside__c);                
            if(newTaskOrder.Set_Aside__c != oldTaskOrder.Set_Aside__c){
                cm.Set_Aside__c= oldTaskOrder.Set_Aside__c;
                cm.New_Set_Aside__c= newTaskOrder.Set_Aside__c;
                System.debug('in Set_Aside__c');
                flag = true;
            }
            
            if(newTaskOrder.Quantity__c != oldTaskOrder.Quantity__c){
                cm.Quantity__c= oldTaskOrder.Quantity__c;
                cm.New_Quantity__c= newTaskOrder.Quantity__c;
                System.debug('in Quantity__c ');
                flag = true;
            }
            
            if(newTaskOrder.ITES_3H_Activity__c!= oldTaskOrder.ITES_3H_Activity__c){
                cm.ITES_3H_Activity__c= oldTaskOrder.ITES_3H_Activity__c;
                cm.New_ITES_3H_Activity__c= newTaskOrder.ITES_3H_Activity__c;
                System.debug('in ITES_3H_Activity__c');
                flag = true;
            }
            // New Changes - 22Feb2018
            
            // New Changes - 3March2018
            if(newTaskOrder.Award_Type__c!= oldTaskOrder.Award_Type__c){
                cm.Award_Type__c= oldTaskOrder.Award_Type__c;
                cm.New_Award_Type__c= newTaskOrder.Award_Type__c;
                System.debug('in Award_Type__c');
                flag = true;
            }
            // New Changes - 3March2018
             
            
           // New Changes - 16March2018
            if(newTaskOrder.TO_Request_Status__c!= oldTaskOrder.TO_Request_Status__c){
                cm.TO_Request_Status__c= oldTaskOrder.TO_Request_Status__c;
                cm.New_TO_Request_Status__c= newTaskOrder.TO_Request_Status__c;
                System.debug('in TO_Request_Status__c');
                flag = true;
            }
            // New Changes - 16March2018
            
            
             // New Changes - 12April2018
            if(newTaskOrder.Zone__c!= oldTaskOrder.Zone__c){
                cm.Zone__c= oldTaskOrder.Zone__c;
                cm.New_Zone__c= newTaskOrder.Zone__c;
                System.debug('in Zone__c');
                flag = true;
            }
            // New Changes - 12april2018 
           
            // New Changes - 16May2018
            if(newTaskOrder.Delivery__c != oldTaskOrder.Delivery__c){
                cm.Delivery__c= oldTaskOrder.Delivery__c;
                cm.New_Delivery__c= newTaskOrder.Delivery__c;
                System.debug('in Delivery__c');
                flag = true;
            }
            
            System.debug('newTaskOrder.Fair_Opportunity_Picklist__c = '+newTaskOrder.Fair_Opportunity_Picklist__c);
            System.debug('oldTaskOrder.Fair_Opportunity_Picklist__c = '+oldTaskOrder.Fair_Opportunity_Picklist__c);
            if(newTaskOrder.Fair_Opportunity_Picklist__c!= oldTaskOrder.Fair_Opportunity_Picklist__c){
                cm.Fair_Opportunity_Picklist__c= oldTaskOrder.Fair_Opportunity_Picklist__c;
                cm.New_Fair_Opportunity_Picklist__c= newTaskOrder.Fair_Opportunity_Picklist__c;
                System.debug('in Fair_Opportunity_Picklist__c');
                flag = true;
            }              
           
            if(newTaskOrder.Outside_System_Order_Picklist__c!= oldTaskOrder.Outside_System_Order_Picklist__c){
                cm.Outside_System_Order_Picklist__c= oldTaskOrder.Outside_System_Order_Picklist__c;
                cm.New_Outside_System_Order_Picklist__c= newTaskOrder.Outside_System_Order_Picklist__c;
                System.debug('in Outside_System_Order_Picklist__c');
                flag = true;
            }
             
            if(newTaskOrder.Will_a_brand_name_justification_Picklist__c!= oldTaskOrder.Will_a_brand_name_justification_Picklist__c){
                cm.Will_a_brand_name_justification_Picklist__c= oldTaskOrder.Will_a_brand_name_justification_Picklist__c;
                cm.New_Will_a_brand_name_justification_Pick__c= newTaskOrder.Will_a_brand_name_justification_Picklist__c;
                System.debug('in Will_a_brand_name_justification_Picklist__c');
                flag = true;
            }
            
            if(newTaskOrder.Expedited_Shipment_Picklist__c!= oldTaskOrder.Expedited_Shipment_Picklist__c){
                cm.Expedited_Shipment_Picklist__c= oldTaskOrder.Expedited_Shipment_Picklist__c;
                cm.New_Expedited_Shipment_Picklist__c= newTaskOrder.Expedited_Shipment_Picklist__c;
                System.debug('in Expedited_Shipment_Picklist__c');
                flag = true;
            }            
            // New Changes - 16May2018 
             
            // New Changes - 23May2018 
            if(newTaskOrder.Stage__c!= oldTaskOrder.Stage__c){
                cm.Stage__c= oldTaskOrder.Stage__c;
                cm.New_Stage__c= newTaskOrder.Stage__c;
                System.debug('in Stage__c');
                flag = true;
            }            
            // New Changes - 23May2018
            
            // New chnages - 04April2019
            if(newTaskOrder.ITES_Format__c!= oldTaskOrder.ITES_Format__c){
                cm.ITES_Format__c= oldTaskOrder.ITES_Format__c;
                cm.New_ITES_Format__c= newTaskOrder.ITES_Format__c;
                System.debug('in ITES_Format__c');
                flag = true;
            }
            // New chnages - 04April2019 

            System.debug('4 = '+ flag);
            if(email != null && email.htmlBody != null){
                cm.Email_Body__c = checkValidString(email.htmlBody,30000);
                System.debug('In Email Body');
            }
            System.debug(''+cm.TaskOrder__c);
            if(flag &&  cm.TaskOrder__c != null){
                System.debug('Insert Contract Modes');
                insert cm;
            }
        }
    }
    
    public static Task_Order__c getExistTaskOrder(String taskOrderNumber, String contractId){
        System.debug(''+taskOrderNumber+'\t'+contractId);
        List<Task_Order__c> taskOrderList = [SELECT Id,Name,Email_Reference__c, Task_Order_Title__c, Due_Date__c, Release_Date__c,Contracting_Officer__c,Contract_Specialist__c,ASB_Action__c,Description__c,Start_Date__c,Stop_Date__c,
                                             Contract_Negotiator__c,Proposal_Due_DateTime__c,Questions_Due_Date__c,Primary_Requirement__c,Requirement__c,Customer_Agency__c,Incumbent__c,
                                             Classification__c,Key_Personnel_Labor_Categories__c,Contract_Type__c,Task_Order_Place_of_Performance__c,POP_Notes__c,Buyer__c,
                                             Individual_Receiving_Shipment__c,Task_Order_Contracting_Activity__c,
                                             Agency__c,Agency_City__c,Agency_State__c,Agency_Street_Address__c,Agency_Zip__c,GPAT__c,Reseller_Requirements__c,SEWP_Agency_ID__c,
                                             Installation__c,Contracting_Office_Name__c,Solicitation_Number__c,Ship_To__c,Payment_Terms__c,FedBid_Category__c,FedBid_Subcategory__c,
                                             FedBid_Seller_Community__c,Contract_Vehicle_picklist__c,Set_Aside__c,Quantity__c,ITES_3H_Activity__c,Award_Type__c,TO_Request_Status__c,Zone__c,
                                             Delivery__c,Fair_Opportunity_Picklist__c,Outside_System_Order_Picklist__c,Will_a_brand_name_justification_Picklist__c,Expedited_Shipment_Picklist__c,
                                             Stage__c,Is_cancelled__c,ITES_Format__c   
                                             FROM Task_Order__c where Name =: taskOrderNumber AND Contract_Vehicle__c =: contractId];
        System.debug('taskOrderList'+taskOrderList);
        if(!taskOrderList.isEmpty()){
            return taskOrderList[0];
        }
        return null;
    }
    
    public static String checkValidString(String str, Integer endLimit){
        if(str != null){
            str = str.normalizeSpace().trim();//to remove leading, trailing, and repeating white space characters
            if(str.length() > endLimit){
                String validString = str.substring(0,endLimit);                
                return validString.trim();// trim() - to check for any leading or trailing white space characters after substring.
            }
        }
        return str;
    }
    
    public static Date getEasternDate(DateTime dt){
        Timezone tz = Timezone.getTimeZone('America/New_York');        
        Integer millisecondsDiff = tz.getOffset(dt);
        system.debug('Time hours = '+ millisecondsDiff/(1000*60*60));
        Date d = dt.addHours(millisecondsDiff/(1000*60*60)).date();
        return d;
    }
}