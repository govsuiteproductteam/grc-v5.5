@isTest
public class EarnedAwardTableControllerTest {
    public static testMethod void earnedAwardTableTest(){
        Account account = TestDataGenerator.createAccount('Test Account');
        insert account;
        Contract_Vehicle__c contractVehicle =  TestDataGenerator.createContractVehicle('12345', account);
        insert contractVehicle;

        Id awardFeeRecordTypeId = Schema.SObjectType.Value_Table__c.getRecordTypeInfosByName().get('Award Fee').getRecordTypeId();
        
     
        Value_Table__c valueTable1 = TestDataGenerator.createValueTable(awardFeeRecordTypeId,contractVehicle.Id);
        insert valueTable1;
        
        Value_Table__c valueTable2 = TestDataGenerator.createValueTable(awardFeeRecordTypeId,contractVehicle.Id);
        insert valueTable2;
        Id valueTableRecordId = valueTable2.Id;
        
        Value_Table__c valueTable3 = TestDataGenerator.createValueTable(awardFeeRecordTypeId,contractVehicle.Id);
        insert valueTable3;
        
         Value_Table__c valueTable4 = TestDataGenerator.createValueTable(awardFeeRecordTypeId,contractVehicle.Id);
        insert valueTable4;
        
        
        Test.startTest();
        List<Value_Table__c> awardTableList = new List<Value_Table__c>();
        awardTableList.add(valueTable1);
        String awardTableListStr = JSON.serialize(awardTableList);
        EarnedAwardTableController.saveEarnedAwardFeeTable(awardTableListStr, contractVehicle.Id, 'abc,'+valueTableRecordId);
        Contract_Vehicle__c contractVehicle1 = EarnedAwardTableController.getContractVehical(contractVehicle.Id);
        System.assert(contractVehicle1 != null);
        List<Value_Table__c> valueTableList1 = EarnedAwardTableController.getValueTableEarnedAwardList(contractVehicle.Id);
        System.assert(valueTableList1.size() != 0);
        Value_Table__c newValueTable = EarnedAwardTableController.newRowEarnedAwardFeeTable(contractVehicle.Id);
        System.assert(newValueTable != null);
        EarnedAwardTableController.isLightningPage();
        Test.stopTest();
    }
}