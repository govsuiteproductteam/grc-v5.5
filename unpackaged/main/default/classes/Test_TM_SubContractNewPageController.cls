@isTest
public class Test_TM_SubContractNewPageController {

    public static testMethod void testContractNewPage(){
         Test.startTest();
        
        //Document document = TestDataGenerator.createDocument('Dependent Field Document contract','Dependent_Field_Document_contract');
        //insert document;
         List<RecordType> rtypes = [Select Name, Id From RecordType where sObjectType='TM_TOMA__SubContract__c' ];//and isActive=true
        
        string recordTypeId ='';
        if(rtypes.size()>0)
        {
            recordTypeId=rtypes[0].Id;
        }
        DependentFieldSettingCtrl depFieldSett = new DependentFieldSettingCtrl();
        depFieldSett.recordTypeId=recordTypeId;
        Document document;
        document = new Document();
        document.Body = Blob.valueOf('');
        document.ContentType = 'txt';
        document.DeveloperName = 'Test_Dependent_Field_Document_subContract';
        document.IsPublic = true;
        document.Name = 'Test Dependent Field Document subContract';
        document.FolderId = [select id from folder where name = 'Dependent Field Doc'].id;
        insert document;
        
        depFieldSett.objectType = 'Sub Contract';
        depFieldSett.selectSObject();
        depFieldSett.go();
        depFieldSett.masterLabel = 'TM_TOMA__Task_Order_Types__c';
        depFieldSett.addMaster();
        depFieldSett.masterValue = 'All Type';
        depFieldSett.listIndex = 0;
        
        depFieldSett.addNewValue();
        depFieldSett.addField();
        depFieldSett.save();
        depFieldSett.masterLabel = 'TM_TOMA__Task_Order_Types__c';
        depFieldSett.addMaster();
        //System.assertEquals('The Field '+depFieldSett.masterLabel+' is already Exist.', depFieldSett.message);
        depFieldSett.removeFieldStructure();
        depFieldSett.masterValue = 'All Type';
        
        depFieldSett.getContractFields();
        
        CLM_Tab__c createCLMTab = TestDataGenerator.createCLMTab(recordTypeId);
        createCLMTab.Is_Contract__c = false;
        insert createCLMTab;
        
        CLM_Section__c clmSection = TestDataGenerator.createCLMSection(recordTypeId);
        clmSection.Is_Contract__c = false;
        clmSection.Order__c = 10;
        insert clmSection;
        
        CLM_FIeld__c clmField = TestDataGenerator.createCLMField(recordTypeId);
        clmField.Is_Contract__c = false;
        clmField.Order__c = 1001;
        insert clmField;
        
        
        PageReference pageRef = Page.TM_SubContractNewPage;
        pageRef.getparameters().Put('RecordType', recordTypeId);
        Test.setCurrentPage(pageRef);
        SubContract__c subContract =  new SubContract__c();

        ApexPages.StandardController sc = new ApexPages.StandardController(subContract);
        TM_SubContractNewPageController contractNewPageController = new TM_SubContractNewPageController(sc);
        contractNewPageController.subContractVehicle = (SubContract__c)sc.getRecord();
        contractNewPageController.subContractVehicle.Sub_Contract_Number__c = '222';
        contractNewPageController.subContractVehicle.Subcontract_Status_Backend__c = 'Draft1';
        contractNewPageController.saveContract1();
        List<SubContract__c> subConList = [Select Id,Sub_Contract_Number__c from SubContract__c];
        System.assertEquals(1, subConList.size());
        System.assertEquals('222', subConList[0].Sub_Contract_Number__c);
        contractNewPageController.saveAndActivate();
        
        contractNewPageController.subContractVehicle.Ceiling_Value__c = 123123123122323233121233.123123123;
        contractNewPageController.saveContract1();
        contractNewPageController.subContractVehicle.Ceiling_Value__c = 123123123122323233121233.123123123;
        contractNewPageController.saveAndActivate();
        
        
        SubContract__c subContract1 = TestDataGenerator.createSubContract('12345');
        subContract1.RecordTypeId = recordTypeId;
        insert subContract1; 
        pageRef = Page.TM_SubContractNewPage;        
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('RecordType', null); 
        // pageRef.getParameters().put('clone','1');
        // System.debug('pageRef'+pageRef);
        ApexPages.currentPage().getParameters().put('id', subContract1.Id);
        ApexPages.currentPage().getParameters().put('clone', '1');
        
        sc = new ApexPages.StandardController(subContract1);
        contractNewPageController = new TM_SubContractNewPageController(sc);
        contractNewPageController.saveContract1();
        contractNewPageController.saveAndActivate();
        System.assertEquals('Final',contractNewPageController.subContractVehicle.Subcontract_Status_Backend__c);
        Test.stopTest();
    }
}