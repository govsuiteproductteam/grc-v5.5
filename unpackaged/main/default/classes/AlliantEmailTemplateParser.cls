public with sharing class AlliantEmailTemplateParser {
    private String emailSubject='';
    Private String titledName='';
    Private Date proposalDueDate;
    private DateTime proposalDueDateTime;
    private Date questionsDueDate;
    private DateTime questionsDueDateTime;
    private String senderEmailId = '';
    private String contactName = '';
    private String contactTitle = '';
    private String taskOrderNumber='';
    private Map <String, Integer> months = new Map <String, Integer> {'january'=>1, 'february'=>2
        , 'march'=>3, 'april'=>4, 'may'=>5, 'june'=>6, 'july'=>7, 'august'=>8, 'september'=>9
        , 'october'=>10, 'november'=>11, 'december'=>12};                 
            
            public void parse(Messaging.InboundEmail email){
                String htmlText = email.HTMLBody;
                String emailPlainText = email.plainTextBody;
                //System.debug('emailPlainText = '+emailPlainText);
                emailSubject = email.subject;
                emailSubject = emailSubject.stripHtmlTags();
                
                System.debug('emailSubject = '+emailSubject);
                taskOrderNumber = getTaskOrderNumber(emailSubject);
                
                //taskOrderNumber = checkValidString(emailSubject.substringBetween('Task Order Request', 'for').trim(), 79);
                System.debug('emailSubject = '+emailSubject);
                titledName = emailSubject.substringAfter(taskOrderNumber).replace('for','').trim();
                System.debug('titledName = '+titledName);
                List<String> dString = getGenericDate(emailPlainText);
                if(dString.size()==2){
                    dateTime dtm = getDatetime(dString[1]);
                    dateTime dtm1 = getDatetime(dString[0]);
                    if(dtm != Null)
                        proposalDueDateTime = dtm;
                    if(proposalDueDateTime!=Null)
                        proposalDueDate = proposalDueDateTime.date();//getProposalDueDate(emailPlainText);
                    System.debug('proposalDueDate = '+proposalDueDate);
                    if(dtm1 != Null)
                        questionsDueDateTime = dtm1;
                    if(questionsDueDateTime!=Null)
                        questionsDueDate = questionsDueDateTime.date();//getQuestionsDueDate(emailPlainText);
                }
                //System.debug('proposalDueDate = '+Date.parse(proposalDueDate));
                List<String> emailTemplateList = emailPlainText.split('\n');
                senderEmailId = email.fromAddress;
                contactName = email.fromName;        
                System.debug('contactName = '+contactName);
                System.debug('senderEmailId = '+senderEmailId);  
                
                for(String line : emailTemplateList){
                    System.debug('line = '+line);
                    line = line.trim();
                    if(line == 'Contracting Specialist' || line == 'Contracting Officer'){
                        contactTitle = line;
                        System.debug('********contactTitle = '+contactTitle);
                    }
                }       
            }
    
    private String checkValidString(String str, Integer endLimit){
        if(str.length() > endLimit){
            String validString = str.substring(0,endLimit);
            return validString;
        }
        return str;
    }    
    
    public Task_Order__c getTaskOrder(String toAddress){       
        if(Schema.sObjectType.Contract_Vehicle__c.isAccessible()){ 
            Task_Order__c taskOrder = new Task_Order__c();
            List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];
            System.debug('*********contract vechcle size  = '+ conVehicalList.size());
            
            if(!conVehicalList.isEmpty()){
                taskOrder.Contract_Vehicle__c = conVehicalList[0].Id;
                // taskOrder.Task_Order_Contracting_Organization__c = conVehicalList[0].Account_Name__c;
                
                System.debug('****email subject'+emailSubject);
                System.debug('****email subject length = '+emailSubject.length()); 
                taskOrder.Name =  taskOrderNumber;
                taskOrder.Task_Order_Title__c = checkValidString(titledName , 255);
                taskOrder.Release_Date__c = Date.today();
                System.debug('questionsDueDate '+questionsDueDate);
                if(questionsDueDate!=null){
                    taskOrder.Questions_Due_Date__c = questionsDueDate;
                }
                System.debug('proposalDueDateTime '+proposalDueDateTime);
                if(proposalDueDateTime!=null){
                    taskOrder.TM_TOMA__Proposal_Due_DateTime__c = proposalDueDateTime;
                    taskOrder.Due_Date__c = proposalDueDate;
                }            
                taskOrder.Contract_Vehicle_picklist__c = 'ALLIANT - ALLIANT FULL AND OPEN';
                taskOrder.Email_Reference__c = checkValidString(getTrimedEmailSubject(emailSubject), 255);
                
                //setting Contact details
                List<Contact> contactList = new List<Contact>();
                if(Schema.sObjectType.Contact.isAccessible()){ 
                    contactList =  [Select id, Email from Contact WHERE Email = :senderEmailId];
                }
                Contact contact;
                if(!contactList.isEmpty()){
                    contact = contactList.get(0);   
                }else{
                    String []salutation = new String[]{'Mr.','Ms.','Mrs.','Dr.','Prof.'}; 
                        contact = new Contact();
                    List<String> split =  contactName.split(' ');
                    
                    contact.FirstName = split.get(0);
                    contact.LastName = split.get(1);   
                    contact.Title = contactTitle;                
                    contact.Is_FedTom_Contact__c = true;
                    if(conVehicalList[0].Account_Name__c != null){
                        contact.AccountId = conVehicalList[0].Account_Name__c;
                    }
                    contact.Email = senderEmailId;
                    DMLManager.insertAsUser(new List<Contact>{contact});
                    //insert new List<Contact>{contact};
                }
                if(contactTitle.equals('Contract Specialist')){
                    taskOrder.Contract_Specialist__c = contact.id;
                }
                if(contactTitle.equals('Contracting Officer')){
                    taskOrder.Contracting_Officer__c = contact.id;
                }
                //End Setting contact details
            }
            
            return taskOrder;
        }
        return null;
    }  
    
    private String getTrimedEmailSubject(String subject){
        if(subject.startsWith('RE:')){
            subject = subject.replaceFirst('RE:', '');            
        }
        if(subject.startsWith('Fwd:') ){
            subject = subject.replaceFirst('Fwd:', '');            
        }   
        return subject.trim();
    }   
    
    /*public Date getQuestionsDueDate(String emailBody){
        //System.debug('emailBody = '+emailBody);
        String rawQuestionsDate = emailBody.replaceAll('\n', ' ').replaceAll('(\\s+)', ' ').substringBetween('Question shall', 'EST');
        System.debug('rawQuestionsDate = '+rawQuestionsDate);
        List<String> rawQuestionsDateSplit = rawQuestionsDate.replaceAll('(\\s+)', ' ').trim().split(' ');
        Integer size = rawQuestionsDateSplit.size();
        //if(size == 7){
        String month = rawQuestionsDateSplit[size - 6].toLowerCase();
        System.debug('month = '+month);
        
        Integer monthInt = months.get(month);
        System.debug('monthInt = '+monthInt);
        String dayString = rawQuestionsDateSplit[size - 5];
        dayString = dayString.replace('st','').replace('nd','').replace('rd','').replace('th','').remove(',');
        String day = dayString ;
        System.debug('day = '+day);
        String year = rawQuestionsDateSplit[size - 4];  
        System.debug('year = '+year);
        try{
            Date parsedDate = Date.parse(''+monthInt+'/'+day+'/'+year);
            System.debug('parsedDate = '+parsedDate);
            return parsedDate;
        }catch(Exception e){
            return null;
        }
       // }
        //return Null;
    }*/
    
    /*public Date getProposalDueDate(String emailBody){
        //System.debug('emailBody = '+emailBody);
        String rawQuestionsDate = emailBody.replaceAll('\n', '').substringBetween('Responses are due by', 'EST');
        System.debug('rawQuestionsDate = '+rawQuestionsDate);
        List<String> rawQuestionsDateSplit = rawQuestionsDate.replaceAll('(\\s+)', ' ').trim().split(' ');
        Integer size = rawQuestionsDateSplit.size();
        System.debug('size '+size);
        if(size == 7){
            String month = rawQuestionsDateSplit[size - 6].toLowerCase();
            System.debug('month = '+month);
            Integer monthInt = months.get(month);
            System.debug('monthInt = '+monthInt);
            // dayString = dayString.replace('st','').replace('nd','').replace('rd','').replace('th','');
            String day = rawQuestionsDateSplit[size - 5].replace('st','').replace('nd','').replace('rd','').replace('th','').remove(',');
            System.debug('day = '+day);
            String year = rawQuestionsDateSplit[size - 4];  
            System.debug('year = '+year);
            try{
                Date parsedDate = Date.parse(''+monthInt+'/'+day+'/'+year);
                System.debug('parsedDate = '+parsedDate);
                return parsedDate;
            }catch(Exception e){
                return null;
            }
        }
        return Null;
    }*/
    
    public datetime getDatetime(String dateString){
        datetime returnDatetime;
        List<String> dateParts = dateString.replaceAll('\n', '').replaceAll('(\\s+)', ' ').trim().split(' ');
        system.debug('****dateParts.size() = '+dateParts.size());
        system.debug('****dateParts = '+dateParts);
        String dateStr;
        if(dateParts.size() == 8){
            if(months.get(dateParts[1].toLowerCase())!=Null){
                dateStr = ''+months.get(dateParts[1].toLowerCase())+'/'+dateParts[2].replace('st','').replace('nd','').replace('rd','').replace('th','').replaceall(',','')+'/'+dateParts[3];
                system.debug('****dateStr = '+dateStr);
            }else{
                return Null;
            }
            try{
                returnDatetime = Datetime.parse(dateStr+' '+dateParts[5]+' '+dateParts[6]);
            }catch(Exception ex){
                return Null;
            }
        }
        return returnDatetime;
    }
    
    public List<String> getGenericDate(String stringToParse){
        Set<String> daysSet = new Set<String>{'sunday','monday', 'tuesday', 'wednesday','thursday','friday','saturday'};
        List<String> dateStrings = new List<String>();
        String searchTerm = 'day';
        Pattern p = Pattern.compile('(?i)\\b\\w*' + Pattern.quote(searchTerm) + '\\w*\\b');//\\w*\\b
        Matcher m = p.matcher(stringToParse);
        
        if(m.find()) {
          do{
              String day = m.group();
              if(daysSet.contains(day.toLowerCase())){
                  Pattern q = Pattern.compile(day+'(.*?)(EST|PST|IST)');              
                  Matcher n = q.matcher(stringToParse);
                  if(n.find()) {
                      do{
                          dateStrings.add(n.group());
                      }while(n.find());
                  }
              }
          }while(m.find());
        }
        system.debug('dateStrings ='+dateStrings);
        return dateStrings;
    }
    
    public Task_Order__c updateTaskOrder(Task_Order__c taskOrder, String toAddress){
        try{
            if(Schema.sObjectType.Contract_Vehicle__c.isAccessible() && Schema.sObjectType.Task_Order__c.isCreateable()){ 
                List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress Limit 10000];
                System.debug('*********contract vechcle size  = '+ conVehicalList.size());
                
                if(!conVehicalList.isEmpty()){
                    taskOrder.Contract_Vehicle__c = conVehicalList[0].Id;
                    // taskOrder.Task_Order_Contracting_Organization__c = conVehicalList[0].Account_Name__c;
                    
                    System.debug('****email subject'+emailSubject);
                    System.debug('****email subject length = '+emailSubject.length()); 
                    taskOrder.Name =  taskOrderNumber;
                    taskOrder.Task_Order_Title__c = checkValidString(titledName , 255);
                    taskOrder.Release_Date__c = Date.today();
                    /*if(questionsDueDate!=null){            
                        taskOrder.Questions_Due_Date__c = questionsDueDate;
                    }
                    if(proposalDueDate!=null){            
                        taskOrder.Due_Date__c = proposalDueDate;
                    } */
                    
                    if(questionsDueDate!=null){
                        taskOrder.Questions_Due_Date__c = questionsDueDate;
                    }
                    System.debug('proposalDueDateTime '+proposalDueDateTime);
                    if(proposalDueDateTime!=null){
                        taskOrder.TM_TOMA__Proposal_Due_DateTime__c = proposalDueDateTime;
                        taskOrder.Due_Date__c = proposalDueDate;
                    }  
                    //taskOrder.Contract_Vehicle_picklist__c = 'ALLIANT - ALLIANT FULL AND OPEN';
                    //taskOrder.Email_Reference__c = checkValidString(getTrimedEmailSubject(emailSubject), 255);
                    
                    //setting Contact details
                    List<Contact> contactList = new List<Contact>();
                    if(Schema.sObjectType.Contact.isAccessible()){ 
                        contactList =  [Select id, Email from Contact WHERE Email = :senderEmailId];
                    }
                    
                    Contact contact;
                    if(!contactList.isEmpty()){
                        contact = contactList.get(0);   
                        contact.Title = contactTitle;
                        DMLManager.updateAsUser(new List<Contact>{contact});
                        //update new List<Contact>{contact};
                    }else{
                        String []salutation = new String[]{'Mr.','Ms.','Mrs.','Dr.','Prof.'}; 
                            contact = new Contact();
                        List<String> split =  contactName.split(' ');
                        
                        contact.FirstName = split.get(0);
                        contact.LastName = split.get(1);   
                        contact.Title = contactTitle;
                        contact.Is_FedTom_Contact__c = true;
                        if(conVehicalList[0].Account_Name__c != null){
                            contact.AccountId = conVehicalList[0].Account_Name__c;
                        }
                        contact.Email = senderEmailId;
                        DMLManager.insertAsUser(new List<Contact>{contact});
                        //insert new List<Contact>{contact};
                    }
                    
                    
                    try{
                        if(taskOrder.Customer_Agency__c != null){
                            contact.AccountId = taskOrder.Customer_Agency__c;
                            //DMLManager.upsertAsUser(con);
                            if(contact.Id == null){
                                // if Task Order with Customer Organization is not having a Contact or if a different Contact(i.e. a Contact with different Email Id) is found
                                //DMLManager.insertAsUser(con);
                                insert contact;
                            }else{
                                // if Task Order with Customer Organization is having a Contact, then update that Contact
                                //DMLManager.updateAsUser(con);
                                update contact;
                            }
                        }
                        //else{
                        // if(contact.id ==null)
                        //   DMLManager.insertAsUser(contact);                    
                        // }                                
                    }catch(Exception e){
                        System.debug('Error in contact creation - '+e.getMessage()+':'+e.getLineNumber());
                    }   
                    
                    
                    if(contactTitle.equals('Contracting Specialist')){
                        taskOrder.Contract_Specialist__c = contact.id;
                    }
                    if(contactTitle.equals('Contracting Officer')){
                        taskOrder.Contracting_Officer__c = contact.id;
                    }
                    //End Setting contact details
                }
            }       
        }catch(Exception e){
            System.debug('error message = '+e.getMessage()+'\tline number = '+e.getLineNumber());
        }
        return taskOrder;
    }
    
    public string getTaskOrderNumber(String emailSubject){
        try{
            String[] words = emailSubject.split(' ');
            String taskOrderNum = '';
            //taskOrderNum.las
            for(Integer i = 0; i<words.size();i++){
                if(words[i].startsWith('ID')){
                    while(words[i].length() !=0){                    
                        try{
                            Integer index = words[i].length();
                            String temp = words[i].substring(index -2);
                            if(temp.startsWith('-')){
                                taskOrderNum += words[i]; 
                                return taskOrderNum;
                            }
                            Integer.valueOf(temp);
                            taskOrderNum += words[i]; 
                            return taskOrderNum.replace('ID','').trim();
                        }catch(Exception e){
                            taskOrderNum += words[i]+' ';
                            i++;
                            continue;
                        }
                        
                    }
                    
                }
            }
            if(taskOrderNumber != null){
                taskOrderNum = taskOrderNum.replace('ID','').trim();
            }
            return taskOrderNumber;
        }catch(Exception e){
            throw new ContractVehicleNotFoundException(MessageConstantController.TASK_ORDER_NOT_FOUND);
        }
        return null;
    }
}