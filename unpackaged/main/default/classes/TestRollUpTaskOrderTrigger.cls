@isTest
public with sharing class TestRollUpTaskOrderTrigger {
    public static TestMethod void RollUpTaskOrderTriggerTest(){
        List<Profile> profileList = [SELECT Id FROM Profile where (Name = 'Custom Partner Community User' OR Name = 'Custom Partner Community Login User' OR Name  = 'Custom Customer Community Plus Login User') Limit 1];
        if(profileList != null && profileList.size()>0){
            Account acc = TestDataGenerator.createAccount('TaskOrderCapture');
            insert acc;
            
            Contact con = TestDataGenerator.createContact('Avanti',acc);
            insert con;
            
            User usr = TestDataGenerator.createUser(con,TRUE,'test1', 'atest11','test1RollUpTaskOrder1@gmail.com','taskordercapture1@demo.com','atest12',profileList[0],'America/Los_Angeles','en_US','UTF-8','en_US');
            insert usr;    
            
            Contract_Vehicle__c contractVehicle = TestDataGenerator.createContractVehicle('TMconVehicle',acc);
            //contractVehicle.Won_Task_Order_Value__c = 16.00;
            insert contractVehicle;
            
            Vehicle_Partner__c vehicalPartner = TestDataGenerator.createVehiclePartner(acc,contractVehicle);
            insert vehicalPartner;
            
            Contract_Vehicle_Contact__c vehicalContact = TestDataGenerator.createContractVehicleContact(con,contractVehicle);
            
            insert vehicalContact;
            
            Task_Order__c taskOrder = TestDataGenerator.createTaskOrder('TMtaskOrder', acc, contractVehicle);
            taskOrder.TM_TOMA__Task_Order_Ceiling_Value_K__c = 1200.00 ;
            taskOrder.Stage__c='Closed Won' ;
            insert taskOrder;
            
            Teaming_Partner__c teamingPartner = TestDataGenerator.createTeamingPartner(vehicalPartner,taskOrder);
            insert teamingPartner;
            
            Task_Order_Contact__c teamingContact = TestDataGenerator.createTaskOrderContact(taskOrder, con);
            insert teamingContact;
            
            test.startTest();
            Contract_Vehicle__c conVeh = [SELECT id,Won_Task_Order_Value__c FROM Contract_Vehicle__c WHERE id =: contractVehicle.id ];
            
            System.assertEquals(conVeh.Won_Task_Order_Value__c , 1200.00);
            test.stopTest();
         }
      }
  }