/*****************************************************************************************************************************************
Description : The controller is used to populate data of Vehicle Partner and  add it to the related list of Contract Vehicle Contacts.
             
VF Page     : TM_VehiclePartnerControllerPage
Developer   : Avanti
*****************************************************************************************************************************************/

global with sharing class TM_VehiclePartnerPageController {
    
    public Id contractVehicleId {get;set;}
    public List<Vehicle_Partner__c> vehiclePartnerList {get;set;}
    public List<VehiclePartnerWrapper> vehiclePartnerWraList {get;set;}
    //public List<User> partnerContactList {get;set;}
   // public List<Contact> conList {get;set;}
   // public List<Contact> conInputList {get;set;}
    public List<ContactWrapper> conWraList {get;set;}
   // public List<Contract_Vehicle_Contact__c> conVehicleContactList{get;set;}
    public String isValidUser{get; set;}
    
    public TM_VehiclePartnerPageController(ApexPages.StandardController controller) {
       isValidUser = LincenseKeyHelper.checkLicensePermitionForFedTOM();
        if(isValidUser != 'Yes')
            return;
        
        if(Schema.sObjectType.Contact.isAccessible() && Schema.sObjectType.User.isAccessible() && Schema.sObjectType.Contract_Vehicle_Contact__c.isAccessible() ){  
        contractVehicleId = ApexPages.CurrentPage().getparameters().get('cid');
        try{
            if(contractVehicleId != null){
                vehiclePartnerList = new List<Vehicle_Partner__c>();
                vehiclePartnerList =[SELECT id, name, Contract_Vehicle__c, Partner__c, Partner__r.Name, Point_of_Contact__c, Role__c FROM Vehicle_Partner__c WHERE Contract_Vehicle__c =: contractVehicleId ];
                system.debug('vehiclePartnerList ==='+vehiclePartnerList );
            }
            
            if(vehiclePartnerList != null && vehiclePartnerList.size() > 0){
                vehiclePartnerWraList = new List<VehiclePartnerWrapper>();
                for(Vehicle_Partner__c vehiclePartner : vehiclePartnerList ){
                    VehiclePartnerWrapper vehiclePartnerWra = new VehiclePartnerWrapper(vehiclePartner);
                    vehiclePartnerWraList.add(vehiclePartnerWra);
                    system.debug('vehiclePartnerWraList=='+vehiclePartnerWraList);
                }
            }
        }
        catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
      }
    }
   
   // ShowContact() used to show all "Vehicle Partner" list on Vf page. 
    public void ShowContact(){
    
      if(Schema.sObjectType.Contact.isAccessible() && Schema.sObjectType.User.isAccessible() && Schema.sObjectType.Contract_Vehicle_Contact__c.isAccessible() ){
       
        Set<Id> contactIdSet = new Set<Id>();
        Set<Id> accountIdSet = new Set<Id>();
        List<Contact> conList = new List<Contact>();
        List<Contact> conInputList = new List<Contact>();
        List<User> partnerContactList = new List<User>();
        List<Contact> allConList = new List<Contact>();
        List<Contract_Vehicle_Contact__c> conVehicleContactList = new List<Contract_Vehicle_Contact__c>();
        
        try{
          /*  partnerContactList = [SELECT id, name,ContactId FROM User WHERE  (Profile.UserLicense.Name='Partner Community' OR Profile.UserLicense.Name= 'Partner Community Login' 
                                                                               OR Profile.UserLicense.Name='Customer Community Plus' OR Profile.UserLicense.Name = 'Customer Community Plus Login')]; 
            system.debug('partnerContactList==='+ partnerContactList);
            
            if(partnerContactList != null && partnerContactList .size() > 0 ){
                for(User usr : partnerContactList ){
                    contactIdSet.add(usr.ContactId);
                }
            }
            
           
           allConList = [Select id, name from contact];
           if(!allConList.isEmpty()){
               for(Contact con : allConList){
                   contactIdSet.add(con.id);
               }
           } */
            
            if(vehiclePartnerWraList != null && vehiclePartnerWraList.size() > 0){
                for(VehiclePartnerWrapper vehiclePartnerWra : vehiclePartnerWraList){
                    if(vehiclePartnerWra.status == TRUE){
                        accountIdSet.add(vehiclePartnerWra.vehiclePartner.Partner__c);
                    }
                }
            }
            
            Set<Id> conVehicleContactSet = new Set<Id>();
            conVehicleContactList = [SELECT id, name, Contact__c, Contract_Vehicle__c, Main_POC__c, Receives_Mass_Emails__c  FROM Contract_Vehicle_Contact__c WHERE Contract_Vehicle__c  =: contractVehicleId]; 
            
            if(conVehicleContactList != null){
                for(Contract_Vehicle_Contact__c conVehicleContact : conVehicleContactList ){
                    conVehicleContactSet.add(conVehicleContact.Contact__c);
                }
            }
            
           // conInputList  = [SELECT id, name, Email, AccountId, Account.Name FROM Contact WHERE AccountId IN : accountIdSet AND id IN : contactIdSet];
            conInputList  = [SELECT id, name, Email, AccountId, Account.Name FROM Contact WHERE AccountId IN : accountIdSet];
            Map<id, Contact> contactMap = new Map<id,Contact>(conInputList  );
            
            for(Id contactId : contactMap.keyset() ){
                if(!conVehicleContactSet.contains(contactId)){
                   Contact con =  contactMap.get(contactId);
                   
                   
                   if(con != null){
                       conList.add(con);
                   }
                }
            }
            
            conWraList = new List<ContactWrapper>();//chages according to issue when there is no contact on account it shows previous contact list 
            if(conList != null && conList.size() > 0){
                //conWraList = new List<ContactWrapper>();
                for(Contact con : conList){
                    ContactWrapper conWra = new ContactWrapper(con);
                    conWraList.add(conWra);
                    system.debug('conWraList=='+ conWraList);
                }
            }  
         }   
         catch(Exception e){
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
         }   
      }  
    }
    
    // addPartner() method used to add vehicle partner as a contract vehicle contact.
    public PageReference addPartner(){
      if(Schema.sObjectType.Contact.isAccessible() &&  Schema.sObjectType.Contract_Vehicle_Contact__c.isAccessible() ){  
       
        List<Contract_Vehicle_Contact__c> conVehicleContactList = new List<Contract_Vehicle_Contact__c>(); 
        Set<ContactWrapper> conAddWraSet  = new Set<ContactWrapper>();
        
        if(conWraList != null && conWraList.size() > 0){
            for(ContactWrapper conWra : conWraList){
                if(conWra.conStatus == TRUE ){
                    conAddWraSet.add(conWra);
                }                                
            }
        }
        
        if(conAddWraSet  != null && conAddWraSet.size() > 0){
            for(contactWrapper conW : conAddWraSet  ){
                Contract_Vehicle_Contact__c conVehicleContact = new Contract_Vehicle_Contact__c();
                conVehicleContact.Contract_Vehicle__c = contractVehicleId ;
                conVehicleContact.Contact__c  = conW.con.id  ;
                conVehicleContact.Main_POC__c = conW.Main_POC ;
                conVehicleContact.Receives_Mass_Emails__c   = conW.Receives_Mass_Emails ;
                conVehicleContactList.add(conVehicleContact);
            }
        }
        
        try{
            if(conVehicleContactList.size() > 0){
               // upsert conVehicleContactList;
                DMLManager.upsertAsUser(conVehicleContactList);
            }
        }
        catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
      
       
        PageReference pg = new PageReference ('/'+ contractVehicleId  );
        pg.setRedirect(true);
        return pg;
        
      }
      return null;
    }
    
   /********************** Wrapper Classes for Vehicle Partner and contact object ******************/
    public class VehiclePartnerWrapper{
        public Vehicle_Partner__c vehiclePartner{get;set;}
        public Boolean status {get;set;}
        
        public VehiclePartnerWrapper(Vehicle_Partner__c vehiclePartner){
            this.vehiclePartner = vehiclePartner;
        }
    }
    
    public class ContactWrapper{
        public Contact con {get;set;}
        public Boolean conStatus {get;set;}
        public Boolean Main_POC {get;set;}
        public Boolean Receives_Mass_Emails {get;set;}
        
        public ContactWrapper(Contact con){
            this.con = con;
        }
    }
     /**********************************************************************************************/ 
}