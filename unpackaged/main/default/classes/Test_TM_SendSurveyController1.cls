@isTest
public class Test_TM_SendSurveyController1 {
    public static TestMethod void Test_TM_SendSurveyController(){
        List<Profile> profileList = [SELECT Id FROM Profile where (Name = 'Custom Partner Community User' OR Name = 'Custom Partner Community Login User' OR Name  = 'Custom Customer Community Plus Login User') Limit 1];
        if(profileList != null && profileList.size()>0){
            
            Account acc = TestDataGenerator.createAccount('TaskOrderCapture');
            insert acc;
            
            List<Contact> conList = new List<Contact>();
            Contact con = TestDataGenerator.createContact('test1',acc);
            conList.add(con);
            Contact con1 = TestDataGenerator.createContact('test2',acc);
            conList.add(con1);
            insert conList;
            
           // User usr = TestDataGenerator.createUser(con,TRUE,'test1', 'atest1','test1@gmail.com','taskordercapture@demo.com','atest1',profileList[0],'America/Los_Angeles','en_US','UTF-8','en_US');
           // insert usr;
            
            Contract_Vehicle__c contractVehicle = TestDataGenerator.createContractVehicle('TMconVehicle',acc);
            insert contractVehicle;
            
            Task_Order__c taskOrder = TestDataGenerator.createTaskOrder('TMtaskOrder', acc, contractVehicle);
            insert taskOrder;
            
            Task_Order_Contact__c tOrderContact = new Task_Order_Contact__c(Contact__c = con.Id, Task_Order__c = taskOrder.Id);
            insert tOrderContact;
            
            Vehicle_Partner__c vehiclePartner = TestDataGenerator.createVehiclePartner(acc,contractVehicle );
            insert vehiclePartner;
            
            Teaming_Partner__c teamPartner = TestDataGenerator.createTeamingPartner(vehiclePartner , taskOrder);
            insert teamPartner;
            
            Survey__c sur = TestDataGenerator.createSurvey();
            insert sur;
            
            Survey__c sur1 = TestDataGenerator.createSurvey();
            insert sur1;
            
            Attachment attachment = new Attachment();
            attachment.Name = 'Test Attachment';
            attachment.ParentId = taskOrder.Id;
            attachment.Body = Blob.valueOf('Test Data');
            insert attachment;
            
            Task_Order_Questionnaire__c taskOrderQue = TestDataGenerator.createTaskOrderQue('https://idiq-sms-demo-developer-edition.na31.force.com/', 'https://idiqcapture-developer-edition.na31.force.com/partner');
            insert taskOrderQue;
            
            List<Questionaries_Email_Template__c> QETTemp = new List<Questionaries_Email_Template__c>();
            Boolean tempFlas = true;
            for(EmailTemplate eTemplate : [SELECT Id FROM EmailTemplate WHERE folder.name = 'TOMA Email Templates']){
                if(tempFlas){
                    QETTemp.add(TestDataGenerator.addQuestionarieEmailTemplate(sur.Id,eTemplate.Id));
                    tempFlas = false;
                }else{
                    QETTemp.add(TestDataGenerator.addQuestionarieEmailTemplate(sur1.Id,eTemplate.Id));
                    tempFlas = true;
                }                
            }
            
            Insert QETTemp;
            
            
            
            Test.startTest();
            ApexPages.CurrentPage().getparameters().put('id',taskOrder.id);
            ApexPages.StandardController controller  = new ApexPages.StandardController(taskOrder);
            TM_SendSurveyController1 surveyController = new TM_SendSurveyController1(controller);
            surveyController.flag = true;
            surveyController.emailBodyToSet = 'body';
            surveyController.subjectToSet = 'subject';
            surveyController.surveyStrId = sur.Id;
            surveyController.selectEmailTemplete();
            surveyController.emailStrId = (new List<Id>(surveyController.emailIdNameMap.keyset()))[0];
            for(TM_SendSurveyController1.ContactWrapper conWrp : surveyController.conWraList ){
                conWrp.conStatus = true;
            }
            PageReference pgRef = surveyController.sendEmail();
            //System.assertNotEquals(pgRef, Null);
            System.assertEquals(surveyController.getFolderId().length(), 15);
            Test.stopTest();
        }
    }
}