({
     doCheckLicense : function(component, event, helper) {
        var action = component.get("c.checkLicensePermition1");
        action.setCallback(this, function(a) {
            //alert('doCheckLicense');
            if(a.getReturnValue() === 'Yes')
            {
                // alert('inside if');
                component.set("v.CheckLicense", 'Yes');
                helper.getTMContractVehicle1(component, event, helper);
                helper.conValTableList(component, event, helper);
                helper.checkValueTablePermissions(component, event, helper); // Check Value Table Permissions
            }
            else
            {
                // alert('inside else');
                component.set("v.CheckLicense", a.getReturnValue());
            }
            
        });
        $A.enqueueAction(action);  
    },    
    addContValRow : function(component, event, helper) {
        event.preventDefault();
        var valTableCotValList=component.get("v.valueTableContVal");
        var action = component.get("c.newRowContValTable"); //call apex controller method
        action.setParams({
            "contractVehicalId": component.get("v.recordId")
        });
        action.setCallback(this,function(a){
            var state= a.getState();
            if(state=="SUCCESS"){
                var newConVal=a.getReturnValue();
                if(newConVal != null){
                    newConVal.TM_TOMA__Start_Date__c='';
                    valTableCotValList.push(newConVal);
                    component.set("v.valueTableContVal",valTableCotValList);
                }else{
                    alert('ERROR:- You do not have CREATE permission for Value Table object.'); // New Change - 16/08/2018
                }
            }
            else if (state == "ERROR") 
            {
                console.log(a.getError());            
            }
        });
        $A.enqueueAction(action);
    },
    getTMContractVehicle : function(component, event, helper) {
        helper.getTMContractVehicle1(component, event, helper);
    },
    
    valueTableContValList : function(component, event, helper) {
        helper.conValTableList(component, event, helper);
    },
    
    removeContValRow : function(component, event, helper) {
        var conf = confirm("Are you sure you want to delete this component?");
        if(conf== true)
        {
            var valTableCotValList=component.get("v.valueTableContVal");
            var delCotValId=component.get("v.delConValId");
            var conValTablesize=component.get("v.valueTableContValSizeFlag");
            var selectedItem = event.currentTarget;
            var removeCounter = selectedItem.id;
            if (removeCounter<conValTablesize) {
                delCotValId+=',';
                conValTablesize-=1;
                component.set("v.valueTableContValSizeFlag",conValTablesize);
                delCotValId=delCotValId+valTableCotValList[removeCounter].Id;
                component.set("v.delConValId",delCotValId);
            } 
            valTableCotValList.splice(removeCounter, 1);
            component.set("v.valueTableContVal", valTableCotValList);
        }
    },
    saveContValTable : function(component, event, helper) {
        event.preventDefault();
        helper.saveContValTableList(component, event, helper);
        
    }
    
    /*    getValueTableJson:function(component){
        var valueTableJSON = '[';
        
        for (var i=0; i<valTableCotValList.length; i++){        
            if(valueTableJSON.length()>1){
                valueTableJSON =  ',{"attributes":{"type":"Value_Table__c"},';
                var description = valTableCotValList[i].Description__c === undefined?'':valTableCotValList[i].Description__c;
                //alert('description = '+description);
                var startDate = valTableCotValList[i].Start_Date__c;
                //var endDate = valTableCotValList[i].End_Date__c;
                var dollars = valTableCotValList[i].Dollars__c;
                //var Exercised_Contract_Value_Options__c  = valTableCotValList[i].Exercised_Contract_Value_Options__c ;
                var appendData = '"Description__c":"'+ description+'" , "Start_Date__c":"'+startDate+'","Dollars__c":'+dollars+',"RecordTypeId":"012370000001WYq" }]';
                valueTableJSON += appendData;
                //alert('descriptionif = '+valueTableJSON);
            }else{
                valueTableJSON =  '{"attributes":{"type":"Value_Table__c"},';
                var description = valTableCotValList[i].Description__c === undefined?'':valTableCotValList[i].Description__c;
               // alert('descriptionelse = '+description);
                var startDate = valTableCotValList[i].Start_Date__c;
                //var endDate = valTableCotValList[i].End_Date__c;
                var dollars = valTableCotValList[i].Dollars__c;
                //var Exercised_Contract_Value_Options__c  = valTableCotValList[i].Exercised_Contract_Value_Options__c ;
                var appendData = '"Description__c":"'+ description+'" , "Start_Date__c":"'+startDate+'","Dollars__c":'+dollars+',"RecordTypeId":"012370000001WYq" }]';
                valueTableJSON += appendData;
            }
        }   
        //alert('valueTableJSON'+valueTableJSON);
        valueTableJSON = ']';  
        component.set("v.jsonString", valueTableJSON);
        return ''
    }*/
})