public with sharing class TM_BookingTable {
    public static Id bookingsRecordTypeId;
    public static Value_Table__c valueTable1{get;set;}
    @AuraEnabled public Boolean isCreateValueTable {get;set;}
    @AuraEnabled public Boolean isUpdateValueTable {get;set;}
    @AuraEnabled public Boolean isDeleteValueTable {get;set;}
    
    /* @AuraEnabled
public static List<String> getPicklistValues(String ObjectApi_name,String Field_name){ 
List<String> lstPickvals=new List<String>();
Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectApi_name);//From the Object Api name retrieving the SObject
Sobject Object_name = targetType.newSObject();
Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
List<Schema.PicklistEntry> pick_list_values = field_map.get(Field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
lstPickvals.add(a.getValue());//add the value  to our final list
}

return lstPickvals;
}*/
    
    @AuraEnabled
    public static String checkLicensePermition1()
    {
        return LincenseKeyHelper.checkLicensePermitionForFedCLM();
    }
    
    @AuraEnabled
    public static TM_BookingTable getValueTablePermissions(){
        TM_BookingTable bookingTable = new TM_BookingTable();
        bookingTable.isCreateValueTable = false;
        bookingTable.isUpdateValueTable = false;
        bookingTable.isDeleteValueTable = false;
        
        if(Schema.sObjectType.Value_Table__c.isCreateable())
            bookingTable.isCreateValueTable = true;
        
        if(Schema.sObjectType.Value_Table__c.isUpdateable())
            bookingTable.isUpdateValueTable = true;
        
        if(Schema.sObjectType.Value_Table__c.isDeletable())
            bookingTable.isDeleteValueTable = true;
        return bookingTable;
    }
    
    @AuraEnabled
    public static list<String> getPickvalIfNotABookingWhy()
    {
        if(Schema.sObjectType.Value_Table__c.isAccessible()){
            list<String> options = new list<String>();
            options.add('--None--');
            Schema.DescribeFieldResult feildResult = Value_Table__c.If_not_a_Booking_why__c.getDescribe();
            LIST<Schema.PicklistEntry> ple = feildResult.getPicklistValues();
            for(Schema.PicklistEntry f : ple)
            {
                options.add(f.getLabel());
                
                
            }
            return options;
        }
        else
        {
            return null;
        }
    }
    @AuraEnabled
    public static Contract_Vehicle__c getContractVehical(Id contractVehicalId)
    {
        if(Schema.sObjectType.Contract_Vehicle__c.isAccessible()){
            Contract_Vehicle__c contVech = new Contract_Vehicle__c();
            if(contractVehicalId!=Null)
            {
                contVech = [select Id,Total_Contract_Value_with_all_Options__c,Total_Exercised_Value_with_Options__c,Initial_Contract_Funding_at_Award__c from Contract_Vehicle__c where Id =: contractVehicalId];
            }
            
            return contVech;
        }
        else
        {
            return null;
        }
    }
    
    
    
    @AuraEnabled
    public static GetBookingTableData addNewBookingTableRowInWrap(Id contractVehicalId)
    {
        system.debug('inside addNewBookingTableRow');
        bookingsRecordTypeId = Schema.SObjectType.Value_Table__c.getRecordTypeInfosByName().get('Bookings').getRecordTypeId();
        system.debug('bookingsRecordTypeId'+bookingsRecordTypeId);
        
        if(Schema.sObjectType.Value_Table__c.isCreateable()){
            Value_Table__c valueTable = new Value_Table__c (RecordTypeId = bookingsRecordTypeId,Contract_Vehicle__c = contractVehicalId,Modification__c='',Description__c='',Start_Date__c=Date.today(),Is_this_a_Booking__c=false,Increase_to_Total_Element_Value__c=00.00,Dollars__c=00.00);//,If_not_a_Booking_why__c=''
            GetBookingTableData newBookTab =new GetBookingTableData(valueTable);
            return newBookTab;
        }
        else
        {
            return null;
        }
    }
    
    @AuraEnabled
    public static String saveBookingTableData(String bookingTableList,String contractVehicalId,string delBookingIdsStr)
    {
        String errorMessage = '';
        try
        {
            System.debug('bookingTableList '+bookingTableList);
            if(delBookingIdsStr!=Null && delBookingIdsStr!='')
            {
                // "If" condition and "Else" block are newly added to restrict user access according to the object permission(17/08/2018)
                if(Schema.sObjectType.Value_Table__c.isDeletable()){
                    string[] delIds=delBookingIdsStr.split(',');
                    delIds.remove(0);
                    List<Value_Table__c> delList = [Select Id from Value_Table__c where Id IN: delIds];
                    DMLManager.deleteAsUser(delList);
                }/*else{
                    errorMessage = 'You do not have DELETE permission for Value Table object.\n';
                }*/
            }
            List<Value_Table__c> vtList;
            if(bookingTableList != Null)
            {
                // "If" condition and "Else" block are newly added to restrict user access according to the object permission(17/08/2018)
                if(Schema.sObjectType.Value_Table__c.isUpdateable()){
                    vtList = (List<Value_Table__c>)JSON.deserialize(bookingTableList, List<Value_Table__c>.class);
                    for(Value_Table__c vt : vtList)
                    {
                        If(vt.Id==Null)
                        {
                            vt.Contract_Vehicle__c=contractVehicalId;
                        }
                    }
                    //we are not using DMLManager beacuse during JSON.deserialize string it gives error and  Error :-  186 unexpected token: '(' js and apex serialise and deserilize string is mismatch 
                    System.debug('vtList '+vtList);
                    upsert(vtList);
                }/*else{
                    if(errorMessage == ''){
                        errorMessage = 'You do not have UPDATE permission for Value Table object.\n';
                    }else{
                        errorMessage = 'You do not have UPDATE and DELETE permission for Value Table object.\n';
                    }
                }*/
                
                //newlly added 17-08 
                if(Schema.sObjectType.Value_Table__c.isCreateable() && !Schema.sObjectType.Value_Table__c.isUpdateable()){
                    vtList = (List<Value_Table__c>)JSON.deserialize(bookingTableList, List<Value_Table__c>.class);
                    List<Value_Table__c> newVtList = new List<Value_Table__c>();
                    for(Value_Table__c vt : vtList)
                    {
                        System.debug('vtid = '+vt.Id);
                        If(vt.Id==Null){
                            vt.Contract_Vehicle__c=contractVehicalId;
                            newVtList.add(vt);
                        }
                    }
                    //we are not using DMLManager beacuse during JSON.deserialize string it gives error and  Error :-  186 unexpected token: '(' js and apex serialise and deserilize string is mismatch
                    insert newVtList;
                }
            }
        }
        catch(Exception e)
        {
            // To show error message on user-side(17/08/2018)
            errorMessage += 'Error :- Server Error '+e.getMessage();
            System.debug(' Error :-  '+e.getLineNumber()+' '+e.getMessage());
        }
        return errorMessage;
    }
    
    @AuraEnabled
    public static Boolean isLightningPage(){
        Boolean var = UserInfo.getUiThemeDisplayed() == 'Theme4d';
        System.debug('@@@@@'+var);
        return var;
    } 
    
    
    @AuraEnabled
    public static List<GetBookingTableData> getBookingValueTableList1 (String contractVehicalId)
    {
        if(Schema.sObjectType.Value_Table__c.isAccessible()){
            bookingsRecordTypeId = Schema.SObjectType.Value_Table__c.getRecordTypeInfosByName().get('Bookings').getRecordTypeId();
            List<Value_Table__c> ValueTablebookingList = new List<Value_Table__c> ();
            ValueTablebookingList=[select RecordTypeId,Contract_Vehicle__c,Modification__c,Description__c,Start_Date__c,Is_this_a_Booking__c,Increase_to_Total_Element_Value__c,Dollars__c,createddate from Value_Table__c where Contract_Vehicle__c =: contractVehicalId AND RecordTypeId =: bookingsRecordTypeId ORDER BY Start_Date__c ASC,createddate ASC];//If_not_a_Booking_why__c,
            if(ValueTablebookingList.size()>0){
                List<GetBookingTableData> bookingTabList= new List<GetBookingTableData>(); 
                for(Value_Table__c bookVal : ValueTablebookingList)
                {
                    GetBookingTableData temp =new GetBookingTableData(bookVal);
                    temp.bookTab = bookVal;
                    bookingTabList.add(temp);
                }
                return bookingTabList;
            }
            else{
                return Null;
            }
        }
        else
        {
            return null;
        }
    }
    
    /* public class GetBookingTableData{

@AuraEnabled
public Value_Table__c bookTab;


public GetBookingTableData(Value_Table__c bookTab1)
{
bookTab=bookTab1;

System.debug('bookTab '+bookTab);
}
}*/
}