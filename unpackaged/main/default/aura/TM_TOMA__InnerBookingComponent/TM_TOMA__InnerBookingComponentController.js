({
    getIfNotABookingWhy: function(component, event, helper) {
        var action = component.get("c.getPickvalIfNotABookingWhy"); //call apex controller method
        var opts = []; //create array
        action.setCallback(this,function(a){
            //var queTypelist = a.getReturnValue();
            //alert(queTypelist);
            var custs = [];
            var conts = a.getReturnValue();
            for (var key in conts ) {
                custs.push({value:conts[key], key:key});
            }
            component.set("v.IfNotABookingWhy", custs);
            
            //component.set("v.IfNotABookingWhy",queTypelist);  //setting value to variable of Input field
        });
        $A.enqueueAction(action);  
    },/*,
    removeBookingRow: function(component, event, helper) {
        
      alert("remove  me");
    }*/
    selectCurrentModificationValue :function(component, event, helper){
        var ModificationText1=component.find("ModificationText");
        var obj=component.get("v.valueTableBooking");
        obj.TM_TOMA__Modification__c=ModificationText1.get("v.value");
        component.set("v.valueTableBooking",obj);
    },
    selectCurrentDescriptionValue :function(component, event, helper){
        var DescriptionTextArear1=component.find("DescriptionTextArear");
        var obj=component.get("v.valueTableBooking");
        obj.TM_TOMA__Description__c=DescriptionTextArear1.get("v.value");
        component.set("v.valueTableBooking",obj);
    },
    selectCurrentValue1 :function(component, event, helper){
        
        var picklist=component.find("o1");
        //component.set("v.selectedvalue",picklist.get("v.value"));
        var obj=component.get("v.valueTableBooking");
        var val= picklist.get("v.value");
        //alert(val);
        obj.TM_TOMA__If_not_a_Booking_why__c=picklist.get("v.value");
        //component.set("v.valueTableBooking",obj);
    },
    selectCurrentValue :function(component, event, helper){
        
        var picklist=component.find("o1");
        //component.set("v.selectedvalue",picklist.get("v.value"));
        var obj=component.get("v.valueTableBooking");
        var val= picklist.get("v.value");
        //alert(val);
        obj.TM_TOMA__If_not_a_Booking_why__c=picklist.get("v.value");
        component.set("v.valueTableBooking",obj);
        var obj=component.get("v.valueTableBooking");
        
    },
    checkCurrencyToDo :function(component, event, helper){
        var currencyVal = event.getSource();
        var valueTableBookingListWrap = component.get("v.valueTableBookingListWrap1");
        var valueTableRow = component.get("v.valueTableRow");
        var obj=component.get("v.valueTableBooking");
        if(valueTableBookingListWrap != undefined){
            if(valueTableRow == 0){
                if(obj.TM_TOMA__Is_this_a_Booking__c == false){
                    obj.TM_TOMA__Dollars__c = 0.0;
                }
                else{
                    obj.TM_TOMA__Dollars__c = obj.TM_TOMA__Increase_to_Total_Element_Value__c;
                }
               helper.helpertocheck(component, event, helper);
            }
            else{
                if(obj.TM_TOMA__Is_this_a_Booking__c == true){
                    var j1 = valueTableRow.valueOf()-1;
                    obj.TM_TOMA__Dollars__c = obj.TM_TOMA__Increase_to_Total_Element_Value__c +valueTableBookingListWrap[j1].bookTab.TM_TOMA__Dollars__c;
                }
                else{
                    var j1 = valueTableRow.valueOf()-1;
                    obj.TM_TOMA__Dollars__c = valueTableBookingListWrap[j1].bookTab.TM_TOMA__Dollars__c;
                }
                helper.helpertocheck(component, event, helper);
            }
        }
        component.set("v.valueTableBooking",obj);
        component.set("v.valueTableBookingListWrap1",valueTableBookingListWrap);
    },
    checkToDo :function(component, event, helper){
        var checkbox = event.getSource();
        var valueTableBookingListWrap = component.get("v.valueTableBookingListWrap1");
        // alert(valueTableBookingListWrap.length);
        var valueTableRow = component.get("v.valueTableRow");
        var obj=component.get("v.valueTableBooking");
        var val= checkbox.get("v.value");
        obj.TM_TOMA__If_not_a_Booking_why__c=val;
        if(val === true){
            if(valueTableRow == 0){
                obj.TM_TOMA__Dollars__c = obj.TM_TOMA__Increase_to_Total_Element_Value__c;
            }
            if(valueTableBookingListWrap != undefined){
                if(valueTableRow == 0){
                    obj.TM_TOMA__Dollars__c = obj.TM_TOMA__Increase_to_Total_Element_Value__c;
                    
                   helper.helpertocheck(component, event, helper);
                }
                else{
                    var j1 = valueTableRow.valueOf()-1;
                    obj.TM_TOMA__Dollars__c = obj.TM_TOMA__Increase_to_Total_Element_Value__c +valueTableBookingListWrap[j1].bookTab.TM_TOMA__Dollars__c;
                   helper.helpertocheck(component, event, helper);
                }
            }
        }
        else{
            if(valueTableBookingListWrap != undefined){
                if(valueTableRow == 0){
                    obj.TM_TOMA__Dollars__c = 0.0;
                   helper.helpertocheck(component, event, helper);
                }
                else{
                    for (var i=1; i<valueTableBookingListWrap.length; i++){
                        if(valueTableRow == i){
                            var j = i.valueOf()-1;
                            obj.TM_TOMA__Dollars__c = valueTableBookingListWrap[j].bookTab.TM_TOMA__Dollars__c;
                        }
                    }
                    helper.helpertocheck(component, event, helper);
                }
            }
        }
        component.set("v.valueTableBooking",obj);
        component.set("v.valueTableBookingListWrap1",valueTableBookingListWrap);
    }
})