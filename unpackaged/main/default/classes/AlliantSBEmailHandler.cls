global with sharing class AlliantSBEmailHandler implements Messaging.InboundEmailHandler {
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        if(Schema.sObjectType.Task_Order__c.isAccessible() && Schema.sObjectType.Contract_Vehicle__c.isAccessible()){ 
            //String myPlainText = email.plainTextBody;       
            String htmlText = email.HTMLBody;
            //stem.debug('email'+htmlText);
            //System.debug('email = '+email.toAddresses.get(0));
            
            
            AllientSBEmailParser parser = new AllientSBEmailParser();
            parser.parse(htmlText);
            
            //System.debug('*******Data After Parsing********\n');
            //System.debug('RFQ ID = '+parser.getRFQId());
            //System.debug('Action = '+parser.getAction());
            //System.debug('Date/Buyer = '+parser.getDate());
            //System.debug('Quotes Due By = '+parser.getQuotesDueBy());
            //System.debug('RFQ Title = '+parser.getRFQTitle());
            
            Task_Order__c taskOrder = parser.setTaskOrder(email.toAddresses.get(0));
            try{
                DMLManager.insertAsUser(taskOrder);
                EmailHandlerHelper.insertBinaryAttachment(taskOrder,email);
                EmailHandlerHelper.insertTextAttachment(taskOrder,email);
                EmailHandlerHelper.insertActivityHistory(taskOrder,Email);
                
            }catch(Exception e){
                System.debug('error message = '+e.getMessage());
            }
        }
        return result;
    }
    
    
    
    
}