({
        doCheckLicense : function(component, event, helper) {
        var action = component.get("c.checkLicensePermition1");
        action.setCallback(this, function(a) {
           //alert('doCheckLicense');
            if(a.getReturnValue() === 'Yes')
            {
               // alert('inside if');
                component.set("v.CheckLicense", 'Yes');
                helper.checkValueTablePermissions(component, event, helper); // Check Value Table Permissions 21-08-2018
                 helper.getContractIncVehicle1(component, event, helper);
                 helper.conValIncModTableList(component, event, helper);
            }
            else
            {
                // alert('inside else');
                component.set("v.CheckLicense", a.getReturnValue());
            }
            
        });
         $A.enqueueAction(action);  
    },
    addIncModFeeRow : function(component, event, helper) {
        event.preventDefault();
        var valueTableIncModFeeList = component.get("v.valueTableIncModFeeList");
        var action = component.get("c.newRowModFeeTable"); //call apex controller method
        action.setParams({           
            "contractVehicalId": component.get("v.recordId")
        });
        action.setCallback(this,function(a){
            var state= a.getState();
            if(state=="SUCCESS"){
                var newConVal=a.getReturnValue();
                // "If" condition and "Else" block are newly added to restrict user access according to the object permission(17/08/2018)
                if(newConVal != null){
                    newConVal.TM_TOMA__Start_Date__c='';
                    newConVal.TM_TOMA__End_Date__c='';
                    valueTableIncModFeeList.push(newConVal);
                    component.set("v.valueTableIncModFeeList",valueTableIncModFeeList);
                }else{
                    alert('ERROR:- You do not have CREATE permission for Value Table object.'); // New Change - 17/08/2018
                }
            }
            else if (state == "ERROR") 
            {
                console.log(a.getError());            
            }
        });
        $A.enqueueAction(action);       
    },
    getContractIncVehicle : function(component, event, helper) {
         helper.getContractIncVehicle1(component, event, helper);
    },
    valueTableIncModList : function(component, event, helper) {
        helper.conValIncModTableList(component, event, helper); // called in helper
    },
    removeIncModFeeRow  : function(component, event, helper) {
        var conf = confirm("Are you sure you want to delete this component?");
        if(conf== true)
        {
            var valueTableIncModFeeList =component.get("v.valueTableIncModFeeList");       
            var delIncModFeeId =component.get("v.delIncModFeeId");       
            var conValIncModTablesize=component.get("v.valueTableIncModFeeSizeFlag");
            var selectedItem = event.currentTarget;
            var removeCounter = selectedItem.id;
            if (removeCounter < conValIncModTablesize) {
                delIncModFeeId+=',';
                conValIncModTablesize-=1;
                component.set("v.valueTableIncModFeeSizeFlag",conValIncModTablesize);
                delIncModFeeId =delIncModFeeId+valueTableIncModFeeList[removeCounter].Id;
                component.set("v.delIncModFeeId",delIncModFeeId);
            } 
            valueTableIncModFeeList.splice(removeCounter, 1);
            component.set("v.valueTableIncModFeeList", valueTableIncModFeeList);
        }
    },
    //save and json method remaining below.
    saveIncModFeeTable : function(component, event, helper) {
        event.preventDefault();
        helper.saveModFeeTableList(component, event, helper); // Defined in helper
    }
  /*  getValueTableJson:function(component){
        var valueTableJSON = '[';
        for (var i=0; i<valueTableIncModFeeList.length; i++){        
            if(valueTableJSON.length()>1){
                valueTableJSON =  ',{"attributes":{"type":"Value_Table__c"},';
                var startDate = valueTableIncModFeeList[i].Start_Date__c;
                var endDate = valueTableIncModFeeList[i].End_Date__c;
                var score =  valueTableIncModFeeList[i].Score__c;
                var dollars = valueTableIncModFeeList[i].Dollars__c;
                var comments = valueTableIncModFeeList[i].Comments__c;
                var appendData = '"Start_Date__c":"'+startDate+'", "End_Date__c":"'+endDate+'","Score__c":'+score+',"Dollars__c":'+dollars+',"Comments__c":"'+comments+'","RecordTypeId":"012370000001WYl" }]';
                valueTableJSON += appendData;
            }else{
                valueTableJSON =  '{"attributes":{"type":"Value_Table__c"},';
                var startDate = valueTableIncModFeeList[i].Start_Date__c;
                var endDate = valueTableIncModFeeList[i].End_Date__c;
                var score =  valueTableIncModFeeList[i].Score__c;
                var dollars = valueTableIncModFeeList[i].Dollars__c;
                var comments = valueTableIncModFeeList[i].Comments__c;
                var appendData = '"Start_Date__c":"'+startDate+'", "End_Date__c":"'+endDate+'","Score__c":'+score+',"Dollars__c":'+dollars+',"Comments__c":"'+comments+'","RecordTypeId":"012370000001WYl" }]';
                valueTableJSON += appendData;
            }
        }    
        valueTableJSON = ']';   
        component.set("v.jsonString", valueTableJSON);
        return ''
    }*/
})