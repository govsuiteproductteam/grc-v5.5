trigger RollUpTaskOrderTrigger on Task_Order__c (After insert, After update, After delete, After Undelete) {
    List<Id> parrentContractVehicleIds;
    Map<Id, Contract_Vehicle__c> contractVehicalIdMap;
    List<Task_Order__c>toList;
    if(trigger.isAfter){
        toList = new List<Task_Order__c>();
        parrentContractVehicleIds = new  List<Id>();
        if(trigger.isInsert || trigger.isUpdate || trigger.isUndelete){
            toList = trigger.new;
        }else{
            toList = trigger.old;
        }
        for(Task_Order__c to : toList){
            if(to.Contract_Vehicle__c !=Null){
                parrentContractVehicleIds.add(to.Contract_Vehicle__c);
            }
        }
        contractVehicalIdMap = new Map<Id, Contract_Vehicle__c>([SELECT Id, won_Task_Order_Value__c FROM Contract_Vehicle__c WHERE id IN :parrentContractVehicleIds]);
        List<AggregateResult> accountTotals = [SELECT SUM(TM_TOMA__Task_Order_Ceiling_Value_K__c)total, TM_TOMA__Contract_Vehicle__c  FROM Task_Order__c WHERE Contract_Vehicle__c IN :parrentContractVehicleIds AND Stage__c='Closed Won'  group by Contract_Vehicle__c LIMIT 10000];
        for(AggregateResult agg : accountTotals){
            if(agg.get('TM_TOMA__Contract_Vehicle__c') != null){
               String mapKey = (String)agg.get('TM_TOMA__Contract_Vehicle__c');
               if(contractVehicalIdMap.get(mapKey)!=Null){
                    contractVehicalIdMap.get(mapKey).won_Task_Order_Value__c = (Decimal)agg.get('total');
               }
           }
        }
        try{
            update contractVehicalIdMap.values();
        }catch(Exception e){}
    }
}