({
    setInnerComponentBody : function(component, event, helper) {
        // component.find("innerDistributionContainer").set("v.body", []);
        var innerDisField = component.get("v.innerDistributionFieldSetWrapperData");
        var distRecord = component.get("v.distributionRecord");
        var rowInd = component.get("v.TableRow");
        var tableSize = component.get("v.tableRowsize");
        var checkNewRow = 'Yes';
        if(component.get("v.tableRowsize") > component.get("v.TableRow"))
        {
            checkNewRow = 'No';
        }
        if(innerDisField.length > 0){
            var configobjFieldDataMap=JSON.parse(JSON.stringify(helper.componentTypeConfigObj));
            var viewFieldDataMap=JSON.parse(JSON.stringify(helper.viewPageFieldsMap));
            for(var j=0; j<innerDisField.length;j++ ){
                var fieldDataMap={};
                
                fieldDataMap["aura:id"] = component.getGlobalId();
                fieldDataMap["label"] = innerDisField[j].label;
                fieldDataMap["access"] = "Global";
                /*  if(innerDisField[j].typeOfField == "STRING"){
                    fieldDataMap["value"] = component.getReference("v.distributionRecord."+innerDisField[j].fieldAPI);
                }
                else*/ /*if(innerDisField[j].typeOfField == "DATETIME"){
                    fieldDataMap["value"] = null;
                }
                    else*/
                if(innerDisField[j].typeOfField == "REFERENCE"){
                    fieldDataMap["isDetailsChildCmp"] = "Yes";
                    fieldDataMap["isRequired"] = true;
                    if(distRecord[innerDisField[j].fieldAPI] != undefined){
                        fieldDataMap["lookUpId"] =distRecord[innerDisField[j].fieldAPI];
                    }else{
                        fieldDataMap["lookUpId"] = '';
                    }
                    fieldDataMap["sObjectType"] =innerDisField[j].sObjectName;
                    fieldDataMap["label"] =innerDisField[j].label;
                    fieldDataMap["pluralLabel"] =innerDisField[j].sObjectName;
                    fieldDataMap["sObjectAPIName"]=innerDisField[j].sObjectName;
                    fieldDataMap["listIconSVGPath"]="/resource/TM_TOMA__SLDS203/assets/icons/standard-sprite/svg/symbols.svg#account";
                    fieldDataMap["listIconClass"]="slds-icon-standard-account";
                    fieldDataMap["recordId"]=component.getReference("v.distributionRecord."+innerDisField[j].fieldAPI);
                    fieldDataMap["lookUpParentLabel"]=innerDisField[j].label;
                    
                    /*Add for undu functionality*/
                    fieldDataMap["index"]=rowInd;
                    fieldDataMap["Api"]=innerDisField[j].fieldAPI;
                    
                    //fieldDataMap["lookupCompId"]=j+'idVeh';
                }
                else if(innerDisField[j].typeOfField == "PICKLIST"){
                    // if(tabWrapList[tabIndex].sectionWrapList[sectionIndex].fieldWrapList[k].isRequired)
                    fieldDataMap["isRequired"] = true;
                    fieldDataMap["fieldName"] = innerDisField[j].fieldAPI;
                    fieldDataMap["fieldLabel"] = innerDisField[j].label;
                    fieldDataMap["selectedPicklist"] = distRecord[innerDisField[j].fieldAPI];
                    fieldDataMap["value"] = component.getReference("v.distributionRecord."+innerDisField[j].fieldAPI);
                    fieldDataMap["selectedvalue"] = component.getReference("v.distributionRecord."+innerDisField[j].fieldAPI);
                 /*Add for undu functionality*/
                    fieldDataMap["index"]=rowInd;
                }
                /* else if(innerDisField[j].typeOfField == "DATE"){
                                            fieldDataMap["value"] = component.getReference("v.distributionRecord."+innerDisField[j].fieldAPI);
                                            fieldDataMap["displayDatePicker"] = "true";
                                            fieldDataMap["format"] = "MM/DD/YYYY";
                                        }   else if(innerDisField[j].typeOfField == "MULTIPICKLIST"){
                                //if(tabWrapList[tabIndex].sectionWrapList[sectionIndex].fieldWrapList[k].isRequired)
                                fieldDataMap["isRequired"] = false;
                                fieldDataMap["recordId"]=distRecord.Id;
                                fieldDataMap["fieldName"] = innerDisField[j].fieldAPI;
                                fieldDataMap["fieldLabel"] = innerDisField[j].label;
                                fieldDataMap["selectedPicklist"] = distRecord[innerDisField[j].fieldAPI];
                                fieldDataMap["value"] = component.getReference("v.distributionRecord."+innerDisField[j].fieldAPI);
                                fieldDataMap["selectedvalue"] = component.getReference("v.distributionRecord."+innerDisField[j].fieldAPI);
                                
                            }
                                else if(innerDisField[j].typeOfField == "BOOLEAN"){
                                    fieldDataMap["value"] =component.getReference("v.distributionRecord."+innerDisField[j].fieldAPI);
                                }
                                    else if(innerDisField[j].typeOfField == "ID"){
                                        fieldDataMap["value"] =component.getReference("v.distributionRecord."+innerDisField[j].fieldAPI);
                                    }*/
                
                /*else if(innerDisField[j].typeOfField == "CURRENCY"){
                                                fieldDataMap["value"] =component.getReference("v.distributionRecord."+innerDisField[j].fieldAPI);
                                            }
                                                else if(innerDisField[j].typeOfField == "TEXTAREA"){
                                                    fieldDataMap["value"] = component.getReference("v.distributionRecord."+innerDisField[j].fieldAPI);
                                                    fieldDataMap["rows"] = 5;
                                                    fieldDataMap["maxlength"] = 10000;
                                                }
                                                    else if(innerDisField[j].typeOfField == "PERCENT"){
                                                        fieldDataMap["value"] =component.getReference("v.distributionRecord."+innerDisField[j].fieldAPI);
                                                    }
                                                        else if(innerDisField[j].typeOfField == "DOUBLE"){
                                                            fieldDataMap["value"] =component.getReference("v.distributionRecord."+innerDisField[j].fieldAPI);
                                                        }
                                                            else if(innerDisField[j].typeOfField == "EMAIL"){
                                                                fieldDataMap["value"] =component.getReference("v.distributionRecord."+innerDisField[j].fieldAPI);
                                                                fieldDataMap["label"] ='';
                                                            }*/
                fieldDataMap["class"] ="bidbox1";//"atAGlanceField";
                var editCompName = configobjFieldDataMap[innerDisField[j].typeOfField];
                var viewCompName = viewFieldDataMap[innerDisField[j].typeOfField];
                $A.createComponent(
                    "c:InnerDistributionCloumeComp",
                    {
                        "componentName":editCompName,
                        "componentBody":fieldDataMap,
                        "viewComponentName":viewCompName,
                        "indxNew":rowInd,
                        "newRow":checkNewRow,
                        "oldVal":innerDisField[j].fieldAPI+':-'+distRecord[innerDisField[j].fieldAPI]}
                    ,function(dynamicCmp,status){
                        // var body = component.find("atAGlanceContainer").get("v.body");
                        // body.push(dynamicCmp);
                        // alert(status);
                        var body1=component.get("v.body");
                        body1.push(dynamicCmp);
                        component.set("v.body",body1);
                    })
            }
        }
        
    },
    
    setInnerComponentBodyNew : function(component, event, helper) {
        // component.find("innerDistributionContainer").set("v.body", []);
        var innerDisField = component.get("v.innerDistributionFieldSetWrapperData");
        var distRecord = component.get("v.distributionRecord");
        var rowInd = component.get("v.TableRow");
        var tableSize = component.get("v.tableRowsize");
        var checkNewRow = 'Yes';
        if(component.get("v.tableRowsize") > component.get("v.TableRow"))
        {
            checkNewRow = 'No';
        }
        if(innerDisField.length > 0){
            var configobjFieldDataMap=JSON.parse(JSON.stringify(helper.componentTypeConfigObj));
            var viewFieldDataMap=JSON.parse(JSON.stringify(helper.viewPageFieldsMap));
            for(var j=0; j<innerDisField.length;j++ ){
                var fieldDataMap={};
                fieldDataMap["aura:id"] = component.getGlobalId();
                fieldDataMap["label"] = innerDisField[j].label;
                fieldDataMap["access"] = "Global";
                if(innerDisField[j].typeOfField == "REFERENCE"){
                    fieldDataMap["isDetailsChildCmp"] = "Yes";
                    fieldDataMap["isRequired"] = true;
                    if(distRecord[innerDisField[j].fieldAPI] != undefined){
                        fieldDataMap["lookUpId"] =distRecord[innerDisField[j].fieldAPI];
                    }else{
                        fieldDataMap["lookUpId"] = '';
                    }
                    fieldDataMap["sObjectType"] =innerDisField[j].sObjectName;
                    fieldDataMap["label"] =innerDisField[j].label;
                    fieldDataMap["pluralLabel"] =innerDisField[j].sObjectName;
                    fieldDataMap["sObjectAPIName"]=innerDisField[j].sObjectName;
                    fieldDataMap["listIconSVGPath"]="/resource/TM_TOMA__SLDS203/assets/icons/standard-sprite/svg/symbols.svg#account";
                    fieldDataMap["listIconClass"]="slds-icon-standard-account";
                    fieldDataMap["recordId"]=component.getReference("v.distributionRecord."+innerDisField[j].fieldAPI);
                    fieldDataMap["lookUpParentLabel"]=innerDisField[j].label;
                }
                else if(innerDisField[j].typeOfField == "PICKLIST"){
                    fieldDataMap["isRequired"] = true;
                    fieldDataMap["fieldName"] = innerDisField[j].fieldAPI;
                    fieldDataMap["fieldLabel"] = innerDisField[j].label;
                    fieldDataMap["selectedPicklist"] = distRecord[innerDisField[j].fieldAPI];
                    fieldDataMap["value"] = component.getReference("v.distributionRecord."+innerDisField[j].fieldAPI);
                    fieldDataMap["selectedvalue"] = component.getReference("v.distributionRecord."+innerDisField[j].fieldAPI);
                }
                fieldDataMap["class"] ="bidbox1";//"atAGlanceField";
                var editCompName = configobjFieldDataMap[innerDisField[j].typeOfField];
                var viewCompName = viewFieldDataMap[innerDisField[j].typeOfField];
                var editCls1 = ''; 
                if(checkNewRow == 'Yes') 
                {
                    editCls1 = ' slds-show';
                }
                else
                {
                    editCls1 = ' slds-hide';
                }
                var indCls = rowInd+'distEditCls';
                editCls1 =indCls+editCls1;
                var viewCls = ''; 
                if(checkNewRow == 'No') 
                {
                    viewCls = ' slds-show';
                }
                else
                {
                    viewCls = ' slds-hide';
                }
                var indxCls = rowInd+'distViewCls';
                viewCls =indxCls+viewCls;
                $A.createComponents([
                    ["aura:HTML", { tag: "td",HTMLAttributes: {"class":"tdClass"} }],
                    ["aura:HTML", { tag: "div",HTMLAttributes: {"id":rowInd+'distEditIn',"data-arg2":innerDisField[j].fieldAPI+':-'+distRecord[innerDisField[j].fieldAPI],"name":rowInd+'distEditIn',"class":editCls1 }}],
                    ["aura:HTML", { tag: "span",HTMLAttributes: {"style":"float:left;width:100%"} }],
                    [configobjFieldDataMap[innerDisField[j].typeOfField], fieldDataMap],
                    ["aura:HTML", { tag: "div",HTMLAttributes: {"id":rowInd+'distEditOut',"data-arg2":innerDisField[j].fieldAPI+':-'+distRecord[innerDisField[j].fieldAPI],"name":rowInd+'distEditOut',"class":viewCls }}],
                    ["aura:HTML", { tag: "span",HTMLAttributes: {"style":"float:left;width:100%"} }],
                    [viewFieldDataMap[innerDisField[j].typeOfField], fieldDataMap]
                ],function(tabData) {
                    var editSpan = tabData[2].get("v.body");
                    editSpan.push(tabData[3]);
                    tabData[2].set("v.body",editSpan);
                    tabData[1].set("v.body", tabData[2]);
                    
                    var viewSpan = tabData[5].get("v.body");
                    viewSpan.push(tabData[6]);
                    tabData[5].set("v.body",viewSpan);
                    tabData[4].set("v.body", tabData[5]);
                    
                    var tddata = tabData[0].get("v.body");
                    tddata.push(tabData[1]);
                    tddata.push(tabData[4]);
                    
                    tabData[0].set("v.body", tddata);
                    component.set("v.body",tabData[0]);
                });
            }
        }
        
    }
})