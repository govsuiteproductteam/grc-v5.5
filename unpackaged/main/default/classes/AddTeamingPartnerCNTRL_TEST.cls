@isTest
public class AddTeamingPartnerCNTRL_TEST {
   public static testMethod void testAddTeamingPartnerCNTRL(){
        String isUserAccesible = AddTeamingPartnerCNTRL.getUserAccesibility();
        List<RecordType> rtypes = [Select Name, Id From RecordType where sObjectType='TM_GovSuite__FedCap_Opportunity__c' and isActive=true];
        string recId='';
        if(rtypes != null && ! rtypes.isEmpty())
        {
            recId=rtypes[0].Id;
        }
        Account acc = TestDataGenerator.createAccount('testAccount');
        insert acc;
       Account acc1 = TestDataGenerator.createAccount('testAccount');
        insert acc1;
       Contract_Vehicle__c contract = TestDataGenerator.createContractVehicle('999',acc);
       insert contract;
       
       Task_Order__c fedOpp = TestDataGenerator.createTaskOrder('TMtaskOrder', acc, contract);
       insert fedOpp;
        
        /*FedCap_Opportunity__c fedOpp = TestDataGenerator.createFedOpp('TestFedOpp', acc, recId, 'Closed Won');
        fedopp.Contract_Vehicle_owned__c =  contract.id;   
        fedOpp.Budget_Approved__c=true;
        fedOpp.Type__c= 'New Business';
        fedOpp.Closed_Reason__c='Test';
        fedOpp.Marketing_Budget__c=8787;
        insert fedOpp;*/
       
       Vehicle_Partner__c vehicle = TestDataGenerator.createVehiclePartner(acc,contract);
       insert vehicle;
       
       Vehicle_Partner__c vehicle2 = TestDataGenerator.createVehiclePartner(acc1,contract);
       insert vehicle2;
       Teaming_Partner__c team = TestDataGenerator.createTeamingPartner(vehicle,fedOpp);
       insert team;
       AddTeamingPartnerCNTRL.getExistingPartners(fedOpp.Id);
       List<AllPartnerwrapper>allPartnerWrp = AddTeamingPartnerCNTRL.matchAccWrapList(acc.Name);       
       System.assertEquals(2,AddTeamingPartnerCNTRL.matchAccWrapList('testAccount').size());
       if(allPartnerWrp != null && ! allPartnerWrp.isEmpty()){
           allPartnerWrp[0].isSelected = true;
       }
       system.debug('allPartnerWrp'+allPartnerWrp);
       AddTeamingPartnerCNTRL.getTeamingPart(JSON.serialize(allPartnerWrp),fedOpp.Id);
       AddTeamingPartnerCNTRL.showAllPartners();
       AddTeamingPartnerCNTRL.dummyUpdateStatus(new List<String>{team.Id+'#####'+'New'},fedOpp.Id);
       AddTeamingPartnerCNTRL.getTeamingPartnerList(fedOpp.Id);
       AddTeamingPartnerCNTRL.addPartner(new List<String>{team.Id},fedOpp.Id);
       AddTeamingPartnerCNTRL.deleteTeam(team.id,fedOpp.Id);
       
    }
    
     
}