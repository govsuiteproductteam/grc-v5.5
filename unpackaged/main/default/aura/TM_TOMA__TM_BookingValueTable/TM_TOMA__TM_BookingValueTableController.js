({
        doCheckLicense : function(component, event, helper) {
        var action = component.get("c.checkLicensePermition1");
        action.setCallback(this, function(a) {
            //alert('doCheckLicense');
            if(a.getReturnValue() === 'Yes')
            {
                // alert('inside if');
                component.set("v.CheckLicense", 'Yes');
                helper.getContractVehicle1(component, event, helper);
                helper.getValueTableBookingListWrapper1(component, event, helper);
                helper.checkValueTablePermissions(component, event, helper); // Check Value Table Permissions
            }
            else
            {
                // alert('inside else');
                component.set("v.CheckLicense", a.getReturnValue());
            }

        });
        $A.enqueueAction(action);  
    },

    getContractVehicle : function(component, event, helper) {
        helper.getContractVehicle1(component, event, helper);
    },
    
    getValueTableBookingListWrapper : function(component, event, helper) {
        helper.getValueTableBookingListWrapper1(component, event, helper);    
    },
    
    getIfNotABookingWhy: function(component, event, helper) {
        helper.getIfNotABookingWhy1(component, event, helper); 
    },
    
    addBookingRowInWrap : function(component, event, helper) {
        event.preventDefault();
        var valueTableBookingList=component.get("v.valueTableBookingListWrap");
        var action = component.get("c.addNewBookingTableRowInWrap"); //call apex controller method
        action.setParams({
            "contractVehicalId": component.get("v.recordId")
        });
        action.setCallback(this,function(a){
            var state= a.getState();
            if(state=="SUCCESS"){
                var newConVal=a.getReturnValue();
                // "If" condition and "Else" block are newly added to restrict user access according to the object permission(17/08/2018)
                if(newConVal != null){
                    valueTableBookingList.push(newConVal);
                    newConVal.bookTab.TM_TOMA__Start_Date__c = '';
                    component.set("v.valueTableBookingListWrap",valueTableBookingList);
                }else{
                    alert('ERROR:- You do not have CREATE permission for Value Table object.'); // New Change - 17/08/2018
                }
            }
            else if (state == "ERROR") 
            {
                console.log(a.getError());            
            }
        });
        $A.enqueueAction(action);
    },
    
    removeBookingRowFromWrap : function(component, event, helper) {
        var conf = confirm("Are you sure you want to delete this component?");
        if(conf== true)
        {
            var valTableBookingList=component.get("v.valueTableBookingListWrap");
            var delBookingId=component.get("v.delBookingId");
            var bookingTablesize=component.get("v.valueTableBookingSizeFlag");
            var selectedItem = event.currentTarget;
            var removeCounter = selectedItem.id;
            
            if (removeCounter<bookingTablesize) {
                delBookingId+=',';
                bookingTablesize-=1;
                component.set("v.valueTableBookingSizeFlag",bookingTablesize);
                delBookingId=delBookingId+valTableBookingList[removeCounter].bookTab.Id;
                component.set("v.delBookingId",delBookingId);
            } 
            valTableBookingList.splice(removeCounter, 1);
            

            if(valTableBookingList != undefined && valTableBookingList != ''){
                if(valTableBookingList[0].bookTab.TM_TOMA__Is_this_a_Booking__c == false){
                    valTableBookingList[0].bookTab.TM_TOMA__Dollars__c =0.0;
                }
                else{
                    valTableBookingList[0].bookTab.TM_TOMA__Dollars__c = valTableBookingList[0].bookTab.TM_TOMA__Increase_to_Total_Element_Value__c;
                }
                for (var i=1; i<valTableBookingList.length; i++){
                    if(valTableBookingList[i].bookTab.TM_TOMA__Is_this_a_Booking__c == false){
                        var j = i.valueOf()-1;
                        // alert(valueTableBookingListWrap[j].bookTab.Dollars__c);
                        valTableBookingList[i].bookTab.TM_TOMA__Dollars__c = valTableBookingList[j].bookTab.TM_TOMA__Dollars__c;
                    }
                    else{
                        var j1 = i.valueOf()-1;
                        // alert(valueTableBookingListWrap[j].bookTab.Dollars__c);
                        valTableBookingList[i].bookTab.TM_TOMA__Dollars__c = valTableBookingList[i].bookTab.TM_TOMA__Increase_to_Total_Element_Value__c + valTableBookingList[j1].bookTab.TM_TOMA__Dollars__c;
                    }
                }
            }
            component.set("v.valueTableBookingListWrap", valTableBookingList);
        }
    },
    saveBookingTableWraper : function (component, event, helper){
        event.preventDefault();
        document.getElementById("saveButton5").disabled = true;
        document.getElementById("saveButton51").disabled = true;
        var invalidDateRow='';   
        var delBookingId=component.get("v.delBookingId");
        var valueTableBookingListWrap = component.get("v.valueTableBookingListWrap");
        var valueTableJSON = '[';
        var checkValidationError = 'Yes';
        if(valueTableBookingListWrap != undefined ){
            
             //code to make value table wrap list in assending order of start date  
         var leng = valueTableBookingListWrap.length;
            for (var i=0; i<leng.valueOf() - 1; i++){
                if(valueTableBookingListWrap[i].bookTab.TM_TOMA__Start_Date__c != ''){
                    for (var j1=i; j1<leng.valueOf(); j1++){ 
                        if(valueTableBookingListWrap[j1].bookTab.TM_TOMA__Start_Date__c != ''){
                            if(valueTableBookingListWrap[i].bookTab.TM_TOMA__Start_Date__c != valueTableBookingListWrap[j1].bookTab.TM_TOMA__Start_Date__c){
                            if(valueTableBookingListWrap[i].bookTab.TM_TOMA__Start_Date__c > valueTableBookingListWrap[j1].bookTab.TM_TOMA__Start_Date__c){
                                var tempValueTable = valueTableBookingListWrap[i].bookTab;
                                valueTableBookingListWrap[i].bookTab = valueTableBookingListWrap[j1].bookTab;
                                valueTableBookingListWrap[j1].bookTab = tempValueTable;
                            }
                            }
                        }
                    }
                }
            }
            if(valueTableBookingListWrap != undefined && valueTableBookingListWrap != '')
            {
                if(valueTableBookingListWrap[0].bookTab.TM_TOMA__Is_this_a_Booking__c == false){
                    valueTableBookingListWrap[0].bookTab.TM_TOMA__Dollars__c =0.0;
                }
                else{
                    valueTableBookingListWrap[0].bookTab.TM_TOMA__Dollars__c = valueTableBookingListWrap[0].bookTab.TM_TOMA__Increase_to_Total_Element_Value__c;
                }
            }
            for (var i=1; i<valueTableBookingListWrap.length; i++){
                if(valueTableBookingListWrap[i].bookTab.TM_TOMA__Is_this_a_Booking__c == false){
                    var j = i.valueOf()-1;
                    valueTableBookingListWrap[i].bookTab.TM_TOMA__Dollars__c = valueTableBookingListWrap[j].bookTab.TM_TOMA__Dollars__c;
                }
                else{
                    var j1 = i.valueOf()-1;
                    valueTableBookingListWrap[i].bookTab.TM_TOMA__Dollars__c = valueTableBookingListWrap[i].bookTab.TM_TOMA__Increase_to_Total_Element_Value__c + valueTableBookingListWrap[j1].bookTab.TM_TOMA__Dollars__c;
                }
            }
            //End

            
            for (var i=0; i<valueTableBookingListWrap.length; i++){        
                if(valueTableJSON.length>1){
                    valueTableJSON +=  ',{"attributes":{"type":"TM_TOMA__Value_Table__c"},';
                    var vTId =valueTableBookingListWrap[i].bookTab.Id;
                    var Modification__c = valueTableBookingListWrap[i].bookTab.TM_TOMA__Modification__c === undefined?'':valueTableBookingListWrap[i].bookTab.TM_TOMA__Modification__c;
                    var Start_Date__c = valueTableBookingListWrap[i].bookTab.TM_TOMA__Start_Date__c;
                    var Description__c = valueTableBookingListWrap[i].bookTab.TM_TOMA__Description__c === undefined?'':valueTableBookingListWrap[i].bookTab.TM_TOMA__Description__c.replace(/(\r\n|\n|\r)/gm," ");
                    var Increase_to_Total_Element_Value__c  = valueTableBookingListWrap[i].bookTab.TM_TOMA__Increase_to_Total_Element_Value__c === undefined?null:valueTableBookingListWrap[i].bookTab.TM_TOMA__Increase_to_Total_Element_Value__c;
                    var RecordTypeId  = valueTableBookingListWrap[i].bookTab.RecordTypeId ;
                    var Is_this_a_Booking__c  = valueTableBookingListWrap[i].bookTab.TM_TOMA__Is_this_a_Booking__c; //=== undefined?'': valueTableBookingListWrap[i].bookTab.Is_this_a_Booking__c;
                    //var If_not_a_Booking_why__c  = valueTableBookingListWrap[i].bookTab.TM_TOMA__If_not_a_Booking_why__c === undefined?'': valueTableBookingListWrap[i].bookTab.TM_TOMA__If_not_a_Booking_why__c ;
                    var Dollars__c  =valueTableBookingListWrap[i].bookTab.TM_TOMA__Dollars__c=== undefined?null:valueTableBookingListWrap[i].bookTab.TM_TOMA__Dollars__c ;
                    var appendData = '';
                    //Code to check a date is invalid or not
                    var inputText1=valueTableBookingListWrap[i].bookTab.TM_TOMA__Start_Date__c;
                    var testmsg =helper.validatedate(component,event,helper,inputText1);
                    if(testmsg===false)
                    {
                        checkValidationError='invalidDate';
                        invalidDateRow=invalidDateRow+''+i;
                        invalidDateRow=invalidDateRow+',';
                    }
                    if(Start_Date__c==='')
                    {
                        checkValidationError='startDate';
                    }
                    if(!(vTId===undefined))
                    {
                        appendData +='"Id":"'+ vTId+'" ,';
                    }
                    appendData += '"TM_TOMA__Modification__c":"'+ Modification__c+'","TM_TOMA__Start_Date__c":"'+Start_Date__c+'","TM_TOMA__Description__c":"'+Description__c+'","TM_TOMA__Increase_to_Total_Element_Value__c":'+Increase_to_Total_Element_Value__c+',"TM_TOMA__Is_this_a_Booking__c":'+Is_this_a_Booking__c+',"TM_TOMA__Dollars__c":'+Dollars__c+',"RecordTypeId":"'+RecordTypeId+'"}';
                    //,"TM_TOMA__If_not_a_Booking_why__c":"'+If_not_a_Booking_why__c+'"
                    valueTableJSON += appendData;
                }else{
                    valueTableJSON +=  '{"attributes":{"type":"TM_TOMA__Value_Table__c"},';
                    var vTId = valueTableBookingListWrap[i].bookTab.Id;
                    var Modification__c = valueTableBookingListWrap[i].bookTab.TM_TOMA__Modification__c === undefined?'':valueTableBookingListWrap[i].bookTab.TM_TOMA__Modification__c;
                    var Start_Date__c = valueTableBookingListWrap[i].bookTab.TM_TOMA__Start_Date__c;
                    var Description__c = valueTableBookingListWrap[i].bookTab.TM_TOMA__Description__c=== undefined?'':valueTableBookingListWrap[i].bookTab.TM_TOMA__Description__c.replace(/(\r\n|\n|\r)/gm," ");
                    var Increase_to_Total_Element_Value__c  = valueTableBookingListWrap[i].bookTab.TM_TOMA__Increase_to_Total_Element_Value__c === undefined?null:valueTableBookingListWrap[i].bookTab.TM_TOMA__Increase_to_Total_Element_Value__c;
                    var RecordTypeId  = valueTableBookingListWrap[i].bookTab.RecordTypeId ;
                    var Is_this_a_Booking__c  = valueTableBookingListWrap[i].bookTab.TM_TOMA__Is_this_a_Booking__c; //=== undefined?'': valueTableBookingListWrap[i].bookTab.Is_this_a_Booking__c;
                    //var If_not_a_Booking_why__c  = valueTableBookingListWrap[i].bookTab.TM_TOMA__If_not_a_Booking_why__c === undefined?'': valueTableBookingListWrap[i].bookTab.TM_TOMA__If_not_a_Booking_why__c;
                    var Dollars__c  = valueTableBookingListWrap[i].bookTab.TM_TOMA__Dollars__c=== undefined?null:valueTableBookingListWrap[i].bookTab.TM_TOMA__Dollars__c ;
                    var appendData = '';
                    //Code to check a date is invalid or not          
                    var inputText1=valueTableBookingListWrap[i].bookTab.TM_TOMA__Start_Date__c;
                    var testmsg =helper.validatedate(component,event,helper,inputText1);
                    if(testmsg===false)
                    {
                        checkValidationError='invalidDate';
                        invalidDateRow=invalidDateRow+''+i;
                        invalidDateRow=invalidDateRow+',';
                    }
                    if(Start_Date__c==='')
                    {
                        checkValidationError='startDate';
                    }
                    if(!(vTId===undefined))
                    {
                        appendData +='"Id":"'+ vTId+'" ,';
                    }
                    appendData += '"TM_TOMA__Modification__c":"'+ Modification__c+'","TM_TOMA__Start_Date__c":"'+Start_Date__c+'","TM_TOMA__Description__c":"'+Description__c+'","TM_TOMA__Increase_to_Total_Element_Value__c":'+Increase_to_Total_Element_Value__c+',"TM_TOMA__Is_this_a_Booking__c":'+Is_this_a_Booking__c+',"TM_TOMA__Dollars__c":'+Dollars__c+',"RecordTypeId":"'+RecordTypeId+'"}';
                    //,"TM_TOMA__If_not_a_Booking_why__c":"'+If_not_a_Booking_why__c+'"
                    valueTableJSON += appendData;
                }
            } 
            valueTableJSON += ']';  
            if(checkValidationError==='Yes'){
                var action = component.get("c.saveBookingTableData");
                action.setParams({ 
                    "bookingTableList": valueTableJSON,
                    "contractVehicalId": component.get("v.recordId"),
                    "delBookingIdsStr": delBookingId
                });
                action.setCallback(this, function(response){
                    var state = response.getState();
                    var messageText = response.getReturnValue();
                    if (state === "SUCCESS")
                    {
                        component.set("v.delBookingId",'');

                        // var checkmsg =response.getReturnValue();
                        //  if(checkmsg === 'Yes')
                        // {
                        /*   var act3 = component.get("c.isLightningPage");
                    act3.setCallback(this, function(a) {
                        if (a.getState() === "SUCCESS") {
                            if(a.getReturnValue() == false){
                                alert("Data save successfully.");
                            }
                            else{ 
                                var toastEvent = $A.get("e.force:showToast");
                                toastEvent.setParams({
                                    "title": "Success!",
                                    "message": "Data save successfully.",
                                    "type":"success"
                                });
                                toastEvent.fire();
                            }
                        }
                    }); 
                    $A.enqueueAction(act3); */
                        helper.getContractVehicle1(component, event, helper);
                        helper.getValueTableBookingListWrapper1(component, event, helper); 
                        //  }
                        /*  else
                    {
                        var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": checkmsg,//"Invalid Effective Date."
                        "type":"error"
                    });
                    toastEvent.fire();  
                    }*/
                        
                        // "If" condition and "Else" block are newly added to restrict user access according to the object permission(17/08/2018)
                        if(messageText == '' || messageText == undefined || messageText == null){
                            alert("Changes were saved successfully.");
                        }else{
                            alert(messageText);
                        }  
                    }
                    else if (state === "ERROR") 
                    {
                        console.log(response.getError());            
                    }
                    document.getElementById("saveButton5").disabled = false;
                    document.getElementById("saveButton51").disabled = false;
                });
                $A.enqueueAction(action);  
            }
            else if(checkValidationError==='invalidDate')
            {
                var checkId = invalidDateRow.split(',');
                //alert(checkId.length);
                for(var i = 0; i < checkId.length-1; i++) {
                    // alert(checkId[i]);
                    var d = document.getElementById(checkId[i]+'booking');
                    d.className += " validationClass";
                }
                alert('ERROR:- Invalid Effective Date, in row:- '+invalidDateRow);
                //alert("ERROR:- Invalid Effective Date.");
                /*  var act3 = component.get("c.isLightningPage");
            act3.setCallback(this, function(a) {
                if (a.getState() === "SUCCESS") {
                    if(a.getReturnValue() == false){
                        alert("ERROR:- Invalid Effective Date.");
                    }
                    else{ 
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!",
                            "message": "Invalid Effective Date.",
                            "type":"error"
                        });
                        toastEvent.fire();
                    }
                }
            }); 
            $A.enqueueAction(act3);*/
                document.getElementById("saveButton5").disabled = false; 
                document.getElementById("saveButton51").disabled = false;
            }
                else{
                    alert("ERROR:- Please enter an Effective Date.");
                    /* var act3 = component.get("c.isLightningPage");
                    act3.setCallback(this, function(a) {
                        if (a.getState() === "SUCCESS") {
                            if(a.getReturnValue() == false){
                                alert("ERROR:- Please enter an Effective Date.");
                            }
                            else{ 
                                var toastEvent = $A.get("e.force:showToast");
                                toastEvent.setParams({
                                    "title": "Error!",
                                    "message": "Enter date for Effective Date.",
                                    "type":"error"
                                });
                                toastEvent.fire();
                            }
                        }
                    }); 
                    $A.enqueueAction(act3);*/
                document.getElementById("saveButton5").disabled = false; 
                document.getElementById("saveButton51").disabled = false;
            }
        }
        else{
            document.getElementById("saveButton5").disabled = false;
            document.getElementById("saveButton51").disabled = false;
        }
    },
    
    saveBookingTable : function (component, event, helper){
        var delBookingId=component.get("v.delBookingId");
        var valTableBookingList=component.get("v.valueTableBookingList");
        var valueTableJSON = '[';
        if(valTableBookingList != undefined){
            for (var i=0; i<valTableBookingList.length; i++){        
                if(valueTableJSON.length>1){
                    valueTableJSON +=  ',{"attributes":{"type":"TM_TOMA__Value_Table__c"},';
                    var vTId = valTableBookingList[i].Id;
                    var Modification__c = valTableBookingList[i].TM_TOMA__Modification__c;
                    var Start_Date__c = valTableBookingList[i].TM_TOMA__Start_Date__c;
                    var Description__c = valTableBookingList[i].TM_TOMA__Description__c;
                    var Increase_to_Total_Element_Value__c  = valTableBookingList[i].TM_TOMA__Increase_to_Total_Element_Value__c ;
                    var RecordTypeId  = valTableBookingList[i].RecordTypeId ;
                    var Is_this_a_Booking__c  = valTableBookingList[i].TM_TOMA__Is_this_a_Booking__c ;
                    var If_not_a_Booking_why__c  = valTableBookingList[i].TM_TOMA__If_not_a_Booking_why__c ;
                    var Dollars__c  = valTableBookingList[i].TM_TOMA__Dollars__c ;
                    var appendData = '';
                    
                    if(!(vTId===undefined))
                    {
                        appendData +='"Id":"'+ vTId+'" ,';
                    }
                    appendData += '"TM_TOMA__Modification__c":"'+ Modification__c+'","TM_TOMA__Start_Date__c":"'+Start_Date__c+'","TM_TOMA__Description__c":"'+Description__c+'","TM_TOMA__If_not_a_Booking_why__c":"'+If_not_a_Booking_why__c+'","TM_TOMA__Increase_to_Total_Element_Value__c":'+Increase_to_Total_Element_Value__c+',"TM_TOMA__Is_this_a_Booking__c":'+Is_this_a_Booking__c+',"TM_TOMA__Dollars__c":'+Dollars__c+',"RecordTypeId":"'+RecordTypeId+'"}';
                    valueTableJSON += appendData;
                }else{
                    valueTableJSON +=  '{"attributes":{"type":"TM_TOMA__Value_Table__c"},';
                    var vTId = valTableBookingList[i].Id;
                    var Modification__c = valTableBookingList[i].TM_TOMA__Modification__c;
                    var Start_Date__c = valTableBookingList[i].TM_TOMA__Start_Date__c;
                    var Description__c = valTableBookingList[i].TM_TOMA__Description__c;
                    var Increase_to_Total_Element_Value__c  = valTableBookingList[i].TM_TOMA__Increase_to_Total_Element_Value__c ;
                    var RecordTypeId  = valTableBookingList[i].RecordTypeId ;
                    var Is_this_a_Booking__c  = valTableBookingList[i].TM_TOMA__Is_this_a_Booking__c ;
                    var If_not_a_Booking_why__c  = valTableBookingList[i].TM_TOMA__If_not_a_Booking_why__c ;
                    var Dollars__c  = valTableBookingList[i].TM_TOMA__Dollars__c ;
                    var appendData = '';
                    if(!(vTId===undefined))
                    {
                        appendData +='"Id":"'+ vTId+'" ,';
                    }
                    appendData += '"TM_TOMA__Modification__c":"'+ Modification__c+'","TM_TOMA__Start_Date__c":"'+Start_Date__c+'","TM_TOMA__Description__c":"'+Description__c+'","TM_TOMA__If_not_a_Booking_why__c":"'+If_not_a_Booking_why__c+'","TM_TOMA__Increase_to_Total_Element_Value__c":'+Increase_to_Total_Element_Value__c+',"TM_TOMA__Is_this_a_Booking__c":'+Is_this_a_Booking__c+',"TM_TOMA__Dollars__c":'+Dollars__c+',"RecordTypeId":"'+RecordTypeId+'"}';
                    valueTableJSON += appendData;
                }
            } 
            valueTableJSON += ']';   
            var action = component.get("c.saveBookingTableData");
            action.setParams({ 
                "bookingTableList": valueTableJSON,
                "contractVehicalId": component.get("v.recordId"),
                "delBookingIdsStr": delBookingId
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                if (state === "SUCCESS")
                {
                    var bookingTableList=response.getReturnValue();
                    component.set("v.delBookingId",'');
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "Changes were saved successfully.",
                        "type":"success"
                    });
                    toastEvent.fire(); 
                    helper.getContractVehicle1(component, event, helper);
                    helper.bookingTableList(component, event, helper); 
                }
                else if (state === "ERROR") 
                {
                    console.log(response.getError());            
                }
            });
            $A.enqueueAction(action);  
        }
        
    }
})