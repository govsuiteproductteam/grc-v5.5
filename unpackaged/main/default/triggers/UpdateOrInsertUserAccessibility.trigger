trigger UpdateOrInsertUserAccessibility on User (before insert, before update) {

    if(Trigger.isInsert){
        List<User> fedTOMUserList = new List<User>();
        List<User> fedCLMUserList = new List<User>();
        for(User usr : Trigger.new){
             if(usr.Subscribed_Apps__c != null && usr.Subscribed_Apps__c.contains('FedTOM') && usr.isActive == true){
                fedTOMUserList.add(usr);
            }
            if(usr.Subscribed_Apps__c != null && usr.Subscribed_Apps__c.contains('FedCLM') && usr.isActive == true){
                fedCLMUserList.add(usr);
            }
        }
        CheckFedTOMLicenseTriggerHelper.checkLicenseOnInsertForFedTOM(fedTOMUserList);
        CheckFedCLMLicenseTriggerHelper.checkLicenseOnInsertForFedCLM(fedCLMUserList);
    }
    
    if(Trigger.isUpdate){
        
        Map<Id,User> oldUserMap = trigger.oldMap;
        Map<Id,User> fedTOMUserMap = new Map<Id,User>();
        Map<Id,User> fedCLMUserMap = new Map<Id,User>();
        for(User usr : Trigger.new){
            User oldUser = oldUserMap.get(usr.id);
            String oldSubscribedApp = oldUser.Subscribed_Apps__c;
            if(oldSubscribedApp  == null)
                oldSubscribedApp = '';
            if((usr.Subscribed_Apps__c != null && usr.Subscribed_Apps__c.contains('FedTOM')  && !oldSubscribedApp.contains('FedTOM') && usr.isActive == true) ||
                (usr.Subscribed_Apps__c != null && usr.Subscribed_Apps__c.contains('FedTOM')  && !oldUser.isActive && usr.isActive) ){
                fedTOMUserMap.put(usr.id,usr);
            }
            if((usr.Subscribed_Apps__c != null && usr.Subscribed_Apps__c.contains('FedCLM') && !oldSubscribedApp.contains('FedCLM') && usr.isActive == true) || 
                (usr.Subscribed_Apps__c != null && usr.Subscribed_Apps__c.contains('FedCLM')  && !oldUser.isActive && usr.isActive) ){
                fedCLMUserMap.put(usr.id,usr);
            }
        }
        CheckFedTOMLicenseTriggerHelper.checkLicenseOnUpdateForFedTOM(fedTOMUserMap);
        CheckFedCLMLicenseTriggerHelper.checkLicenseOnUpdateForFedCLM(fedCLMUserMap);
        
    }
   /* if (Trigger.isBefore) {
        Set<User> tempUserSet = new Set<User>();
        //set<User> updateUserSet = new Set<User>();
        //Folder documentFolder = [Select Id from Folder where Name='FedTOM'];
        TM_License_Key_Setting__c customSetting = TM_License_Key_Setting__c.getOrgDefaults();
        String FedTOMKey = customSetting.FedTOM_Key__c;
        EncryptionDecryptionManager.KeyInfo keyInfo;
        if(FedTOMKey != null){
            String decriptedKey = EncryptionDecryptionManager.decryptValue('FedTOM',FedTOMKey );
            keyInfo = EncryptionDecryptionManager.getKeyInfo(decriptedKey );
        }
        else{
            if(Trigger.isUpdate){
                Map<id,User> userMap =   Trigger.oldMap;
                for(User user : Trigger.new){
                    if((userMap.get(user.Id).Subscribed_Apps__c == null || !userMap.get(user.Id).Subscribed_Apps__c.contains('FedTOM')) && user.Subscribed_Apps__c != null && user.Subscribed_Apps__c.contains('FedTOM')){ 
                        user.addError('Please enter FedTOM Product key in custom setting.');
                    }
                }
            }
            if(Trigger.isInsert){
                for(User user :  Trigger.new){
                    if(user.Subscribed_Apps__c != null && user.Subscribed_Apps__c.contains('FedTOM')){ 
                        user.addError('Please enter FedTOM Product key in custom setting.');
                    }
                }
            }
        }
        
        try{
            if(keyInfo != null && keyInfo.StartDate != null && keyInfo.EndDate != null && keyInfo.organizationId != null && 
                       keyInfo.licenseCount != null && keyInfo.packageName != null){
                List<Organization> orgList = [Select Id From Organization where id =: keyInfo.organizationId];
                System.debug('keyInfo != null = '+keyInfo != null);
                System.debug('!orgList.isEmpty() =  '+!orgList.isEmpty());
                System.debug('keyInfo.StartDate <= Date.today() = '+(keyInfo.StartDate <= Date.today()));
                System.debug('keyInfo.EndDate >= Date.today() = '+(keyInfo.EndDate >= Date.today()));
                if(keyInfo != null && !orgList.isEmpty() &&  keyInfo.StartDate <= Date.today() && keyInfo.EndDate >= Date.today()){
                    List<user> userListFedCap = [SELECt Id FROM User WHERE Subscribed_Apps__c includes('FedTOM')];
                  
                    if(userListFedCap != null){
                        Integer fedCapCounter;
                                            
                        fedCapCounter= userListFedCap.size();                        
                        
                        System.debug('fedCapCounter = '+fedCapCounter );
                        
                        if(Trigger.isUpdate){  
                            Map<id,User> userMap =   Trigger.oldMap;
                            for(User user : Trigger.new){
                                //System.debug('user.TM_TOMA__FedTOM_User__c = '+ user.TM_TOMA__FedTOM_User__c);
                                System.debug('!userMap.get(user.Id).Subscribed_Apps__c.contains = '+ userMap.get(user.Id).Subscribed_Apps__c);
                                System.debug(' user.Subscribed_Apps__c.contains = '+  user.Subscribed_Apps__c);
                                if((userMap.get(user.Id).Subscribed_Apps__c == null || !userMap.get(user.Id).Subscribed_Apps__c.contains('FedTOM')) && user.Subscribed_Apps__c != null && user.Subscribed_Apps__c.contains('FedTOM')){
                                    fedCapCounter++; 
                                    if(keyInfo == null || fedCapCounter > keyInfo.licenseCount){
                                        tempUserSet.add(user);
                                        //user.addError('FedTom cannot Update More than '+ fedTOMdata+' records or FedCLM cannot Update More than '+fedCLMValue+' records.');
                                        //continue;                                        
                                    }
                                }
                            }
                            
                            System.debug('fedCapCounter = '+fedCapCounter );
                           
                            for(User user : tempUserSet){
                                user.addError('FedTom cannot Update More than '+'license records.');
                            }
                        } 
                        if(Trigger.isInsert){
                            for(User user : Trigger.new){
                                //System.debug('user.TM_TOMA__FedTOM_User__c = '+ user.TM_TOMA__FedTOM_User__c);
                                if(user.Subscribed_Apps__c.contains('FedTOM')){
                                    fedCapCounter++; 
                                    if(fedCapCounter > keyInfo.licenseCount){
                                        tempUserSet.add(user);
                                        //user.addError('FedTom cannot Update More than '+ fedTOMdata+' records or FedCLM cannot Update More than '+fedCLMValue+' records.');
                                        //continue;                                        
                                    }
                                }
                            }
                            
                            System.debug('fedCapCounter = '+fedCapCounter );
                           
                            for(User user : tempUserSet){
                                user.addError('FedTom cannot Update More than '+ 'lincense records.');
                            }
                        }
                    }     
                }              
                else{
                    if(Trigger.isUpdate){
                        Map<id,User> userMap =   Trigger.oldMap;
                        for(User user : tempUserSet){
                            if((userMap.get(user.Id).Subscribed_Apps__c == null || !userMap.get(user.Id).Subscribed_Apps__c.contains('FedTOM')) && user.Subscribed_Apps__c != null && user.Subscribed_Apps__c.contains('FedTOM')){ 
                                user.addError('FedTOM Product key entered in custom setting is not valid or expired.');
                            }
                        }
                    }
                    if(Trigger.isInsert){
                        for(User user : tempUserSet){
                            if(user.Subscribed_Apps__c != null && user.Subscribed_Apps__c.contains('FedTOM')){ 
                                user.addError('FedTOM Product key entered in custom setting is not valid or expired.');
                            }
                        }
                    }
                }
            }
            else{
                if(Trigger.isUpdate){
                    Map<id,User> userMap =   Trigger.oldMap;
                    for(User user : tempUserSet){
                        if((userMap.get(user.Id).Subscribed_Apps__c == null || !userMap.get(user.Id).Subscribed_Apps__c.contains('FedTOM')) && user.Subscribed_Apps__c != null && user.Subscribed_Apps__c.contains('FedTOM')){ 
                            user.addError('Please enter FedTOM Product key in custom setting.');
                        }
                    }
                }
                if(Trigger.isInsert){
                    for(User user : tempUserSet){
                        if(user.Subscribed_Apps__c != null && user.Subscribed_Apps__c.contains('FedTOM')){ 
                            user.addError('Please enter FedTOM Product key in custom setting.');
                        }
                    }
                }
            }
        }catch(Exception e){
            if(Trigger.isUpdate){
                Map<id,User> userMap =   Trigger.oldMap;
                for(User user : tempUserSet){
                    if((userMap.get(user.Id).Subscribed_Apps__c == null || !userMap.get(user.Id).Subscribed_Apps__c.contains('FedTOM')) && user.Subscribed_Apps__c != null && user.Subscribed_Apps__c.contains('FedTOM')){ 
                        user.addError('Please enter FedTOM Product key in custom setting.');
                    }
                }
            }
            if(Trigger.isInsert){
                for(User user : tempUserSet){
                    if(user.Subscribed_Apps__c != null && user.Subscribed_Apps__c.contains('FedTOM')){ 
                        user.addError('Please enter FedTOM Product key in custom setting.');
                    }
                }
            }
        }
    }
    */
    /*if (Trigger.isBefore) {
        Set<User> tempUserSet = new Set<User>();
        //set<User> updateUserSet = new Set<User>();
        Folder documentFolder = [Select Id from Folder where Name='FedTom'];
        if(documentFolder!=null){  
            List<Document> fedTomDocumentList = [select id,Body from Document where FolderId = : documentFolder.Id And DEveloperName=: 'FedTomUserDoc' Limit 1];
            List<Document> fedCLMDocumentList = [select id,Body from Document where FolderId = : documentFolder.Id And DEveloperName=: 'FedCLMUserDoc' Limit 1]; 
            if(Test.isRunningTest()){                
                fedTomDocumentList = [select id,Body from Document where FolderId = : documentFolder.Id And DEveloperName=: 'FedTomUserDoc1' Limit 1];
                fedCLMDocumentList = [select id,Body from Document where FolderId = : documentFolder.Id And DEveloperName=: 'FedCLMUserDoc1' Limit 1];            
            }
            if(fedTomDocumentList != null && !fedTomDocumentList.isEmpty() && fedCLMDocumentList != null && !fedCLMDocumentList.isEmpty()){  
                try{
                    String fedTOMdata = EncryptionDecryptionManager.decryptValue(fedTomDocumentList[0].Body);
                    Integer fedTOMValue = Integer.valueOf(fedTOMdata);
                    System.debug('fedTOMValue = '+fedTOMValue);
                    String fedCLMdata = EncryptionDecryptionManager.decryptValue(fedCLMDocumentList[0].Body);
                    Integer fedCLMValue = Integer.valueOf(fedCLMdata);
                    System.debug('fedCLMValue = '+fedCLMValue);
                    
                    List<user> userListTom = [SELECt Id FROM User WHERE TM_TOMA__FedTOM_User__c = true];
                    List<user> userListCLM = [SELECt Id FROM User WHERE TM_TOMA__FedCLM_User__c = true];
                    if((userListTom!= null) && (userListCLM!= null )){
                        Integer fedTomCounter;
                        Integer fedCLMCounter;                        
                        fedTomCounter = userListTom.size();                        
                        fedCLMCounter = userListCLM.size();
                        
                        System.debug('fedTomCounter = '+fedTomCounter);
                        System.debug('fedCLMCounter = '+fedCLMCounter); 
                        
                        if(Trigger.isUpdate){  
                            Map<id,User> userMap =   Trigger.oldMap;
                            for(User user : Trigger.new){
                                //System.debug('user.TM_TOMA__FedTOM_User__c = '+ user.TM_TOMA__FedTOM_User__c);
                                if(!userMap.get(user.Id).TM_TOMA__FedTOM_User__c && user.TM_TOMA__FedTOM_User__c){
                                    fedTomCounter++; 
                                    if(fedTomCounter > fedTOMValue){
                                        tempUserSet.add(user);
                                        //user.addError('FedTom cannot Update More than '+ fedTOMdata+' records or FedCLM cannot Update More than '+fedCLMValue+' records.');
                                        //continue;                                        
                                    }
                                }
                            }
                            for(User user : Trigger.new){
                                if(!userMap.get(user.Id).TM_TOMA__FedCLM_User__c && user.TM_TOMA__FedCLM_User__c ){
                                    fedCLMCounter++;
                                    if(fedCLMCounter > fedCLMValue){
                                        tempUserSet.add(user);
                                        //user.addError('FedTom cannot Update More than '+ fedTOMdata+' records or FedCLM cannot Update More than '+fedCLMValue+' records.');
                                        //continue;
                                    }                                                       
                                }
                            }
                            System.debug('fedTomCounter = '+fedTomCounter);
                            System.debug('fedCLMCounter = '+fedCLMCounter);   
                            for(User user : tempUserSet){
                                user.addError('FedTom cannot Update More than '+ fedTOMdata+' records or FedCLM cannot Update More than '+fedCLMValue+' records.');
                            }
                        } 
                        if(Trigger.isInsert){
                            for(User user : Trigger.new){
                                //System.debug('user.TM_TOMA__FedTOM_User__c = '+ user.TM_TOMA__FedTOM_User__c);
                                if(user.TM_TOMA__FedTOM_User__c){
                                    fedTomCounter++; 
                                    if(fedTomCounter > fedTOMValue){
                                        tempUserSet.add(user);
                                        //user.addError('FedTom cannot Update More than '+ fedTOMdata+' records or FedCLM cannot Update More than '+fedCLMValue+' records.');
                                        //continue;                                        
                                    }
                                }
                            }
                            for(User user : Trigger.new){
                                if(user.TM_TOMA__FedCLM_User__c ){
                                    fedCLMCounter++;
                                    if(fedCLMCounter > fedCLMValue){
                                        tempUserSet.add(user);
                                        // user.addError('FedTom cannot Update More than '+ fedTOMdata+' records or FedCLM cannot Update More than '+fedCLMValue+' records.');
                                        //continue;
                                    }                                                       
                                }
                            }
                            System.debug('fedTomCounter = '+fedTomCounter);
                            System.debug('fedCLMCounter = '+fedCLMCounter);   
                            for(User user : tempUserSet){
                                user.addError('FedTom cannot Update More than '+ fedTOMdata+' records or FedCLM cannot Update More than '+fedCLMValue+' records.');
                            }
                        }
                    }                   
                }catch(Exception e){
                    
                }
            }
        }
    }*/
}