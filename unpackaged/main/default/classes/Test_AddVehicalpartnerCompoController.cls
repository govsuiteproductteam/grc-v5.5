@IsTest
public class Test_AddVehicalpartnerCompoController {
    static testMethod void testAddVehicalpartnerCompoCont(){
        account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        account acc1 = TestDataGenerator.createAccount('TestAcc1');
        insert acc1;
        Contract_Vehicle__c conVeh = TestDataGenerator.createContractVehicle('1234567', acc);
        insert conVeh;
        Vehicle_Partner__c vehPart = TestDataGenerator.createVehiclePartner(acc, conVeh);
        insert vehPart;
        Vehicle_Partner__c vehPart1 = TestDataGenerator.createVehiclePartner(acc1, conVeh);
        insert vehPart1;
        AddVehicalPatnerCompoController.checkLicensePermition();
        list<Vehicle_Partner__c> VehPartnerList = AddVehicalPatnerCompoController.getVehicalPatner(conVeh.Id);
        System.assertEquals(2, VehPartnerList.size());
        AddVehicalPatnerCompoController.deleteVehicalPatner(vehPart1.Id);
        VehPartnerList = AddVehicalPatnerCompoController.getVehicalPatner(conVeh.Id);
        System.assertEquals(1, VehPartnerList.size());
        AddVehicalPatnerCompoController.addNewVehicalPartner(conVeh.Id);
        string saveData = '['+vehPart.Id+'SPLITDATA'+conVeh.Id+'SPLITDATA'+acc.Id+'SPLITDATA'+'Sub-Contractor'+'SPLITPARENT'+'undefined'+'SPLITDATA'+conVeh.Id+'SPLITDATA'+acc1.Id+'SPLITDATA'+'Sub-Contractor'+'SPLITPARENT';
        AddVehicalPatnerCompoController.saveVehicalPatner(saveData);
        VehPartnerList = AddVehicalPatnerCompoController.getVehicalPatner(conVeh.Id);
        System.assertEquals(2, VehPartnerList.size());
        map<string,string> strMap = AddVehicalPatnerCompoController.getRoleMap();
        System.assert(strMap != null);
    }
}