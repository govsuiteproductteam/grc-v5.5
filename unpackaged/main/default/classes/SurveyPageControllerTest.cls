@isTest
public with sharing class SurveyPageControllerTest{
    public static TestMethod void TestSurveyPageController(){
        Survey__c sur1 = TestDataGenerator.createSurvey();
        insert sur1;
        
        Question__c que1 = TestDataGenerator.createQuestion('What',sur1);
        que1.Option_1__c ='opt1';
        que1.Option_2__c ='opt2';
        que1.Option_3__c ='opt3';
        que1.Option_4__c ='opt4';
        que1.Option_5__c ='opt5';
        que1.Option_6__c ='opt6';
        que1.Option_7__c ='opt7'; 
        que1.Option_8__c ='opt8';
        que1.Option_9__c ='opt9';   
        que1.Option_10__c ='opt10';  
        que1.Question_Type__c = 'Radio';
        que1.Is_mandatory__c = false;
        
        insert que1;
        
        Test.starttest();
        
        ApexPages.CurrentPage().getparameters().put('id', sur1.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(sur1); 
        surveyPageController surController = new surveyPageController (controller );
      
        surController.survey.Description__c = 'IDIQ survey';
        surController.queWraList.get(0).question.Question__c = 'What';
        surController.queWraList.get(0).question.Option_1__c = 'opt1';
        
        surController.rowIndex = 0;
        surController.queNo = 0;
        surController.optNo =0;
        surController.toggleContent();
        PageReference pgRef = surController.save();
        system.assertNotEquals(pgRef, null);
        
        surController.AddQuestion();
        surController.RemoveQuestion();
        system.assertNotEquals(surController.SurveyId  , null);
       
        Test.Stoptest();
    }
    
     public static testMethod void surveyPageControllerTest2()
    {
        Survey__c sur1 = TestDataGenerator.createSurvey();
        insert sur1;
        
        Question__c que1 = TestDataGenerator.createQuestion('What',sur1);
        que1.Option_1__c ='opt1';
        que1.Option_2__c ='opt2';
        que1.Option_3__c ='opt3';
        que1.Option_4__c ='opt4';
        que1.Option_5__c ='opt5';
        que1.Option_6__c ='opt6';
        que1.Option_7__c ='opt7'; 
        que1.Option_8__c ='opt8';
        que1.Option_9__c ='opt9';   
        que1.Option_10__c ='opt10';  
        que1.Question_Type__c = 'Radio';
        que1.Is_mandatory__c = false;
        
        insert que1;
        Test.starttest();
        
       ApexPages.CurrentPage().getparameters().put('id', null);
        ApexPages.StandardController controller = new ApexPages.StandardController(sur1); 
        surveyPageController surController = new surveyPageController (controller );
       
        surController.survey.Description__c = 'IDIQ survey';
        surController.queWraList.get(0).question.Question__c = 'What';
        surController.queWraList.get(0).question.Option_1__c = 'opt1';
        
        
        surController.rowIndex = 0;
        surController.queNo = 0;
        surController.optNo =0;
        surController.toggleContent();
        surController.save();
        surController.AddQuestion();
      //  surController.queWraList[0].AddOption();
        surController.RemoveQuestion();
       
        //surController.queWraList[0].RemoveOption(); 
        system.assertEquals(surController.SurveyId  , null);       
        Test.Stoptest();
    } 
}