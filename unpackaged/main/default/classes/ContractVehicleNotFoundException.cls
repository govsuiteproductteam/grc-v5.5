public with sharing class ContractVehicleNotFoundException extends Exception{
    
    public ContractVehicleNotFoundException(String exceptionString,Integer lineNo){
        this(exceptionString);
    }
}