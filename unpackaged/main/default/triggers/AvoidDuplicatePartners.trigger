trigger AvoidDuplicatePartners on Vehicle_Partner__c (before insert) {
    List<Id> contractVehicleIds = new List<Id>();    
    List<Id> partnerIds = new List<Id>();    
    for(Vehicle_Partner__c vp : Trigger.New){
        contractVehicleIds.add(vp.Contract_Vehicle__c);
        partnerIds.add(vp.Partner__c);
    }
    Set<Id> contractIdsSet = new Set<Id>(contractVehicleIds);
      for(Vehicle_Partner__c vp : [SELECT Id,Contract_Vehicle__c,Partner__c FROM Vehicle_Partner__c WHERE Contract_Vehicle__c IN: contractIdsSet]){
        for(Vehicle_Partner__c vpNew : Trigger.New){           
            if(vpNew.Contract_Vehicle__c == vp.Contract_Vehicle__c && vpNew.Partner__c == vp.Partner__c)
                vpNew.addError('This partner has already been setup');
        }        
    }
}