@isTest
public class Test_TM_NewAtglanceContractController {
    static testMethod void testMethodContractVehicalAtglanceController(){
        test.startTest();
        List<RecordType> rtypes = [Select Name, Id From RecordType where sObjectType='TM_TOMA__Contract_Vehicle__c' and isActive=true];
        string recId='';
        if(rtypes.size()>0)
        {
            recId=rtypes[0].Id;
        }
        
        CLM_AtaGlanceField__c newOppAtAGlance1 = TestDataGenerator.createAtaGlanceField(recId);
        newOppAtAGlance1.Is_Contract__c=true;
        newOppAtAGlance1.FieldApiName__c='TM_TOMA__Contracting_Agency__c';
        newOppAtAGlance1.FieldLabel__c='Contracting Organization';
        newOppAtAGlance1.Order__c=2;
        insert newOppAtAGlance1; 
        /*CLM_AtaGlanceField__c newOppAtAGlance2 = TestDataGenerator.createAtaGlanceField(recId);
newOppAtAGlance2.Is_Contract__c=true;
newOppAtAGlance2.FieldApiName__c='Test';
newOppAtAGlance2.FieldLabel__c='TestComponent';
newOppAtAGlance2.Is_Component__c=true;
newOppAtAGlance2.Order__c=3;
insert newOppAtAGlance2; */
        
        CLM_AtaGlanceField__c newOppAtAGlance3 = TestDataGenerator.createAtaGlanceField(recId);
        newOppAtAGlance3.Is_Contract__c=true;
        newOppAtAGlance3.FieldApiName__c='TM_TOMA__Contracting_Agency__c';
        newOppAtAGlance3.FieldLabel__c='Contracting Organization';
        newOppAtAGlance3.Order__c=2;
        insert newOppAtAGlance3;
        
        CLM_AtaGlanceField__c newOppAtAGlance4= TestDataGenerator.createAtaGlanceField(recId);
        newOppAtAGlance4.Is_Contract__c=true;
        newOppAtAGlance4.FieldApiName__c='TM_TOMA__Contract_Amount__c';
        newOppAtAGlance4.FieldLabel__c='Our Anticipated Contract Value';
        newOppAtAGlance4.Order__c=2;
        insert newOppAtAGlance4;
        
        CLM_AtaGlanceField__c newOppAtAGlance5 = TestDataGenerator.createAtaGlanceField(recId);
        newOppAtAGlance5.Is_Contract__c=true;
        newOppAtAGlance5.FieldApiName__c='TM_TOMA__Option_Years_End_Date__c';
        newOppAtAGlance5.FieldLabel__c='Option Years End Date';
        newOppAtAGlance5.Order__c=2;
        insert newOppAtAGlance5;
        
        Account acc = TestDataGenerator.createAccount('testAccount');
        insert acc;
        
        Contract_Vehicle__c ConVeh = TestDataGenerator.createContractVehicle('Test contract vehicle', acc);
        ConVeh.recordTypeId = recId;
        ConVeh.Contract_Type__c= TestDataGenerator.getPicklistValue('TM_TOMA__Contract_Vehicle__c','TM_TOMA__Contract_Type__c');
        insert ConVeh;
        test.stopTest();
        String licenceStatus=TM_NewAtglanceContractController.getUserAccesibility();
        Map<String,String> stagePickListVal=TM_NewAtglanceContractController.getstagePicklistvalue();
        TM_AtglanceWrap atglanceWrap=TM_NewAtglanceContractController.getFields(ConVeh.Id);
        System.assert(atglanceWrap!=null);
        
    }
}