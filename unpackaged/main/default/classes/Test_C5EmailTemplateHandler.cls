@isTest
public class Test_C5EmailTemplateHandler {
    
    public Static TestMethod void testNewTaskOrderAndMappedFields (){
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Contact con = TestDataGenerator.createContact('TestCon', acc);
        insert con;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('CV-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'c5@q-vb1jh15u8xqysey4oxh50kv1qjafs6k1mqfzw8ss3ab3ypkqv.61-zhsweao.na34.apex.salesforce.com';
        insert conVehi;
        
        Messaging.InboundEmail email  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        List<StaticResource> srList = [SELECT Id,Body FROM StaticResource WHERE Name='C5EmailTest'];
        
        email.plainTextBody = srList.get(0).Body.toString();
        email.fromAddress ='test@test.com';
        email.fromName = 'ABC XYZ';
        email.subject = 'Fwd: C5 Request for White Papers - C5-17-RWP-0070 - Military to Military Engagements Learning Platform (with RWP Attachment)';
        email.toaddresses = new List<String>();
        email.toaddresses.add('c5@q-vb1jh15u8xqysey4oxh50kv1qjafs6k1mqfzw8ss3ab3ypkqv.61-zhsweao.na34.apex.salesforce.com');
        
        C5EmailTemplateHandler c5EmailTemplateHandler = new C5EmailTemplateHandler();
        Test.startTest();
        Messaging.InboundEmailResult result = c5EmailTemplateHandler.handleInboundEmail(email, env);
        Test.stopTest();
        
        List<Task_Order__c> tOrderList = [SELECT Id, Name, Task_Order_Title__c, Questions_Due_Date__c, Proposal_Due_DateTime__c,
                                          Contracting_Officer__c, Contracting_Officer__r.Name, Contract_Specialist__c, 
                                          Contract_Specialist__r.Name, Description__c, Primary_Requirement__c, Requirement__c 
                                          FROM Task_Order__c];
        Task_Order__c taskOrder = tOrderList.get(0);
        System.assertEquals(1, tOrderList.size());
        System.assertEquals('C5-17-RWP-0070', taskOrder.Name);
        System.assertEquals('Military to Military Engagements Learning Platform (with RWP Attachment)', taskOrder.Task_Order_Title__c);
        //System.assertEquals(Date.valueOf('4/27/2017'), taskOrder.Questions_Due_Date__c);
        System.assertEquals('Amanda Rieman', taskOrder.Contracting_Officer__r.Name);
        System.assertEquals('Denise Anderson', taskOrder.Contract_Specialist__r.Name);
        System.assertEquals('The United States Africa Command (USAFRICOM) works with U.S. agencies and international partners to build defense capabilities, respond to crises and deter and defeat transnational threats to secure U.S. national interests and promote regional security and stability across the Area of Responsibility. The Directorate of Command, Control, Communications and Computer Systems (J6) develops, directs and provides Command, Control, Communications and Computer Systems Intelligence, Surveillance, and Reconnaissance (C4ISR) support and cyber security to deliver and improve operational capability, network security and support to the AFRICOM Theater Campaign Plan. To achieve this mission, USAFRICOM J6 executes Military to Military (Mil-to-Mil) engagements for multi-lateral, inter-agency engagements, which are used to establish an understanding of information sharing and an analysis of current capabilities and procedures for operating and sustaining communication and cyber defense systems required to support U.S. foreign policy and relations with allies. As part of Mil-to-Mil engagements, the United Nations has a signals academy on the continent of Africa to enable communication on the fundamentals and tactical applications of C4I which account for 80% of the professional development of these communications specialists. The other 20% is critical thinking and decision making.',
                            taskOrder.Description__c);
        System.assertEquals('Currently, there is a gap in providing these tacit skills. To overcome this gap, USAFRICOM J6 requires the development, demonstration and evaluation of a platform for knowledge transfer to overcome the shortfall. The learning platform must enable tacit s', 
                            taskOrder.Primary_Requirement__c);
        System.debug('Req test=='+taskOrder.Requirement__c);
        System.assertEquals('Currently, there is a gap in providing these tacit skills. To overcome this gap, USAFRICOM J6 requires the development, demonstration and evaluation of a platform for knowledge transfer to overcome the shortfall. The learning platform must enable tacit skills for U.S. partners to build a capacity strategy beyond the technical aspects of communications and into a new level of C4 interoperability and capacity through user professional development. The Government envisions a notional multi-phase development and evaluation pathway: Phase 1: Concept Development and Preliminary Assessment Phase 2: Integration and User Evaluation Phase 3: Enhancements and User Evaluation. The learning platform developed will be evaluated at a Continental United States (CONUS) location to determine if the solution is a sustainable platform that has the potential for scalable and affordable implementation across allies to ensure improved operational capabilities for the AFRICOM Theater Campaign Plan.',
                            taskOrder.Requirement__c);
        
    }
    
    public Static TestMethod void testExistingTaskOrderAndContractMods (){
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Contact con = TestDataGenerator.createContact('TestCon', acc);
        insert con;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('CV-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'c5@q-vb1jh15u8xqysey4oxh50kv1qjafs6k1mqfzw8ss3ab3ypkqv.61-zhsweao.na34.apex.salesforce.com';
        insert conVehi;
        
        Messaging.InboundEmail email  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        List<StaticResource> srList = [SELECT Id,Body FROM StaticResource WHERE Name='C5EmailTest'];
        
        email.plainTextBody = srList.get(0).Body.toString();
        email.fromAddress ='test@test.com';
        email.fromName = 'ABC XYZ';
        email.subject = 'Fwd: C5 Request for White Papers - C5-17-RWP-0070 - Military to Military Engagements Learning Platform (with RWP Attachment)';
        email.toaddresses = new List<String>();
        email.toaddresses.add('c5@q-vb1jh15u8xqysey4oxh50kv1qjafs6k1mqfzw8ss3ab3ypkqv.61-zhsweao.na34.apex.salesforce.com');
        
        // Update Task Order
        Messaging.InboundEmail email1  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env1 = new Messaging.InboundEnvelope();
        List<StaticResource> srList1 = [SELECT Id,Body FROM StaticResource WHERE Name='C5EmailTestUpdated'];
        
        email1.plainTextBody = srList1.get(0).Body.toString();
        email1.fromAddress ='test@test.com';
        email1.fromName = 'ABC XYZ';
        email1.subject = 'Fwd: C5 Request for White Papers - C5-17-RWP-0070 - Military to Military Engagements Learning Platform (with RWP Attachment)';
        email1.toaddresses = new List<String>();
        email1.toaddresses.add('c5@q-vb1jh15u8xqysey4oxh50kv1qjafs6k1mqfzw8ss3ab3ypkqv.61-zhsweao.na34.apex.salesforce.com');
        
        C5EmailTemplateHandler c5EmailTemplateHandler = new C5EmailTemplateHandler();
        Test.startTest();
        Messaging.InboundEmailResult result = c5EmailTemplateHandler.handleInboundEmail(email, env);
        Messaging.InboundEmailResult result1 = c5EmailTemplateHandler.handleInboundEmail(email1, env1);
        Test.stopTest();
        
        List<Task_Order__c> tOrderList = [SELECT Id, Name, Task_Order_Title__c, Description__c, Primary_Requirement__c, Requirement__c, 
                                          Contracting_Officer__c, Contract_Specialist__c, Questions_Due_Date__c, Proposal_Due_DateTime__c
                                          FROM Task_Order__c];
        Task_Order__c taskOrder = tOrderList.get(0);
        System.assertEquals(1, tOrderList.size());
        List<Contract_Mods__c> contractModList = [SELECT Id, New_Description__c, New_Requirement__c, New_Contracting_Officer__c,
                                                  New_Contract_Specialist__c, New_Questions_Due_Date__c, New_Proposal_Due_Date_Time__c 
                                                  FROM Contract_Mods__c];
        Contract_Mods__c contractMod = contractModList.get(0);
        System.assertEquals(1, contractModList.size());
        System.assertEquals(taskOrder.Description__c, contractMod.New_Description__c);
        System.assertEquals(taskOrder.Requirement__c, contractMod.New_Requirement__c);
        System.assertEquals(taskOrder.Contracting_Officer__c, contractMod.New_Contracting_Officer__c);
        System.assertEquals(taskOrder.Contract_Specialist__c, contractMod.New_Contract_Specialist__c);
        System.assertEquals(taskOrder.Questions_Due_Date__c, contractMod.New_Questions_Due_Date__c);
        System.assertEquals(taskOrder.Proposal_Due_DateTime__c, contractMod.New_Proposal_Due_Date_Time__c);
        
    }
}