global class C5EmailTemplateHandler implements Messaging.InboundEmailHandler{
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        if(Schema.sObjectType.Task_Order__c.isAccessible() && Schema.sObjectType.Contract_Vehicle__c.isAccessible()){ 
            List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:email.toAddresses.get(0)];
            if(!conVehicalList.isEmpty()){
                C5EmailTemplateParser parser = new C5EmailTemplateParser();
                parser.parse(email);//Email Parsing will be handle by parse method.
                if(parser.taskOrderNumber!=null && parser.taskOrderNumber.trim().length()>0){
                    /*Below Checked for existing TaskOrder if alredy created before*/
                    Task_Order__c taskOrder = EmailHandlerHelper.getExistTaskOrder(parser.taskOrderNumber, conVehicalList[0].ID);
                    
                    // System.debug('Question Due date in Handler 1==>'+taskOrder.Questions_Due_Date__c);
                    
                    if(taskOrder == null){//If email template not exists            
                        taskOrder = parser.getTaskOrder(email.toAddresses.get(0));
                        if(taskOrder != null){  
                            try{
                                DMLManager.insertAsUser(taskOrder);
                                EmailHandlerHelper.insertBinaryAttachment(taskOrder,email);
                                EmailHandlerHelper.insertTextAttachment(taskOrder,email);
                                EmailHandlerHelper.insertActivityHistory(taskOrder,email);
                            }catch(Exception e){
                                System.debug('error message1 = '+e.getMessage()+'\tline number = '+e.getLineNumber());
                            }
                        }
                    }else{// If TaskOrder already present then execution comes to else block   
                        Task_Order__c oldTaskOrder = taskOrder.clone(false,true,false,false);       
                        Task_Order__c updatedTakOrder = parser.updateTaskOrder(taskOrder, email.toAddresses.get(0));
                        
                        //DMLManager.updateAsUser(updatedTakOrder);
                        try{
                            System.debug('Question Due date in Handler 2==>'+oldTaskOrder.Questions_Due_Date__c);
                            EmailHandlerHelper.insertContractMods(updatedTakOrder , oldTaskOrder,email);
                            //DMLManager.updateAsUser(updatedTakOrder);
                            update updatedTakOrder;
                            EmailHandlerHelper.insertBinaryAttachment(updatedTakOrder,email);
                            EmailHandlerHelper.insertTextAttachment(updatedTakOrder,email);
                            EmailHandlerHelper.insertActivityHistory(updatedTakOrder,email);                
                            system.debug('Already Exists');
                        }catch(Exception e){
                            System.debug('error message2 = '+e.getMessage()+'\tline number = '+e.getLineNumber());
                            System.debug('error message2 = '+e);
                        }
                    }
                }
            }
            else{
                System.debug('Contract Vehicle not found.');
            }
        }
        return new Messaging.InboundEmailresult();
        return null;
    }
    
}