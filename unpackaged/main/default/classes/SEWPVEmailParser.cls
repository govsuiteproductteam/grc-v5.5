public class SEWPVEmailParser {    
    public String taskOrderNumber;
    private String requestType;
    private String agencyId;
    private String subject;
    private String agency;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String address1;
    private String address2;
    private String address3;
    private String address4;
    private Date requestDate;
    private DateTime replyByDate;
    private DateTime cutOffDate;
    private String city;
    private String state;
    private String zip;
    private String conus;
    private Integer gpat;
    private String requirements;
    private String resellerRequirements;
    public String modLevel;
    public String modDate;
    public String modRemarks;
    public String additionalRemarks;
    public String emailType;
    public boolean isCancel;
    private Contact con;
    private String ContractingOfficerName;
    
    public void parse(Messaging.InboundEmail inboundEmail){
        String emailBody = inboundEmail.plainTextBody;
        if(String.isNotBlank(emailBody)){    
            //DateTimeHelper dtHelper = new DateTimeHelper();
            
            ContractingOfficerName = inboundEmail.subject;
            if(ContractingOfficerName.contains('(') && ContractingOfficerName.contains(')')){
                try{
                    ContractingOfficerName = ContractingOfficerName.substringBetween('(', ')').SubstringAfter(',').trim();
                }catch(Exception e){
                    System.debug('Error in ContractingOfficerName:'+e.getMessage()+':'+e.getLineNumber());
                }   
            }
            System.debug('ContractingOfficerName = '+ContractingOfficerName);
            
            if(emailBody.contains('Request Type :')){
                try{
                    requestType = emailBody.substringBetween('Request Type :', 'SEWP Version :').trim();
                }catch(Exception e){
                    System.debug('Error in requestType:'+e.getMessage()+':'+e.getLineNumber());
                }   
            }
            System.debug('requestType = '+requestType);
            
            if(emailBody.contains('Email Type :')){
                try{
                    emailType = emailBody.substringBetween('Email Type :', 'Request Type :').trim();
                }catch(Exception e){
                    System.debug('Error in emailType:'+e.getMessage()+':'+e.getLineNumber());
                }   
            }
            System.debug('emailType = '+emailType);
            
            if(emailBody.contains('Request ID# :')){
                try{
                    taskOrderNumber = emailBody.substringBetween('Request ID# :','Agency ID :').trim();
                }catch(Exception e){
                    System.debug('Error in taskOrderNumber:'+e.getMessage()+':'+e.getLineNumber()); 
                }   
            }
            System.debug('taskOrderNumber = '+taskOrderNumber);
            
            if(emailBody.contains('Agency ID :')){
                try{
                    agencyId = emailBody.substringBetween('Agency ID :','Subject :').trim();
                }catch(Exception e){
                    System.debug('Error in agencyId:'+e.getMessage()+':'+e.getLineNumber()); 
                }   
            }
            System.debug('agencyId = '+agencyId);
            
            if(emailBody.contains('Subject :')){
                try{
                    subject = emailBody.substringBetween('Subject :','Request Date :').trim();
                }catch(Exception e){
                    System.debug('Error in subject:'+e.getMessage()+':'+e.getLineNumber()); 
                }   
            }
            System.debug('subject = '+subject);
            
            /*String[] subjectSplit = inboundEmail.subject.split(' ');
isCancel = false;
System.debug(''+subjectSplit);
for(String word : subjectSplit){
if(word.toLowerCase().contains('cancel'))
isCancel = true;
}*/
            isCancel = false;
            if(emailType.toLowerCase().contains('cancel'))
                isCancel = true;
            
            if(emailBody.contains('Request Date :')){
                try{
                    String requestDateRaw = emailBody.substringBetween('Request Date :','Reply by Date :').trim();
                    System.debug('requestDateRaw='+requestDateRaw);
                    String formattedDate = getFormattedDateTime(requestDateRaw);
                    DateTimeHelper dtHelper = new DateTimeHelper();
                    if(formattedDate != '')
                        requestDate = dtHelper.getDateTimeSpecialCase(formattedDate.trim()).date();
                }catch(Exception e){
                    System.debug('Error in requestDate:'+e.getMessage()+':'+e.getLineNumber()); 
                }   
            }
            System.debug('requestDate = '+requestDate);
            
            if(emailBody.contains('Reply by Date :')){
                try{
                    String replyByDateRaw = emailBody.substringBetween('Reply by Date :','Mod Level :').trim();
                    System.debug('replyByDateRaw='+replyByDateRaw);
                    DateTimeHelper dtHelper = new DateTimeHelper();
                    String formattedDate;
                    if(replyByDateRaw.contains('Cutoff Date:')){
                        formattedDate = getFormattedDateTime(replyByDateRaw.substringBefore('Q&').trim());
                        if(formattedDate != '')
                            replyByDate = dtHelper.getDateTimeSpecialCase(formattedDate.trim());
                        
                        formattedDate = getFormattedDateTime(replyByDateRaw.subStringAfter('Cutoff Date:').trim());
                        if(formattedDate != '') 
                            cutOffDate = dtHelper.getDateTimeSpecialCase(formattedDate.trim());
                    }else{
                        formattedDate = getFormattedDateTime(replyByDateRaw.trim());
                        if(formattedDate != '')
                            replyByDate = dtHelper.getDateTimeSpecialCase(formattedDate.trim());
                    }
                }catch(Exception e){
                    System.debug('Error in replyByDate or cutOffDate:'+e.getMessage()+':'+e.getLineNumber()); 
                }   
            }
            System.debug('replyByDate = '+replyByDate);
            System.debug('cutOffDate = '+cutOffDate);
            
            if(emailBody.contains('Mod Level :')){
                try{
                    modLevel = emailBody.substringBetween('Mod Level :','Mod Date :').trim();
                }catch(Exception e){
                    System.debug('Error in modLevel:'+e.getMessage()+':'+e.getLineNumber());
                }   
            }
            System.debug('modLevel = '+modLevel);
            
            if(emailBody.contains('Mod Date :')){
                try{
                    modDate = emailBody.substringBetween('Mod Date :','First Name :').trim();
                }catch(Exception e){
                    System.debug('Error in modDate:'+e.getMessage()+':'+e.getLineNumber());
                }   
            }
            System.debug('modDate = '+modDate);
            
            if(emailBody.contains('First Name :')){
                try{
                    firstName = emailBody.substringBetween('First Name :','Last Name :').trim();
                }catch(Exception e){
                    System.debug('Error in firstName:'+e.getMessage()+':'+e.getLineNumber());
                }   
            }
            System.debug('firstName = '+firstName);
            
            if(emailBody.contains('Last Name :')){
                try{
                    lastName = emailBody.substringBetween('Last Name :','Phone Number :').trim();
                }catch(Exception e){
                    System.debug('Error in lastName:'+e.getMessage()+':'+e.getLineNumber());
                }   
            }
            System.debug('lastName = '+lastName);
            
            if(emailBody.contains('Phone Number :')){
                try{
                    phone = emailBody.substringBetween('Phone Number :','Email :').trim();
                }catch(Exception e){
                    System.debug('Error in phone:'+e.getMessage()+':'+e.getLineNumber());
                }   
            }
            System.debug('phone = '+phone);
            
            if(emailBody.contains('Email :')){
                try{
                    email =  emailBody.substringBetween('Email :','Agency :').trim();
                }catch(Exception e){
                    System.debug('Error in email:'+e.getMessage()+':'+e.getLineNumber());
                }   
            }
            System.debug('email = '+email);
            
            if(emailBody.contains('Agency :')){
                try{
                    agency =  emailBody.substringBetween('Agency :','Address1 :').trim();
                }catch(Exception e){
                    System.debug('Error in agency:'+e.getMessage()+':'+e.getLineNumber());
                }   
            }
            System.debug('agency = '+agency);
            
            if(emailBody.contains('Address1 :')){
                try{
                    address1 = emailBody.substringBetween('Address1 :','Address2 :').trim();
                }catch(Exception e){
                    System.debug('Error in address1:'+e.getMessage()+':'+e.getLineNumber());
                }   
            }
            System.debug('address1 = '+address1);
            
            if(emailBody.contains('Address2 :')){
                try{
                    address2 = emailBody.substringBetween('Address2 :','Address3 :').trim();
                    if(address2!=null && address2!='')
                        address1 += '\n'+address2;  
                }catch(Exception e){
                    System.debug('Error in address2:'+e.getMessage()+':'+e.getLineNumber());
                }   
            }
            System.debug('address2 = '+address2);
            
            if(emailBody.contains('Address3 :')){
                try{
                    address3 = emailBody.substringBetween('Address3 :','Address4 :').trim();
                    if(address3!=null && address3!='')
                        address1 += '\n'+address3;  
                }catch(Exception e){
                    System.debug('Error in address3:'+e.getMessage()+':'+e.getLineNumber());
                }   
            }
            System.debug('address3 = '+address3);
            
            if(emailBody.contains('Address4 :')){
                try{                    address4 = emailBody.substringBetween('Address4 :','City :').trim();
                    if(address4!=null && address4!='')
                        address1 += '\n'+address4;  
                   }catch(Exception e){
                       System.debug('Error in address4:'+e.getMessage()+':'+e.getLineNumber());
                   }   
            }
            System.debug('address4 = '+address4);
            
            if(emailBody.contains('City :')){
                try{
                    city = emailBody.substringBetween('City :','State :').trim();
                }catch(Exception e){
                    System.debug('Error in city:'+e.getMessage()+':'+e.getLineNumber());
                }   
            }
            System.debug('city = '+city);
            
            if(emailBody.contains('State :')){
                try{
                    state = emailBody.substringBetween('State :','Zip :').trim();
                }catch(Exception e){
                    System.debug('Error in state:'+e.getMessage()+':'+e.getLineNumber());
                }   
            }
            System.debug('State = '+state);
            
            if(emailBody.contains('Zip :')){
                try{
                    zip = emailBody.substringBetween('Zip :','CONUS :').trim();
                }catch(Exception e){
                    System.debug('Error in zip:'+e.getMessage()+':'+e.getLineNumber());
                }   
            }
            System.debug('Zip = '+Zip);
            
            if(emailBody.contains('CONUS :')){
                try{
                    conus = emailBody.substringBetween('CONUS :','Agency :').trim();
                }catch(Exception e){
                    System.debug('Error in conus:'+e.getMessage()+':'+e.getLineNumber());
                }   
            }
            System.debug('conus = '+conus);
            
            if(emailBody.contains('GPAT :')){
                try{
                    gpat = Integer.valueOf(emailBody.substringBetween('GPAT :','Requirements :').trim());
                }catch(Exception e){
                    System.debug('Error in gpat:'+e.getMessage()+':'+e.getLineNumber());
                }   
            }
            System.debug('gpat = '+gpat);
            
            if(emailBody.contains('Requirements :')){
                try{
                    requirements = emailBody.substringBetween('Requirements :','Reseller Requirements :').trim();
                }catch(Exception e){
                    System.debug('Error in requirements:'+e.getMessage()+':'+e.getLineNumber());
                }   
            }
            System.debug('requirements = '+requirements);
            
            if(emailBody.contains('Reseller Requirements :')){
                try{
                    resellerRequirements = emailBody.substringBetween('Reseller Requirements :','MOD Remarks :').trim();
                }catch(Exception e){
                    System.debug('Error in resellerRequirements:'+e.getMessage()+':'+e.getLineNumber());
                }   
            }
            System.debug('resellerRequirements = '+resellerRequirements);
            
            if(emailBody.contains('MOD Remarks :')){
                try{
                    //Integer modRemarkIndex = emailBody.indexOf('MOD Remarks :');
                    //modRemarks = emailBody.substring(modRemarkIndex, emailBody.length()).trim();
                    if(emailBody.contains('Additional Remarks :')){
                        modRemarks = emailBody.substringBetween('MOD Remarks :','Additional Remarks :').trim();
                    }else{
                        modRemarks = emailBody.substringBetween('MOD Remarks :','**********End of machine readable section**********').trim();
                    }                    
                }catch(Exception e){
                    System.debug('Error in modRemarks:'+e.getMessage()+':'+e.getLineNumber());
                }   
            }
            System.debug('modRemarks = '+modRemarks);
            
            if(emailBody.contains('Additional Remarks :')){
                try{
                    additionalRemarks = emailBody.substringBetween('Additional Remarks :','**********End of machine readable section**********').trim();                    
                }catch(Exception e){
                    System.debug('Error in additionalRemarks:'+e.getMessage()+':'+e.getLineNumber());
                }   
            }
            System.debug('additionalRemarks = '+additionalRemarks);
            
        }
        // Create Contact for the Task Order  
        createContact();
    }
    
    public Task_Order__c getTaskOrder(String toAddress){ 
        if(taskOrderNumber==null){
            return null;
        }        
        Task_Order__c taskOrder = new Task_Order__c();
        List<Contract_Vehicle__c> conVehicalList = [SELECT Id FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];        
        if(!conVehicalList.isEmpty()){
            taskOrder.Contract_Vehicle__c = conVehicalList[0].Id; // Contract Vehicle
            taskOrder.Name = taskOrderNumber; //Task Order Number
            
            
            if(ContractingOfficerName != null)
                taskOrder.Contracting_Office_Name__c = ContractingOfficerName.normalizeSpace().trim(); //Contracting Office Name
            
            if(subject != null)
                taskOrder.Task_Order_Title__c = subject.normalizeSpace().trim(); //Task Order Title
            
            if(agencyId != null)
                taskOrder.SEWP_Agency_ID__c = agencyId;
            
            if(requestType != null)
                taskOrder.TO_Request_Status__c = requestType;
            
            if(requestDate != null)
                taskOrder.Release_Date__c = requestDate;
            
            if(replyByDate !=null){            
                taskOrder.Due_Date__c = replyByDate.date(); // Due Date
                taskOrder.Proposal_Due_DateTime__c = replyByDate; // Proposal Due Date/Time
            }
            
            if(cutOffDate != null)
                taskOrder.Questions_Due_Date__c = cutOffDate.date();
            
            if(agency !=null)           
                taskOrder.Agency__c = agency; // Agency
            
            if(address1 !=null){   
                try{
                    // taskOrder.Agency_Street_Address__c = address1.normalizeSpace().trim() + '\n' + address2.normalizeSpace().trim() + '\n' + address3.normalizeSpace().trim() + '\n' + address4.normalizeSpace().trim(); // Agency Street Address
                    taskOrder.Agency_Street_Address__c = address1.trim();
                }catch(Exception e){
                    
                }
            }
            
            if(city !=null)          
                taskOrder.Agency_City__c = city; // Agency City
            
            if(state !=null)          
                taskOrder.Agency_State__c = state; // Agency State
            
            if(zip !=null)
                taskOrder.Agency_Zip__c = zip; // Agency Zip
            
            if(conus !=null)
                taskOrder.Task_Order_Place_of_Performance__c = conus; // Task Order Place of Performance
            
            if(gpat !=null)
                taskOrder.GPAT__c = gpat; // GPAT
            
            if(requirements !=null)
                taskOrder.Requirement__c = requirements; // Requirements
            
            if(resellerRequirements !=null)
                taskOrder.Reseller_Requirements__c = resellerRequirements; // Reseller Requirements
            
            taskOrder.Contract_Vehicle_picklist__c = 'SEWP V - SOLUTIONS FOR ENTERPRISEWIDE PROCUREMENT';
            
            try{
                if(con.id == null)
                    DMLManager.insertAsUser(con); 
            }catch(Exception e){
                System.debug(''+e.getMessage()+'\t'+e.getLineNumber());
            }
            
            if(con.Id!= null) {           
                if(con.title != null){
                    if(con.title == 'Contract Specialist')
                        taskOrder.Contract_Specialist__c = con.Id;
                    if(con.title == 'Contracting Officer')
                        taskOrder.Contracting_Officer__c = con.Id;
                }
            }
            
            if(isCancel){
                taskOrder.Is_cancelled__c = true;
            }
            
            
            return taskOrder;
        }else{
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
        }
    }
    
    public Task_Order__c updateTaskOrder(Task_Order__c taskOrder, String toAddress){
        List<Contract_Vehicle__c> conVehicalList = [SELECT Id FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];
        
        if(!conVehicalList.isEmpty()){
            taskOrder.Contract_Vehicle__c = conVehicalList[0].Id; // Contract Vehicle
            
            if(ContractingOfficerName != null)
                taskOrder.Contracting_Office_Name__c = ContractingOfficerName.normalizeSpace().trim(); //Contracting Office Name
            
            if(subject != null)
                taskOrder.Task_Order_Title__c = subject.normalizeSpace().trim(); //Task Order Title
            
            if(agencyId != null)
                taskOrder.SEWP_Agency_ID__c = agencyId;
            
            if(requestType != null)
                taskOrder.TO_Request_Status__c = requestType;
            
            if(requestDate != null)
                taskOrder.Release_Date__c = requestDate;
            
            if(replyByDate !=null){            
                taskOrder.Due_Date__c = replyByDate.date(); // Due Date
                taskOrder.Proposal_Due_DateTime__c = replyByDate; // Proposal Due Date/Time
            }
            
            if(cutOffDate != null)
                taskOrder.Questions_Due_Date__c = cutOffDate.date();
            
            if(agency !=null)           
                taskOrder.Agency__c = agency; // Agency
            
            if(address1 !=null){   
                try{
                    taskOrder.Agency_Street_Address__c = address1.trim();
                    //taskOrder.Agency_Street_Address__c = address1.normalizeSpace().trim() + '\n' + address2.normalizeSpace().trim() + '\n' + address3.normalizeSpace().trim() + '\n' + address4.normalizeSpace().trim(); // Agency Street Address
                }catch(Exception e){
                    
                }
            }            
            if(city !=null)          
                taskOrder.Agency_City__c = city; // Agency City
            
            if(state !=null)          
                taskOrder.Agency_State__c = state; // Agency State
            
            if(zip !=null)
                taskOrder.Agency_Zip__c = zip; // Agency Zip
            
            if(conus !=null)
                taskOrder.Task_Order_Place_of_Performance__c = conus; // Task Order Place of Performance
            
            if(gpat !=null)
                taskOrder.GPAT__c = gpat; // GPAT
            
            if(requirements !=null)
                taskOrder.Requirement__c = requirements; // Requirements
            
            if(resellerRequirements !=null)
                taskOrder.Reseller_Requirements__c = resellerRequirements; // Reseller Requirements
            
            try{
                if(taskOrder.Customer_Agency__c != null){
                    con.AccountId = taskOrder.Customer_Agency__c; // Set Customer Organization of the Task Order as an Account for Contact
                    System.debug('Customer Organization = ' + con.AccountId);
                    DMLManager.upsertAsUser(con);
                    //upsert con;
                    /*if(con.Id == null){
// if Task Order with Customer Organization is not having a Contact or if a different Contact(i.e. a Contact with different Email Id) is found
DMLManager.insertAsUser(con);
}else{
// if Task Order with Customer Organization is having a Contact, then update that Contact
DMLManager.updateAsUser(con);
}*/
                }else{
                    if(con.Id == null)
                        DMLManager.insertAsUser(con);
                }  
            }catch(Exception e){
                System.debug(''+e.getMessage()+'\t'+e.getLineNumber());
            }
            
            if(con.Id!= null) {           
                if(con.title != null){
                    if(con.title == 'Contract Specialist')
                        taskOrder.Contract_Specialist__c = con.Id;
                    if(con.title == 'Contracting Officer')
                        taskOrder.Contracting_Officer__c = con.Id;
                }
            }
            
            if(isCancel){
                taskOrder.Is_cancelled__c = true;
            }
            
            return taskOrder;
        }else{
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
        }
    }
    
    private void createContact(){
        try{
            con = new Contact();
            if(email != null){
                //con = new Contact();
                List<Contact> contactList;  
                contactList = [SELECT Id, Name, Title FROM Contact WHERE email =:email];                    
                
                if(contactList != null && !contactList.isEmpty() ){//for existing contact
                    System.debug('if = '+contactList[0].id);
                    con = contactList[0];               
                }else{//for new contact
                    System.debug('else');
                    con.Is_FedTom_Contact__c = true;
                    con.Email = email;
                    con.firstName = firstName;
                    con.lastName = lastName;
                    con.Phone = phone;
                    con.Title = 'Contracting Officer';
                }
            }
        }catch(Exception e){
            System.debug(''+e.getMessage()+'\t'+e.getLineNumber());
        }
        
    }
    
    //private DateTime getDateTime(String dateStr){
    /*private Date getDateTime(String dateStr){
System.debug(''+dateStr);
try{
String[] splitDateTimeRaw = dateStr.split(' ');

String[] splitDateRawSplit = splitDateTimeRaw[0].split('-');
String rawDate = '';
String year = splitDateRawSplit[2];
String month = new DateTimeHelper().monthSet.get(splitDateRawSplit[1].toLowerCase())+'';
String day = splitDateRawSplit[0];
rawDate = year + '-' + month + '-' + day;
String hour = '00'; String minute = '00'; String second = '00';
if(splitDateTimeRaw.size() > 1){
String[] splitTimeRawSplit = splitDateTimeRaw[1].split(':');
hour = splitTimeRawSplit[0];
if(splitTimeRawSplit.size() > 1){
minute = splitTimeRawSplit[1];
}
if(splitTimeRawSplit.size() > 2){
second = splitTimeRawSplit[2];
}
rawDate += + ' ' + hour + ':' + minute + ':' + second;
}
System.debug('rawDate='+rawDate);

return Date.valueOf(rawDate);
}catch(Exception e){
system.debug(''+e.getMessage()+':'+e.getLineNumber());
return null;
}
}*/
    
    /*private DateTime getDateTime(String dateStr){        
try{
DateTimeHelper dtHelper = new DateTimeHelper();
String filterDateStr = dateStr.replace('- ','/').replace('-','/');            
String[] splitDateTime = filterDateStr.split(' ');
if(splitDateTime.size() == 1){
String[] splitDate = splitDateTime[0].split('/');
return DateTime.newInstance(Integer.valueOf(splitDate[2].trim()), dtHelper.monthSet.get(splitDate[1].toLowerCase().trim()), Integer.valueOf(splitDate[0].trim()));
}else{
String[] splitTime = splitDateTime[1].split(':');               
String[] splitDate = splitDateTime[0].split('/');
return DateTime.newInstance(Integer.valueOf(splitDate[2].trim()), dtHelper.monthSet.get(splitDate[1].toLowerCase().trim()), Integer.valueOf(splitDate[0].trim()),Integer.valueOf(splitTime[0].trim()),Integer.valueOf(splitTime[1].trim()),0);
}

}catch(Exception e){
system.debug(''+e.getMessage()+':'+e.getLineNumber());
}
return null;
}*/
    
    private String getFormattedDateTime(String dateStr){ 
        // Ex. Original - "18-Jan- 2018"(dd-mm-yyyy) and Formatted - "1/18/2018"(mm/dd/yyyy)
        // Ex. Original - "13-08- 2018 18:00"
        String formattedDate = '';
        try{
            DateTimeHelper dtHelper = new DateTimeHelper();
            String filterDateStr = dateStr.replace('- ','/').replace('-','/');  
            String[] filterDateStrSplit = filterDateStr.split(' ');
            if(filterDateStrSplit.size() > 0){
                String dateString = filterDateStrSplit[0];
                String lastWord = filterDateStrSplit[filterDateStrSplit.size() - 1];
                if(dateString.contains('/')){
                    String[] dateStringSplit = dateString.split('/');
                    if(dateStringSplit.size() > 0){
                        // Get numeric value for month
                        Integer numericMonth = dtHelper.monthSet.get(dateStringSplit[1].toLowerCase().trim());
                        if(numericMonth != null){
                            formattedDate += numericMonth + '/' + dateStringSplit[0].trim() + '/' + dateStringSplit[2].trim();
                        }else{
                            formattedDate += dateStringSplit[1].trim() + '/' + dateStringSplit[0].trim() + '/' + dateStringSplit[2].trim();
                        }
                    }
                }
                
                if(formattedDate != ''){
                    if(filterDateStrSplit.size() > 1)
                        formattedDate += ' ' + filterDateStrSplit[1].trim();
                    
                    if(!formattedDate.toLowerCase().endsWith('t') || !formattedDate.toLowerCase().endsWith('time'))
                        formattedDate += ' ET';
                }
                return formattedDate;  
            }
        }catch(Exception e){
            System.debug('Error in getFormattedDateTime:'+e.getMessage()+':'+e.getLineNumber());
        }
        return dateStr;
    }
}