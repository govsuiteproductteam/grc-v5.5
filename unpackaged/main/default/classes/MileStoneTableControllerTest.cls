@isTest
public class MileStoneTableControllerTest {
    public static testMethod void testMilestoneTable(){
		Account account = TestDataGenerator.createAccount('Test Account');
        insert account;
        Contract_Vehicle__c contractVehicle =  TestDataGenerator.createContractVehicle('12345', account);
        insert contractVehicle;

       // Id recordTypeId = Schema.SObjectType.Value_Table__c.getRecordTypeInfosByName().get('Bookings').getRecordTypeId();
        Id milestoneBillingRecordTypeId = Schema.SObjectType.Value_Table__c.getRecordTypeInfosByName().get('Milestone Billing').getRecordTypeId();
      
        Value_Table__c valueTable1 = TestDataGenerator.createValueTable(milestoneBillingRecordTypeId,contractVehicle.Id);
        insert valueTable1;
        
        Value_Table__c valueTable2 = TestDataGenerator.createValueTable(milestoneBillingRecordTypeId,contractVehicle.Id);
        insert valueTable2;
        
        Value_Table__c valueTable3 = TestDataGenerator.createValueTable(milestoneBillingRecordTypeId,contractVehicle.Id);
        insert valueTable3;
        
        Id valueTableRecordId = valueTable2.Id;
        Test.startTest();
        List<Value_Table__c> mileTableList = new List<Value_Table__c>();
        mileTableList.add(valueTable1);
        String mileTableListStr = JSON.serialize(mileTableList);
        TM_MileStoneTableController.saveContractValTable(mileTableListStr, contractVehicle.Id, 'abc,'+valueTableRecordId);
        List<Value_Table__c> valueTableList = [SELECT Id FROM Value_Table__c WHERE id =: valueTableRecordId];
        System.assert(valueTableList.size() == 0);
        Contract_Vehicle__c contractVehicle1 =  TM_MileStoneTableController.getContractVehical(contractVehicle.Id);
        System.assert(contractVehicle1 != null);
        List<Value_Table__c> valueTableList1 = TM_MileStoneTableController.getValueTableContValList(contractVehicle.Id);
        System.assert(valueTableList1.size() == 2);
        Value_Table__c newValueTable = TM_MileStoneTableController.newRowContValTable(contractVehicle.Id);
        System.assert(newValueTable != null);
        TM_MileStoneTableController.isLightningPage();
        Test.stopTest();
    }
}