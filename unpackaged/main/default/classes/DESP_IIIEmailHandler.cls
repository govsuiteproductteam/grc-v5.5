global class DESP_IIIEmailHandler implements Messaging.InboundEmailHandler {
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:email.toAddresses.get(0)];
        if(!conVehicalList.isEmpty()){
            DESP_IIIEmailParser parser = new DESP_IIIEmailParser();
            parser.parse(email);//Email Parsing will be handle by parse method.
                Task_Order__c taskOrder = parser.getTaskOrder(email.toAddresses.get(0));  // Map Values for New Task Order
                    if(taskOrder != null){  
                        try{
                            DMLManager.insertAsUser(taskOrder); // Insert New Task Order
                            EmailHandlerHelper.insertBinaryAttachment(taskOrder, email);
                            EmailHandlerHelper.insertTextAttachment(taskOrder, email);
                            EmailHandlerHelper.insertActivityHistory(taskOrder, email);
                        }catch(Exception e){
                            System.debug('error message = '+e.getMessage());
                        }
                    }
        } else{
            System.debug('Contract Vehicle not found.');
        }
        return new Messaging.InboundEmailresult();
    }
    
}