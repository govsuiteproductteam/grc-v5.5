@isTest
public class ITES3HProductEmailHandlerTest {
    
    public static testmethod void testCase1(){
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Contact con = TestDataGenerator.createContact('TestCon', acc );
        insert con;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('T-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'ites3h_product@k31x9pxwiyp8dwfsyv2j2e8o92jwvzzsdkk5js7po0bbyx974.61-zhsweao.na34.apex.salesforce.com';
        
        insert conVehi;
        
        //for new Task Order Creation  
        Messaging.InboundEmail email  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        /*List<StaticResource> srList = [SELECT Id,Body FROM StaticResource WHERE Name='ITES3HEmailTest'];
email.plainTextBody = srList.get(0).Body.toString();*/
        email.plainTextBody = 'Dear Intelligent Decisions,\n'+
            'The CHESS IT e-mart has brought you a potential customer! Keep in mind that CHESS customers are awaiting\n'+
            'your response, and your timeliness in submitting a Bid or No Bid would be greatly appreciated. By accessing\n'+
            'the Request for Quote (RFQ) below, you are acknowledging the receipt of the RFQ.\n'+
            'Please coordinate with the customer concerning their pending RFQ by logging into the Request Tool.\n'+
            'Your Point of Contact Is:\n'+
            'Aaron West\n'+
            'Email: aaron.m.west19.civ@mail.mil\n'+
            'Phone: 973-724-6034\n'+
            'Organization: ARMY / ARMY MATERIEL COMMAND\n'+
            'The following outlines the details of this request:\n'+
            'Request for Quote ID: 230129\n'+
            'Request for Quote Name: B407 - PAISD - M.2 HDD\n'+
            'Product Category: Storage and Network Storage\n'+
            'Request for Quote Due Date: 2/12/2018\n'+
            'Request for Quote # of Attachments: 0\n'+
            'Details: Please provide a quote for 2x SAMSUNG 960 EVO M.2 500GB NVMe PCI-Express 3.0 x4 Internal\n'+
            'Solid State Drive (SSD) MZ-V6E500BW or any brand equivalent with capacity 500GB or higher.\n'+
            'POC: Aaron West - aaron.m.west19.civ@mail.mil\n'+
            'Agency: ARMY\n'+
            'Activity: U.S. ARMY MATERIEL COMMAND, (AMC)\n'+
            'Installation: PICATINNY ARSENAL\n'+
            'Contracting Office: Army Contracting Command - New Jersey\n'+
            'Major End Item: Hard Drive\n'+
            '\n'+
            'Thank you for using the CHESS IT e-mart system!\n';
        email.fromAddress ='test@test.com';
        email.fromName = 'ABC XYZ';
        email.subject = 'ITES-3H RFQ Created - Reference #230129';
        email.toaddresses = new List<String>();
        email.toaddresses.add('ites3h_product@k31x9pxwiyp8dwfsyv2j2e8o92jwvzzsdkk5js7po0bbyx974.61-zhsweao.na34.apex.salesforce.com');
        
        //For Updating existing task order with existing Contact
        Messaging.InboundEmail email1  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env1 = new Messaging.InboundEnvelope();
        /*List<StaticResource> srList1 = [SELECT Id,Body FROM StaticResource WHERE Name='ITES3HEmailTestUpdated'];
email1.plainTextBody = srList1.get(0).Body.toString();*/
        email1.plainTextBody = 'ABC XYZ | PQR | www.test.com\n'+
            'Email: test@test.com | Office: (710)105-1243\n'+
            '\n'+
            '---------- Forwarded message ----------\n'+
            'From: ABC XYZ <abc@test.com>\n'+
            'Date: Mon, Feb 12, 2018 at 9:23 PM\n'+
            'Subject: ITES-3H RFQ Created - Reference #230129\n'+
            'To: ites3h@l-viwly58f08zktgac6y8ugqiw174v7yge8yhakyvaacmiph9ki.37-pvateac.na31.apex.salesforce.com\n'+
            '\n'+
            'Dear Intelligent Decisions,\n'+
            'The CHESS IT e-mart has brought you a potential customer! Keep in mind that CHESS customers are awaiting\n'+
            'your response, and your timeliness in submitting a Bid or No Bid would be greatly appreciated. By accessing\n'+
            'the Request for Quote (RFQ) below, you are acknowledging the receipt of the RFQ.\n'+
            'Please coordinate with the customer concerning their pending RFQ by logging into the Request Tool.\n'+
            'Your Point of Contact Is:\n'+
            'Aaron West\n'+
            'Email: aaron.m.west19.civ@mail.mil\n'+
            'Phone: 973-724-6034\n'+
            'Organization: ARMY / ARMY MATERIEL COMMAND\n'+
            'The following outlines the details of this request:\n'+
            'Request for Quote ID: 230129\n'+
            'Request for Quote Name: B407 - PAISD - M.2 HDD 1\n'+
            'Product Category: Storage and Network Storage\n'+
            'Request for Quote Due Date: 2/12/2018\n'+
            'Request for Quote # of Attachments: 0\n'+
            'Details: Please provide a quote for 2x SAMSUNG 960 EVO M.2 500GB NVMe PCI-Express 3.0 x4 Internal\n'+
            'Solid State Drive (SSD) MZ-V6E500BW or any brand equivalent with capacity 500GB or higher.\n'+
            'POC: Aaron West - aaron.m.west19.civ@mail.mil\n'+
            'Agency: ARMY\n'+
            'Activity: U.S. ARMY MATERIEL COMMAND, (AMC)\n'+
            'Installation: PICATINNY ARSENAL\n'+
            'Contracting Office: Army Contracting Command - New Jersey\n'+
            'Major End Item: Hard Drive\n'+
            '\n'+
            'Thank you for using the CHESS IT e-mart system!\n';
        email1.fromAddress ='test@test.com';
        email1.fromName = 'ABC XYZ';
        email1.subject = 'Subject: ITES-3H RFQ Created - Reference #230129';
        email1.toaddresses = new List<String>();
        email1.toaddresses.add('ites3h_product@k31x9pxwiyp8dwfsyv2j2e8o92jwvzzsdkk5js7po0bbyx974.61-zhsweao.na34.apex.salesforce.com');
        
        //For task order Amendment
        Messaging.InboundEmail email2  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env2 = new Messaging.InboundEnvelope();
        /*List<StaticResource> srList2 = [SELECT Id,Body FROM StaticResource WHERE Name='ITES3HEmailTestUpdated_1'];
email2.plainTextBody = srList2.get(0).Body.toString();*/
        email2.plainTextBody = 'Dear Intelligent Decisions,\n'+
            'This email is to inform you that a customer has a recently amended a Request For Quote (RFQ) that was\n'+
            'previously submitted to you. This amendment has re-opened the RFQ. Please respond to the customer&#39;s RFQ\n'+
            'for action.\n'+
            'The RFQ information is:\n'+
            'RFQ ID: 230076\n'+
            'Proposal: Hard drive compatible with Dell Precision 7520 laptop. Quantity 40.\n'+
            'See attached Salient Characteristics.\n'+
            'POC: Juliana Capasso E-mail: Juliana.c.capasso.ctr@mail.mil\n'+
            'Date Due: 2/12/2018\n'+
            'Attachments: 2\n'+
            'Thank you for using the CHESS IT e-mart!';
        email2.fromAddress ='test@test.com';
        email2.fromName = 'ABC XYZ';
        email2.subject = 'ITES-3H RFQ Amendment - Reference #230129';
        email2.toaddresses = new List<String>();
        email2.toaddresses.add('ites3h_product@k31x9pxwiyp8dwfsyv2j2e8o92jwvzzsdkk5js7po0bbyx974.61-zhsweao.na34.apex.salesforce.com');
        
        //For task order cancellation
        Messaging.InboundEmail email3  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env3 = new Messaging.InboundEnvelope();
        /*List<StaticResource> srList3 = [SELECT Id,Body FROM StaticResource WHERE Name='ITES3HEmailTestUpdated_2'];
email3.plainTextBody = srList2.get(0).Body.toString();*/
        email3.plainTextBody = 'Dear Intelligent Decisions,\n'+
            'This email is to inform you that a customer recently cancelled the Request for Quote (RFQ) below.\n'+
            'Proposal ID: 230129\n'+
            'The reason given for cancellation was: &quot;Change in Requirement&quot;.\n'+
            'Thank you for using the CHESS IT e-mart system!\n';
        email3.fromAddress ='test@test.com';
        email3.fromName = 'ABC XYZ';
        email3.subject = 'ITES-3H RFQ Cancellation - Reference #230129';
        email3.toaddresses = new List<String>();
        email3.toaddresses.add('ites3h_product@k31x9pxwiyp8dwfsyv2j2e8o92jwvzzsdkk5js7po0bbyx974.61-zhsweao.na34.apex.salesforce.com');
        
        
        //For New RFI Task Order Template
        Messaging.InboundEmail email4  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env4 = new Messaging.InboundEnvelope();
        /*List<StaticResource> srList3 = [SELECT Id,Body FROM StaticResource WHERE Name='ITES3HEmailTestUpdated_2'];
email3.plainTextBody = srList2.get(0).Body.toString();*/
        email4.plainTextBody = 'Dear Intelligent Decisions,\n'+
            'The CHESS IT e-mart has brought you a potential customer! Keep in mind that CHESS customers are awaiting your response, and your timeliness in submitting a Bid or No Bid would be greatly appreciated. By accessing the Request for Information (RFI) below, you are acknowledging the receipt of the RFI.\n'+
            'Please respond to the customers pending RFI by logging into the RFI Tool.\n'+
            'Your Point of Contact Is:\n'+
            'Monica Senthill\n'+
            'Email: monica.r.senthill@usace.army.mil\n'+
            'Phone: 011-81-46-407-5415\n'+
            'Organization: ARMY / U.S. ARMY CORPS OF ENGINEERS, (USACE)\n'+
            'The following outlines the details of this request:\n'+
            'Request for Information ID: 240302\n'+
            'Request for Information Name: iOS_Case_Readers\n'+
            'Request for Information Due Date: 5/18/2018\n'+
            'Request for Information # of Attachments: 0\n'+
            'Details: iOS_Case_Readers\n'+
            'Agency: ARMY\n'+
            'Activity: U.S. ARMY CORPS OF ENGINEERS, (USACE)\n'+
            'Installation: CAMP ZAMA/SAGAMIHARA\n'+
            'Thank you for using the CHESS IT e-mart system!\n'+
            'This email was generated automatically. DO NOT REPLY. Questions should be directed to the CHESS Customer Support Team at: armychess@mail.mil or (888) 232-4405.\n'+
            'PLEASE NOTE THIS IS PROPRIETARY INFORMATION AND SHOULD NOT BE SHARED WITH OTHER VENDORS.\n'+
            'To stop receiving emails from the CHESS IT e-mart, please log in to CHESS and set your email preferences under My Account: Email Subscriptions';
        email4.fromAddress ='test@test.com';
        email4.fromName = 'ABC XYZ';
        email4.subject = 'ITES-3H RFI Created - Reference #240302';
        email4.toaddresses = new List<String>();
        email4.toaddresses.add('ites3h_product@k31x9pxwiyp8dwfsyv2j2e8o92jwvzzsdkk5js7po0bbyx974.61-zhsweao.na34.apex.salesforce.com');
        
        ITES3HProductEmailHandler handler = new ITES3HProductEmailHandler();
        Test.startTest();
        Messaging.InboundEmailResult result = handler.handleInboundEmail(email, env);
        List<Task_Order__c> taskOrderList = [SELECT Id FROM Task_Order__c];
        Messaging.InboundEmailResult result1 = handler.handleInboundEmail(email1, env1);
        List<Contract_Mods__c> contractModList = [SELECT Id FROM Contract_Mods__c];
        Messaging.InboundEmailResult result2 = handler.handleInboundEmail(email2, env2);
        Messaging.InboundEmailResult result3 = handler.handleInboundEmail(email3, env3);
        Messaging.InboundEmailResult result4 = handler.handleInboundEmail(email4, env4);
        
        Test.stopTest();
        
        System.assertEquals(1, taskOrderList.size());
        System.assertEquals(contractModList.size(), 1);
        
    }
    
  /*  public static testmethod void testCase2(){
        
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Contact con = TestDataGenerator.createContact('TestCon', acc );
        insert con;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('T-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'ites3h_product@k31x9pxwiyp8dwfsyv2j2e8o92jwvzzsdkk5js7po0bbyx974.61-zhsweao.na34.apex.salesforce.com';
        
        insert conVehi;
        
        String str = 'Dear Intelligent Decisions,\n'+
            'The CHESS IT e-mart has brought you a potential customer! Keep in mind that CHESS customers are awaiting your response, and your timeliness in submitting a Bid or No Bid would be greatly appreciated. By accessing the Request for Information (RFI) below, you are acknowledging the receipt of the RFI.\n'+
            'Please respond to the customers pending RFI by logging into the RFI Tool.\n'+
            'Your Point of Contact Is:\n'+
            'Monica Senthill\n'+
            'Email: monica.r.senthill@usace.army.mil\n'+
            'Phone: 011-81-46-407-5415\n'+
            'Organization: ARMY / U.S. ARMY CORPS OF ENGINEERS, (USACE)\n'+
            'The following outlines the details of this request:\n'+
            'Request for Information ID: 240302\n'+
            'Request for Information Name: iOS_Case_Readers\n'+
            'Request for Information Due Date: 5/18/2018\n'+
            'Request for Information # of Attachments: 0\n'+
            'Details: iOS_Case_Readers\n'+
            'Agency: ARMY\n'+
            'Activity: U.S. ARMY CORPS OF ENGINEERS, (USACE)\n'+
            'Installation: CAMP ZAMA/SAGAMIHARA\n'+
            'Thank you for using the CHESS IT e-mart system!\n'+
            'This email was generated automatically. DO NOT REPLY. Questions should be directed to the CHESS Customer Support Team at: armychess@mail.mil or (888) 232-4405.\n'+
            'PLEASE NOTE THIS IS PROPRIETARY INFORMATION AND SHOULD NOT BE SHARED WITH OTHER VENDORS.\n'+
            'To stop receiving emails from the CHESS IT e-mart, please log in to CHESS and set your email preferences under My Account: Email Subscriptions';
        
        //for new Task Order Creation  
        Messaging.InboundEmail email  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        email.plainTextBody = str;
        email.fromAddress ='test@test.com';
        email.fromName = 'ABC XYZ';
        email.subject = 'ITES-3H RFI Created - Reference #230129';
        email.toaddresses = new List<String>();
        email.toaddresses.add('ites3h_product@k31x9pxwiyp8dwfsyv2j2e8o92jwvzzsdkk5js7po0bbyx974.61-zhsweao.na34.apex.salesforce.com');
        
        Messaging.InboundEmail email1  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env1 = new Messaging.InboundEnvelope();
        String str1 = str;
        str1 = str1.replace('Request for Information Due Date:', '');
        email1.plainTextBody = str1;
        email1.fromAddress ='test@test.com';
        email1.fromName = 'ABC XYZ';
        email1.subject = 'ITES-3H RFI Created - Reference #230129';
        email1.toaddresses = new List<String>();
        email1.toaddresses.add('ites3h_product@k31x9pxwiyp8dwfsyv2j2e8o92jwvzzsdkk5js7po0bbyx974.61-zhsweao.na34.apex.salesforce.com');
        
         Messaging.InboundEmail email2  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env2 = new Messaging.InboundEnvelope();
        String str2 = str;
        str2=str2.replace('Agency:','');
        System.debug('+========================='+str2);
        email2.plainTextBody = str2;
        email2.fromAddress ='test@test.com';
        email2.fromName = 'ABC XYZ';
        email2.subject = 'ITES-3H RFI Created - Reference #230129';
        email2.toaddresses = new List<String>();
        email2.toaddresses.add('ites3h_product@k31x9pxwiyp8dwfsyv2j2e8o92jwvzzsdkk5js7po0bbyx974.61-zhsweao.na34.apex.salesforce.com');
        
        
        
        
        ITES3HProductEmailHandler handler = new ITES3HProductEmailHandler();
        Test.startTest();
        Messaging.InboundEmailResult result = handler.handleInboundEmail(email, env);
        Messaging.InboundEmailResult result1 = handler.handleInboundEmail(email1, env1);
        Messaging.InboundEmailResult result2 = handler.handleInboundEmail(email2, env2);
        List<Task_Order__c> taskOrderList = [SELECT Id FROM Task_Order__c];
        List<Contract_Mods__c> contractModList = [SELECT Id FROM Contract_Mods__c];
        Test.stopTest();
        
        System.assertEquals(1, taskOrderList.size());
        System.assertEquals(contractModList.size(), 1);
        
    }*/
    
}