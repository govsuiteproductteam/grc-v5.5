public with sharing  class FLSController{
    
    public static String getQueryWithFLS(String inputQuery,String ObjectApiName){
        /*if(inputQuery != null && inputQuery.trim().length() > 0){
            
            if(Schema.getGlobalDescribe().get(ObjectApiName).getDescribe().isAccessible()){
                String[] apiNameString = inputQuery.split(',');
                
                Schema.getGlobalDescribe().get(ObjectApiName);
            } 
        }*/
        if(inputQuery != null && inputQuery.trim().length() > 0 && ObjectApiName != null && ObjectApiName.trim().length() > 0){
            if(Schema.getGlobalDescribe().get(ObjectApiName.trim()).getDescribe().isAccessible()){
                String queryString = '';
                Schema.DescribeSobjectResult result = Schema.describeSObjects(new List<String>{ObjectApiName})[0];
                Map<String, Schema.SobjectField> fieldMap = result.fields.getMap();
                for(String api : inputQuery.split(',')){
                    Schema.SobjectField sof = fieldMap.get(api.trim());
                    if(sof != Null){
                        if(sof.getDescribe().isAccessible()){
                            queryString += api+',';
                        }
                    }
                }
                
                queryString = queryString.removeEnd(',');
                
                return queryString;
            }
        }
        return '';
    }
    
}