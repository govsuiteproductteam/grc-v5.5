@RestResource(urlMapping='/GSAConnectGET/*')
global with sharing class GSAConnectGET{
    static String statusTemp;

   /* @HttpGet
    global static String doGet()
    {
        statusTemp = '';
        try{
            RestRequest req = RestContext.request;
            String strBody = req.requestBody.toString();
            //System.debug('Hi This is Body = '+strBody);
            
        }
        catch(Exception ee){}
        list<GSA_Connect__c> gsaConnectList = [SELECT RFG_Id__c, Contract_Number__c, Task_Order_Id__c FROM GSA_Connect__c ORDER BY lastmodifieddate DESC limit 1];
        //String retValue = 'rfqId=RFQ1071729##contractNumber=GS-10F-0101S';
        String retValue; 
        if(gsaConnectList.size() > 0)
        {
            retValue = 'rfqId='+gsaConnectList[0].RFG_Id__c+'##contractNumber='+gsaConnectList[0].Contract_Number__c;
        }
        else
        {
            retValue = 'No Data Found';
        }
        
        
        
        return retValue ;
    }*/
    
    @HttpPost
    global static String doPost(){
        statusTemp = '';
        String urls = '';
        try{
            RestRequest req = RestContext.request;
            String strBody = req.requestBody.toString();
            //System.debug('Hi This is Body = '+strBody);
            urls = GSAHTMLParserNew.parse(strBody);
           
        }
        catch(Exception ee){}
        System.debug('Final Url list = '+urls);
        String[] splitList = urls.split('SPLITURL');
        Integer i=0;
        for(String str: splitList ){
            System.debug('i  = '+i+'  =  ' +str);
            i++;
        }
        System.debug('Final Url list size  = '+new Set<String>(splitList).size());
        System.debug('urls = '+urls);
        return urls;
    }   
    
}