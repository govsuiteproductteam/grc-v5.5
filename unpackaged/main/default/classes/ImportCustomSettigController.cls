public with sharing class ImportCustomSettigController {

    public ImportCustomSettigController(){
    }
    
    public void import(){
        ManageContractCustomSetting manageSetting =new ManageContractCustomSetting();
        String str=manageSetting.updateCustomeSetting();
        if(str != null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, str ));
        }
        ManageSubContractCustomSetting manageSubContractSetting = new ManageSubContractCustomSetting();
        String str1=manageSubContractSetting.updateCustomeSetting();
        if(str1 != null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, str1 ));
        }
    }
    
    public void export(){
        ManageContractCustomSetting manageSetting =new ManageContractCustomSetting();
        manageSetting.storeCustomeSetting();
        System.debug('Subcontract Start');
        ManageSubContractCustomSetting manageSubContractSetting = new ManageSubContractCustomSetting();
        manageSubContractSetting.storeCustomeSetting();
        System.debug('Subcontract End');
    }
    
    public void clear(){
        ManageContractCustomSetting manageSetting =new ManageContractCustomSetting();
        manageSetting.clearCustomSetting();
        
        ManageSubContractCustomSetting manageSubContractSetting = new ManageSubContractCustomSetting();
        manageSubContractSetting.clearCustomSetting();
    }
}