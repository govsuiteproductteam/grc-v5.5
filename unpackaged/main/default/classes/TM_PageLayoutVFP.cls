public class TM_PageLayoutVFP {
	 @TestVisible private TM_PageLayoutController plController;
    
    public transient String vfpPageLabel{get;set;}
    public transient String vfpPageAPI{get;set;}
    public transient Integer vfpWidth{get;set;}
    public transient Integer vfpHeight{get;set;}
    public transient Integer vfpTabSequence{get;set;}
    public transient Integer vfpSequenceUpdate{get;set;}
    public transient Id recordCloinId{get;set;}
    public transient Boolean vfpIsComponant{get;set;}
    public transient String vfpColor{get;set;}
    public transient String vfpAppApi{get;set;}
    public TM_PageLayoutVFP(TM_PageLayoutController plController){
        this.plController = plController;
    }
    
    public void addVFPPage(){
        if(vfpPageLabel!=Null && vfpPageLabel!='' && vfpPageAPI!=Null && vfpPageAPI!='' && vfpTabSequence!=Null){
            if(vfpSequenceUpdate!=Null && vfpSequenceUpdate>0){
                plController.tabLanding = vfpTabSequence;
                vfpTabSequence -= 1;
                vfpSequenceUpdate -= 1;
                try{
                   CLM_Section__c tempVfp = plController.tabWrapperList[vfpTabSequence].sectionWrapperList[vfpSequenceUpdate].vfCustomSection;
                   tempVfp.SectionName__c = vfpPageLabel;
                   //tempVfp.InlineLabel__c = vfpPageLabel;
                   tempVfp.Inline_Api_Name_Component_Api__c = vfpPageAPI;
                   tempVfp.Width__c = vfpWidth;
                   tempVfp.Height__c = vfpHeight;
                   tempVfp.BackgroundColor__c = vfpColor;
                   tempVfp.AppApi__c = vfpAppApi;
                   if(vfpIsComponant){
                        tempVfp.Is_Lightning_Component__c = true;
                        tempVfp.Is_InlineVF_Page__c = false;
                    }else{
                        tempVfp.Is_Lightning_Component__c = false;
                        tempVfp.Is_InlineVF_Page__c = true;
                    }
                }catch(Exception e){}
                
            }else{
                CLM_Section__c tempVfp = new CLM_Section__c(SectionName__c=vfpPageLabel, Inline_Api_Name_Component_Api__c=vfpPageAPI, Width__c = vfpWidth, Height__c = vfpHeight, Is_Lightning_Component__c = vfpIsComponant, Is_InlineVF_Page__c = (!vfpIsComponant), BackgroundColor__c = vfpColor,AppApi__c = vfpAppApi);
                plController.addVFPPage(tempVfp,vfpTabSequence);
            }
        }
    }
    
    public void deleteVFPPage(){
        if(vfpTabSequence != Null && vfpSequenceUpdate != Null){
			plController.deleteVFPPage(vfpTabSequence,vfpSequenceUpdate);
        }
    }
    
    //VFP Rearrange
     public void vfpRearrange(){
         if(vfpTabSequence!=Null && vfpTabSequence>0){
            vfpTabSequence -= 1;
            plController.tabRearrangeList.clear();
           try{
               for(TM_PageLayoutController.VfpWrapper tempVfpWrap : plController.tabWrapperList[vfpTabSequence].VfpWrapperList){
                   plController.tabRearrangeList.add(tempVfpWrap.vfpPage.InlineLabel__c);
               }
           }catch(Exception e){}
         }
    }
    
    public void applyVfpRearrange(){
        if(plController.tabRearrangeString != Null && vfpTabSequence!=Null && vfpTabSequence>0){
            plController.tabLanding = vfpTabSequence;
            vfpTabSequence -= 1;
            Integer i=1;
            try{
                for(String reVfpName : plController.tabRearrangeString.split('SPLITXCOMMA')){//newlly added split('SPLITXCOMMA') in place of split(',') 17-05-2018
                    if(reVfpName!=''){
                        if(reVfpName.contains('&amp;')){
                            reVfpName.replace('&amp;','&');
                        }
                        for(TM_PageLayoutController.VfpWrapper tempVfpWrap : plController.tabWrapperList[vfpTabSequence].VfpWrapperList){
                            if(tempVfpWrap.vfpPage.InlineLabel__c.replace(' ','')+''+tempVfpWrap.vfpOrder == reVfpName.replace(' ','')){
                                tempVfpWrap.vfpOrder = i;
                                break;
                            }
                        }
                        i++;
                    }                
                }
                plController.tabWrapperList[vfpTabSequence].VfpWrapperList.sort();
                plController.tabRearrangeList.clear();
            }catch(Exception e){}
        }
    }
    
    public void cloneLayout(){
        plController.init(recordCloinId, true);
        plController.cloneInitiated = true;
        plController.tempRecordId = (''+plController.recordTypeId).substring(0,15);
    }
    
    public void go(){
        if(plController.objectType == 'Sub Contract'){
            plController.isContract = false;
        }else{
            plController.isContract = true;
        }
        plController.cloneInitiated = false;
        plController.init(plController.recordTypeId, false);
    }
}