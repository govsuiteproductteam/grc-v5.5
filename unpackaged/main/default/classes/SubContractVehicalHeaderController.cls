public class SubContractVehicalHeaderController {
public static Map<String, Schema.SObjectField> fieldsMapAccessible{get;set;}
    
    
    static{
         fieldsMapAccessible=Schema.getGlobalDescribe().get('TM_TOMA__SubContract__c').getDescribe().SObjectType.getDescribe().fields.getMap();
    }
    
      @AuraEnabled
    public static String checkLicensePermition1()
    {
        return LincenseKeyHelper.checkLicensePermitionForFedCLM();
    }
    
 @AuraEnabled
    public static TM_TOMA__SubContract__c getContractVehicle(String contractVehicleId) {
    	if(Schema.sObjectType.TM_TOMA__SubContract__c.isAccessible()){
        List<TM_TOMA__SubContract__c> contractVehicleList = New List<TM_TOMA__SubContract__c>();
        if(contractVehicleId != null && contractVehicleId !=''){
            //String query1='SELECT '+getAllFields('TM_TOMA__SubContract__c')+' FROM TM_TOMA__SubContract__c WHERE '+' id = :contractVehicleId ';
            //newlly added 24-05-2018 for onlly get at a glance field
       String query1='SELECT '+getAllATAGlanceFields('TM_TOMA__SubContract__c',contractVehicleId)+' FROM TM_TOMA__SubContract__c WHERE '+' id = :contractVehicleId ';
            contractVehicleList= Database.query(query1);
        }
        if(contractVehicleList != null && ! contractVehicleList.isEmpty()){
            return contractVehicleList[0];
        }
        else{return null;}
        }
        else{
            return null;
        }
    }
     @AuraEnabled
    public static string changeOwnerIdFromHeader(String recordId,String ownId){
        //system.debug('@@@@@@@@@'+recordId+' '+ownId);
        if(recordId != ''  && ownId != '')
        {
            if(Schema.sObjectType.TM_TOMA__SubContract__c.isAccessible()){
           // SubContract__c contractVehicle=[select Id,OwnerId from SubContract__c where Id =:recordId];
            String query1='SELECT '+getAllFields('TM_TOMA__SubContract__c')+' FROM TM_TOMA__SubContract__c WHERE '+' id = :recordId ';
            TM_TOMA__SubContract__c contractVehicle = Database.query(query1);
            contractVehicle.OwnerId=ownId;
            update contractVehicle;
           return contractVehicle.OwnerId;  
            }
            else
            {
                return null;
            }
        }
        else{ return null;}
    }
    
    @AuraEnabled
    public static List<SubContractVehicalHeaderController.AtAGlanceWrap> getMetadata(String recordId){
        System.debug('hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh'+recordId);
          Map<String,String> fieldDatatypeMap = new Map<String,String>();
          Map<String,Schema.SObjectField> M = new Map<String,Schema.SObjectField>();
          M =  Schema.SObjectType.TM_TOMA__SubContract__c.fields.getMap();
         string recId;
        if(Schema.sObjectType.TM_TOMA__SubContract__c.isAccessible() && Schema.sObjectType.CLM_AtaGlanceField__c.isAccessible()){
         List<TM_TOMA__SubContract__c> contractVehicle = [SELECT Id,Name,recordTypeId FROM TM_TOMA__SubContract__c WHERE Id =:recordId limit 1];
         system.debug('contractVehicle '+contractVehicle);
        
        //system.debug('opp recordTypeId '+opp[0].recordTypeId);
         
         if(contractVehicle[0].recordTypeId != Null)
         {
             system.debug('Inside recordTypeId If'+contractVehicle[0].recordTypeId);
             recId=contractVehicle[0].recordTypeId;
             recId=recId.substring(0,15);
             system.debug('recId '+recId);
         }
        
         List<CLM_AtaGlanceField__c> clmAtAGlanceList= new List<CLM_AtaGlanceField__c>();
        // if(contractVehicle[0].recordTypeId != Null || contractVehicle[0].recordTypeId != '')
        if(recId != '')
         clmAtAGlanceList=[select Color__c,ColSpan__c,Document_Id__c,FieldApiName__c,FieldLabel__c,Is_Component__c,Order__c,RecordType_Id__c,Is_Contract__c from CLM_AtaGlanceField__c where RecordType_Id__c=:recId AND Is_Contract__c = false ORDER BY Order__c ASC];
        else
       clmAtAGlanceList=[select Color__c,ColSpan__c,Document_Id__c,FieldApiName__c,FieldLabel__c,Is_Component__c,Order__c,RecordType_Id__c,Is_Contract__c from CLM_AtaGlanceField__c where Is_Contract__c = false ORDER BY Order__c ASC];

        system.debug('clmAtAGlanceList '+clmAtAGlanceList.size());
        //return clmAtAGlanceList; 
        List<AtAGlanceWrap> atAGlanceList = new List<AtAGlanceWrap>();
         System.debug('M '+ M);
         for(CLM_AtaGlanceField__c clmAt:clmAtAGlanceList)
         {
             
             if(! clmAt.Is_Component__c)
             {
                 Schema.SObjectField field = M.get(clmAt.FieldApiName__c.trim());
                 System.debug('field '+field);
                 String fldType = ''+field.getDescribe().getType();
                 AtAGlanceWrap atAgla = new AtAGlanceWrap(clmAt,fldType);
                 atAGlanceList.add(atAgla);
             }
             else{
                 AtAGlanceWrap atAgla = new AtAGlanceWrap(clmAt,'');
                 atAGlanceList.add(atAgla);
             }
         }
         return atAGlanceList;
        }
        else{
            return null;
        }
    }
 
    @AuraEnabled
    public static void delContractVehicle(String contractVehicleId){
        if(contractVehicleId != ''){
            TM_TOMA__SubContract__c contractVehicle= new TM_TOMA__SubContract__c();
             contractVehicle = [
            SELECT Id, Name 
            FROM TM_TOMA__SubContract__c
            WHERE Id =:contractVehicleId
        ];
        DMLManager.deleteAsUser(contractVehicle) ;
        }
      
    }
    
       public static String getAllFields(String sobjectname){
           System.debug('getAllFields');
        if(!Schema.getGlobalDescribe().containsKey(sobjectname)) return 'Exception';
        Map<String, Schema.SObjectField> fields = Schema.getGlobalDescribe().get(sobjectname).getDescribe().SObjectType.getDescribe().fields.getMap();
        List<String> accessiblefields = new List<String>();
        for(Schema.SObjectField field : fields.values()){
            if(field.getDescribe().isAccessible())
                accessiblefields.add(field.getDescribe().getName());
        }
        String allfields='';
        for(String fieldname : accessiblefields)
            allfields += fieldname+',';
        allfields = allfields.subString(0,allfields.length()-1);
           System.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'+allfields);
        return allfields;
    }
    
    public static String getAllATAGlanceFields(String sobjectname,string recId){//newlly added 24-05-2018
        System.debug('getAllATAGlanceFields');
        if(!Schema.getGlobalDescribe().containsKey(sobjectname)) return 'Exception';
        List<CLM_AtaGlanceField__c> clmAtAGlanceList= new List<CLM_AtaGlanceField__c>();
        string rectypeId;
        set<String> accessiblefields = new set<String>();
        Map<String,Schema.SObjectField> M = new Map<String,Schema.SObjectField>();
        M =  Schema.SObjectType.TM_TOMA__SubContract__c.fields.getMap();
        List<TM_TOMA__SubContract__c> contractVehicle = [SELECT Id,Name,recordTypeId FROM TM_TOMA__SubContract__c WHERE Id =:recId limit 1];
        if(contractVehicle[0].recordTypeId != Null)
        {
            rectypeId=contractVehicle[0].recordTypeId;
            rectypeId=rectypeId.substring(0,15);
        }
        if(rectypeId != '')
            clmAtAGlanceList=[select Color__c,ColSpan__c,Document_Id__c,FieldApiName__c,FieldLabel__c,Is_Component__c,Order__c,RecordType_Id__c,Is_Contract__c from CLM_AtaGlanceField__c where RecordType_Id__c=:rectypeId AND Is_Contract__c = false ORDER BY Order__c ASC];
        else
            clmAtAGlanceList=[select Color__c,ColSpan__c,Document_Id__c,FieldApiName__c,FieldLabel__c,Is_Component__c,Order__c,RecordType_Id__c,Is_Contract__c from CLM_AtaGlanceField__c where Is_Contract__c = false ORDER BY Order__c ASC];
        if(clmAtAGlanceList != null){
        for(CLM_AtaGlanceField__c clmAt:clmAtAGlanceList)
        {
            if(!clmAt.Is_Component__c)
            {
                Schema.SObjectField field = M.get(clmAt.FieldApiName__c.trim());
                if(field.getDescribe().isAccessible())
                    accessiblefields.add(field.getDescribe().getName());
            }
        }
    }
        String allfields='';
        if(accessiblefields != null){
        for(String fieldname : accessiblefields)
            allfields += fieldname+',';
        }
        allfields = allfields.subString(0,allfields.length()-1);
        return allfields;
    }
    
    public class AtAGlanceWrap{
        
        @AuraEnabled
        public CLM_AtaGlanceField__c atAGlance{get;set;} 
        @AuraEnabled
        public String fieldType{get;set;}
         @AuraEnabled
        public String lookupName{get;set;}
        
        public AtAGlanceWrap(CLM_AtaGlanceField__c atAGlance,String fieldType)
            {
                Map<String,Schema.SObjectField> schemaMap = Schema.SObjectType.TM_TOMA__SubContract__c.fields.getMap();
                this.atAGlance=atAGlance;
                this.fieldType=fieldType;
                
                 System.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'+fieldType);
                if(fieldType == 'REFERENCE'){
                     System.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'+fieldType);
                     Schema.SObjectField field = schemaMap.get(atAGlance.FieldApiName__c.trim());
                    System.debug(''+field.getDescribe().getReferenceTo().get(0));
                  this.lookupName = ''+field.getDescribe().getReferenceTo().get(0);
                }

            }
    }
}