public class ITES3SEmailParser {
    public String taskOrderNumber;
    private String taskOrderTitle;
    private String contactName;
    private String email;
    private String phone;
    private String agency;
    private String installation;
    private String activity;
    private String description;
    private DateTime proposalDate;
    public String modificationName;
    public String modificationText;
    private String itesformat;
    private boolean isCancel;
    private Integer maxSize = 500;
    
    
    public void parse(Messaging.InboundEmail inboundEmail){
        String emailBody = inboundEmail.plainTextBody;
        String subject = inboundEmail.subject;
        isCancel = false;
        
        if(subject.toLowerCase().contains('ites-3s')){
            if(subject.toLowerCase().contains('created')){
                parseNew(emailBody);
            }else if(subject.toLowerCase().contains('amendment')){
                parseAmendment(emailBody);
            }else if(subject.toLowerCase().contains('cancellation')){
                parseCancellation(emailBody);
            }  
        }        
    }
    
    private void parseNew(String emailBody){
        DateTimeHelper dtHelper = new DateTimeHelper();
        if(emailBody.contains('Request for Information ID:')){
            try{
                taskOrderNumber = emailBody.substringBetween('Request for Information ID:','Request for Information Name:').trim();
            }catch(Exception ex){
                System.debug('Error in TO Number :'+ex.getMessage()+' : '+ex.getLineNumber());
            }
            System.debug('Task Order Number :'+taskOrderNumber.trim());
        }  
        
        if(emailBody.contains('Request for Information Name:')){
            try{
                taskOrderTitle = emailBody.substringBetween('Request for Information Name:','Request for Information Due Date:').trim();
            }catch(Exception ex){
                System.debug('Error in TO Title :'+ex.getMessage()+' : '+ex.getLineNumber());
            }
            System.debug('Task Order Title :'+taskOrderTitle.trim());
        }
        
        if(emailBody.contains('Request for Information Due Date:')){
            try{                
                proposalDate = dtHelper.getDateTimeSpecialCase(emailBody.substringBetween('Request for Information Due Date:','Request for Information # of Attachments:').trim());
            }catch(Exception ex){
                System.debug('Error in Due Date :'+ex.getMessage());
            }
            System.debug('Proposal Date:'+proposalDate);
        }
        
        if(emailBody.contains('Your Point of Contact Is:')){
            try{
                contactName = emailBody.substringBetween('Your Point of Contact Is:','Email:').trim();
            }catch(Exception ex){
                System.debug('Error in Contact Name :'+ex.getMessage());
            }
            System.debug('ContactName :'+contactName);
        }
        
        if(emailBody.contains('Email:')){
            try{
                email = emailBody.substringBetween('Email:','Phone:').trim();
            }catch(Exception ex){
                System.debug('Error in Email :'+ex.getMessage());
            }
            System.debug('Email :'+email);
        }
        
        if(emailBody.contains('Phone:')){
            try{
                phone = emailBody.substringBetween('Phone:','Organization:').trim();
            }catch(Exception ex){
                System.debug('Error in Phone :'+ex.getMessage());
            }
            System.debug('Phone :'+phone);
        }
        
        if(emailBody.contains('Details:')){
            try{
                description = emailBody.substringBetween('Details:','Due Date:').trim();
            }catch(Exception ex){
                System.debug('Error in Details :'+ex.getMessage());
            }
            System.debug('Description :'+description);
        }
        
        if(emailBody.contains('Agency:')){
            try{
                agency = emailBody.substringBetween('Agency:','Activity:').trim();
            }catch(Exception ex){
                System.debug('Error in Agency :'+ex.getMessage());
            }
            System.debug('Agency :'+agency);
        }
        
        if(emailBody.contains('Activity:')){
            try{
                activity = emailBody.substringBetween('Activity:','Installation:').trim();
            }catch(Exception ex){
                System.debug('Error in Activity :'+ex.getMessage());
            }
            System.debug('Activity = '+activity); 
        }
        
        if(emailBody.contains('Installation:')){
            try{
                installation =emailBody.substringBetween('Installation:','Thank you for using the CHESS IT e-mart system!').trim();
            }catch(Exception ex){
                System.debug('Error in Installation :'+ex.getMessage());
            }   
            System.debug('Installation = '+installation);        
        }
        
        if(emailBody.contains('Format:')){
            try{
                itesformat = emailBody.substringBetween('Format:', 'Agency:').trim();
                itesformat = itesformat.normalizeSpace();
            }catch(Exception ex){
                System.debug('Error in Format :'+ex.getMessage()+' : '+ex.getLineNumber());
            }  
            
            if(itesformat != null){
                if(itesformat.length() > maxSize){
                    itesformat = itesformat.substring(0,maxSize).trim();
                    itesformat = itesformat.normalizeSpace();
                }
            }
        }
    }
    
    private void parseAmendment(String emailBody){
        if(emailBody.contains('RFI ID:')){
            try{
                taskOrderNumber = emailBody.substringBetween('RFI ID:', 'Proposal:').trim();
            }catch(Exception ex){
                System.debug('Error in TO Number :'+ex.getMessage()+' : '+ex.getLineNumber());
            }   
            if(taskOrderNumber ==null){
                throw new ContractVehicleNotFoundException(MessageConstantController.TASK_ORDER_NOT_FOUND );
            }
            System.debug('taskOrderNumber = '+taskOrderNumber);        
        }
        
        if(emailBody.contains('Proposal:')){
            try{
                description = emailBody.substringBetween('Proposal:', 'Due Date:').trim();
            }catch(Exception ex){
                System.debug('Error in Description :'+ex.getMessage()+' : '+ex.getLineNumber());
            }               
            System.debug('description = '+description);        
        }
        
        DateTimeHelper dtHelper = new DateTimeHelper();        
        if(emailBody.contains('Date Due:')){
            try{
                proposalDate = dtHelper.getDateTimeSpecialCase(emailBody.substringBetween('Date Due:','\nAttachments:').trim());
            }catch(Exception ex){
                System.debug('Error in Date Due :'+ex.getMessage()+' : '+ex.getLineNumber());
            }               
            System.debug('proposalDate = '+proposalDate);        
        }
        
        if(emailBody.contains('Format:')){
            try{
               itesformat = emailBody.substringBetween('Format:', 'Date Due:').trim();
            }catch(Exception ex){
                System.debug('Error in Format :'+ex.getMessage()+' : '+ex.getLineNumber());
            }  
            
            if(itesformat != null){
                if(itesformat.length() > maxSize){
                    itesformat = itesformat.substring(0,maxSize).trim();
                }
            }
        }
        
        modificationText = emailBody.replaceAll('Dear.*\\,.*', ''); // Remove Greeting
        modificationName = 'Amendement';
    }
    
    
    private void parseCancellation(String emailBody){
        if(emailBody.contains('Proposal ID:')){
            try{
                taskOrderNumber = emailBody.substringBetween('Proposal ID:', 'The reason given for cancellation was:').trim();
                taskOrderNumber = taskOrderNumber.remove(':');
            }catch(Exception ex){
                System.debug('Error in Proposal ID :'+ex.getMessage()+' : '+ex.getLineNumber());
            }   
            if(taskOrderNumber ==null){
                throw new ContractVehicleNotFoundException(MessageConstantController.TASK_ORDER_NOT_FOUND );
            }
            System.debug('taskOrderNumber = '+taskOrderNumber);        
        }
        
        if(emailBody.contains('The reason given for cancellation was:')){
            try{
                if(emailBody.contains('Thank you for using the CHESS IT e-mart system!')){
                    modificationText = 'The reason given for cancellation was: ' + emailBody.substringBetween('The reason given for cancellation was:', 'Thank you for using the CHESS IT e-mart system!').trim();
                }                
            }catch(Exception ex){
                System.debug('Error in cancellation was :'+ex.getMessage()+' : '+ex.getLineNumber());
            }               
            System.debug('modificationText = '+modificationText);        
        }
        
        modificationName = 'Cancellation';
        isCancel = true;
    }
    
     public Task_Order__c getTaskOrder(String toAddress){ 
        if(taskOrderNumber==null){
            return null;
        }        
        Task_Order__c taskOrder = new Task_Order__c();
        List<Contract_Vehicle__c> conVehicalList = [SELECT Id FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];  
        System.debug(''+conVehicalList);
        if(!conVehicalList.isEmpty()){
            taskOrder.Contract_Vehicle__c = conVehicalList[0].Id; // Label = Contract Vehicle
            taskOrder.Name = taskOrderNumber; //Task Order Number
            
            if(taskOrderTitle !=null)           
                taskOrder.Task_Order_Title__c = taskOrderTitle;// Task Order Title
            
            if(description !=null)  
                taskOrder.Description__c = description.normalizeSpace();// Program Summary 
            
            if(proposalDate !=null){            
                taskOrder.Due_Date__c = proposalDate.date(); // Due Date
                taskOrder.Proposal_Due_DateTime__c = proposalDate; // Proposal Due Date/Time
            }
            
            if(agency !=null)           
                taskOrder.Agency__c = agency; // Agency 
            
            if(activity !=null)           
                taskOrder.ITES_3H_Activity__c = activity; // activity 
            
            if(installation !=null)           
                taskOrder.Installation__c = installation; // Installation 
            
            if(itesformat != null)
                taskOrder.ITES_Format__c = itesformat;//ITES Format
                       
            
            taskOrder.Release_Date__c = Date.today();//RFP/TO Release Date
            taskOrder.Contract_Vehicle_picklist__c = 'ITES-3S: Information Technology Enterprise Solutions-3 Services';
            
            try{
                Contact contact = createContact(contactName,phone,email);
                if(contact != null){
                    if(contact.Id == null)
                        DMLManager.insertAsUser(contact);
                    
                    taskOrder.Contracting_Officer__c = contact.Id;
                }                                
            }catch(Exception e){
                System.debug('Error in contact - '+e.getMessage()+':'+e.getLineNumber());
            }
            
            if(isCancel)
                taskOrder.Is_cancelled__c = true;
            
            return taskOrder;
        }else{
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
        }
    }
    
    public Task_Order__c updateTaskOrder(Task_Order__c taskOrder, String toAddress){
        List<Contract_Vehicle__c> conVehicalList = [SELECT Id FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];  
        System.debug(''+conVehicalList);
        if(!conVehicalList.isEmpty()){
            taskOrder.Contract_Vehicle__c = conVehicalList[0].Id; // Label = Contract Vehicle
            taskOrder.Name = taskOrderNumber; //Task Order Number
            
            if(taskOrderTitle !=null)           
                taskOrder.Task_Order_Title__c = taskOrderTitle;// Task Order Title
            
            if(description !=null)  
                taskOrder.Description__c = description.normalizeSpace();// Program Summary 
            
            if(proposalDate !=null){            
                taskOrder.Due_Date__c = proposalDate.date(); // Due Date
                taskOrder.Proposal_Due_DateTime__c = proposalDate; // Proposal Due Date/Time
            }
            
            if(agency !=null)           
                taskOrder.Agency__c = agency; // Agency 
            
            if(activity !=null)           
                taskOrder.ITES_3H_Activity__c = activity; // activity 
            
            if(installation !=null)           
                taskOrder.Installation__c = installation; // Installation 
            
            if(itesformat != null)
                taskOrder.ITES_Format__c = itesformat;//ITES Format
                            
            try{
                Contact contact = createContact(contactName,phone,email);
                if(contact != null){
                    if(contact.Id == null){
                        if(taskOrder.Customer_Agency__c != null){
                            contact.AccountId = taskOrder.Customer_Agency__c;
                        }
                        DMLManager.insertAsUser(contact);
                    }
                    taskOrder.Contracting_Officer__c = contact.Id;  
                }
            }catch(Exception e){
                System.debug('Error in contact creation - '+e.getMessage()+':'+e.getLineNumber());
            }
            
            if(isCancel)
                taskOrder.Is_cancelled__c = true;
            
            return taskOrder;
        }else{
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
        }    
    }
    
    
    private Contact createContact(String name, String phone, String email){
        try{
            List<Contact> contactList = new List<Contact>();
            if(email != null && email != ''){
                // To remove the url part, everything including the "<" and ">"
                /* Ex. avery.e.williams.civ@mail.mil <aaron.m.west19.civ@mail.mil>, 
                remove "<aaron.m.west19.civ@mail.mil>" from the above Email Id*/
                email = email.replaceAll('\\<[^\\<\\>]*[^\\>\\<]*\\>', '').replaceAll('\\s+','');
                contactList = [SELECT Id,email From Contact WHERE email=:email];
            }
            if(contactList.isEmpty()){
                if(name != null){
                    Contact contact = new Contact();
                    String[] nameSplit = name.split(' ');
                    if(nameSplit.size()>2 && nameSplit.size()==3){
                        contact.FirstName = nameSplit[0];
                        contact.lastName = nameSplit[2];  
                    }else{
                        contact.FirstName = nameSplit[0];
                        contact.lastName = nameSplit[1];    
                    }
                    // To remove the url part, everything including the "<" and ">"
                    /* Ex. 7 <973-724-6034>03-340-1308, 
                    remove "<973-724-6034>" from the above Phone Number*/
                    if(phone != null)
                        contact.phone = phone.replaceAll('\\<[^\\<\\>]*[^\\>\\<]*\\>', '').replaceAll('\\s+','');
                    contact.Email = email;
                    contact.Title = 'Contracting Officer';
                    contact.Is_FedTom_Contact__c = true;
                    return contact; 
                }                              
            }
            else{
                return contactList[0];
            }
        }catch(Exception ex){
            System.debug('Error in createContact method - '+ex.getMessage()+':'+ex.getLineNumber());
        }
        return null;
    }
}