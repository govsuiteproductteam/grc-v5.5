public class JEClassEmailParser {
    private String emailSubject;
    public String taskOrderNumber;
    //Private String titledName;
    Private Date proposalDueDate;
    Private Datetime proposalDueDateTime;
    Private Date questionDueDate;
    //Old
    //private Id contactId1;
    //private Id contactId2;
    //New 12April2018
    private Contact contact1;
    private Contact contact2;
    
    public void parse(Messaging.InboundEmail email){
        emailSubject = email.subject;
        if(emailSubject.length() == 0){
            throw new ContractVehicleNotFoundException(MessageConstantController.TASK_ORDER_NOT_FOUND );
        }
        
        if(emailSubject.startsWith('Fwd:'))
            emailSubject = emailSubject.replace('Fwd:', '');
        if(emailSubject.startsWith('Re:'))
            emailSubject = emailSubject.replace('Re:', '');
        
        if(emailSubject.toLowerCase().contains('je-class task'))
            taskOrderNumber = emailSubject.toLowerCase().substringBetween('je-class task ', ' ').trim();
        else if(emailSubject.toLowerCase().contains('je class task'))
            taskOrderNumber = emailSubject.toLowerCase().substringBetween('je class task ', ' ').trim();
        System.debug(''+taskOrderNumber);
        
        String body = email.plainTextBody.toLowerCase();
        if(body.contains('the offeror shall provide the task order proposal (top) by')){
            String rawDate = body.substringBetween('the offeror shall provide the task order proposal (top) by', 'in accordance').normalizeSpace();
            System.debug(''+rawDate);
            DateTimeHelper dtHelper = new DateTimeHelper();
            proposalDueDateTime =  dtHelper.getDateTimeSpecialCase(rawDate);
            System.debug(''+proposalDueDateTime);
        }
        if(body.contains('all questions in response to the tor must be submitted in writing by')){            
            String rawDate = body.substringBetween('all questions in response to the tor must be submitted in writing by', '.').normalizeSpace();
            System.debug(''+rawDate);
            DateTimeHelper dtHelper = new DateTimeHelper();
            questionDueDate =  dtHelper.getDateTimeSpecialCase(rawDate).date();
            System.debug(''+questionDueDate);                                                                                                                            
        }
        
        
        String contactInfo = body.substringAfter('thank you,').normalizeSpace();
        System.debug(''+contactInfo);
        //titledName = emailSubject;
        createContact(contactInfo);
        
    }
    
    public Task_Order__c getTaskOrder(String toAddress){ 
        if(taskOrderNumber==null){
            return null;
        }        
        Task_Order__c taskOrder = new Task_Order__c();
        List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];        
        if(!conVehicalList.isEmpty()){
            taskOrder.Contract_Vehicle__c = conVehicalList[0].Id;
            taskOrder.Name = taskOrderNumber;//Task Order Number
            
            if(proposalDueDateTime !=null){            
                taskOrder.Due_Date__c = proposalDueDateTime.date();
                taskOrder.Proposal_Due_DateTime__c = proposalDueDateTime;
            } 
            
            if(questionDueDate!=null)
                taskOrder.Questions_Due_Date__c = questionDueDate;
            
            try{               
                if(contact1.Id == null && contact2.Id == null)
                    DMLManager.insertAsUser(contact1); 
                DMLManager.insertAsUser(contact2);                 
            }catch(Exception e){
                System.debug('Error in contact - '+e.getMessage()+':'+e.getLineNumber());
            }
            
            
            if(contact1 != null)            
                taskOrder.Contracting_Officer__c = contact1.Id;
            
            if(contact2 != null)            
                taskOrder.Contract_Specialist__c = contact2.Id;
            
            //Print
            System.debug('contact1.Id = '+contact1.Id);
            System.debug('contact2.Id = '+contact2.Id);
            //
            
            taskOrder.Release_Date__c = Date.today();//RFP/TO Release Date
            return taskOrder;
        }else{
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
        }
    }
    
    public Task_Order__c updateTaskOrder(Task_Order__c taskOrder, String toAddress){
        List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];
        
        if(!conVehicalList.isEmpty()){
            taskOrder.Contract_Vehicle__c = conVehicalList[0].Id;
            
            if(proposalDueDateTime !=null){            
                taskOrder.Due_Date__c = proposalDueDateTime.date();
                taskOrder.Proposal_Due_DateTime__c = proposalDueDateTime;
            } 
            
            //taskOrder.Release_Date__c = Date.today();//RFP/TO Release Date
            if(questionDueDate!=null)
                taskOrder.Questions_Due_Date__c = questionDueDate;
            
            
            try{
                if(taskOrder.Customer_Agency__c != null){
                    contact1.AccountId = taskOrder.Customer_Agency__c;
                    contact2.AccountId = taskOrder.Customer_Agency__c;
                    //DMLManager.upsertAsUser(con);
                    if(contact1.Id == null && contact2.Id == null){
                        // if Task Order with Customer Organization is not having a Contact or if a different Contact(i.e. a Contact with different Email Id) is found
                        //DMLManager.insertAsUser(con);
                        insert contact1;
                        insert contact2;
                    }else{
                        // if Task Order with Customer Organization is having a Contact, then update that Contact
                        //DMLManager.updateAsUser(con);
                        update contact1;
                        update contact2;
                    }
                }else{
                    if(contact1.Id == null && contact2.Id == null)
                        DMLManager.insertAsUser(contact1);  
                    DMLManager.insertAsUser(contact2);                    
                }                
                if(contact1 != null)            
                    taskOrder.Contracting_Officer__c = contact1.Id;
                
                if(contact2 != null)            
                    taskOrder.Contract_Specialist__c = contact2.Id; 
            }catch(Exception e){
                System.debug('Error in contact creation - '+e.getMessage()+':'+e.getLineNumber());
            }   
            
            
            
            
            return taskOrder;
        }else{
            system.debug('##################################');
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
            
        }
    }
    
    private void createContact(String contactInfo){
        // System.debug(''+);
        try{
            String[] contactInfoSplit = contactInfo.split('thank you,');
            
            if(contactInfoSplit.size()>0){
                Contact con1 = new Contact();
                String email1;
                String[] contact1InfoSplit = contactInfoSplit[0].split(' ');
                if(contact1InfoSplit.size()>0  && contact1InfoSplit.get(contact1InfoSplit.size()-1).contains('@') )
                    email1 = contact1InfoSplit.get(contact1InfoSplit.size()-1);
                
                List<Contact> conOfficerList1;
                if(email1!=null)
                    conOfficerList1 = [SELECT id , name FROM Contact Where email =:email1];
                if(conOfficerList1 != null && !conOfficerList1.isEmpty() ){
                    System.debug('if = '+conOfficerList1[0].id);
                    //Old
                    //contactId1 = conOfficerList1[0].id;               
                    //New 12April2018
                    contact1 = conOfficerList1[0]; 
                }else{
                    String lastName = contactInfoSplit[0].subStringBefore('contracting officer').trim();
                    System.debug('lastName = '+lastName);
                    if(lastName!=null){
                        con1.LastName = lastName;
                        con1.Email = email1;
                        String phone = contactInfoSplit[0].subStringBetween('(p)',' ').normalizeSpace().trim();
                        System.debug(phone.replaceAll('-',''));
                        if(phone != null)
                            con1.Phone = phone.replaceAll('-','');
                        String fax = contactInfoSplit[0].subStringBetween('(f)',' ').normalizeSpace().trim();
                        if(fax!=null)
                            con1.Fax = fax.replaceAll('-','');
                        con1.Title = 'Contracting Officer';
                        con1.Is_FedTom_Contact__c = true;
                        //Old
                        //DMLManager.insertAsUser(con1);                         
                        //contactId1 = con1.id;        
                        //New 12April2018
                        contact1 = con1;        
                    }
                }
            }
            
            if(contactInfoSplit.size()>1){
                List<Contact> conOfficerList2;
                String email2 = contactInfoSplit[1].subStringBetween('e-mail: ',' ').normalizeSpace();
                System.debug('email2 = '+email2);
                if(email2!=null)
                    conOfficerList2 = [SELECT id , name FROM Contact Where email =:email2];
                if(conOfficerList2 != null && !conOfficerList2.isEmpty() ){
                    System.debug('if = '+conOfficerList2[0].id);
                    //Old
                    //contactId2 = conOfficerList2[0].id;               
                    //New 12April2018
                    contact2 = conOfficerList2[0];               
                }else{
                    Contact con1 = new Contact();
                    String lastName = contactInfoSplit[1].subStringBefore('- contract specialist').trim();
                    System.debug('lastName = '+lastName);
                    if(lastName!=null){
                        con1.LastName = lastName;
                        String phone = contactInfoSplit[1].subStringBetween('phone: ',' ').normalizeSpace();
                        if(phone != null)
                            con1.Phone = phone.replaceAll('-','');
                        
                        if(email2!=null)
                            con1.email = email2.trim();
                        con1.Title = 'Contract Specialist';
                        con1.Is_FedTom_Contact__c = true;
                        //Old
                        //DMLManager.insertAsUser(con1);    
                        //contactId2 = con1.id;   
                        //New 12April2018
                        contact2 = con1;
                    }
                }
            }
        }catch(Exception e){
            System.debug(''+e.getMessage()+'\t'+e.getLineNumber());
        }
        
    }
}