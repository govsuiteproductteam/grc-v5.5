public with sharing class GSAModifier{
    
    public static String parseModification(String inputBody ,String modificationName ,String taskOrderId){
        if(Schema.sObjectType.Task_Order__c.isAccessible() && Schema.sObjectType.Contact.isAccessible()){ 
            if(inputBody != null){
                System.debug('Input Text = '+inputBody );
                String inputWithoutHtml = inputBody.stripHtmlTags().trim();
                String inputeData = inputWithoutHtml.substringAfter('RFQ ID:');
                System.debug('Input Data = '+inputeData );
                System.debug('modificationName  = '+modificationName);
                modificationName = modificationName.trim();
                //if(taskOrderId != null && taskOrderId != '' && modificationName != null && modificationName != '' && !inputeData.contains('Description of modification made at :')){
                if(taskOrderId != null && taskOrderId != '' && modificationName != null && modificationName != '' ){                                
                    List<Task_Order__c> taskOrderList = [Select Id,Name From Task_Order__c where id =: taskOrderId ];
                    if(!taskOrderList.isEmpty()){
                        if(inputeData.contains(taskOrderList[0].name)){
                            Contract_Mods__c contractMod = new Contract_Mods__c();
                            List<Contract_Mods__c> contractModeList;
                            if(Schema.sObjectType.Contract_Mods__c.isAccessible())
                                contractModeList = [Select Id,Modification_Name__c,Modification__c  From Contract_Mods__c where TaskOrder__c =: taskOrderId AND Modification_Name__c =: modificationName Limit 1];
                            
                            if(contractModeList != null && !contractModeList.isEmpty()){
                                contractMod = contractModeList[0];
                            }
                            contractMod.RecordTypeId = Schema.SObjectType.Contract_Mods__c.getRecordTypeInfosByName().get('Contract Mod').getRecordTypeId();
                            contractMod.Modification_Name__c =  modificationName;    
                            contractMod.Modification__c =  inputeData;
                            try{
                                if(modificationName == 'Q&A'){
                                    String rfqId = getRFQId(inputBody);
                                     contractMod.ebuy_Url__c = 'https://www.ebuy.gsa.gov/advantage/seller/view_qandas.do?rfqId='+rfqId+'&mapName=/seller/prepareQuote';
                                }else{
                                    String[] versionNum = modificationName.split(' ');
                                    contractMod.ebuy_Url__c = 'https://www.ebuy.gsa.gov/advantage/seller/RFQ_modification_description.do?&rfqId='+taskOrderList[0].name+'&versionNumber='+versionNum[1]+'&fromPage=/seller/prepareQuote.do';
                                }
                            }catch(Exception e){}
                            
                            if(contractMod.id == null){
                                contractMod.TaskOrder__c = taskOrderId; 
                                DMLManager.insertAsUser(contractMod);                                
                                /*if(modificationName == 'Q&A' || isValidMod(inputeData,taskOrderId,modificationName)){
									DMLManager.insertAsUser(contractMod);
								}*/
                            }
                            else{
                                if(!inputeData.contains('Description of modification made at :')){
                                    DMLManager.updateAsUser(contractMod);  
                                    //update contractMod;
                                }
                                
                            }
                            
                            
                            
                            String retrnURLString = '';
                            if(inputeData.contains('The following Q&A document(s) are attached to this RFQ:')){
                                String[] rows = inputBody.split('\n');
                                
                                for(String row : rows){
                                    if(row.contains('/advantage/ebuy/view_doc.do')){
                                        String documentName = row.stripHtmlTags().trim();
                                        String tempUrl = row.substringBetween('href=', '>').trim();                     
                                        tempUrl = tempUrl.replaceAll('\'', '');
                                        tempUrl = tempUrl.replaceAll('"', '');   
                                        
                                        String fileName = tempUrl.substringBetween('filename=','&path=');
                                        System.debug('File Name = '+fileName);
                                        Integer index3 = fileName.lastIndexof('.');
                                        String fileExtension = +'.'+fileName.substringAfterLast('.');
                                        
                                        retrnURLString += 'https://www.ebuy.gsa.gov'+tempUrl+'SPLITDATA'+documentName+fileExtension+'SPLITDATA'+taskOrderId+'SPLITURL';
                                    }
                                }
                                return retrnURLString;
                            }
                        }
                    }
                }
            }
        }
        return '';
        
    }
    
    private static string getRFQId(String inputData){
        String rfqId = inputData.substringBetween('rfqId=', '"').trim();
        return rfqId;
    }
    
    /*private static boolean isValidMod(String modDescription, Id taskOrderId, String modificationName){
//This method will check for the mod description and make sure that modification from ebuy server is created today
//Also this method will return false if the same contract mod(will prevent to create new mod) is already present on TaskOrder
List<Contract_Mods__c> cmList = [SELECT Id FROM Contract_Mods__c WHERE Id =: taskOrderId AND Modification_Name__c =:modificationName];
if(cmList.isEmpty()){
try{
modDescription = modDescription.substringBetween('made at',':');
DateTimeHelper dtHelper = new DateTimeHelper();  

if(dtHelper.getDateTimeSpecialCase(modDescription).date()==(Date.today())){
return true;           
}
}catch(Exception e){

}
}
return false;        
}*/
}