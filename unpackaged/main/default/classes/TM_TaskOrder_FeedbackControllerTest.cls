@isTest
public with sharing class  TM_TaskOrder_FeedbackControllerTest{
    public Static TestMethod void TestTM_TaskOrder_FeedbackController(){
         List<Profile> profileList = [SELECT Id FROM Profile where (Name = 'Custom Partner Community User' OR Name = 'Custom Partner Community Login User' OR Name = 'Custom Customer Community Plus Login User') Limit 1];
         if(profileList != null && profileList.size()>0){
        
            Account acc = TestDataGenerator.createAccount('TaskOrderCapture');
            insert acc;
            
            List<Contact> conList = new List<Contact>(); 
            Contact con = TestDataGenerator.createContact('Avanti',acc);
            conList.add(con);
            Contact con1 = TestDataGenerator.createContact('Avanti1',acc);
            conList.add(con1);
            Contact con2 = TestDataGenerator.createContact('Avanti2',acc);
            conList.add(con2);
            insert conlist;
            
            List<User> userList = new List<User>();
            User usr = TestDataGenerator.createUser(con,TRUE,'test1', 'atest1','test1@gmail.com','taskordercapture@demo.com','atest1',profileList[0],'America/Los_Angeles','en_US','UTF-8','en_US');
            userList.add(usr);
            User usr1 = TestDataGenerator.createUser(con1,TRUE,'test2', 'atest1','test1@gmail.com','taskordercapture1@demo.com','atest2',profileList[0],'America/Los_Angeles','en_US','UTF-8','en_US');
            userList.add(usr1);
            User usr2 = TestDataGenerator.createUser(con2,TRUE,'test2', 'atest1','test2@gmail.com','taskordercapture2@demo.com','atest3',profileList[0],'America/Los_Angeles','en_US','UTF-8','en_US');
            userList.add(usr2);
                
            
            Contract_Vehicle__c contractVehicle = TestDataGenerator.createContractVehicle('TMconVehicle',acc);
            insert contractVehicle;
            
            Task_Order__c taskOrder = TestDataGenerator.createTaskOrder('TMtaskOrder', acc, contractVehicle);
            insert taskOrder;
            
            Survey__c sur = TestDataGenerator.createSurvey();
            insert sur;
           
            Survey_Task__c surveyTask = TestDataGenerator.createSurveyTask(sur ,taskOrder);
            insert surveyTask ;
            
            List<Question__c> queList = new List<Question__c>();
            Question__c que = TestDataGenerator.createQuestion('What',sur);
            queList.add(que);
            Question__c que1 = TestDataGenerator.createQuestion('What1',sur);
            queList.add(que1);
            Question__c que2 = TestDataGenerator.createQuestion('What2',sur);
            queList.add(que2);
            insert queList;
            
            List <Feedback__c> feedbackList = new List<Feedback__c>();
            Feedback__c feedBack = TestDataGenerator.createFeedback(con, surveyTask , usr);
            feedBack.Task_Order__c = taskOrder.id;
            feedbackList.add(feedBack);
            Feedback__c feedBack1 = TestDataGenerator.createFeedback(con1, surveyTask , usr1);
            feedBack1.Task_Order__c = taskOrder.id;
            feedbackList.add(feedBack1);
            Feedback__c feedBack2 = TestDataGenerator.createFeedback(con2, surveyTask , usr2);
            feedBack2.Task_Order__c = taskOrder.id;
            feedbackList.add(feedBack2);
            insert feedbackList;
            
            List<Survey_Answer__c> surveyAnsList = new List<Survey_Answer__c>();
            Survey_Answer__c surveyAns = TestDataGenerator.createSurveyAnswer(feedBack, que , surveyTask  );
            surveyAns.Option_1__c = 'opt1';
            surveyAns.Option_2__c = 'opt2';
            surveyAns.Option_3__c = 'opt3';
            surveyAns.Option_4__c = 'opt4';
            surveyAns.Option_5__c = 'opt5';
            surveyAns.Option_6__c = 'opt6';
            surveyAns.Option_7__c = 'opt7';
            surveyAns.Option_8__c = 'opt8';
            surveyAns.Option_9__c = 'opt9';      
            surveyAns.Option_10__c = 'opt10';
            surveyAns.Answer_Descriptive__c = 'descriptiveAns';     
            surveyAnsList.add(surveyAns);
            
            Survey_Answer__c surveyAns1 = TestDataGenerator.createSurveyAnswer(feedBack1, que1 , surveyTask  );
            surveyAns.Option_1__c = 'opt1';
            surveyAns.Option_2__c = 'opt2';
            surveyAns.Option_3__c = 'opt3';
            surveyAns.Option_4__c = 'opt4';
            surveyAns.Option_5__c = 'opt5';
            surveyAns.Option_6__c = 'opt6';
            surveyAns.Option_7__c = 'opt7';
            surveyAns.Option_8__c = 'opt8';
            surveyAns.Option_9__c = 'opt9';      
            surveyAns.Option_10__c = 'opt10';     
            surveyAnsList.add(surveyAns1);
            
            Survey_Answer__c surveyAns2 = TestDataGenerator.createSurveyAnswer(feedBack2, que2 , surveyTask  );
            surveyAns.Option_1__c = 'opt1';
            surveyAns.Option_2__c = 'opt2';
            surveyAns.Option_3__c = 'opt3';
            surveyAns.Option_4__c = 'opt4';
            surveyAns.Option_5__c = 'opt5';
            surveyAns.Option_6__c = 'opt6';
            surveyAns.Option_7__c = 'opt7';
            surveyAns.Option_8__c = 'opt8';
            surveyAns.Option_9__c = 'opt9';      
            surveyAns.Option_10__c = 'opt10';     
            surveyAnsList.add(surveyAns2);
            insert surveyAnsList;
            
            Test.startTest();
            ApexPages.CurrentPage().getparameters().put('id', taskOrder.id);
            Apexpages.StandardController controller = new ApexPages.standardController(taskOrder);
            TM_TaskOrder_FeedbackController taskOrder_FeedbackController = new TM_TaskOrder_FeedbackController(controller);
            
            taskOrder_FeedbackController.init();
            
            TM_TaskOrder_FeedbackController.SurveyAnsWrapper surveyAnsW = new  TM_TaskOrder_FeedbackController.SurveyAnsWrapper(surveyAns);
           
            surveyAnsW.descriptiveAns = 'descriptiveAns';
            
            system.assertNotEquals(taskOrder_FeedbackController.surveyQuestionMap.size(), 0);
            Test.stopTest();
            
       }
    }
}