global class TM_ContractVehicleAutoProcessClauses {
    @InvocableMethod(label='Create Contract Vehicle Clauses' description='Copies clauses from a master contract vehicle to the associate modification contract vehicle.') 
    global static void copyClausesToContractVehicle(List<Id> contractVehicleIdList){
        //System.debug('contractVehicleIdList '+contractVehicleIdList);
        if(contractVehicleIdList.size() > 0){
            Map<Id,Set<Id>> masterConIdCubIdSetMap = new Map<Id,Set<Id>>();
            map<Id,Id> contractIdvsMasterContrctIdMap = new map<Id,Id>();
            List<Id> masterContractIdList= new List<id>();
            List<Contract_Vehicle__c> tempContractList = [select id,Master_Contract_Vehicle__c,Master_Contract_Vehicle__r.Are_there_any_Center_Clauses__c,Master_Contract_Vehicle__r.Are_there_any_Contract_specific_clauses__c,Master_Contract_Vehicle__r.Are_there_any_Department_clauses__c,Master_Contract_Vehicle__r.Are_there_any_FAR_clauses__c,Master_Contract_Vehicle__r.Are_there_any_agency_specific_clauses__c,Master_Contract_Vehicle__r.Synopsis_of_specific_H_clauses__c,Master_Contract_Vehicle__r.Contract_specific_H_clauses__c from Contract_Vehicle__c where id in:contractVehicleIdList];
            List<Contract_Vehicle__c> ContractList= new List<Contract_Vehicle__c>(); 
            //System.debug('tempContractList '+tempContractList.size());
            for(Contract_Vehicle__c con : tempContractList)
            {
                //System.debug('con.Master_Contract_Vehicle__c '+con.Master_Contract_Vehicle__c);
                if(con.Master_Contract_Vehicle__c != null )
                {
                    masterContractIdList.add(con.Master_Contract_Vehicle__c);
                    contractIdvsMasterContrctIdMap.put(con.Id,con.Master_Contract_Vehicle__c);
                    Set<Id> tempIdSet = masterConIdCubIdSetMap.get(con.Master_Contract_Vehicle__c);
                    if(tempIdSet != Null){
                        tempIdSet.add(con.Id);
                    }else{
                        masterConIdCubIdSetMap.put(con.Master_Contract_Vehicle__c, new Set<Id>{con.Id});
                    }
                    con.Are_there_any_Center_Clauses__c = con.Master_Contract_Vehicle__r.Are_there_any_Center_Clauses__c;
                    con.Are_there_any_Contract_specific_clauses__c = con.Master_Contract_Vehicle__r.Are_there_any_Contract_specific_clauses__c;
                    con.Are_there_any_Department_Clauses__c = con.Master_Contract_Vehicle__r.Are_there_any_Department_clauses__c;
                    con.Are_there_any_FAR_clauses__c = con.Master_Contract_Vehicle__r.Are_there_any_FAR_clauses__c;
                    
                    con.Are_there_any_agency_specific_clauses__c = con.Master_Contract_Vehicle__r.Are_there_any_agency_specific_clauses__c;
                    con.Synopsis_of_specific_H_clauses__c = con.Master_Contract_Vehicle__r.Synopsis_of_specific_H_clauses__c;
                    con.Contract_specific_H_clauses__c = con.Master_Contract_Vehicle__r.Contract_specific_H_clauses__c;
                    ContractList.add(con);
                }
            }
            //System.debug('ContractList '+ContractList);
            try{
                if(ContractList.size()>0)
                {
                    update ContractList;
                }
            }
            catch(Exception e){
                System.debug('Error:- '+e);
            }
            
            List<Contract_Clauses__c> tempMasterContractClausesList = [SELECT Id,Contract_Vehicle__c,Clauses__c FROM Contract_Clauses__c where Contract_Vehicle__c in:masterContractIdList];
            List<Contract_Clauses__c> ConClauList=new List<Contract_Clauses__c>();
            //System.debug('tempMasterContractClausesList '+tempMasterContractClausesList);
            for(Contract_Clauses__c conClos : tempMasterContractClausesList){
                Set<Id> ContractIdSet = masterConIdCubIdSetMap.get(conClos.Contract_Vehicle__c);
                if(ContractIdSet != Null){
                    for(Id ConId : ContractIdSet){
                        Contract_Clauses__c tempSubConCla = new Contract_Clauses__c();
                        tempSubConCla.Clauses__c=conClos.Clauses__c ;
                        tempSubConCla.Contract_Vehicle__c=ConId;
                        ConClauList.add(tempSubConCla);
                    }
                }
            }
            //System.debug('ConClauList '+ConClauList);
            try{
                if(ConClauList.size()>0){
                    insert ConClauList;
                } 
            }
            catch(Exception e)
            {
                System.debug('Error:- '+e);
            }
        }
    }
}