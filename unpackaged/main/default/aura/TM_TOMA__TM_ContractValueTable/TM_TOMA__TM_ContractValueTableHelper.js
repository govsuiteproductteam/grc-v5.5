({
    conValTableList : function(component, event, helper) {
        console.log('conValTableList');
        var action = component.get("c.getValueTableContValList");
        action.setParams({
            
            "contractVehicalId": component.get("v.recordId")
        });
        
        action.setCallback(this, function(a) {
            var state= a.getState();
            if(state=="SUCCESS"){
                console.log('conValTableList SUCCESS');
                var conValTableList=a.getReturnValue();
                // alert("******conValTableList.length*****"+conValTableList.length);
                if(conValTableList == null || conValTableList == undefined){ 
                    component.set("v.valueTableContValSizeFlag",0);
                }
                else{
                    component.set("v.valueTableContValSizeFlag",conValTableList.length);
                    //17-08-2018
                    var valueTableList = a.getReturnValue();
                    if(valueTableList != null){
                        console.log('@@@@@@@@@' + valueTableList);
                        
                        for(var i = 0; i < valueTableList.length; i++){
                            console.log('Start_Date__c@@@@@@@@@' + valueTableList[i].TM_TOMA__Start_Date__c);
                            console.log('End_Date__c@@@@@@@@@' + valueTableList[i].TM_TOMA__End_Date__c);
                            if(valueTableList[i].TM_TOMA__Start_Date__c == undefined){
                                //valueTableList[i].Start_Date__c = 
                                valueTableList[i].TM_TOMA__Start_Date__c='';
                            }
                            if(valueTableList[i].TM_TOMA__End_Date__c == undefined){
                                valueTableList[i].TM_TOMA__End_Date__c='';
                            }
                            
                        }
                    }
                    component.set("v.valueTableContVal", valueTableList);//17-08-2018
                    
                    //component.set("v.valueTableContVal", a.getReturnValue());
                    

                } 
            }
            else if (state == "ERROR") 
            {
                console.log(a.getError());            
            }
        })
        $A.enqueueAction(action); 
    },
    
    getContractVehicle1 : function(component, event, helper) {
        var action = component.get("c.getContractVehical");
        action.setParams({
            recordId: component.get("v.recordId")
        });
        
        action.setCallback(this, function(a) {
            var state= a.getState();
            if(state=="SUCCESS"){
                component.set("v.contract", a.getReturnValue());
            }
            else if (state == "ERROR") 
            {
                console.log(a.getError());            
            }
        })
        $A.enqueueAction(action); 
    },
    
    saveContValTableList : function (component, event, helper){
        document.getElementById("saveButton1").disabled = true;
        document.getElementById("saveButton11").disabled = true;
        //  var btn = event.getSource();
        // btn.set("v.disabled",true);
        var invalidStartDateRow='';
        var invalidEndDateRow='';
        var delConValIds=component.get("v.delConValId");
        var valTableCotValList=component.get("v.valueTableContVal");
        var valueTableJSON = '[';
        var checkValidationError = 'Yes';
        var checkStartDateError = 'Yes';
        var checkEndDateError = 'Yes';
        var Contract = component.get("v.contract");
        
        if(valTableCotValList != undefined){
            for (var i=0; i<valTableCotValList.length; i++){        
                if(valueTableJSON.length>1){
                    valueTableJSON +=  ',{"attributes":{"type":"TM_TOMA__Value_Table__c"},';
                    var vTId = valTableCotValList[i].Id;
                    var description = valTableCotValList[i].TM_TOMA__Description__c=== undefined?'':valTableCotValList[i].TM_TOMA__Description__c.replace(/(\r\n|\n|\r)/gm," ");
                    var startDate = valTableCotValList[i].TM_TOMA__Start_Date__c;
                    var endDate = valTableCotValList[i].TM_TOMA__End_Date__c;
                    var dollars = valTableCotValList[i].TM_TOMA__Dollars__c === undefined?null:valTableCotValList[i].TM_TOMA__Dollars__c;
                    var Exercised_Contract_Value_Options__c = valTableCotValList[i].TM_TOMA__Exercised_Contract_Value_Options__c === undefined?null:valTableCotValList[i].TM_TOMA__Exercised_Contract_Value_Options__c;
                    var RecordTypeId  = valTableCotValList[i].RecordTypeId ;
                    var Contract_Vehicle__c  = valTableCotValList[i].TM_TOMA__Contract_Vehicle__c ;
                    var appendData = '';
                    
                    //Code to check a date is invalid or not          
                    var testmsg =helper.validatedate(component,event,helper,startDate);
                    var testmsg1 =helper.validatedate(component,event,helper,endDate);
                    if(testmsg===false)
                    {
                        checkStartDateError='InvalidStartDate';
                        invalidStartDateRow=invalidStartDateRow+''+i;
                        invalidStartDateRow=invalidStartDateRow+',';
                    }
                    if(testmsg1===false)
                    {
                        checkEndDateError='InvalidEndDate';
                        invalidEndDateRow=invalidEndDateRow+''+i;
                        invalidEndDateRow=invalidEndDateRow+',';
                    }
                    if(startDate==='')
                    {
                        checkValidationError='startDate';
                    }
                    if(endDate==='')
                    {
                        checkValidationError='endDate';
                    }
                    if(!(vTId===undefined))
                    {
                        appendData +='"Id":"'+ vTId+'" ,';
                    }
                    appendData += '"TM_TOMA__Description__c":"'+ description+'" , "TM_TOMA__Start_Date__c":"'+startDate+'", "TM_TOMA__End_Date__c":"'+endDate+'","TM_TOMA__Dollars__c":'+dollars+',"TM_TOMA__Exercised_Contract_Value_Options__c":'+Exercised_Contract_Value_Options__c+',"RecordTypeId":"'+RecordTypeId+'" }';
                    valueTableJSON += appendData;
                }else{
                    valueTableJSON +=  '{"attributes":{"type":"TM_TOMA__Value_Table__c"},';
                    var vTId = valTableCotValList[i].Id;
                    var description = valTableCotValList[i].TM_TOMA__Description__c=== undefined?'':valTableCotValList[i].TM_TOMA__Description__c.replace(/(\r\n|\n|\r)/gm," ");
                    var startDate = valTableCotValList[i].TM_TOMA__Start_Date__c;
                    var endDate = valTableCotValList[i].TM_TOMA__End_Date__c;
                    var dollars = valTableCotValList[i].TM_TOMA__Dollars__c;
                    var RecordTypeId  = valTableCotValList[i].RecordTypeId ;
                    var dollars = valTableCotValList[i].TM_TOMA__Dollars__c === undefined?null:valTableCotValList[i].TM_TOMA__Dollars__c;
                    var Exercised_Contract_Value_Options__c = valTableCotValList[i].TM_TOMA__Exercised_Contract_Value_Options__c === undefined?null:valTableCotValList[i].TM_TOMA__Exercised_Contract_Value_Options__c;
                    var appendData = '';
                    //Code to check a date is invalid or not          
                    var testmsg =helper.validatedate(component,event,helper,startDate);
                    var testmsg1 =helper.validatedate(component,event,helper,endDate);
                    if(testmsg===false)
                    {
                        checkStartDateError='InvalidStartDate';
                        invalidStartDateRow=invalidStartDateRow+''+i;
                        invalidStartDateRow=invalidStartDateRow+',';
                    }
                    if(testmsg1===false)
                    {
                        checkEndDateError='InvalidEndDate';
                        invalidEndDateRow=invalidEndDateRow+''+i;
                        invalidEndDateRow=invalidEndDateRow+',';
                    }
                    if(startDate==='')
                    {
                        checkValidationError='startDate';
                    }
                    if(endDate==='')
                    {
                        checkValidationError='endDate';
                    }
                    if(!(vTId===undefined))
                    {
                        appendData +='"Id":"'+ vTId+'" ,';
                    }
                    appendData += '"TM_TOMA__Description__c":"'+ description+'" , "TM_TOMA__Start_Date__c":"'+startDate+'", "TM_TOMA__End_Date__c":"'+endDate+'","TM_TOMA__Dollars__c":'+dollars+',"TM_TOMA__Exercised_Contract_Value_Options__c":'+Exercised_Contract_Value_Options__c+',"RecordTypeId":"'+RecordTypeId+'" }';
                    valueTableJSON += appendData;
                }
            } 
            
            valueTableJSON += ']';   
            console.log('@@@@@@@@@@valueTableJSON@@@@@@@@@@@@@@ '+valueTableJSON);
            if(checkValidationError==='Yes' && checkStartDateError==='Yes' && checkEndDateError==='Yes'){
                var conontractvalueupdateparmition = component.get("v.contractValueTableCtrl");
                    if((Contract.TM_TOMA__Initial_Contract_Funding_at_Award__c != undefined  && Contract.TM_TOMA__Contract_Status_Backend__c == 'Final'))
                    {  
                        
                        var action = component.get("c.saveConValTable");
                        action.setParams({ 
                            "contValList": valueTableJSON,
                            "recordId": component.get("v.recordId"),
                            "delConValIdsStr": delConValIds,
                            "contractRec" : Contract
                        });
                        action.setCallback(this, function(response){
                            var state = response.getState();
                            var messageText = response.getReturnValue();
                            if (state === "SUCCESS")
                            {
                                component.set("v.delConValId",'');
                                helper.getContractVehicle1(component, event, helper);
                                helper.conValTableList(component, event, helper);
                                // "If" condition and "Else" block are newly added to restrict user access according to the object permission(17/08/2018)
                                if(messageText == '' || messageText == undefined || messageText == null){
                                    alert("Changes were saved successfully.");
                                }else{
                                    alert(messageText);
                                }
                            }
                            else if (state === "ERROR") {
                                console.log(response.getError());            }
                            //  btn.set("v.disabled",false);
                            document.getElementById("saveButton1").disabled = false;
                            document.getElementById("saveButton11").disabled = false;
                        });
                        $A.enqueueAction(action);
                    }
                    else  if((Contract.TM_TOMA__Contract_Status_Backend__c == 'Draft' || Contract.TM_TOMA__Contract_Status_Backend__c == null || Contract.TM_TOMA__Contract_Status_Backend__c == '' || Contract.TM_TOMA__Contract_Status_Backend__c == undefined)){
                        var action = component.get("c.saveConValTable");
                        action.setParams({ 
                            "contValList": valueTableJSON,
                            "recordId": component.get("v.recordId"),
                            "delConValIdsStr": delConValIds,
                            "contractRec" : Contract
                        });
                        action.setCallback(this, function(response){
                            var state = response.getState();
                            var messageText = response.getReturnValue();
                            //alert(messageText);
                            if (state === "SUCCESS")
                            {
                                component.set("v.delConValId",'');
                                helper.getContractVehicle1(component, event, helper);
                                helper.conValTableList(component, event, helper);
                                // "If" condition and "Else" block are newly added to restrict user access according to the object permission(16/08/2018)
                                if(messageText == '' || messageText == undefined || messageText == null){
                                    alert("Changes were saved successfully.");
                                }else{
                                    alert(messageText);
                                }
                            }
                            else if (state === "ERROR") {
                                console.log(response.getError());            }
                            //  btn.set("v.disabled",false);
                            document.getElementById("saveButton1").disabled = false;
                            document.getElementById("saveButton11").disabled = false;
                        });
                        $A.enqueueAction(action);
                    }
                        else if(conontractvalueupdateparmition.isUpdateContractVehical === false){
                              var action = component.get("c.saveConValTable");
                        action.setParams({ 
                            "contValList": valueTableJSON,
                            "recordId": component.get("v.recordId"),
                            "delConValIdsStr": delConValIds,
                            "contractRec" : Contract
                        });
                        action.setCallback(this, function(response){
                            var state = response.getState();
                            var messageText = response.getReturnValue();
                            //alert(messageText);
                            if (state === "SUCCESS")
                            {
                                component.set("v.delConValId",'');
                                helper.getContractVehicle1(component, event, helper);
                                helper.conValTableList(component, event, helper);
                                // "If" condition and "Else" block are newly added to restrict user access according to the object permission(16/08/2018)
                                if(messageText == '' || messageText == undefined || messageText == null){
                                    alert("Changes were saved successfully.");
                                }else{
                                    alert(messageText);
                                }
                            }
                            else if (state === "ERROR") {
                                console.log(response.getError());            }
                            //  btn.set("v.disabled",false);
                            document.getElementById("saveButton1").disabled = false;
                            document.getElementById("saveButton11").disabled = false;
                        });
                        $A.enqueueAction(action);
                        }
                        else{
                            document.getElementById("saveButton1").disabled = false;
                            document.getElementById("saveButton11").disabled = false;
                            alert("Please enter an Initial Contract Funding at Award value.");
                        }
            }
            else if(checkStartDateError==='InvalidStartDate' && checkEndDateError==='Yes' && checkValidationError==='Yes')
            {
                var checkId = invalidStartDateRow.split(',');
                for(var i = 0; i < checkId.length-1; i++) {
                    var d = document.getElementById(checkId[i]+'conStartDate');
                    d.className += " validationClass";
                }
                alert('ERROR:- Invalid Start Date, in row:- '+invalidStartDateRow);
                /*  var act3 = component.get("c.isLightningPage");
            act3.setCallback(this, function(a) {
                if (a.getState() === "SUCCESS") {
                    if(a.getReturnValue() == false){
                        alert("ERROR:- Invalid Pop Start Date.");
                    }
                    else{ 
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!",
                            "message": "Invalid Pop Start Date.",
                            "type":"error"
                        });
                        toastEvent.fire();
                    }
                }
            }); 
            $A.enqueueAction(act3);*/
                document.getElementById("saveButton1").disabled = false; 
                 document.getElementById("saveButton11").disabled = false; 
            }
                else if(checkEndDateError==='InvalidEndDate' && checkStartDateError==='Yes' && checkValidationError==='Yes')
                {
                    var checkId = invalidEndDateRow.split(',');
                    for(var i = 0; i < checkId.length-1; i++) {
                        var d = document.getElementById(checkId[i]+'conEndDate');
                        d.className += " validationClass";
                    }
                    alert('ERROR:- Invalid End Date, in row:- '+invalidEndDateRow);
                    /* var act3 = component.get("c.isLightningPage");
                act3.setCallback(this, function(a) {
                    if (a.getState() === "SUCCESS") {
                        if(a.getReturnValue() == false){
                            alert("ERROR:- Invalid Pop End Date.");
                        }
                        else{ 
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                "title": "Error!",
                                "message": "Invalid Pop End Date.",
                                "type":"error"
                            });
                            toastEvent.fire();
                        }
                    }
                }); 
                $A.enqueueAction(act3);*/
                
                document.getElementById("saveButton1").disabled = false; 
                    document.getElementById("saveButton11").disabled = false
            }
                else if(checkEndDateError==='InvalidEndDate' && checkStartDateError==='InvalidStartDate' && checkValidationError==='Yes')
                {
                    
                    var checkId = invalidStartDateRow.split(',');
                    for(var index = 0; index < checkId.length-1; index++) {
                        var d = document.getElementById(checkId[index]+'conStartDate');
                        d.className += " validationClass";
                    }
                    var checkId1 = invalidEndDateRow.split(',');
                    for(var jindex = 0; jindex < checkId1.length-1; jindex++) {
                        var d = document.getElementById(checkId1[jindex]+'conEndDate');
                        d.className += " validationClass";
                    }
                    alert('ERROR:- Invalid Start Date, in row:- '+invalidStartDateRow+'\nERROR:- Invalid End Date, in row:- '+invalidEndDateRow);
                    document.getElementById("saveButton1").disabled = false; 
                    document.getElementById("saveButton11").disabled = false
                }
                    else{
                        alert("ERROR:- Please enter a Start Date or End Date.");
                        /*var act3 = component.get("c.isLightningPage");
                    act3.setCallback(this, function(a) {
                        if (a.getState() === "SUCCESS") {
                            if(a.getReturnValue() == false){
                                alert("ERROR:- Enter date for POP Start Date or POP End Date.");
                            }
                            else{ 
                                var toastEvent = $A.get("e.force:showToast");
                                toastEvent.setParams({
                                    "title": "Error!",
                                    "message": "Enter date for POP Start Date or POP End Date.",
                                    "type":"error"
                                });
                                toastEvent.fire();
                            }
                        }
                    }); 
                    $A.enqueueAction(act3);*/
                        // alert("Enter date for POP Start Date or POP End Date.");
                        
                        document.getElementById("saveButton1").disabled = false;
                        document.getElementById("saveButton11").disabled = false
                        // btn.set("v.disabled",false);
                    }
        }
        else{
            document.getElementById("saveButton1").disabled = false;
            document.getElementById("saveButton11").disabled = false
        }
    },
    validatedate:function(component,event,helper,inputText)
    {
        var dateformat = /(\d{4})-(\d{2})-(\d{2})/;
        if(inputText.match(dateformat))
        {
            var opera1 = inputText.split('/');
            var opera2 = inputText.split('-');
            var lopera1 = opera1.length;
            var lopera2 = opera2.length;
            if (lopera1>1)
            {
                var pdate = inputText.split('/');
            }
            else if (lopera2>1)
            {
                var pdate = inputText.split('-');
            }
            var mm  = parseInt(pdate[1]);
            var dd = parseInt(pdate[2]);
            var yy = parseInt(pdate[0]);
            // Create list of days of a month [assume there is no leap year by default]
            var ListofDays = [31,28,31,30,31,30,31,31,30,31,30,31];
            if (mm==1 || mm>2)
            {
                if (dd>ListofDays[mm-1])
                {
                    return false;
                }
            }
            if (mm==2)
            {
                var lyear = false;
                if ( (!(yy % 4) && yy % 100) || !(yy % 400)) 
                {
                    lyear = true;
                }
                if ((lyear==false) && (dd>=29))
                {
                    return false;
                }
                if ((lyear==true) && (dd>29))
                {
                    return false;
                }
            }
        }
        else
        {
            return false;
        }
    },
    checkValueTablePermissions : function(component, event, helper) {
        //Check "Value Table" object Permissions
        var action = component.get('c.getValueTablePermissions');
        action.setCallback(this,function(response){
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.contractValueTableCtrl', response.getReturnValue());
                var c=response.getReturnValue();
                if(c.isUpdateValueTable===false && c.isCreateValueTable===false && c.isDeleteValueTable===false && c.isUpdateContractVehical===false){
                    component.set("v.savePermission",false);
                }
            }
        });
        $A.enqueueAction(action);

    }
})