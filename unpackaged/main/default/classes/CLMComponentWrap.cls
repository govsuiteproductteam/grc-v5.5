public class CLMComponentWrap {
        @AuraEnabled
        public String componentOrPage{get;set;}
        @AuraEnabled
        public Double height{get;set;}
        @AuraEnabled
        public Double width{get;set;}        
        
        public CLMComponentWrap(CLM_Section__c section){
            this.componentOrPage = section.Inline_Api_Name_Component_Api__c;
            this.height = section.Height__c == null ? 50:section.Height__c;
             this.width = section.Width__c == null ? 100:section.Width__c;
        }
         public CLMComponentWrap(){
             
         }
    }