@isTest
public class EGOSRecordConnectPostTest {
    
    public static testMethod void testEGOSRecordCase1(){
        Account acc = new Account(Name='Acc-123');
        insert acc;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('CV-123', acc);
        conVehi.SINs__c = 'CIO-CS';
        insert conVehi;
        
        TM_TOMA__Task_Order__c taskOrder=new TM_TOMA__Task_Order__c();
        taskOrder.Name='SN-39841';
        taskOrder.TM_TOMA__Status__c = 'Cancelled';
        taskOrder.TM_TOMA__Contract_Vehicle__c=conVehi.id;
        insert  taskOrder;
               
            
        StaticResource sr1 = [SELECT Id, Body FROM StaticResource WHERE Name = 'EGOSRecord' LIMIT 1];
        String body1 = sr1.Body.toString();
        
        StaticResource sr2 = [SELECT Id, Body FROM StaticResource WHERE Name = 'EGOSRecordUpdated' LIMIT 1];
        String body2 = sr2.Body.toString();
        
        Test.startTest();        
        RestRequest req1 = new RestRequest(); 
        RestResponse res1 = new RestResponse();        
        req1.requestBody = Blob.valueof(body1);
        req1.httpMethod = 'POST';
        req1.addHeader('taskOrderNumber', 'SN-39841');
        req1.addHeader('timezone', '-330');
        req1.addHeader('responseStatus','No Bid');
        req1.addHeader('releaseDate', '04/18/2018 07:45 PM');
        req1.addHeader('status','Cancelled');
        RestContext.request = req1;
        RestContext.response = res1;
        String urls1 = EGOSRecordConnectPost.doPost();
        
        
        RestRequest req2 = new RestRequest(); 
        RestResponse res2 = new RestResponse();        
        req2.requestBody = Blob.valueof(body2);
        req2.httpMethod = 'POST';
        req2.addHeader('taskOrderNumber', 'SN-39841');
        req2.addHeader('timezone', '-330');
        req2.addHeader('releaseDate', '04/18/2018 07:45 PM');
        RestContext.request = req2;
        RestContext.response = res2;
        String urls2 = EGOSRecordConnectPost.doPost();
        Test.stopTest();
        System.assert(urls2.length() > 0); 
    }
    
    public static testMethod void testEGOSRecordCase2(){
        String body = '';
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();      
        req.requestBody = Blob.valueof(body);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        String urls = EGOSRecordConnectPost.doPost();
        System.assert(urls == '' || urls == 'SPLITURL');
    }
    
}