({
    componentTypeConfigObj: {
        "DATETIME" : "ui:inputDateTime",
        "REFERENCE" : "c:LightningLookupComp",//c:LightningLookupComp//c:lightningLookup
        "STRING" : "ui:inputText",
        "PICKLIST":"c:TM_DynamicFieldsCreatePicklist",
        "MULTIPICKLIST":"ui:inputTextArea",//"TM_TOMA:TM_DynamicFieldsCreateMultiPicklistCMP",
        "BOOLEAN" :"ui:inputCheckbox",
        "ID" : "ui:inputText",
        "DATE" : "ui:outputDate",
        "CURRENCY" : "ui:inputCurrency",
        "TEXTAREA" : "ui:inputTextArea",
        "PERCENT" : "ui:inputNumber",
        "DOUBLE" : "ui:inputNumber",
        "EMAIL" : "ui:inputEmail"
    },
    
    viewPageFieldsMap: {
        "DATETIME" : "ui:inputDateTime",
        "REFERENCE" : "c:TM_GetLookupUserNameCMP",
        "STRING" : "ui:outputRichText",
        "PICKLIST":"ui:outputText",
        "MULTIPICKLIST":"ui:outputRichText",//"TM_TOMA:TM_DynamicFieldsViewMultiPicklist",
        "BOOLEAN" :"ui:outputCheckbox",
        "ID" : "ui:outputText",
        "DATE" : "ui:outputDate",
        "CURRENCY" : "ui:outputCurrency",
        "TEXTAREA" : "ui:outputRichText",
        "PERCENT" : "ui:outputText",
        "DOUBLE" : "ui:outputNumber",
        "EMAIL" : "ui:outputEmail"
    }
})