public with sharing class ConsolidatedScheduleEmailTemplateParser {
    private String emailSubject='';    
    private List<String> parsedData1=new List<String>();
    public void parse(Messaging.InboundEmail email){
        emailSubject = email.subject;
        emailSubject = emailSubject.stripHtmlTags();
        System.debug('emailSubject = '+emailSubject);        
        System.debug('inside parse method');
        String emailBody = email.htmlBody;
        //System.debug('*************************emailBody = '+emailBody);
        if(emailBody.length()>0 && emailBody.contains('</table>')){
            String [] emailBodySplit = emailBody.split('</table>');
            //     String data = emailBody.substringBetween('<table border="1" cellspacing="3" cellpadding="0">', '</table>');
            //    System.debug('data = '+ data);
            if(emailBodySplit.size()>0){
                String email1 =  emailBodySplit[0];
                if(email1.contains('</tr>') && email1.contains('</tbody>')){
                    String data1 = email1.substringBetween('</tr>', '</tbody>');
                    if(data1.contains('</td>')){
                        parsedData1 =  data1.split('</td>');
                    }
                }
                
            }
            if(emailBodySplit.size()>1){
                String email2 =  emailBodySplit[1];
            }       
            
        }
        
    }
    
    public String getRFQId(){
        try{
            String rFQId = parsedData1[0];            
            String cleanRFQId = getTaskOrderNumber(rFQId.stripHtmlTags());
            if(cleanRFQId!= null){
                System.debug('cleanRFQId ='+cleanRFQId);
                return checkValidString(cleanRFQId,80);
            }
        }catch(Exception e){
            return null;
        }
        return null;
    }
    
    public String getAction(){
        try{
            String action = parsedData1[1];
            String cleanAction = action.stripHtmlTags();
            return checkValidString(cleanAction,255);
        }catch(Exception e){
            return null;
        }
    }
    
    public Date getDate(){
        try{
            String dateValue = parsedData1[2];
            String []dateSplit = dateValue.split('/');
            dateValue = dateSplit[0];
            dateValue += '/' + dateSplit[1];
            dateValue += '/' + dateSplit[2].substring(0, 4);        
            Date parsedDate = Date.parse(dateValue.stripHtmlTags());
            System.debug('parsedDate = '+parsedDate);
            return parsedDate;
        }catch(Exception e){
            return null;
        }              
    }
    
    public Datetime getQuotesDueBy(){
        try{
            String dateValue = parsedData1[3];           
            String []dateSplit = dateValue.split('/');
            dateValue = dateSplit[0];
            dateValue += '/' + dateSplit[1];
            //dateValue += '/' + dateSplit[2].substring(0, 4);
            //dateValue =  dateValue.substringAfter((indexOfChar('/')-2));
            if(dateSplit[2].containsIgnoreCase('AM'))
            {
            dateValue += ('/' +dateSplit[2].substringBeforeLast(':')+' AM').trim();
            }
            else if(dateSplit[2].containsIgnoreCase('PM'))
            {
            dateValue += ('/' +dateSplit[2].substringBeforeLast(':')+' PM').trim();
            }
            /*try{
           
           dateValue=(dateValue.substringBefore('AM ')+'AM').trim();
           }catch(Exception e)
           {
           dateValue=(dateValue.substringBefore('PM ')+'PM').trim();
           }*/
           
           system.debug('@@**'+dateValue);
           system.debug('@@@***'+dateValue.stripHtmlTags());
            Datetime parsedDate = Datetime.parse(dateValue.stripHtmlTags());
            System.debug('****parsedDate = '+parsedDate);
            return parsedDate;
        }catch(Exception e){
            return null;
        }   
        
    }
    
    public String getRFQTitle(){
        try{
            String rFQTitle = parsedData1[4];
            String cleanRFQTitle = rFQTitle.stripHtmlTags();
            return checkValidString(cleanRFQTitle,255);
        }catch(Exception e){
            return null;
        }
    }
    
    
    private String checkValidString(String str, Integer endLimit){
        if(str.length() > endLimit){
            String validString = str.substring(0,endLimit);
            return validString;
        }
        return str;
    }    
    
    public Task_Order__c getTaskOrder(String toAddress){     
        if(Schema.sObjectType.Task_Order__c.isAccessible() && Schema.sObjectType.Contract_Vehicle__c.isAccessible()){
        if( getRFQId()==null || getRFQId().trim().length()==0){
            System.debug('*********null');
            return null;
        }
        
        List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];
        //System.debug('*********contract vechcle size  = '+ conVehicalList.size()+'\tAccount Name = '+conVehicalList.get(0).Account_Name__c);
        
        //if(!conVehicalList.isEmpty()){        //
        Task_Order__c taskOrder = new Task_Order__c();
        taskOrder.Contract_Vehicle__c = conVehicalList[0].Id;
        taskOrder.Name = checkValidString(getRFQId(),79);
        System.debug('####'+getAction());
        taskOrder.ASB_Action__c = getAction();
        System.debug('#####'+taskOrder.ASB_Action__c);
        if(getDate() != null){
            taskOrder.Release_Date__c = getDate();
        }
        if(getQuotesDueBy() != null){
            taskOrder.Due_Date__c = getQuotesDueBy().date();
            taskOrder.Proposal_Due_DateTime__c =getQuotesDueBy();
        }
        taskOrder.Task_Order_Title__c = getRFQTitle();
        //taskOrder.Email_Reference__c = getTrimedEmailSubject(checkValidString(emailSubject,255));
        
        //taskOrder.Contract_Vehicle_picklist__c
        
        return taskOrder;
        // }
        }
        return null;
    }
    
    /*private String getTrimedEmailSubject(String subject){
        if(subject.startsWith('RE:')){
            subject = subject.replaceFirst('RE:', '');            
        }
        if(subject.startsWith('Fwd:') ){
            subject = subject.replaceFirst('Fwd:', '');            
        }   
        return subject.trim();
    }*/
    
    public Task_Order__c updateTaskOrder(Task_Order__c taskOrder, String toAddress){
        List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];
        //System.debug('*********contract vechcle size  = '+ conVehicalList.size()+'\tAccount Name = '+conVehicalList.get(0).Account_Name__c);
        System.debug('Inside update');
        //if(!conVehicalList.isEmpty()){
        taskOrder.Contract_Vehicle__c = conVehicalList[0].Id;
        taskOrder.Name = checkValidString(getRFQId(),79);
        System.debug('####'+getAction());
        taskOrder.ASB_Action__c = getAction();
        System.debug('#####'+taskOrder.ASB_Action__c);
        if(getDate() != null){
            taskOrder.Release_Date__c = getDate();
        }
        if(getQuotesDueBy() != null){
            taskOrder.Due_Date__c = getQuotesDueBy().date();
            taskOrder.Proposal_Due_DateTime__c =getQuotesDueBy();
        }
        taskOrder.Task_Order_Title__c = getRFQTitle();
        return taskOrder;
    }
    
    private string getTaskOrderNumber(String emailSubject){
        String[] words = emailSubject.split(' ');
        String taskOrderNum = '';
        //taskOrderNum.las
        for(Integer i = 0; i<words.size();i++){
            if(words[i].startsWith('RF')){
                while(words[i].length() !=0){                    
                    try{
                        Integer index = words[i].length();
                        String temp = words[i].substring(index -2);
                        if(temp.startsWith('-')){
                            taskOrderNum += words[i]; 
                            return taskOrderNum;
                        }
                        Integer.valueOf(temp);
                        taskOrderNum += words[i]; 
                        return taskOrderNum;
                    }catch(Exception e){
                        taskOrderNum += words[i]+' ';
                        i++;
                        continue;
                    }
                    
                }
                
            }
        }
        return null;
    }
    
}