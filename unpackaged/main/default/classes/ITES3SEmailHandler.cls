global class ITES3SEmailHandler implements Messaging.InboundEmailHandler {

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:email.toAddresses.get(0)];
        if(!conVehicalList.isEmpty()){
            ITES3SEmailParser parser = new ITES3SEmailParser();
            parser.parse(email);//Email Parsing will be handle by parse method.
            if(parser.taskOrderNumber!=null && parser.taskOrderNumber.trim().length()>0){
                //Below Checked for existing TaskOrder if alredy created before
                Task_Order__c taskOrder = EmailHandlerHelper.getExistTaskOrder(parser.taskOrderNumber, conVehicalList[0].ID);
                if(taskOrder == null){
                    taskOrder = parser.getTaskOrder(email.toAddresses.get(0));
                    if(taskOrder != null){  
                        try{
                            DMLManager.insertAsUser(taskOrder);
                            EmailHandlerHelper.insertBinaryAttachment(taskOrder,email);
                            EmailHandlerHelper.insertTextAttachment(taskOrder,email);
                            EmailHandlerHelper.insertActivityHistory(taskOrder,email);
                            if(parser.modificationName != null){
                                createContractMod(parser.modificationName,parser.modificationText,taskOrder.Id);
                            }
                        }catch(Exception e){
                            System.debug('error message = '+e.getMessage());
                        }
                    }
                }else{// If TaskOrder already present then execution comes to else block
                    if(taskOrder.Is_cancelled__c == false){
                        Task_Order__c oldTaskOrder = taskOrder.clone(false,true,false,false);       
                        Task_Order__c updatedTakOrder = parser.updateTaskOrder(taskOrder, email.toAddresses.get(0));
                        try{
                            EmailHandlerHelper.insertContractMods(updatedTakOrder , oldTaskOrder,email);
                            update updatedTakOrder;                        
                            EmailHandlerHelper.insertBinaryAttachment(updatedTakOrder,email);
                            EmailHandlerHelper.insertTextAttachment(updatedTakOrder,email);
                            EmailHandlerHelper.insertActivityHistory(updatedTakOrder,email);    
                            if(parser.modificationName != null){
                                createContractMod(parser.modificationName,parser.modificationText,taskOrder.Id);
                            }
                            system.debug('Already Exists');                              
                        }catch(Exception e){
                            System.debug('error message = '+e.getMessage()+'\tline number = '+e.getLineNumber());
                        }
                    }                                        
                }
            }
        }else{
            System.debug('Contract Vehicle not found.');
        }
        return new Messaging.InboundEmailresult();
    }
    
    
    private void createContractMod(String modificationName,String modificationText,Id taskOrderId){
        Contract_Mods__c contractMod = new Contract_Mods__c();
        RecordType contractModRecordType = [SELECT Id FROM RecordType WHERE DeveloperName = 'Contract_Mod' AND sObjectType = 'TM_TOMA__Contract_Mods__c' LIMIT 1][0];
        contractMod.RecordTypeId = contractModRecordType.Id;
        contractMod.TaskOrder__c = taskOrderId;
        contractMod.Modification_Name__c = modificationName;
        contractMod.Modification__c = modificationText;
        DMLManager.insertAsUser(contractMod);    
    }
}