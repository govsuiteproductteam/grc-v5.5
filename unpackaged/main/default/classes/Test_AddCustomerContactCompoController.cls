@IsTest
public class Test_AddCustomerContactCompoController {
    static testMethod void testAddCustomerContactCompoCont(){
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Contact con = TestDataGenerator.createContact('Adam', acc);
         con.LastName = 'Smith';
        insert con;
        Contract_Vehicle__c conVeh = TestDataGenerator.createContractVehicle('1234567', acc);
        insert conVeh;
        Customer_Contacts__c cusCon = TestDataGenerator.createCustomerContact(con,conVeh);
        insert cusCon;
       // AddCustomerContactCompoController.checkLicensePermition();
        List<Customer_Contacts__c> custConList = AddCustomerContactCompoController.getVehicalPatner(conVeh.Id);
        System.assertEquals(1, custConList.size());
        
        System.assert(AddCustomerContactCompoController.getRoleMap() != Null);
        
        AddCustomerContactCompoController.deleteCustomerContact(cusCon.Id);
        custConList = AddCustomerContactCompoController.getVehicalPatner(conVeh.Id);
        System.assertEquals(0, custConList.size());
        AddCustomerContactCompoController.addNewVehicalPartner(conVeh.Id);
        
        string dataStr = '['+'undefined'+'SPLITDATA'+conVeh.Id+'SPLITDATA'+con.Id+'SPLITDATA'+'Administrative Contracting Officer'+'SPLITPARENT';
        AddCustomerContactCompoController.saveCustomerContact(dataStr);
        
        custConList = AddCustomerContactCompoController.getVehicalPatner(conVeh.Id);
        System.assertEquals(1, custConList.size());
    }
    
}