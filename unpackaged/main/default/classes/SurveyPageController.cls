/*
Developer   :    Avanti
Description :    Apex Class to create Survey questionnaire for IDIQ Survey app.
VF Page     :    SurveyPage
*/

global with sharing class SurveyPageController
{
    
    public Id SurveyId {get;set;}
    public string msg{get;set;}
    public Survey__c survey{get;set;}
    public List<QuestionWrapper> queWraList{get;set;}
    public  QuestionWrapper qw{get;set;}
    Public Question__c que{get;set;}
    public Integer rowIndex { get; set; } 
    public Integer queNo { get; set; }
    public Integer optNo { get; set; }
    public Integer qTypeNo{get;set;}
    public List<Question__c> removeQuestionList{get; set;}
    public boolean showAddQuestion {get; set;}
    public boolean showAddQues {get; set;} 
    // public Survey_Task__c st {get;set;}
    public String taskOrderId{get;set;}
    public Boolean dialogBoxFlag{get;set;}
    public String isModified {get;set;}
    public String cl{get;set;}
    private List<OptionWrapper> lstOptions = new List<OptionWrapper>{new OptionWrapper('',1),new OptionWrapper('',2),new OptionWrapper('',3),new OptionWrapper('',4),new OptionWrapper('',5),new OptionWrapper('',6),new OptionWrapper('',7),new OptionWrapper('',8),new OptionWrapper('',9),new OptionWrapper('',10)};
        public String isValidLiecence{get;set;}
    
    public surveyPageController(ApexPages.StandardController controller){
        if(Schema.sObjectType.Survey__c.isAccessible() && Schema.sObjectType.Question__c.isAccessible() ){
            isValidLiecence = LincenseKeyHelper.checkLicensePermitionForFedTOM();
            if(isValidLiecence == 'Yes'){
                msg='';
                dialogBoxFlag = false;
                removeQuestionList = new List<Question__c>();        
                if( ApexPages.currentPage().getParameters().get('id') != null ){
                    surveyId = String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('id'));
                }           
                if( ApexPages.currentPage().getParameters().get('tId') != null ){
                    taskOrderId = String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('tId'));
                }           
                if( ApexPages.currentPage().getParameters().get('clone') != null ){
                    cl= String.escapeSingleQuotes(ApexPages.CurrentPage().getParameters().get('clone'));
                }
                if( ApexPages.currentPage().getParameters().get('isModified') != null ){
                    isModified= String.escapeSingleQuotes(ApexPages.CurrentPage().getParameters().get('isModified'));
                }
                // taskOrderId =  controller.getid();
                
                if(SurveyId  == null ){
                    queWraList= new List<QuestionWrapper>();
                    que = new Question__c();
                    survey = new Survey__c();
                    showAddQuestion = true;
                    showAddQues = true;
                    AddQuestion();
                    qw = new QuestionWrapper(this,null);
                }else{
                    survey = [Select Id,description__c,Name, isActive__c From Survey__c where id =: SurveyId];
                    
                    List<Question__c> queList = new List<Question__c>();
                    queList = [SELECT Id,Question__c,Question_Type__c,Option_1__c,Option_2__c,Option_3__c,Option_4__c,Option_5__c,Option_6__c, 
                               Option_7__c,Option_8__c ,Option_9__c ,Is_Mandatory__c ,Option_10__c ,sequence__c FROM Question__c WHERE Survey__r.id=:surveyId Order by sequence__c ASC Limit 10000];
                    queWraList = new List<QuestionWrapper>();
                    if(queList != null && queList.size()>0){                                      
                        for(Question__c que : queList ){   
                            if(cl != null)
                            {
                                que.id = null;
                                que.survey__c= null;
                            }
                            QuestionWrapper qw = new QuestionWrapper (this,que);
                            
                            qw.isDescriptive = false;
                            qw.isText = false;
                            qw.srNo = queWraList.size()+1;
                            qW.question.Sequence__c = qW.srNo;
                            queWraList.add(qw);
                            
                            if(que.Question_Type__c == 'Descriptive' || que.Question_Type__c == 'Text' )
                            {
                                qw.isDescriptive = false;
                                qw.isText = false;
                            }
                            else
                            {
                                qw.isDescriptive = true;
                                qw.isText = true;
                            }
                        }
                    }
                    
                }
                if(cl != null)
                {
                    survey.id = null;
                }  
            }
        }                    
    }   
    
    public void closeDialogBox(){
        dialogBoxFlag = false;
    }
    
    // The save() method is used to save all question with there respective options.
    public Pagereference save(){
        if(Schema.sObjectType.question__c.isAccessible() && Schema.sObjectType.Survey__c.isAccessible() ){
            List<question__c> queList = new List<question__c>();
            if(survey.name != null && survey.name != ''){
                /*  List<Survey__c> surveyNameList = [SELECT id from Survey__c WHERE name=: survey.name];
if(surveyNameList.size()>0){
dialogBoxFlag = true;
msg ='Survey Name already existed.';
// ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Survey Name already existed.')); 
//survey.name.addError('Survey Name already existed.'); 
return null;
}
else{
upsert Survey;
} */
                
                DMLManager.upsertAsUser(Survey);
                for(QuestionWrapper queWra : queWraList){
                    question__c qs = queWra.question;
                    if(queWra.question.Question__c != null && queWra.question.Question__c != ''){
                        
                        for(Integer optCount =0; optCount<10; optCount++){
                            Integer apiCount = optCount+1;
                            if(optCount<queWra.lstOptions.size() && queWra.question.Question_Type__c!='Descriptive' && queWra.question.Question_Type__c!='Text'){
                                qs.put('Option_'+apiCount+'__c',queWra.lstOptions.get(optCount).opt);
                            }else{
                                qs.put('Option_'+apiCount+'__c',Null);
                            }
                        }
                        
                        qs.Question__c=queWra.question.Question__c;
                        qs.Is_Mandatory__c = queWra.question.Is_Mandatory__c;
                        qs.Question_Type__c = queWra.question.Question_Type__c;
                        
                        if(qs.Survey__c == null)
                            qs.Survey__c = Survey.id;
                        
                        queList.add(qs);
                    }
                }
                
                try {
                    if(queList.size() > 0 ){
                        DMLManager.upsertAsUser(queList);
                        DMLManager.deleteAsUser(removeQuestionList);
                    }
                    
                    PageReference pg = new Pagereference('/apex/SurveyDisplayPage?id='+Survey.id);
                    pg.setRedirect(true);
                    return pg;
                    
                }catch(Exception e){                   
                    String error = e.getMessage();
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info,error));
                    return null;
                }
            }else{
                dialogBoxFlag = true;
                msg ='Please Enter Survey Name.';
            }
        }
        return null;
    }
    
    //The addQuestion() method is used to add questions       
    public void AddQuestion (){ 
        
        QuestionWrapper qw = new QuestionWrapper(this,null);
        qw.question = new question__c();
        qw.question.Question_Type__c='Radio';
        qw.srNo = queWraList.size() + 1;
        qw.question.Sequence__c = queWraList.size() +1;
        qw.question.Survey__c = Survey.id;               
        queWraList.add(qw);   
    }
    
    //The removeQuestion() method is used to remove question.
    public void RemoveQuestion (){
        QuestionWrapper qw = new QuestionWrapper(this,null);
        // Integer i=0;
        //  i = queWraList.size() - 1;
        if(removeQuestionList  == null){
            removeQuestionList =new List<Question__c>();
        }
        if(queWraList.get(rowIndex)!= null && queWraList.get(rowIndex).question.id != null ){
            String idString = queWraList.get(rowIndex).question.id;
            if(idString.length() > 0){
                removeQuestionList.add(queWraList.get(rowIndex).question);
            }
        }
        queWraList.remove(rowIndex);
        
        for(Integer i = 0; i < queWraList.size()  ; i++ )
        {           
            queWraList[ i ].srNo = i+1; 
        }
        
    }
    
    // the toggleContent() is used to hide "Add Option" button on Survey page.
    public void toggleContent(){ 
        for(QuestionWrapper q:queWraList)
        {
            if((q.question.Question_Type__c=='Descriptive' || q.question.Question_Type__c=='Text')  && q.srNo==qTypeNo)
            {
                q.isDescriptive = false;
                q.isText = false;
                break;
            }
            else{
                if((q.question.Question_Type__c!='Descriptive' || q.question.Question_Type__c!= 'Text') && q.srNo==qTypeNo){
                    q.isDescriptive = true;
                    q.isText = true;
                    break;
                }
            }
        }
    }
    
    /**************************************** Question with option Wrapper Classes *****************************************/
    public class OptionWrapper
    { 
        //   public Question__c quest{get;set;}
        public String opt{get;set;}
        public Integer Sr {get;set;}
        public OptionWrapper()
        {
        }
        public OptionWrapper(String str,integer sr)
        {
            opt=str ;
            this.sr = sr;
        }
    }
    
    
    public class QuestionWrapper
    {
        public Question__c question {get;set;}
        public integer srNo {get; set;}
        public list<OptionWrapper> lstOptions{get;set;}
        public boolean showMand {get; set;}
        public boolean isDescriptive{get;set;}
        public boolean isText {get;set;}
        public Integer qTypeNo{get;set;}
        public Integer optNo{get;set;}
        public Integer dataVal{get;set;}
        surveyPageController sc;
        
        public QuestionWrapper(surveyPageController sc,Question__c ques)
        {
            if(ques == null){
                this.sc = sc;
                question = new Question__c();
                isDescriptive = true;
                isText = true;
                dataVal = 2;
                lstOptions = new list<OptionWrapper>();
                lstOptions.add(new OptionWrapper('',1));
                lstOptions.add(new OptionWrapper('',2));
                //lstOptions = sc.lstOptions;               
                showMand = false;                
            }
            else{
                question = ques;
                lstOptions = new list<OptionWrapper>();               
                Integer i=1;
                isDescriptive = false;
                isText = false;
                dataVal = 0;
                while(i <= 10 && question.get('Option_'+i+'__c')!=null){
                    String optStr = (String)question.get('Option_'+i+'__c');
                    if(optStr.length() > 0){
                        lstOptions.add(new OptionWrapper( (String)question.get('Option_'+i+'__c'),i));
                        dataVal++;
                    }
                    i++;
                }
            }
        }
        
        public void addOption(Question__c que,List<OptionWrapper> optWrapper){
            System.debug(''+optWrapper.size());            
            if(que.Question_Type__c!='Descriptive' || que.Question_Type__c!= 'Text' )
            {
                optWrapper.add(new OptionWrapper('',optWrapper.size() + 1));  
            } 
            
        }
        
        public void AddOption()
        {                 
            if(question.Question_Type__c!='Descriptive' || question.Question_Type__c!= 'Text' )
            {
                lstOptions.add(new OptionWrapper('',lstOptions.size() + 1));  
            }                 
        }
        
        public void RemoveOption()
        {
            //System.debug('option number '+sc.optNo);
            lstOptions.remove(optNo);
            for(Integer i = 0; i < lstOptions.size()  ; i++ ){
                lstOptions[i].sr = i+1; 
            }
        }
        
    }   
    
    public Integer index{get;set;}
    public void addOptionOnQuestionTypeChange(){
        QuestionWrapper queWrapper = queWraList.get(index);
        if(queWrapper.lstOptions != null && queWrapper.lstOptions.isEmpty()){
            queWrapper.addOption(queWrapper.question, queWrapper.lstOptions);
            queWrapper.addOption(queWrapper.question, queWrapper.lstOptions);
        }
        System.debug(''+index);        
    }
    /********************************************************************************************************************/ 
}