public class TM_AtglanceWrap {
    @AuraEnabled
    public  List<CLM_AtaGlanceField__c> atglanceList{get;set;}
    @AuraEnabled
    public  Map<String,String> fieldMap{get;set;}
    @AuraEnabled
    public  Map<String,String> fieldacceccMap{get;set;}//25-10-2018
    public TM_AtglanceWrap(List<CLM_AtaGlanceField__c> atglanceList,Map<String,String> fieldMap){
        this.atglanceList=atglanceList;
        this.fieldMap=fieldMap;
    }
    //25-10-2018
    public TM_AtglanceWrap(List<CLM_AtaGlanceField__c> atglanceList,Map<String,String> fieldMap,Map<String,String> fieldseqlevmap){
        this.atglanceList=atglanceList;
        this.fieldMap=fieldMap;
        this.fieldacceccMap=fieldseqlevmap;
    }
    
}