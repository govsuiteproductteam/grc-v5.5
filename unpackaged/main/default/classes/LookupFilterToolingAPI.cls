public class LookupFilterToolingAPI {
    
    public enum SObjectType {
        CustomField,
            EntityDefinition,
            FieldDefinition,
            User,
            SearchLayout
            }
    
    // The API version used relates to the types and structures defined here
    private static final String TOOLING_API_URI = '/services/data/v36.0/tooling';
    
    // Session Id can be resovled automatically depending on consturctor used
    private String sessionId;
    
    // Interface used to implement customi serialization on SObject based types
    private interface ISerialize {
        void serialize(JSONGenerator generator);
    }
    
    /**
* Uses the current users Session Id, only compatible in a interactive context
* @throws ToolingAPIException if no Session Id can be resolved (e.g. in a batch context)
**/
    public LookupFilterToolingAPI() {
        this.sessionId = UserInfo.getSessionId();
        if(this.sessionId==null)
            throw new ToolingAPIException('Unable to obtain Session Id');
    }
    
    /**
* Uses the given Session Id, useful when using the API in a batch context
**/
    public LookupFilterToolingAPI(String sessionId) {
        this.sessionId = sessionId;
    }
    
    /**
* Using this query as an example for calling the private static helper method.
* query
* @description Uses the queryString to issue a query via the Tooling API
* @param The query string to use
* @return a ToolingAPI Query Result
* @throws ToolingAPIException if an an exception was encountered.
*/
    public QueryResult query(String queryString) {
        HttpResponse response = submitRestCall('/query/?q=' + EncodingUtil.urlEncode(queryString, 'UTF-8'));
        System.debug('response.getBody() '+response.getBody());
        return parseQueryResult(response.getBody());
    }
    
    public class QueryResult {
        public boolean              done;
        public String               entityTypeName;
        public String               nextRecordsUrl;
        public SObject_x[]          records;
        public Integer              size;
        public Integer              totalSize;
        public String               queryLocator;
    }
    
    /*
* Helper method for submitting the REST HTTP GET request.
*
* @throws ToolingAPIAuthorizationException if the remote site is not authorized.
*/
    private HttpResponse submitRestCall(String relativeUrl){
        return submitRestCall(relativeUrl, 'GET');
    }
    
    /*
* Helper method for submitting the REST request using the given method.
*
* @throws ToolingAPIAuthorizationException if the remote site is not authorized.
*/
    private HttpResponse submitRestCall(String relativeUrl, String method){
        return submitRestCall(relativeUrl, method, null);
    }
    
    /*
* Helper method for submitting the REST request using the given method and data.
*
* @throws ToolingAPIAuthorizationException if the remote site is not authorized.
*/
    private HttpResponse submitRestCall(String relativeUrl, String method, Object data){
        Http h = new Http();
        HttpRequest queryReq = new HttpRequest();
        queryReq.setEndpoint(URL.getSalesforceBaseUrl().toExternalForm() + TOOLING_API_URI + relativeUrl);
        
        queryReq.setHeader('Authorization', 'OAuth ' + this.sessionId);
        queryReq.setHeader('Content-Type', 'application/json');
        queryReq.setMethod(method);
        if(data!=null)
        {
            // Custom serializer?
            if(data instanceof ISerialize)
            {
                ISerialize dataToserialize = (ISerialize) data;
                JSONGenerator jsonGen = JSON.createGenerator(false);
                jsonGen.writeStartObject();
                dataToserialize.serialize(jsonGen);
                jsonGen.writeEndObject();
                queryReq.setBody(jsonGen.getAsString());
            }
            else
            {
                // Standard JSON serializer emits null values, 
                //    which are generally not tolerated by Tooling API
                queryReq.setBody(JSON.serialize(data));
            }
        }
        
        HttpResponse queryRes = null;
        try
        {
            queryRes = h.send(queryReq);
        }
        catch (System.CalloutException ce)
        {
            if (ce.getMessage().containsIgnoreCase('unauthorized endpoint'))
            {
                throw new ToolingAPIAuthorizationException(ce);
            }
            else
            {
                throw ce;
            }
        }
        
        Integer successCode = 200;
        if(method.equals('POST'))
            successCode = 201;
        else if(method.equals('DELETE'))
            successCode = 204;
        if(queryRes.getStatusCode() != successCode)
            if(queryRes.getBody().length()>0)
            throw new ToolingAPIException((List<ErrorResponse>) JSON.deserialize(queryRes.getBody(), List<ErrorResponse>.class));
        else
            throw new ToolingAPIException('Unexpected HTTP Status ' + queryRes.getStatusCode());
        return queryRes;
    }
    
    /**
* Helper method for parsing query results
**/
    private QueryResult parseQueryResult(String jsonStr){
        QueryResult queryResult = (QueryResult)JSON.deserialize(jsonStr, LookupFilterToolingAPI.QueryResult.class);
        queryResult.records = getQueryResultRecords(jsonStr);
        return queryResult;
    }
    
    public virtual class SObject_x {
        public transient SObjectType type_x {get; private set;}
        public transient String[]    fieldsToNull;
        public Id                    id;
        public Id                    createdById;
        public User_x                createdBy;
        public DateTime              createdDate;
        public boolean               isDeleted;
        public Id                    lastModifiedById;
        public User_x                lastModifiedBy;
        public Datetime              lastModifiedDate;
        public DateTime              systemModstamp;
        public SObject_x(SObjectType sObjectType){
            type_x = sObjectType;
        }
        public virtual void serialize(JSONGenerator jsonGen) {
            if(id!=null)
                jsonGen.writeStringField('id', id);
            if(fieldsToNull!=null)
                for(String fieldToNull : fieldsToNull)
                jsonGen.writeNullField(fieldToNull);
        }
    }
    
    private List<SObject_x> getQueryResultRecords(String jsonStr){
        
        String recordType = getRecordType(jsonStr);
        
        if(recordType != null){
            JSONParser parser = JSON.createParser(jsonStr);
            
            while (parser.nextToken() != null) {
                if ((parser.getText() == 'records')) {
                    parser.nextToken();
                    return (List<SObject_x>)parser.readValueAs(Type.forName('List<LookupFilterToolingAPI.'+recordType+'>'));
                }
            }
        }
        
        return null;
    }
    
    private String getRecordType(String jsonStr){
        JSONParser parser = JSON.createParser(jsonStr);
        
        while (parser.nextToken() != null) {
            if ((parser.getText() == 'records')) {
                while(parser.nextToken() != null) {
                    if(parser.getText() == 'attributes'){
                        while(parser.nextToken() != null){
                            if(parser.getText() == 'type'){
                                //Value of type attribute
                                parser.nextToken();
                                return parser.getText();
                            }
                        }
                    }
                }
            }
        }
        return null;
    }
    
    public class ToolingAPIException extends Exception{
        public ToolingAPIException(List<ErrorResponse> errorResponses){
            this(errorResponses[0].errorCode + ' : ' + errorResponses[0].message);
        }
    }
    
    public class ToolingAPIAuthorizationException extends Exception
    {
        
    }
    
    public class ErrorResponse{
        public List<String> fields;
        public String errorCode;
        public String message;
    }
    
    public class User_x extends SObject_x implements ISerialize {
        public QueryResult delegatedUsers;
        public QueryResult userPreferences;
        public Id          workspaceId;
        public User_x() {
            super(SObjectType.User);
        }
        public override void serialize(JSONGenerator jsonGen) {
            super.serialize(jsonGen);
            if(delegatedUsers!=null)
                jsonGen.writeObjectField('delegatedUsers', delegatedUsers);
            if(userPreferences!=null)
                jsonGen.writeObjectField('userPreferences', userPreferences);
            if(workspaceId!=null)
                jsonGen.writeIdField('workspaceId', workspaceId);
        }                
    }
    
    public class CustomField extends SObject_x implements ISerialize {
        public String              fullName;
        public String              developerName;
        public CustomFieldMetadata metadata;
        public String              namespacePrefix;
        public String              tableEnumOrId;
        public CustomField() {
            super(SObjectType.CustomField);
        }
        public override void serialize(JSONGenerator jsonGen) {
            super.serialize(jsonGen);
            if(fullName!=null)
                jsonGen.writeStringField('fullName', fullName);
            if(developerName!=null)
                jsonGen.writeStringField('developerName', developerName);
            if(metadata!=null)
                jsonGen.writeObjectField('metadata', metadata);
            if(namespacePrefix!=null)
                jsonGen.writeStringField('namespacePrefix', namespacePrefix);
            if(tableEnumOrId!=null)
                jsonGen.writeStringField('tableEnumOrId', tableEnumOrId);
        }        
    }
    
    public class CustomFieldMetadata {
        public String label;
        public LookupFilter lookupFilter;
        public String type;
        public String relationshipName;
        public String relationshipLabel;
    }
    
    public class LookupFilter {
        public FilterItem[] filterItems;
        public Boolean active;
        public String booleanFilter;
        public boolean isOptional;
    }
    
    public class FilterItem {
        public String field;
        public String operation;
        public String value;
        public String valueField;
    }
    
    public class EntityDefinition extends SObject_x implements ISerialize {
        public String developerName;
        public String fullName;
        
        public EntityDefinition() {
            super(SObjectType.EntityDefinition);
        }
        public override void serialize(JSONGenerator jsonGen) {
            super.serialize(jsonGen);
            if(developerName!=null)
                jsonGen.writeStringField('DeveloperName', developerName);
            if(fullName!=null)
                jsonGen.writeStringField('FullName', fullName);
        }          
    }
    
    public class FieldDefinition extends SObject_x implements ISerialize {
        public String developerName;
        public EntityDefinition entityDefinition;
        public CustomFieldMetadata metadata;
        public String dataType;
        
        public FieldDefinition() {
            super(SObjectType.FieldDefinition);
        }
        public override void serialize(JSONGenerator jsonGen) {
            super.serialize(jsonGen);
            if(developerName!=null)
                jsonGen.writeStringField('developerName', developerName);
            if(entityDefinition!=null)
                jsonGen.writeObjectField('EntityDefinition', entityDefinition);
            if(metadata!=null)
                jsonGen.writeObjectField('metadata', metadata);
        }        
    }
     // New Changes for Lookup Search Layout - Payal(7March2018)    
    public class SearchLayoutField {
        public String apiName;
        public String label;
        public String sortable;
    }
    
    public class SearchLayoutFields {
        public SearchLayoutField[] fields;
    }
    
    public class SearchLayout extends SObject_x implements ISerialize {
        public String entityDefinitionId;
        public String layoutType;
        public SearchLayoutFields fieldsDisplayed;
        
        public SearchLayout() {
            super(SObjectType.SearchLayout);
        }
        public override void serialize(JSONGenerator jsonGen) {
            super.serialize(jsonGen);
            if(entityDefinitionId!=null)
                jsonGen.writeStringField('EntityDefinitionId', entityDefinitionId);
            if(layoutType!=null)
                jsonGen.writeStringField('LayoutType', layoutType);
            if(fieldsDisplayed!=null)
                jsonGen.writeObjectField('FieldsDisplayed', fieldsDisplayed);
        }        
    }
    
    public static LookupFilterToolingAPI.SearchLayoutFields getLookupSearchLayoutInfo(String parentObjName){
        try{
            System.debug('parentObjName=='+parentObjName);
            if(parentObjName != null){
                LookupFilterToolingAPI toolingAPI = new LookupFilterToolingAPI(UserInfo.getSessionId());
                List<LookupFilterToolingAPI.SearchLayout> result = (List<LookupFilterToolingAPI.SearchLayout>)
                    toolingAPI.query('Select EntityDefinitionId, FieldsDisplayed, LayoutType From SearchLayout where EntityDefinitionId=\''+parentObjName+'\' AND LayoutType=\'Lookup\'').records;
                System.debug('result=='+toolingAPI.query('Select EntityDefinitionId, FieldsDisplayed, LayoutType From SearchLayout where EntityDefinitionId=\''+parentObjName+'\' AND LayoutType=\'Lookup\''));
                if(result != null){
                    System.debug('EntityDefinitionId==' + result[0].EntityDefinitionId);
                    System.debug('FieldsDisplayed==' + result[0].FieldsDisplayed);
                    LookupFilterToolingAPI.SearchLayoutFields searchLayoutFields = result[0].FieldsDisplayed;
                    return searchLayoutFields;
                }
            }
        }catch(Exception ex){
            System.debug('Error in Lookup Search Layout:' + ex.getMessage() + '\t' + ex.getLineNumber());
        }
        return null;
    }
    // New Changes for Lookup Search Layout - Payal(7March2018)
 /*     public String getDataType(String objName, String fieldAPI){
     System.debug('fieldAPI '+fieldAPI);   
        if(fieldAPI != '' && fieldAPI != null)
        {
            if(fieldAPI.contains('.') )
            {
                List<String> FilApiList = fieldAPI.split('\\.');
                System.debug('@@@@@@ '+fieldAPI.split('\\.'));
                integer lastInd = FilApiList.size();
                System.debug('FilApiList '+FilApiList);
                System.debug('lastInd '+lastInd);
                fieldAPI =  FilApiList[lastInd-1];
                objName = FilApiList[lastInd-2];
                System.debug('fieldAPI '+fieldAPI+' objName '+objName);
                if(objName.endsWith('__c') ) 
                {
                   objName = objName.removeEnd('__c');
                }
                else if(objName.endsWith('__r'))
                {
                  objName = objName.removeEnd('__r');
                }
            }
        }
        System.debug('fieldAPI '+fieldAPI);
        System.debug('fieldAPI '+objName);
        
        LookupFilterToolingAPI toolingAPI = new LookupFilterToolingAPI();
        List<LookupFilterToolingAPI.FieldDefinition> result = (List<LookupFilterToolingAPI.FieldDefinition>)
            toolingAPI.query('Select DeveloperName,DataType From FieldDefinition where EntityDefinition.DeveloperName=\''+objName+'\' AND DeveloperName=\''+fieldAPI+'\'').records;
        for(LookupFilterToolingAPI.FieldDefinition eachfield : result){
            System.debug(eachfield.DeveloperName);
            System.debug(eachfield.DataType);
            System.debug('------------------');
        }
        return '';
    }*/
    
}