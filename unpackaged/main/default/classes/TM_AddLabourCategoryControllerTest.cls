@isTest
public class TM_AddLabourCategoryControllerTest{
    public Static TestMethod void testMethod1(){
        
           Account acc = TestDataGenerator.createAccount('TaskOrderCapture');
            insert acc;
            
            List<Contact> conList = new List<Contact>();
            Contact con = TestDataGenerator.createContact('test1',acc);
            conList.add(con);
            Contact con1 = TestDataGenerator.createContact('test2',acc);
            conList.add(con1);
            insert conList;
            
              Contract_Vehicle__c contractvehicle= TestDataGenerator.createContractVehicle('TMconVehicle',acc);
            insert contractvehicle;
        
        Project__c proj = TestDataGenerator.createProject(contractvehicle, 'In progress');
        insert proj;
        
        CLM_Labor_Category__c clmLaborCatgory  = TestDataGenerator.createClmLabCategory(contractvehicle ,'Doctorate', proj);
        insert clmLaborCatgory;
        
        Test.startTest();
         ApexPages.currentPage().getParameters().put('id',proj.id);
         ApexPages.StandardController controller = new ApexPages.StandardController(proj);
         TM_AddLabourCategoryController addLaborCatContrl = new TM_AddLabourCategoryController(controller );
         addLaborCatContrl.addContValRow();
         addLaborCatContrl.notifyPM();
         addLaborCatContrl.save();
         system.assertNotEquals(addLaborCatContrl.clmLabourCategoryList.size(), 0);
         addLaborCatContrl.edit();
         ApexPages.currentPage().getParameters().put('index','1');
         addLaborCatContrl.removeContValRow();
        Test.stopTest();
    }
}