({
doInit : function(component, event, helper) {
		var lookupName=component.get("c.getNameFromLookupId");
   // alert(component.get("v.sObjectType"));
   // alert(component.get("v.lookUpId"));
        lookupName.setParams({
            "sObjectType":component.get("v.sObjectType"),
            "lookupId":component.get("v.lookUpId")
        });
        var lowerCaseName=component.get("v.sObjectType").toLowerCase();
        component.set("v.sObjectType",lowerCaseName);
        lookupName.setCallback(this,function(data){
            
            var state= data.getState();
            
            if(state=="SUCCESS"){
                if(data.getReturnValue() != null)
                component.set("v.nameOfObject",data.getReturnValue().Name);
            }
            
        });
        
        $A.enqueueAction(lookupName);
	},
    
    gotoURL : function (component, event, helper) {
    var urlEvent = $A.get("e.force:navigateToURL");
    urlEvent.setParams({
      "url": "/"+component.get("v.lookUpId")
    });
    urlEvent.fire();
}
})