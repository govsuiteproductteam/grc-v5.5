global class RS3EmailHandler implements Messaging.InboundEmailHandler{
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:email.toAddresses.get(0)];
        if(!conVehicalList.isEmpty()){
            RS3EmailParser parser = new RS3EmailParser();
            parser.parse(email);//Email Parsing will be handle by parse method.
            if(parser.taskOrderNumber!=null && parser.taskOrderNumber.trim().length()>0){
                /*Below Checked for existing TaskOrder if alredy created before*/
                Task_Order__c taskOrder = EmailHandlerHelper.getExistTaskOrder(parser.taskOrderNumber, conVehicalList[0].ID);
                
                if(taskOrder == null && !parser.isAmendent){//If email template not exists            
                    taskOrder = parser.getTaskOrder(email.toAddresses.get(0));
                    if(taskOrder != null){  
                        try{
                            DMLManager.insertAsUser(taskOrder);
                            EmailHandlerHelper.insertBinaryAttachment(taskOrder,email);
                            EmailHandlerHelper.insertTextAttachment(taskOrder,email);
                            EmailHandlerHelper.insertActivityHistory(taskOrder,email);
                        }catch(Exception e){
                            System.debug('error message = '+e.getMessage());
                        }
                    }
                }else{// If TaskOrder already present then execution comes to else block   
                    if(!parser.isAmendent){
                        Task_Order__c oldTaskOrder = taskOrder.clone(false,true,false,false);       
                        Task_Order__c updatedTakOrder = parser.updateTaskOrder(taskOrder, email.toAddresses.get(0));
                        //DMLManager.updateAsUser(updatedTakOrder);
                        try{
                            EmailHandlerHelper.insertContractMods(updatedTakOrder , oldTaskOrder,email);
                            update updatedTakOrder;                        
                            EmailHandlerHelper.insertBinaryAttachment(updatedTakOrder,email);
                            EmailHandlerHelper.insertTextAttachment(updatedTakOrder,email);
                            EmailHandlerHelper.insertActivityHistory(updatedTakOrder,email);                
                            system.debug('Already Exists');                              
                        }catch(Exception e){
                            System.debug('error message = '+e.getMessage()+'\tline number = '+e.getLineNumber());
                        }
                    }else{
                        try{
                        if(taskOrder.id != null){
                            Contract_Mods__c contractMod = new Contract_Mods__c();       
                            contractMod.RecordTypeId = Schema.SObjectType.Contract_Mods__c.getRecordTypeInfosByName().get('Contract Mod').getRecordTypeId();
                            String body = email.plainTextBody;
                            contractMod.TaskOrder__c = taskOrder.id;
                            String amd = body.substringAfter(parser.taskOrderNumber);
                            String[] splittedAmd = amd.trim().split(' ', 3);
                            System.debug(''+splittedAmd);
                            if(splittedAmd.size() > 2)
                                contractMod.Modification_Name__c = splittedAmd[0]+' '+splittedAmd[1];
                            
                            //contractMod.Modification__c = body;
                            
                            String cmModification = body.substringBetween(',', 'the below email address.');
                            contractMod.Modification__c = cmModification.trim() + ' the below email address.';
                            
                            DMLManager.insertAsUser(contractMod);    
                            EmailHandlerHelper.insertBinaryAttachment(taskOrder,email);
                            EmailHandlerHelper.insertTextAttachment(taskOrder,email);
                            EmailHandlerHelper.insertActivityHistory(taskOrder,email);
                        }
                        }catch(Exception e){
                            
                        }
                    }     
                }
            }
        }
        else{
            System.debug('Contract Vehicle not found.');
        }
        return new Messaging.InboundEmailresult();
    }
}