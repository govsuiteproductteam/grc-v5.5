@IsTest
public class Test_TM_PageLayoutController {
    public static testMethod void testPageLayoutController(){
        Test.startTest();
        List<RecordType> rtypes = [Select Name, Id From RecordType where sObjectType='TM_TOMA__Contract_Vehicle__c' and isActive=true];
        string recordTypeId ='';
        if(rtypes.size()>0)
        {
            recordTypeId=rtypes[0].Id;
        }
        CLM_Tab__c tabSetting = TestDataGenerator.createCLMTab(recordTypeId);
        tabSetting.TabName__c='Internal Organisation';
        insert tabSetting;  
        CLM_Tab__c tabSetting1 = TestDataGenerator.createCLMTab(recordTypeId);
        tabSetting1.Is_Contract__c = true;
        tabSetting1.Order__c = 2;
        tabSetting1.TabName__c='Contract Description';
        insert tabSetting1;   
        CLM_Section__c sectionSetting = TestDataGenerator.createCLMSection(recordTypeId);
        sectionSetting.Is_Contract__c = true;
        insert sectionSetting;
        CLM_AtaGlanceField__c newOppAtAGlance = TestDataGenerator.createAtaGlanceField(recordTypeId);
        newOppAtAGlance.Is_Contract__c=true;
        insert newOppAtAGlance;
        CLM_FIeld__c fieldSetting = TestDataGenerator.createCLMField(recordTypeId);
        fieldSetting.Is_Contract__c = true;
        fieldSetting.ViewFieldApi__c='LastModifiedById';
        fieldSetting.Order__c = 1001;
        insert fieldSetting;
        
        Account acc = TestDataGenerator.createAccount('testAccount');
        insert acc;
        Contract_Vehicle__c conVeh = TestDataGenerator.createContractVehicle('Test contract vehicle', acc);
        conVeh.recordTypeId = recordTypeId;
        conVeh.Contract_Type__c= TestDataGenerator.getPicklistValue('TM_TOMA__Contract_Vehicle__c','TM_TOMA__Contract_Type__c');
        insert conVeh;
        Test.stopTest(); 
        //ApexPages.currentPage().getParameters().put
        PageReference pageRef = Page.TM_Custom_Page_Layout;
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController stdCntrl = new ApexPages.StandardController(conVeh);
        ApexPages.currentPage().getParameters().put('recordTId',recordTypeId);
        TM_PageLayoutController pageLayoutController1 = new TM_PageLayoutController();
        pageLayoutController1.init(recordTypeId, true);
        TM_PageLayoutController pageLayoutController = new TM_PageLayoutController();
        
        System.assertEquals('Contract', pageLayoutController.objectType);
        
        //TM_PageLayoutTab Start
        TM_PageLayoutTab pageLayoutTab = new TM_PageLayoutTab(pageLayoutController);
        pageLayoutTab.tabName = 'RFP-1SnapshotXIddj';
        pageLayoutTab.bgColor = '#3a4c6b';
        pageLayoutTab.tabStatus = 'green';
        pageLayoutTab.tabNumUpdate =1;
        // pageLayoutTab.buttonLabel = 'test';
        //pageLayoutTab.buttonApiName = 'test';
        pageLayoutTab.tabNumberToDelete = 1;
        // pageLayoutTab.buttonNuUpdateDelete = 1;
        // pageLayoutTab.buttonAction = 'cancel';
        // pageLayoutTab.buttonjsAction = 'cancel';
        pageLayoutController.tabRearrangeString = '&amp;';
        pageLayoutTab.addTab();
        pageLayoutTab.applytabRearrange();
        pageLayoutTab.tabRearrange();
        pageLayoutTab.deleteTab();
        //TM_PageLayoutTab End
        //TM_PageLayoutSection Start
        TM_PageLayoutSection  pageLayoutSection = new TM_PageLayoutSection(pageLayoutController);
        pageLayoutSection.sectionName = 'RFP-10Opportunity Health2Xv23';
        pageLayoutSection.tabSequence = 1;
        pageLayoutSection.secSequenceUpdate = 10;
        pageLayoutSection.tabSequenceDel = 1;
        pageLayoutSection.secSequenceDel = 10;
        //pageLayoutSection.relatedLstObjApiName = 'test';
        //pageLayoutSection.relatedLstLabel = 'test';
        //pageLayoutSection.relatedLstApiName = 'test';
        //pageLayoutSection.showSubmitApproval = true;
        pageLayoutSection.addSection();
        // pageLayoutSection.addRelatedLst();
        pageLayoutSection.deleteSection();
        // pageLayoutSection.deleteRelatedLst();
        pageLayoutSection.tabSequence = 1;
        pageLayoutSection.SecRearrange();
        //pageLayoutSection.relatedLstRearrange();
        // pageLayoutSection.applyrelatedLstRearrange();
        pageLayoutSection.tabSequence = 1;
        pageLayoutSection.applySecRearrange();
        
        
        //TM_PageLayoutSection End
        //
        //
        //
        //
        TM_PageLayoutTab pageLayoutTab1 = new TM_PageLayoutTab(pageLayoutController);
        pageLayoutTab1.tabName = 'RFP-1SnapshotXIddj';
        pageLayoutTab1.bgColor = '#3a4c6b';
        pageLayoutTab1.tabStatus = 'green';
        pageLayoutTab1.addTab();
        
        
        TM_PageLayoutSection  pageLayoutSection1 = new TM_PageLayoutSection(pageLayoutController);
        pageLayoutSection1.sectionName = 'RFP-10Opportunity Health2Xv23';
        pageLayoutSection1.tabSequence = 1;
        pageLayoutSection1.addSection();
        
        TM_PageLayoutField pageLayoutField = new TM_PageLayoutField(pageLayoutController);
        pageLayoutField.fieldApi = 'TM_TOMA__Contract_Vehicle__c&amp;';
        pageLayoutField.fieldLabel = 'Contract Vehicle';
        pageLayoutField.helpText = 'description';
        pageLayoutField.ViewFieldApi = 'TM_TOMA__Contract_Vehicle__c';
        pageLayoutField.isRequired = false;
        pageLayoutField.tabFieldSeq = 1;
        pageLayoutField.secFieldSeq = 1;
        pageLayoutField.glanceFieldBackColor = '#3a4c6b';
        pageLayoutField.glanceIsComponent = true;
        pageLayoutField.glanceColSpan = 3;
        pageLayoutField.glanceDocId = 'dasbs';
        pageLayoutField.tabSequenceFieldDel = 1;
        pageLayoutField.secSequenceFieldDel = 1;
        
        pageLayoutField.addField();
        pageLayoutField.addGlance();
        pageLayoutField.tabFieldSeq = 1;
        pageLayoutField.secFieldSeq = 1;
        pageLayoutField.fieldApi = 'TM_TOMA__Contract_Vehicle1__c&amp;';
        pageLayoutField.glanceIsComponent = false;
        pageLayoutField.addField();
        pageLayoutField.addGlance();        
        pageLayoutField.fieldSequenceUpdate = 1;
        pageLayoutField.addField();
        pageLayoutField.fieldSequenceUpdate = 1;
        pageLayoutField.addGlance();
        pageLayoutField.tabFieldSeq = 1;
        pageLayoutField.secFieldSeq = 1;
        
        pageLayoutField.fieldRearrange();
        pageLayoutField.glanceRearrange();
        pageLayoutField.tabFieldSeq = 1;
        pageLayoutField.secFieldSeq = 1;
        pageLayoutField.plController.tabRearrangeString = 'TM_TOMA__Contract_Vehicle__c&amp;,TM_TOMA__Contract_Vehicle1__c&amp;';
        pageLayoutField.applyFieldRearrange();
        pageLayoutField.tabFieldSeq = 1;
        pageLayoutField.secFieldSeq = 1;
        pageLayoutField.plController.tabRearrangeString = 'TM_TOMA__Contract_Vehicle__c&amp;,TM_TOMA__Contract_Vehicle1__c&amp;';
        pageLayoutField.applyGlanceRearrange();
        pageLayoutField.fieldSequenceFieldDel = 1;
        pageLayoutField.deleteField();
        pageLayoutField.deleteGlance();
        
        
        
        
        //pageLayoutField.addGlance();
        //pageLayoutField.fieldSequenceUpdate = 0;
        //pageLayoutField.addGlance();
        pageLayoutController.ataGlanceWrapperList.get(0).ataGlanceField.Is_Component__c = false;
        pageLayoutField.applyGlanceRearrange();
        //
        //
        //( recordTypeId, 'Test Page', 1, 1,'test', 'test');
        TM_PageLayoutVFP pageLayoutVFP = new TM_PageLayoutVFP(pageLayoutController);
        pageLayoutVFP.vfpPageLabel = 'test';
        pageLayoutVFP.vfpPageAPI = 'test';
        pageLayoutVFP.vfpWidth = 100;
        pageLayoutVFP.vfpHeight = 100;
        pageLayoutVFP.vfpTabSequence = 1;
        //pageLayoutVFP.vfpSequenceUpdate = 1;
        pageLayoutVFP.vfpIsComponant = true;
        pageLayoutVFP.recordCloinId = recordTypeId;
        pageLayoutVFP.addVFPPage();
        pageLayoutVFP.vfpSequenceUpdate = 1;
        pageLayoutVFP.addVFPPage();
        pageLayoutVFP.vfpSequenceUpdate = 1;
        pageLayoutVFP.vfpIsComponant = false;
        pageLayoutVFP.addVFPPage();
        pageLayoutVFP.vfpSequenceUpdate = 10;
        pageLayoutVFP.addVFPPage();
        pageLayoutVFP.vfpPageLabel = 'test';
        pageLayoutVFP.vfpTabSequence = 1;
        pageLayoutVFP.vfpRearrange();
        pageLayoutVFP.vfpTabSequence = 1;
        pageLayoutVFP.applyVfpRearrange();
        pageLayoutVFP.cloneLayout();
        System.assertEquals(true, pageLayoutVFP.plController.cloneInitiated);
        pageLayoutVFP.go();
        
        pageLayoutVFP.deleteVFPPage();
        //
        string fieldApi = pageLayoutController.getFieldApis();
        System.assert(fieldApi != '');
        pageLayoutController.getFieldLabelApi();
        pageLayoutController.getFieldLabels();
        List<SelectOption> recOptList = pageLayoutController.getRecordTypeOptions();
        System.assert(recOptList.size() >0);
        // pageLayoutController.addVFPPage(oppRecordTypeSectionSetting,1);
        pageLayoutController.doImport();
        // pageLayoutController.doExport();
        pageLayoutController.quickSave();
        PageReference cancelPageRef = pageLayoutController.cancel();
        //System.debug('!!!!!!!!!!!!!!!!!!!!!!!!!! = '+cancelPageRef.getUrl());
        //pageLayoutController.moveVfp();
        //pageLayoutController.moveSection();
        
        pageLayoutController.saveclone();
        //System.debug('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!savePageRef = '+savePageRef.getUrl());
        pageLayoutController.deleteGlance(1);
        pageLayoutController.deleteVFPPage(1, 1);
        pageLayoutController.deleteField(1, 1,1);
        pageLayoutController.deleteSection(1, 1);
        pageLayoutController.deleteTab(2);
        //pageLayoutController.deleteRelatedLst(1);
        PageReference clearPageRef = pageLayoutController.doClear();
        System.debug('clearPageRef @@@@@@@@@@ '+cancelPageRef.getUrl());
        //System.assertEquals('/apex/tm_custom_page_layout', cancelPageRef.getUrl());
        
        pageLayoutController.selectSObject();
    }
    
    public static testMethod void testPageLayoutController1(){
        Test.startTest();
        List<RecordType> rtypes = [Select Name, Id From RecordType where sObjectType='TM_TOMA__Contract_Vehicle__c' and isActive=true];
        string recordTypeId ='';
        if(rtypes.size()>0)
        {
            recordTypeId=rtypes[0].Id;
        }
        //Id recordTypeId = Schema.SObjectType.FedCap_Opportunity__c.getRecordTypeInfosByName().get('RFP').getRecordTypeId();
        //TM_OppRecordTypeSetting__c oppRecordTypeSetting = TM_TestDataGenerator.createOppRecordTypeFieldSetting(recordTypeId);
        // insert oppRecordTypeSetting;
        //TM_OppRecordTypeTabSetting__c oppRecordTypeTabSetting = TM_TestDataGenerator.createOppRecordTypeTabSetting(recordTypeId);
        //insert oppRecordTypeTabSetting;        
        CLM_Section__c sectionSetting = TestDataGenerator.createCLMSection(recordTypeId);
        insert sectionSetting;
        CLM_AtaGlanceField__c newOppAtAGlance = TestDataGenerator.createAtaGlanceField(recordTypeId);
        newOppAtAGlance.Is_Contract__c=true;
        insert newOppAtAGlance;
        
        //TM_RelatedListSettingForOpp__c  relatedListSettingForOpp = TM_TestDataGenerator.createRelatedListSettingForOpp(recordTypeId, 'Opportunity', 1, 'Account','Opportunity', 'Opportunity');
        //insert relatedListSettingForOpp;
        Account acc = TestDataGenerator.createAccount('testAccount');
        insert acc;
        Contract_Vehicle__c conVeh = TestDataGenerator.createContractVehicle('Test contract vehicle', acc);
        conVeh.recordTypeId = recordTypeId;
        conVeh.Contract_Type__c= TestDataGenerator.getPicklistValue('TM_TOMA__Contract_Vehicle__c','TM_TOMA__Contract_Type__c');
        insert conVeh;
        //ApexPages.currentPage().getParameters().put
        PageReference pageRef = Page.TM_ContractNewPage;
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController stdCntrl = new ApexPages.StandardController(conVeh);
        ApexPages.currentPage().getParameters().put('recordTId',recordTypeId);
        TM_PageLayoutController pageLayoutController = new TM_PageLayoutController();
        TM_PageLayoutController pageLayoutController1 = new TM_PageLayoutController();
        pageLayoutController1.init(recordTypeId, true);
        System.assertEquals(false, pageLayoutController1.isRelatedLst);
        System.assert(pageLayoutController1.ataGlanceWrapperList.size()>0);
        //System.debug('%%%%%%%%%%%%%%%% '+pageLayoutController1.);
    }
    
    public static testMethod void moveSectionTest(){
        List<RecordType> rtypes = [Select Name, Id From RecordType where sObjectType='TM_TOMA__Contract_Vehicle__c' and isActive=true];
        String rType;
        if(!rtypes.isEmpty()){
            rType = rtypes[0].Id;
        }
        
        Test.setCurrentPageReference(new PageReference('Page.TM_Custom_Page_Layout')); 
        System.currentPageReference().getParameters().put('recordTId', rType);
        
        TM_PageLayoutController pLCtrl = new TM_PageLayoutController();
        
        //Create tab
        TM_PageLayoutTab tabCtrl = new TM_PageLayoutTab(pLCtrl);
        tabCtrl.congaApiName = 'TM_TOMA__TestField__c';
        tabCtrl.bgColor = 'Red';
        tabCtrl.tabName = 'Snapshot';
        tabCtrl.tabStatus = 'TM_TOMA__TestField__c';
        tabCtrl.addTab();
        
        TM_PageLayoutTab tabCtrl2 = new TM_PageLayoutTab(pLCtrl);
        tabCtrl2.congaApiName = 'TM_TOMA__TestField__c';
        tabCtrl2.bgColor = 'Blue';
        tabCtrl2.tabName = 'Intiligence';
        tabCtrl2.tabStatus = 'TM_TOMA__TestField__c';
        tabCtrl2.addTab();
        
        TM_PageLayoutTab tabCtrlUpdate = new TM_PageLayoutTab(pLCtrl);
        tabCtrlUpdate.congaApiName = 'TM_TOMA__TestField1__c';
        tabCtrlUpdate.bgColor = 'Blue';
        tabCtrlUpdate.tabName = 'Intiligence';
        tabCtrlUpdate.tabStatus = 'TM_TOMA__TestField__c';        
        tabCtrlUpdate.tabNumUpdate = 1;
        tabCtrlUpdate.addTab();
        
        TM_PageLayoutSection secCtrl = new TM_PageLayoutSection(pLCtrl);
        secCtrl.sectionName = 'Section 1';
        secCtrl.tabSequence = 1;
        secCtrl.addSection();
        
        TM_PageLayoutSection secCtrl2 = new TM_PageLayoutSection(pLCtrl);
        secCtrl2.sectionName = 'Section 2';
        secCtrl2.tabSequence = 1;
        secCtrl2.addSection();
        
        TM_PageLayoutSection secCtrlUpdate = new TM_PageLayoutSection(pLCtrl);
        secCtrlUpdate.sectionName = 'Section Two';
        secCtrlUpdate.tabSequence = 1;
        secCtrlUpdate.secSequenceUpdate = 1;
        secCtrlUpdate.addSection();
        
        pLCtrl.tabRearrangeString = 'Section Two1,Section 22';
        TM_PageLayoutSection secCtrlRearrange = new TM_PageLayoutSection(pLCtrl);
        secCtrlRearrange.tabSequence = 1;
        secCtrlRearrange.applySecRearrange();
        
        pLCtrl.fromTabNo = 1;
        pLCtrl.toTabNo = 2;
        pLCtrl.sectionMoveNo = 1;
        pLCtrl.moveSection();
        
        TM_PageLayoutField felPg = new  TM_PageLayoutField(pLCtrl);
        felPg.fieldApi = 'TM_TOMA__TestField__c';
        felPg.fieldLabel = 'TestField';
        felPg.helpText = 'Test';
        felPg.ViewFieldApi = 'TM_TOMA__TestField__c';
        felPg.isRequired =true;
        felPg.tabFieldSeq=1;
        felPg.secFieldSeq=1;
        felPg.addField();
        pLCtrl.getRecordTypeOptions();
        pLCtrl.save();
        TM_PageLayoutSection delSec = new TM_PageLayoutSection(pLCtrl);
        delSec.tabSequenceDel=1;
        delSec.secSequenceDel=1;  
        delSec.deleteSection();
        
        TM_PageLayoutTab delTeb = new TM_PageLayoutTab(pLCtrl);
        delTeb.tabNumberToDelete=2;
        delTeb.deleteTab();
        pLCtrl.save();
        System.assertEquals(1, [SELECT count() FROM CLM_Tab__c]);
    }
    
    
    static testMethod void testMethod_TM_PageLayoutController(){
        List<RecordType> rtypes = [Select Name, Id From RecordType where sObjectType='TM_TOMA__Contract_Vehicle__c' and isActive=true];
        String recId;
         if(rtypes.size()>0)
        {
           recId=(''+rtypes[0].Id).substring(0,15);
        }
        CLM_Tab__c tabSetting =TestDataGenerator.createCLMTab(recId);
        tabSetting.TabName__c='Internal Organisation';
        tabSetting.Is_Contract__c = true;
        insert tabSetting;  
        CLM_Tab__c tabSetting1 = TestDataGenerator.createCLMTab(recId);
        tabSetting1.Is_Contract__c = true;
        tabSetting1.Order__c = 2;
        tabSetting1.TabName__c='Contract Description';
        insert tabSetting1;   
        CLM_Section__c sectionSetting = TestDataGenerator.createCLMSection(recId);
        sectionSetting.Is_Contract__c = true;
        insert sectionSetting;
        CLM_Section__c sectionSetting1 = TestDataGenerator.createCLMSection(recId);
        sectionSetting1.Is_Contract__c = true;
        sectionSetting1.Order__c = 20;
        sectionSetting1.Is_Lightning_Component__c= true;
        sectionSetting1.Inline_Api_Name_Component_Api__c='TestComponent';
        insert sectionSetting1;
        CLM_Section__c sectionSetting2 = TestDataGenerator.createCLMSection(recId);
        sectionSetting2.Is_Contract__c = true;
        sectionSetting2.Order__c = 30;
        sectionSetting2.Is_InlineVF_Page__c= true;
        sectionSetting2.Inline_Api_Name_Component_Api__c='TestVFPage';
        insert sectionSetting2;
        CLM_FIeld__c fieldSetting = TestDataGenerator.createCLMField(recId);
        fieldSetting.Is_Contract__c = true;
        fieldSetting.ViewFieldApi__c='LastModifiedById';
        fieldSetting.Order__c = 1001;
        insert fieldSetting;
        CLM_FIeld__c fieldSetting1 = TestDataGenerator.createCLMField(recId);
        fieldSetting1.Is_Contract__c = true;
        fieldSetting1.FieldApiName__c = 'OwnerId';
        fieldSetting1.ViewFieldApi__c='OwnerId';
        fieldSetting1.Order__c = 1002;
        insert fieldSetting1;
        CLM_FIeld__c fieldSetting2 = TestDataGenerator.createCLMField(recId);
        fieldSetting2.Is_Contract__c = true;
        fieldSetting2.FieldApiName__c = 'RecordTypeId';
        fieldSetting2.ViewFieldApi__c='RecordTypeId';
        fieldSetting2.Order__c = 1003;
        insert fieldSetting2;
        CLM_AtaGlanceField__c newOppAtAGlance = TestDataGenerator.createAtaGlanceField(recId);
        newOppAtAGlance.Is_Contract__c=true;
        insert newOppAtAGlance;
        
        
        ///controllars initialisation
        TM_PageLayoutController pLCon = new TM_PageLayoutController();
        pLCon.recordTypeId=recId;
        TM_PageLayoutVFP vfpLayout = new TM_PageLayoutVFP(pLCon);
        TM_PageLayoutTab tabLayout = new TM_PageLayoutTab(pLCon);
        TM_PageLayoutSection secLayout = new TM_PageLayoutSection(pLCon);
        TM_PageLayoutField fieldLayout = new TM_PageLayoutField(pLCon);
        
       
       // pLCon.recordTypeId = (''+[SELECT Id FROM RecordType WHERE sobjecttype = 'TM_GovSuite__FedCap_Opportunity__c' and name = 'RFP' LIMIT 1].id).substring(0,15);
        vfpLayout.go();
        pLCon.getRecordTypeOptions();        
        
        
        System.assert(!String.isEmpty(pLCon.getFieldApis()));
        System.assert(!String.isEmpty(pLCon.getFieldLabels()));
        System.assert(pLCon.getFieldLabelApi().size() > 0);
        System.assert(pLCon.getRecordTypeOptions().size() > 0);
        
      
        Document document = new Document();
        document.AuthorId = UserInfo.getUserId();
        document.FolderId = [SELECT id FROM Folder WHERE DeveloperName = 'FedTom_Custom_setting_doc'].id;//DeveloperName = 'Fedcapture_Custom_Setting_Doc'
        //System.debug(document.FolderId);
        document.name = 'Custom Setting Doc1';
        document.DeveloperName = 'Custom_Setting_Doc1';
        document.Body = Blob.valueOf('Test Doc');
        insert document;
        //System.debug([Select Id,DeveloperName from Document WHERE DeveloperName=:'Custom_Setting_Doc1' AND Folder.Name = :'GovSuit_Images']);
        System.debug(document.Id);
        System.debug(document.DeveloperName);
        PageReference exportPageRef = pLCon.doExport();
        System.assertEquals('/apex/tm_toma__tm_custom_page_layout?recordTId='+pLCon.recordTypeId,exportPageRef.getUrl());
        PageReference clearPageRef = pLCon.doClear();
        System.assertEquals('/apex/tm_toma__tm_custom_page_layout?recordTId='+pLCon.recordTypeId,clearPageRef.getUrl());
        PageReference importPageRef = pLCon.doImport();
        System.assertEquals('/apex/tm_toma__tm_custom_page_layout?recordTId='+pLCon.recordTypeId,importPageRef.getUrl());
    }
    
}