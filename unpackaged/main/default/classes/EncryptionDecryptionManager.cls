public with sharing class EncryptionDecryptionManager {    
    Private static final String CRYPTO_KEY = 'DKkGa4wyOawtzA3C9T8SDmPtDL04j/cXwAA5qFPGApg=';//this is coded key 
    Private static final Map<String,String> CRYPTO_KEY_MAP = new Map<String,String>{'FedCapture' => 'lWOALrtcSQ7A4akNmOZyt0EEjbPQT8ioQq3oztqNOvc=', 
                                                                                    'FedTOM' => 'DKkGa4wyOawtzA3C9T8SDmPtDL04j/cXwAA5qFPGApg=',
                                                                                    'FedCLM' => 'UgBueu2kcvYfQwSc3geXr4hHID4YnzqmUsajwhxpTKI='}; 
    Public static Blob encryptValue(String value){        
        Blob data = Blob.valueOf(value);
        // Encrypt the data and have Salesforce.com generate the initialization vector
        Blob encryptedData = Crypto.encryptWithManagedIV('AES256',EncodingUtil.base64Decode(CRYPTO_KEY), data);       
        return encryptedData;//encryptedData.toString();
    }
    
    Public static String decryptValue(Blob encryptedData){        
        //Blob cryptoKey = Crypto.generateAesKey(256); 
        Blob decryptedData = Crypto.decryptWithManagedIV('AES256', EncodingUtil.base64Decode(CRYPTO_KEY), encryptedData);        
        // Decode the decrypted data for subsequent use
        String decryptedDataString = decryptedData.toString();       
        return decryptedDataString;
    }
    
    Public static String decryptValue(String packageName,String  encryptedDataString){       
        Blob encryptedData = EncodingUtil.base64Decode(encryptedDataString); 
        //Blob cryptoKey = Crypto.generateAesKey(256); 
        Blob decryptedData = Crypto.decryptWithManagedIV('AES256', EncodingUtil.base64Decode(CRYPTO_KEY_MAP.get(packageName)), encryptedData);        
        // Decode the decrypted data for subsequent use
        String decryptedDataString = decryptedData.toString();       
        return decryptedDataString;
    }
    
     public static KeyInfo getKeyInfo(String inputString){
        if(inputString != null && inputString.length() > 0){
            String[] dataArray = inputString.split('SPLIT');
            if(dataArray.size() == 5){
                KeyInfo keyInfo = new KeyInfo();
                if(dataArray[0].isNumeric()){
                    keyInfo.licenseCount = Integer.valueOf(dataArray[0]);
                }
                String startDateString = dataArray[1];
                Date startDate = getDate(startDateString );
                keyInfo.startDate = startDate ;
                
                String endDateString = dataArray[2];
                Date endDate = getDate(endDateString );
                keyInfo.endDate = endDate ;
                
                keyInfo.organizationId = dataArray[3];
                keyInfo.packageName = dataArray[4];
                
                return keyInfo;
            }
        }
        return null;
    }
    
    private static Date getDate(String inputString){
        if(inputString != null && inputString.length() > 0){
            String[] splitArray = inputString.split('/');
            if(splitArray.size() == 3){
                Date myDate = Date.newInstance(Integer.valueOf(splitArray[2]),Integer.valueOf(splitArray[0]),Integer.valueOf(splitArray[1]));
                return myDate;
            }
           
        }
        return null;
    }
    
    public class KeyInfo{
        public String organizationId{get; set;}
        public Date startDate{get; set;}
        public Date endDate{get; set;}
        public Integer licenseCount{get; set;}
        public String packageName{get; set;}
        
    } 
}