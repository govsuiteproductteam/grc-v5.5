public with sharing class TM_Task_Order_Nav_Controller {
    private Id toId;
    private String pageRefer;
    private String pageReferEdit;
    private String pageReferNew;    
    public String isValidLiecence{get;set;}
    
    public TM_Task_Order_Nav_Controller(ApexPages.StandardController controller){
        toId = ApexPages.CurrentPage().getparameters().get('id');
        
        Schema.DescribeSObjectResult r = TM_TOMA__Task_Order__c.sObjectType.getDescribe();
        
        String keyPrefix = r.getKeyPrefix();
        System.debug('keyPrefix = '+keyPrefix);
        //if(Schema.sObjectType.User.isAccessible()){ 
        //    User u = new User();
        //   u = [SELECT id, TM_TOMA__FedTOM_User__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        
        //   if(u.TM_TOMA__FedTOM_User__c == true)
        isValidLiecence = LincenseKeyHelper.checkLicensePermitionForFedTOM();
        Boolean isLightningPage = UserInfo.getUiThemeDisplayed() == 'Theme4d';
        System.debug('isLightningPage = '+isLightningPage);
        if(isValidLiecence == 'Yes')
        {
            if(toId!=Null){
                pageRefer = '/'+toId+'?nooverride=1';
                if(isLightningPage){
                    System.debug('isLightningPage = '+isLightningPage);
                    pageReferEdit = '/one/one.app?source=alohaHeader#/sObject/'+toId+'/edit';
                }else{
                     pageReferEdit = '/'+toId+'/e?retURL=/'+toId+'&nooverride=1'; 
                }                              
                System.debug('toid');
            }else{
                pageReferNew = '/'+keyPrefix+'/e?retURL=/'+keyPrefix+'/o&nooverride=1';
            }
        }
        else
        {	
            //pageRefer = '/apex/TM_TO_ViewButton_Nav';
            pageRefer = '/apex/TM_TOMA__TM_License_Required_Error_Notification';
            pageReferEdit = '/apex/TM_TOMA__TM_License_Required_Error_Notification';
            pageReferNew = '/apex/TM_TOMA__TM_License_Required_Error_Notification';
        }
        //}
    }
    
    public PageReference setRedirect(){
        System.debug('plain');
        PageReference pg = new Pagereference(pageRefer);
        pg.setRedirect(true);
        return pg;
    }
    
    public PageReference setRedirectEdit(){
        System.debug('edit');
        PageReference pg = new Pagereference(pageReferEdit);
        pg.setRedirect(true);
        return pg;
    }
    
    public PageReference setRedirectNew(){
        System.debug('new');
        PageReference pg = new Pagereference(pageReferNew);
        pg.setRedirect(true);
        return pg;
    }
}