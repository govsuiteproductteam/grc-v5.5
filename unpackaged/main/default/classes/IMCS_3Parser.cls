public class IMCS_3Parser {
    private String emailSubject='';
    //Private String name;
    Private String title;
    Private String emailId;
    //New Changes
    private String contactTitle = '';
    private Contact con;
    
    public void parse(Messaging.InboundEmail email){
        String htmlText = email.HTMLBody;
        String emailPlainText = email.plainTextBody;
        System.debug('\n***********Plain Text**********\n');
        //Old //parseContactDetails(emailPlainText);
        //New
        createContact(emailPlainText,email); 
        
        // retrieving Contact Details
        
       /*List<Task_Order__c> taskOrderRecords =  [Select name from Task_Order__c];
        for(Task_Order__c record:taskOrderRecords){
            System.debug('***'+record.Name);
            if(name.equals(record.Name)){
                Task_Order__c taskorder = new Task_Order__c();
            }
        }*/
        // End retrieving Contact Details   
        
        // retrieving Subject 
        emailSubject = email.subject;
        emailSubject = emailSubject.stripHtmlTags();
        emailSubject = checkValidString(emailSubject, 255);
        //End Subject
        
       
    }
    
   /* private void parseContactDetails(String emailPlainText){
        system.debug(emailPlainText);
        Integer index =  emailPlainText.indexOf('We look forward to receiving your proposal.');
        String ContactDetails = emailPlainText.substring(index);
         system.debug(ContactDetails);
        List<String> contactList = ContactDetails.split('\n');
        name = contactList.get(3);
        System.debug(contactList.get(3));
        title = contactList.get(4);
        System.debug(contactList.get(4));
        String emailStr = contactList.get(9);
        System.debug(contactList.get(9));
        emailId = emailStr.substringAfter('E-mail:  ').trim();
        System.debug('\n******email id = ' +emailId);
       // System.debug(contactList.get(4));
    }*/
    private String checkValidString(String str, Integer endLimit){
        if(str.length() > endLimit){
            String validString = str.substring(0,endLimit);
            return validString;
        }
        return str;
    }
    
    public Task_Order__c getTaskOrder(String toAddress){
            
            Task_Order__c taskOrder = new Task_Order__c();
            List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];
            System.debug('*********contract vechcle size  = '+ conVehicalList.size());
            
            if(!conVehicalList.isEmpty()){
                taskOrder.Contract_Vehicle__c = conVehicalList[0].Id;
                taskOrder.Task_Order_Contracting_Organization__c = conVehicalList[0].Account_Name__c;
                               
                System.debug('****email subject'+emailSubject);
                System.debug('****email subject length = '+emailSubject.length()); 
                taskOrder.Name =  checkValidString(emailSubject , 79);
                taskOrder.Task_Order_Title__c = emailSubject;
                taskOrder.Release_Date__c = Date.today();
                taskOrder.Contract_Vehicle_picklist__c = 'IMCS III - INFORMATION MANAGEMENT COMMUNICATIONS SERVICES III';
            
                //setting Contact details
                try{
                    if(con.id == null){
                        if(conVehicalList[0].Account_Name__c != null){
                            con.AccountId = conVehicalList[0].Account_Name__c;
                        }
                        DMLManager.insertAsUser(con); 
                    }
                        
                }catch(Exception e){
                    System.debug(''+e.getMessage()+'\t'+e.getLineNumber());
                }
                
                if(con.Id != null) {           
                    if(con.Title != null){
                        if(con.Title == 'Contract Specialist')
                            taskOrder.Contract_Specialist__c = con.Id;
                        if(con.Title == 'Contracting Officer')
                            taskOrder.Contracting_Officer__c = con.Id;
                    }
                }
                //List<Contact> contactList =  [Select id, Email from Contact WHERE Email = :emailId];
                //system.debug(contactList);
                //Contact contact;
               // if(!contactList.isEmpty()){
               //     contact = contactList.get(0);   
                //}else{
                //    String []salutation = new String[]{'Mr.','Ms.','Mrs.','Dr.','Prof.'}; 
                //    contact = new Contact();
//                    List<String> split =  name.split(' ', 2);
                
                  //  contact.FirstName = split.get(0);
                   // contact.LastName = split.get(1);   
                   // contact.Title = title;
                   // if(conVehicalList[0].Account_Name__c != null){
                    //    contact.AccountId = conVehicalList[0].Account_Name__c;
                    //}
                    //contact.Email = emailId;
                   // insert contact;
               // }
                //if(title.equals('Contract Specialist')){
                //    taskOrder.Contract_Specialist__c = con.id;
                //}
                //if(title.equals('Contracting Officer')){
                ////    taskOrder.Contracting_Officer__c = con.id;
               // }
                //End Setting contact details
            }
        
            return taskOrder;
        }
    
    
    private void createContact(String emailBody,Messaging.InboundEmail email){
        try{
            con = new Contact();            
            List<Contact> contactList = [Select id, Email, Title from Contact WHERE Email = :email.fromAddress];
            if(contactList != null && !contactList.isEmpty() ){//for existing contact
                con = contactList[0]; 
            }else{//for new contact
                String emailFromName = email.fromName;        
                String[] nameSplited = emailFromName.split(' ');
                con.FirstName = nameSplited[0];
                con.LastName = nameSplited[1];
                con.Email =  email.fromAddress;
                String contactRawString = emailBody.substringAfter('We look forward to receiving your proposal.');
                System.debug('Raw String = '+contactRawString);
                
                if(contactRawString.trim().contains('Contract Specialist')){
                    contactTitle = 'Contract Specialist';
                }else if(contactRawString.trim().contains('Contracting Officer')){
                    contactTitle = 'Contracting Officer';
                }
                System.debug('contactTitle=='+contactTitle);
                con.Title = contactTitle.trim(); 
            }                                                                                  
        }catch(Exception ex){
            System.debug('Create Contact Error ='+ex.getMessage()+'\tLine Number = '+ex.getLineNumber());
        }                
    }

}