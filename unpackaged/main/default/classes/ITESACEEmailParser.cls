public class ITESACEEmailParser implements IEmailParser {
    
    private String emailSubject;
    public String taskOrderNumber;
    private String taskOrderTitle;
    private String agency;
    private String phone;
    private String firstName;
    private String lastName;
    //private String emailId;
    private Contact con;
    
    public void parse(Messaging.InboundEmail email){       
        emailSubject = email.subject.trim();
        if(emailSubject.length() == 0){
            throw new ContractVehicleNotFoundException(MessageConstantController.TASK_ORDER_NOT_FOUND);
        }
        
        try{
            if(emailSubject.startsWith('Fwd:'))
                emailSubject = emailSubject.replace('Fwd:', '');
            if(emailSubject.startsWith('Re:'))
                emailSubject = emailSubject.replace('Re:', '');
        }catch(Exception ex){
            System.debug('Fwd Re Error : '+ex.getMessage()+'\t: '+ex.getLineNumber());
        }
        
        //emailId = email.fromAddress;
        
        try{
            if(emailSubject.contains('[') && emailSubject.contains(']')){
                taskOrderNumber = emailSubject.substringBetween('[',']').trim();
            }   
        }catch(Exception ex){
            System.debug('Task Order Number Error : '+ex.getMessage()+'\t: '+ex.getLineNumber());
        }
        
        
        String emailBody = email.plainTextBody;
        emailBody = emailBody.stripHtmlTags();
        emailBody = emailBody.replace('*','').trim();
        //String emailBody = email.htmlBody.stripHtmlTags().trim();
        try{
            taskOrderTitle = emailBody.substringBetween('The order for request, '+taskOrderNumber,'has been approved').trim();
            if(taskOrderTitle.startsWith('-')){
                taskOrderTitle = taskOrderTitle.replace('-','').trim();
            }            
        }catch(Exception ex){
            System.debug('Task Order Title Error : '+ex.getMessage()+'\t: '+ex.getLineNumber());
        }
        
        
        
        parseRawText(emailBody);
        createContact(firstName,lastName,phone);
        
        
        // Print Start
        System.debug('Task Order Number = '+taskOrderNumber);
        System.debug('Task Order Title = '+taskOrderTitle);
        System.debug('Agency = '+agency);
        System.debug('Phone = '+phone);
        System.debug('First Name = '+firstName);
        System.debug('Last Name = '+lastName);
        //System.debug('Email Id = '+emailId);
        // Print End
        
    }
    
    public Task_Order__c getTaskOrder(String toAddress){
        if(taskOrderNumber==null){
            return null;
        }  
        
        Task_Order__c taskOrder = new Task_Order__c();
        List<Contract_Vehicle__c> conVehicalList = [SELECT Id FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];
        if(!conVehicalList.isEmpty()){
            taskOrder.Contract_Vehicle__c = conVehicalList[0].Id; // Label = Contract Vehicle
            taskOrder.Name = taskOrderNumber.trim(); //Task Order Number
            
            if(taskOrderTitle !=null)           
                taskOrder.Task_Order_Title__c = taskOrderTitle.trim();// Task Order Title
            
            if(agency !=null)           
                taskOrder.Agency__c = agency.trim(); // Agency
            
            taskOrder.Release_Date__c = Date.today();//RFP/TO Release Date
            
            
            try{               
                if(con.Id == null)
                    DMLManager.insertAsUser(con); 
                
            }catch(Exception e){
                System.debug('Error in contact - '+e.getMessage()+':'+e.getLineNumber());
            }
            
            if(con.Id !=null){
                taskOrder.Contracting_Officer__c = con.Id;
            }                        
            return taskOrder;
        }else{
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
        }                         
    }
    
    public Task_Order__c updateTaskOrder(Task_Order__c taskOrder, String toAddress){
        List<Contract_Vehicle__c> conVehicalList = [SELECT Id FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress]; 
        if(!conVehicalList.isEmpty()){
            taskOrder.Contract_Vehicle__c = conVehicalList[0].Id; // Label = Contract Vehicle
            //taskOrder.Name = taskOrderNumber.trim(); //Task Order Number
            
            if(taskOrderTitle !=null)           
                taskOrder.Task_Order_Title__c = taskOrderTitle.trim();// Task Order Title
            
            if(agency !=null)           
                taskOrder.Agency__c = agency.trim(); // Agency 
            
            
            try{
                if(taskOrder.Customer_Agency__c != null){
                    con.AccountId = taskOrder.Customer_Agency__c;
                    //DMLManager.upsertAsUser(con);
                    if(con.Id == null){
                        // if Task Order with Customer Organization is not having a Contact or if a different Contact(i.e. a Contact with different Email Id) is found
                        //System.debug('&&& Inside new contact &&&');
                        //DMLManager.insertAsUser(con);
                        insert con;
                    }else{
                        // if Task Order with Customer Organization is having a Contact, then update that Contact
                        //System.debug('&&& Inside update contact &&&');
                        //DMLManager.updateAsUser(con);
                        update con;
                    }
                }else{
                    if(con.id ==null)
                        DMLManager.insertAsUser(con);                    
                }                
                taskOrder.Contracting_Officer__c = con.Id; 
            }catch(Exception e){
                System.debug('Error in contact creation - '+e.getMessage()+':'+e.getLineNumber());
            }                                    
            return taskOrder;
        }else{
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
        }    
    }
    
    
    private void parseRawText(String emailBody){
        try{
            String rawText = emailBody.substringBetween('Any questions should be directed to','Note:').trim();
            rawText = rawText.stripHtmlTags();
            System.debug('Raw Text = '+rawText);
            String rawPhoneText = rawText.substringAfter('at').trim();  
            rawPhoneText = rawPhoneText.remove('.'); // remove dot
            phone = rawPhoneText.trim();                                    
                
            String rawtext1 = rawText.substringBefore('at');
            System.debug('Raw Text 1 = '+rawText1);
            
            
            if(rawtext1.contains(',')){
                rawtext1 = rawtext1.remove(',');
                System.debug('Raw Text 1 = '+rawText1);                 
            }
            
            agency = '';
            String tempStr='';
            String[] splitedArray = rawtext1.split(' ');
            if(splitedArray.size()>0 && splitedArray!=null){                                        
                lastName = splitedArray[0];
                try{
                    firstName = splitedArray[1];                    
                    //agency  = rawtext1.substring(rawtext1.indexOf(splitedArray[3]),splitedArray.size()-1);
                    //agency = rawText.substringBetween(lastName+', '+firstName,'at');
                    for(Integer i=2;i<splitedArray.size();i++){
                       tempStr = splitedArray[i];
                        if(tempStr.length()>1){
                            agency +=splitedArray[i]+' ';    // Task Order Agency Field logic
                        }
                    } 
                    agency.trim();
                }catch(Exception ex){
                    System.debug('Agency Parse Error : '+ex.getMessage()+'\t: '+ex.getLineNumber());
                }  
            }                
        }catch(Exception ex){
            System.debug('Parse Raw Data Error : '+ex.getMessage()+'\t: '+ex.getLineNumber());
        }                                          
        
    }
    
    
    
    private void createContact(String fName,String lName,String phone){
        try{
            List<Contact> contactList = new List<Contact>();
            //System.debug('email='+email); 
            System.debug('phone='+phone);
            if(fName != null && fName != '' && lName != null && lName != '' && phone != null && phone != ''){
                 // To remove the url part, everything including the "<" and ">"
                /* Ex. 7 <973-724-6034>03-340-1308, 
				remove "<973-724-6034>" from the above Phone Number*/                
                //phone = phone.replaceAll('\\<[^\\<\\>]*[^\\>\\<]*\\>', '').replaceAll('\\s+',''); 
                //contactList = [SELECT Id,firstName,lastName,phone From Contact WHERE firstName=:fName.trim() AND lastName=:lName.trim() AND phone=:phone.trim()];
                String searchTerms = '("' + firstName +'" AND "'+lastName+'" AND "'+phone+'")';
                List<List<Contact>> conList= [FIND :searchTerms RETURNING  Contact(Id,firstName,lastName,phone)];
                if(!conList.isEmpty()){
                    contactList = conList.get(0);
                }
            }
            
            if(contactList.isEmpty()){
                con = new Contact();               
                con.FirstName = fName.trim();
                con.lastName = lName.trim();                               
                //con.Email = email;
                con.Phone = phone;
                con.Title = 'Contracting Officer';
                con.Is_FedTom_Contact__c = true;                              
            }
            else{
                System.debug('*** Inside else ***');
                con = contactList[0];
            }
        }catch(Exception ex){
            System.debug('Create Contact Error : '+ex.getMessage()+'\t: '+ex.getLineNumber());
        }       
    }
    
    
    
}