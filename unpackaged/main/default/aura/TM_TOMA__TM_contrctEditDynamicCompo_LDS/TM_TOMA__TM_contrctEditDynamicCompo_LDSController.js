({
    doInit : function(component, event, helper) {
        console.log("inside pr");
         component.set("v.showSpinner",true);
        var action = component.get("c.getUserAccesibility");
        
        action.setCallback(this,function(response){
            var status = response.getState();
            console.log("getUserAccesibility > "+status);
            if(status ==="SUCCESS"){
                var accessStatus ;
                accessStatus = response.getReturnValue();
                console.log("accessStatus > "+accessStatus);
                component.set("v.AccessStatus",accessStatus);
                if(accessStatus === 'Yes'){
                    helper.helperDoInit(component, event,helper);
                    // helper.getTabWrapperData(component, event);
                }else{
                    component.set("v.showSpinner",false);
                }
                
            }
        });
        
        $A.enqueueAction(action);  
    },
    tabPress: function(component, event, helper) {
        //helper.helperDoInit(component, event);
        component.set("v.showSpinner",true);
        helper.handleTabPress(component, event);
    },
    /*onPencilClick: function(component, event, helper) {
        //component.set("v.editFormRefreshFlag",false);
        // component.set("v.editFormRefreshFlag",true);
        component.set("v.isEditMode",true);
    },*/
    saveData: function(component, event, helper){
        var tempForm = component.find("CLM_EditForm");
        var previousErrorMsgs =document.getElementsByClassName("errormsgcls");
        var previousErrorsOnTabs = document.getElementsByClassName("tab_erroredit_div");
        if(previousErrorMsgs != null){
            for(var i=0;i<previousErrorMsgs.length;i++){
                previousErrorMsgs[i].style.display='none';
            }
        }
        if(previousErrorsOnTabs != null){
            for(var i=0;i<previousErrorsOnTabs.length;i++){
                previousErrorsOnTabs[i].style.display='none';
            }
        }
        
        var reqFieldsData = document.getElementsByClassName("customRequiredDiv");
        if(reqFieldsData != null && reqFieldsData.length>0){
            var formData = event.getParam('fields');
            var validationErr = false;
            for(var i=0;i<reqFieldsData.length;i++){
                var reqFld =reqFieldsData[i].dataset.record;
                if(reqFld != null && reqFld != undefined){
                    if(formData.hasOwnProperty(reqFld)){
                        if(formData[reqFld]== null || formData[reqFld]== undefined || formData[reqFld]== ''){
                            var showErrOnFld = document.getElementById('erroredit-'+reqFld);
                            if(showErrOnFld != null && showErrOnFld != undefined){
                                console.log("REQUIREDFIELDERR - "+reqFld);
                                showErrOnFld.style.display='';
                                var parentTabName = showErrOnFld.dataset.record;
                                if(parentTabName!= null){
                                    var showErrOnTab = document.getElementById('tab_erroredit_icon_'+parentTabName);
                                    if(showErrOnTab != null)
                                        showErrOnTab.style.display='';
                                }
                                validationErr = true;
                            }
                            
                        }
                    }
                }
            }
            if(!validationErr)
                component.set("v.isSubmitAction",true);
            
            component.set("v.isValidationError",validationErr);
            
        }
        
        // helper.handleSaveData(component, event);
    },
    cancelAction: function(component, event, helper){
        event.preventDefault();
        // component.set("v.isEditMode",false);
        // component.set("v.upperTabRefreshFlag",false);
        //component.set("v.upperTabRefreshFlag",true);
    //    window.open('/'+component.get('v.recordId'),'_top');//newlly added 29-08-2018
      //window.open('/'+component.get('v.recordId'),'_top');
        window.open('/lightning/r/TM_TOMA__Contract_Vehicle__c/'+component.get('v.recordId')+'/view','_top');
       /* var navigateEvent = $A.get("e.force:navigateToSObject");
        navigateEvent.setParams({ "recordId": component.get('v.recordId') });
        navigateEvent.fire();*/
    },
    handleRecLoad: function(component, event, helper){
        console.log('RECORD Loaded');
        component.set("v.showSpinner",false);
        component.set("v.myChangeCall",false);
         var isClonePage = component.get("v.isClonePage");
        //alert(isClonePage);
        if( isClonePage == true ){
            var uniqueFieldArr = [];
            
            var allTabData = component.get("v.TabDataListSorted");
            //console.log('&*&&*&**& allTabData '+JSON.stringify(allTabData));
            for(var i in allTabData )
            {
                var secList = allTabData[i].sectionWrapList;
                for(var k in secList)
                {
                    var fieldList = secList[k].fieldWrapList
                    for(var l in fieldList)
                    {
                        if(fieldList[l].isUnique == true){
                            var field = fieldList[l].field;
                            //alert('&*&&*&**& '+JSON.stringify(field));
                            for(var a in field){
                                uniqueFieldArr.push(field.TM_TOMA__FieldApiName__c);
                                console.log('field[a].FieldApiName__c '+field[a].TM_TOMA__FieldApiName__c);
                                
                            }
                        }
                    }
                }
            }
            //alert('&*&&*&**& ARR'+JSON.stringify(uniqueFieldArr));
            var requiredFldsData = component.find("myId");
            for(var cmp in requiredFldsData){
                var fldApiName=requiredFldsData[cmp].get("v.fieldName");
                if(uniqueFieldArr.includes(fldApiName)){
                    
                    //console.log('&&&&&&&&&&&&&&&&&&&&&'+fldApiName);
                    requiredFldsData[cmp].set("v.value",'');
                }
                
            }
            
        }
        // var eventFields = event.getParam("recordUi").record.fields;
        // var errorParamsJSON = JSON.stringify(eventFields);
        //  console.log(errorParamsJSON);
        
    },
    handleRecError: function(component, event, helper){
        component.set("v.showSpinner",false);
        if(component.get("v.isClonePage")){
            component.set("v.recordId",component.get("v.pageReference").state.recordId);
        }
        var payload;
        var errorMetaData = event.getParams(); //newly removed @ 22/05/2019 event.getParams().error
        console.log(JSON.stringify(event.getParams()));
        
         //newlly added 23-10-2018
        var errorDataNew = event.getParams();
        var errorCode = '';
        var errorMessage = '';
        
        var isclone=component.get("v.isClonePage");//02-11-2018 for clone issue
        if(errorDataNew["output"]["fieldErrors"] !=null && errorDataNew["output"]["fieldErrors"] !=undefined && errorDataNew["output"]["fieldErrors"] !=''){
            for(var cmp in component.find("myId")){
                var compData = component.find("myId")[cmp];
                var output = ''+compData.get("v.fieldName");
                if(JSON.stringify(event.getParams()).includes(''+output)){
                    errorCode = errorDataNew["output"]["fieldErrors"][output][0]["errorCode"];
                    errorMessage = errorDataNew["output"]["fieldErrors"][output][0]["message"];
                    //alert("fieldErrors *** "+fieldErrors);
                    var showErrOnFld = document.getElementById('erroredit-'+output);
                    
                    
                    if(showErrOnFld != null && showErrOnFld != undefined){
                        //var  showErrOnFldspan = document.getElementById('Servererror-'+output);
                        //showErrOnFldspan.innerHTML = "<b>Field ERROR:&nbsp;</b>"+errorMessage;
                       // showErrOnFld.style.display='';
                        var parentTabName = showErrOnFld.dataset.record;
                        // var fldLabel = showErrOnFld.dataset.fldlabel;
                        // vaidationErrorList.push(fldLabel);
                        if(parentTabName!= null){
                            var showErrOnTab = document.getElementById('tab_erroredit_icon_'+parentTabName);
                            if(showErrOnTab != null)
                                showErrOnTab.style.display='';
                        }
                    }
                }
                
                
                //newlly added code related to clone issue 02-11-2018
                if(isclone)
                {
                    console.log('output '+output+' value '+compData.get("v.value"));
                    compData.set("v.value",compData.get("v.value"));
                }
                //End of code
            }
        }
         // End of code 23-10-2018
        console.log('ERROR  '+JSON.stringify(errorMetaData));
        if(errorMetaData!= null && errorMetaData!=undefined){
            var errorData = errorMetaData; //newly removed @ 22/05/2019 errorMetaData.data
            var fieldLimitExceeded = errorMetaData.errorCode;
            if(errorData!= null && errorData !=undefined){
                payload = errorData.output.fieldErrors;
            }else if(fieldLimitExceeded!= null && fieldLimitExceeded != undefined && fieldLimitExceeded=="QUERY_TOO_COMPLICATED"){
                 var errdiv = component.find("errmsgdiv");
            $A.util.removeClass(errdiv,"slds-hide");
               
                    console.log("success - with fields limit exceeded on page layout");
                    console.log("isSubmitAction"+component.get("v.isSubmitAction"));
            }else if(JSON.stringify(event.getParams()).includes('DUPLICATE_VALUE')){//newlly added 19-10-18
                var errdiv = component.find("errmsgdiv");
                $A.util.removeClass(errdiv,"slds-hide");
                component.set("v.isSubmitAction",false);
                console.log("Duplicate Unique Field value Found");
                console.log("isSubmitAction"+component.get("v.isSubmitAction"));
            }else if(JSON.stringify(event.getParams()).includes('NUMBER_OUTSIDE_VALID_RANGE')){//newlly added 19-10-18
                var errdiv = component.find("errmsgdiv");
                $A.util.removeClass(errdiv,"slds-hide");
                component.set("v.isSubmitAction",false);
                console.log("NUMBER_OUTSIDE_VALID_RANGE");
                console.log("isSubmitAction"+component.get("v.isSubmitAction"));
            }
            else if(JSON.stringify(event.getParams()).includes('FIELD_CUSTOM_VALIDATION_EXCEPTION')){//newlly added 19-10-18
                var errdiv = component.find("errmsgdiv");
                $A.util.removeClass(errdiv,"slds-hide");
                component.set("v.isSubmitAction",false);
                console.log("FIELD_CUSTOM_VALIDATION_EXCEPTION");
                console.log("isSubmitAction"+component.get("v.isSubmitAction"));
            }else if(errorCode !=''){
               
                component.set("v.isSubmitAction",false);
                console.log("errorCode "+errorCode);
                console.log("isSubmitAction"+component.get("v.isSubmitAction"));
                
                
            }
            
            
            
            if(payload!= null && payload != undefined){
                console.log("Hello from payload"+JSON.stringify(payload));
                //{"Pursuit_Budget__c":[{"constituentField":null,"duplicateRecordError":null,"errorCode":"REQUIRED_FIELD_MISSING","field":"Pursuit_Budget__c","fieldLabel":"Pursuit Budget Estimate","message":"Required fields are missing: [Pursuit_Budget__c]"}]}
                var obj;
                try{
                    obj = JSON.parse(JSON.stringify(payload));
                }catch(err){
                    console.log("EXCEPTION OCCURED"+err);
                }
                if(obj != null && obj !=undefined){	
                    var errorFieldName='';
                    var errorFieldMetaData='';
                    Object.keys(obj).forEach(function(key){
                        errorFieldMetaData = obj[key];
                        errorFieldName = key;
                    });
                    if(errorFieldMetaData != null && errorFieldMetaData != undefined && errorFieldMetaData !=''){
                        component.find('notificationCenter').showNotice({
                            "variant": "error",
                            "title":"Error",
                            "header": "Something has gone wrong!",
                            "message": errorFieldMetaData[0]["message"]
                        });
                    }
                    
                    var errdiv = component.find("errmsgdiv");
                    $A.util.removeClass(errdiv,"slds-hide");
                    // console.log(JSON.stringify(errorMessages));
                    //console.log(JSON.stringify(errorFieldMetaData[0]["message"]));
                }
            }else{
                console.log("success");
                if(component.get("v.isSubmitAction"))
                    helper.helperHandleRecSuccess(component, event);
            }
            
        }else{
            console.log("success2");
            if(component.get("v.isSubmitAction"))
                helper.helperHandleRecSuccess(component, event);
        }
    },
    handleRecSuccess: function(component, event, helper){
        helper.helperHandleRecSuccess(component, event);
    },
    callFieldChangeDepandancy: function(component, event, helper){
        //alert('callFieldChange');
        var originalVal = event.getSource().get("v.value");
        var fieldnam = event.getSource().get("v.fieldName");
        var fieldclass = event.getSource().get("v.class");
        var fieldfieldtypemap = component.get("v.FieldVSFieldTypeMap");
        var fielddepandancymap = component.get("v.FieldVSDepandancyMap");
        console.log('originalVal  '+originalVal);
        console.log('fieldnam   '+fieldnam);
        for (var i in fieldfieldtypemap){
            fieldfieldtypemap[i];
        }
        for (var i in fielddepandancymap){
            fielddepandancymap[i];
        }
        var typeOfField = fieldfieldtypemap.get(fieldnam);
        var fildepandancy = fielddepandancymap.get(fieldnam);
        console.log('typeOfField '+typeOfField);
        console.log('fildepandancy '+fildepandancy);
        if(fildepandancy != ''){
            var fieldDependancyArray = fildepandancy.split('SPLITREC');
            console.log('fieldDependancyArray '+fieldDependancyArray );
            var processApiName = '';
            for(var index=0;index < fieldDependancyArray.length;index++){
                console.log('fieldDependancyArray[index] '+fieldDependancyArray[index] );
                if(fieldDependancyArray[index].trim() != ''){
                    var dataRec = fieldDependancyArray[index];
                    var dataRecArray = dataRec.split('SPLITVAL');
                    if(dataRecArray.length == 3){
                        console.log('@@@ '+dataRecArray [0]+' '+dataRecArray [1]+' '+dataRecArray [2]);
                        if(!processApiName.includes(dataRecArray [2]) ){
                            var res;
                            var expectingVal = dataRecArray [0];
                            if(typeOfField == "MULTIPICKLIST"){
                                if(expectingVal.trim().includes(';'))
                                {
                                    console.log('@@@ inside if MULTIPICKLIST ');
                                    var loopList = expectingVal.split(';');
                                    console.log('@@@  '+loopList);
                                    console.log('@@@  '+loopList.length);
                                    res = false;
                                    for(var i=0;i<(loopList.length-1);i++){
                                        if(originalVal.trim().includes(loopList[i].trim()))
                                        {
                                            res = true;
                                            break;
                                        }
                                    }
                                }
                                else{
                                    //old
                                    console.log('@@@ inside else MULTIPICKLIST ');
                                    res = originalVal.trim().includes(expectingVal.trim());
                                }
                            }
                            else{
                                res = originalVal.trim()==expectingVal.trim();
                            }
                            console.log(res);
                            if(res){
                                var className = dataRecArray [1];
                                for(var cmp in component.find("myId")){
                                    var compData = component.find("myId")[cmp];
                                    var output = compData.get("v.class");//component.find("myId")[cmp].get("v.class");
                                    console.log('true'+output);
                                    console.log('true'+className);
                                    output = output.trim();
                                    className = className.trim();
                                    
                                    console.log(output +'  '+className+'  '+output.search(className) != -1);
                                    if(output.search(className) != -1 && output != '' && className != ''){
                                        console.log('if '+className+' '+output);
                                        console.log('true if '+className);
                                        //new code to make lookup field disebled
                                        var tempfieldName = compData.get("v.fieldName");
                                        // var getelementdata = document.getElementById(tempfieldName);
                                        //showErrOnFld.style.display='';
                                        var getfieldType = fieldfieldtypemap.get(tempfieldName);
                                        if(getfieldType == 'REFERENCE' || getfieldType == 'TEXTAREA')
                                        {
                                            var geteledata = document.getElementById(tempfieldName+'div');
                                            geteledata.classList.remove("hidelookupdiv");
                                        }
                                        
                                        var fildepandancyOfChield = fielddepandancymap.get(tempfieldName);
                                        var fildepandancyOfChieldvalue = compData.get("v.value");
                                        if(fildepandancyOfChield != '')
                                        {
                                            helper.calldepandantfieldDepandacy(component, event,helper,fildepandancyOfChieldvalue,tempfieldName);
                                        }
                                        compData.set("v.disabled",false);
                                        break;
                                    }
                                    
                                }
                            }
                            else{
                                var className = dataRecArray [1];
                                for(var cmp in component.find("myId")){
                                    var compData = component.find("myId")[cmp];
                                    var output = ''+compData.get("v.class");//component.find("myId")[cmp].get("v.class");
                                    console.log(output);
                                    console.log(className);
                                    output = output.trim();
                                    className = className.trim();
                                    console.log(output +'  '+className+'  '+output.search(className) != -1);
                                    if(output.search(className) != -1 && output != '' && className != ''){
                                        console.log('if '+className+' '+output);
                                        //newlly added 06-08-2018
                                        var tempfieldName = compData.get("v.fieldName");
                                        console.log('tempfieldName '+tempfieldName);
                                        //new code to make lookup field disebled
                                        //var getelementdata = document.getElementById(tempfieldName);
                                        //showErrOnFld.style.display='';
                                        //var getfieldType = getelementdata.dataset.record;
                                        var getfieldType = fieldfieldtypemap.get(tempfieldName);
                                        if(getfieldType == 'REFERENCE' || getfieldType == 'TEXTAREA')
                                        {
                                            var geteledata = document.getElementById(tempfieldName+'div');
                                            geteledata.classList.add("hidelookupdiv");
                                            //compData.set("v.value",'');
                                        }
                                        /*  else if(getfieldType == 'PICKLIST')//code on 8-8-18 if we give blank value to picklist field then it replace selacted value to blank if selected value is 'Prime' and we use compData.set("v.value",''); then after when we select prime it will give blank value in onchange
                                        {
                                            var geteledata = document.getElementById(tempfieldName+'div');
                                            geteledata.classList.remove("hidelookupdiv");
                                          //  compData.set("v.value",'');
                                        }
                                            else{
                                                compData.set("v.value",'');
                                            }*/
                                        if(document.getElementById(tempfieldName) != null){//11-12-2018
                                        document.getElementById(tempfieldName).click();
                                        }
                                        //compData.change();
                                        //$A.util.addClass(compData, 'depandancydisabled');
                                        compData.set("v.disabled",true);
                                        //setTimeout(function(){ alert('hi');compData.set("v.disabled",false); alert('hi2');}, 3000);
                                        //break;
                                    }
                                    
                                }
                            }
                            
                        }
                        
                        //if(resDepend )
                        //break;
                    }
                }
                
            }
            
        }
        
        // console.log(fieldfieldtypemap.get(fieldnam));
        // console.log(fielddepandancymap.get(fieldnam));
    },
    callProxyFieldChangeDepandancy: function(component, event, helper){
        //alert('callProxyFieldChangeDepandancy');
        var fieldfieldtypemap = component.get("v.FieldVSFieldTypeMap");
        var fielddepandancymap = component.get("v.FieldVSDepandancyMap");
        var selectedItem = event.currentTarget;
        var fieldnam = selectedItem.id;
        //var fieldnam = event.getSource().getLocalId()
        // alert('fieldnam'+fieldnam);
        var originalVal ='';
        var typeOfField = fieldfieldtypemap.get(fieldnam);
        var fildepandancy = fielddepandancymap.get(fieldnam);
        //  alert('typeOfField '+typeOfField);
        
        if(fildepandancy != ''){
            // alert('fildepandancy '+fildepandancy);
            var fieldDependancyArray = fildepandancy.split('SPLITREC');
            // alert('fieldDependancyArray '+fieldDependancyArray );
            var processApiName = '';
            for(var index=0;index < fieldDependancyArray.length;index++){
                
                console.log('fieldDependancyArray[index] '+fieldDependancyArray[index] );
                if(fieldDependancyArray[index].trim() != ''){
                    var dataRec = fieldDependancyArray[index];
                    // alert('dataRec'+dataRec);
                    var dataRecArray = dataRec.split('SPLITVAL');
                    //alert('dataRecArray'+dataRecArray.length);
                    if(dataRecArray.length == 3){
                        //alert('@@@ inside '+dataRecArray [0]+' '+dataRecArray [1]+' '+dataRecArray [2]);
                        // alert('@@@ '+processApiName.includes(dataRecArray [2]));
                        if(!processApiName.includes(dataRecArray [2]) ){
                            var res;
                            var expectingVal = dataRecArray [0];
                            //   alert('@@@ '+expectingVal+'  '+typeOfField);
                            if(typeOfField == "MULTIPICKLIST"){
                                if(expectingVal.trim().includes(';'))
                                {
                                    console.log('@@@ inside if MULTIPICKLIST ');
                                    var loopList = expectingVal.split(';');
                                    console.log('@@@  '+loopList);
                                    console.log('@@@  '+loopList.length);
                                    res = false;
                                    for(var i=0;i<(loopList.length-1);i++){
                                        if(originalVal.trim().includes(loopList[i].trim()))
                                        {
                                            res = true;
                                            break;
                                        }
                                    }
                                }
                                else{
                                    //  alert('@@@ inside else MULTIPICKLIST ');
                                    res = originalVal.trim().includes(expectingVal.trim());
                                    
                                }
                            }
                            else{
                                res = originalVal.trim()==expectingVal.trim();
                            }
                            console.log('res '+res);
                            if(res){
                                var className = dataRecArray [1];
                                for(var cmp in component.find("myId")){
                                    var compData = component.find("myId")[cmp];
                                    var output = compData.get("v.class");//component.find("myId")[cmp].get("v.class");
                                    console.log('true'+output);
                                    console.log('true'+className);
                                    //if(output == className){
                                    if(output.search(className) != -1 && output != '' && className != ''){
                                        // alert('true if '+className);
                                        //new code to make lookup field disebled
                                        var tempfieldName = compData.get("v.fieldName");
                                        //var getelementdata = document.getElementById(tempfieldName);
                                        //showErrOnFld.style.display='';
                                        // var getfieldType = getelementdata.dataset.record;
                                        var getfieldType = fieldfieldtypemap.get(tempfieldName);
                                        console.log('tempfieldName '+tempfieldName+'getfieldType '+getfieldType+'tempfieldName '+tempfieldName);
                                        if(getfieldType == 'REFERENCE' || getfieldType == 'TEXTAREA')
                                        {
                                            // alert(tempfieldName+'div');
                                            var geteledata = document.getElementById(tempfieldName+'div');
                                            geteledata.classList.remove("hidelookupdiv");
                                        }
                                        var fildepandancyOfChield = fielddepandancymap.get(tempfieldName);
                                        var fildepandancyOfChieldvalue = compData.get("v.value");
                                        if(fildepandancyOfChield != '')
                                        {
                                            helper.calldepandantfieldDepandacy(component, event,helper,fildepandancyOfChieldvalue,tempfieldName);
                                        }
                                        compData.set("v.disabled",false);
                                        //  compData.set("v.value",'');
                                        break;
                                    }
                                    
                                }
                            }
                            else{
                                //alert('@@@@res else ');
                                var className = dataRecArray [1];
                                
                                for(var cmp in component.find("myId")){
                                    var compData = component.find("myId")[cmp];
                                    var output = compData.get("v.class");//component.find("myId")[cmp].get("v.class");
                                    console.log('@@@@ output '+output+' className '+className);
                                    //if(output == className){
                                    if(output.search(className) != -1 && output != '' && className != ''){
                                        //compData.set("v.value",'');
                                        //alert('if');
                                        var tempfieldName = compData.get("v.fieldName");
                                        //new code to make lookup field disebled
                                        //var getelementdata = document.getElementById(tempfieldName);
                                        //showErrOnFld.style.display='';
                                        //var getfieldType = getelementdata.dataset.record;
                                        var getfieldType = fieldfieldtypemap.get(tempfieldName);
                                        console.log('tempfieldName '+tempfieldName+'getfieldType '+getfieldType+'tempfieldName '+tempfieldName);
                                        if(getfieldType == 'REFERENCE' || getfieldType == 'TEXTAREA')
                                        {
                                            var geteledata = document.getElementById(tempfieldName+'div');
                                            geteledata.classList.add("hidelookupdiv");
                                            // compData.set("v.value",'');
                                        }
                                        /*   else if(getfieldType == 'PICKLIST')//code on 8-8-18 if we give blank value to picklist field then it replace selacted value to blank if selected value is 'Prime' and we use compData.set("v.value",''); then after when we select prime it will give blank value in onchange
                                        {
                                            var geteledata = document.getElementById(tempfieldName+'div');
                                            geteledata.classList.remove("hidelookupdiv");
                                        }
                                            else{
                                                compData.set("v.value",'');
                                            }*/
                                       console.log('tempfieldName '+tempfieldName);
                                        if(document.getElementById(tempfieldName) != null){//11-12-2018
                                       document.getElementById(tempfieldName).click();
                                        }
                                       compData.set("v.disabled",true);
                                   }
                                    
                                }
                            }
                            
                        }
                    }
                }
                
            }
            
        }
    },
    callMyActiveCallback: function(component, event, helper){
        var requiredFldsData = component.find("myId");
        var validationErr = false;
        if(requiredFldsData != undefined && requiredFldsData != null){
            var previousErrorMsgs =document.getElementsByClassName("errormsgcls");
            var previousErrorsOnTabs = document.getElementsByClassName("tab_erroredit_div");
            if(previousErrorMsgs != null){
                for(var i=0;i<previousErrorMsgs.length;i++){
                    previousErrorMsgs[i].style.display='none';
                }
            }
            if(previousErrorsOnTabs != null){
                for(var i=0;i<previousErrorsOnTabs.length;i++){
                    previousErrorsOnTabs[i].style.display='none';
                }
            }
            //var vaidationErrorList = [];
            for(var cmp in requiredFldsData){
                var fldValue = requiredFldsData[cmp].get("v.value");
                var fldclass=requiredFldsData[cmp].get("v.class");
                var fldApiName=requiredFldsData[cmp].get("v.fieldName");
                var fldDisabledStatus=requiredFldsData[cmp].get("v.disabled");
                if(fldclass != null && fldclass !='' && (fldclass.includes('alrequired') || fldclass.includes('actrequired')) && fldDisabledStatus != true){
                    if(fldApiName != null && fldApiName !=''){
                        if(fldValue == undefined || fldValue ==null || fldValue==''){
                            var showErrOnFld = document.getElementById('erroredit-'+fldApiName);
                            if(showErrOnFld != null && showErrOnFld != undefined){
                                showErrOnFld.style.display='';
                                var parentTabName = showErrOnFld.dataset.record;
                                // var fldLabel = showErrOnFld.dataset.fldlabel;
                                // vaidationErrorList.push(fldLabel);
                                if(parentTabName!= null){
                                    var showErrOnTab = document.getElementById('tab_erroredit_icon_'+parentTabName);
                                    if(showErrOnTab != null)
                                        showErrOnTab.style.display='';
                                }
                                validationErr = true;
                            }
                        }
                    }
                }
                
            }
        }
        console.log("validationErr"+validationErr);
        if(!validationErr){
            if(requiredFldsData != undefined && requiredFldsData != null){
                for(var cmp in requiredFldsData){
                    var fldDisabledStatus=requiredFldsData[cmp].get("v.disabled"); 
                    var fldname=requiredFldsData[cmp].get("v.fieldName");
                    // alert('fldDisabledStatus'+fldDisabledStatus);
                    if(fldDisabledStatus == true  && fldname != 'CreatedDate' && fldname != 'CreatedById' && fldname != 'LastModifiedDate' && fldname != 'LastModifiedById' && fldname != 'OwnerId')
                    {
                        //alert(requiredFldsData[cmp].get("v.fieldName"));
                        requiredFldsData[cmp].set("v.value",'');
                    }
                    /*else{
                        alert(requiredFldsData[cmp].get("v.fieldName"));
                    }*/
                }
            }
            component.set("v.isSubmitAction",true);
            //code wrote to change Record Status field 22-08-2018
            var recordStatusField = component.find("isrecordStatus");
            recordStatusField.set("v.value",'Final');
            console.log('@@@@@@@@@@@@@@@ Final')
            //End
            //code wrote to change Clone field 27-08-2018
            var isClonePage = component.get("v.isClonePage");
            if(isClonePage){
                component.set("v.recordId",null);
                var isCloneCheckBox = component.find("isclonecheckbox");
                isCloneCheckBox.set("v.value",true);
            }
            //End
            var tempForm = component.find("CLM_EditForm");
            tempForm.submit();
            
        }else{
           /* var errPopupDiv=component.find("errorpoptop");
            $A.util.removeClass(errPopupDiv,"hide");
            var errPopupDiv=component.find("errorpop");
            $A.util.removeClass(errPopupDiv,"hide");
            var cmpTargetTop = component.find('errorListtop');
            $A.util.removeClass(cmpTargetTop,'hide');
            var cmpTargetTop = component.find('errorList');
            $A.util.removeClass(cmpTargetTop,'hide');*/
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "message": "Please fill all the required fields.",
                "type":"error"
            });
            toastEvent.fire();
            
		
        }
    },
    callMyDraftCallback: function(component, event, helper){
        var requiredFldsData = component.find("myId");
        var validationErr = false;
        if(requiredFldsData != undefined && requiredFldsData != null){
            var previousErrorMsgs =document.getElementsByClassName("errormsgcls");
            var previousErrorsOnTabs = document.getElementsByClassName("tab_erroredit_div");
            if(previousErrorMsgs != null){
                for(var i=0;i<previousErrorMsgs.length;i++){
                    previousErrorMsgs[i].style.display='none';
                }
            }
            if(previousErrorsOnTabs != null){
                for(var i=0;i<previousErrorsOnTabs.length;i++){
                    previousErrorsOnTabs[i].style.display='none';
                }
            }
            //var vaidationErrorList = [];
            for(var cmp in requiredFldsData){
                var fldValue = requiredFldsData[cmp].get("v.value");
                var fldclass=requiredFldsData[cmp].get("v.class");
                var fldApiName=requiredFldsData[cmp].get("v.fieldName");
               // alert(' fldApiName '+fldApiName+' fldValue '+fldValue);
                var fldDisabledStatus=requiredFldsData[cmp].get("v.disabled");
                if(fldclass != null && fldclass !='' && fldclass.includes('alrequired') && fldDisabledStatus != true){
                    if(fldApiName != null && fldApiName !=''){
                        if(fldValue == undefined || fldValue ==null || fldValue==''){
                            var showErrOnFld = document.getElementById('erroredit-'+fldApiName);
                            if(showErrOnFld != null && showErrOnFld != undefined){
                                showErrOnFld.style.display='';
                                var parentTabName = showErrOnFld.dataset.record;
                                // var fldLabel = showErrOnFld.dataset.fldlabel;
                                // vaidationErrorList.push(fldLabel);
                                if(parentTabName!= null){
                                    var showErrOnTab = document.getElementById('tab_erroredit_icon_'+parentTabName);
                                    if(showErrOnTab != null)
                                        showErrOnTab.style.display='';
                                }
                                validationErr = true;
                            }
                        }
                    }
                }
                
            }
        }
        console.log("validationErr"+validationErr);
        if(!validationErr){
            component.set("v.isSubmitAction",true);
            if(requiredFldsData != undefined && requiredFldsData != null){
                for(var cmp in requiredFldsData){
                    var fldDisabledStatus=requiredFldsData[cmp].get("v.disabled");  
                    var fldname=requiredFldsData[cmp].get("v.fieldName");
                    // alert('fldDisabledStatus'+fldDisabledStatus);
                    if(fldDisabledStatus == true  && fldname != 'CreatedDate' && fldname != 'CreatedById' && fldname != 'LastModifiedDate' && fldname != 'LastModifiedById' && fldname != 'OwnerId')
                    {
                        //alert(requiredFldsData[cmp].get("v.fieldName"));
                        requiredFldsData[cmp].set("v.value",'');
                    }
                    /*else{
                        alert(requiredFldsData[cmp].get("v.fieldName"));
                    }*/
                }
            }
            //code wrote to change Record Status field 22-08-2018
            var recordStatusField = component.find("isrecordStatus");
            recordStatusField.set("v.value",'Draft');
            //End
            //code wrote to change Clone field 27-08-2018
            var isClonePage = component.get("v.isClonePage");
            if(isClonePage){
                component.set("v.recordId",null);
                var isCloneCheckBox = component.find("isclonecheckbox");
                isCloneCheckBox.set("v.value",true);
            }
            //End
            var tempForm = component.find("CLM_EditForm");
            tempForm.submit();
            
        }else{
            /*var errPopupDiv=component.find("errorpoptop");
            $A.util.removeClass(errPopupDiv,"hide");
            var errPopupDiv=component.find("errorpop");
            $A.util.removeClass(errPopupDiv,"hide");
            var cmpTargetTop = component.find('errorListtop');
            $A.util.removeClass(cmpTargetTop,'hide');
            var cmpTargetTop = component.find('errorList');
            $A.util.removeClass(cmpTargetTop,'hide');*/
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "message": "Please fill all the required fields.",
                "type":"error"
            });
            toastEvent.fire();
        }
    },
    itemsChange: function(component, event, helper){
        console.log('itemsChange'+component.get("v.myChangeCall"));
        helper.callDepandacyonLoad(component, event,helper);
        
    }
})