public class SixthCaseLookupFilter{
    public static List<sObject> filter(List<sObject> inputRecordList,LookupFilterToolingAPI.FilterItem eachFilterItem,String parentSObjectName,String firstValue){
        
        List<sObject> filteredRecordList = new List<sObject>();
        //newlly added
        sObject currentUser;
        if(eachFilterItem.valueField.contains('$User.')){
            string userFields='';
            if(eachFilterItem.valueField.contains('$User'))
                userFields = eachFilterItem.valueField.replace('$User.','')+',';
            if(userFields != '' && userFields.endsWith(',') )
            {
                userFields = userFields.removeEnd(',');
            }
            //currentUser =  [select Id,IsActive, username from User where Id = :UserInfo.getUserId()]; 
            string userId = ''+UserInfo.getUserId(); 
            System.debug('Select '+userFields+' From User where id = \''+UserInfo.getUserId()+ '\' Limit 1');
            try{
                currentUser = Database.query('Select '+userFields+' From User where id = \''+UserInfo.getUserId()+ '\' Limit 1');
            }
            catch(Exception ex)
            {
                System.debug('Exception '+ex);
            }
            System.debug('inside if currentUser '+currentUser);
        }
        if(eachFilterItem.valueField.contains('$Profile.')){
            string profileFields='';
            if(eachFilterItem.valueField.contains('$Profile'))
                profileFields = eachFilterItem.valueField.replace('$Profile.','')+',';
            if(profileFields != '' && profileFields.endsWith(',') )
            {
                profileFields = profileFields.removeEnd(',');
            }
            //currentUser =  [select Id,IsActive, username from User where Id = :UserInfo.getUserId()]; 
            string profileId = ''+UserInfo.getProfileId(); 
            System.debug('Select '+profileFields+' From Profile where id = \''+profileId+ '\' Limit 1');
            try{
                currentUser = Database.query('Select '+profileFields+' From Profile where id = \''+profileId+ '\' Limit 1');
            }
            catch(Exception ex)
            {
                System.debug('Exception '+ex);
            }
        }
        if(eachFilterItem.valueField.contains('$UserRole.')){
            string userFields='';
            if(eachFilterItem.valueField.contains('$UserRole'))
                userFields = eachFilterItem.valueField.replace('$UserRole.','')+',';
            if(userFields != '' && userFields.endsWith(',') )
            {
                userFields = userFields.removeEnd(',');
            }
            string roleId = ''+UserInfo.getUserRoleId(); 
            System.debug('Select '+userFields+' From UserRole where id = \''+roleId+ '\' Limit 1');
            try{
                currentUser = Database.query('Select '+userFields+' From UserRole where id = \''+roleId+ '\' Limit 1');
            }
            catch(Exception ex)
            {
                System.debug('Exception '+ex);
            }
        }
        
        Map<String, Schema.SObjectField> fieldsMapAccessible = Schema.getGlobalDescribe().get(parentSObjectName).getDescribe().SObjectType.getDescribe().fields.getMap();
        //end
        for(sObject obj : inputRecordList){
            System.debug('eachFilterItem '+eachFilterItem);
            System.debug('field '+eachFilterItem.field);
            if(!isFiltered(obj,eachFilterItem,parentSObjectName,firstValue,currentUser,fieldsMapAccessible)){//newlly added ,fieldsMapAccessible
                filteredRecordList.add(obj);
            }
        }
        return filteredRecordList;
    }
    
    private static Boolean isFiltered(sObject obj,LookupFilterToolingAPI.FilterItem eachFilterItem,String parentSObjectName,String firstValue,sObject userData,Map<String, Schema.SObjectField>parentFieldMap){//newlly added Map<String, Schema.SObjectField>parentFieldMap
        System.debug('@@@@@@@@@@@@ eachFilterItem.operation '+eachFilterItem.operation);
        // System.debug('@@@@@@@@@@@@ parentSObjectName'+parentSObjectName);
        if(eachFilterItem.operation == 'equals'){
            if(eachFilterItem.valueField.contains('$User') || eachFilterItem.valueField.contains('$Profile') || eachFilterItem.valueField.contains('$UserRole')){
                try{
                    if(firstValue == ''+userData.get(eachFilterItem.valueField.replace('$User.','').replace('$Profile.','').replace('$UserRole.',''))){
                        return false;
                    }
                }
                catch(Exception ex)
                {
                    System.debug('Exception '+ex);
                }
            }
            else{
                try{
                    if(firstValue == ''+obj.get(eachFilterItem.valueField.replace(parentSObjectName+'.',''))){
                        return false;
                    }
                }
                catch(Exception ex)
                {
                    System.debug('Exception '+ex);
                }
            }
        }
        if(eachFilterItem.operation == 'notEqual'){
            if(eachFilterItem.valueField.contains('$User') || eachFilterItem.valueField.contains('$Profile') || eachFilterItem.valueField.contains('$UserRole')){
                try{
                    if(firstValue  != ''+userData.get(eachFilterItem.valueField.replace('$User.','').replace('$Profile.','').replace('$UserRole.',''))){
                        return false;
                    }
                }
                catch(Exception ex)
                {
                    System.debug('Exception '+ex);
                }
            }
            else{
                try{
                    if(firstValue  != ''+obj.get(eachFilterItem.valueField.replace(parentSObjectName+'.',''))){
                        return false;
                    }
                }
                catch(Exception ex)
                {
                    System.debug('Exception '+ex);
                }
            }
        }
        if(eachFilterItem.operation == 'startsWith'){
            if(eachFilterItem.valueField.contains('$User') || eachFilterItem.valueField.contains('$Profile') || eachFilterItem.valueField.contains('$UserRole')){
                try{
                    if(firstValue.startsWith(''+obj.get(eachFilterItem.valueField.replace('$User.','').replace('$Profile.','').replace('$UserRole.','')))){
                        return false;
                    }
                }
                catch(Exception ex)
                {
                    System.debug('Exception '+ex);
                }
            }else{
                try{
                    if(firstValue.startsWith(''+obj.get(eachFilterItem.valueField.replace(parentSObjectName+'.','')))){
                        return false;
                    }
                }
                catch(Exception ex)
                {
                    System.debug('Exception '+ex);
                }
            }
            
        }
        if(eachFilterItem.operation == 'contains'){
            if(eachFilterItem.valueField.contains('$User') || eachFilterItem.valueField.contains('$Profile') || eachFilterItem.valueField.contains('$UserRole')){
                try{
                    if(firstValue.contains(''+obj.get(eachFilterItem.valueField.replace('$User.','').replace('$Profile.','').replace('$UserRole.','')))){
                        // if(firstValue.contains(''+obj.get(eachFilterItem.field.replace(parentSObjectName+'.','')))){
                        return false;
                    }
                }
                catch(Exception ex)
                {
                    System.debug('Exception '+ex);
                }
            }
            else{
                try{
                    if(firstValue.contains(''+obj.get(eachFilterItem.valueField.replace(parentSObjectName+'.','')))){
                        // if(firstValue.contains(''+obj.get(eachFilterItem.field.replace(parentSObjectName+'.','')))){
                        return false;
                    }
                }
                catch(Exception ex)
                {
                    System.debug('Exception '+ex);
                }
            }
            
        }
        if(eachFilterItem.operation == 'notContain'){
            if(eachFilterItem.valueField.contains('$User') || eachFilterItem.valueField.contains('$Profile') || eachFilterItem.valueField.contains('$UserRole')){
                try{
                    if(!firstValue.contains(''+obj.get(eachFilterItem.valueField.replace('$User.','').replace('$Profile.','').replace('$UserRole.','')))){
                        // if(!firstValue.contains(''+obj.get(eachFilterItem.field.replace(parentSObjectName+'.','')))){
                        return false;
                    }
                }
                catch(Exception ex)
                {
                    System.debug('Exception '+ex);
                }
            }
            else{
                try{
                    if(!firstValue.contains(''+obj.get(eachFilterItem.valueField.replace(parentSObjectName+'.','')))){
                        // if(!firstValue.contains(''+obj.get(eachFilterItem.field.replace(parentSObjectName+'.','')))){
                        return false;
                    }
                }
                catch(Exception ex)
                {
                    System.debug('Exception '+ex);
                }
            }
            
        }
        
        if(eachFilterItem.operation == 'includes'){
            if(eachFilterItem.valueField.contains('$User') || eachFilterItem.valueField.contains('$Profile') || eachFilterItem.valueField.contains('$UserRole')){
                if(firstValue.contains(''+obj.get(eachFilterItem.valueField.replace('$User.','').replace('$Profile.','').replace('$UserRole.','')))){
                    // if(firstValue.contains(''+obj.get(eachFilterItem.field.replace(parentSObjectName+'.','')))){
                    return false;
                }
            }
            else{
                if(firstValue.contains(''+obj.get(eachFilterItem.valueField.replace(parentSObjectName+'.','')))){
                    // if(firstValue.contains(''+obj.get(eachFilterItem.field.replace(parentSObjectName+'.','')))){
                    return false;
                }
            }
            
        }
        if(eachFilterItem.operation == 'excludes'){
            if(eachFilterItem.valueField.contains('$User') || eachFilterItem.valueField.contains('$Profile') || eachFilterItem.valueField.contains('$UserRole')){
                if(!firstValue.contains(''+obj.get(eachFilterItem.valueField.replace('$User.','').replace('$Profile.','').replace('$UserRole.','')))){
                    // if(!firstValue.contains(''+obj.get(eachFilterItem.field.replace(parentSObjectName+'.','')))){
                    return false;
                }
            }
            else{
                if(!firstValue.contains(''+obj.get(eachFilterItem.valueField.replace(parentSObjectName+'.','')))){
                    // if(!firstValue.contains(''+obj.get(eachFilterItem.field.replace(parentSObjectName+'.','')))){
                    return false;
                }
            }
            
        }
        if(eachFilterItem.operation == 'lessThan'){//newlly added
            try{
                String secondValueStr = obj.get(eachFilterItem.valueField.replace(parentSObjectName+'.',''))+'';
                Schema.SObjectField sobjectField = parentFieldMap.get(eachFilterItem.valueField.replace(parentSObjectName+'.',''));
                String fieldType = sobjectField.getDescribe().getType() +'';
                System.debug('fieldType '+fieldType);
                if(fieldType == 'datetime'){
                    if(Datetime.valueOf(firstValue) < Datetime.valueOf(secondValueStr)){
                        System.debug('false from if datetime');
                        return false;
                    }
                }
                else{
                    if(firstValue.isNumeric() && secondValueStr.isNumeric()){
                        if(Decimal.valueOf(firstValue) < Decimal.valueOf(secondValueStr)){
                            return false;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                System.debug(' Exception '+ex);
            }
        }
        if(eachFilterItem.operation == 'greaterThan'){
            try{
                String secondValueStr = obj.get(eachFilterItem.valueField.replace(parentSObjectName+'.',''))+'';
                Schema.SObjectField sobjectField = parentFieldMap.get(eachFilterItem.valueField.replace(parentSObjectName+'.',''));
                String fieldType = sobjectField.getDescribe().getType() +'';
                System.debug('fieldType '+fieldType);
                if(fieldType == 'datetime'){
                    if(Datetime.valueOf(firstValue) > Datetime.valueOf(secondValueStr)){
                        System.debug('false from if datetime');
                        return false;
                    }
                }
                else{
                    if(firstValue.isNumeric() && secondValueStr.isNumeric()){
                        if(Decimal.valueOf(firstValue) > Decimal.valueOf(secondValueStr)){
                            return false;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                System.debug(' Exception '+ex);
            }
        }
        if(eachFilterItem.operation == 'lessOrEqual'){
            try{
                String secondValueStr = obj.get(eachFilterItem.valueField.replace(parentSObjectName+'.',''))+'';
                Schema.SObjectField sobjectField = parentFieldMap.get(eachFilterItem.valueField.replace(parentSObjectName+'.',''));
                String fieldType = sobjectField.getDescribe().getType() +'';
                System.debug('fieldType '+fieldType);
                if(fieldType == 'datetime'){
                    if(Datetime.valueOf(firstValue) <= Datetime.valueOf(secondValueStr)){
                        System.debug('false from if datetime');
                        return false;
                    }
                }
                else{
                    if(firstValue.isNumeric() && secondValueStr.isNumeric()){
                        if(Decimal.valueOf(firstValue) <= Decimal.valueOf(secondValueStr)){
                            return false;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                System.debug(' Exception '+ex);
            }
        }
        if(eachFilterItem.operation == 'greaterOrEqual'){
            try{
                String secondValueStr = obj.get(eachFilterItem.valueField.replace(parentSObjectName+'.',''))+'';
                Schema.SObjectField sobjectField = parentFieldMap.get(eachFilterItem.valueField.replace(parentSObjectName+'.',''));
                String fieldType = sobjectField.getDescribe().getType() +'';
                System.debug('fieldType '+fieldType);
                if(fieldType == 'datetime'){
                    if(Datetime.valueOf(firstValue) >= Datetime.valueOf(secondValueStr)){
                        System.debug('false from if datetime');
                        return false;
                    }
                }
                else{
                    if(firstValue.isNumeric() && secondValueStr.isNumeric()){
                        if(Decimal.valueOf(firstValue) >= Decimal.valueOf(secondValueStr)){
                            return false;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                System.debug(' Exception '+ex);
            }
        }
        return true;
    }
}