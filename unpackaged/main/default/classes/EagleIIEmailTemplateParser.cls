public class EagleIIEmailTemplateParser {
    private String emailSubject='';
    Private String titledName='';
    Private Date proposalDueDate;
    Private Datetime proposalDueDateTime;
    private String senderEmailId = '';
    private String contactName = '';
    private String contactTitle = '';
    public String taskOrderNumber='';
    
    public void parse(Messaging.InboundEmail email){
        String htmlText = email.HTMLBody;
        String emailPlainText = email.plainTextBody;
        System.debug('emailPlainText = '+emailPlainText);
        emailSubject = email.subject;
        emailSubject = emailSubject.stripHtmlTags();
        
        System.debug('emailSubject = '+emailSubject);
        String toNum = emailSubject.substringBetween('Request for Quote: ', ' ');
        taskOrderNumber = checkValidString(toNum, 79);
        System.debug('emailSubject = '+emailSubject);
        titledName = emailSubject.substringAfter('Request for Quote: '+toNum).trim();
        System.debug('titledName = '+titledName);
        try{
            proposalDueDateTime = getformatedDate(emailPlainText.substringBetween('Responses are due to the Contract Specialist *no later than','EST').replace('*','').trim());
            System.debug('***proposalDueDate = '+proposalDueDate);
            proposalDueDate = proposalDueDateTime.date();
            System.debug('proposalDueDate = '+proposalDueDate);
        }catch(Exception e){
            proposalDueDateTime = null;
            proposalDueDate = null;
            System.debug(''+e.getMessage()+'\tLine Number = '+e.getLineNumber());
        }
        //System.debug('proposalDueDate = '+Date.parse(proposalDueDate));
        List<String> emailTemplateList = emailPlainText.split('\n');
        senderEmailId = email.fromAddress;
        contactName = email.fromName;        
        System.debug('contactName = '+contactName);
        System.debug('senderEmailId = '+senderEmailId);        
        for(String line : emailTemplateList){
            line = line.trim();
            if(line == 'Contract Specialist' || line == 'Contracting Officer'){
                contactTitle = line;
                System.debug('contactTitle = '+contactTitle); 
            }
        }       
    }
    
    private String checkValidString(String str, Integer endLimit){
        if(str.length() > endLimit){
            String validString = str.substring(0,endLimit);
            return validString;
        }
        return str;
    }    
    
    public Task_Order__c getTaskOrder(String toAddress){       
        List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];
        System.debug('*********contract vechcle size  = '+ conVehicalList.size());
        
        if(!conVehicalList.isEmpty()){
            Task_Order__c taskOrder = new Task_Order__c();
            taskOrder.Contract_Vehicle__c = conVehicalList[0].Id;
            // taskOrder.Task_Order_Contracting_Organization__c = conVehicalList[0].Account_Name__c;
            
            System.debug('****email subject'+emailSubject);
            System.debug('****email subject length = '+emailSubject.length()); 
            taskOrder.Name =  taskOrderNumber;
            taskOrder.Task_Order_Title__c = checkValidString(titledName , 255);
            taskOrder.Release_Date__c = Date.today();
            taskOrder.Contract_Vehicle_picklist__c = 'EAGLE II - UNRESTRICTED';
            taskOrder.Email_Reference__c = checkValidString(getTrimedEmailSubject(emailSubject), 255);
            if(proposalDueDate!=null){            
                taskOrder.Due_Date__c = proposalDueDate;
                taskOrder.Proposal_Due_DateTime__c = proposalDueDateTime;                
            } 
            //setting Contact details
            List<Contact> contactList =  [Select id, Email from Contact WHERE Email = :senderEmailId];
            Contact contact;
            if(!contactList.isEmpty()){
                contact = contactList.get(0);   
            }else{
                String []salutation = new String[]{'Mr.','Ms.','Mrs.','Dr.','Prof.'}; 
                    contact = new Contact();
                try{
                    List<String> split =  contactName.split(' ');                
                    contact.FirstName = split.get(0);
                    contact.LastName = split.get(1);   
                    contact.Title = contactTitle;
                    contact.Is_FedTom_Contact__c = true;
                }catch(Exception e){
                    
                }
                if(conVehicalList[0].Account_Name__c != null){
                    contact.AccountId = conVehicalList[0].Account_Name__c;
                }
                contact.Email = senderEmailId;
                insert contact;
            }
            if(contactTitle.equals('Contract Specialist')){
                taskOrder.Contract_Specialist__c = contact.id;
            }
            if(contactTitle.equals('Contracting Officer')){
                taskOrder.Contracting_Officer__c = contact.id;
            }
            //End Setting contact details
            return taskOrder;
        }
        
        return null;
    }  
    
    private String getTrimedEmailSubject(String subject){
        if(subject.startsWith('RE:')){
            subject = subject.replaceFirst('RE:', '');            
        }
        if(subject.startsWith('Fwd:') ){
            subject = subject.replaceFirst('Fwd:', '');            
        }   
        return subject.trim();
    }
    
    /* private Date getformatedDate(){
//This method is specific to the task only.
Map <String, Integer> months = new Map <String, Integer> {'january'=>1, 'febuary'=>2
, 'march'=>3, 'april'=>4, 'may'=>5, 'june'=>6, 'july'=>7, 'august'=>8, 'september'=>9
, 'october'=>10, 'november'=>11, 'december'=>12};
List<String> splitDate = proposalDueDate.replaceAll('(\\s+)', ' ').trim().split(' ');        
string month = splitdate.get(0).toLowerCase();
for(string d : splitDate){
System.debug('d = '+d);
}
Integer monthInt = months.get(month);
List<String> splitDate1 = splitDate.get(1).split('\n');
for(string d1 : splitDate1){
System.debug('d1 = '+d1);
}
string day = splitDate1.get(0).replace(',','');
string year = splitDate1.get(1);
//Date.newInstance(year, month, day);
try{
Date parsedDate = Date.parse(''+monthInt+'/'+day+'/'+year);
System.debug('parsedDate'+parsedDate);
return parsedDate;
}catch(Exception e){
System.debug('proposalDueDate = '+e.getMessage()+'\t'+e.getLineNumber());
return null;
} 
}*/
    
    private DateTime getformatedDate(String dateTimeString){
        //This method is specific to the task only.
        Map <String, Integer> months = new Map <String, Integer> {'january'=>1, 'february'=>2
            , 'march'=>3, 'april'=>4, 'may'=>5, 'june'=>6, 'july'=>7, 'august'=>8, 'september'=>9
            , 'october'=>10, 'november'=>11, 'december'=>12};
                List<String> splitDate = dateTimeString.replaceAll('(\\s+)', ' ').trim().split(' ');        
        /*string month = splitdate.get(0).toLowerCase();
for(string d : splitDate){
System.debug('d = '+d);
}
Integer monthInt = months.get(month);
List<String> splitDate1 = splitDate.get(1).split('\n');
for(string d1 : splitDate1){
System.debug('d1 = '+d1);
}
string day = splitDate1.get(0).replace(',','');
string year = splitDate1.get(1);
//Date.newInstance(year, month, day);*/
        //
        //
        Integer monthInt;
        string day;
        string year;
        System.debug('******sp '+splitDate);
        //All the 
        //if(splitDate.size()==3){
        if(months.get(splitDate[0].trim().toLowerCase()) == null){
            return null;                
        }
        monthInt = months.get(splitDate[0].trim().toLowerCase());                
        day = splitDate[1].trim().replace(',','');
        year = splitDate[2].trim();
        try{
            DateTime parsedDate = DateTime.parse(''+monthInt+'/'+day+'/'+year+' '+splitDate[4]+' '+splitDate[5]);
            System.debug('parsedDate'+parsedDate);
            return parsedDate;
        }catch(Exception e){
            System.debug('proposalDueDate = '+e.getMessage()+'\t'+e.getLineNumber());
            return null;
        } 
        //}
        return Null;
    }
    
    public Task_Order__c updateTaskOrder(Task_Order__c taskOrder, String toAddress){        
        List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];
        System.debug('*********contract vechcle size  = '+ conVehicalList.size());
        
        //if(!conVehicalList.isEmpty()){
        taskOrder.Contract_Vehicle__c = conVehicalList[0].Id;
        // taskOrder.Task_Order_Contracting_Organization__c = conVehicalList[0].Account_Name__c;
        
        System.debug('****email subject'+emailSubject);
        System.debug('****email subject length = '+emailSubject.length()); 
        taskOrder.Name =  taskOrderNumber;
        taskOrder.Task_Order_Title__c = checkValidString(titledName , 255);
        taskOrder.Release_Date__c = Date.today();
        //taskOrder.Contract_Vehicle_picklist__c = 'EAGLE II - UNRESTRICTED';
        // taskOrder.Email_Reference__c = checkValidString(getTrimedEmailSubject(emailSubject), 255);
        if(proposalDueDate!=null){            
            taskOrder.Due_Date__c = proposalDueDate;
            taskOrder.Proposal_Due_DateTime__c = proposalDueDateTime;                
        }  
        System.debug('taskOrder.Due_Date__c'+taskOrder.Due_Date__c);
        //setting Contact details
        List<Contact> contactList =  [Select id, Email from Contact WHERE Email = :senderEmailId];
        Contact contact;
        if(!contactList.isEmpty()){
            contact = contactList.get(0);   
        }else{
            String []salutation = new String[]{'Mr.','Ms.','Mrs.','Dr.','Prof.'}; 
                contact = new Contact();
            try{
                List<String> split =  contactName.split(' ');            
                contact.FirstName = split.get(0);
                contact.LastName = split.get(1);   
                contact.Title = contactTitle;
                contact.Is_FedTom_Contact__c = true;
            }catch(Exception e){
                System.debug(''+e.getMessage()+'\tLine Number = '+e.getLineNumber());
            }
            if(conVehicalList[0].Account_Name__c != null){
                contact.AccountId = conVehicalList[0].Account_Name__c;
            }
            contact.Email = senderEmailId;
            //insert contact;
        }
        
        
        try{
            if(taskOrder.Customer_Agency__c != null){
                contact.AccountId = taskOrder.Customer_Agency__c;                    
                //DMLManager.upsertAsUser(con);
                if(contact.Id == null){
                    // if Task Order with Customer Organization is not having a Contact or if a different Contact(i.e. a Contact with different Email Id) is found
                    //DMLManager.insertAsUser(con);
                    insert contact;                        
                }else{
                    // if Task Order with Customer Organization is having a Contact, then update that Contact
                    //DMLManager.updateAsUser(con);
                    update contact;                        
                }
            }else{
                if(contact.Id == null)
                    DMLManager.insertAsUser(contact);                                         
            }                
            if(contactTitle.equals('Contract Specialist')){
                taskOrder.Contract_Specialist__c = contact.id;
            }
            if(contactTitle.equals('Contracting Officer')){
                taskOrder.Contracting_Officer__c = contact.id;
            }
        }catch(Exception e){
            System.debug('Error in contact creation - '+e.getMessage()+':'+e.getLineNumber());
        }                                           
        //End Setting contact details
        return taskOrder;
        //}        
        // return null;   
    }
    
}