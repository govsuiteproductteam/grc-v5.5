public with sharing class GetBookingTableData{
        
        @AuraEnabled
        public Value_Table__c bookTab;
       
        
        public GetBookingTableData(Value_Table__c bookTab1)
        {
            bookTab=bookTab1;
            
            System.debug('bookTab '+bookTab);
        }
    }