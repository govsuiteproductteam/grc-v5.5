@isTest
public with sharing class SurveyDisplayControllerTest{
    public static TestMethod void TestSurveyDisplayController(){
        List<Profile> profileList = [SELECT Id FROM Profile where (Name = 'Custom Partner Community User' OR Name = 'Custom Partner Community Login User' OR  Name  = 'Custom Customer Community Plus Login User') Limit 1];
        if(profileList != null && profileList.size()>0){
        
            Account acc = TestDataGenerator.createAccount('TaskOrderCapture');
            insert acc;
            
            Contact con = TestDataGenerator.createContact('Avanti',acc);
            insert con;
            
            User usr = TestDataGenerator.createUser(con,TRUE,'test1', 'atest1','SurveyDisplaytest1@gmail.com','SurveyDisplaytaskordercapture@demo.com','atest1',profileList[0],'America/Los_Angeles','en_US','UTF-8','en_US');
            insert usr;    
            
            Contract_Vehicle__c contractVehicle = TestDataGenerator.createContractVehicle('TMconVehicle',acc);
            insert contractVehicle;
            
            Task_Order__c taskOrder = TestDataGenerator.createTaskOrder('TMtaskOrder', acc, contractVehicle);
            insert taskOrder;
            
            Survey__c sur = TestDataGenerator.createSurvey();
            insert sur;
           
            Survey_Task__c surveyTask = TestDataGenerator.createSurveyTask(sur ,taskOrder);
            insert surveyTask ;
            
            Question__c que = TestDataGenerator.createQuestion('What',sur);
            que.Option_1__c = 'opt1';
            que.Option_2__c = 'opt2';
            que.Option_3__c = 'opt3';
            que.Option_4__c = 'opt4';
            que.Option_5__c = 'opt5';
            que.Option_6__c = 'opt6';
            que.Option_7__c = 'opt7';
            que.Option_8__c = 'opt8';
            que.Option_9__c = 'opt9';
            que.Option_10__c = 'opt10';
            insert que;
            
            List<Questionaries_Email_Template__c> QETTemp = new List<Questionaries_Email_Template__c>();
            Boolean tempFlas = true;
            for(EmailTemplate eTemplate : [SELECT Id FROM EmailTemplate WHERE folder.name = 'TOMA Email Templates']){
                if(tempFlas){
                    QETTemp.add(TestDataGenerator.addQuestionarieEmailTemplate(sur.Id,eTemplate.Id));
                    tempFlas = false;
                }
            }
            
            Insert QETTemp;
            
            Test.startTest();
            ApexPages.CurrentPage().getparameters().put('id',sur.id);
           
            ApexPages.StandardController controller = new ApexPages.StandardController(sur);
            SurveyDisplayController SurveyDisplayController = new SurveyDisplayController(controller);
            SurveyDisplayController.saveQET();
            
            SurveyDisplayController.displayData();
            system.assertNotEquals(SurveyDisplayController.SurveyId, null );
            
            PageReference pgRef = SurveyDisplayController.myEdit();
            system.assertNotEquals (pgRef, null);
            
            PageReference pgRe = SurveyDisplayController.cloneSurvey();
            system.assertNotEquals(pgRe, null );
            
            PageReference pr= SurveyDisplayController.Cancel();
            system.assertNotEquals(pr, null);
            
            SurveyDisplayController.qetDelete();
            
            Test.stopTest();
        } 
    } 
}