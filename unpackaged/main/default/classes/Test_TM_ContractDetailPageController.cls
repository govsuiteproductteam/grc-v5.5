@IsTest
public class Test_TM_ContractDetailPageController {
    
    Public static testMethod void testContractDetailPageController(){
        Test.startTest();
        List<RecordType> rtypes = [Select Name, Id From RecordType where sObjectType='TM_TOMA__Contract_Vehicle__c' and isActive=true];
        string recId='';
        if(rtypes.size()>0)
        {
            recId=rtypes[0].Id;
        }
        
        CLM_AtaGlanceField__c atAGlance = TestDataGenerator.createAtaGlanceField(recId);
        atAGlance.Is_Contract__c = true;
        atAGlance.Is_Component__c = False;
        insert atAGlance;
        CLM_Tab__c tabSetting = TestDataGenerator.createCLMTab(recId);
        tabSetting.TabName__c='Internal Organisation';
        insert tabSetting;  
        CLM_Tab__c tabSetting1 = TestDataGenerator.createCLMTab(recId);
        tabSetting1.Order__c = 2;
        tabSetting1.TabName__c='Contract Description';
        insert tabSetting1;   
        CLM_Section__c sectionSetting = TestDataGenerator.createCLMSection(recId);
        insert sectionSetting;
        CLM_Section__c sectionSetting1 = TestDataGenerator.createCLMSection(recId);
        sectionSetting1.Order__c = 20;
        sectionSetting1.Is_Lightning_Component__c= true;
        sectionSetting1.Inline_Api_Name_Component_Api__c='TestComponent';
        insert sectionSetting1;
        CLM_Section__c sectionSetting2 = TestDataGenerator.createCLMSection(recId);
        sectionSetting2.Order__c = 30;
        sectionSetting2.Is_InlineVF_Page__c= true;
        sectionSetting2.Inline_Api_Name_Component_Api__c='TestVFPage';
        insert sectionSetting2;
        CLM_FIeld__c fieldSetting = TestDataGenerator.createCLMField(recId);
        fieldSetting.ViewFieldApi__c='';
        fieldSetting.Order__c = 1001;
        fieldSetting.Required__c = true;
        fieldSetting.Required_Error_Message__c='Enter value for field';
        insert fieldSetting;
        CLM_FIeld__c fieldSetting1 = TestDataGenerator.createCLMField(recId);
        fieldSetting1.FieldApiName__c = 'OwnerId';
        fieldSetting1.ViewFieldApi__c='OwnerId';
        fieldSetting1.Order__c = 1002;
        insert fieldSetting1;
        CLM_FIeld__c fieldSetting2 = TestDataGenerator.createCLMField(recId);
        fieldSetting2.FieldApiName__c = 'RecordTypeId';
        fieldSetting2.ViewFieldApi__c='RecordTypeId';
        fieldSetting2.Order__c = 1003;
        insert fieldSetting2;
        
        Account acc = TestDataGenerator.createAccount('testAccount');
        insert acc;
        
        Contract_Vehicle__c conVeh = TestDataGenerator.createContractVehicle('Test contract vehicle', acc);
        conVeh.recordTypeId = recId;
        conVeh.Contract_Type__c= TestDataGenerator.getPicklistValue('TM_TOMA__Contract_Vehicle__c','TM_TOMA__Contract_Type__c');
        insert conVeh;
        
        PageReference pageRef = Page.TM_ContractDetailPage;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id', conVeh.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(conVeh);
        TM_ContractDetailPageController contractDetailPageController = new TM_ContractDetailPageController(sc);
        System.assertEquals(contractDetailPageController.contractVehicle.Name, 'Test contract vehicle');
        System.assert(contractDetailPageController.tabWrapperList.size()>0); 
        contractDetailPageController.changeRecordType();
        contractDetailPageController.changeOwner();
        contractDetailPageController.chandeMode();
        System.assertEquals(contractDetailPageController.mode, 'edit');
        contractDetailPageController.cancelEdit();
        
        contractDetailPageController.contractVehicle.Name = 'Test contract vehicle 1';
        contractDetailPageController.saveContract1();
        System.assertEquals(contractDetailPageController.contractVehicle.Contract_Status_Backend__c, 'Draft');
        contractDetailPageController.saveAndActivate();
        System.assertEquals(contractDetailPageController.contractVehicle.Contract_Status_Backend__c, 'Final');
        //code to cover catch block code 
        contractDetailPageController.contractVehicle.Ceiling__c = 123123123123121233.123123123;
        contractDetailPageController.saveContract1();
        System.assertEquals(contractDetailPageController.mode, 'edit');
        contractDetailPageController.contractVehicle.Ceiling__c = 123123123123121233.123123123;
        contractDetailPageController.saveAndActivate();
        System.assertEquals(contractDetailPageController.mode, 'edit');
        contractDetailPageController.contractVehicle.ownerId = null;
        contractDetailPageController.changeOwner();
        contractDetailPageController.changeOwnerCancel();
        contractDetailPageController.changeRecordTypeCancel();
        Test.stopTest();
    }
}