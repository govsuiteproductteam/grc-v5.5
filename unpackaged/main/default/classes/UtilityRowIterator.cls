public with sharing class UtilityRowIterator implements Iterator<String>, Iterable<String>
{
   private String mData;
   private Integer mIndex = 0;
   private String mRowDelimiter = '\n';

   public UtilityRowIterator(String fileData)
   {
      mData = fileData; 
   }
   public UtilityRowIterator(String fileData, String rowDelimiter)
   {
      mData = fileData; 
      mRowDelimiter = rowDelimiter;
   }

   public Boolean hasNext()
   {
      return mIndex < mData.length() ? true : false;
   }
   public String next()
   {     
      Integer key = mData.indexOf(mRowDelimiter, mIndex);

      if (key == -1)
        key = mData.length();

      String row = mData.subString(mIndex, key);
      mIndex = key + 1;
       //System.debug('***row = '+row);
      return row;
   }
   public Iterator<String> Iterator()
   {
      return this;   
   }
}