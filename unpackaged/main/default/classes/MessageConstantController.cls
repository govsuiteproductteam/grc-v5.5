public with sharing Class MessageConstantController{    
    public final static String CONTRACT_NOT_FOUND = 'Contract Vehicle not found with this email address';
    public final static String TASK_ORDER_NOT_FOUND = 'Task Order Number not found with the email';
    public final static String IMPROPER_EMAIL_TEMPLATE = 'Improper email template.';
}