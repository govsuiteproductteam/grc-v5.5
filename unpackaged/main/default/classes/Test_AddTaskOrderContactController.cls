@isTest
public with sharing class Test_AddTaskOrderContactController {
    public static TestMethod void AddTaskOrderContactController(){
        Test.startTest();
        List<Profile> profileList = [SELECT Id FROM Profile where (Name = 'Custom Partner Community User' OR Name = 'Custom Partner Community Login User' OR Name  = 'Custom Customer Community Plus Login User') Limit 1];
        if(profileList != null && profileList.size()>0){
        
            Account acc = TestDataGenerator.createAccount('TaskOrderCapture');
            insert acc;
            
            List<Contact> conList = new List<Contact>();
            Contact con = TestDataGenerator.createContact('test1',acc);
            conList.add(con);
            
            Contact con1 = TestDataGenerator.createContact('test2',acc);
            conList.add(con1);
            insert conList;
            
            User usr = TestDataGenerator.createUser(con,TRUE,'test1', 'a2test1','1test1@gmail.com','taskordercap@demo.com','atest1',profileList[0],'America/Los_Angeles','en_US','UTF-8','en_US');
            insert usr;
            
            User usr1 = TestDataGenerator.createUser(con1,TRUE,'test2', 'a2test2','2test2@gmail.com','taskordercap2@demo.com','atest2',profileList[0],'America/Los_Angeles','en_US','UTF-8','en_US');
            insert usr1;
            
            Contract_Vehicle__c contractVehicle = TestDataGenerator.createContractVehicle('TMconVehicle',acc);
            insert contractVehicle;
            
            Task_Order__c taskOrder = TestDataGenerator.createTaskOrder('TMtaskOrder', acc, contractVehicle);
            insert taskOrder;
            
            Task_Order_Contact__c tOrderContact = new Task_Order_Contact__c(Contact__c = con.Id, Task_Order__c = taskOrder.Id);
            insert tOrderContact;
            
            Vehicle_Partner__c vehiclePartner = TestDataGenerator.createVehiclePartner(acc,contractVehicle );
            insert vehiclePartner;
            
            Teaming_Partner__c teamPartner = TestDataGenerator.createTeamingPartner(vehiclePartner , taskOrder);
            insert teamPartner;
            
            
               ApexPages.CurrentPage().getparameters().put('tid',taskOrder.id);
               ApexPages.StandardController controller  = new ApexPages.StandardController(tOrderContact);
               AddTaskOrderContactController ContactController = new AddTaskOrderContactController(controller);
               System.assertEquals(ContactController.taskOrderContactWrapperList.size(), 1);
               ContactController.taskOrderContactWrapperList[0].status = true;
               PageReference pg = ContactController.addTaskOrderContact();
               System.assertNotEquals(pg, Null);
            Test.stopTest();
        }
    }
}