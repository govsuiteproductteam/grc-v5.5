public with sharing class TestDataGeneratorGSA {
    public static TM_TOMA__Contract_Vehicle__c createContractVehicle(List<Account> accountList, List<Contact> contactList){
        TM_TOMA__Contract_Vehicle__c contract = new TM_TOMA__Contract_Vehicle__c();
       // contract.Industry__c = 'Civil';
       // contract.Division__c = 'DOT';
      //  contract.Accounting_Organization__c = 'AdvanceMed (Org 107.194)';
       // contract.Date_Executed_Document_Received__c = Date.today();
        contract.TM_TOMA__Contract_Number__c = 'GS-10F-0101S';
       // contract.Top_Level_Project_Title__c = 'abc';
       // contract.Is_this_a_BPA__c = 'true';
       // contract.BPA__c = 'abc';
       // contract.BPA_Description__c = 'asdva';
       // contract.DD254_Applicable__c = 'false';
       // contract.Current_Project_Number__c = '1234';
       // contract.Project_Number_Description__c = 'dhahdfka';
        contract.TM_TOMA__Contract_Period_Start_Date__c = Date.today();
        contract.TM_TOMA__Contract_Period_End_Date__c = Date.today() + 1;
       // contract.Final_Contract_End_Date__c = Date.today() + 2;
       // contract.Contract_Status__c = 'Active';
       // contract.Market_Sector__c = 'Commercial';
       //contract.Contractor_Status__c = 'Prime';
       // contract.Is_this_a_Multiple_Award_Contract__c = 'NO';
       // contract.Is_there_a_SB_Subcontracting_Plan__c = 'NO';
       // contract.Is_there_a_SDB_Participating_Plan__c = 'NO';
       // contract.Does_this_contract_require_notification__c = 'NO';
       // contract.Is_a_DD250_required__c = 'NO';
       // contract.Does_this_contract_have_GFE__c = 'NO';
       // contract.Does_this_contract_have_OCI_Limitation__c = 'NO';
       // contract.Does_this_contract_require_Aviation__c = false;
      //  contract.Are_Key_Personnel_Clauses_part_of__c = 'NO';
       // contract.Does_this_contract_have_a_Wage__c = 'NO';
       // contract.Is_this_a_FMS_Foreign_Military_Sales__c = 'NO';
       // contract.Is_the_TINA_applicable_in_the_base__c = 'NO';
       // contract.Are_foreign_nationals_prohibited__c = 'NO';
       // contract.Does_the_contract_state__c = 'NO';
       // contract.Is_CSC_s_proposal_incorporated__c = 'NO';
       // contract.Does_this_contract_have_EVMS_requirement__c = 'NO';
       // contract.Does_this_contract_have_the_CPAR__c = 'NO';
       // contract.Are_position_descriptions_part_of__c = 'NO';
       // contract.Are_unique_position_descriptions__c = 'NO';
       // contract.Does_this_specific_project_require__c = 'NO';
        contract.TM_TOMA__Contract_Type__c = 'BOA';
      //  contract.Is_this_an_Award_Term_Contract__c = 'NO';
        //contract.Does_the_Contract_have_Labor_Category__c = 'NO';
      //  contract.Does_the_Order_have_billing_rates__c = 'NO';
        //contract.Does_the_Contract_have_Labor__c = 'NO';
      //  contract.Does_the_Order_have_billing_rate__c = 'NO';
       // contract.Is_52_204_10_first_tier_subcontract__c = 'No, CSC is a subcontractor.';
       // contract.Is_this_Contract_CAS_Covered__c = 'NO';
       // contract.Does_the_contract_require_Limitations__c = 'NO';
      //  contract.Have_any_AARA_Funds__c = 'NO';
       // contract.Period_Covered__c = 'ahdjkd';
       // contract.Plus_Minus__c = 'adhadadla';
       // contract.Minimum_LOE__c = 'adhlkfj';
       // contract.Target_LOE__c = 'dfshfldsa';
       // contract.Maximum_LOE__c = 'dfhdsafad';
       // contract.Are_there_any_FAR_clauses__c = 'NO';
       // contract.Are_there_any_Agency_Supplement_clauses__c = 'NO';
       // contract.Are_there_any_Center_Clauses__c = 'NO';
       // contract.Are_there_any_Department_clauses__c = 'NO';
       // contract.Are_there_any_Contract_specific_clauses__c = 'NO';
      //  contract.Client_1_Name__c = accountList.get(0).Id;
      //  contract.Client_2_Name__c = accountList.get(1).Id;
        /*contract.Client_3_Name__c = accountList.get(2).Id;
        Contract.Contracting_Officer__c = contactList.get(0).Id;
        contract.Cust_Tech_Lead__c = contactList.get(0).Id;
        contract.Contract_Specialist__c = contactList.get(0).Id;
        contract.Administrative_Contracting_Officer__c = contactList.get(0).Id;
        contract.Program_Manager__c = UserInfo.getUserId();
        contract.Contract_Administrator__c = UserInfo.getUserId();
        contract.Program_Control__c = UserInfo.getUserId();
        contract.Subcontract_Administrator__c = UserInfo.getUserId();
        contract.Industry_Executive__c = UserInfo.getUserId();
        contract.Offering_Executive__c =  UserInfo.getUserId();
        contract.Sister_Division_IWO_POC__c = UserInfo.getUserId();
        contract.Billing_Specialist__c = UserInfo.getUserId();
        contract.Audit_delegation_to_DCAA_or_DCMA__c = 'NO';
        contract.Initial_Contract_Funding_at_Award__c = 1000; */ 
        contract.TM_TOMA__SINs__c= '874 1'; //sins 00CORP: 874 1
        //contract.SINs_New__c = '00CORP: 874 1'; 
        return contract;
    }
    
    public static List<Account> createAccountList(){
        List<Account> accountList = new List<Account>();
        for(Integer i = 1; i<4;i++){
            Account account = new Account();
            account.Name = 'acc '+i;
            accountList.add(account);
        }
        
        return accountList;
    }
    
    public static List<Contact> createContactList(List<Account> accountList){
        List<Contact> contactList = new List<Contact>();
        for(Integer i = 1; i<4;i++){
            Contact contact = new Contact();
            contact.FirstName = 'ABC '+ i;
            contact.LastName = 'XYZ '+ i;
            contact.AccountId = accountList.get(i-1).Id;
            contactList.add(contact);
        }
        return contactList;
    }
    
    public static List<TM_TOMA__Task_Order__c> createTaskOrder(List<Contact> contactList,List<Account> accountList, TM_TOMA__Contract_Vehicle__c contract){
        List<TM_TOMA__Task_Order__c> taskOrderList = new List<TM_TOMA__Task_Order__c>();
        
        TM_TOMA__Task_Order__c taskOrder1 = new TM_TOMA__Task_Order__c();
        taskOrder1.Name = 'RFQ1074470';
        taskOrder1.TM_TOMA__Task_Order_Title__c = 'title';
        taskOrder1.TM_TOMA__Customer_Agency__c = accountList.get(0).Id;
        taskOrder1.TM_TOMA__Contracting_Officer__c = contactList.get(0).Id;
        //taskOrder1.Primary_Brand__c = 'Mission';
        //taskOrder1.Industry__c = 'Civil';
       // taskOrder1.Brand_Category__c = 'Mission (NMS)';
        taskOrder1.TM_TOMA__Contract_Vehicle__c = contract.Id;
        taskOrder1.TM_TOMA__Due_Date__c = Date.today();
       // taskOrder1.Proposal_Due_Date__c = Date.today();
        taskOrder1.TM_TOMA__Description__c = 'Description';
        taskOrderList.add(taskOrder1);
        
      /*  TM_TOMA__Task_Order__c taskOrder2 = new TM_TOMA__Task_Order__c();
        taskOrder2.Name = 'rfqId';
        taskOrder2.TM_TOMA__Task_Order_Title__c = 'title';
        taskOrder2.TM_TOMA__Customer_Agency__c = accountList.get(1).Id;
        taskOrder2.TM_TOMA__Contracting_Officer__c = contactList.get(1).Id;
        taskOrder2.Primary_Brand__c = 'Mission';
        taskOrder2.Industry__c = 'Civil';
        taskOrder2.Brand_Category__c = 'Mission (NMS)';
        taskOrder2.TM_TOMA__Contract_Vehicle__c = contract.Id;
        taskOrder2.TM_TOMA__Due_Date__c = Date.today();
        taskOrder2.Proposal_Due_Date__c = Date.today();
        taskOrder2.TM_TOMA__Description__c = 'Description';        
        taskOrderList.add(taskOrder2);*/
        return taskOrderList;
        
    }
    
    /*
    public static User createUser(){
        User user = new User();
        user.FirstName = 'User First Name';
        user.LastName = 'User Last Name';
        user.Alias = 'abc@technomile.com';
        user.Username = 'abc@technomile.com';
        //user.
        return user;
    }*/
    
}