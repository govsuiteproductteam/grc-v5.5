public with sharing class CheckFedCLMLicenseTriggerHelper{
    
    public static void checkLicenseOnUpdateForFedCLM(Map<Id,User> FedCLMUserMap){
        List<User> userList = FedCLMUserMap.values();
        
        TM_License_Key_Setting__c customSetting = TM_License_Key_Setting__c.getOrgDefaults();
        String FedCLMKey = customSetting.FedCLM_Key__c;
        EncryptionDecryptionManager.KeyInfo keyInfo;
        if(FedCLMKey != null){
            String decriptedKey = EncryptionDecryptionManager.decryptValue('FedCLM',FedCLMKey );
            keyInfo = EncryptionDecryptionManager.getKeyInfo(decriptedKey );
        }
        else{
            for(User usr : userList ){             
                    usr.addError('Please enter your FedCLM product key into the TM License Key Setting under custom settings.');
            }
            return;
        }
        try{
            if(keyInfo != null && keyInfo.StartDate != null && keyInfo.EndDate != null && keyInfo.organizationId != null && 
                       keyInfo.licenseCount != null && keyInfo.packageName != null){
                List<Organization> orgList = [Select Id From Organization where id =: keyInfo.organizationId];
                System.debug('keyInfo != null = '+keyInfo != null);
                System.debug('!orgList.isEmpty() =  '+!orgList.isEmpty());
                System.debug('keyInfo.StartDate <= Date.today() = '+(keyInfo.StartDate <= Date.today()));
                System.debug('keyInfo.EndDate >= Date.today() = '+(keyInfo.EndDate >= Date.today()));
                if(keyInfo != null && !orgList.isEmpty() &&  keyInfo.StartDate <= Date.today() && keyInfo.EndDate >= Date.today()){
                    //List<user> userListFedCap = [SELECt Id FROM User WHERE Subscribed_Apps__c includes('FedCLM')];
                    Integer fedCapCounter = [Select count() FROM User WHERE Subscribed_Apps__c includes('FedCLM') and id not In : FedCLMUserMap.keyset() And isActive = true];
                   
                    System.debug('fedCapCounter = '+fedCapCounter );
                    
                    if((fedCapCounter + userList.size()) > keyInfo.licenseCount){
                        for(User user : userList){
                            user.addError('FedCLM license limit has been exceeded. You currently have '+fedCapCounter+' out of '+keyInfo.licenseCount+' licenses assigned.');
                        }
                      
                    }  
                    System.debug('fedCapCounter = '+fedCapCounter );
                   
                    
                }              
                else{
                    for(User user : userList){
                        user.addError('The FedCLM product key entered in the custom settings is not valid or has expired.');
                    }
                }
            }
            else{
                    for(User user : userList){
                        user.addError('Please enter your FedCLM product key into the TM License Key Setting under custom settings.');
                    }
                
               
            }
        }catch(Exception e){ 
           
            for(User user : userList){
                user.addError('Please enter your FedCLM product key into the TM License Key Setting under custom settings.');   
            }
        }
    }
    
    public static void checkLicenseOnInsertForFedCLM(List<User> userList){
        
        TM_License_Key_Setting__c customSetting = TM_License_Key_Setting__c.getOrgDefaults();
        String FedCLMKey = customSetting.FedCLM_Key__c;
        EncryptionDecryptionManager.KeyInfo keyInfo;
        if(FedCLMKey != null){
            String decriptedKey = EncryptionDecryptionManager.decryptValue('FedCLM',FedCLMKey );
            keyInfo = EncryptionDecryptionManager.getKeyInfo(decriptedKey );
        }
        else{
            for(User usr : userList ){             
                    usr.addError('Please enter your FedCLM product key into the TM License Key Setting under custom settings.');
            }
            return;
        }
        try{
            if(keyInfo != null && keyInfo.StartDate != null && keyInfo.EndDate != null && keyInfo.organizationId != null && 
                       keyInfo.licenseCount != null && keyInfo.packageName != null){
                List<Organization> orgList = [Select Id From Organization where id =: keyInfo.organizationId];
                System.debug('keyInfo != null = '+keyInfo != null);
                System.debug('!orgList.isEmpty() =  '+!orgList.isEmpty());
                System.debug('keyInfo.StartDate <= Date.today() = '+(keyInfo.StartDate <= Date.today()));
                System.debug('keyInfo.EndDate >= Date.today() = '+(keyInfo.EndDate >= Date.today()));
                if(keyInfo != null && !orgList.isEmpty() &&  keyInfo.StartDate <= Date.today() && keyInfo.EndDate >= Date.today()){
                    //List<user> userListFedCap = [SELECt Id FROM User WHERE Subscribed_Apps__c includes('FedCLM')];
                    Integer fedCapCounter = [Select count() FROM User WHERE Subscribed_Apps__c includes('FedCLM')  And isActive = true];
                   
                    System.debug('fedCapCounter = '+fedCapCounter );
                    
                    if((fedCapCounter + userList.size()) > keyInfo.licenseCount){
                        for(User user : userList){
                            user.addError('FedCLM license limit has been exceeded. You currently have '+fedCapCounter+' out of '+keyInfo.licenseCount+' licenses assigned.');
                        }
                      
                    }  
                    System.debug('fedCapCounter = '+fedCapCounter );
                   
                    
                }              
                else{
                    for(User user : userList){
                        user.addError('The FedCLM product key entered in the custom settings is not valid or has expired.');
                    }
                }
            }
            else{
                    for(User user : userList){
                        user.addError('Please enter your FedCLM product key into the TM License Key Setting under custom settings.');
                    }
                
               
            }
        }catch(Exception e){ 
           
            for(User user : userList){
                user.addError('Please enter your FedCLM product key into the TM License Key Setting under custom settings.');   
            }
        }
    }
}