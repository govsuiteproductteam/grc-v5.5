@isTest
public class TEISIIIEmailHandlerTest {
    
    public static testMethod void testTEISIIIForNewTaskOrder(){
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Contact con = TestDataGenerator.createContact('TestCon', acc);
        insert con;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('T-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'teisiii@4-xhdbsfy27w44ph0ojaeg2w4izfic5gskwu7t78xc19qvu50k3.37-pvateac.na31.apex.salesforce.com';
        
        insert conVehi;
        
        Messaging.InboundEmail email  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        List<StaticResource> srList = [SELECT Id, Body FROM StaticResource WHERE Name='TEISIIIEmailTest'];
        
        email.plainTextBody = srList.get(0).Body.toString();
        email.fromAddress ='test@test.com';
        email.fromName = 'ABC XYZ';
        email.subject = 'Request for Proposal for PWS 2017005 (UNCLASSIFIED)';
        email.toaddresses = new List<String>();
        email.toaddresses.add('teisiii@4-xhdbsfy27w44ph0ojaeg2w4izfic5gskwu7t78xc19qvu50k3.37-pvateac.na31.apex.salesforce.com');
        
        TEISIIIEmailHandler handler = new TEISIIIEmailHandler();
        Test.startTest();
        Messaging.InboundEmailResult result = handler.handleInboundEmail(email, env);
        Test.stopTest();
        
        List<Task_Order__c> taskOrderList = [SELECT Id, Name, Contract_Specialist__r.Name FROM Task_Order__c];
        Task_Order__c taskOrder = taskOrderList.get(0);
        System.assertEquals(taskOrderList.size(), 1);
        System.assertEquals('PWS 2017005', taskOrder.Name);
        System.assertEquals('ABC XYZ', taskOrder.Contract_Specialist__r.Name);
    }
    
    public static testMethod void testTEISIIIForExistingTaskOrder(){
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Contact con = TestDataGenerator.createContact('TestCon', acc);
        insert con;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('T-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'teisiii@4-xhdbsfy27w44ph0ojaeg2w4izfic5gskwu7t78xc19qvu50k3.37-pvateac.na31.apex.salesforce.com';
        
        insert conVehi;
        
        Messaging.InboundEmail email1  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env1 = new Messaging.InboundEnvelope();
        List<StaticResource> srList1 = [SELECT Id, Body FROM StaticResource WHERE Name='TEISIIIEmailTest'];
        
        email1.plainTextBody = srList1.get(0).Body.toString();
        email1.fromAddress ='test@test.com';
        email1.fromName = 'ABC XYZ';
        email1.subject = 'Request for Proposal for PWS 2017005 (UNCLASSIFIED)';
        email1.toaddresses = new List<String>();
        email1.toaddresses.add('teisiii@4-xhdbsfy27w44ph0ojaeg2w4izfic5gskwu7t78xc19qvu50k3.37-pvateac.na31.apex.salesforce.com');
        
        //For Updating existing task order with existing Contact
        Messaging.InboundEmail email2  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env2 = new Messaging.InboundEnvelope();
        List<StaticResource> srList2 = [SELECT Id, Body FROM StaticResource WHERE Name='TEISIIIEmailTestUpdated'];
        email2.plainTextBody = srList2.get(0).Body.toString();
        email2.fromAddress ='test@test.com';
        email2.fromName = 'ABC XYZ';
        email2.subject = 'Fwd: Request for Proposal for PWS 2017005 (UNCLASSIFIED)';
        email2.toaddresses = new List<String>();
        email2.toaddresses.add('teisiii@4-xhdbsfy27w44ph0ojaeg2w4izfic5gskwu7t78xc19qvu50k3.37-pvateac.na31.apex.salesforce.com');
        
        TEISIIIEmailHandler handler = new TEISIIIEmailHandler();
        Test.startTest();
        Messaging.InboundEmailResult result1 = handler.handleInboundEmail(email1, env1);
        Messaging.InboundEmailResult result2 = handler.handleInboundEmail(email2, env2);
        Test.stopTest();
        
        List<Task_Order__c> taskOrderList = [SELECT Id, Name, Due_Date__c, Questions_Due_Date__c FROM Task_Order__c];
        Task_Order__c taskOrder = taskOrderList.get(0);
        System.assertEquals(taskOrderList.size(), 1);
        System.assertEquals('PWS 2017005', taskOrder.Name);
        
        List<Contract_Mods__c> contractModList = [SELECT Id, New_Due_Date__c, New_Questions_Due_Date__c FROM Contract_Mods__c];
        Contract_Mods__c contractMod = contractModList.get(0);
        System.assertEquals(1, contractModList.size());
        System.assertEquals(taskOrder.Due_Date__c, contractMod.New_Due_Date__c);
        System.assertEquals(taskOrder.Questions_Due_Date__c, contractMod.New_Questions_Due_Date__c);
    }
    
    public static testMethod void testTEISIIIForNewContactOnExistingTaskOrder(){
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Contact con = TestDataGenerator.createContact('TestCon', acc);
        insert con;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('T-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'teisiii@4-xhdbsfy27w44ph0ojaeg2w4izfic5gskwu7t78xc19qvu50k3.37-pvateac.na31.apex.salesforce.com';
        
        insert conVehi;
        
        Messaging.InboundEmail email1  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env1 = new Messaging.InboundEnvelope();
        List<StaticResource> srList1 = [SELECT Id,Body FROM StaticResource WHERE Name='TEISIIIEmailTest'];
        
        email1.plainTextBody = srList1.get(0).Body.toString();
        email1.fromAddress ='test@test.com';
        email1.fromName = 'ABC XYZ';
        email1.subject = 'Request for Proposal for PWS-2017005 (UNCLASSIFIED)';
        email1.toaddresses = new List<String>();
        email1.toaddresses.add('teisiii@4-xhdbsfy27w44ph0ojaeg2w4izfic5gskwu7t78xc19qvu50k3.37-pvateac.na31.apex.salesforce.com');
        
        //For Updating existing task order with existing Contact
        Messaging.InboundEmail email2  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env2 = new Messaging.InboundEnvelope();
        List<StaticResource> srList2 = [SELECT Id,Body FROM StaticResource WHERE Name='TEISIIIEmailTestUpdated_1'];
        email2.plainTextBody = srList2.get(0).Body.toString();
        email2.fromAddress ='test@test.com';
        email2.fromName = 'ABC XYZ';
        //email2.subject = 'Re: Request for Proposal for PWS-2017005 (UNCLASSIFIED)';
        email2.subject = 'Re: Request for Proposal for PWS 2017005 (UNCLASSIFIED)';
        email2.toaddresses = new List<String>();
        email2.toaddresses.add('teisiii@4-xhdbsfy27w44ph0ojaeg2w4izfic5gskwu7t78xc19qvu50k3.37-pvateac.na31.apex.salesforce.com');
        
        TEISIIIEmailHandler handler = new TEISIIIEmailHandler();
        Test.startTest();
        Messaging.InboundEmailResult result1 = handler.handleInboundEmail(email1, env1);
        Messaging.InboundEmailResult result2 = handler.handleInboundEmail(email2, env2);
        Test.stopTest();
        
        List<Task_Order__c> taskOrderList = [SELECT Id, Name, Contracting_Officer__r.Name FROM Task_Order__c];
        Task_Order__c taskOrder = taskOrderList.get(0);
        
        System.assertEquals(taskOrderList.size(), 1);
        //System.assertEquals('PWS-2017005', taskOrder.Name);
        //System.assertEquals(null, taskOrder.Contracting_Officer__r.Name);
    }
    
    
}