public with sharing class TM_AddLabourCategoryController {
    
    public List<CLM_Labor_Category__c> clmLabourCategoryList{get; set;} 
    public List<CLM_Labor_Category__c> deleteCLMLabourCategoryList{get; set;} 
    public String projectId{get; set;}
    public Project__c project{get; set;} 
    public Boolean isEdit{get;set;}
    
    public TM_AddLabourCategoryController(ApexPages.StandardController controller) {
        isEdit = false;
        projectId = ApexPages.CurrentPage().getparameters().get('id');
        clmLabourCategoryList = new List<CLM_Labor_Category__c>();
        deleteCLMLabourCategoryList = new List<CLM_Labor_Category__c>();
        init();
    }    
    
    private void init(){
        if(Schema.sObjectType.CLM_Labor_Category__c.isAccessible() && Schema.sObjectType.Project__c.isAccessible()){
        if(projectId != null){
            clmLabourCategoryList = [Select Id,Name,Req_Education_Level__c,Contract_Vehicle__c,Project__c ,Status__c,Req_years_of_experience__c
                                     From CLM_Labor_Category__c where Project__c =: projectId ];
            
            List<Project__c> projectList = [Select Id,Contract_Vehicle__c,Notify_PM__c From Project__c where id =: projectId Limit 1];
            
            if(projectList != null && !projectList.isEmpty() ){
                project = projectList[0];
            }
            
        }
        }
    }
    public void addContValRow(){
        if (Schema.sObjectType.CLM_Labor_Category__c.isAccessible()){
            CLM_Labor_Category__c clmLabourCategory = new CLM_Labor_Category__c();
            if(project != null){ 
                if (Schema.sObjectType.CLM_Labor_Category__c.fields.Contract_Vehicle__c.isAccessible() && Schema.sObjectType.CLM_Labor_Category__c.fields.Contract_Vehicle__c.isCreateable()) 
                    clmLabourCategory.Contract_Vehicle__c = project.Contract_Vehicle__c;
                if (Schema.sObjectType.CLM_Labor_Category__c.fields.Project__c.isAccessible() && Schema.sObjectType.CLM_Labor_Category__c.fields.Project__c.isCreateable()) 
                    clmLabourCategory.Project__c = Project.id;
            }
            if (Schema.sObjectType.CLM_Labor_Category__c.fields.Status__c.isAccessible() && Schema.sObjectType.CLM_Labor_Category__c.fields.Status__c.isCreateable()) 
                clmLabourCategory.Status__c = 'Unfilled';
            clmLabourCategoryList.add(clmLabourCategory );
        }
    }
    
    public void removeContValRow(){
        if(deleteCLMLabourCategoryList == null){
            deleteCLMLabourCategoryList  = new List<CLM_Labor_Category__c>();
        }
        Integer param = Integer.valueOf(Apexpages.currentpage().getParameters().get('index'));
        if(param >= 0){
            CLM_Labor_Category__c  clmCategory = clmLabourCategoryList.remove(param);
            if(clmCategory .id != null){
                deleteCLMLabourCategoryList.add(clmCategory );
            }
        }    
    }
    
    public PageREference notifyPM(){
        if(project != null){
            if(Schema.sObjectType.Project__c.fields.Notify_PM__c.isAccessible() && Schema.sObjectType.Project__c.fields.Notify_PM__c.isUpdateable())
                project.Notify_PM__c = !project.Notify_PM__c ;
            try{
                if(project.Id == null){
                    if (Schema.sObjectType.Project__c.isCreateable())
                        DMLManager.upsertAsUser(project);
                }else{
                    if (Schema.sObjectType.Project__c.isUpdateable())
                        DMLManager.upsertAsUser(project);
                }
                
                
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Nofied to Project Manager.'));
            }
            catch(Exception ex){
            }
        }
        return null;
    }
    
    public void save(){
        try{
            if (Schema.sObjectType.CLM_Labor_Category__c.isAccessible()){
                if(!clmLabourCategoryList.isEmpty()){
                    if (Schema.sObjectType.CLM_Labor_Category__c.isCreateable() || Schema.sObjectType.CLM_Labor_Category__c.isUpdateable())
                    DMLManager.upsertAsUser(clmLabourCategoryList);
                }
                if(!deleteCLMLabourCategoryList.isEmpty()){
                    if (Schema.sObjectType.CLM_Labor_Category__c.isDeletable())
                        DMLManager.deleteAsUser(deleteCLMLabourCategoryList);
                }
                
                init();
            }
        }
        catch(Exception ex){
        }
        isEdit = false;
    }
    
    public void edit(){
        isEdit = true;
    }
    
}