global class TM_AutoInsertTORelatedList {
    
    @InvocableMethod(label='Create Partner And Contact' description='Auto create Teaming Partners and TO Contact on creation of TO.')
    global static void insertTeamingPartnerAndTOContact(List<Id> taskOrderIdList){
        if(Schema.sObjectType.Task_Order__c.isAccessible() && Schema.sObjectType.Task_Order_Contact__c.isAccessible() && Schema.sObjectType.Contract_Vehicle__c.isAccessible() && Schema.sObjectType.Contract_Vehicle_Contact__c.isAccessible() && Schema.sObjectType.Vehicle_Partner__c.isAccessible() && Schema.sObjectType.Teaming_Partner__c.isAccessible() && Schema.sObjectType.Task_Order_Contact__c.isAccessible()){ 
        Id taskOrderId = taskOrderIdList[0];
        try{
            List<Task_Order__c> taskOrderList = [SELECT Id,Name,Contract_Vehicle__c From Task_Order__c WHERE Id =:taskOrderId LIMIT 1];
            if(!taskOrderList.isEmpty())
            {
                Id contractVehicleId = taskOrderList.get(0).Contract_Vehicle__c;
                if(contractVehicleId != null){
                    List<Contract_Vehicle__c> contractVehicleList = [SELECT Id,(SELECT Id,contact__c,Main_POC__c,Receives_Mass_Emails__c  FROM Contract_Vehicle_Contacts__r where contact__c != null ANd (Main_POC__c = true OR Receives_Mass_Emails__c = true))FROM Contract_Vehicle__c WHERE Id =:contractVehicleId];
                    if(!contractVehicleList.isEmpty()){                                              
                        List<Contract_Vehicle_Contact__c> contractVehicleContactList = contractVehicleList.get(0).Contract_Vehicle_Contacts__r;                        
                        //System.debug(''+contractVehicleContactList);
                        if(!contractVehicleContactList.isEmpty() ){
                            insertTaskOrderContact(contractVehicleContactList, taskOrderId);  
                                                        
                            List<Id> contactIds = new List<Id>();                            
                            for(Contract_Vehicle_Contact__c cvContact : contractVehicleContactList){
                                contactIds.add(cvContact.Contact__c);
                            }
                            
                            Set<Id> partnerIds = new Set<Id>();
                            for(Contact contact : [SELECT Id,AccountId FROM Contact WHERE Id In: contactIds]){
                                partnerIds.add(contact.AccountId);
                            }
                            List<Vehicle_Partner__c> vehiclePartnerList = [SELECT Id,Name,Mass_Email_Address__c,Partner__c FROM Vehicle_Partner__c WHERE Contract_Vehicle__c=:contractVehicleId AND Partner__c IN : partnerIds];
                            if(!vehiclePartnerList.isEmpty()){
                                insertTeamingPartner(vehiclePartnerList, taskOrderId);
                            }
                        }
                    }
                }
            }
        }catch(Exception e){
            System.debug(''+e.getMessage()+'\t'+e.getLineNumber());
        }
        
    }}
    
    private static void insertTeamingPartner(List<Vehicle_Partner__c> vehiclePartnerList,Id taskOrderId){
        List<Teaming_Partner__c> teamingPartnerList = new List<Teaming_Partner__c>();
        for(Vehicle_Partner__c vehiclePartner : vehiclePartnerList){
            Teaming_Partner__c teamingPartner = new Teaming_Partner__c();
            teamingPartner.Task_Order__c = taskOrderId;
            teamingPartner.Vehicle_Partner__c = vehiclePartner.Id;
            teamingPartnerList.add(teamingPartner);
        }
        if(!teamingPartnerList.isEmpty())
            DMLManager.insertAsUser(teamingPartnerList);
    }
    
    private static void insertTaskOrderContact(List<Contract_Vehicle_Contact__c> contractVehicleContactList,Id taskOrderId){
        //System.debug(''+taskOrderId);
        List<Task_Order_Contact__c> taskOrderContactList = new List<Task_Order_Contact__c>();
        for(Contract_Vehicle_Contact__c cvContact : contractVehicleContactList){
            Task_Order_Contact__c taskOrderContact = new Task_Order_Contact__c();
            taskOrderContact.Task_Order__c = taskOrderId;
            if(cvContact.Contact__c != null)
                taskOrderContact.Contact__c = cvContact.Contact__c;
            if(cvContact.Receives_Mass_Emails__c != null)
                taskOrderContact.Receives_Mass_Emails__c = cvContact.Receives_Mass_Emails__c;
            if(cvContact.Main_POC__c != null)
                taskOrderContact.Main_POC__c = cvContact.Main_POC__c;
            taskOrderContactList.add(taskOrderContact);
        }
        if(!taskOrderContactList.isEmpty())
            DMLManager.insertAsUser(taskOrderContactList);
    }
}