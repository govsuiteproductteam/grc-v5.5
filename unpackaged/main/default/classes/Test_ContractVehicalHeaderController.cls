@IsTest
public class Test_ContractVehicalHeaderController {
    static testMethod void testMethodContractVehicalHeaderController(){
        List<RecordType> rtypes = [Select Name, Id From RecordType where sObjectType='TM_TOMA__Contract_Vehicle__c' and isActive=true];
        string recId='';
        if(rtypes.size()>0)
        {
            recId=rtypes[0].Id;
        }
        CLM_AtaGlanceField__c newOppAtAGlance = TestDataGenerator.createAtaGlanceField(recId);
        newOppAtAGlance.Is_Contract__c=true;
        insert newOppAtAGlance;
        CLM_AtaGlanceField__c newOppAtAGlance1 = TestDataGenerator.createAtaGlanceField(recId);
        newOppAtAGlance1.Is_Contract__c=true;
        newOppAtAGlance1.FieldApiName__c='TM_TOMA__Contracting_Agency__c';
        newOppAtAGlance1.FieldLabel__c='Contracting Organization';
        newOppAtAGlance1.Order__c=2;
        insert newOppAtAGlance1; 
        CLM_AtaGlanceField__c newOppAtAGlance2 = TestDataGenerator.createAtaGlanceField(recId);
        newOppAtAGlance2.Is_Contract__c=true;
        newOppAtAGlance2.FieldApiName__c='TM_TOMA__Test';
        newOppAtAGlance2.FieldLabel__c='TestComponent';
        newOppAtAGlance2.Is_Component__c=true;
        newOppAtAGlance2.Order__c=3;
        insert newOppAtAGlance2; 
        
        Account acc = TestDataGenerator.createAccount('testAccount');
        insert acc;
        
        Contract_Vehicle__c ConVeh = TestDataGenerator.createContractVehicle('Test contract vehicle', acc);
        ConVeh.recordTypeId = recId;
        ConVeh.Contract_Type__c= TestDataGenerator.getPicklistValue('TM_TOMA__Contract_Vehicle__c','TM_TOMA__Contract_Type__c');
        insert ConVeh;
        
        Contract_Vehicle__c newConVeh = ContractVehicalHeaderController.getContractVehicle(ConVeh.Id);
        System.assertEquals('Test contract vehicle',newConVeh.Name);
        
        string newOwnerId = ContractVehicalHeaderController.changeOwnerIdFromHeader(ConVeh.Id, '');
        System.assertEquals(Null, newOwnerId);
        
        List<ContractVehicalHeaderController.AtAGlanceWrap> atAGlanceList= ContractVehicalHeaderController.getMetadata(ConVeh.Id);
        System.assert(atAGlanceList.size()>0);
        
        
        Contract_Vehicle__c conVehical = ContractVehicalHeaderController.getContractVehicle('');
        System.assertEquals(null,conVehical);
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User usr = new User(Alias = 'standt', Email='user@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = p.Id, 
                            TimeZoneSidKey='America/Los_Angeles', UserName='user@testtechno.com');
        insert usr ;
        System.runAs(usr)
        {
            newOwnerId = ContractVehicalHeaderController.changeOwnerIdFromHeader(ConVeh.Id, usr.id);
            System.assertEquals(usr.id, newOwnerId);
        }
        
        ContractVehicalHeaderController.delContractVehicle(ConVeh.Id);
        ContractVehicalHeaderController.delContractVehicle('');
        ContractVehicalHeaderController.checkChatter();
        ContractVehicalHeaderController.checkLicensePermition1();
    }
}