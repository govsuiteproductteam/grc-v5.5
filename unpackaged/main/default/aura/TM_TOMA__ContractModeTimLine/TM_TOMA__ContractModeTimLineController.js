({
    doCheckLicense:function(component, event, helper){
        //component.set("v.checkSpinner", 'Yes');
        var action = component.get("c.getCheckLicensePermition");
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                var permission=a.getReturnValue();
                component.set("v.CheckLicense", permission);                
             }
            else if (a.getState() === "ERROR") {
                $A.log("Errors", a.getError());
                //component.set("v.checkSpinner", 'No');
            }   
            
        });
        
        $A.enqueueAction(action);   
        
    },
    doInit : function(component, event, helper) {
            //component.set("v.checkSpinner", 'Yes');
        var action = component.get("c.getContractModeList");
        action.setParams({
            "tskId" : component.get("v.recordId")
        });
        
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                
                var conModeList=a.getReturnValue();
                
                component.set("v.contractModeList", conModeList);
                 //component.set("v.checkSpinner", 'No');
                
                
            }
            else if (a.getState() === "ERROR") {
                $A.log("Errors", a.getError());
               // component.set("v.checkSpinner", 'No');
            }
            
            
            
        });
        $A.enqueueAction(action);  
    },
    hideTop : function(component, event, helper) {
      var topDiv=component.find("topdiv");
        $A.util.addClass(topDiv,"slds-hide");
        var bottomDiv=component.find("bottom");
        $A.util.removeClass(bottomDiv,"slds-hide");
        var selectedItem = event.currentTarget;
        var id = selectedItem.dataset.record;
        var action1 = component.get("c.timelineRec");
        action1.setParams({
            "timelineId" : id,
            "oppId" : component.get("v.recordId")
        });
        action1.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                var check=a.getReturnValue();
                component.set("v.apiList", a.getReturnValue());
            }
            else if (a.getState() === "ERROR") {
                $A.log("Errors", a.getError());
            }
        });
        $A.enqueueAction(action1);
      
        
    },
    backTop : function(component, event, helper) {
        var topDiv=component.find("topdiv");
        $A.util.removeClass(topDiv,"slds-hide");
        var bottomDiv=component.find("bottom");
        $A.util.addClass(bottomDiv,"slds-hide");
     },
})