@isTest
public class TM_MailSenderTest {
    public static testMethod void testMailSend(){
        Test.startTest();
        Account account = TestDataGenerator.createAccount('Test Account');
        insert account;
        Contact contact = TestDataGenerator.createContact('test', account);
        insert contact;
        Contract_Vehicle__c contractVehicle = TestDataGenerator.createContractVehicle('Test cv', account);
        insert contractVehicle;
        Task_Order__c taskOrder = TestDataGenerator.createTaskOrder('Test To-1001', account, contractVehicle);        
        insert taskOrder;
        Task_Order_Contact__c taskOrderContact = TestDataGenerator.createTaskOrderContact(taskOrder, contact);
        taskOrderContact.Main_POC__c = true;
        insert taskOrderContact;
        Attachment attachment = new Attachment();
        attachment.Name = 'Test Attachment';
        attachment.ParentId = taskOrder.Id;
        attachment.Body = Blob.valueOf('Test Data');
        insert attachment;
        Test.stopTest();
        
        TM_MailSender.sendMail(new List<Id>{taskOrder.Id});
        System.assert(taskOrderContact != null);
    }
}