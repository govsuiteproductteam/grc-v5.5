public with sharing class AddTaskOrderContactCompoController {
    
    @AuraEnabled
    public static String checkLicensePermition()
    {
        return LincenseKeyHelper.checkLicensePermitionForFedTOM();
    }
    
    @AuraEnabled
    public static List<Task_Order_Contact__c> getContractVehicalContact(Id fedCapOpportunityId)
    {
        List<Task_Order_Contact__c> taskOrderConList = new  List<Task_Order_Contact__c>();
        if(Schema.sObjectType.Task_Order_Contact__c.isAccessible()){
            if(fedCapOpportunityId!=Null)
            {
                taskOrderConList = [select Id,Name,Contact__c,Contact__r.Account.Id,Contact__r.Account.Name,Contact__r.Name,Contact__r.FirstName,Contact__r.LastName,Contact__r.Email,Task_Order__c,Main_POC__c,Receives_Mass_Emails__c from Task_Order_Contact__c where Task_Order__c =: fedCapOpportunityId ORDER BY Contact__r.Account.Name ASC Limit 1000];
            }
            
            return taskOrderConList;
        }
        else{
            return null;
        }
    }
    
    @AuraEnabled
    public static void deleteTaskOrderContact(Id taskOrderConId)
    {
        if(taskOrderConId!=Null)
        {
            if(Schema.sObjectType.Task_Order_Contact__c.isAccessible()){
            List<Task_Order_Contact__c> taskOrderConList = [select Id from Task_Order_Contact__c where id =: taskOrderConId];
            if(taskOrderConList.size() > 0)
            {
                DMLManager.deleteAsUser(taskOrderConList);
            }
            }
        }
        
    }
    @AuraEnabled
    public static List<TaskOrderContactWrap>  addTaskOrderContact (Id taskOrderId){
        List<TaskOrderContactWrap > TaskOrderContactWrapList = new List<TaskOrderContactWrap >();
        if(taskOrderId != null && Schema.sObjectType.Task_Order__c.isAccessible()){
            
            Task_Order__c taskOrder = [Select Id,Contract_Vehicle_picklist__c,Contract_Vehicle__c  From Task_Order__c Where id =: taskOrderId Limit 1];
            
            Set<ID> accIdSet = new Set<Id>();
            if(Schema.sObjectType.Teaming_Partner__c.isAccessible()){
                for(Teaming_Partner__c teamingaPartner : [Select Vehicle_Partner__r.Partner__c From Teaming_Partner__c where Task_Order__c =: taskOrder.id] ){
                    accIdSet.add(teamingaPartner.Vehicle_Partner__r.Partner__c );
                }
                
                if(Schema.sObjectType.Contact.isAccessible()){
                    List<Contact> contractContactList = [Select Id,Name,Account.Name,Email,AccountId From Contact where 
                                                         AccountId IN:accIdSet AND
                                                         ID NOT IN (Select Contact__c From Task_Order_Contact__c where Task_Order__c =: taskOrder.id) ORDER BY Name ASC Limit 1000];
                    
                    if(Schema.sObjectType.Teaming_Partner__c.isCreateable()){
                        for(Contact con : contractContactList ){
                            if(accIdSet.contains(con.AccountId)){
                                Task_Order_Contact__c taskOrderContact = new Task_Order_Contact__c();
                                taskOrderContact.Contact__r = con;
                                taskOrderContact.Contact__c = con.id;
                                taskOrderContact.Task_Order__r = taskOrder;
                                taskOrderContact.Task_Order__c = taskOrder.id ;
                                TaskOrderContactWrap taskOrderContactWrapper = new TaskOrderContactWrap(taskOrderContact );
                                TaskOrderContactWrapList.add(taskOrderContactWrapper );
                            }
                        }
                    }
                    System.debug(''+TaskOrderContactWrapList);
                    return TaskOrderContactWrapList;
                }
                
            }
            
        }
        return null;
    }
    
    @AuraEnabled
    public static List<TaskOrderContactWrap >addSearchTaskOrderContact(Id taskOrderId, string searchString){
        if(searchString != '' && searchString != Null){
            searchString+='%';
            List<TaskOrderContactWrap > TaskOrderContactWrapList = new List<TaskOrderContactWrap >();
            if(taskOrderId != null && Schema.sObjectType.Task_Order__c.isAccessible()){
                
                Task_Order__c taskOrder = [Select Id,Contract_Vehicle_picklist__c,Contract_Vehicle__c  From Task_Order__c Where id =: taskOrderId Limit 1];
                
                Set<ID> accIdSet = new Set<Id>();
                if(Schema.sObjectType.Teaming_Partner__c.isAccessible()){
                    for(Teaming_Partner__c teamingaPartner : [Select Vehicle_Partner__r.Partner__c From Teaming_Partner__c where Task_Order__c =: taskOrder.id] ){
                        accIdSet.add(teamingaPartner.Vehicle_Partner__r.Partner__c );
                    }
                    
                    if(Schema.sObjectType.Contact.isAccessible()){
                        List<Contact> contractContactList = [Select Id,Name,LastName,FirstName,Account.Name,Email,AccountId From Contact where 
                                                             AccountId IN:accIdSet AND
                                                             ID NOT IN (Select Contact__c From Task_Order_Contact__c where Task_Order__c =: taskOrder.id) AND (name LIKE :searchString OR LastName LIKE :searchString OR FirstName LIKE :searchString OR Account.Name LIKE :searchString OR Email LIKE :searchString) ORDER BY Name ASC Limit 1000];
                        
                        if(Schema.sObjectType.Teaming_Partner__c.isCreateable()){
                            for(Contact con : contractContactList ){
                                if(accIdSet.contains(con.AccountId)){
                                    Task_Order_Contact__c taskOrderContact = new Task_Order_Contact__c();
                                    taskOrderContact.Contact__r = con;
                                    taskOrderContact.Contact__c = con.id;
                                    taskOrderContact.Task_Order__r = taskOrder ;
                                    taskOrderContact.Task_Order__c = taskOrder.id ;
                                    TaskOrderContactWrap taskOrderContactWrapper = new TaskOrderContactWrap(taskOrderContact );
                                    TaskOrderContactWrapList.add(taskOrderContactWrapper );
                                }
                            }
                        }
                        return TaskOrderContactWrapList;
                    }
                    
                }
                
            }
            return null;
        }
        else{
            return null;
        }
    }
    
    
    
    @AuraEnabled
    public static void saveTaskOrderContact(string taskOrderContactList)
    {
        if(Schema.sObjectType.Task_Order_Contact__c.isAccessible()){
        List<Task_Order_Contact__c> taskOrderConList = new List<Task_Order_Contact__c>();
        if(taskOrderContactList != null && taskOrderContactList.length()>1)
        {
            taskOrderContactList =taskOrderContactList.substring(1);
            List<String> taskorderConstrList = taskOrderContactList.split('SPLITPARENT');
            if(taskorderConstrList.size() > 0)
            {
                for(String temp : taskorderConstrList){
                    System.debug(''+temp);
                    Task_Order_Contact__c taskOrderCon = new Task_Order_Contact__c();
                    taskOrderCon = getTaskOrderContact(temp);
                    System.debug(''+taskOrderCon);
                    taskOrderConList.add(taskOrderCon);
                }
            }         
            
            if(!taskOrderConList.isEmpty()){
                try{
                    DMLManager.updateAsUser(taskOrderConList);
                }catch(Exception e){
                    System.debug('Exception '+e);
                }
            }
        }
        }
    }
    
    private static Task_Order_Contact__c getTaskOrderContact(String taskOrderStr){
        if(taskOrderStr != null && taskOrderStr.trim().length() > 0){
            String[] taskOrderStrList = taskOrderStr.split('SPLITDATA');
            if(taskOrderStrList.size() > 0){
                if(Schema.sObjectType.Task_Order_Contact__c.isCreateable()){
                Task_Order_Contact__c taskOrderContact = new Task_Order_Contact__c();
                if(taskOrderStrList[0] != 'undefined'){
                    taskOrderContact.id = taskOrderStrList[0];
                } 
                if(taskOrderStrList.size() > 1){
                    taskOrderContact.Main_POC__c = Boolean.valueOf(taskOrderStrList[1]);
                }
                if(taskOrderStrList.size() > 2){
                    taskOrderContact.Receives_Mass_Emails__c = Boolean.valueOf(taskOrderStrList[2]);
                }
                return taskOrderContact;
                }
            }
        }
        return null;
    }
    
    @AuraEnabled
    public static void addNewTaskOrderContact(string taskOrderConList){
        List<Task_Order_Contact__c> taskOrderContactList = new List<Task_Order_Contact__c>();
        if(taskOrderConList != null && taskOrderConList.length()>1)
        {
            taskOrderConList =taskOrderConList.substring(1);
            List<String> taskOrderConStrList = taskOrderConList.split('SPLITPARENT');
            if(taskOrderConStrList.size() > 0)
            {
                for(String temp : taskOrderConStrList){
                    Task_Order_Contact__c taskOrderContact = new Task_Order_Contact__c();
                    taskOrderContact = getContractVehicleCon(temp);
                    taskOrderContactList.add(taskOrderContact);
                }
            }         
            
            if(!taskOrderContactList.isEmpty()){
                try{
                    DMLManager.insertAsUser(taskOrderContactList);
                }catch(Exception e){
                    System.debug('Exception '+e);
                }
            }
        }
    }
    
    private static Task_Order_Contact__c getContractVehicleCon(String taskOrderContStr){
        if(Schema.sObjectType.Task_Order_Contact__c.isAccessible()){
        if(taskOrderContStr != null && taskOrderContStr.trim().length() > 0){
            String[] taskOrderContList = taskOrderContStr.split('SPLITDATA');
            if(taskOrderContList.size() > 0){
                if(Schema.sObjectType.Task_Order_Contact__c.isCreateable()){
                    Task_Order_Contact__c taskOrdVehCon = new Task_Order_Contact__c();
                    if(!taskOrderContList[0].equalsIgnoreCase('undefined')){
                        taskOrdVehCon.Contact__c = taskOrderContList[0];
                    }   
                    if(taskOrderContList.size() > 1){
                        taskOrdVehCon.Task_Order__c = taskOrderContList[1];
                    }
                    if(taskOrderContList.size() > 2){
                        taskOrdVehCon.Main_POC__c = Boolean.valueOf(taskOrderContList[2]);
                    }
                    if(taskOrderContList.size() > 3){
                        taskOrdVehCon.Receives_Mass_Emails__c = Boolean.valueOf(taskOrderContList[3]);
                    }
                    return taskOrdVehCon;
                }
            }
        }
        }
        return null;
    }
    
    
    
    
    /*  public class TaskOrderContactWrap{
@AuraEnabled
public Task_Order_Contact__c taskOrderContact{get; set;}
@AuraEnabled
public Boolean status{get; set;}

public TaskOrderContactWrap(Task_Order_Contact__c taskOrderContact){
this.taskOrderContact = taskOrderContact;
status = false;
}
}*/
}