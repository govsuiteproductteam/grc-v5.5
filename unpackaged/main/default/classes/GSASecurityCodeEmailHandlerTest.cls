@isTest
public class GSASecurityCodeEmailHandlerTest {
    public static testmethod void testCase1(){
		Test.startTest();        
        Messaging.InboundEmail email  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();       
        email.plainTextBody = 'Your One-Time Security Code is 112211 Within 30 minutes \n Confirmation code: 11223344 To allow';
        email.fromAddress ='test@test.com';
        email.fromName = 'ABC XYZ';
        email.subject = 'test subject';
        email.toaddresses = new List<String>();
        email.toaddresses.add('gsasecuritycode@1kin1g2nukqhkdk7bg3acq6giycmjm9rwypvifan4t8co6utto.61-zhsweao.na34.apex.salesforce.com');
        GSASecurityCodeEmailHandler gSASecurityCodeEmailHandler = new GSASecurityCodeEmailHandler();
        Messaging.InboundEmailResult result = gSASecurityCodeEmailHandler.handleInboundEmail(email, env);
        
        //added by Mai 8/1/2018
        Messaging.InboundEmail email2  = new Messaging.InboundEmail();       
        email2.htmlBody = 'We have generated a One-Time Security Code to verify your identity as an authorized user.<br><br><b>Your One-Time Security Code is 024236 </b> <br><br> Within 30 minutes of receiving this email, please enter the One-Time Security Code at the prompt.';
        email2.plainTextBody = null;
        email2.fromAddress ='test@test.com';
        email2.fromName = 'ABC XYZ';
        email2.subject = 'test subject';
        email2.toaddresses = new List<String>();
        email2.toaddresses.add('gsasecuritycode@1kin1g2nukqhkdk7bg3acq6giycmjm9rwypvifan4t8co6utto.61-zhsweao.na34.apex.salesforce.com');
        GSASecurityCodeEmailHandler gSASecurityCodeEmailHandler2 = new GSASecurityCodeEmailHandler();
        Messaging.InboundEmailResult result2 = gSASecurityCodeEmailHandler.handleInboundEmail(email2, env);
        
        Test.stopTest();
    }
}