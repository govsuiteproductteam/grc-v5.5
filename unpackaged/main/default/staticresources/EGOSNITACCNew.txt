The Department of Health and Human Services, NIH, NHGRI released RFQ for IT Assessment. 
Please visit e-GOS at https://cio.egos.nih.gov for detailed information. 

E-GOS ID: CS-43499
Request: RFQ
Title: 15 inch Macbook Pro Retina Display with Touch bar
Description: Hardware Specs 2.8GHz quad-core 7th-generation Intel Core i7
processorTurbo Boost up to 3.8GHz16GB 2133MHz LPDDR3 memory512
SSD storageRadeon Pro 555 with 2GB memoryFour Thunderbolt 3
portsTouch Bar and Touch IDSize and WeightHeight: 0.61 inch (1.55
cm)Width: 13.75 inches (34.93 cm)Depth: 9.48 inches (24.07 cm)Weight: 4.02
pounds (1.83 kg)
Government Customer: Department of Health and Human Services, NIH,
NHGRI

Proposals are due by 03/03/2018 5:00 PM Eastern Standard Time. 
______________________________________________________ 
nitaac.nih.gov
Phone: 1.888.773.6542
NITAACsupport@nih.gov
6011 Executive Boulevard, Suite 501
Rockville, Maryland 20852