({

    callFunction:function(component,event,helper){
        $("#mydiv").focusout(function() {
            var lookupList = component.find("lookuplist");
            $A.util.addClass(lookupList, 'hideClass');
        });
    },
    doInit:function(component,event,helper){  
       
      var recId=component.get("v.recordId");
        console.log("DInit recId  = "+recId);
        
        var sObjType= component.get("v.sObjectType");
        console.log("DInit sObjType  = "+sObjType);
        var lookupId=component.get("v.lookUpId");
        console.log("DINT "+lookupId);
        var recId=component.get("v.recordId");
        if(typeof recId === "undefined" && recId === null){
            recId='';
            component.set("v.recordId",recId);
        }
        if(typeof sObjType != "undefined" && sObjType != null && typeof lookupId != "undefined" && lookupId != null){
           
        var lookupName=component.get("c.getNameFromId");
        lookupName.setParams({
            "sObjectType":sObjType,
            "objectId":lookupId
        });
        lookupName.setCallback(this,function(data){
            
            var state= data.getState();
            
            if(state=="SUCCESS"){
                if(data.getReturnValue() != null){
                    //alert('data.getReturnValue'+data.getReturnValue().Name);
                    var searchStr=data.getReturnValue().Name;
                    component.set("v.searchString", searchStr);
            // Show the Lookup pill
        var lookupPill = component.find("lookup-pill");
        $A.util.removeClass(lookupPill, 'hideClass');
        
        // Lookup Div has selection
        var inputElement = component.find('lookup-div');
        $A.util.addClass(inputElement, 'slds-has-selection'); 
        }
                }
                
            
            
        });
        $A.enqueueAction(lookupName);
        }
    },
    hideLookuplist :function(component,event,helper){ 
       // alert('checkEvent');
       //helper.doSelection(component, event);
         var lookupList = component.find("lookuplist");
           $A.util.addClass(lookupList, 'hideClass');
    },
    doInitMatchingResult: function(cmp,event,helper) {
      
        var sObjectAPIName = cmp.get('v.sObjectAPIName');
        
        // Create an Apex action
        var action = cmp.get("c.getDataForLookup");
     //console.log('inside doInitMatchingResult');
        action.setParams({"sObjectAPIName" : sObjectAPIName});
        
        // Define the callback function
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            // Callback succeeded
            if (cmp.isValid() && state === "SUCCESS")
            {
                // Get the search matches
               // console.log('inside if of success');
                 var lookupList = cmp.find("lookuplist");
                $A.util.removeClass(lookupList, 'hideClass');
                //console.log('after remove class methode');
                var matchingResult = response.getReturnValue();
                
                // If we have no matches, return
                if (matchingResult.length == 0)
                {
                    cmp.set('v.matchingResult', null);
                    return;
                }
                
                // Set the results in matchingResult attribute of component
              // console.log('--Result found--'+matchingResult);
                cmp.set('v.matchingResult', matchingResult);
                var lookupList = cmp.find("lookuplist");
                    $($('#'+cmp.get("v.lookupCompId"))).removeClass('hideClass')
               console.log('--Result found2--'+cmp.get('v.matchingResult'));
            }
            else if (state === "ERROR") // Handle any error 
            {
                var errors = response.getError();
                
                if (errors) 
                {
                    if (errors[0] && errors[0].message) 
                    {
                        this.displayErrorDialog('Error', errors[0].message);
                    }
                }
                else
                {
                    this.displayErrorDialog('Error', 'Unknown error.');
                }
            }
        });
        
        // Enqueue the action 
        $A.enqueueAction(action); 
    },
    /**
 * Search an SObject for a match
 */
    search : function(cmp, event, helper) {
       // console.log('--Inside Search method of Controller---' + event.getSource());
        helper.doSearch(cmp); 
    },
    
    /**
 * Select an SObject from a list
 */
    select: function(cmp, event, helper) {
      //  console.log('--Inside selection method of Controller---' + event.currentTarget.id);
        helper.doSelection(cmp, event);
    },
    
    /**
 * Clear the currently selected SObject
 */
    clear: function(cmp, event, helper) {
        // helper.clearSelection(cmp); 
      // alert(event.currentTarget);
       if(typeof event.currentTarget !='undefined'){
          // alert(event.currentTarget);
          // console.log('here in if of clear');
            helper.clearSelection(cmp);
        }else{
         // alert(event.currentTarget);
            setTimeout( 
                function () {
                     // console.log('here in else of clear');
             //  var lookupList = cmp.find("lookuplist");
                    $($('#'+cmp.get("v.lookupCompId"))).removeClass('hideClass').addClass('hideClass');
                    //console.log($('#'+cmp.get("v.lookupCompId")));
                //$A.util.addClass(lookupList, 'hideClass');
    },500);
        }
         return false;
    },
    
    clear1: function(cmp, event, helper) {
        helper.clearSelection(cmp);
    }
})