@IsTest
public class Test_ContractModeTimeLineController {
    static testMethod void testMethod1(){
        Test.startTest();
        Account account = TestDataGenerator.createAccount('Test Account');
        insert account;
        Contract_Vehicle__c ContractVehicle=TestDataGenerator.createContractVehicle('Test Contract Vehicle',account);
        insert ContractVehicle;
       
        Task_Order__c taskOreder = TestDataGenerator.createTaskOrder('Test task Order', account, ContractVehicle);
        insert taskOreder;
        
        Id conRecordTypeId1 = Schema.SObjectType.Contract_Mods__c.getRecordTypeInfosByName().get('Update').getRecordTypeId();
        Contract_Mods__c contractMod1=TestDataGenerator.getContractMods(conRecordTypeId1,taskOreder.Id,'1234');
        insert contractMod1;
        
        Id conRecordTypeId2 = Schema.SObjectType.Contract_Mods__c.getRecordTypeInfosByName().get('Update').getRecordTypeId();
        Contract_Mods__c contractMod2=TestDataGenerator.getContractMods(conRecordTypeId2,taskOreder.Id,'12365');
        insert contractMod2;
        
        String permission=ContractModeTimeLineController.getCheckLicensePermition();
        System.assertEquals('Yes',permission);
        
        ContractModeTimeLineController conTime=new ContractModeTimeLineController();
        List<Contract_Mods__c> conList=ContractModeTimeLineController.getContractModeList(taskOreder.Id);
        System.debug(conList.size());
        System.assertEquals(2,conList.size());
        
        Contract_Mods__c contractMod3 = TestDataGenerator.updateContractMods(contractMod2,'new title');
        update contractMod3;
        
        
        
        List<HistoryController> hList=ContractModeTimeLineController.timelineRec(contractMod3.Id,taskOreder.Id);
        ApexPages.StandardController sc=new ApexPages.StandardController(contractMod3);
        new ContractModeControllar(sc);
        System.assertEquals(1,hList.size());
        
        Test.stopTest();
       
        
    }
}