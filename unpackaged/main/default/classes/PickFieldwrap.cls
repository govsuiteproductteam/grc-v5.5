public with sharing class PickFieldwrap{
        @AuraEnabled
        public String pickLabel{get;set;}
         @AuraEnabled
        public String pickValue{get;set;}
        
        public PickFieldwrap(String label,String value){
            this.pickLabel = label;
            this.pickValue = value;
        }
        
    }