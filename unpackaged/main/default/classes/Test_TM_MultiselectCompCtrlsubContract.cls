@IsTest
public class Test_TM_MultiselectCompCtrlsubContract {
 public static testMethod void testMultiselectCompCtrlsubContract(){
        TM_MultiselectCompCtrlsubContract mulCompCtrl = new TM_MultiselectCompCtrlsubContract();
        mulCompCtrl.dependantDataNew='';
        mulCompCtrl.selectedValues = 'Alaskan Native Corporation';
        mulCompCtrl.picApiName = 'TM_TOMA__Subcontractor_Size__c';
        
        System.assert(mulCompCtrl.getPickListOption()!= Null);
    }
}