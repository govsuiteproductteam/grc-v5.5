@isTest
public class Test_SOSSEC_EmailTemplateHandler {
    public static testmethod void testCase1(){
        Test.startTest();
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        
        Contact con = TestDataGenerator.createContact('TestCon', acc );
        insert con;
        
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('T-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'sossec@x-2wm2fnwmqoygs818pj7bxogxun28nsqvat0mnq4haisbl0njt2.61-zhsweao.na34.apex.salesforce.com';
        insert conVehi;
        
        Test.stopTest();
        
        SOSSEC_EmailTemplateHandler  sossecEmailTemplateHandler = new SOSSEC_EmailTemplateHandler();
        //for new Task Order Creation  
        Messaging.InboundEmail email  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        List<StaticResource> srList = [SELECT Id,Body FROM StaticResource WHERE Name='SOSSECEmailTest'];
        
        email.plainTextBody = srList.get(0).Body.toString();
        email.fromAddress ='test@test.com';
        email.fromName = 'ABC XYZ';
        email.subject = 'Fwd:Opportunity Re-Issued: 001TCRI Analytic Software Applications Prototype';
        email.toaddresses = new List<String>();
        email.toaddresses.add('sossec@x-2wm2fnwmqoygs818pj7bxogxun28nsqvat0mnq4haisbl0njt2.61-zhsweao.na34.apex.salesforce.com');
        
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        attachment.body = blob.valueOf('my attachment text');
        attachment.fileName = 'textfileone.txt';
        attachment.mimeTypeSubType = 'text/plain';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
            
            Messaging.InboundEmail.TextAttachment attachmenttext = new Messaging.InboundEmail.TextAttachment();
        attachmenttext.body = 'my attachment text';
        attachmenttext.fileName = 'textfiletwo3.txt';
        attachmenttext.mimeTypeSubType = 'texttwo/plain';
        email.textAttachments =   new Messaging.inboundEmail.TextAttachment[] { attachmenttext };
            
            // update
            Messaging.InboundEmail email1  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env1 = new Messaging.InboundEnvelope();
        List<StaticResource> srList1 = [SELECT Id,Body FROM StaticResource WHERE Name='SOSSECEmailTestUpdated'];
        
        email1.plainTextBody = srList1.get(0).Body.toString();
        email1.fromAddress ='test@test.com';
        email1.fromName = 'ABC XYZ';
        email1.subject = 'Re:Opportunity Re-Issued: 001TCRI Analytic Software Applications Prototype';
        email1.toaddresses = new List<String>();
        email1.toaddresses.add('sossec@x-2wm2fnwmqoygs818pj7bxogxun28nsqvat0mnq4haisbl0njt2.61-zhsweao.na34.apex.salesforce.com');
        
        Messaging.InboundEmail.BinaryAttachment attachment1 = new Messaging.InboundEmail.BinaryAttachment();
        attachment1.body = blob.valueOf('my attachment text');
        attachment1.fileName = 'textfileone.txt';
        attachment1.mimeTypeSubType = 'text/plain';
        email1.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment1 };
            
            Messaging.InboundEmail.TextAttachment attachmenttext1 = new Messaging.InboundEmail.TextAttachment();
        attachmenttext1.body = 'my attachment text';
        attachmenttext1.fileName = 'textfiletwo3.txt';
        attachmenttext1.mimeTypeSubType = 'texttwo/plain';
        email1.textAttachments =   new Messaging.inboundEmail.TextAttachment[] { attachmenttext1 };
            
            Messaging.InboundEmail email2  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env2 = new Messaging.InboundEnvelope();
        //List<StaticResource> srList1 = [SELECT Id,Body FROM StaticResource WHERE Name='SOSSECEmailTestUpdated'];
        
        email2.plainTextBody = srList1.get(0).Body.toString();
        email2.fromAddress ='test@test.com';
        email2.fromName = 'ABC XYZ';
        email2.subject = 'Re:Opportunity Re-Issued: 001TCRI Analytic Software Applications Prototype sdafhjdgsf sdfgjhsdfgsad fsdafjksadhfjksdahjfksdahjkf sdafhjksdafhjsadf sadfjhskdafsdjahkfsda fsdajfhsadfhjsd fs';
        email2.toaddresses = new List<String>();
        email2.toaddresses.add('sossec@x-2wm2fnwmqoygs818pj7bxogxun28nsqvat0mnq4haisbl0njt2.61-zhsweao.na34.apex.salesforce.com');
            
            Messaging.InboundEmailResult result = sossecEmailTemplateHandler.handleInboundEmail(email, env);
        Messaging.InboundEmailResult result1 = sossecEmailTemplateHandler.handleInboundEmail(email1, env1);
        Messaging.InboundEmailResult result2 = sossecEmailTemplateHandler.handleInboundEmail(email2, env2);
        
        
        List<Task_Order__c> taskOrderList = [SELECT Id,Name,Task_Order_Title__c FROM Task_Order__c];
        
        System.assertEquals(taskOrderList.size(), 2);
        
        System.debug('Name==>'+taskOrderList.get(0).Name);
        System.debug('Task_Order_Title__c==>'+taskOrderList.get(0).Task_Order_Title__c);
        
    }
}