({
	doInit : function(component, event, helper) {
        var isLiecenceValid =  component.get("c.getUserAccesibility");
        isLiecenceValid.setCallback(this,function(validityData){
            var state= validityData.getState();
			console.log(validityData.getReturnValue()) ;           
            if(state==="SUCCESS"){
                if(validityData.getReturnValue() === 'Yes'){
                    component.set("v.checkLicense",validityData.getReturnValue());
                    helper.helperDoInit(component, event, helper);
                }else{
                    component.set("v.checkLicense",validityData.getReturnValue());
                }
            }
        });
        $A.enqueueAction(isLiecenceValid);
		
	},
    addTeamingPartner : function(component, event, helper){
        helper.helperAddTeamPart(component, event, helper);
    },
    openTeamingModal : function(component,event,helper){
        helper.helperFetchModalData(component,event,helper);
        
    },
    closeTeamingModal : function(component,event,helper){
        helper.helperCloseModal(component,event,helper);
        
    },
    deleteThisTeam : function(component,event,helper){
        helper.deleteThisTeamHlpr(component,event,helper);
    },
    navigateToTeam: function(component,event,helper){
        helper.navigateToTeamHlpr(component,event,helper);
    },
    openEmailModal: function(component,event,helper){
        helper.openEmailModalHlpr(component,event,helper);
    },
    closeEmailModal: function(component,event,helper){
        helper.closeEmailModalHlpr(component,event,helper);
    },
    editThisTeam: function(component,event,helper){
        helper.editThisTeamHlpr(component,event,helper);
    },
    editStatus: function(component,event,helper){
        helper.editStatusHlpr(component,event,helper);
    },
    undoStatus: function(component,event,helper){
        helper.undoStatusTeamHlpr(component,event,helper);
    },
    updateStatus: function(component,event,helper){
        helper.updateStatusHlpr(component,event,helper);
    },
    selectStatusVal: function(component,event,helper){
        helper.selectStatusValHlpr(component,event,helper);
    },
    showAllOrg: function(component,event,helper){
        helper.showAllOrgHlpr(component,event,helper);
    },
    showfilteredOrg: function(component,event,helper){
        helper.showfilteredOrgHlpr(component,event,helper);
    },
    addAllTeamingPartner: function(component,event,helper){
        //helper.addAllTeamingPartnerHlpr(component,event,helper);
        var add_all_teaming_part = component.find("add_all_teaming_part");
        $A.util.addClass(add_all_teaming_part,'cust_disable');
        var allPartnerListWrp = component.get("v.allPartnersListWrp");
        if(! $A.util.isUndefined(allPartnerListWrp) && ! $A.util.isEmpty(allPartnerListWrp)){
            var isAnyValSelected = false;
            for(var i=0;i<allPartnerListWrp.length;i++){
                if(allPartnerListWrp[i].isSelected)
                    isAnyValSelected = true;
            }
            if(isAnyValSelected){
        var allPartnerStringJson = JSON.stringify(allPartnerListWrp);
        var oppId =component.get("v.recordId");
        var createPatners = component.get("c.getTeamingPart");
        createPatners.setParams({
            "allPartnerData":allPartnerStringJson,
            "oppId":oppId
        });
        createPatners.setCallback(this,function(resultData){
            var state= resultData.getState();
            if(state=="SUCCESS"){
                    //alert("SUCCESS");
                    component.set("v.teamList",resultData.getReturnValue());
                    var modal = component.find("teamingpartnerdiv");
                     $A.util.addClass(modal,'hidethis');
                 $A.util.removeClass(add_all_teaming_part,'cust_disable');
                var appEvent = $A.get("e.c:RefreshTaskOrderContacts_EVT");
                        appEvent.fire();
            }else{
                $A.util.removeClass(add_all_teaming_part,'cust_disable');
                var appEvent = $A.get("e.c:RefreshTaskOrderContacts_EVT");
                        appEvent.fire();
            }
        });
        $A.enqueueAction(createPatners);
  
            }else{
                //alert("Please Select Atleat one Account");
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": "Please Select At least one Account",
                    "type":"error"
                });
                toastEvent.fire();
                 $A.util.removeClass(add_all_teaming_part,'cust_disable');
            }
        }//end of if
    },
    matchAccList: function(component,event,helper){
           var accHint = component.get("v.searchTerm");
           var chkAllPartnr =component.find("chk_allPart");
       // chkAllPartnr.set("v.value",false);
           if(typeof accHint === 'undefined' || accHint.length < 2)
           {
               
               var tempList = component.get("v.tempAllPartnersListWrp");
                   if(tempList != undefined){
                       for(var i=0;i<tempList.length;i++)
                       {
                          if(tempList[i].status == false) 
                          {
                              chkAllPartnr.set("v.value",false);
                          }
                       }
                      component.set("v.allPartnersListWrp",tempList); 
                                     }
               
               component.set("v.allPartnersListWrp",component.get("v.tempAllPartnersListWrp"));
           }
           else{
               var action = component.get("c.matchAccWrapList");
        action.setParams({
            searchString: accHint
        });
        action.setCallback(this, function(a) {
            component.set("v.allPartnersListWrp", a.getReturnValue());
        })
        $A.enqueueAction(action); 
           }
     },
    checkAllTeamingPartner: function(component,event,helper){
        var isChecked = component.find("chk_all").get("v.value");

        var TeamingPartnetListWrp = component.get("v.TeamingPartnetListWrp");
        if(! $A.util.isUndefined(TeamingPartnetListWrp) && ! $A.util.isEmpty(TeamingPartnetListWrp)){
            for(var i=0;i<TeamingPartnetListWrp.length;i++){
                if(isChecked){
                    TeamingPartnetListWrp[i].status = true;
                }else{
                    TeamingPartnetListWrp[i].status = false;
                }
            }
            component.set("v.TeamingPartnetListWrp",TeamingPartnetListWrp);
        }
    },
    checkAllPartner: function(component,event,helper){
         var isChecked = component.find("chk_allPart").get("v.value");

        var allPartnersListWrp = component.get("v.allPartnersListWrp");
        if(! $A.util.isUndefined(allPartnersListWrp) && ! $A.util.isEmpty(allPartnersListWrp)){
            for(var i=0;i<allPartnersListWrp.length;i++){
                if(isChecked){
                    allPartnersListWrp[i].isSelected = true;
                }else{
                    allPartnersListWrp[i].isSelected = false;
                }
            }
            component.set("v.allPartnersListWrp",allPartnersListWrp);
        }
    },
    teamingPartnerChkClick :function(component,event,helper){
        helper.teamingPartnerChkClickHlpr(component,event,helper);
    },
    AllTeamingPartnerChkClick :function(component,event,helper){
        helper.AllTeamingPartnerChkClickHlpr(component,event,helper);
    }
    
    /*undoAndCancel : function(component,event,helper){
        document.getElementById("savestatusbtn").classList.add("slds-hide");
        document.getElementById("headbtncancl").classList.add("slds-hide");
        document.getElementById("headbtn1").classList.remove("slds-hide");
        document.getElementById("headbtn2").classList.remove("slds-hide");
    }*/
})