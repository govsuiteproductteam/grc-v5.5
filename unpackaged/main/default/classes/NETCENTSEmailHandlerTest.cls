@isTest
public class NETCENTSEmailHandlerTest {
    
    public static testMethod void testNETCENTSForNewTaskOrder(){
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Contact con = TestDataGenerator.createContact('TestCon', acc);
        insert con;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('T-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'netcents@a-2ntsszdlyjs9ekfzqdxsneaw0pg4n949m4dat0wi7uvjzodp8x.37-pvateac.na31.apex.salesforce.com';
        
        insert conVehi;
        
        Messaging.InboundEmail email  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        List<StaticResource> srList = [SELECT Id,Body FROM StaticResource WHERE Name='NETCENTSEmailTest'];
        
        email.plainTextBody = srList.get(0).Body.toString();
        email.fromAddress ='test@test.com';
        email.fromName = 'ABC XYZ';
        email.subject = 'AFWay Request For Quote: RFQ ID - 185717';
        email.toaddresses = new List<String>();
        email.toaddresses.add('netcents@a-2ntsszdlyjs9ekfzqdxsneaw0pg4n949m4dat0wi7uvjzodp8x.37-pvateac.na31.apex.salesforce.com');
        
        NETCENTSEmailHandler handler = new NETCENTSEmailHandler();
        Test.startTest();
        Messaging.InboundEmailResult result = handler.handleInboundEmail(email, env);
        Test.stopTest();
        
        List<Task_Order__c> taskOrderList = [SELECT Id, Name, Task_Order_Title__c, Description__c FROM Task_Order__c];
        Task_Order__c taskOrder = taskOrderList.get(0);
        System.assertEquals(taskOrderList.size(), 1);
        System.assertEquals('185717', taskOrder.Name);
        System.assertEquals('750 Cable Project', taskOrder.Task_Order_Title__c);
    }
    
    public static testMethod void testNETCENTSForExistingTaskOrder(){
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Contact con = TestDataGenerator.createContact('TestCon', acc);
        insert con;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('T-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'netcents@a-2ntsszdlyjs9ekfzqdxsneaw0pg4n949m4dat0wi7uvjzodp8x.37-pvateac.na31.apex.salesforce.com';
        
        insert conVehi;
        
        Messaging.InboundEmail email1  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env1 = new Messaging.InboundEnvelope();
        List<StaticResource> srList1 = [SELECT Id,Body FROM StaticResource WHERE Name='NETCENTSEmailTest'];
        
        email1.plainTextBody = srList1.get(0).Body.toString();
        email1.fromAddress ='test@test.com';
        email1.fromName = 'ABC XYZ';
        email1.subject = 'AFWay Request For Quote: RFQ ID - 185717';
        email1.toaddresses = new List<String>();
        email1.toaddresses.add('netcents@a-2ntsszdlyjs9ekfzqdxsneaw0pg4n949m4dat0wi7uvjzodp8x.37-pvateac.na31.apex.salesforce.com');
        
        //For Updating existing task order with existing Contact
        Messaging.InboundEmail email2  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env2 = new Messaging.InboundEnvelope();
        List<StaticResource> srList2 = [SELECT Id,Body FROM StaticResource WHERE Name='NETCENTSEmailTestUpdated'];
        email2.plainTextBody = srList2.get(0).Body.toString();
        email2.fromAddress ='test@test.com';
        email2.fromName = 'ABC XYZ';
        email2.subject = 'AFWay Request For Quote: RFQ ID - 185717';
        email2.toaddresses = new List<String>();
        email2.toaddresses.add('netcents@a-2ntsszdlyjs9ekfzqdxsneaw0pg4n949m4dat0wi7uvjzodp8x.37-pvateac.na31.apex.salesforce.com');
        
        NETCENTSEmailHandler handler = new NETCENTSEmailHandler();
        Test.startTest();
        Messaging.InboundEmailResult result1 = handler.handleInboundEmail(email1, env1);
        Messaging.InboundEmailResult result2 = handler.handleInboundEmail(email2, env2);
        Test.stopTest();
        
        List<Task_Order__c> taskOrderList = [SELECT Id, Name, Task_Order_Title__c, Due_Date__c, Description__c FROM Task_Order__c];
        Task_Order__c taskOrder = taskOrderList.get(0);
        
        System.assertEquals(taskOrderList.size(), 1);
        System.assertEquals('185717', taskOrder.Name);
        System.assertEquals('750 Cable Project1', taskOrder.Task_Order_Title__c);
        
        List<Contract_Mods__c> contractModList = [SELECT Id, New_Task_Order_Title__c, New_Description__c, New_Due_Date__c, New_Proposal_Due_Date_Time__c 
                                                  FROM Contract_Mods__c];
        Contract_Mods__c contractMod = contractModList.get(0);
        System.assertEquals(1, contractModList.size());
        System.assertEquals(taskOrder.Task_Order_Title__c, contractMod.New_Task_Order_Title__c);
        System.assertEquals(taskOrder.Description__c, contractMod.New_Description__c);
        System.assertEquals(taskOrder.Due_Date__c, contractMod.New_Due_Date__c);
    }
    
    public static testMethod void testNETCENTSForNewContactOnExistingTaskOrder(){
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Contact con = TestDataGenerator.createContact('TestCon', acc);
        insert con;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('T-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'netcents@a-2ntsszdlyjs9ekfzqdxsneaw0pg4n949m4dat0wi7uvjzodp8x.37-pvateac.na31.apex.salesforce.com';
        
        insert conVehi;
        
        Messaging.InboundEmail email1  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env1 = new Messaging.InboundEnvelope();
        List<StaticResource> srList1 = [SELECT Id,Body FROM StaticResource WHERE Name='NETCENTSEmailTest'];
        
        email1.plainTextBody = srList1.get(0).Body.toString();
        email1.fromAddress ='test@test.com';
        email1.fromName = 'ABC XYZ';
        email1.subject = 'AFWay Request For Quote: RFQ ID - 185717';
        email1.toaddresses = new List<String>();
        email1.toaddresses.add('netcents@a-2ntsszdlyjs9ekfzqdxsneaw0pg4n949m4dat0wi7uvjzodp8x.37-pvateac.na31.apex.salesforce.com');
        
        //For Updating existing task order with existing Contact
        Messaging.InboundEmail email2  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env2 = new Messaging.InboundEnvelope();
        List<StaticResource> srList2 = [SELECT Id,Body FROM StaticResource WHERE Name='NETCENTSEmailTestUpdated_1'];
        email2.plainTextBody = srList2.get(0).Body.toString();
        email2.fromAddress ='test@test.com';
        email2.fromName = 'ABC XYZ';
        email2.subject = 'AFWay Request For Quote: RFQ ID - 185717';
        email2.toaddresses = new List<String>();
        email2.toaddresses.add('netcents@a-2ntsszdlyjs9ekfzqdxsneaw0pg4n949m4dat0wi7uvjzodp8x.37-pvateac.na31.apex.salesforce.com');
        
        NETCENTSEmailHandler handler = new NETCENTSEmailHandler();
        Test.startTest();
        Messaging.InboundEmailResult result1 = handler.handleInboundEmail(email1, env1);
        Messaging.InboundEmailResult result2 = handler.handleInboundEmail(email2, env2);
        Test.stopTest();
        
        List<Task_Order__c> taskOrderList = [SELECT Id, Name, Buyer__r.Name FROM Task_Order__c];
        Task_Order__c taskOrder = taskOrderList.get(0);
        
        System.assertEquals(taskOrderList.size(), 1);
        System.assertEquals('185717', taskOrder.Name);
        System.assertEquals('DEAN1', taskOrder.Buyer__r.Name);
    }
    
}