








 
<HTML lang=en><HEAD><TITLE> 
			
				GSA Advantage! eBuy RFQ Modification Description
			
			
    	</TITLE>
<META http-equiv=content-type content="text/html; charset=iso-8859-2">
<LINK href="/images/ebuy/content.css" type=text/css rel=stylesheet>
<LINK href="/images/ebuy/tree.css" type=text/css rel=stylesheet>
<LINK href="/images/ebuy/adv_styles.css" type=text/css rel=stylesheet>
<META content="MSHTML 6.00.2900.2963" name=GENERATOR>
</HEAD>
<BODY vLink=#660000 link=#990000 bgColor=#ffffff leftMargin=0 topMargin=0 
marginheight="0" marginwidth="0" onLoad="showtimeout();"><!--GSA Advantage Header -->
<TABLE cellSpacing=0 cellPadding=0 width="99%" border=0>
<TBODY>
  	<TR>
    	<TD>
      		
<script language="JavaScript" type="text/javascript">
/********************************* Timeout Scripts*******************************/
var minutes = 60;	// the number of minutes until timeout
var remindTime = 60;	// remaining number of seconds when the popup window appears

var timeoutpath = "/advantage/ebuy/tiles/timeout_alert.jsp";	// the path to "timeout.html"
var timedoutpath = "/advantage/ebuy/tiles/timedout_alert.jsp";	// the path to "timedout.html"

var secondsLeft = minutes*60;
//var timeoutwindowopen = false;	// popup window is closed to begin with
var timeWindow = null;	// popup window does not exist

// Called from the header image: onLoad="showtimeout();"		
function showtimeout() {	
			
	if (secondsLeft < 0) {
		replaceWindow(timedoutpath); // path to timedout.html
		return;
	}

	if ((secondsLeft > 0) && (secondsLeft <= remindTime)) {
		openWindow(timeoutpath, 150);	// path to timeout.html
	}
			
	var minutes = Math.floor(secondsLeft/60);
	var seconds = secondsLeft%60;
			
	if (seconds < 10) {
		seconds = "0" + seconds;
	}
			
	// Show the remaining time in the window
	if (seconds <= 0 && minutes <= 0) {
		window.status = formatTime("Your e-Buy Session has timed out.");
	} else {
		window.status = formatTime("Your e-Buy Session will timeout in : " + minutes + ":" + seconds);
	}
			
	var period = 1;
	setTimeout("showtimeout()", period*1000);
	secondsLeft -= period;	
}

// Format the time		
function formatTime(x) {
	var pattern = new RegExp("&#([0-9]+);");
	while ((result = pattern.exec(x)) != null) {
		x = x.replace(result[0], String.fromCharCode(RegExp.$1));
	}
	x = x.replace(/&nbsp;/g, ' ');
	return x;
}

// open the popup window		
function openWindow(url, height) {
	var options = 'width=500,height='+height+',left=250,top=250,resizable=yes,scrollbars=yes,menubar=no,status=no,toolbar=no,location=no';
	timeWindow = window.open(url,"timeWindow",options);
	if (timeWindow.opener == null) {
		timeWindow.opener = self;
	}
	//timeoutwindowopen=true;
}

// replaces the existing timeout popup window with timed-out window		
function replaceWindow(url) {
	if (timeWindow != null) {
		timeWindow.close();
		openWindow(url, 400);
		//timeWindow.document.location.replace(url, 400);
	} else {
		openWindow(url, 400);
	}
}
</script>












<!-- The following code is to collect web site statistics as part of Federal wide initiative -->

<script language="javascript" id="_fed_an_ua_tag" src="/images/adv12/js/Universal-Federated-Analytics.1.04.js?agency=GSA"></script>




<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
<TR>
	<TD>
		<TABLE cellSpacing=0 cellPadding=0 width=100% bgColor=#ffffff border=0>
		<TR>
			<TD vAlign=bottom align=middle width=250>
				<a href="#skipnavigation"></a>
				
				<A href="/advantage/ebuy/seller/home.do?"><img src="/images/ebuy/eBuy_V7_Banner.gif" alt="e-Buy home" width="250" height="50" border="0"></A>
			</TD>
			<TD vAlign=bottom align=right width=100%>
				<TABLE cellSpacing=0 cellPadding=0 width="100%" align=right border=0>
				<TR>
					<TD align=right height=20>
						<!--<TABLE cellSpacing=0 cellPadding=0 align=right border=0>
						<TR>
							<TD>
								<FONT size=1><A href="file:///C:/Documents%20and%20Settings/SarahAWheet/My%20Documents/GSA%20Web/GSA%20Advantage/GSA%20Advantage%2010/banner/">GSA Advantage! </A>&nbsp; | &nbsp; </FONT>
							</TD>
							<TD>
								<FONT size=1><A href="file:///C:/Documents%20and%20Settings/SarahAWheet/My%20Documents/GSA%20Web/GSA%20Advantage/GSA%20Advantage%2010/banner/">e-Library  </A></FONT>
							</TD>
						</TR>
						</TABLE>-->
					</TD>
				</TR>
				<TR>
					<TD align=right>
						<TABLE cellSpacing=0 cellPadding=1 align=right border=0>
						<TR>
							<TD><a href="/advantage/ebuy/seller/home.do?" title="e-Buy home"><img src="/images/ebuy/b_home.gif" alt="e-Buy home" width="50" height="21" border="0"></a></TD>
							<TD><a href="/advantage/ebuy/seller/active_quotes.do?" title="My Quotes"><img src="/images/ebuy/b_myQuotes.gif" alt="My Quotes" width="75" height="21" border="0"></a></TD>
							<TD><a href="/advantage/ebuy/seller/active_RFQs.do?" title="RFQs"><img src="/images/ebuy/b_RFQs.gif" alt="RFQs" width="75" height="21" border="0"></a></TD>
							<TD><a href="/advantage/ebuy/seller/profile.do?" title="My Profile"><img src="/images/ebuy/b_profile.gif" alt="My Profile" width="60" height="21" border="0"></a></TD>
							<!--<TD><a href="/advantage/ebuy/seller/information_page.do?&amp;keyName=SELLER_GUIDANCE" title="e-Buy Guidance"><img src="/images/ebuy/b_guidance.gif" alt="e-Buy Guidance" width="100" height="21" border="0"></a></TD>-->
							<TD><a href="/images/products/elib/pdf_files/sguide.pdf" title="e-Buy Guidance"><img src="/images/ebuy/b_guidance.gif" alt="e-Buy Guidance" width="100" height="21" border="0"></a></TD>
							<TD><a href="/advantage/ebuy/main/ebuy_tutorial.do?" title="e-Buy Training"><img src="/images/ebuy/b_training.gif" alt="e-Buy Training" width="100" height="21" border="0"></a></TD>
							<TD><a href="/advantage/ebuy/seller/logout.do?" title="Log off"><img src="/images/ebuy/b_logoff.gif" alt="Log off" width="52" height="21" border="0"></a><a name="skipnavigation"></a></TD>
						</TR>
						</TABLE>
					</TD>
				</TR>
				</TABLE>
			</TD>
		</TR>
		</TABLE>
	</TD>
</TR>
</TABLE>

<table border=1 bordercolor="#006699" width=100% cellspacing="0" cellpadding="0">
<tr>
<td>






<table width="98%" border="0" cellspacing="0" cellpadding="0">
<tr valign="top"> 
	<td> 
	<!-- navigation_main include -->
	</td>
</tr>

<tr valign="top"> 
	<td align="left">
		<table border="0" width="95%" cellspacing=0 cellpadding=0>
		<tr valign="top"> 
			<td align="left" bgcolor="#FFFFFF" width="5%" background="/images/ebuy/spacer.gif" rowspan="5" alt=""><img src="/images/ebuy/spacer.gif" width="15" height="1" alt=""></td>
			<td colspan="4" align="left"><img src="/images/ebuy/title_modification_descr.gif" width="199" height="37" alt="Modification Description"></td>
			<td rowspan="7" align="right">&nbsp; </td>
		</tr>
		<tr>
			<td><img src="/images/ebuy/spacer.gif" width="15" height="15" alt=""><td>
		</tr>
		<tr> 
			<td align="left" width="2%" height="560"></td>
			<td align="left" width="98%"  valign="top"> 
				<table width="98%" border="0" cellspacing="0" cellpadding="0">
				<tr>   
					<td align="left" colspan="6" valign="top">
						<label for="name" accesskey="N"><font size="2" class="columntitle">RFQ ID: </font>
						<font size="2">RFQ1073583</font></label>
					</td>
				</tr>
				<tr> 
					<td align="left" colspan="3" valign="middle"><font size="2" class="columntitle"><br>Description of modification 1 made at 03/16/2016 11:44:10 AM EDT:</font></td>
				</tr>
				<tr> 
					<td align="left" colspan="3" valign="middle"><label for="address4" accesskey="4"><font size="2"><br>This modification 1 to this RFQ 1073583 is issued to facilitate contractor questions.  See extended description.</font></label></td>
				</tr>
				<tr> 
					<td colspan="4">&nbsp;<br></td>
				</tr>
				<tr> 
					<td align="center" colspan="4">
					
					
					<a href="/advantage/seller/prepareQuote.do?"><img src="/images/ebuy/but_back.gif" width="88" height="30" border="0" alt="Back"></a></td>
				</tr>
				</table>
				<p></p>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table></td></tr></table>
</TD></TR></TBODY></TABLE></BODY></HTML>