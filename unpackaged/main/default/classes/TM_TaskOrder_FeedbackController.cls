global with sharing class TM_TaskOrder_FeedbackController{
    
    private static List<String> optionAPIList = new List<String>{'Option_1__c', 'Option_2__c', 'Option_3__c', 'Option_4__c', 'Option_5__c', 'Option_6__c', 'Option_7__c',
                                                         'Option_8__c', 'Option_9__c', 'Option_10__c'};  // List of API Name for option.
    public Map<Id,List<SurveyAnsWrapper>> questionIdSurveyWrapListMap{get;set;}// Map to store question Answer given by user with respective question.
    public Map<Id,Question__c> questionIdMap{get;set;} // To maintain question Id and question in String
    public String returnUrltask{get;set;}
    public String returnUrlTaskSurvey{get;set;}
    public Map<Id,List<Question__c>> surveyQuestionMap{get;set;}
    public Survey_Task__c currentSurveyTask{get;set;}
    public Map<Id,Survey__c> surveyMap{get; set;}
    private List<Task_Order_Questionnaire__c> communityUrlList;
    Public Task_Order__c taskOrder{get; set;}
    public String isValidUser{get; set;}
       
    public TM_TaskOrder_FeedbackController(ApexPages.StandardController controller){
        isValidUser = LincenseKeyHelper.checkLicensePermitionForFedTOM();
        if(isValidUser != 'Yes')
            return;
        
      if(!Test.isRunningTest()){
          controller.Addfields(new String[]{'Name','Contract_Vehicle__r.Name'});
      }
      if(Schema.sObjectType.Feedback__c.isAccessible() && Schema.sObjectType.Survey__c.isAccessible() && Schema.sObjectType.Survey_Answer__c.isAccessible() && Schema.sObjectType.Question__c.isAccessible() && Schema.sObjectType.User.isAccessible() && Schema.sObjectType.Task_Order__c.isAccessible()){ 
        init(); //To initialize member variable
      
      }  
    }
    
    public void init(){                 //To initialize member variable
        Id taskOrderId = ApexPages.CurrentPage().getparameters().get('id');
        
        List<Task_Order__c> taskOrderList = [Select Id,Name,Contract_Vehicle__r.Name from Task_Order__c where id =: taskOrderId Limit 1];
        if(taskOrderList != null && taskOrderList.size() > 0 ){
            taskOrder = taskOrderList[0];
        }
        else{
            taskOrder = new Task_Order__c();
        }
        
        //SOQL to get all Feedback and answers given by user with respective feedback.
        List<Feedback__c> feedbackList =  [SELECT id , User__c, Contact__c, Company_Name__c, Contact__r.Name,
                         (SELECT id, Question__c, Question__r.question__c, Answer_Descriptive__c, Survey_Taken__c, Option_1__c, Option_2__c, Option_3__c, Option_4__c, 
                         Option_5__c, Option_6__c, Option_7__c, Option_8__c, Option_9__c, Option_10__c,Feedback__c, Feedback__r.User__r.name, Feedback__r.Company_Name__r.name, Feedback__r.Contact__r.name,
                         Feedback__r.Date_Response_Received__c, Feedback__r.Date_Teaming_Request_sent__c FROM Survey_Answers__r)
                         FROM Feedback__c WHERE Task_Order__c =:taskOrderId ];
        
        
        if(!feedbackList.isEmpty()){            
            map<id, Feedback__c> feedBackIdMap = new map<id, Feedback__c>(feedbackList);
            questionIdSurveyWrapListMap = new map<id,List<SurveyAnsWrapper>>();
            
            for(Feedback__c feedBack : feedbackList){
                List<Survey_Answer__c> surveyAnsList = feedBack.Survey_Answers__r;
                for(Survey_Answer__c surveyAns : surveyAnsList){
                    if(questionIdSurveyWrapListMap.get(surveyAns.Question__c)!=Null){
                        questionIdSurveyWrapListMap.get(surveyAns.Question__c).add(new SurveyAnsWrapper(surveyAns));
                    }else{
                        List<SurveyAnsWrapper> temSurveyAnsList = new List<SurveyAnsWrapper>{new SurveyAnsWrapper(surveyAns)};
                        questionIdSurveyWrapListMap.put(surveyAns.Question__c, temSurveyAnsList);
                    }                    
                }
            }       
            questionIdMap = new map<id,Question__c>([SELECT Question__c,Survey__c FROM Question__c WHERE Id In :questionIdSurveyWrapListMap.keySet() Order by Sequence__c ASC]);
            surveyQuestionMap = new Map<Id,List<Question__c>>();
            
            for(Id questionId :questionIdMap.keySet() ){
                Question__c quest = questionIdMap.get(questionId);
                if(quest != null){
                    List<Question__C> questionList = surveyQuestionMap.get(quest.Survey__c);
                    if(questionList == null){
                        questionList = new List<Question__c>();
                    }
                    questionList.add(quest);
                    surveyQuestionMap.put(quest.Survey__c,questionList);
                }
            }
            
           surveyMap = new Map<Id,Survey__c>([Select Id,Name From Survey__c where id IN: surveyQuestionMap.keySet()]);
        }
        
        communityUrlList = [SELECT id, Customer_Community_URL__c, Partner_Community_URL__c  FROM Task_Order_Questionnaire__c LIMIT 1];
        User usr = [SELECT id, contactId,Profile.UserLicense.Name FROM User WHERE id = : UserInfo.getUserId()];
        
        if(usr.Profile.UserLicense.Name.containsIgnoreCase('partner')){
            if(communityUrlList[0].Partner_Community_URL__c  != null){
                //returnUrltask = 'partner/'+ taskOrderId ;
                  returnUrltask = communityUrlList[0].Partner_Community_URL__c +'/'+taskOrderId ;
                //returnUrlTaskSurvey = 'partner/'+ taskOrderId ;
                  returnUrlTaskSurvey = communityUrlList[0].Partner_Community_URL__c +'/'+taskOrderId ;
            }
        }
        else{
            if(usr.Profile.UserLicense.Name.containsIgnoreCase('Customer')){
                if(communityUrlList[0].Customer_Community_URL__c != null){
                    //returnUrltask = 'Customer/'+taskOrderId ;
                      returnUrltask = communityUrlList[0].Customer_Community_URL__c+'/'+taskOrderId ;
                    //returnUrlTaskSurvey = 'Customer/'+ taskOrderId ;
                      returnUrlTaskSurvey = communityUrlList[0].Customer_Community_URL__c+'/'+taskOrderId;
                }
            }
            else{
                returnUrltask = ''+ taskOrderId ;
                returnUrlTaskSurvey = ''+taskOrderId ;
            }
        }
    }
 
 /********************************** Wrapper class *********************************************/   
    public class SurveyAnsWrapper{
        public Survey_Answer__c surveyAnswer{get;set;}
        public List<String> ansList{get;set;}
        public String descriptiveAns{get;set;}
        
        public SurveyAnsWrapper(Survey_Answer__c surveyAnswer){
            this.surveyAnswer = surveyAnswer;
            if(surveyAnswer.Answer_Descriptive__c!=Null){
                descriptiveAns = surveyAnswer.Answer_Descriptive__c;
            }else{
                ansList = new List<String>();
                for(String str : TM_TaskOrder_FeedbackController.optionAPIList){
                    if(surveyAnswer.get(str)!=Null && surveyAnswer.get(str)!=''){
                        ansList.add(''+surveyAnswer.get(str));
                    }
                }
            }
        }
    }
   
}