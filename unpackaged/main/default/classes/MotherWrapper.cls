public Class MotherWrapper{
        @AuraEnabled
        List<Teaming_Partner__c> teamingPartnerList{get;set;}
        @AuraEnabled
        List<PickFieldwrap> pickListVals{get;set;}
        
        public MotherWrapper(List<Teaming_Partner__c> teamingPartnerList,List<PickFieldwrap> pickListVals){
            this.teamingPartnerList = teamingPartnerList == null ? new List<Teaming_Partner__c>() : teamingPartnerList;
            this.pickListVals = pickListVals == null ? new List<PickFieldwrap>() : pickListVals;
        }
    }