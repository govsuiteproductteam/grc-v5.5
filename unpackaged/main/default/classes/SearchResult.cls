public with sharing class SearchResult
{
    @AuraEnabled public String SObjectLabel {get; set;}
    @AuraEnabled public Id SObjectId {get; set;}
    @AuraEnabled public SObject SObj {get; set;}//newlly added 27-06-2018 
    public SearchResult(String sObjectLabel, Id sObjectId)
    {
        this.SObjectLabel = sObjectLabel;
        this.SObjectId = sObjectId;
    }
    public SearchResult(String sObjectLabel, Id sObjectId,SObject SObj)//newlly added 27-06-2018
    {
        this.SObjectLabel = sObjectLabel;
        this.SObjectId = sObjectId;
        this.SObj = SObj;
    }
}