global with sharing class AlliantEmailTemplateHandler implements Messaging.InboundEmailHandler{
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        if(Schema.sObjectType.Task_Order__c.isAccessible() && Schema.sObjectType.Contract_Vehicle__c.isAccessible() && Schema.sObjectType.Contact.isAccessible()){ 
        List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:email.toAddresses.get(0)];
        if(!conVehicalList.isEmpty()){
            AlliantEmailTemplateParser parser = new AlliantEmailTemplateParser();
            //System.debug('AkashDebugggggggggggggggggg '+ email.plainTextBody);
            //if(email.plainTextBody!=''){
            parser.parse(email);//Email Parsing will be handle by parse method.
            //}
            Task_Order__c taskOrder;
            if(email.subject!=Null && email.subject!=''){
                taskOrder = getExistTaskOrder(parser.getTaskOrderNumber(getTrimedEmailSubject(email.subject)), conVehicalList[0].ID);//Checked for existing TaskOrder if alredy created before
            }else{
                 throw new ContractVehicleNotFoundException(MessageConstantController.TASK_ORDER_NOT_FOUND );
            }
            
            
            if(taskOrder == null){//If email template not exists            
                taskOrder = parser.getTaskOrder(email.toAddresses.get(0));
                if(taskOrder != null){  
                    try{
                        DMLManager.insertAsUser(taskOrder);
                        EmailHandlerHelper.insertBinaryAttachment(taskOrder,email);
                        EmailHandlerHelper.insertTextAttachment(taskOrder,email);
                        EmailHandlerHelper.insertActivityHistory(taskOrder,email);
                    }catch(Exception e){
                        System.debug('error message = '+e.getMessage());
                    }
                }
            }else{// If TaskOrder already present then execution comes to else block            
                try{
                    //insertContractMods(taskOrder);
                    Task_Order__c oldTaskOrder = taskOrder.clone(false,true,false,false);
                    Task_Order__c updatedTakOrder = parser.updateTaskOrder(taskOrder, email.toAddresses.get(0));
                    //DMLManager.updateAsUser(updatedTakOrder);
                    EmailHandlerHelper.insertContractMods(updatedTakOrder , oldTaskOrder,email);
                    DMLManager.updateAsUser(updatedTakOrder);
                    //update updatedTakOrder;
                    EmailHandlerHelper.insertBinaryAttachment(updatedTakOrder,email);
                    EmailHandlerHelper.insertTextAttachment(updatedTakOrder,email);
                    EmailHandlerHelper.insertActivityHistory(updatedTakOrder,email);                
                    system.debug('Already Exists');
                }catch(Exception e){
                    System.debug('error message = '+e.getMessage()+'\tline number = '+e.getLineNumber());
                }
            }
        }else{
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
        }
        }
        return result;
    }
    private Task_Order__c getExistTaskOrder(String emailSubject, String contractId){
        
        System.debug('Email Allient Task Order Number  = '+emailSubject);
        emailSubject.trim();
        List<Task_Order__c> taskOrderList = [SELECT Id,Name,Email_Reference__c, Task_Order_Title__c, Due_Date__c, Release_Date__c,Contracting_Officer__c,Contract_Specialist__c,ASB_Action__c,Description__c,Start_Date__c,Stop_Date__c,
                                             Contract_Negotiator__c,Proposal_Due_DateTime__c,Questions_Due_Date__c,Customer_Agency__c FROM Task_Order__c where Name =:emailSubject AND Contract_Vehicle__c =: contractId];
        System.debug('taskOrderList'+taskOrderList);
        if(!taskOrderList.isEmpty()){
            return taskOrderList[0];
        }
        return null;
    }
    
    private String getTrimedEmailSubject(String subject){
        if(subject.startsWith('RE:')){
            subject = subject.replaceFirst('RE:', '');            
        }
        if(subject.startsWith('Fwd:') ){
            subject = subject.replaceFirst('Fwd:', '');            
        }   
        return subject.trim();
    }
}