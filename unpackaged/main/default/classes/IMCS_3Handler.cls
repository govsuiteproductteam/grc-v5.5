global with sharing class IMCS_3Handler implements Messaging.InboundEmailHandler{
global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
    if(Schema.sObjectType.Task_Order__c.isAccessible()){
        //String myPlainText = email.plainTextBody;       
        String htmlText = email.HTMLBody;
        system.debug('htmlText=='+ htmlText);
    
        IMCS_3Parser parser = new IMCS_3Parser();
        parser.parse(email);
        System.debug('****inside IMCS3 Handler*****');
        
        Task_Order__c taskOrder = parser.getTaskOrder(email.toAddresses.get(0));
        try{
            DMLManager.insertAsUser(taskOrder);
            EmailHandlerHelper.insertBinaryAttachment(taskOrder,email);
            EmailHandlerHelper.insertTextAttachment(taskOrder,email);
            EmailHandlerHelper.insertActivityHistory(taskOrder,email);
            
        }catch(Exception e){
            System.debug('error message = '+e.getMessage());
        }
            
    }
    
     return result;
    }
}