global class GSASecurityCodeEmailHandler implements Messaging.InboundEmailHandler{
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        //added by Mai 8/1/2018
        If(email.plainTextBody==null){
            String emailBody = email.htmlBody;
        	System.debug(''+emailBody);
            try{
                String securityCode='';
                if(emailBody.contains('Your One-Time Security Code is')){
                    securityCode = emailBody.normalizeSpace().substringBetween('Your One-Time Security Code is', 'Within 30 minutes').stripHtmlTags().trim();
                    System.debug('securityCode = '+securityCode);
                }
                List<Document> documentList = [SELECT Id,Body FROM Document WHERE DeveloperName='EbuySecurityCode'];
                documentList[0].body =  Blob.valueOf(securityCode);
                update documentList;
                system.debug('document uploaded');
            }catch(Exception e){
                
            }        
            return null;
        }
        else{
        String emailBody = email.plainTextBody;
        System.debug(''+emailBody);
        try{
            String securityCode='';
            if(emailBody.contains('Your One-Time Security Code is')){
                securityCode = emailBody.normalizeSpace().substringBetween('Your One-Time Security Code is', 'Within 30 minutes').remove('*').trim();
                System.debug('securityCode = '+securityCode);
            }
            if(emailBody.contains('Confirmation code:')){
                securityCode = emailBody.normalizeSpace().substringBetween('Confirmation code:', 'To allow').remove('*').trim();
                System.debug('securityCode = '+securityCode);
            }
            List<Document> documentList = [SELECT Id,Body FROM Document WHERE DeveloperName='EbuySecurityCode'];
            documentList[0].body =  Blob.valueOf(securityCode);
            update documentList;            
        }catch(Exception e){
            
        }        
        return null;
        }
    }
}