public with sharing class AutomaticResourcesEmailTemplateParser {    
    private String emailSubject='';
    public String taskOrderNumber='';
    public void parse(Messaging.InboundEmail email){
        emailSubject = email.subject;
        emailSubject = emailSubject.stripHtmlTags();
        System.debug('emailSubject = '+emailSubject);
        taskOrderNumber = getTrimedEmailSubject(checkValidString(emailSubject, 79));
        
    }
    
    private String checkValidString(String str, Integer endLimit){
        if(str.length() > endLimit){
            String validString = str.substring(0,endLimit);
            return validString;
        }
        return str;
    }    
    
    public Task_Order__c getTaskOrder(String toAddress){        
        if(taskOrderNumber==null){
            System.debug('*********null');
            return null;
        } 
        if(Schema.sObjectType.Task_Order__c.isAccessible() && Schema.sObjectType.Contract_Vehicle__c.isAccessible()){
            List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];        
            //if(!conVehicalList.isEmpty()){
            Task_Order__c taskOrder = new Task_Order__c();
            taskOrder.Contract_Vehicle__c = conVehicalList[0].Id;
            taskOrder.Name = taskOrderNumber;//Task Order Number  
            taskOrder.Email_Reference__c = checkValidString(getTrimedEmailSubject(emailSubject) , 255);
            taskOrder.Contract_Vehicle_picklist__c = 'Automatic Resources - DOI Foundation Cloud';//Contract Vehicle
            taskOrder.Release_Date__c = Date.today();//RFP/TO Release Date
            return taskOrder;
        }else{
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
        }
        //}
        //return null;
    }
    
    public Task_Order__c updateTaskOrder(Task_Order__c taskOrder, String toAddress){
        if(Schema.sObjectType.Task_Order__c.isAccessible() && Schema.sObjectType.Contract_Vehicle__c.isAccessible()){        
            List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];        
            //if(!conVehicalList.isEmpty()){            
            taskOrder.Contract_Vehicle__c = conVehicalList[0].Id;
            taskOrder.Name = taskOrderNumber;//Task Order Number  
            //taskOrder.Email_Reference__c = checkValidString(getTrimedEmailSubject(emailSubject) , 255);
            taskOrder.Contract_Vehicle_picklist__c = 'Automatic Resources - DOI Foundation Cloud';//Contract Vehicle
            taskOrder.Release_Date__c = Date.today();//RFP/TO Release Date
            return taskOrder;
            //}   
        }else{
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
        }     
    }
    
    private String getTrimedEmailSubject(String subject){
        if(subject.startsWith('RE:')){
            subject = subject.replaceFirst('RE:', '');            
        }
        if(subject.startsWith('Fwd:') ){
            subject = subject.replaceFirst('Fwd:', '');            
        }   
        return subject.trim();
    }
}