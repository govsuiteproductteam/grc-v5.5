global with sharing class AddTaskOrderContactController {
    public Task_Order__c taskOrder{get; set;}
    public List<TaskOrderContactWrapper > taskOrderContactWrapperList{get; set;}
    Id tOrderId;
    public String isValidUser{get; set;}
    
    public AddTaskOrderContactController(ApexPages.StandardController controller) {
        isValidUser = LincenseKeyHelper.checkLicensePermitionForFedTOM();
        if(isValidUser != 'Yes')
            return;
        
        tOrderId = ApexPages.CurrentPage().getparameters().get('tid');
        taskOrderContactWrapperList = new List<TaskOrderContactWrapper >();
        if(tOrderId != null && Schema.sObjectType.Task_Order__c.isAccessible()){
            
            taskOrder = [Select Id,Contract_Vehicle__c  From Task_Order__c Where id =: tOrderId Limit 1];
           
            //Edits by Mai 8/10/16
           Set<ID> accIdSet = new Set<Id>();
           if(Schema.sObjectType.Teaming_Partner__c.isAccessible()){
               for(Teaming_Partner__c teamingaPartner : [Select Vehicle_Partner__r.Partner__c From Teaming_Partner__c where Task_Order__c =: taskOrder.id AND Vehicle_Partner__c!=null] ){
                   accIdSet.add(teamingaPartner.Vehicle_Partner__r.Partner__c );
               }
           
               if(Schema.sObjectType.Contact.isAccessible()){
                    List<Contact> contractContactList = [Select Id,Name,Account.Name,Email,AccountId From Contact where 
                    //update MAI 8/10/16
                    AccountId IN:accIdSet AND
                    ID NOT IN (Select Contact__c From Task_Order_Contact__c where Task_Order__c =: taskOrder.id) Limit 1000];
                    
                    if(Schema.sObjectType.Teaming_Partner__c.isCreateable()){
                        for(Contact con : contractContactList ){
                            if(accIdSet.contains(con.AccountId)){
                                Task_Order_Contact__c taskOrderContact = new Task_Order_Contact__c();
                                taskOrderContact.Contact__r = con;
                                taskOrderContact.Contact__c = con.id;
                                taskOrderContact.Task_Order__r = taskOrder ;
                                taskOrderContact.Task_Order__c = taskOrder.id ;
                                TaskOrderContactWrapper taskOrderContactWrapper = new TaskOrderContactWrapper(taskOrderContact );
                                taskOrderContactWrapperList.add(taskOrderContactWrapper );
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    public PageReference addTaskOrderContact(){
        List<Task_Order_Contact__c> taskOrderContactList = new List<Task_Order_Contact__c>();
        for(TaskOrderContactWrapper taskOrderContact : taskOrderContactWrapperList){
            if(taskOrderContact.status){
                taskOrderContactList.add(taskOrderContact.taskOrderContact);
            }
        }
        if(!taskOrderContactList.isEmpty()){
            //insert taskOrderContactList;
            try{
                DMLManager.insertAsUser(taskOrderContactList);
                //shareTaskOrder(taskOrderContactList);
            }catch(Exception e){}
        }
        
       
        
        PageReference pageRef = new PageReference('/'+taskOrder.id);
        pageRef.setRedirect(true);
        return new PageReference('/'+taskOrder.id);
    }
    
    /*private void shareTaskOrder(List<Task_Order_Contact__c> taskOrderContactList){
        Set<Id> contactIdSet = new Set<ID>();
        List<Task_Order__Share> taskOrderShareList = new List<Task_Order__Share>();
        
        for(Task_Order_Contact__c taskOrderContact : taskOrderContactList){
            contactIdSet.add(taskOrderContact.Contact__c);
        }
        for(User usr : [Select Id From User where contactId In : contactIdSet]){
           Task_Order__share toshare= new  Task_Order__share();
           toshare.ParentId = tOrderId;           
           toshare.UserOrGroupId = usr.id;           
           toshare.Accesslevel='Read';                                      
           taskOrderShareList.add(toshare);
        }
        if(!taskOrderShareList.isEmpty()){
            //insert taskOrderShareList;
            DMLManager.insertAsUser(taskOrderShareList);
        }
    }*/
    
    public class TaskOrderContactWrapper{
        public Task_Order_Contact__c taskOrderContact{get; set;}
        public Boolean status{get; set;}
        
        public TaskOrderContactWrapper(Task_Order_Contact__c taskOrderContact){
            this.taskOrderContact = taskOrderContact;
            status = false;
        }
    }

}