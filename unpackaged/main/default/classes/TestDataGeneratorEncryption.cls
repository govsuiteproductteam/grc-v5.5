public with sharing class TestDataGeneratorEncryption {
    
    public static Document createDocument(String docName,Id folderId){
        Document document = new Document();
        document.FolderId = folderId;
        document.Name = docName;
        document.DeveloperName = docName;
        return document;
    }
    
    public static User createUser(Id profileId, String uniqueWord){       
        User user = new User(Alias = 'standt'+uniqueWord, Email='standarduser'+uniqueWord+'@testorg.com', 
                             EmailEncodingKey='UTF-8', LastName='Testing'+uniqueWord, LanguageLocaleKey='en_US', 
                             LocaleSidKey='en_US', ProfileId = profileId, 
                             TimeZoneSidKey='America/Los_Angeles', UserName='standarduser'+uniqueWord+'@testorg.com');
        return user;
    }
    
}