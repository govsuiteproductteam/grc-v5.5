({
	doInit : function(component, event, helper) {
		//helper.doInitHlpr(component, event, helper);
         var isLiecenceValid =  component.get("c.getUserAccesibility");
        isLiecenceValid.setCallback(this,function(validityData){
            var state= validityData.getState();
			//console.log('asddddsds'+validityData.getReturnValue()) ;           
            if(state==="SUCCESS"){
                if(validityData.getReturnValue() === 'Yes'){
                    component.set("v.checkLicense",validityData.getReturnValue());
                    helper.doInitHlpr(component, event, helper);
                }else{
                    component.set("v.checkLicense",validityData.getReturnValue());
                }
            }
        });
        $A.enqueueAction(isLiecenceValid);
	},
    navigateToContact : function(component, event, helper) {
        helper.navigateToContactHlpr(component, event, helper);
    },
    sendEmail: function(component, event, helper) {
        helper.sendEmailHlpr(component, event, helper,false);
    },
    sendCustomEmail: function(component, event, helper) {
         var subErr = component.find("sub-err");
        var bodyErr = component.find("body-err");
        $A.util.addClass(subErr,"slds-hide");
        $A.util.addClass(bodyErr,"slds-hide");
         var emailList = component.get("v.emailWraList");
            if(!$A.util.isEmpty(emailList) && ! $A.util.isUndefined(emailList)){
                for(var i=0;i<emailList.length;i++){
                    if(emailList[i].isSelected)
                        emailList[i].isSelected = false;
                }
            }
        component.set("v.emailWraList",emailList);
        var subjectToSet = component.find("customemailsub").get("v.value");
        var emailBodyToSet = component.find("customemailbody").get("v.value");
        var toastEvent = $A.get("e.force:showToast");
        if(subjectToSet == null || subjectToSet==''){
            
            $A.util.removeClass(subErr,"slds-hide");
        }if(emailBodyToSet== null || emailBodyToSet==''){
            
            $A.util.removeClass(bodyErr,"slds-hide");
            
        }if(subjectToSet != null && subjectToSet !='' && emailBodyToSet!= null && emailBodyToSet!= ''){
           
            $A.util.addClass(subErr,"slds-hide");
            
            $A.util.addClass(bodyErr,"slds-hide");
            helper.sendEmailHlpr(component, event, helper,true);
            helper.closeMailPopUpHlpr(component, event, helper);
        }
        
    },
    noQuestionnaire :function(component, event, helper) {
        
            helper.noQuestionnaireHlpr(component, event, helper);
        
    },
    refreshEmailTempList:function(component, event, helper) {
        
        helper.refreshEmailTempListHlpr(component, event, helper);
         
    },
    openMailPopUp:function(component, event, helper) {
        helper.openMailPopUpHlpr(component, event, helper);
    },
    closeMailPopUp:function(component, event, helper) {
        helper.closeMailPopUpHlpr(component, event, helper);
    },
    goToFedCapOpp:function(component, event, helper) {
        helper.goToFedCapOppHlpr(component, event, helper);
    },
    checkAllContacts :function(component, event, helper) {
        helper.checkAllContactsHlpr(component, event, helper);
    },
    checkOnContact : function(component,event,helper){
        helper.checkContactClick(component, event, helper);
    },
    checkAllAttachments: function(component,event,helper){
        helper.checkAllAttachmentsHlpr(component, event, helper);
    },
    attachClickSelect: function(component,event,helper){
        helper.attachClickSelectHlpr(component, event, helper);
    }
})