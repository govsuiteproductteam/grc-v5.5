@isTest
public class RTEPRecordConnectPostTest {
    
    public static testMethod void testRTEPRecordCase1(){
        StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'RTEPRecordDetails' LIMIT 1];
        String body = sr.Body.toString();
        
        Test.startTest();        
        //scenario 1
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        req.requestBody = Blob.valueof(body);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        String urls = RTEPRecordConnectPost.doPost();
        System.assert(urls.length()>0);  
        Test.stopTest();
        
    }
    
    public static testMethod void testRTEPRecordCase2(){
        String body = '';
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();      
        req.requestBody = Blob.valueof(body);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        String urls = RTEPRecordConnectPost.doPost();
        System.assert(urls == '' || urls == 'SPLITURL');
        
    }
}