global with sharing class SharingTaskOrderTriggerHelper{
    
    public void executeSharing(List<Teaming_Partner__c> teamingPartnerList){
        if(Schema.sObjectType.Task_Order_Contact__c.isAccessible() && Schema.sObjectType.Vehicle_Partner__c.isAccessible() && Schema.sObjectType.Task_Order__share.isAccessible() && Schema.sObjectType.Contact.isAccessible() && Schema.sObjectType.User.isAccessible()){
            Set<Id> taskOrderAccessRemoveIdSet = new Set<ID>();
            Set<Id> taskOrderAccessGiveIdSet = new Set<ID>();
            set<Id> vehicalPartnerremoveAccess = new set<Id>();
            set<Id> vehicalPartnerGiveAccess = new set<Id>();
            Map<Id,Id>mapContctUserIds = new Map<Id,Id>();
            List<Task_Order__Share> taskOrderShareList = new List<Task_Order__Share>();
            for(Teaming_Partner__c teamingPartner : teamingPartnerList){
                if(teamingPartner.Status__c=='Not Selected' || teamingPartner.Status__c == 'Partner Not Interested' ){
                    taskOrderAccessRemoveIdSet.add(teamingPartner.Task_Order__c);
                    vehicalPartnerremoveAccess.add(teamingPartner.Vehicle_Partner__c);
                }else{
                    taskOrderAccessGiveIdSet.add(teamingPartner.Task_Order__c);
                    vehicalPartnerGiveAccess.add(teamingPartner.Vehicle_Partner__c);
                }
            }
            List<Vehicle_Partner__c> vehicalPartnerRemoveList = [SELECT Partner__c FROM Vehicle_Partner__c WHERE Id In :vehicalPartnerremoveAccess];
            List<Vehicle_Partner__c> vehicalPartnerGiveList = [SELECT Partner__c FROM Vehicle_Partner__c WHERE Id In :vehicalPartnerGiveAccess];
            List<Task_Order_Contact__c>taskOrderConGiveAccess = [SELECT Contact__c, Task_Order__c FROM Task_Order_Contact__c WHERE Task_Order__c IN :taskOrderAccessGiveIdSet];
            List<Task_Order_Contact__c>taskOrderConRemoveAccess = [SELECT Contact__c, Contact__r.AccountId, Task_Order__c FROM Task_Order_Contact__c WHERE Task_Order__c IN :taskOrderAccessRemoveIdSet];
            
    
            ////////// give user access
            Set<Id> conPartnerIdGiveList = (new map<id,Contact>([Select Id FROM Contact WHERE accountId In :getAccountIdList(vehicalPartnerGiveList)])).keySet();
            conPartnerIdGiveList.retainAll(getContactIdfromTOrderCon(taskOrderConGiveAccess));
            List<User> provideAccessUsers = [Select Id, contactId, contact.AccountId From User where contactId In :conPartnerIdGiveList];
            for(User usr : provideAccessUsers){
                mapContctUserIds.put(usr.contactId,usr.Id);
            }
            
            for(Task_Order_Contact__c tordercon : taskOrderConGiveAccess){
                if(mapContctUserIds.get(tordercon.Contact__c)!=Null){
                    Task_Order__share toshare= new  Task_Order__share();
                    toshare.ParentId = tordercon.Task_Order__c;           
                    toshare.UserOrGroupId = mapContctUserIds.get(tordercon.Contact__c);           
                    toshare.Accesslevel='Read';                                      
                    taskOrderShareList.add(toshare);
                }
            }
            if(!taskOrderShareList.isEmpty()){
                try{
                    //insert taskOrderShareList;
                    DMLManager.insertAsUser(taskOrderShareList);
                }catch(Exception ex){}
            }
            ////////// give user access end
            ////////// remove access
            ///New dev @ 05-10-2015 variables
            //
                Map<Id,Set<Id>>mapTaskorderIdContactIdRemove = new Map<Id,Set<Id>>();
                List<Id> tOrderIdListRemoveAcc = new List<Id>();
                List<Id> contactIdListRemoveAcc = new List<Id>();
                
                for(Task_Order_Contact__c taskOrderContact : taskOrderConRemoveAccess){
                    if(mapTaskorderIdContactIdRemove.get(taskOrderContact.Task_Order__c)!=Null){
                        mapTaskorderIdContactIdRemove.get(taskOrderContact.Task_Order__c).add(taskOrderContact.Contact__r.AccountId);
                    }else{
                        mapTaskorderIdContactIdRemove.put(taskOrderContact.Task_Order__c, new Set<Id>{taskOrderContact.Contact__r.AccountId});
                    }
                    tOrderIdListRemoveAcc.add(taskOrderContact.Task_Order__c);
                    contactIdListRemoveAcc.add(taskOrderContact.Contact__c);
                }
            
            Set<Id> contactIdToRemove = (new map<id,Contact>([SELECT ID FROM Contact WHERE accountId In : getAccountIdList(vehicalPartnerRemoveList) AND Id In :contactIdListRemoveAcc])).keySet();// ;
            //List<Id> tOrderIdListRemoveAcc = getContactIdfromTOrderCon(taskOrderConRemoveAccess,false);
            //Set<Id> conPartnerIdRemoveList = (new map<id,Contact>([Select Id FROM Contact WHERE accountId In :getAccountIdList(vehicalPartnerGiveList)])).keySet();
            List<User> usserRemoveList = [Select Id, contact.accountId From User where contactId In :contactIdToRemove];
            Map<id,id>userAndContactIdMap = new Map<id,id>();
            for(User u : usserRemoveList){
                userAndContactIdMap.put(u.Id,u.contact.accountId);
            }            
            Set<Id> userIdToRemove = userAndContactIdMap.keySet();
            try{            
                List<Task_Order__share> deleteTOrderShare = new List<Task_Order__share>();
                for(Task_Order__share tOrderShare : [SELECT Id, ParentId, UserOrGroupId FROM Task_Order__share WHERE UserOrGroupId IN :userIdToRemove AND ParentId In :tOrderIdListRemoveAcc]){
                    if(mapTaskorderIdContactIdRemove.get(tOrderShare.ParentId)!=Null && mapTaskorderIdContactIdRemove.get(tOrderShare.ParentId).contains(userAndContactIdMap.get(tOrderShare.UserOrGroupId))){
                        deleteTOrderShare.add(tOrderShare);
                    }
                }               
                DMLManager.deleteAsUser(deleteTOrderShare);
            }catch(Exception ex){}
        }
    }
    
    private List<Id> getContactIdfromTOrderCon(List<Task_Order_Contact__c>torderContact){
        List<Id> contactIds = new List<ID>();       
        for(Task_Order_Contact__c tOrderCon : torderContact){
            contactIds.add(tOrderCon.Contact__c);
        }
        return contactIds;
    }
    private List<Id> getAccountIdList(List<Vehicle_Partner__c> vehicalPartners){
        List<Id> accountIds = new List<ID>();
        for(Vehicle_Partner__c vp : vehicalPartners){
           accountIds.add(vp.Partner__c);
        }
        return accountIds;
    }
}