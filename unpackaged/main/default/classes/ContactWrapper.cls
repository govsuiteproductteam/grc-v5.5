public class ContactWrapper{
        @AuraEnabled
        public boolean isSelected{get;set;}
        @AuraEnabled
        public Task_Order_Contact__c con {get;set;}
        
        public ContactWrapper(Task_Order_Contact__c con){
            this.con = con;
            this.isSelected=false;
        }
    }