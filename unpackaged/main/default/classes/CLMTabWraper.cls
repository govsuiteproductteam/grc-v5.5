public class CLMTabWraper {
    @AuraEnabled
    public List<CLMSectionWrap> sectionWrapList{get;set;}
    @AuraEnabled
    public CLM_Tab__c tab{get;set;}
    @AuraEnabled
    public Integer order{get;set;}
    @AuraEnabled
    public Map<Integer,CLMSectionWrap> sectionWrapMap{get;set;}
    @AuraEnabled
    Public String tabNameTrimmed{get;set;}
    @AuraEnabled
    public String tabDataRecord{get;set;} //includes tabname(Trimmed)+#####+tab color
    @AuraEnabled
    public Boolean isLoadedInDom{get;set;}
    
    public CLMTabWraper(CLM_Tab__c tab){
        this.tab = tab;
        this.order = (Integer)tab.Order__c;
        this.sectionWrapMap = new Map<Integer,CLMSectionWrap>();
        this.sectionWrapList = new List<CLMSectionWrap>();
        this.tabNameTrimmed= (tab.TabName__c).replaceAll('[^a-z^A-z^0-9]', '');
        this.tabDataRecord = (tab.TabName__c).replaceAll('[^a-z^A-z^0-9]', '')+'#####'+tab.BackgroundColor__c;
    }
}