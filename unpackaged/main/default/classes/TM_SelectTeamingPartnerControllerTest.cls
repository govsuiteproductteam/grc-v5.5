@isTest
public with sharing class TM_SelectTeamingPartnerControllerTest{
    public static TestMethod void TestTM_SelectTeamingPartnerController(){
        List<Profile> profileList = [SELECT Id FROM Profile where (Name = 'Custom Partner Community User' OR Name = 'Custom Partner Community Login User' OR Name  = 'Custom Customer Community Plus Login User') Limit 1];
        if(profileList != null && profileList.size()>0){
        
            Account acc = TestDataGenerator.createAccount('TaskOrderCapture');
            insert acc;
            
            Contact con = TestDataGenerator.createContact('Avanti',acc);
            insert con;
            
            User usr = TestDataGenerator.createUser(con,TRUE,'test1', 'atest1','test1TeamingPartner@gmail.com','taskorderTeamingPartner@demo.com','atest1',profileList[0],'America/Los_Angeles','en_US','UTF-8','en_US');
            insert usr;    
            
            Contract_Vehicle__c contractVehicle = TestDataGenerator.createContractVehicle('TMconVehicle',acc);
            insert contractVehicle;
            
            Task_Order__c taskOrder = TestDataGenerator.createTaskOrder('TMtaskOrder', acc, contractVehicle);
            insert taskOrder;
            
            Contract_Vehicle_Contact__c contVehicleContact = TestDataGenerator.createContractVehicleContact(con , contractVehicle );
            
            contVehicleContact.Main_POC__c = true;
            contVehicleContact.Receives_Mass_Emails__c = true;
            insert contVehicleContact;
            
            Task_Order_Contact__c taskOrderContact = TestDataGenerator.createTaskOrderContact(taskOrder , con);
            insert taskOrderContact ;
            
            Vehicle_Partner__c vehiclePartner = TestDataGenerator.createVehiclePartner(acc,contractVehicle );
            insert vehiclePartner;
            Vehicle_Partner__c vehiclePartner1 = TestDataGenerator.createVehiclePartner(acc,contractVehicle );
            
            Teaming_Partner__c teamPartner = TestDataGenerator.createTeamingPartner(vehiclePartner1 , taskOrder);
            insert teamPartner;
            
            Test.startTest();
            ApexPages.CurrentPage().getparameters().put('tid', taskOrder.id);
            ApexPages.StandardController controller = new ApexPages.StandardController(taskOrder);
            TM_SelectTeamingPartnerController selectPartnerController = new TM_SelectTeamingPartnerController (controller );
           
            selectPartnerController.vehiclePartnerWrapList[0].status = true;
            
            selectPartnerController.addTeamingPartner();
            selectPartnerController.cancel();
             system.assertNotEquals(selectPartnerController.vehiclePartnerWrapList.size(), 0);
            Test.stopTest();
       }
        
    }
}