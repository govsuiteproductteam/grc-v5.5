public class EmailWrapper{
        @AuraEnabled
        public boolean isSelected{get;set;}
        @AuraEnabled
        public EmailTemplate emailTemp {get;set;}
        
        public EmailWrapper(EmailTemplate emailTemp){
            this.emailTemp = emailTemp;
            this.isSelected=false;
        }
    }