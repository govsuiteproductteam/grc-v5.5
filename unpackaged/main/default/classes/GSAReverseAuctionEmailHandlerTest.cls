@isTest
public class GSAReverseAuctionEmailHandlerTest {
    
    public static testmethod void testCase1(){
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Contact con = TestDataGenerator.createContact('TestCon', acc );
        insert con;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('T-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'gsa_reverse_auctions@7-1ar3hpiroldlgf41q6o8iby05716kq4k5evzhhqvtq4qo9ilh4.61-zhsweao.na34.apex.salesforce.com';
        
        insert conVehi;
        
        //For new Task Order Creation
        Messaging.InboundEmail email  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        List<StaticResource> srList = [SELECT Id,Body FROM StaticResource WHERE Name='GSAReverseAuctionNew'];
        email.plainTextBody = srList.get(0).Body.toString();
        //System.debug('Body = '+email.plainTextBody);
        email.fromAddress ='test@test.com';
        email.fromName = 'ABC XYZ';
        email.subject = 'Reverse Auctions: Invitation to bid for Auction: 12270 Auction Name:LED MONITORS';
        email.toaddresses = new List<String>();
        email.toaddresses.add('gsa_reverse_auctions@7-1ar3hpiroldlgf41q6o8iby05716kq4k5evzhhqvtq4qo9ilh4.61-zhsweao.na34.apex.salesforce.com');
        
        
        //To update existing task order 
        Messaging.InboundEmail email1  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env1 = new Messaging.InboundEnvelope();
        List<StaticResource> srList1 = [SELECT Id,Body FROM StaticResource WHERE Name='GSAReverseAuctionUpdated'];
        email1.plainTextBody = srList1.get(0).Body.toString();
        //System.debug('Body = '+email1.plainTextBody);
        email1.fromAddress ='test@test.com';
        email1.fromName = 'ABC XYZ';
        email1.subject = 'Reverse Auctions: Invitation to bid for Auction: 12270 Auction Name:LED MONITORS';
        email1.toaddresses = new List<String>();
        email1.toaddresses.add('gsa_reverse_auctions@7-1ar3hpiroldlgf41q6o8iby05716kq4k5evzhhqvtq4qo9ilh4.61-zhsweao.na34.apex.salesforce.com');
        
        GSAReverseAuctionEmailHandler handler = new GSAReverseAuctionEmailHandler();
        Test.startTest();
        Messaging.InboundEmailResult result = handler.handleInboundEmail(email, env);
        List<Task_Order__c> taskOrderList = [SELECT Id FROM Task_Order__c];
        Messaging.InboundEmailResult result1 = handler.handleInboundEmail(email1, env1);
        List<Contract_Mods__c> contractModList = [SELECT Id FROM Contract_Mods__c];
        Test.stopTest();
        
        System.assertEquals(1, taskOrderList.size());
        System.assertEquals(1, contractModList.size());
    }
}