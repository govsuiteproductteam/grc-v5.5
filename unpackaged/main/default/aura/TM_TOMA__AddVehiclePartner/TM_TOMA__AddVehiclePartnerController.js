({
    checkAccess : function(component, event, helper) {
        var isLiecenceValid =  component.get("c.checkLicensePermition");
        isLiecenceValid.setCallback(this,function(validityData){
            var state= validityData.getState();
            if(state=="SUCCESS"){
                console.log("Stages Status"+validityData.getReturnValue());
                if(validityData.getReturnValue() == 'Yes'){
                    component.set("v.CheckLicense",true);
                    helper.getRole1(component, event, helper);
                    helper.getVehicalPatnerList1(component, event, helper);
                }/*else if(validityData.getReturnValue() == 'No'){
                    component.set("v.CheckLicense",false);
                }*/
                    else{
                    component.set("v.CheckLicense",false);
                        component.set("v.errorMsg",validityData.getReturnValue());
                }
            }
            else if (state == "ERROR") 
            {
                console.log(validityData.getError());            
            }
        });
        $A.enqueueAction(isLiecenceValid);
    },
    
    getVehicalPatnerList : function(component, event, helper) {
        helper.getVehicalPatnerList1(component, event, helper);
    },
    getRole: function(component, event, helper) {
        helper.getRole1(component, event, helper);  
    },
    selectCurrentValue :function(component, event, helper){
        var temp = component.get("v.vehicalPatnerList");
        if(temp != undefined){
            for(var i=0;i<temp.length;i++)
            {
                if(event.getSource().get("v.text") === i)
                {
                    temp[i].TM_TOMA__Role__c = event.getSource().get("v.value");
                }
            }
            var obj=component.set("v.vehicalPatnerList",temp);
        }
    },
    showPatner : function (component, event, helper) {
        var selectedItem = event.currentTarget;
        var tempId = selectedItem.id;
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": tempId,
            "slideDevName": "detail"
        });
        navEvt.fire();
    },
    addVehicalPartner : function(component, event, helper) {
        // alert("inside add");
        var vehicalPatnerList1=component.get("v.vehicalPatnerList");
        var action = component.get("c.addNewVehicalPartner"); //call apex controller method
        action.setParams({
            "contractVehicalId": component.get("v.recordId")
        });
        action.setCallback(this,function(a){
            var state= a.getState();
            if(state=="SUCCESS"){
                var newVal=a.getReturnValue();
                if(newVal.Id === '' || newVal.Id === null || newVal.Id === undefined){
                    newVal.TM_TOMA__Partner__c='';
                }
                vehicalPatnerList1.push(newVal);
                component.set("v.vehicalPatnerList",vehicalPatnerList1);
                var checksave = component.get("v.checkSavebutton");
                checksave=checksave+1;
                component.set("v.checkSavebutton",checksave);
                var RefreshVehicalPatnerPageFiredEvent =$A.get("e.c:RefreshVehicalPatnerPage"); //component.getEvent("RefreshVehicalPatnerPageFired");
                RefreshVehicalPatnerPageFiredEvent.fire();
            }
            else if (state == "ERROR") 
            {
                console.log(a.getError());            
            }
        });
        $A.enqueueAction(action);
    },
    editVehicalPatner : function (component, event, helper) {
        var selectedItem = event.currentTarget;
        var tempId = selectedItem.id;
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            "recordId": tempId
        });
        editRecordEvent.fire();
    },
    editInlineRole : function(component, event, helper) {
        var selectedItem = event.currentTarget;
        var checkCounter = selectedItem.id;
        var d = document.getElementById(checkCounter+'editInlineOut');
        d.className += " slds-hide";
        d.classList.remove("slds-show");
        var d1 = document.getElementById(checkCounter+'editInlineIn');
        d1.className += " slds-show";
        d1.classList.remove("slds-hide");
        var d2 = document.getElementById(checkCounter+'editDiv');
        d2.className += " slds-hide";
        d2.classList.remove("slds-show");
        var d3 = document.getElementById(checkCounter+'editUndoDiv');
        d3.className += " slds-show";
        d3.classList.remove("slds-hide");
        /* var saveBtn = document.getElementById('saveVehiclePartner');
        saveBtn.className += " slds-show";
        saveBtn.classList.remove("slds-hide");*/
        var checksave = component.get("v.checkSavebutton");
        checksave=checksave+1;
        component.set("v.checkSavebutton",checksave);
        // var oldval = selectedItem.dataset.arg1;
        document.getElementById(checkCounter+'edit').dataset.arg1 = selectedItem.dataset.arg1;
    },
    closeInlineRole : function(component, event, helper) {
        var selectedItem = event.currentTarget;
        var checkCounter = selectedItem.id;
        var oldval = selectedItem.dataset.arg1;
        //event.currentTarget.dataset.caseID;
        var d = document.getElementById(checkCounter+'InlineOut');
        d.className += " slds-show";
        d.classList.remove("slds-hide");
        var d1 = document.getElementById(checkCounter+'InlineIn');
        d1.className += " slds-hide";
        d1.classList.remove("slds-show");
        //alert(checkCounter);
        var d2 = document.getElementById(checkCounter+'Div');
        d2.className += " slds-show";
        d2.classList.remove("slds-hide");
        var d3 = document.getElementById(checkCounter+'UndoDiv');
        d3.className += " slds-hide";
        d3.classList.remove("slds-show");
        var checksave = component.get("v.checkSavebutton");
        checksave=checksave-1;
        component.set("v.checkSavebutton",checksave);
        var index = selectedItem.dataset.arg2;
        var temp = component.get("v.vehicalPatnerList");
        if(temp != undefined){
            for(var i=0;i<temp.length;i++)
            {
                if(index === i+'')
                {
                    temp[i].TM_TOMA__Role__c = oldval;
                }
            }
            component.set("v.vehicalPatnerList",temp);
        }
    },
    removeVehicalPatner : function(component, event, helper) {
         var conf = confirm("Are you sure you want to delete this Vehicle Partner record? This action will delete all Teaming Partner records on related Task Orders.");
        if(conf== true)
        {
        var vehicalPatnerList1 = component.get("v.vehicalPatnerList");
        var selectedItem = event.currentTarget;
        var removeCounter = selectedItem.id;
       // var recName = selectedItem.dataset.arg2;
        var tempCont = selectedItem.dataset.arg1;
        //alert(removeCounter);
        if (removeCounter != undefined && removeCounter != '') {
            var action = component.get("c.deleteVehicalPatner");
            action.setParams({ 
                "delId": removeCounter
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                if (state === "SUCCESS")
                {
                    helper.getVehicalPatnerList1(component, event, helper);
                    var RefreshVehicalPatnerPageFiredEvent =$A.get("e.c:RefreshVehicalPatnerPage");
                    RefreshVehicalPatnerPageFiredEvent.fire();
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "Vehicle Partner Was Deleted",
                        "type":"success"
                    });
                    toastEvent.fire();
                }
                else if (state === "ERROR") 
                {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": "Error occurred while deleting the record.Please refresh the page.",
                        "type":"error"
                    });
                    toastEvent.fire();
                    console.log(response.getError());    
                }
            });
            $A.enqueueAction(action);
        } 
        else{
            var checksave = component.get("v.checkSavebutton");
            checksave=checksave-1;
            component.set("v.checkSavebutton",checksave);
            vehicalPatnerList1.splice(tempCont, 1);
            component.set("v.vehicalPatnerList", vehicalPatnerList1);
        }
    }
    },
    saveVehicalPartner : function (component, event, helper){
             
        document.getElementById("saveVehiclePartner").disabled = true;
        var invalidDateRow = ''
        var valTableList=component.get("v.vehicalPatnerList");
        
        var partnerDataString = '[';
        var checkPartner = 'No';
        if(valTableList != undefined){
            var listSize = valTableList.length;
            for (var i=0; i<listSize; i++){
                for (var j=0; j<listSize; j++){  
                    if(i!=j){
                        if(valTableList[i].TM_TOMA__Partner__c == valTableList[j].TM_TOMA__Partner__c){     
                            document.getElementById("saveVehiclePartner").disabled = false;
                            console.log("if");
                            var toastEvent = $A.get("e.force:showToast");                            
                            toastEvent.setParams({
                                "title": "Error!",
                                "message": "This partner has already been setup",
                                "type":"error"
                            });
                            toastEvent.fire();
                            return;
                        }
                    }
                }
            }
            
            for (var i=0; i<listSize; i++){                 
                var vTId = valTableList[i].Id;
                var ContractVehicle = component.get("v.recordId");
                var Partner = valTableList[i].TM_TOMA__Partner__c;
                var Role__c  = valTableList[i].TM_TOMA__Role__c === undefined?'':valTableList[i].TM_TOMA__Role__c;
                var appendData = '';
                if(Partner===undefined || Partner==='' || Partner===null)
                {
                    checkPartner = 'Yes';
                    invalidDateRow=invalidDateRow+','+i;
                }
                partnerDataString += vTId+'SPLITDATA'+ContractVehicle+'SPLITDATA'+Partner+'SPLITDATA'+Role__c+'SPLITPARENT';
            }
        }
        if(checkPartner === 'No'){
            var action = component.get("c.saveVehicalPatner");
            action.setParams({ 
                "vehicalPatnerList": partnerDataString
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                if (state === "SUCCESS")
                {
                    helper.getVehicalPatnerList1(component, event, helper);
                    var RefreshVehicalPatnerPageFiredEvent =$A.get("e.c:RefreshVehicalPatnerPage"); //component.getEvent("RefreshVehicalPatnerPageFired");
                    RefreshVehicalPatnerPageFiredEvent.fire();
                    component.set("v.checkSavebutton",0);
                    document.getElementById("saveVehiclePartner").disabled = false;
                    //alert("Data save successfully");
                    /*  var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "Data Updated successfully.",
                        "type":"success"
                    });
                    toastEvent.fire(); */
                }
                else if (state === "ERROR") 
                {
                    console.log(response.getError()); 
                    component.set("v.checkSavebutton",0);
                    document.getElementById("saveVehiclePartner").disabled = false;
                }
            });
            $A.enqueueAction(action);
        }
        else{
            var checkId = invalidDateRow.split(',');
            if(checkId != undefined){
                for(var i = 1; i < checkId.length; i++) {
                    var d = document.getElementById(checkId[i]+'vehicle');
                    d.className += " validationClass";
                }
            }
            
            document.getElementById("saveVehiclePartner").disabled = false;
            // alert('Please enter value for Account');
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "message": "Please enter value for Account.",
                "type":"error"
            });
            toastEvent.fire();
        }
        /*}else{
            alert("This partner has already been setup");
        }*/
    }
})