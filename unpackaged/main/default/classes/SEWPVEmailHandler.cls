global class SEWPVEmailHandler implements Messaging.InboundEmailHandler {

    SEWPVEmailParser parser = new SEWPVEmailParser();
    Task_Order__c taskOrder;
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:email.toAddresses.get(0)];
        if(!conVehicalList.isEmpty()){
            parser.parse(email);//Email Parsing will be handle by parse method.
            if(parser.taskOrderNumber!=null && parser.taskOrderNumber.trim().length()>0){
                /*Below Checked for existing TaskOrder if alredy created before*/
                taskOrder = EmailHandlerHelper.getExistTaskOrder(parser.taskOrderNumber, conVehicalList[0].ID);
                
                if(taskOrder == null){//If email template not exists            
                    taskOrder = parser.getTaskOrder(email.toAddresses.get(0));
                    if(taskOrder != null){  
                        try{
                            DMLManager.insertAsUser(taskOrder);
                            EmailHandlerHelper.insertBinaryAttachment(taskOrder,email);
                            EmailHandlerHelper.insertTextAttachment(taskOrder,email);
                            EmailHandlerHelper.insertActivityHistory(taskOrder,email);
                            // If modLevel is greater than 0
                            if(parser.modLevel != null && Integer.valueOf(parser.modLevel) > 0){
                                createContractMod(parser.modLevel);  
                            }
                            
                        }catch(Exception e){
                            System.debug('error message = '+e.getMessage());
                        }
                    }
                }else{// If TaskOrder already present then execution comes to else block                      
                        Task_Order__c oldTaskOrder = taskOrder.clone(false,true,false,false);       
                        Task_Order__c updatedTakOrder = parser.updateTaskOrder(taskOrder, email.toAddresses.get(0));
                        //DMLManager.updateAsUser(updatedTakOrder);
                        try{
                            EmailHandlerHelper.insertContractMods(updatedTakOrder , oldTaskOrder,email);
                            update updatedTakOrder;                        
                            EmailHandlerHelper.insertBinaryAttachment(updatedTakOrder,email);
                            EmailHandlerHelper.insertTextAttachment(updatedTakOrder,email);
                            EmailHandlerHelper.insertActivityHistory(updatedTakOrder,email);    
                            List<Contract_Mods__c> ModificationList = [SELECT Id FROM Contract_Mods__c WHERE TaskOrder__c =:updatedTakOrder.Id AND Modification_Name__c =:parser.modLevel];
                            if(parser.isCancel){
                                createContractMod('Cancellation');
                            }
                            //Below Condition is, if the Modification Level is not already present and modLevel is greater than 0
                            else if(ModificationList.isEmpty() && parser.modLevel != null && Integer.valueOf(parser.modLevel) > 0){
                                createContractMod(parser.modLevel);                           
                            }
                            system.debug('Already Exists');                              
                        }catch(Exception e){
                            System.debug('error message = '+e.getMessage()+'\tline number = '+e.getLineNumber());
                        }
                    //}     
                }
            }
        }
        else{
            System.debug('Contract Vehicle not found.');
        }
        return new Messaging.InboundEmailresult();
    }
    
    private void createContractMod(String modificationName){
        Contract_Mods__c contractMod = new Contract_Mods__c();
        RecordType contractModRecordType = [SELECT Id FROM RecordType WHERE DeveloperName = 'Contract_Mod' AND sObjectType = 'TM_TOMA__Contract_Mods__c' LIMIT 1][0];
        contractMod.RecordTypeId = contractModRecordType.Id;
        contractMod.TaskOrder__c = taskOrder.id;
        contractMod.Modification_Name__c = modificationName;
        contractMod.Modification__c = 'Mod Date: '+(parser.modDate!=null? parser.modDate:'')+ 
            '\nMod Remarks: '+(parser.modRemarks!=null?parser.modRemarks:'' )+ 
            '\nAdditional Remarks: '+(parser.additionalRemarks!=null ? parser.additionalRemarks:'');
        DMLManager.insertAsUser(contractMod);    
    }
    
}