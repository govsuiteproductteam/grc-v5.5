public class FirstCaseLookupFilter{
    
    public static List<sObject> filter(List<sObject> inputRecordList,LookupFilterToolingAPI.FilterItem eachFilterItem,String parentSObjectName){
        string Field;//newlly added 22-03
        string valueField;//newlly added 22-03
        string Fieldtype;//newlly added 22-03
        string valueFieldtype;//newlly added 22-03
        List<sObject> filteredRecordList = new List<sObject>();
        //newlly added
        sObject currentUser ;
        //newlly added 22-03
        if(eachFilterItem.field.contains(parentSObjectName+'.') || eachFilterItem.valueField.contains(parentSObjectName+'.')){
            Map<String, Schema.SObjectField> fieldsMapAccessible = Schema.getGlobalDescribe().get(parentSObjectName).getDescribe().SObjectType.getDescribe().fields.getMap();
            if(eachFilterItem.field.contains(''+parentSObjectName+'.'))
            {
                Field = eachFilterItem.field.replace(parentSObjectName+'.','');
                Schema.SObjectField sobjectFieldField = fieldsMapAccessible.get(Field);
                try{
                    Fieldtype = sobjectFieldField.getDescribe().getType() +'';
                }
                catch(Exception e){
                    
                }
            }
            if(eachFilterItem.valueField.contains(''+parentSObjectName))
            {
                valueField = eachFilterItem.valueField.replace(parentSObjectName+'.','');
                Schema.SObjectField sobjectvalueField = fieldsMapAccessible.get(valueField);
                try{
                    valueFieldtype = sobjectvalueField.getDescribe().getType() +'';
                }
                catch(Exception e){
                    
                }
            }
        }
        if(eachFilterItem.field.contains('$User.') || eachFilterItem.valueField.contains('$User.')){
            //newlly added 22-03
            Map<String, Schema.SObjectField> userFieldsMapAccessible = Schema.getGlobalDescribe().get('User').getDescribe().SObjectType.getDescribe().fields.getMap();       
            string userFields='';
            /*if(eachFilterItem.field.contains('$User'))
userFields = eachFilterItem.field.replace('$User.','')+',';
if(eachFilterItem.valueField.contains('$User'))
userFields = eachFilterItem.valueField.replace('$User.','')+',';*/
            //newlly added 22-03
            if(eachFilterItem.field.contains('$User.')){
                userFields = eachFilterItem.field.replace('$User.','')+',';
                Field = eachFilterItem.field.replace('$User.','');
                Schema.SObjectField sobjectFieldField = userFieldsMapAccessible.get(Field);
                try{
                    Fieldtype = sobjectFieldField.getDescribe().getType() +'';
                }
                catch(Exception e){
                    
                }
            }
            if(eachFilterItem.valueField.contains('$User.')){
                userFields = eachFilterItem.valueField.replace('$User.','')+',';
                valueField = eachFilterItem.valueField.replace('$User.','');
                Schema.SObjectField sobjectvalueField = userFieldsMapAccessible.get(valueField);
                try{
                    valueFieldtype = sobjectvalueField.getDescribe().getType() +'';
                }
                catch(Exception e){
                    
                }
            }
            if(userFields != '' && userFields.endsWith(',') )
            {
                userFields = userFields.removeEnd(',');
            }
            //currentUser =  [select Id,IsActive, username from User where Id = :UserInfo.getUserId()]; 
            string userId = ''+UserInfo.getUserId(); 
            System.debug('Select '+userFields+' From User where id = \''+userId+ '\' Limit 1');
            try{
                currentUser = Database.query('Select '+userFields+' From User where id = \''+userId+ '\' Limit 1');
            }
            catch(Exception ex)
            {
                System.debug(' Exception '+ex);
            }
            
        }
        if(eachFilterItem.field.contains('$Profile.') || eachFilterItem.valueField.contains('$Profile.')){
            //newlly added 22-03
            Map<String, Schema.SObjectField> profileFieldsMapAccessible = Schema.getGlobalDescribe().get('Profile').getDescribe().SObjectType.getDescribe().fields.getMap();       
            string profileFields='';
            /* if(eachFilterItem.field.contains('$Profile'))
profileFields = eachFilterItem.field.replace('$Profile.','')+',';
if(eachFilterItem.valueField.contains('$Profile'))
profileFields = eachFilterItem.valueField.replace('$Profile.','')+',';*/
            //newlly added 22-03
            // System.debug('eachFilterItem Profile '+eachFilterItem);
            if(eachFilterItem.field.contains('$Profile')){
                
                profileFields = eachFilterItem.field.replace('$Profile.','')+',';
                Field = eachFilterItem.field.replace('$Profile.','');
                Schema.SObjectField sobjectFieldField = profileFieldsMapAccessible.get(Field);
                try{
                    Fieldtype = sobjectFieldField.getDescribe().getType() +'';
                }
                catch(Exception e){
                    
                }
            }
            if(eachFilterItem.valueField.contains('$Profile')){
                profileFields = eachFilterItem.valueField.replace('$Profile.','')+',';
                valueField = eachFilterItem.valueField.replace('$Profile.','');
                Schema.SObjectField sobjectvalueField = profileFieldsMapAccessible.get(valueField);
                try{
                    valueFieldtype = sobjectvalueField.getDescribe().getType() +'';
                }
                catch(Exception e){
                    
                }
            }
            if(profileFields != '' && profileFields.endsWith(',') )
            {
                profileFields = profileFields.removeEnd(',');
            }
            string profileId = ''+UserInfo.getProfileId(); 
            System.debug('Select '+profileFields+' From Profile where id = \''+profileId+ '\' Limit 1');
            try{
                currentUser = Database.query('Select '+profileFields+' From Profile where id = \''+profileId+ '\' Limit 1');
            }
            catch(Exception ex)
            {
                System.debug(' Exception '+ex);
            }
        }
        if(eachFilterItem.field.contains('$UserRole.') || eachFilterItem.valueField.contains('$UserRole.')){
            //newlly added 22-03
            Map<String, Schema.SObjectField> roleFieldsMapAccessible = Schema.getGlobalDescribe().get('UserRole').getDescribe().SObjectType.getDescribe().fields.getMap();       
            string roleFields='';
            /*if(eachFilterItem.field.contains('$UserRole'))
roleFields = eachFilterItem.field.replace('$UserRole.','')+',';
if(eachFilterItem.valueField.contains('$UserRole'))
roleFields = eachFilterItem.valueField.replace('$UserRole.','')+',';
*/
            //newlly added 22-03
            if(eachFilterItem.field.contains('$UserRole')){
                roleFields = eachFilterItem.field.replace('$UserRole.','')+',';
                Field = eachFilterItem.field.replace('$UserRole.','');
                Schema.SObjectField sobjectFieldField = roleFieldsMapAccessible.get(Field);
                try{
                    Fieldtype = sobjectFieldField.getDescribe().getType() +'';
                }
                catch(Exception e){
                    
                }
            }
            if(eachFilterItem.valueField.contains('$UserRole')){
                roleFields = eachFilterItem.valueField.replace('$UserRole.','')+',';
                valueField = eachFilterItem.valueField.replace('$UserRole.','');
                Schema.SObjectField sobjectvalueField = roleFieldsMapAccessible.get(valueField);
                try{
                    valueFieldtype = sobjectvalueField.getDescribe().getType() +'';
                }
                catch(Exception e){
                    
                }
            }
            if(roleFields != '' && roleFields.endsWith(',') )
            {
                roleFields = roleFields.removeEnd(',');
            }
            string roleId = ''+UserInfo.getUserRoleId(); 
            System.debug('Select '+roleFields+' From UserRole where id = \''+roleId+ '\' Limit 1');
            try{
                currentUser = Database.query('Select '+roleFields+' From UserRole where id = \''+roleId+ '\' Limit 1');
            }
            catch(Exception ex)
            {
                System.debug(' Exception '+ex);
            }
        }
        //end 
        for(sObject obj : inputRecordList){
            if(!isFiltered(obj,eachFilterItem,parentSObjectName,currentUser,Fieldtype,valueFieldtype)){//nwelly added ,currentUser,currentProfile,Fieldtype,valueFieldtype
                filteredRecordList.add(obj);
            }
        }
        return filteredRecordList;
    }
    // private static Boolean isFiltered(sObject obj,LookupFilterToolingAPI.FilterItem eachFilterItem,String parentSObjectName){
    private static Boolean isFiltered(sObject obj,LookupFilterToolingAPI.FilterItem eachFilterItem,String parentSObjectName,sObject userData,string firestFieldDataType,string secoundFieldDataType){//newlly added ,sobject profileData 
        //System.debug('isFiltered eachFilterItem - '+eachFilterItem);
        if(eachFilterItem.operation == 'equals'){
            //newlly added
            if(eachFilterItem.field.contains('$User') || eachFilterItem.field.contains('$Profile') || eachFilterItem.field.contains('$UserRole'))
            {
                try{
                    if(userData.get(eachFilterItem.field.replace('$User.','').replace('$Profile.','').replace('$UserRole.','')) == obj.get(eachFilterItem.valueField.replace(parentSObjectName+'.',''))){
                        return false;
                    }
                }
                catch(Exception ex)
                {
                    System.debug(' Exception '+ex);
                }
            }
            else if(eachFilterItem.valueField.contains('$User') || eachFilterItem.valueField.contains('$Profile') || eachFilterItem.valueField.contains('$UserRole'))
            {
                try{
                    if(obj.get(eachFilterItem.field.replace(parentSObjectName+'.','')) == userData.get(eachFilterItem.valueField.replace('$User.','').replace('$Profile.','').replace('$UserRole.',''))){
                        return false;
                    }
                }
                catch(Exception ex)
                {
                    System.debug(' Exception '+ex);
                }
            }
            else{
                //end
                try{
                    if(obj.get(eachFilterItem.field.replace(parentSObjectName+'.','')) == obj.get(eachFilterItem.valueField.replace(parentSObjectName+'.','').replace('.',''))){//newlly added .replace('.','')
                        return false;
                    }
                }
                catch(Exception ex)
                {
                    System.debug(' Exception '+ex);
                }
            }
        }
        if(eachFilterItem.operation == 'notEqual'){
            if(eachFilterItem.field.contains('$User') || eachFilterItem.field.contains('$Profile') || eachFilterItem.field.contains('$UserRole'))
            {
                try{
                    if(userData.get(eachFilterItem.field.replace('$User.','').replace('$Profile.','').replace('$UserRole.','')) != obj.get(eachFilterItem.valueField.replace(parentSObjectName+'.',''))){
                        return false;
                    }
                }
                catch(Exception ex)
                {
                    System.debug(' Exception '+ex);
                }
            }
            else if(eachFilterItem.valueField.contains('$User') || eachFilterItem.valueField.contains('$Profile') || eachFilterItem.valueField.contains('$UserRole'))
            {
                try{
                    if(obj.get(eachFilterItem.field.replace(parentSObjectName+'.','')) != userData.get(eachFilterItem.valueField.replace('$User.','').replace('$Profile.','').replace('$UserRole.',''))){
                        return false;
                    }
                }
                catch(Exception ex)
                {
                    System.debug(' Exception '+ex);
                }
            }
            else{
                try{
                    if(obj.get(eachFilterItem.field.replace(parentSObjectName+'.','')) != obj.get(eachFilterItem.valueField.replace(parentSObjectName+'.',''))){
                        return false;
                    }
                }
                catch(Exception ex)
                {
                    System.debug(' Exception '+ex);
                }
            }
        }
        if(eachFilterItem.operation == 'startsWith'){
            if(eachFilterItem.field.contains('$User') || eachFilterItem.field.contains('$Profile') || eachFilterItem.field.contains('$UserRole'))
            {
                try{
                    if((userData.get(eachFilterItem.field.replace('$User.','').replace('$Profile.','').replace('$UserRole.',''))+'').startsWith(''+obj.get(eachFilterItem.valueField.replace(parentSObjectName+'.','')))){
                        return false;
                    }
                }
                catch(Exception ex)
                {
                    System.debug(' Exception '+ex);
                }
            }
            else if(eachFilterItem.valueField.contains('$User') || eachFilterItem.valueField.contains('$Profile') || eachFilterItem.valueField.contains('$UserRole'))
            {
                try{
                    if((obj.get(eachFilterItem.field.replace(parentSObjectName+'.',''))+'').startsWith(''+userData.get(eachFilterItem.valueField.replace('$User.','').replace('$Profile.','').replace('$UserRole.','')))){
                        return false;
                    }
                }
                catch(Exception ex)
                {
                    System.debug(' Exception '+ex);
                }
            }
            else{
                try{
                    if((obj.get(eachFilterItem.field.replace(parentSObjectName+'.',''))+'').startsWith(''+obj.get(eachFilterItem.valueField.replace(parentSObjectName+'.','')))){
                        return false;
                    }
                }
                catch(Exception ex)
                {
                    System.debug(' Exception '+ex);
                }
            }
        }
        if(eachFilterItem.operation == 'contains'){
            if(eachFilterItem.field.contains('$User') || eachFilterItem.field.contains('$Profile')  || eachFilterItem.field.contains('$UserRole'))
            {
                try{
                    if((userData.get(eachFilterItem.field.replace('$User.','').replace('$Profile.','').replace('$UserRole.',''))+'').contains(''+obj.get(eachFilterItem.valueField.replace(parentSObjectName+'.','')))){
                        return false;
                    }
                }
                catch(Exception ex)
                {
                    System.debug(' Exception '+ex);
                }
            }
            else if(eachFilterItem.valueField.contains('$User') || eachFilterItem.valueField.contains('$Profile') || eachFilterItem.valueField.contains('$UserRole'))
            {
                try{
                    if((obj.get(eachFilterItem.field.replace(parentSObjectName+'.',''))+'').contains(''+userData.get(eachFilterItem.valueField.replace('$User.','').replace('$Profile.','').replace('$UserRole.','')))){
                        return false;
                    }
                }
                catch(Exception ex)
                {
                    System.debug(' Exception '+ex);
                }
            }
            else{
                try{
                    if((obj.get(eachFilterItem.field.replace(parentSObjectName+'.',''))+'').contains(''+obj.get(eachFilterItem.valueField.replace(parentSObjectName+'.','')))){
                        return false;
                    }
                }
                catch(Exception ex)
                {
                    System.debug(' Exception '+ex);
                }
            }
        }
        if(eachFilterItem.operation == 'notContain'){
            if(eachFilterItem.field.contains('$User') || eachFilterItem.field.contains('$Profile')  || eachFilterItem.field.contains('$UserRole'))
            {
                try{
                    if(!(userData.get(eachFilterItem.field.replace('$User.','').replace('$Profile.','').replace('$UserRole.',''))+'').contains(''+obj.get(eachFilterItem.valueField.replace(parentSObjectName+'.','')))){
                        return false;
                    }
                }
                catch(Exception ex)
                {
                    System.debug(' Exception '+ex);
                }
            }
            else if(eachFilterItem.valueField.contains('$User') || eachFilterItem.valueField.contains('$Profile')  || eachFilterItem.valueField.contains('$UserRole'))
            {
                try{
                    if(!(obj.get(eachFilterItem.field.replace(parentSObjectName+'.',''))+'').contains(''+userData.get(eachFilterItem.valueField.replace('$User.','').replace('$Profile.','').replace('$UserRole.','')))){
                        return false;
                    }
                }
                catch(Exception ex)
                {
                    System.debug(' Exception '+ex);
                }
            }
            else{
                try{
                    if(!(obj.get(eachFilterItem.field.replace(parentSObjectName+'.',''))+'').contains(''+obj.get(eachFilterItem.valueField.replace(parentSObjectName+'.','')))){
                        return false;
                    }
                }
                catch(Exception ex)
                {
                    System.debug(' Exception '+ex);
                }
            }
        }
        if(eachFilterItem.operation == 'lessThan' || eachFilterItem.operation == 'within'){
            try{
                //String firstValueStr = obj.get(eachFilterItem.field.replace(parentSObjectName+'.',''))+'';
                // String secondValueStr = obj.get(eachFilterItem.valueField.replace(parentSObjectName+'.',''))+'';
                // newlly added 22-03
                String firstValueStr;
                String secondValueStr;
                if(eachFilterItem.field.contains('$User') || eachFilterItem.field.contains('$Profile')  || eachFilterItem.field.contains('$UserRole'))
                {
                    firstValueStr =  userData.get(eachFilterItem.field.replace('$User.','').replace('$Profile.','').replace('$UserRole.',''))+'';
                }
                else{
                    firstValueStr = obj.get(eachFilterItem.field.replace(parentSObjectName+'.',''))+''; 
                }
                if(eachFilterItem.valueField.contains('$User') || eachFilterItem.valueField.contains('$Profile')  || eachFilterItem.valueField.contains('$UserRole'))
                {
                    secondValueStr = userData.get(eachFilterItem.valueField.replace('$User.','').replace('$Profile.','').replace('$UserRole.',''))+'';
                }
                else{
                    secondValueStr = obj.get(eachFilterItem.valueField.replace(parentSObjectName+'.',''))+'';
                }
                if(firstValueStr.isNumeric() && secondValueStr.isNumeric()){
                    if(Decimal.valueOf(firstValueStr) < Decimal.valueOf(secondValueStr)){
                        return false;
                    }
                }  
                else{// newlly added 22-03
                    //System.debug('secoundFieldDataType '+secoundFieldDataType);
                    // System.debug('firestFieldDataType '+firestFieldDataType);
                    if(secoundFieldDataType == 'datetime' && firestFieldDataType == 'datetime')
                    {
                        if(Datetime.valueOf(firstValueStr) < Datetime.valueOf(secondValueStr)){
                            return false;
                        }
                    }
                    if(secoundFieldDataType == 'date' && firestFieldDataType == 'date')
                    {
                        if(Date.valueOf(firstValueStr) < Date.valueOf(secondValueStr)){
                            return false;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                System.debug(' Exception '+ex);
            }
        }
        if(eachFilterItem.operation == 'greaterThan'){
            try{
                //String firstValueStr = obj.get(eachFilterItem.field.replace(parentSObjectName+'.',''))+'';
                //String secondValueStr = obj.get(eachFilterItem.valueField.replace(parentSObjectName+'.',''))+'';
                String firstValueStr;
                String secondValueStr;
                if(eachFilterItem.field.contains('$User') || eachFilterItem.field.contains('$Profile')  || eachFilterItem.field.contains('$UserRole'))
                {
                    firstValueStr =  userData.get(eachFilterItem.field.replace('$User.','').replace('$Profile.','').replace('$UserRole.',''))+'';
                }
                else{
                    firstValueStr = obj.get(eachFilterItem.field.replace(parentSObjectName+'.',''))+''; 
                }
                if(eachFilterItem.valueField.contains('$User') || eachFilterItem.valueField.contains('$Profile')  || eachFilterItem.valueField.contains('$UserRole'))
                {
                    secondValueStr = userData.get(eachFilterItem.valueField.replace('$User.','').replace('$Profile.','').replace('$UserRole.',''))+'';
                }
                else{
                    secondValueStr = obj.get(eachFilterItem.valueField.replace(parentSObjectName+'.',''))+'';
                }
                //  System.debug('firstValueStr '+firstValueStr);
                // System.debug('secondValueStr '+secondValueStr);
                // System.debug(Datetime.valueOf(secondValueStr));
                if(firstValueStr.isNumeric() && secondValueStr.isNumeric()){
                    if(Decimal.valueOf(firstValueStr) > Decimal.valueOf(secondValueStr)){
                        return false;
                    }
                } 
                else{
                    //System.debug('secoundFieldDataType '+secoundFieldDataType);
                    // System.debug('firestFieldDataType '+firestFieldDataType);
                    if(secoundFieldDataType == 'datetime' && firestFieldDataType == 'datetime')
                    {
                        if(Datetime.valueOf(firstValueStr) > Datetime.valueOf(secondValueStr)){
                            return false;
                        }
                    }
                    if(secoundFieldDataType == 'date' && firestFieldDataType == 'date')
                    {
                        if(Date.valueOf(firstValueStr) > Date.valueOf(secondValueStr)){
                            return false;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                System.debug(' Exception '+ex);
            }
        }
        if(eachFilterItem.operation == 'lessOrEqual'){
            try{
                //String firstValueStr = obj.get(eachFilterItem.field.replace(parentSObjectName+'.',''))+'';
                //String secondValueStr = obj.get(eachFilterItem.valueField.replace(parentSObjectName+'.',''))+'';
                String firstValueStr;
                String secondValueStr;
                if(eachFilterItem.field.contains('$User') || eachFilterItem.field.contains('$Profile')  || eachFilterItem.field.contains('$UserRole'))
                {
                    firstValueStr =  userData.get(eachFilterItem.field.replace('$User.','').replace('$Profile.','').replace('$UserRole.',''))+'';
                }
                else{
                    firstValueStr = obj.get(eachFilterItem.field.replace(parentSObjectName+'.',''))+''; 
                }
                if(eachFilterItem.valueField.contains('$User') || eachFilterItem.valueField.contains('$Profile')  || eachFilterItem.valueField.contains('$UserRole'))
                {
                    secondValueStr = userData.get(eachFilterItem.valueField.replace('$User.','').replace('$Profile.','').replace('$UserRole.',''))+'';
                }
                else{
                    secondValueStr = obj.get(eachFilterItem.valueField.replace(parentSObjectName+'.',''))+'';
                }
                if(firstValueStr.isNumeric() && secondValueStr.isNumeric()){
                    if(Decimal.valueOf(firstValueStr) <= Decimal.valueOf(secondValueStr)){
                        return false;
                    }
                }  
                else{
                    //System.debug('secoundFieldDataType '+secoundFieldDataType);
                    // System.debug('firestFieldDataType '+firestFieldDataType);
                    if(secoundFieldDataType == 'datetime' && firestFieldDataType == 'datetime')
                    {
                        if(Datetime.valueOf(firstValueStr) <= Datetime.valueOf(secondValueStr)){
                            return false;
                        }
                    }
                    if(secoundFieldDataType == 'date' && firestFieldDataType == 'date')
                    {
                        if(Date.valueOf(firstValueStr) <= Date.valueOf(secondValueStr)){
                            return false;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                System.debug(' Exception '+ex);
            }
        }
        if(eachFilterItem.operation == 'greaterOrEqual'){
            try{
                //String firstValueStr = obj.get(eachFilterItem.field.replace(parentSObjectName+'.',''))+'';
                // String secondValueStr = obj.get(eachFilterItem.valueField.replace(parentSObjectName+'.',''))+'';
                String firstValueStr;
                String secondValueStr;
                if(eachFilterItem.field.contains('$User') || eachFilterItem.field.contains('$Profile')  || eachFilterItem.field.contains('$UserRole'))
                {
                    firstValueStr =  userData.get(eachFilterItem.field.replace('$User.','').replace('$Profile.','').replace('$UserRole.',''))+'';
                }
                else{
                    firstValueStr = obj.get(eachFilterItem.field.replace(parentSObjectName+'.',''))+''; 
                }
                if(eachFilterItem.valueField.contains('$User') || eachFilterItem.valueField.contains('$Profile')  || eachFilterItem.valueField.contains('$UserRole'))
                {
                    secondValueStr = userData.get(eachFilterItem.valueField.replace('$User.','').replace('$Profile.','').replace('$UserRole.',''))+'';
                }
                else{
                    secondValueStr = obj.get(eachFilterItem.valueField.replace(parentSObjectName+'.',''))+'';
                }
                if(firstValueStr.isNumeric() && secondValueStr.isNumeric()){
                    if(Decimal.valueOf(firstValueStr) >= Decimal.valueOf(secondValueStr)){
                        return false;
                    }
                } 
                else{
                    //System.debug('secoundFieldDataType '+secoundFieldDataType);
                    // System.debug('firestFieldDataType '+firestFieldDataType);
                    if(secoundFieldDataType == 'datetime' && firestFieldDataType == 'datetime')
                    {
                        if(Datetime.valueOf(firstValueStr) >= Datetime.valueOf(secondValueStr)){
                            return false;
                        }
                    }
                    if(secoundFieldDataType == 'date' && firestFieldDataType == 'date')
                    {
                        if(Date.valueOf(firstValueStr) >= Date.valueOf(secondValueStr)){
                            return false;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                System.debug(' Exception '+ex);
            }
        }
        
        if(eachFilterItem.operation == 'includes'){
            try{
                if((obj.get(eachFilterItem.field.replace(parentSObjectName+'.',''))+'').contains(''+obj.get(eachFilterItem.valueField.replace(parentSObjectName+'.','')))){
                    return false;
                }
            }
            catch(Exception ex)
            {
                System.debug(' Exception '+ex);
            }
        }
        if(eachFilterItem.operation == 'excludes'){
            try{
                if(!(obj.get(eachFilterItem.field.replace(parentSObjectName+'.',''))+'').contains(''+obj.get(eachFilterItem.valueField.replace(parentSObjectName+'.','')))){
                    return false;
                }
            }
            catch(Exception ex)
            {
                System.debug(' Exception '+ex);
            }
        }
        return true;
    }
    
}