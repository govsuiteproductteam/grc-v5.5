NETCENTS2IMO@NCIINC.COM
A Request For Quote (Quote Number: 185717) has been submitted for products on your contract (NCI INFORMATION SYSTEMS, INC.- NET OPS F & O: FA8732-15-D-0045).

Customer Information:
        Name: DEAN ARMSTRONG
        Address: 4600 RANDOLPH AVE SW. BLDG 1010 RM 155
                KAFB, NM 87117
        Email: dean.armstrong.1.ctr@us.af.mil
        Phone: 5058536326

RFQ Information:
        RFQID: 185717
        RFQ Name: 750 Cable Project
        Vendor Response Due Date: 9/4/2017
        Desired Delivery Date: 11/1/2017
        Quantity: 1
        Description: The 377th Communications Division is       requesting a quote for replacement of the cabling in building 750 area. See attached documentation for information regarding this project. Submit any questions to the POC listed on the attached documentation.

To respond to this request:

        1. Go to https://www.afway.af.mil
        2. Click on the User Profile link
        3. Log on using your email and password
        4. Click on RFQ Number:
        5. Submit response in accordance with instructions in the Vendor Manual.