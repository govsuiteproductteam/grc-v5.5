({
	helperDoInit : function(component,event,helper) {
		var getexistingTeamingPartners = component.get("c.getExistingPartners");
        var recordId = component.get("v.recordId");
        
        getexistingTeamingPartners.setParams({
            "oppId":recordId
        });
        getexistingTeamingPartners.setCallback(this,function(resultData){
            
            var status = resultData.getState();
            
            if(status=="SUCCESS"){
                var motherWrapperData = resultData.getReturnValue();
                console.log(JSON.stringify(motherWrapperData));
                if(! $A.util.isUndefined(motherWrapperData) && ! $A.util.isEmpty(motherWrapperData)){
                    component.set("v.teamList",motherWrapperData.teamingPartnerList);
                    component.set("v.statusPicklistvalues",motherWrapperData.pickListVals);
                component.set("v.isDataLoaded",true);
                }
                
            }
        });
        $A.enqueueAction(getexistingTeamingPartners);
	},
    
    helperFetchModalData : function(component,event,helper){
        var teambtn = component.find("headbtn1");
        $A.util.addClass(teambtn,"cust_disable");
        component.find("chk_all").set("v.value",false);
        component.find("chk_allPart").set("v.value",false);
        component.set("v.searchTerm",'');
        var getTeamingPartner = component.get("c.getTeamingPartnerList");
        var recordId = component.get("v.recordId");
        getTeamingPartner.setParams({
            "tOrderId":recordId
        });
        getTeamingPartner.setCallback(this,function(resultData){
            
            var status = resultData.getState();
            
            if(status=="SUCCESS"){
                component.set("v.TeamingPartnetListWrp",resultData.getReturnValue());
                component.set("v.isShowAllPartners",false);
                var modal = component.find("teamingpartnerdiv");
                $A.util.removeClass(modal,'hidethis');
                $A.util.removeClass(teambtn,"cust_disable");
            }else{
              //  alert('SOMETHING IS NOT RIGHT...!!! Please refresh the page');
                $A.util.removeClass(modal,'hidethis');
                $A.util.removeClass(teambtn,"cust_disable");
            }
        });
        $A.enqueueAction(getTeamingPartner);
    },
    helperAddTeamPart : function(component,event,helper){
         var add_teaming_part = component.find("add_teaming_part");
        $A.util.addClass(add_teaming_part,'cust_disable');
        var wrapperData = component.get("v.TeamingPartnetListWrp");
        var oppIdList =[];
        if(wrapperData != null && typeof wrapperData !='undefined'){
            for(var i=0;i<wrapperData.length;i++){
                if(wrapperData[i].status){
                    oppIdList.push(wrapperData[i].vehiclePartner.Id);
                }
            }
            if(oppIdList != null && oppIdList.length>0){
                var addTeamingPartners = component.get("c.addPartner");
                var recordId=component.get("v.recordId");
                addTeamingPartners.setParams({
                    "vehiclPartnerIds":oppIdList,
                    "oppId":recordId
                });
                addTeamingPartners.setCallback(this,function(resultData){
                    var status = resultData.getState();
                    if(status == "SUCCESS"){
                     //   alert("SUCCESS");
                        component.set("v.teamList",resultData.getReturnValue());
                        var modal = component.find("teamingpartnerdiv");
                     $A.util.addClass(modal,'hidethis');
                         $A.util.removeClass(add_teaming_part,'cust_disable');
                        var appEvent = $A.get("e.c:RefreshTaskOrderContacts_EVT");
                        appEvent.fire();
                    }else{
                         $A.util.removeClass(add_teaming_part,'cust_disable');
                        var appEvent = $A.get("e.c:RefreshTaskOrderContacts_EVT");
                        appEvent.fire();
                    }
                });
                $A.enqueueAction(addTeamingPartners);
            }else{
                //alert("Please select atleast one Teaming Partner.")
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": "Please select at least one Teaming Partner",
                    "type":"error"
                });
                toastEvent.fire();
                 $A.util.removeClass(add_teaming_part,'cust_disable');
            }
        }else{
             $A.util.removeClass(add_teaming_part,'cust_disable');
        }
    },
    helperCloseModal : function(component,event,helper){
        var modal = component.find("teamingpartnerdiv");
        $A.util.addClass(modal,'hidethis'); 
    },
    openEmailModalHlpr: function(component,event,helper){
        var modal = component.find("sendpartneremail");
        $A.util.removeClass(modal,'hidethis');
        //var emailPartDiv = component.find("emailmodaldatadiv").get("v.body");
        $A.createComponent("c:SendPartnerEmailCmp",{"recordId":component.get("v.recordId"),
                                                    "cancelBtnAction":component.getReference("c.closeEmailModal")
                                                   },function(emailData, status, errorMessage){
            if(component.isValid() && status == "SUCCESS"){
                component.find("emailmodaldatadiv").set("v.body",emailData);
            }else{
                console.log("Error in Email Modal");
                $A.util.addClass(modal,'hidethis');
            }
        });
    },
    closeEmailModalHlpr: function(component,event,helper){
        var modal = component.find("sendpartneremail");
        $A.util.addClass(modal,'hidethis');
    },
    deleteThisTeamHlpr: function(component,event,helper){
        var addSpinner=component.find("addteamspinnr");
        $A.util.removeClass(addSpinner,"slds-hide");
         var teamToDelete = event.currentTarget.dataset.record;
        var recName = event.currentTarget.dataset.record1;
        if(recName == null)
            recName = 'Record';
        var oppId = component.get("v.recordId");
        var deleteThisTeam = component.get("c.deleteTeam");
        deleteThisTeam.setParams({
            "teamId":teamToDelete,
            "oppId":oppId
        });
        deleteThisTeam.setCallback(this,function(resultData){
            var status = resultData.getState();
            if(status=="SUCCESS"){
               // alert( "Teaming Partner "+recName+" Was Deleted");
                component.set("v.teamList",resultData.getReturnValue());
                document.getElementById("savestatusbtn").classList.add("slds-hide");
                document.getElementById("headbtn1").classList.remove("slds-hide");
                document.getElementById("headbtn2").classList.remove("slds-hide");
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "Teaming Partner "+recName+" Was Deleted",
                    "type":"success"
                });
                toastEvent.fire();
                $A.util.addClass(addSpinner,"slds-hide");
            }else{
                //alert("Error..!! Please refresh the page.");
                document.getElementById("savestatusbtn").classList.add("slds-hide");
                document.getElementById("headbtn1").classList.remove("slds-hide");
                document.getElementById("headbtn2").classList.remove("slds-hide");
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": "Error occurred while deleting the record.Please refresh the page",
                    "type":"error"
                });
                toastEvent.fire();
                $A.util.addClass(addSpinner,"slds-hide");
            }
        });
        $A.enqueueAction(deleteThisTeam);
    },
    navigateToTeamHlpr: function(component,event,helper){
       var teamId = event.currentTarget.dataset.record;
        var urlEvent = $A.get("e.force:navigateToURL");
    urlEvent.setParams({
      "url": "/"+teamId
    });
    urlEvent.fire();
    },
    editThisTeamHlpr: function(component,event,helper){
         var teamToDelete = event.currentTarget.dataset.record;
        var editThisTeam = $A.get("e.force:editRecord");
        editThisTeam.setParams({
             "recordId": teamToDelete
        });
        editThisTeam.fire();
    },
    editStatusHlpr: function(component,event,helper){
        var editIndex = event.currentTarget.dataset.index;
        
        document.getElementById("editstatusbtn"+editIndex).classList.add("slds-hide");
        document.getElementById("undostatusbtn"+editIndex).classList.remove("slds-hide");
        document.getElementById("undostatusbtn"+editIndex).classList.add("undoexist");
        document.getElementById("savestatusbtn").classList.remove("slds-hide");
       // document.getElementById("headbtncancl").classList.remove("slds-hide");
        document.getElementById("headbtn1").classList.add("slds-hide");
        document.getElementById("headbtn2").classList.add("slds-hide");
        document.getElementById("statusview"+editIndex).classList.add("slds-hide");
        document.getElementById("statusedit"+editIndex).classList.remove("slds-hide");
        
        
    },
    undoStatusTeamHlpr: function(component,event,helper){
        var undoIndex = event.currentTarget.dataset.index;
        var oldval = event.currentTarget.dataset.oldval;
        var recId = event.currentTarget.dataset.recid;
        var picklistcls =event.currentTarget.dataset.picklistcls;
        
        //document.getElementsByClassName(""+picklistcls)[0].setAttribute("value", "oldval");
       // alert(picklistcls);
       // document.getElementsByClassName(""+picklistcls).setAttribute("value", oldval);
        //alert(document.getElementsByClassName(""+picklistcls)[0].getAttribute("class"));
        
        //var tempVar=document.getElementsByClassName(""+picklistcls);
        //alert(document.getElementsByClassName(""+picklistcls)[0].getAttribute("class"));
        var statusList =  component.get("v.selectedStatus");
        statusList[undoIndex]=recId+"#####"+oldval;
        component.set("v.selectedStatus",statusList);
        document.getElementById("editstatusbtn"+undoIndex).classList.remove("slds-hide");
        document.getElementById("undostatusbtn"+undoIndex).classList.add("slds-hide");
        document.getElementById("undostatusbtn"+undoIndex).classList.remove("undoexist");
        
        document.getElementById("statusview"+undoIndex).classList.remove("slds-hide");
        document.getElementById("statusedit"+undoIndex).classList.add("slds-hide");
        
        var unDoExistList = document.getElementsByClassName("undoexist");
        if(unDoExistList != null && unDoExistList.length>0){
            console.log("undo Exist");
        }else{
             document.getElementById("savestatusbtn").classList.add("slds-hide");
                document.getElementById("headbtn1").classList.remove("slds-hide");
                document.getElementById("headbtn2").classList.remove("slds-hide");
        }
    },
    updateStatusHlpr: function(component,event,helper){
      document.getElementById("savestatusbtn").classList.add("cust_disable");
       var statusList =  component.get("v.selectedStatus");
        
       //var selectedIndex = event.currentTarget.dataset.index;
      // var recordId = event.currentTarget.dataset.record;
       // var status = statusList[selectedIndex];
        var oppId = component.get("v.recordId");
        var updateStatus = component.get("c.dummyUpdateStatus");
        updateStatus.setParams({
            "statusVal" : statusList,
            "oppId":oppId
        });
        
        updateStatus.setCallback(this,function(resultData){
            var status2 = resultData.getState();
           
            if(status2=="SUCCESS"){
               // alert("SUCCESS");
                component.set("v.teamList",resultData.getReturnValue());
                
               // document.getElementById("headbtncancl").classList.add("slds-hide");
                
                document.getElementById("headbtn1").classList.remove("slds-hide");
                document.getElementById("headbtn2").classList.remove("slds-hide");
                document.getElementById("savestatusbtn").classList.remove("cust_disable");
                //$A.util.removeClass(savebtn,"cust_disable");
                document.getElementById("savestatusbtn").classList.add("slds-hide");
            }else{
                 $A.util.removeClass(savebtn,"cust_disable");
            }
        });
        $A.enqueueAction(updateStatus);
        
    },
    selectStatusValHlpr: function(component,event,helper){
        var target = event.getSource();  
            var selectedVal = target.get("v.value");
        var indexSelected = parseInt(target.get("v.class").split("#####")[0]);
        var recId = target.get("v.class").split("#####")[1];
        var selectedStatus = component.get("v.selectedStatus");
        selectedStatus[indexSelected] = recId+"#####"+selectedVal;
        component.set("v.selectedStatus",selectedStatus);
              
    },
    showAllOrgHlpr: function(component,event,helper){
        var showAll = component.get("c.showAllPartners");
        component.find("chk_allPart").set("v.value",false);
        showAll.setCallback(this,function(resultData){
            var state= resultData.getState();
            if(state=="SUCCESS"){
                component.set("v.allPartnersListWrp",resultData.getReturnValue());
                component.set("v.tempAllPartnersListWrp",resultData.getReturnValue());
                
                component.set("v.isShowAllPartners",true);
            }
        });
        $A.enqueueAction(showAll);
    },
    showfilteredOrgHlpr: function(component,event,helper){
         component.set("v.isShowAllPartners",false);
    },
    addAllTeamingPartnerHlpr: function(component,event,helper){
        
            },
    teamingPartnerChkClickHlpr : function(component,event,helper){
        var isChkAll = true;
        var TeamingPartnetListWrp = component.get("v.TeamingPartnetListWrp");
        if(! $A.util.isUndefined(TeamingPartnetListWrp)  && ! $A.util.isEmpty(TeamingPartnetListWrp)){
            for(var i=0;i<TeamingPartnetListWrp.length;i++){
                if(!TeamingPartnetListWrp[i].status)
                    isChkAll = false;
            }
            component.find("chk_all").set("v.value",isChkAll);
        }
            },
    AllTeamingPartnerChkClickHlpr: function(component,event,helper){
        var isChkAll = true;
        var allPartnersListWrp = component.get("v.allPartnersListWrp");
        if(! $A.util.isUndefined(allPartnersListWrp)  && ! $A.util.isEmpty(allPartnersListWrp)){
            for(var i=0;i<allPartnersListWrp.length;i++){
                if(!allPartnersListWrp[i].isSelected)
                    isChkAll = false;
            }
            component.find("chk_allPart").set("v.value",isChkAll);
        }
    }
})