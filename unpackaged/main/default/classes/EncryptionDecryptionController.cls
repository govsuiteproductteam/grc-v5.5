public with sharing class EncryptionDecryptionController {
    public String fedCLMLicence{get;set;}
    public String fedTOMLicence{get;set;}
    
    public EncryptionDecryptionController(){
        if(Schema.sObjectType.Folder.isAccessible() && Schema.sObjectType.Document.isAccessible()){
            Folder documentFolder = [Select Id from Folder where Name='FedTom'];
            if(documentFolder!=null){  
                
                
                System.debug('Document Folder Found');
                List<Document> fedTomDocumentList = [select id,Body from Document where FolderId = : documentFolder.Id And DEveloperName=: 'FedTomUserDoc' Limit 1];
                List<Document> fedCLMDocumentList = [select id,Body from Document where FolderId = : documentFolder.Id And DEveloperName=: 'FedCLMUserDoc' Limit 1];            
                if(Test.isRunningTest()){                
                    fedTomDocumentList = [select id,Body from Document where FolderId = : documentFolder.Id And DEveloperName=: 'FedTomUserDoc1' Limit 1];
                    fedCLMDocumentList = [select id,Body from Document where FolderId = : documentFolder.Id And DEveloperName=: 'FedCLMUserDoc1' Limit 1];            
                }
                if(fedTomDocumentList != null && !fedTomDocumentList.isEmpty() && fedCLMDocumentList != null && !fedCLMDocumentList.isEmpty()){  
                    System.debug('Document Found');
                    try{                    
                        fedCLMLicence = EncryptionDecryptionManager.decryptValue(fedCLMDocumentList[0].Body);                  
                        fedTOMLicence = EncryptionDecryptionManager.decryptValue(fedTomDocumentList[0].Body);
                    }catch(Exception e){
                        
                    }
                }         
            }
        }
    }
    public void createCryptoKeyDoc(){
        if(Schema.sObjectType.Folder.isAccessible() && Schema.sObjectType.Document.isAccessible()){
            Folder documentFolder = [Select Id from Folder where Name='FedTom'];
            if(documentFolder!=null){  
                System.debug('Document Folder Found');
                List<Document> fedTomDocumentList = [select id,Body from Document where FolderId = : documentFolder.Id And DEveloperName=: 'FedTomUserDoc' Limit 1];
                List<Document> fedCLMDocumentList = [select id,Body from Document where FolderId = : documentFolder.Id And DEveloperName=: 'FedCLMUserDoc' Limit 1];            
                if(Test.isRunningTest()){                
                    fedTomDocumentList = [select id,Body from Document where FolderId = : documentFolder.Id And DEveloperName=: 'FedTomUserDoc1' Limit 1];
                    fedCLMDocumentList = [select id,Body from Document where FolderId = : documentFolder.Id And DEveloperName=: 'FedCLMUserDoc1' Limit 1];            
                }
                if(fedTomDocumentList != null && !fedTomDocumentList.isEmpty() && fedCLMDocumentList != null && !fedCLMDocumentList.isEmpty()){  
                    System.debug('Document Found');
                    try{
                        Integer.valueOf(fedTOMLicence);
                        Integer.valueOf(fedCLMLicence);
                        Blob fedTOMEncryptedData = EncryptionDecryptionManager.encryptValue(fedTOMLicence);
                        Blob fedCLMEncryptedData = EncryptionDecryptionManager.encryptValue(fedCLMLicence);                    
                        if(fedCLMEncryptedData != null && fedTOMEncryptedData != null){ 
                            System.debug('correct');
                            Document fedTomDocument = fedTomDocumentList[0]; 
                            Document fedCLMDocument = fedCLMDocumentList[0]; 
                            fedTomDocument.Body = fedTOMEncryptedData;
                            fedCLMDocument.Body = fedCLMEncryptedData;
                            //update fedTomDocument;
                            DMLManager.updateAsUser(fedTomDocument);
                            //update fedCLMDocument;
                            DMLManager.updateAsUser(fedCLMDocument );
                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Successfully Updated'));
                        }
                    }catch(Exception e){
                        System.debug('exception');
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Enter valid Number'));
                    }
                }
            }
            
        }
    }
}

/*
Folder documentFolder = [Select Id from Folder where Name='FedTom'];
System.debug('Document Folder Found');
List<Document> documentList = [select id,Body from Document where FolderId = : documentFolder.Id And DEveloperName=: 'FedTomUserDoc' Limit 1];
Document document = documentList[0];           
Blob body = document.Body;
String data = EncryptionDecryptionManager.decryptValue(body);
system.debug('Data = '+data);
*/