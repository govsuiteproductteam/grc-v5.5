@isTest
public with sharing class Test_CIO_CSHandler {
    public Static TestMethod void CIO_CS_EmailHandler (){
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Contact con = TestDataGenerator.createContact('TestContact', acc );
        insert con;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('T-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'cio_cs@yekxo0mxsu3sklxvrrtt57pxm3fu0advp58dwnjhp6uu2ma9w.37-pvateac.na31.apex.salesforce.com';        
        insert conVehi;    
        
        Messaging.InboundEmail email  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        email.plainTextBody = 'RFQ CS-34160 titled Palo Alto Engagement from Executive Branch, Department of Health and Human Services, National Institutes of Health has been released for competition. '+
            
            'Proposals are due by 02/12/2016 12:00 PM Eastern Time.'+
            
            'Please visit e-GOS at https://cio.egos.nih.gov for more information. '+
            '______________________________________________________ '+
            
            'nitaac.nih.gov'+
            'Phone: 1.888.773.6542'+
            'NITAACsupport@nih.gov'+
            
            '6011 Executive Boulevard, Suite 501'+
            'Rockville, Maryland 20852 ';
        email.fromAddress ='test@test.com';
        email.subject = 'RE: RFQ CS-34160 Released for Competition Cancelled';
        email.toaddresses = new List<String>();
        email.toaddresses.add('cio_cs@yekxo0mxsu3sklxvrrtt57pxm3fu0advp58dwnjhp6uu2ma9w.37-pvateac.na31.apex.salesforce.com');             
        
        Messaging.InboundEmail email1  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env1 = new Messaging.InboundEnvelope();
        email1.plainTextBody = 'RFQ CS-34160 titled Palo Alto Engagement from Executive Branch, Department of Health and Human Services, National Institutes of Health has been released for competition. '+
            
            'Proposals are due by 13/13/2016 12:00 PM Eastern Time.\n'+
            
            'Please visit e-GOS at https://cio.egos.nih.gov for more information. '+
            '______________________________________________________ '+
            
            'nitaac.nih.gov'+
            'Phone: 1.888.773.6542'+
            'NITAACsupport@nih.gov'+
            
            '6011 Executive Boulevard, Suite 501'+
            'Rockville, Maryland 20852 ';
        email1.fromAddress ='test@test.com';
        email1.subject = 'RFQ CS-34160 Released for Competition cancelled';
        email1.toaddresses = new List<String>();
        email1.toaddresses.add('cio_cs@yekxo0mxsu3sklxvrrtt57pxm3fu0advp58dwnjhp6uu2ma9w.37-pvateac.na31.apex.salesforce.com');   
        
       
        Messaging.InboundEmail email2  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env2 = new Messaging.InboundEnvelope();
        email2.plainTextBody = 'An amendment for RFQ CS-45103 titled San Diego Engagement hgsadgshf jsdfjhsdhjsdf shfsdhfhsdfhjsd sdfsdajhfsdahjfsdahjf fhsdhjsdfhjksdhjkfsd fsdhfhdjshjsdfhjf hfshdfhjkdsjhdfhf fhjsdfhjsdfjhfdsjhfsd hjfshdfshfsdhjfsdhjfsd hjsfdjhsfdjhsfdjhfsdhj fsdjhkfsdjhfsdhjsdfjhfsd hfsdjhsdfhjsfdjhkfds sghsdfghsdfghsdf fsdjdsfjsdafjgksdfgjsfd fdskjfdsjhgfdshjfdshjfdshjfsd sdfjdfshjfdshjfdshjfdshjfds fdsjhfdshjfdshjfdsjhfdsh sfdkjhkdfshjsfdhjfsdhjsdf fdsjhkfsdjhfsdhjsdfhijl fdsldsfhdsfjkhdfshjkdfsjhkfds hfsdjhsdfhjsdfhjsfdjhfdshjfds jhfsdhjfsdjhsdfhjshjsdfhjfsdhjdfs from Executive Branch, Department of Health and Human Services, National Institutes of Health has been released for competition. Please visit e-GOS at https://cio.egos.nih.gov to view the Amendment. \n'+

                                'Amendment Description: \n'+
                                'The due date for RFI submission has changed:From: Tuesday April 12, 2016 at 12:00pm To: Wednesday May 4, 2016 at 3:00pm \n'+

                                'Amendment number: \n'+
                                '1\n'+ 
                                '______________________________________________________ \n'+
                                'nitaac.nih.gov \n'+
                                'Phone: 1.888.773.6542 \n'+
                                'NITAACsupport@nih.gov \n'+
                                '6011 Executive Boulevard, Suite 501 \n'+
                                'Rockville, Maryland 20852 ';
        email2.fromAddress ='test@test.com';
        email2.subject = ' Amendment for RFQ CS-45103 Released.';
        email2.toaddresses = new List<String>();
        email2.toaddresses.add('cio_cs@yekxo0mxsu3sklxvrrtt57pxm3fu0advp58dwnjhp6uu2ma9w.37-pvateac.na31.apex.salesforce.com');
        
        CIO_CSHandler cioEmailHandler = new CIO_CSHandler();
        
        Test.startTest();
        Messaging.InboundEmailResult result = cioEmailHandler.handleInboundEmail(email, env);
        Messaging.InboundEmailResult result1 = cioEmailHandler.handleInboundEmail(email1, env1);
        Messaging.InboundEmailResult result2 = cioEmailHandler.handleInboundEmail(email2, env2);
        Test.stopTest();
        
        List<Task_Order__c> tOrderList = [SELECT Id FROM Task_Order__c];
        System.assertEquals(tOrderList.size(), 2);
    }
}