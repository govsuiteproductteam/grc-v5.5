@isTest
public class CandidateNewCtrlTest{
    public static TestMethod void testMethod1(){
            Account acc = TestDataGenerator.createAccount('TaskOrderCapture');
            insert acc;
            
            List<Contact> conList = new List<Contact>();
            Contact con = TestDataGenerator.createContact('test1',acc);
            conList.add(con);
            Contact con1 = TestDataGenerator.createContact('test2',acc);
            conList.add(con1);
            insert conList;
            
            Contract_Vehicle__c contractvehicle= TestDataGenerator.createContractVehicle('TMconVehicle',acc);
            insert contractvehicle;
        
            Project__c proj = TestDataGenerator.createProject(contractvehicle, 'In progress');
            insert proj;
            
            CLM_Labor_Category__c clmLaborCatgory  = TestDataGenerator.createClmLabCategory(contractvehicle ,'Doctorate', proj);
            insert clmLaborCatgory;
            
            LC_Candidate_Assignment__c lcCandidateAssign = TestDataGenerator.createLcCandidateAssign(proj , clmLaborCatgory );
            lcCandidateAssign.Email__c = 'avanti@csra.com';
            insert lcCandidateAssign ;
            
            Candidate__c candidate = TestDataGenerator.createCandidate('New', 'avanti', 'vaidya', 'avanti@csra.com');
            insert candidate ;
            
            Attachment attach1 = new Attachment();      
            attach1.Name = 'Unit Test Attachment';
            Blob bodyBlob1 =Blob.valueOf('Unit Test Attachment Body');
            attach1.body= bodyBlob1;
           // attach1.parentId = candidate.id;
            attach1.IsPrivate = true;
          //  insert attach1;
                  
            Test.startTest();
             ApexPages.currentPage().getParameters().put('lcaId',lcCandidateAssign.id);
             ApexPages.StandardController controller = new ApexPages.StandardController(candidate );
             CandidateNewCtrl candidateContrl = new CandidateNewCtrl(controller);
                
             candidateContrl.resume = attach1;
             system.debug('  candidateContrl.resume='+   candidateContrl.resume);
           
             Attachment attach = candidateContrl.resume;        
             //candidateContrl.save();      
             pageReference  pgRe = candidateContrl.save();
             system.assertNotEquals(pgRe , null);
           
            Test.stopTest();
    }
    
        public static TestMethod void testMethod2(){
            Account acc = TestDataGenerator.createAccount('TaskOrderCapture');
            insert acc;
            
            List<Contact> conList = new List<Contact>();
            Contact con = TestDataGenerator.createContact('test1',acc);
            conList.add(con);
            Contact con1 = TestDataGenerator.createContact('test2',acc);
            conList.add(con1);
            insert conList;
            
            Contract_Vehicle__c contractvehicle= TestDataGenerator.createContractVehicle('TMconVehicle',acc);
            insert contractvehicle;
        
            Project__c proj = TestDataGenerator.createProject(contractvehicle, 'In progress');
            insert proj;
            
            CLM_Labor_Category__c clmLaborCatgory  = TestDataGenerator.createClmLabCategory(contractvehicle ,'Doctorate', proj);
            insert clmLaborCatgory;
            
            LC_Candidate_Assignment__c lcCandidateAssign = TestDataGenerator.createLcCandidateAssign(proj , clmLaborCatgory );
            insert lcCandidateAssign ;
            
            Candidate__c candidate = TestDataGenerator.createCandidate('New', 'avanti', 'vaidya', 'avanti@csra.com');
            insert candidate ;
                      
            Test.startTest();
             ApexPages.currentPage().getParameters().put('lcaId',lcCandidateAssign.id);
             ApexPages.currentPage().getParameters().put('success','1');
             
             ApexPages.StandardController controller = new ApexPages.StandardController(candidate );
             CandidateNewCtrl candidateContrl = new CandidateNewCtrl(controller);       
             Attachment att = candidateContrl.resume;
             system.assertEquals(att , new attachment());
          
            Test.stopTest();
    }
}