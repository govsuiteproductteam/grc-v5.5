@isTest
public class SavantageJETSEmailHandlerTest{
    public static testMethod void testCase1(){
        Test.startTest();
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Contact con = TestDataGenerator.createContact('TestCon', acc );
        insert con;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('T-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'savantagejets@2o7bz18ud7hzz04euldtetezhm89pra67pdi5tilbyq93oziqw.37-pvateac.na31.apex.salesforce.com';
        
        insert conVehi;
        Test.stopTest();
        
        SavantageJETSEmailHandler handler = new SavantageJETSEmailHandler();
        //for new Task Order Creation  
        Messaging.InboundEmail email  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        List<StaticResource> srList = [SELECT Id,Body FROM StaticResource WHERE Name='SavantageJETSTest'];
        
        email.plainTextBody = srList.get(0).Body.toString();
        email.fromAddress ='test@test.com';
        email.fromName = 'ABC XYZ';
        email.subject = 'Fwd:Draft PWS - JETS SP4709-18-Q-0010 DMLSS-W Application Dev and Sustainment';
        email.toaddresses = new List<String>();
        email.toaddresses.add('savantagejets@2o7bz18ud7hzz04euldtetezhm89pra67pdi5tilbyq93oziqw.37-pvateac.na31.apex.salesforce.com');
        
        //For Updating existing task order with existing Contact
        
        
        List<StaticResource> srList1 = [SELECT Id,Body FROM StaticResource WHERE Name='SavantageJETSTest1'];
        Messaging.InboundEmail email1  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env1 = new Messaging.InboundEnvelope();
        email1.plainTextBody = srList1.get(0).Body.toString();
        email1.fromAddress ='test@test.com';
        email1.fromName = 'ABC XYZ';
        email1.subject = 'Re:Draft PWS - JETS SP4709-18-Q-0010 DMLSS-W Application Dev and Sustainment';
        email1.toaddresses = new List<String>();
        email1.toaddresses.add('savantagejets@2o7bz18ud7hzz04euldtetezhm89pra67pdi5tilbyq93oziqw.37-pvateac.na31.apex.salesforce.com');
        
        //For Updating existing task order with new Contact
        List<StaticResource> srList2 = [SELECT Id,Body FROM StaticResource WHERE Name='SavantageJETSTest1'];
        Messaging.InboundEmail email2  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env2 = new Messaging.InboundEnvelope();
        email2.plainTextBody = srList2.get(0).Body.toString();
        email2.fromAddress ='test@test.com';
        email2.fromName = 'ABC XYZ';
        email2.subject = 'Draft PWS - JETS SP4709-18-Q-0010 DMLSS-W Application Dev and Sustainment';
        email2.toaddresses = new List<String>();
        email2.toaddresses.add('savantagejets@2o7bz18ud7hzz04euldtetezhm89pra67pdi5tilbyq93oziqw.37-pvateac.na31.apex.salesforce.com');
        Messaging.InboundEmailResult result = handler.handleInboundEmail(email, env);
        List<Task_Order__c> taskOrderList = [SELECT Id FROM Task_Order__c];
        System.assertEquals(taskOrderList.size(), 1);
        //taskOrderList[0].Customer_Agency__c = acc.id;
        //update taskOrderList;
        Messaging.InboundEmailResult result1 = handler.handleInboundEmail(email1, env1);
        List<Contract_Mods__c> contractModList = [SELECT Id FROM Contract_Mods__c];
        System.assertEquals(contractModList.size(), 1);
        Messaging.InboundEmailResult result2 = handler.handleInboundEmail(email2, env2);
    }
}