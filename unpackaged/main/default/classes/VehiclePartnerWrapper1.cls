public with sharing class VehiclePartnerWrapper1 {
  @AuraEnabled
    public Vehicle_Partner__c vehiclePartner{get;set;}
     @AuraEnabled  
    public Boolean status {get;set;}
        
        public VehiclePartnerWrapper1(Vehicle_Partner__c vehiclePartner){
            System.debug('vehiclePartner1 '+vehiclePartner);
            this.vehiclePartner = vehiclePartner;
            status = false;
        }
}