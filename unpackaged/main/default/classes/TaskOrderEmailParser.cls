global with sharing class TaskOrderEmailParser{    
    private String originalStr;
    public boolean isOldTakOrder; 
    public Task_Order__c oldTaskOrder;
    public Contact contractingOfficerCon;
    public Contact contractingSpecialistCon;
    
    public TaskOrderEmailParser(){
        isOldTakOrder = false;
    }
    // for seaport we have separate email parser
    public Task_Order__c seaPortTaskOrderParser(String str, String toAddress){
        return Null;
    }
    
    public Task_Order__c tFourTaskOrderParser(String str, String toAddress){
        System.debug(''+toAddress); 
        if(Schema.sObjectType.Contract_Vehicle__c.isAccessible() && Schema.sObjectType.Task_Order__c.isAccessible()){
            originalStr = str.replace(' ',' ');//Do not delete ' ',' '
            String rtepNumber = getDatatFour('RTEP Number:', false);
            if(rtepNumber.length() > 80){
                rtepNumber = rtepNumber.substring(0,80);
            }                
            
            if(rtepNumber != null && rtepNumber != ''){
                
                List<Contract_Vehicle__c> conVehicalList = [SELECT id FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];
                Id contractVehicleId;
                if(!conVehicalList.isEmpty())
                    contractVehicleId = conVehicalList[0].Id;
                
                if(contractVehicleId != null){                      
                    Task_Order__c  taskOrder = EmailHandlerHelper.getExistTaskOrder(rtepNumber, contractVehicleId);                                     
                    
                    if(taskOrder == null){
                        taskOrder = new Task_Order__c();
                        //if new task order is creating from email parser it must have 'Task Order' Record type, if updating task order it will go as it is with existing record type
                        /*List<TM_RecordTypeSetting__c> recordTypeSetting = [Select Id,RecordType_Id__c From TM_RecordTypeSetting__c where Is_TO_Record_Type__c = true];
if(!recordTypeSetting.isEmpty() && recordTypeSetting[0].RecordType_Id__c != null){
taskOrder.RecordTypeId = recordTypeSetting[0].RecordType_Id__c;
}*/
                    }else{
                        isOldTakOrder = true;
                        oldTaskOrder = taskOrder.clone(false,true,false,false);
                    }
                    
                    taskOrder.Name = rtepNumber;
                    taskOrder.Contract_Vehicle__c = contractVehicleId;
                    
                    try{
                        String rtepName = getDatatFour('RTEP Name:', false);
                        if(rtepName != null){
                            if(rtepName.length() > 80){
                                taskOrder.Task_Order_Title__c = rtepName.substring(0,80);
                            }
                            else{
                                taskOrder.Task_Order_Title__c = rtepName;
                            }
                        }
                        
                        String tacStr = getDatatFour('TAC #:', false);
                        if(tacStr != null){
                            if(tacStr.length() > 255){
                                taskOrder.Request_No__c = tacStr.substring(0,255);
                                System.debug('Request_No__c '+taskOrder.Request_No__c);
                            }
                            else{
                                taskOrder.Request_No__c = tacStr;
                                System.debug('Request_No__c '+taskOrder.Request_No__c);
                            }
                        }
                        
                        String atomsStr = getDatatFour('ATOMS Status:', false);
                        if(atomsStr != null){
                            if(atomsStr.length() > 80){
                                taskOrder.TO_Request_Status__c = atomsStr.substring(0,80);
                                System.debug('taskOrder.TO_Request_Status__c '+taskOrder.TO_Request_Status__c);
                            }
                            else{
                                taskOrder.TO_Request_Status__c = atomsStr ;
                                System.debug('taskOrder.TO_Request_Status__c '+taskOrder.TO_Request_Status__c);
                            }
                        }
                        
                        string stage =  getDatatFour('RTEP Phase:', false);
                        if(stage !=null && stage != ''){                      
                            taskOrder.Stage__c = stage;
                        }
                        
                        String scopeStr = getDatatFour('Scope/Description:', false);
                        if(scopeStr != null){
                            if(scopeStr.length() > 32768 ){
                                taskOrder.Description__c = scopeStr.substring(0,32768);
                                System.debug('taskOrder.Description__c '+taskOrder.Description__c);
                            }
                            else{
                                taskOrder.Description__c = scopeStr;
                                System.debug('taskOrder.Description__c '+taskOrder.Description__c);
                            }
                        }
                        
                        String incumStr = getDatatFour('Incumbent:', false);
                        if(incumStr != null){
                            if(incumStr.length() > 255){
                                taskOrder.Incumbent__c = incumStr.substring(0,255);
                                System.debug('taskOrder.Incumbent__c '+taskOrder.Incumbent__c);
                            }
                            else{
                                taskOrder.Incumbent__c = incumStr;
                                System.debug('taskOrder.Incumbent__c '+taskOrder.Incumbent__c);
                            }
                        }
                        System.debug('******************** = '+getDatatFour('Customer Organization:', false));
                        DateTimeHelper dtHelper = new DateTimeHelper();
                        taskOrder.Task_Order_Contracting_Organization__c = getAccountId(getDatatFour('Customer Organization:', false).normalizeSpace().trim());                    
                        //taskOrder.Market_Research_Due_Date__c = parseDateNTime(getDatatFour('Market Research Due Date:', false));
                        taskOrder.Market_Research_Due_Date__c = dtHelper.getDateTimeSpecialCase(getDatatFour('Market Research Due Date:', false));
                        
                        ///taskOrder.Questions_Due_Date__c = parseDateTime(getDatatFour('Questions Due Date:', false));
                        try{
                            taskOrder.Questions_Due_Date__c = dtHelper.getDateTimeSpecialCase(getDatatFour('Questions Due Date:', false).normalizeSpace().trim()).date();
                        }catch(Exception e){
                            
                        }
                        
                        System.debug('************************************************1');
                        ///taskOrder.Due_Date__c = parseDateTime(getDatatFour('RTEP Due Date:', false));
                        try{
                            DateTime proposalDueDateTime =  dtHelper.getDateTimeSpecialCase(getDatatFour('RTEP Due Date:', false).normalizeSpace().trim());
                            taskOrder.Proposal_Due_DateTime__c = proposalDueDateTime;
                            if(proposalDueDateTime!=null)
                                taskOrder.Due_Date__c = proposalDueDateTime.date();
                        }catch(Exception e){
                            System.debug('Exception = '+e.getMessage()+':'+e.getLineNumber());
                        }
                        
                        System.debug('************************************************2');
                        taskOrder.Submitted_Market_Research__c = getStatus(getDatatFour('Submitted Market Research?:', false));
                        //taskOrder.Task_Order_Ceiling_Value_K__c = toDecimal(getDatatFour('Award Price:', false));
                        taskOrder.Task_Order_Ceiling_Value_K__c = toDecimal(getDatatFour('Award Price:', false));
                        System.debug('************************************************3');
                        ///taskOrder.Award_Date__c = parseDateTime(getDatatFour('Date of Award:', false));
                        try{
                            taskOrder.Award_Date__c = dtHelper.getDateTimeSpecialCase(getDatatFour('Date of Award:', false).normalizeSpace().trim()).date();
                        }catch(Exception e){
                            
                        }
                        
                        System.debug('************************************************4');
                        //taskOrder.Market_Research_Due_Date__c = parseDateTime(getDatatFour('Market Research Due Date:', false));
                        ///taskOrder.Base_Year_End_Date__c = parseDateTime(getDatatFour('Base Year End Date:', false));
                        ///taskOrder.Performance_End_Date__c = parseDateTime(getDatatFour('Performance End Date:', false));
                        try{
                            taskOrder.Base_Year_End_Date__c =dtHelper.getDateTimeSpecialCase(getDatatFour('Base Year End Date:', false).normalizeSpace().trim()).date();
                        }catch(Exception e){
                            
                        }
                        try{
                            taskOrder.Performance_End_Date__c = dtHelper.getDateTimeSpecialCase(getDatatFour('Performance End Date:', false).normalizeSpace().trim()).date();
                        }catch(Exception e){
                            
                        }
                        
                        System.debug('after contact');
                        
                        //New Changes for Govsuite
                        taskOrder.Period_of_Performance__c = toInteger(getDatatFour('PoP Years:', false));
                        //if(noOfYears != null)
                        //  taskOrder.Period_of_Performance_Months__c = noOfYears * 12;
                        System.debug('taskOrder.Period_of_Performance__c = '+taskOrder.Period_of_Performance__c);
                        taskOrder.Contract_Vehicle_picklist__c = 'T4 - TRANSFORMATION TWENTY ONE TOTAL TECHNOLOGY PR';
                        
                        String popStr = getDatatFour('PoP Notes:', false);
                        System.debug('popStr = '+popStr);
                        if(popStr != null){
                            if(popStr.length() > 255){
                                taskOrder.POP_Notes__c = popStr.substring(0,255);
                            }
                            else{
                                taskOrder.POP_Notes__c = popStr;
                            }
                        }
                        
                        getContact(getDatatFour('Contracting Officer:', false),taskOrder.Task_Order_Contracting_Organization__c,'Contracting Officer');
                        getContact(getDatatFour('Contracting Specialist:', false),taskOrder.Task_Order_Contracting_Organization__c,'Contract Specialist');
                        
                        if(contractingOfficerCon != null && contractingOfficerCon.Id != null)
                            taskOrder.TM_TOMA__Contracting_Officer__c = contractingOfficerCon.Id;
                        
                        if(contractingSpecialistCon != null && contractingSpecialistCon.Id != null)
                            taskOrder.TM_TOMA__Contract_Specialist__c = contractingSpecialistCon.Id;
                        
                        String sdvStr = getDatatFour('SDVOSB Set Aside?:', false);
                        if(sdvStr != null){
                            if(sdvStr.length() > 255){
                                taskOrder.Set_Aside__c = sdvStr.substring(0,255);
                            }
                            else{
                                taskOrder.Set_Aside__c = sdvStr;
                            }
                        }
                        
                        if(Test.isRunningTest()){
                            toAddress = 'testmail@gmail.com';
                        }
                        
                        return taskOrder;
                    }catch(Exception e){
                        //While mapping if any exception comes...
                        System.debug(''+e.getMessage()+'\t'+e.getLineNumber());
                    }
                }else{
                    //if contract vehicle not found, throw exception with custom message, below line send exception mail to the email sender
                    throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
                }
            }
            //if task order number not found in email.
            return null;
        }
        return Null;
    }
    
    private void getContact(String contactName, Id accountId, String title){
        List<Contact> tempConList = [SELECT Id,title FROM Contact WHERE Name = :contactName LIMIT 1000];
        
        if(!tempConList.isEmpty() && tempConList[0].title != null){
            if(tempConList[0].title != null &&  tempConList[0].title != '' && tempConList[0].title.equals('Contracting Officer')){
                contractingOfficerCon = tempConList[0];
                //return contractingOfficerCon;
            }       
            else if(tempConList[0].title != null &&  tempConList[0].title != '' && tempConList[0].title.equals('Contract Specialist')){
                contractingSpecialistCon = tempConList[0];
                //return contractingSpecialistCon;
            }
            //return tempConList[0];
            
        } else {
            Contact contact = new Contact();
            contact.LastName = contactName;
            contact.Is_FedTom_Contact__c=true;
            if(accountId != null)
                contact.AccountId = accountId;
            contact.Title = title;
            DMLManager.insertAsUser(contact);            
            
            if(contact.title.equalsIgnoreCase('Contracting Officer')){
                contractingOfficerCon = contact;
                //return contractingOfficerCon;
            }       
            else if(contact.title.equalsIgnoreCase('Contract Specialist')){
                contractingSpecialistCon = contact;
                //return contractingSpecialistCon;
            }
        }
        //return null;
    }
    
    private Id getAccountId(String accName){
        System.debug('abc'+accName);
        List<Account> tempConList = [SELECT Id FROM Account WHERE Name = :accName LIMIT 1000];
        System.debug('tempConList = '+tempConList);
        if(!tempConList.isEmpty()){
            return tempConList[0].Id;
        }
        return Null;
    }
    
    private Integer toInteger(String intString){
        Integer intTemp;
        try{
            System.debug(''+intString);
            intString = intString.normalizeSpace().trim();
            intTemp = Integer.valueOf(intString);
            return intTemp;
        }catch(Exception e){
            System.debug(''+e.getMessage());
            return Null;
        }        
    }
    
    private Decimal toDecimal(String intString){
        System.debug('intString '+intString);
        Decimal decTemp;
        try{
            intString = intString.normalizeSpace().trim();
            decTemp = Decimal.valueOf(intString);
            System.debug('decTemp '+decTemp);
            return decTemp;
        }catch(Exception e){
            return Null;
        }        
    }
    private boolean getStatus(String YnadN){
        if(YnadN=='Yes'){
            return true;            
        }else{
            return false;
        }
    }
    /*  private DateTime parseDateNTime(String dateTimeString){
System.debug(''+dateTimeString);
if(dateTimeString !=Null && dateTimeString!='' && dateTimeString!=' ' && dateTimeString.length()>20){
try{
String[] dateTimeSplit = dateTimeString.trim().split(' ');
Integer ind =0;
while(dateTimeSplit[ind].trim().length() == 0)
ind++;
String dateString = dateTimeSplit[ind];
String[] dateArray = dateString.split('/');
Date myDate = date.newInstance(Integer.valueOF(dateArray[2].trim()),Integer.valueOF(dateArray[0].trim()),Integer.valueOF(dateArray[1].trim()));
String timeString = dateTimeSplit[ind+1];
String[] timeArray = timeString.split(':');
Integer hour;
if(dateTimeSplit[ind+2].trim() == 'PM' && Integer.valueOF(timeArray[0].trim())!=12){
hour = Integer.valueOF(timeArray[0]) + 12;
}else if(dateTimeSplit[ind+2].trim() == 'PM' && Integer.valueOF(timeArray[0].trim())==12){
hour = 12;
}
if(dateTimeSplit[ind+2].trim() == 'AM' && Integer.valueOF(timeArray[0].trim())==12){
hour = 0;
}        
Time myTime = Time.newInstance(hour,Integer.valueOF(timeArray[1].trim()),0,0);        
DateTime myDateTime = DateTime.newInstanceGMT(mydate,myTime);       
return myDateTime;
}catch(Exception e){
System.debug('Exception '+e.getMessage()+' Cause '+e.getCause()+' Line No. '+e.getLineNumber());
return Null;
}
}else{
return Null;            
}

}*/
    
    private string getDatatFour(String paraStr, Boolean withHtml){
        System.debug('paraStr '+paraStr);
        Integer len = paraStr.length()+5;
        Integer strIndex = originalStr.indexof(paraStr) + len;
        System.debug('strIndex '+strIndex);
        Integer strIndex1 = originalStr.indexOf('</td>', strIndex);  
        System.debug('strIndex1 '+strIndex1);
        if(withHtml) {
            return originalStr.substring(strIndex ,strIndex1);
        }   
        System.debug('parseHTMLTagsAngles '+parseHTMLTagsAngles(originalStr.substring(strIndex ,strIndex1)).trim());
        return parseHTMLTagsAngles(originalStr.substring(strIndex ,strIndex1)).trim();
    }
    /*private String getData(String paraStr, Boolean withHtml){        
Integer len = paraStr.length();
Integer strIndex = originalStr.indexof(paraStr) + len;
Integer strIndex1 = originalStr.indexOf('</div>', strIndex);
if(withHtml) {
return originalStr.substring(strIndex ,strIndex1);
}       
return parseHtmlTags(parseHashHtmlTags(originalStr.substring(strIndex ,strIndex1)));
}*/
    
    /*private Id insertContact(String lName){
String name = parseHtmlTags(parseHashHtmlTags(lName)).substringBefore('(').trim();
String email = getContactEmail(lName).trim();
List<Contact> conOfficerList = [SELECT id , name FROM Contact Where email =:email];
if(conOfficerList != null && !conOfficerList.isEmpty() ){
return conOfficerList[0].id;               
}else{
Contact con = new Contact();
con.LastName = name;
con.Email = email;
con.Is_FedTom_Contact__c = true;
DMLManager.insertAsUser(con);            
return con.id;               
}
}*/
    
    /*  private Date parseDateTime(String dateTimeString){
System.debug('dateTimeString '+dateTimeString);
dateTimeString = dateTimeString.trim();
if(dateTimeString!=Null && dateTimeString!=''){ 
DateTimeHelper dt = new DateTimeHelper();
DateTime myDate = dt.getDateTime(dateTimeString);
date newDate = date.newinstance(myDate.year(), myDate.month(), myDate.day());
return newDate;
}else{ return Null;}

if(dateTimeString!=Null && dateTimeString!='' && dateTimeString!=' ' && dateTimeString.length()>20){
Date myDate;      
String dateS = dateTimeString.substringBefore(' ');  
System.debug('dateS '+dateS);
List<String> dateList = dateS.split('/');  
System.debug('dateList '+dateList);
myDate = date.newInstance(Integer.valueOF(dateList[2].trim()),Integer.valueOF(dateList[0].replace(' ','').trim()),Integer.valueOF(dateList[1].trim()));
System.debug('myDate '+myDate);
return myDate;
}else{
return Null;
}       
}*/
    
    /* private String parseHashHtmlTags(String parseString){
if(parseString!=Null && parseString!=''){
string result = parseString.replaceAll('&lt;br /&gt;', '\n');
result = result.replaceAll('&amp;nbsp;', ' ');
string HTML_TAG_PATTERN = '&lt;.*?&gt;.*?&lt;/.*?&gt;';
pattern myPattern = pattern.compile(HTML_TAG_PATTERN);
matcher myMatcher = myPattern.matcher(result);
result = myMatcher.replaceAll(' ');
result = result.replaceAll('\\n',' ').replaceAll('\\r',' ');
return result.replace('()', '').trim();
}else{
return Null;
}
}*/
    
    /* public String getContactEmail(String parseString){
String regex = '([a-zA-Z0-9_\\-\\.]+)@(((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3}))';       
Pattern myPattern = Pattern.compile(regex );    
Matcher myMatcher = myPattern.matcher(parseString);
if(myMatcher.find()){
return myMatcher.group();
}
return null;
}*/
    
    /* public String parseHtmlTags(String parseString){
if(parseString!=Null && parseString!=''){
string result = parseString.replaceAll('<br />', '');
result = result.replaceAll('<br/>', '');
result = result.replaceAll('&amp;nbsp;', ' ');
string HTML_TAG_PATTERN = '<.*?>*?</.*>';
pattern myPattern = pattern.compile(HTML_TAG_PATTERN);
matcher myMatcher = myPattern.matcher(result);
result = myMatcher.replaceAll(' ');
result = result.replaceAll('\\n',' ').replaceAll('\\r',' ');
return result.replace('()', '').trim();
}else{
return Null;
}
}*/
    
    private String parseHTMLTagsAngles(String parseString){
        if(parseString!=Null && parseString!=''){
            string result = parseString;            
            string HTML_TAG_PATTERN = '<.*?>';
            pattern myPattern = pattern.compile(HTML_TAG_PATTERN);
            matcher myMatcher = myPattern.matcher(result);
            result = myMatcher.replaceAll('');            
            return result;
        }else{
            return Null;
        }
    }
    
}