public with sharing class CIO_CSEmailParser {
    private String emailSubject='';
    public String taskOrderNumber='';
    Private String titledName='';
    Private Date proposalDueDate;
    private Datetime proposalDueDateTime;
    
    public void parse(Messaging.InboundEmail email){       
        // retrieving Subject 
        emailSubject = email.subject;
        if(emailSubject.length() == 0){
            throw new ContractVehicleNotFoundException(MessageConstantController.TASK_ORDER_NOT_FOUND );
        }
        
        try{
            if(emailSubject.toLowerCase().contains('amendment')){
                parseAmendment(email);
            }else{
                parseGeneral(email);
            }
        }catch(Exception e){
            throw new ContractVehicleNotFoundException(MessageConstantController.IMPROPER_EMAIL_TEMPLATE);
        }
    }
    
    private void parseAmendment(Messaging.InboundEmail email){
        String htmlText = email.HTMLBody;
        String emailPlainText = email.plainTextBody;
        System.debug('\n***********Plain Text**********\n');
        //emailSubject = emailSubject.stripHtmlTags(); 
        System.debug('emailSubject = '+emailSubject);        
        taskOrderNumber = checkValidString(getTaskOrderNumber(emailSubject), 79);
        String titledNameString = emailPlainText.substringBetween('titled','from');
        if(titledNameString != null && titledNameString.trim().length() > 0)
            titledName = emailPlainText.substringBetween('titled','from').trim();
        system.debug('titledName  amend=='+ titledName );
        System.debug('titledName = '+titledName);
        try{
            String proposalDueDateStr =  emailPlainText.substringBetween('To:', 'Amendment number:').trim();
            DateTimeHelper dateTimeHrlper = new DateTimeHelper();
            if(proposalDueDateStr != null && proposalDueDateStr.trim() != ''){
                proposalDueDateTime = dateTimeHrlper.getDateTime(proposalDueDateStr);
                if(proposalDueDateTime != null){
                    proposalDueDate = proposalDueDateTime.date();
                    System.debug('proposalDueDate = '+proposalDueDate);
                }
            }
        }catch(Exception e){
            proposalDueDate = null;
            System.debug('proposalDueDate = '+e.getMessage()+'\t'+e.getLineNumber());
        } 
    }
    
    private void parseGeneral(Messaging.InboundEmail email){
        String htmlText = email.HTMLBody;
        String emailPlainText = email.plainTextBody;
        System.debug('\n***********Plain Text**********\n');
        emailSubject = emailSubject.stripHtmlTags(); 
        System.debug('emailSubject = '+emailSubject);        
        taskOrderNumber = checkValidString(getTaskOrderNumber(emailSubject), 79);
        titledName = emailPlainText.substringBetween('titled ', ' from');
        //System.debug('titledName = '+titledName);
        try{
            //proposalDueDate = Date.parse(emailPlainText.substringBetween('Proposals are due by ', ' ').remove('*'));
            List<String> splitDate = emailPlainText.substringBetween('Proposals are due by', '\n').remove('*').replaceAll('(\\s+)', ' ').trim().split(' ');
            
            proposalDueDateTime = Datetime.parse(splitDate[0]+' '+splitDate[1]+' '+splitDate[2]);
            proposalDueDate = proposalDueDateTime.date();
            //getformatedDate(emailPlainText.substringBetween('To:', '\n').trim())
            System.debug('proposalDueDate = '+proposalDueDate);
        }catch(Exception e){
            proposalDueDate = null;
            System.debug('proposalDueDate = '+e.getMessage()+'\t'+e.getLineNumber());
        } 
    }   
    
    private String checkValidString(String str, Integer endLimit){
        if(str.length() > endLimit){
            String validString = str.substring(0,endLimit);
            return validString;
        }
        return str;
    }    
    
    public Task_Order__c getTaskOrder(String toAddress){        
        if(taskOrderNumber==null){
            System.debug('*********null');
            return null;
        } 
        if(Schema.sObjectType.Task_Order__c.isAccessible() && Schema.sObjectType.Contract_Vehicle__c.isAccessible()){ 
            List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];        
            if(!conVehicalList.isEmpty()){
                Task_Order__c taskOrder = new Task_Order__c();
                taskOrder.Contract_Vehicle__c = conVehicalList[0].Id;
                taskOrder.Name = taskOrderNumber;//Task Order Number
                if(titledName != null){
                    taskOrder.Task_Order_Title__c = checkValidString(titledName , 255);//Task Order Title
                }
                //taskOrder.Email_Reference__c = checkValidString(getTrimedEmailSubject(emailSubject) , 255);
                if(proposalDueDate!=null){            
                    taskOrder.Due_Date__c = proposalDueDate;
                    taskOrder.TM_TOMA__Proposal_Due_DateTime__c = proposalDueDateTime;
                } 
                taskOrder.Contract_Vehicle_picklist__c = 'CIO CS - IT Commodities/Solutions';//Contract Vehicle
                taskOrder.Release_Date__c = Date.today();//RFP/TO Release Date
                if(emailSubject.toLowerCase().contains('cancelled')){
                    taskOrder.Is_cancelled__c = true;
                }
                return taskOrder;
            }else{
                throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
            }
        }else{
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
        }
        
    }  
    
    public Task_Order__c updateTaskOrder(Task_Order__c taskOrder, String toAddress){
        if(Schema.sObjectType.Task_Order__c.isAccessible() && Schema.sObjectType.Contract_Vehicle__c.isAccessible()){         
            List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];
            System.debug('*********contract vechcle size  = '+ conVehicalList.size());
            if(!conVehicalList.isEmpty()){
                taskOrder.Contract_Vehicle__c = conVehicalList[0].Id;
                //taskOrder.Name = taskOrderNumber;//Task Order Number
                if(titledName != null){
                    taskOrder.Task_Order_Title__c = checkValidString(titledName , 255);//Task Order Title
                }
                if(proposalDueDate!=null){            
                    taskOrder.Due_Date__c = proposalDueDate;
                    taskOrder.TM_TOMA__Proposal_Due_DateTime__c = proposalDueDateTime;
                } 
                //taskOrder.Contract_Vehicle_picklist__c = 'CIOSP3 Unrestricted - CHIEF INFORMATION OFFICER SO';//Contract Vehicle
                taskOrder.Release_Date__c = Date.today();//RFP/TO Release Date
                if(emailSubject.toLowerCase().contains('cancelled')){
                    taskOrder.Is_cancelled__c = true;
                }
                return taskOrder;
            }else{
                throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
            }
        }else{
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
        }
    }
    
    private string getTaskOrderNumber(String emailSubject){
        try{
            String[] words = emailSubject.split(' ');
            String taskOrderNum = '';
            //taskOrderNum.las
            for(Integer i = 0; i<words.size();i++){
                if(words[i].startsWith('RF')){
                    while(words[i].length() !=0){                    
                        try{
                            Integer index = words[i].length();
                            String temp = words[i].substring(index -2);
                            if(temp.startsWith('-')){
                                taskOrderNum += words[i]; 
                                return taskOrderNum;
                            }
                            Integer.valueOf(temp);
                            taskOrderNum += words[i]; 
                            return taskOrderNum;
                        }catch(Exception e){
                            taskOrderNum += words[i]+' ';
                            i++;
                            continue;
                        }
                        
                    }
                    
                }
            }
            
            return taskOrderNumber;
        }catch(Exception e){
            throw new ContractVehicleNotFoundException(MessageConstantController.TASK_ORDER_NOT_FOUND);
        }
        return null;
    }
 /*   private Datetime getformatedDate(String dateTimeInString){
        System.debug('dateTimeInString = '+dateTimeInString);
        dateTimeInString = dateTimeInString.toLowerCase();
        //This method is specific to the task only.
        Map <String, Integer> months = new Map <String, Integer> {'january'=>1, 'february'=>2
            , 'march'=>3, 'april'=>4, 'may'=>5, 'june'=>6, 'july'=>7, 'august'=>8, 'september'=>9
            , 'october'=>10, 'november'=>11, 'december'=>12};
                dateTimeInString = dateTimeInString.replaceAll('(\\s+)', ' ').trim();  
        //Wednesday March 2, 2016 at 3:00pm
        try{
            List<String> splitDate = dateTimeInString.split(' ');
            for(String str : splitDate){
                System.debug('str'+str);
            }
            Integer monthInt;
            string day;
            string year;
            String timeStr;
            monthInt = months.get(splitDate.get(1).toLowerCase());
            System.debug('monthInt = '+monthInt);
            day = splitDate.get(2).remove(',');
            System.debug('day = '+day);
            year = splitDate.get(3);
            System.debug('year = '+year);
            timeStr = splitDate.get(5).trim().subString(0,splitDate.get(5).length() - 2);
            System.debug('timeStr = '+timeStr);
            if(splitDate.get(5).toLowerCase().contains('am')){
                timeStr += ' AM';
            }else{
                timeStr += ' PM';
            }
            
            //timeStr.
            DateTime parsedDateTime;
            try{   
                parsedDateTime = DateTime.parse(monthInt+'/'+day+'/'+year+' '+timeStr);
                system.debug('parsedDateTime = '+parsedDateTime);
                return parsedDateTime;
            }catch(Exception e){                
                return null;
            }
        }catch(Exception e){
            
        }
        //system.debug('proposalDueDate =='+ proposalDueDate);
        
        return Null;
    }    */
    
}