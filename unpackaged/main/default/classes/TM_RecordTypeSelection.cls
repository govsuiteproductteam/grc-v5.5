public with sharing class TM_RecordTypeSelection {
    public String recordTypeId{get;set;}
    public List<SelectOption> recordTypeOptionList{get; set;}
    public String isValidLiecence{get;set;}
    private ApexPages.StandardController sc;
    public TM_RecordTypeSelection(ApexPages.StandardController sc){
          isValidLiecence = LincenseKeyHelper.checkLicensePermitionForFedCLM();
        if(isValidLiecence == 'Yes'){
        this.sc = sc;
        fillRecordTypeOptions();
        }
    }
    
    public PageReference ContinueOnRcdTypeSelection() {
        PageReference pgRef = Page.TM_ContractNewPage;
        pgRef.getparameters().Put('RecordType', recordTypeId);
        pgRef.getparameters().PutAll(apexpages.currentpage().getparameters());
        pgRef.setRedirect(true);
        return pgRef;
    }
    
    /*public PageReference onClickCancel() {
        String retUrl = apexpages.currentpage().getparameters().get('retURL');
        PageReference pgRef = sc.cancel();
        if(String.isNotBlank(retUrl)) {
            pgRef = new PageReference(retUrl);
        }
        pgRef.setRedirect(true);
        return pgRef;
    }*/
    
    private void fillRecordTypeOptions(){
        recordTypeOptionList = new List<SelectOption>();
        Schema.DescribeSObjectResult describeResult = Contract_Vehicle__c.SObjectType.getDescribe();
        List<Schema.RecordTypeInfo> recordTypeInfoList = describeResult .getRecordTypeInfos();
        for(Schema.RecordTypeInfo recordTypeInfo: recordTypeInfoList){
            if(recordTypeInfo.isAvailable() && recordTypeInfo.getName() != 'Master'){
                recordTypeOptionList.add(new SelectOption(recordTypeInfo.getRecordTypeId(),recordTypeInfo.getName()));
            }
        }
    }
}