public class EGOS_NITACC_EmailParser implements IEmailParser {
    
    private String emailSubject;
    public String taskOrderNumber;
    private String taskOrderTitle;
    private String toRequestStatus;
    private String agency;
    private DateTime proposalDueDateTime;
    private Date proposalDueDate;
    private Date questionsDueDate;
    private String programSummary;
    public String modificationName;
    public String modificationText;
    
    
    
    public void parse(Messaging.InboundEmail email){
        emailSubject = email.subject.trim();
        if(emailSubject.length() == 0){
            throw new ContractVehicleNotFoundException(MessageConstantController.TASK_ORDER_NOT_FOUND);
        }
        
        String emailBody;
        if(Test.isRunningTest()){
            emailBody = email.plainTextBody;
        }else{
            emailBody = email.htmlBody.stripHtmlTags().trim();
        }     
        emailBody = emailBody.replaceall('[^\\x00-\\x7F]', '');
        emailBody = emailBody.replaceAll('=C2=B7','');
        emailBody = emailBody.replaceAll('%C2 %B7','');
        //emailBody = emailBody.replaceAll('','').trim();
        System.debug('Body = '+emailBody);  
        
        try{
            if(emailSubject.startsWith('Fwd:'))
                emailSubject = emailSubject.replace('Fwd:', '');
            if(emailSubject.startsWith('Re:'))
                emailSubject = emailSubject.replace('Re:', '');
        }catch(Exception ex){
            System.debug('Fwd Re Error : '+ex.getMessage()+'\t: '+ex.getLineNumber());
        }
        
        if(emailBody.contains('E-GOS ID:')){            
            taskOrderNumber = getPreDefinedString(emailbody, 'E-GOS ID:', 'Request:').trim();
            //taskOrderNumber = emailBody.substringBetween('E-GOS ID:','Request:').trim();                  
            //taskOrderNumber = taskOrderNumber.stripHtmlTags().trim();
            taskOrderNumber = taskOrderNumber.trim();          
        }
        System.debug('Task Order Number = '+taskOrderNumber);                        
        
        if(emailBody.contains('Title:')){
            taskOrderTitle = getPreDefinedString(emailbody, 'Title:', 'Description:').trim();
            //taskOrderTitle = emailBody.substringBetween('Title:','Description:').trim(); 
            //taskOrderTitle = taskOrderTitle.stripHtmlTags().trim();            
        }
        System.debug('Task Order Title = '+taskOrderTitle);				           
        
        if(emailBody.contains('Request:')){
            toRequestStatus = getPreDefinedString(emailbody, 'Request:', 'Title:').trim();
            //toRequestStatus = emailBody.substringBetween('Request:','Title:').trim();
            //toRequestStatus = toRequestStatus.stripHtmlTags().trim();            
        }
        System.debug('TO Request Status = '+toRequestStatus);
        
        if(emailBody.contains('Description:')){
            programSummary = getPreDefinedString(emailbody, 'Description:', 'Government Customer:').trim();
            //programSummary = emailBody.substringBetween('Description:','Government Customer:');
            //programSummary = programSummary.stripHtmlTags().trim();                            
        }
        System.debug('Program Summary = '+programSummary);

        String proposalDueDateTimeRaw;
        DateTimeHelper dtHelper = new DateTimeHelper();
        if(emailBody.contains('Proposals are due by')){               
            proposalDueDateTimeRaw= getPreDefinedString(emailbody,'Proposals are due by','\n');          
        }else if(emailBody.contains('Responses are due by')){              
            proposalDueDateTimeRaw = getPreDefinedString(emailbody,'Responses are due by','\n');            
        }
        System.debug('proposalDueDateTimeRaw = '+proposalDueDateTimeRaw);
        //*03/27/2018 8:00 PM Eastern Daylight Time*.        
        try{
            proposalDueDateTimeRaw = proposalDueDateTimeRaw.replace('*', '').replace('.', ''); //Remove * from Raw 
            proposalDueDateTime = dtHelper.getDateTimeSpecialCase(proposalDueDateTimeRaw);    
            if(proposalDueDateTime!=null)
                proposalDueDate = proposalDueDateTime.date();  
        }catch(Exception ex){
            System.debug('Proposal Due Date Time Error : '+ex.getMessage()+'\t: '+ex.getLineNumber());    
        }        
        System.debug('Proposal Due Date Time = '+proposalDueDateTime);
        System.debug('Proposal Due Date = '+proposalDueDate);
        
        
        if(emailBody.contains('Government Customer:')){                      
            if(emailBody.contains('Proposals are due by')){                   
                agency = getPreDefinedString(emailbody,'Government Customer:','Proposals are due by');
            }else if(emailBody.contains('Your Response Status:')){                    
                agency = getPreDefinedString(emailbody,'Government Customer:','Your Response Status:');
            }else if(emailBody.contains('You will be notified via email')){
                agency = getPreDefinedString(emailbody,'Government Customer:','You will be notified via email');
            }else if(emailBody.contains('Responses are due by')){                       
                agency = getPreDefinedString(emailbody,'Government Customer:','Responses are due by');
            }                                       
        }
        System.debug('Agency = '+agency);
        
        
        if(emailBody.contains('All questions must be submitted by')){            
            String questionsDueDateRaw = getPreDefinedString(emailbody,'All questions must be submitted by','\n');            
            //System.debug('Data = '+questionsDueDateRaw);
            try{
                questionsDueDateRaw = questionsDueDateRaw.replace('*', '').replace('.', '');
                questionsDueDate = dtHelper.getDateTimeSpecialCase(questionsDueDateRaw).date();
            }catch(Exception ex){
                System.debug('Question Due Date Error : '+ex.getMessage()+'\t: '+ex.getLineNumber());   
            }            
        }
        System.debug('Questions Due Date = '+questionsDueDate);
        
        if(emailSubject.toLowerCase().contains('question')){
            modificationName = 'Q&A';
            modificationText = emailBody.replaceAll('Dear.*\\,.*', '').trim(); // Remove Greeting
        }else if(emailSubject.toLowerCase().contains('response')){
            modificationName = 'Response Requested';
            modificationText = emailBody.replaceAll('Dear.*\\,.*', '').trim(); // Remove Greeting
        }
        System.debug('modificationName = '+modificationName);
        
        
    }
    
    public Task_Order__c getTaskOrder(String toAddress){
        if(taskOrderNumber==null){
            return null;
        }
        
        Task_Order__c taskOrder = new Task_Order__c();
        List<Contract_Vehicle__c> conVehicalList = [SELECT Id FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];  
        
        if(!conVehicalList.isEmpty()){
            taskOrder.Contract_Vehicle__c = conVehicalList[0].Id; // Label = Contract Vehicle
            taskOrder.Name = taskOrderNumber; //Task Order Number
            
            if(taskOrderTitle !=null)
                taskOrder.Task_Order_Title__c = taskOrderTitle.trim();// Task Order Title
            
            if(toRequestStatus !=null)
                taskOrder.TO_Request_Status__c = toRequestStatus.trim();// TO Request Status
            
            if(agency !=null){
                taskOrder.Agency__c = agency.trim();// Agency
            }
            
            if(programSummary !=null){
                taskOrder.Description__c = programSummary.trim();// Program Summary                
            }
            
            if(proposalDueDate !=null){
                taskorder.Due_Date__c = proposalDueDate;// Proposal Due Date
            }
            
            if(proposalDueDateTime !=null){
                taskOrder.Proposal_Due_DateTime__c = proposalDueDateTime;// Proposal Due DateTime
            }
            
            if(questionsDueDate !=null){
                taskOrder.Questions_Due_Date__c = questionsDueDate;// Questions Due Date
            }
            
            taskOrder.Release_Date__c = Date.today();//RFP TO Release Date
            return taskOrder;
        }else{
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
        }       
    }
    
    public Task_Order__c updateTaskOrder(Task_Order__c taskOrder, String toAddress){
        List<Contract_Vehicle__c> conVehicalList = [SELECT Id FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];          
        if(!conVehicalList.isEmpty()){
            taskOrder.Contract_Vehicle__c = conVehicalList[0].Id; // Label = Contract Vehicle
            //taskOrder.Name = taskOrderNumber.trim(); //Task Order Number
            
            if(taskOrderTitle !=null)
                taskOrder.Task_Order_Title__c = taskOrderTitle.trim();// Task Order Title
            
            if(toRequestStatus !=null)
                taskOrder.TO_Request_Status__c = toRequestStatus.trim();// TO Request Status
            
            if(agency !=null){
                taskOrder.Agency__c = agency.trim();// Agency
            }
            
            if(programSummary !=null){
                taskOrder.Description__c = programSummary.trim();// Program Summary                
            }
            
            if(proposalDueDate !=null){
                taskorder.Due_Date__c = proposalDueDate;// Proposal Due Date
            }
            
            if(proposalDueDateTime !=null){
                taskOrder.Proposal_Due_DateTime__c = proposalDueDateTime;// Proposal Due DateTime
            }
            
            if(questionsDueDate !=null){
                taskOrder.Questions_Due_Date__c = questionsDueDate;// Questions Due Date
            }
            return taskOrder;
        }else{
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
        }          
    }
              
    private String getPreDefinedString(String emailbody,String startText,String endText){
        System.debug('Start Text = '+startText);
        System.debug('End Text = '+endText);
        try{
            return emailBody.substringBetween(startText,endText).trim();           
        }catch(Exception ex){
            System.debug('Error Pre Defined String Method : '+ex.getMessage()+'\t: '+ex.getLineNumber());
            
        } 
        return '';
    }
    
    /*private String getModificationName(String emailSubject){       
        try{
            if(emailSubject.toLowerCase().contains('question')){
                return 'Q&A';
            }else if(emailSubject.toLowerCase().contains('response')){
                return 'Response Requested';                
            }
        }catch(Exception ex){            
            System.debug('Modification Name Error : '+ex.getMessage()+'\t: '+ex.getLineNumber());            
        }
        return null;
    }*/
}