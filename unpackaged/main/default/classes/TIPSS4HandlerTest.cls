@isTest
public class TIPSS4HandlerTest {
    public static testMethod void testCase1(){
        
        
        Account acc = TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Contract_Vehicle__c conVehi = TestDataGenerator.createContractVehicle('T-123', acc);
        conVehi.Incoming_Task_Order_Mailbox__c = 'tipss4@o-ow3ih7wdz676pjknyerpo2f4xlunif1foer07lh7orqp87abo.37-pvateac.na31.apex.salesforce.com';
        
        insert conVehi;
        
        Test.startTest();
        TIPSS4Handler handler = new TIPSS4Handler();
        //for new Task Order Creation  
        Messaging.InboundEmail email  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        List<StaticResource> srList = [SELECT Id,Body FROM StaticResource WHERE Name='TIPSS4'];
        
        email.plainTextBody = srList.get(0).Body.toString();
        email.fromAddress ='test@test.com';
        email.fromName = 'ABC XYZ';
        email.subject = 'Fwd:RTPP/RTCP – 4348, entitled “Common Integration Services (CIS) Architectural Analysis and System Design Support”';
        email.toaddresses = new List<String>();
        email.toaddresses.add('tipss4@o-ow3ih7wdz676pjknyerpo2f4xlunif1foer07lh7orqp87abo.37-pvateac.na31.apex.salesforce.com');
        
        //For Updating existing task order with existing Contact
        
        
        List<StaticResource> srList1 = [SELECT Id,Body FROM StaticResource WHERE Name='TIPSS4_1'];
        Messaging.InboundEmail email1  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env1 = new Messaging.InboundEnvelope();
        email1.plainTextBody = srList1.get(0).Body.toString();
        email1.fromAddress ='test@test.com';
        email1.fromName = 'ABC XYZ';
        email1.subject = 'Re:RTPP/RTCP – 4348, entitled “Common Integration Services (CIS) Architectural Analysis and System Design Support”';
        email1.toaddresses = new List<String>();
        email1.toaddresses.add('tipss4@o-ow3ih7wdz676pjknyerpo2f4xlunif1foer07lh7orqp87abo.37-pvateac.na31.apex.salesforce.com');
        
        //For Updating existing task order with new Contact
        List<StaticResource> srList2 = [SELECT Id,Body FROM StaticResource WHERE Name='TIPSS4_1'];
        Messaging.InboundEmail email2  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env2 = new Messaging.InboundEnvelope();
        email2.plainTextBody = srList2.get(0).Body.toString();
        email2.fromAddress ='test@test.com';
        email2.fromName = 'ABC XYZ';
        email2.subject = 'RFP RS3-18-0003';
        email2.toaddresses = new List<String>();
        email2.toaddresses.add('tipss4@o-ow3ih7wdz676pjknyerpo2f4xlunif1foer07lh7orqp87abo.37-pvateac.na31.apex.salesforce.com');
        Messaging.InboundEmailResult result = handler.handleInboundEmail(email, env);
        List<Task_Order__c> taskOrderList = [SELECT Id FROM Task_Order__c];
        System.assertEquals(taskOrderList.size(), 1);
        //taskOrderList[0].Customer_Agency__c = acc.id;
        //update taskOrderList;
        Messaging.InboundEmailResult result1 = handler.handleInboundEmail(email1, env1);
        List<Contract_Mods__c> contractModList = [SELECT Id FROM Contract_Mods__c];
        System.assertEquals(contractModList.size(), 1);
        Messaging.InboundEmailResult result2 = handler.handleInboundEmail(email2, env2);
        
        Test.stopTest();
    }    
}