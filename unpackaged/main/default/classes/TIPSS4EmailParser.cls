public class TIPSS4EmailParser {
private String emailSubject;
    public String taskOrderNumber;
    public boolean isAmendent;   //AMENDMENT 
    private String taskOrderTitle;   
    Private Datetime proposalDueDateTime; 
    Private DateTime questionDueDateTime;    
    private Contact con;
    private String classification;
    
    public void parse(Messaging.InboundEmail email){
        isAmendent = false;
        emailSubject = email.subject;
        if(emailSubject.length() == 0){
            throw new ContractVehicleNotFoundException(MessageConstantController.TASK_ORDER_NOT_FOUND );
        }
        
        if(emailSubject.startsWith('Fwd:'))
            emailSubject = emailSubject.replace('Fwd:', '');
        if(emailSubject.startsWith('Re:'))
            emailSubject = emailSubject.replace('Re:', '');
        
        taskOrderNumber = emailSubject.substringBetween('RT',',');
        if(taskOrderNumber != null)
            taskOrderNumber = 'RT'+taskOrderNumber;
        
        System.debug(''+taskOrderNumber);
        
        taskOrderTitle = emailSubject.substringAfter('entitled'); 
        if(taskOrderTitle!=null && taskOrderTitle.contains('“'))
                taskOrderTitle = taskOrderTitle.replace('“','');
        
        if(taskOrderTitle!=null && taskOrderTitle.contains('”'))
            taskOrderTitle = taskOrderTitle.replace('”','');
        
        if(taskOrderTitle!=null && taskOrderTitle.contains('"'))
            taskOrderTitle = taskOrderTitle.replace('"','');
        System.debug('taskOrderTitle = '+taskOrderTitle);      
        
        String body = email.plainTextBody;       
        
        if(body!=null && body!=''){
            String bodyNormalize = body.normalizeSpace();
            DateTimeHelper dtHelper = new DateTimeHelper();
            String questionDateRaw = bodyNormalize.substringBetween('Past performance references and questions are due', '.');
            String proposalDateRaw = bodyNormalize.substringBetween('Technical and Price Proposal responses to this RTPP/RTCP are due', '.');
            if(proposalDateRaw != null && proposalDateRaw != ''){
                proposalDueDateTime =  dtHelper.getDateTimeSpecialCase(proposalDateRaw.replace('*','').trim());
                System.debug(''+proposalDueDateTime);
            }
            
            
            if(questionDateRaw != null && questionDateRaw != ''){
                questionDueDateTime=  dtHelper.getDateTimeSpecialCase(questionDateRaw.replace('*','').trim());
                System.debug(''+questionDueDateTime);
            }
                       
            
            //String contactInfo = body.substringAfter('Thank you,');        
            //System.debug(''+contactInfo.split('\n'));        
            //createContact(contactInfo);
            // System.debug('contactId1 = '+contactId1);
            
            // New changes New Contact method
            createContact(body,email);
        }
    }
    
    public Task_Order__c getTaskOrder(String toAddress){ 
        if(taskOrderNumber==null){
            return null;
        }        
        Task_Order__c taskOrder = new Task_Order__c();
        List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];        
        if(!conVehicalList.isEmpty()){
            taskOrder.Contract_Vehicle__c = conVehicalList[0].Id;
            taskOrder.Name = taskOrderNumber;//Task Order Number
            
            if(taskOrderTitle != null)
                taskOrder.Task_Order_Title__c = EmailHandlerHelper.checkValidString(taskOrderTitle.trim(),255);
            
                       
            if(proposalDueDateTime !=null){            
                taskOrder.Due_Date__c = proposalDueDateTime.date();
                taskOrder.Proposal_Due_DateTime__c = proposalDueDateTime;
            }
            
             if(questionDueDateTime !=null){            
                taskOrder.Questions_Due_Date__c= questionDueDateTime.date();                
            }
            
            try{
                if(con !=null && con.id ==null)
                    DMLManager.insertAsUser(con); 
                   // insert con;
            }catch(Exception e){
               // System.debug(con);
            }
            
            if(con !=null && con.Id!= null) {           
                if(con.title != null){
                    if(con.title == 'Contract Specialist')
                        taskOrder.Contract_Specialist__c = con.Id;
                    if(con.title == 'Contracting Officer')
                        taskOrder.Contracting_Officer__c = con.Id;
                }
            }
            
            
            taskOrder.Release_Date__c = Date.today();//RFP/TO Release Date
            taskOrder.Contract_Vehicle_picklist__c = 'TIPSS-4 Information Technology Services (ITS)';
            return taskOrder;
        }else{
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
        }
    }
    
    public Task_Order__c updateTaskOrder(Task_Order__c taskOrder, String toAddress){
        List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];
        
        if(!conVehicalList.isEmpty()){
            taskOrder.Contract_Vehicle__c = conVehicalList[0].Id;
            
            if(taskOrderTitle != null)
                taskOrder.Task_Order_Title__c = EmailHandlerHelper.checkValidString(taskOrderTitle.trim(),255);           
           
            
            if(proposalDueDateTime !=null){            
                taskOrder.Due_Date__c = proposalDueDateTime.date();
                taskOrder.Proposal_Due_DateTime__c = proposalDueDateTime;
            }  
            
            if(questionDueDateTime !=null){            
                taskOrder.Questions_Due_Date__c= questionDueDateTime.date();                
            }  
            
           try{
                if(con != null){ 
                    if(taskOrder.Customer_Agency__c != null){
                        con.AccountId = taskOrder.Customer_Agency__c;
                        DMLManager.upsertAsUser(con);
                    }else{
                        if(con.id ==null){
                            DMLManager.insertAsUser(con);
                        }
                    }                
                 } 
            }catch(Exception e){
                
            }    
            
            if(con !=null && con.id!= null) {           
                if(con.title != null){
                    if(con.title == 'Contract Specialist')
                        taskOrder.Contract_Specialist__c = con.Id;
                    if(con.title == 'Contracting Officer')
                        taskOrder.Contracting_Officer__c = con.Id;
                }
            }                          
            
            return taskOrder;
        }else{
            system.debug('##################################');
            throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
            
        }
    }
    
    /*private void createContact(String contactInfo){
        //System.debug(''+contactInfo);        
        try{
            contactInfo = contactInfo.trim();
            String[] contactInfoSplit = contactInfo.split('\n');
            System.debug(''+contactInfoSplit);
            if(contactInfoSplit.size()>0){
                String email;
                //String[] contact1InfoSplit = contactInfoSplit[0].split(' ');
                for(String emailId : contactInfoSplit){
                    System.debug(' = '+emailId);
                    // if(emailId.contains('@') && emailId.contains('.com')){
                    if(emailId.contains('@')){
                        email = emailId.trim();
                        break;
                    }                    
                }
                
                System.debug('email = '+email);
                con = new Contact();
                if(email!=null){
                    
                    List<Contact> contactList;  
                    contactList = [SELECT id ,name,title FROM Contact Where email =:email];                    
                    
                    if(contactList != null && !contactList.isEmpty() ){//for existing contact
                        System.debug('if = '+contactList[0].id);
                        con = contactList[0];               
                    }else{//for new contact;
                        System.debug('else');
                        con.Is_FedTom_Contact__c = true;
                        con.Email = email;
                        String[] namesplit = contactInfoSplit[0].split(' ');
                        if(namesplit!= null && !namesplit.isEmpty()){
                            if(nameSplit.size()==2){
                                con.firstName = nameSplit[0];
                                con.lastName = nameSplit[1];
                            }else if(nameSplit.size()==3){
                                con.firstName = nameSplit[0];
                                con.lastName = nameSplit[2];
                            }else {
                                con.lastName = nameSplit[0];
                            }
                        }
                        
                        for(String info : contactInfoSplit){
                            if(info.toLowerCase().contains('contract specialist') ){
                                con.title = 'Contract Specialist';
                            }else If(info.toLowerCase().contains('contracting officer')){
                                con.title = 'Contracting Officer';
                            }       
                        }
                    }
                }
            }
            
            
        }catch(Exception e){
            System.debug(''+e.getMessage()+'\t'+e.getLineNumber());
        }
        
    }*/
    
    
    private void createContact(String emailBody,Messaging.InboundEmail email){
        try{
            con = new Contact();            
            List<Contact> contactList = [Select id, FirstName, LastName, Email, Title from Contact WHERE Email = :email.fromAddress];
            if(contactList != null && !contactList.isEmpty() ){//for existing contact
                con = contactList[0]; 
                System.debug('Con Available = '+con.FirstName+':'+con.Id+':'+con.Title);
            }else{//for new contact
                String emailFromName = email.fromName;        
                String[] nameSplited = emailFromName.split(' ');
                con.FirstName = nameSplited[0];
                con.LastName = nameSplited[1];
                con.Email =  email.fromAddress;
                String contactInfo = emailBody.substringAfter('Thank you,');        
                //System.debug('*********'+contactInfo.split('\n'));
                contactInfo = contactInfo.trim();
                String[] contactInfoSplit = contactInfo.split('\n');
                if(contactInfoSplit.size()>0){                    
                    for(String info : contactInfoSplit){
                        //System.debug('info^^^^^^^^^^'+info);
                        if(info.toLowerCase().contains('contract specialist') ){
                            con.title = 'Contract Specialist';
                        }else If(info.toLowerCase().contains('contracting officer')){
                            con.title = 'Contracting Officer';
                        }                               
                    }
                }                                                
            }                                                                                  
        }catch(Exception ex){
            System.debug('Create Contact Error ='+ex.getMessage()+'\tLine Number = '+ex.getLineNumber());
        }                
    }
}