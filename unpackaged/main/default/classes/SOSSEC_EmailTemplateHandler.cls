global with sharing class SOSSEC_EmailTemplateHandler implements Messaging.InboundEmailHandler{
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        if(Schema.sObjectType.Task_Order__c.isAccessible() && Schema.sObjectType.Contract_Vehicle__c.isAccessible() && Schema.sObjectType.Contact.isAccessible()){
            List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:email.toAddresses.get(0)];
            if(!conVehicalList.isEmpty()){
                SOSSEC_Parser parser = new SOSSEC_Parser();
                parser.parse(email);//Email Parsing will be handle by parse method.
                if(parser.taskOrderNumber!=null && parser.taskOrderNumber.trim().length()>0){
                    Task_Order__c taskOrder = EmailHandlerHelper.getExistTaskOrder(parser.taskOrderNumber,conVehicalList[0].ID);
                    if(taskOrder == null){//If email template not exists
                        taskOrder = parser.getTaskOrder(email.toAddresses.get(0));
                        if(taskOrder != null){  
                            try{
                                taskOrder.Release_Date__c = System.today();
                                DMLManager.insertAsUser(taskOrder);
                                EmailHandlerHelper.insertBinaryAttachment(taskOrder,email);
                                EmailHandlerHelper.insertTextAttachment(taskOrder,email);
                                EmailHandlerHelper.insertActivityHistory(taskOrder,email);
                            }catch(Exception e){
                                System.debug('error message = '+e.getMessage());
                            }
                        }
                    }else{
                        Task_Order__c oldTaskOrder = taskOrder.clone(false,true,false,false);       
                        Task_Order__c updatedTakOrder = parser.updateTaskOrder(taskOrder, email.toAddresses.get(0));
                        try{
                            EmailHandlerHelper.insertContractMods(updatedTakOrder , oldTaskOrder,email);
                            //DMLManager.updateAsUser(updatedTakOrder);
                            update updatedTakOrder;
                            EmailHandlerHelper.insertBinaryAttachment(taskOrder,email);
                            EmailHandlerHelper.insertTextAttachment(taskOrder,email);
                            EmailHandlerHelper.insertActivityHistory(taskOrder,email);
                        }catch(Exception ex){}
                    }
                }
            }else{
                throw new ContractVehicleNotFoundException(MessageConstantController.CONTRACT_NOT_FOUND );
            }
        }
        return result;
    }    
}