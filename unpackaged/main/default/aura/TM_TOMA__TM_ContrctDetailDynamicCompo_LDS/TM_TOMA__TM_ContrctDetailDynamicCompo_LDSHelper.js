({
    /*helperDoInit : function(component, event) {
        var start_time = new Date().getTime();
        var fieldList = component.get("c.getAllFieldsApi");
        fieldList.setParams({
            "sobjectname":"Opportunity",
            "recId":component.get("v.recordId")
        });
        fieldList.setCallback(this,function(resultData){
            var status = resultData.getState();
            if(status ==="SUCCESS"){
                var resultList = resultData.getReturnValue();
                var testList = [];
                if(! $A.util.isEmpty(resultList) && ! $A.util.isUndefined(resultList)){
                    for(var i=0;i<resultList.length;i++){
                        testList.push(resultList[i].trim());
                        console.log(resultList[i]);
                        
                    }
                    component.set("v.fieldList",testList);
                    component.set("v.dataLoaded",true);
                    var request_time = new Date().getTime() - start_time;
                    console.log('request_time-'+request_time);
                }
                
            }
        });
        $A.enqueueAction(fieldList);
    },*/
    getTabWrapperData: function(component, event, helper) {
        console.log('getTabWrapperData');
        var start_time = new Date().getTime();
        var fieldList = component.get("c.createContract");
        fieldList.setParams({
            "recordId":component.get("v.recordId")
        });
        fieldList.setCallback(this,function(resultData){
            var status = resultData.getState();
            console.log('getTabWrapperData status '+status);
            if(status ==="SUCCESS"){
                var resultList = resultData.getReturnValue();
                component.set("v.JsonDataList",resultList);
                 console.log('777777777777 '+status);
                if(resultList != null && resultList.tabWrapList != null && resultList.tabWrapList.length >0){
                    component.set("v.TabDataListSorted",resultList.tabWrapList);
                    component.set("v.TabDataListSorted_edit",resultList.tabWrapList);
                    component.set("v.TabNamesListWrp",resultList.allTabsNameWrpList);
                    component.set("v.hasNoTabs",resultList.hasNoTabs);
                    component.set("v.hasEditPermission",resultList.doUserHaveEditPermission);//08-2019 check is record have edit access
                    var recTypId = resultList.contractObj['RecordTypeId'];
                    component.set("v.recordTypeId",recTypId);
                    component.set("v.dataLoaded",true);
                    /*code newlly added for adding depandancy*/
                    var allTabData = resultList.tabWrapList;
                    var fieldfieldtypemap = new Map();
                    var fielddepandancymap = new Map();
                    
                    for(var i in allTabData )
                    {
                        var secList = allTabData[i].sectionWrapList;
                        //alert('secList '+secList);
                        for(var k in secList)
                        {
                            var fieldList = secList[k].fieldWrapList
                            //alert('fieldList '+fieldList);
                            for(var l in fieldList)
                            {
                                //alert('fieldList FieldApiName__c '+fieldList[l].field.FieldApiName__c+' '+fieldList[l].fieldType);
                                // console.log('fieldfieldtypemap '+fieldfieldtypemap.length);
                                //console.log('FieldApiName__c '+fieldList[l].field.FieldApiName__c);
                                // console.log('fieldType '+fieldList[l].fieldType);
                                fieldfieldtypemap.set(fieldList[l].field.TM_TOMA__FieldApiName__c, fieldList[l].fieldType);
                                
                                fielddepandancymap.set(fieldList[l].field.TM_TOMA__FieldApiName__c, fieldList[l].dependantFieldString);
                            }
                        }
                    }
                    //alert('fieldfieldtypemap '+fieldfieldtypemap.size);
                    component.set("v.FieldVSFieldTypeMap",fieldfieldtypemap);
                    component.set("v.FieldVSDepandancyMap",fielddepandancymap);
                    var request_time = new Date().getTime() - start_time;
                    console.log('request_time-'+request_time);
                }
              // component.set("v.showSpinner",false); 
            }else{
                console.log("Error ");
            }
            
            
        });
        $A.enqueueAction(fieldList);
       /* if(component.get("v.myChangeCall"))
        {
            alert('@ false');
            component.set("v.myChangeCall",false);
        }
        else{
            alert('@ false');
            component.set("v.myChangeCall",true);  
        }
        */
       // helper.callDepandacyonLoad(component, event,helper);
    },
    handleTabPress: function(component, event) {
        var targetevent = event.target;
        var tabDataRec = targetevent.getAttribute('data-record');
        var tabData = tabDataRec.split('#####');//tabDataRec.split('######'); change on 20-08-2018 if their is onlly 5# instaed of 6#
        var tabLoadedMapJs = component.get("v.LoadedTabMap");
        var editMode = component.get("v.isEditMode");
        if(editMode){
            component.set("v.isFirstLoad",false);
            component.set("v.currentTabId",tabData[0]);
            component.set("v.showSpinner",false);
            var selectedTabData = document.getElementById('data_'+tabData[0]);
            selectedTabData.style.display='block';
            return;
        }
        if(tabLoadedMapJs == null){
            var firstTab = document.getElementById('first_tab');
            if(firstTab != null){
                var firstTabData = firstTab.dataset.record;
                var tabMap = new Map();
                tabMap[firstTabData]='1';
                component.set("v.LoadedTabMap",tabMap);
            }
            tabLoadedMapJs = component.get("v.LoadedTabMap");
        }
        
        if(tabLoadedMapJs != null){
            console.log("if tabLoadedMapJs not null");
            if(tabLoadedMapJs[tabDataRec] == undefined ){
                console.log('Hello');
                var tabWrpList = component.get("v.TabDataListSorted");
                for(var i=0;i<tabWrpList.length;i++){
                    if(tabWrpList[i].tabDataRecord == tabDataRec){
                        tabWrpList[i].isLoadedInDom =true;
                        break;
                    }
                    
                }
                
                component.set("v.TabDataListSorted",tabWrpList);
                tabLoadedMapJs[tabDataRec]='1';
                component.set("v.LoadedTabMap",tabLoadedMapJs);
                component.set("v.currentTabId",tabData[0]);
                component.set("v.upperTabRefreshFlag",false);
                component.set("v.upperTabRefreshFlag",true);
                setTimeout(function(){ 
                    if(component.get("v.showSpinner"))
                        component.set("v.showSpinner",false); 
                }, 3000);
                return;
            }else{
                // component.set("v.upperTabRefreshFlag",false);
                //component.set("v.upperTabRefreshFlag",true);
                component.set("v.isFirstLoad",false);
                component.set("v.currentTabId",tabData[0]);
                component.set("v.showSpinner",false);
                console.log('@@@@ data_'+tabData[0]);
                var selectedTabData = document.getElementById('data_'+tabData[0]);
                selectedTabData.style.display='block';
                return;
                
            }
        }else{
            var tabWrpList = component.get("v.TabDataListSorted");
            for(var i=0;i<tabWrpList.length;i++){
                if(tabWrpList[i].tabDataRecord == tabDataRec){
                    tabWrpList[i].isLoadedInDom =true;
                    break;
                }
                
            }
            
            component.set("v.TabDataListSorted",tabWrpList);
            var tabMap = new Map();
            tabMap[tabDataRec]='1';
            component.set("v.LoadedTabMap",tabMap);
        }
        component.set("v.isFirstLoad",false);
        component.set("v.currentTabId",tabData[0]);
        component.set("v.tabRefreshFlag",false);
        component.set("v.tabRefreshFlag",true);
        
        setTimeout(function(){ 
            if(component.get("v.showSpinner"))
                component.set("v.showSpinner",false); 
        }, 4000);
    },
    handleSaveData :function(component, event) {
        var eventFields = event.getParam("fields");
        console.log(JSON.stringify(eventFields));
        // component.set("v.isEditMode",false);
        //   component.set("v.dataLoaded",false);
        //  component.set("v.dataLoaded",true);
        
    },
    helperHandleRecSuccess:function(component, event) {
        //alert("v.myChangeCall");
        component.set("v.isSubmitAction",false);
        console.log("Validation error-"+component.get("v.isValidationError"));
        if(!component.get("v.isValidationError")){
          //  var eventFields = event.getParam("fields");
          //  console.log("EVENTFIELDS"+JSON.stringify(eventFields));
            component.set("v.isEditMode",false);
          //  component.set("v.dataLoaded",false);
            //component.set("v.dataLoaded",true);
            
            var firstTab = document.getElementById('first_tab');
            if(firstTab != null){
                var firstTabData = firstTab.dataset.firsttab;
                if(firstTabData!=null && firstTabData !=undefined){
                    component.set("v.currentTabId",firstTabData);
                }else{
                    component.set("v.currentTabId",'NONE');
                }
                
            }
            component.find('notificationCenter').showToast({
                'variant':'success',
                'title':'SUCCESS',
                'message':'Record has been updated Successfully.'
            });
            //newlly added 09-08-2018
           /* var navigateEvent = $A.get("e.force:navigateToSObject");
            navigateEvent.setParams({ "recordId": component.get('v.recordId') });
            navigateEvent.fire()*/
            //alert('@@')
            var payload = event.getParams().response;
            /* $A.get("e.force:navigateToURL").setParams({ 
                "url": "/"+payload.id
            }).fire();*/
            // window.open('/'+payload.id,'_top');
           var existingid = component.get('v.recordId')
           // window.open('/'+existingid,'_top');
             window.open('/lightning/r/TM_TOMA__Contract_Vehicle__c/'+existingid+'/view','_top');
        }
        
    },
    callDepandacyonLoad:function(component, event,helper) {
       // alert('callDepandacyonLoad');
        var fieldfieldtypemap = component.get("v.FieldVSFieldTypeMap");
        var fielddepandancymap = component.get("v.FieldVSDepandancyMap");
        var isErrorInLastsave = false;
        for(var cmp in component.find("myId")){
            var compData = component.find("myId")[cmp];
            var fieldnam = compData.get("v.fieldName");
            
            var fildepandancy = fielddepandancymap.get(fieldnam);
            
            if(fildepandancy != ''){
                // var output = compData.get("v.class");
                var originalVal = compData.get("v.value");
                var outputClass = compData.get("v.class");//06-11-2018 for field level read access issue
                //this code is done due to some time depandancy donot blank the value due to which one field is in disabled mode and it have value so it open its child depandant field
                var isdisbal = compData.get("v.disabled");//29-08-2018 for depandacy 
                if(isdisbal === true){//29-08-2018
                   if(originalVal != null && originalVal != 'null' && originalVal != undefined && originalVal != 'undefined')
                   {
                       //isErrorInLastsave = true;
                       if(outputClass.search('depandancyreadonlly') != -1 == false)//06-11-2018 for field level read access issue
                       {
                           console.log(fieldnam+' if outputClass '+outputClass); 
                           isErrorInLastsave = true;
                           originalVal = '';
                       }
                   }
                    //originalVal = '';
                }
                if(originalVal == null || originalVal == 'null' || originalVal == undefined || originalVal == 'undefined')
                {
                    originalVal='';
                }
                console.log('fieldnam '+fieldnam+' originalVal '+originalVal);
                var typeOfField = fieldfieldtypemap.get(fieldnam);
                //   alert(fieldnam+'  '+compData.get("v.value"));
                console.log('fildepandancy ###**** '+fildepandancy);
                var fieldDependancyArray = fildepandancy.split('SPLITREC');
                //alert('fieldDependancyArray '+fieldDependancyArray );
                var processApiName = '';
                for(var index=0;index < fieldDependancyArray.length;index++){
                    console.log('fieldDependancyArray[index] '+fieldDependancyArray[index] );
                    if(fieldDependancyArray[index].trim() != ''){
                        var dataRec = fieldDependancyArray[index];
                        var dataRecArray = dataRec.split('SPLITVAL');
                        if(dataRecArray.length == 3){
                            console.log('@@@ '+dataRecArray [0]+' '+dataRecArray [1]+' '+dataRecArray [2]);
                            if(!processApiName.includes(dataRecArray [2]) ){
                                var res;
                                var expectingVal = dataRecArray [0];
                                if(typeOfField == "MULTIPICKLIST"){
                                    console.log('expectingVal '+expectingVal);
                                    if(expectingVal.trim().includes(';'))
                                    {
                                        console.log('@@@ inside if MULTIPICKLIST ');
                                        var loopList = expectingVal.split(';');
                                        console.log('@@@  '+loopList);
                                        console.log('@@@  '+loopList.length);
                                        res = false;
                                        for(var i=0;i<(loopList.length-1);i++){
                                            console.log('originalVal '+originalVal);
                                            console.log('loopList[i] '+loopList[i]);
                                            if(originalVal.trim().includes(loopList[i].trim()))
                                            {
                                                res = true;
                                                break;
                                            }
                                        }
                                    }
                                    else{
                                        //old
                                        console.log('@@@ inside else MULTIPICKLIST ');
                                        console.log('expectingVal '+expectingVal+'originalVal '+originalVal);
                                        res = originalVal.trim().includes(expectingVal.trim());
                                    }
                                }
                                else{
                                    console.log('field type not multipicklist originalVal '+originalVal+' expectingVal '+expectingVal);
                                    res = originalVal.trim()==expectingVal.trim();
                                }
                                console.log(res);
                                if(res){
                                    var className = dataRecArray [1];
                                    for(var cmpchild in component.find("myId")){
                                        var compData1 = component.find("myId")[cmpchild];
                                        var output = compData1.get("v.class");//component.find("myId")[cmpchild].get("v.class");
                                        console.log('output true '+output);
                                        console.log('className true '+className);
                                        
                                        output = output.trim();
                                        className = className.trim();
                                        
                                        console.log(output +'  '+className+'  '+output.search(className) != -1);
                                        if(output.search(className) != -1 && output != '' && className != ''){
                                            console.log('if '+className+' '+output);
                                            console.log('true if '+className);
                                            //new code to make lookup field disebled
                                            var tempfieldName = compData1.get("v.fieldName");
                                            var getfieldType = fieldfieldtypemap.get(tempfieldName);
                                            if(getfieldType == 'REFERENCE'  || getfieldType == 'TEXTAREA')
                                            {
                                                var geteledata = document.getElementById(tempfieldName+'det');
                                                geteledata.classList.remove("hidelookupdiv");
                                            }
                                            compData1.set("v.disabled",false);
                                            /* var fildepandancyOfChield = fielddepandancymap.get(tempfieldName);
                                        var fildepandancyOfChieldvalue = compData1.get("v.value");
                                        if(fildepandancyOfChield != '')
                                        {
                                            helper.calldepandantfieldDepandacy(component, event,helper,fildepandancyOfChieldvalue,tempfieldName);
                                        }*/
                                            
                                            break;
                                        }
                                        
                                    }
                                }
                                else{
                                    var className = dataRecArray [1];
                                    for(var cmpnew in component.find("myId")){
                                        var compData1 = component.find("myId")[cmpnew];
                                        var output = ''+compData1.get("v.class");//component.find("myId")[cmpnew].get("v.class");
                                        console.log('output '+output);
                                        console.log('className '+className);
                                        output = output.trim();
                                        className = className.trim();
                                        if(output.search(className) != -1 && output != '' && className != ''){
                                            console.log('if '+className+' '+output);
                                            //newlly added 06-08-2018
                                            var tempfieldName = compData1.get("v.fieldName");
                                            console.log('tempfieldName '+tempfieldName);
                                            //new code to make lookup field disebled
                                            //var getelementdata = document.getElementById(tempfieldName);
                                            //showErrOnFld.style.display='';
                                            //var getfieldType = getelementdata.dataset.record;
                                            var getfieldType = fieldfieldtypemap.get(tempfieldName);
                                            if(getfieldType == 'REFERENCE'  || getfieldType == 'TEXTAREA')
                                            {
                                                var geteledata = document.getElementById(tempfieldName+'det');
                                                geteledata.classList.add("hidelookupdiv");
                                                //compData1.set("v.value",'');
                                            }
                                            if(document.getElementById(tempfieldName) != null){
                                            document.getElementById(tempfieldName).click();
                                                }
                                            //compData1.change();
                                            //$A.util.addClass(compData, 'depandancydisabled');
                                            compData1.set("v.disabled",true);
                                            //newlly added 24-09-2018 for depandancy is popup not get proparlly 
                                            var compData1originalVal = compData1.get("v.value");
                                            console.log('compData1originalVal '+compData1originalVal+'  tempfieldName '+tempfieldName)
                                            if(compData1originalVal != null && compData1originalVal != 'null' && compData1originalVal != undefined && compData1originalVal != 'undefined')
                                            {
                                                
                                                    isErrorInLastsave = true;
                                               
                                            }
                                            /*newly added @ 10 Sept for proper confirm box for depedency  after save record START
                                            var fildepandancy1 = fielddepandancymap.get(tempfieldName);
                                            var originalValtempfieldName = compData1.get("v.value");
                                            if(fildepandancy1 === ''){
                                                if(originalValtempfieldName != null && originalValtempfieldName != 'null' && originalValtempfieldName != undefined && originalValtempfieldName != 'undefined' && originalValtempfieldName !='' && originalValtempfieldName != 0)
                                                {
                                                    //alert('********** '+tempfieldName+' '+originalValtempfieldName);
                                                    isErrorInLastsave = true;
                                                } 
                                                
                                            }
                                            //newly added @ 10 Sept for proper confirm box for depedency  after save record END*/
                                        }
                                        
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        if(isErrorInLastsave){
            var conf = confirm('Based on your organization\'s current dependency rule setup, one or more of the hidden fields on this record contains data.\n\nTo automatically remove the invalid data and reset the hidden fields to blank, please click OK below.\n\nIf you do not need to edit your record and wish to preserve the current data, please click Cancel below.')
            if(conf== true)
            {
                var requiredFldsData = component.find("myId");
                    if(requiredFldsData != undefined && requiredFldsData != null){
                        for(var cmp in requiredFldsData){
                            var fldDisabledStatus=requiredFldsData[cmp].get("v.disabled"); 
                            var fldname=requiredFldsData[cmp].get("v.fieldName");
                            if(fldDisabledStatus == true && fldname != 'CreatedDate' && fldname != 'CreatedById' && fldname != 'LastModifiedDate' && fldname != 'LastModifiedById' && fldname != 'OwnerId')
                            {
                                requiredFldsData[cmp].set("v.value",null);
                            }
                        }
                    }
                var tempForm = component.find("CLM_EditForm");
                tempForm.submit();
            }
        }
    },
    calldepandantfieldDepandacy:function(component, event,helper,orignalval,fieldname) {
        console.log('calldepandantfieldDepandacy orignalval '+orignalval+' fieldname '+fieldname);
        //alert('callProxyFieldChangeDepandancy');
        var fieldfieldtypemap = component.get("v.FieldVSFieldTypeMap");
        var fielddepandancymap = component.get("v.FieldVSDepandancyMap");
        var fieldnam = fieldname;
        //var fieldnam = event.getSource().getLocalId()
        // alert('fieldnam'+fieldnam);
        var originalVal =orignalval;
        var typeOfField = fieldfieldtypemap.get(fieldnam);
        var fildepandancy = fielddepandancymap.get(fieldnam);
        //  alert('typeOfField '+typeOfField);
        
        if(fildepandancy != '' && originalVal!='' && originalVal!=null && originalVal!=undefined){
            // alert('fildepandancy '+fildepandancy);
            var fieldDependancyArray = fildepandancy.split('SPLITREC');
            // alert('fieldDependancyArray '+fieldDependancyArray );
            var processApiName = '';
            for(var index=0;index < fieldDependancyArray.length;index++){
                
                console.log('fieldDependancyArray[index] '+fieldDependancyArray[index] );
                if(fieldDependancyArray[index].trim() != ''){
                    var dataRec = fieldDependancyArray[index];
                    // alert('dataRec'+dataRec);
                    var dataRecArray = dataRec.split('SPLITVAL');
                    //alert('dataRecArray'+dataRecArray.length);
                    if(dataRecArray.length == 3){
                        //alert('@@@ inside '+dataRecArray [0]+' '+dataRecArray [1]+' '+dataRecArray [2]);
                        // alert('@@@ '+processApiName.includes(dataRecArray [2]));
                        if(!processApiName.includes(dataRecArray [2]) ){
                            var res;
                            var expectingVal = dataRecArray [0];
                            //   alert('@@@ '+expectingVal+'  '+typeOfField);
                            if(typeOfField == "MULTIPICKLIST"){
                                if(expectingVal.trim().includes(';'))
                                {
                                    console.log('@@@ inside if MULTIPICKLIST ');
                                    var loopList = expectingVal.split(';');
                                    console.log('@@@  '+loopList);
                                    console.log('@@@  '+loopList.length);
                                    res = false;
                                    for(var i=0;i<(loopList.length-1);i++){
                                        if(originalVal.trim().includes(loopList[i].trim()))
                                        {
                                            res = true;
                                            break;
                                        }
                                    }
                                }
                                else{
                                    //  alert('@@@ inside else MULTIPICKLIST ');
                                    res = originalVal.trim().includes(expectingVal.trim());
                                    
                                }
                            }
                            else{
                                res = originalVal.trim()==expectingVal.trim();
                            }
                            console.log('res '+res);
                            if(res){
                                var className = dataRecArray [1];
                                for(var cmp in component.find("myId")){
                                    var compData = component.find("myId")[cmp];
                                    var output = compData.get("v.class");//component.find("myId")[cmp].get("v.class");
                                    console.log('true'+output);
                                    console.log('true'+className);
                                    if(output.search(className) != -1 && output != '' && className != ''){
                                        //new code to make lookup field disebled
                                        var tempfieldName = compData.get("v.fieldName");
                                        var getfieldType = fieldfieldtypemap.get(tempfieldName);
                                        console.log('tempfieldName '+tempfieldName+'getfieldType '+getfieldType+'tempfieldName '+tempfieldName);
                                        if(getfieldType == 'REFERENCE'  || getfieldType == 'TEXTAREA')
                                        {
                                            var geteledata = document.getElementById(tempfieldName+'det');
                                            geteledata.classList.remove("hidelookupdiv");
                                        }
                                        var fildepandancyOfChield = fielddepandancymap.get(tempfieldName);
                                        var fildepandancyOfChieldvalue = compData.get("v.value");
                                        if(fildepandancyOfChield != '')
                                        {
                                            helper.calldepandantfieldDepandacy(component, event,helper,fildepandancyOfChieldvalue,tempfieldName);
                                        }
                                        compData.set("v.disabled",false);
                                        break;
                                    }
                                }
                            }
                            else{
                                var className = dataRecArray [1];
                                for(var cmp in component.find("myId")){
                                    var compData = component.find("myId")[cmp];
                                    var output = compData.get("v.class");//component.find("myId")[cmp].get("v.class");
                                    console.log('@@@@ output '+output+' className '+className);
                                    if(output.search(className) != -1 && output != '' && className != ''){
                                        var tempfieldName = compData.get("v.fieldName");
                                        //new code to make lookup field disebled
                                        var getfieldType = fieldfieldtypemap.get(tempfieldName);
                                        console.log('tempfieldName '+tempfieldName+'getfieldType '+getfieldType+'tempfieldName '+tempfieldName);
                                        if(getfieldType == 'REFERENCE'  || getfieldType == 'TEXTAREA')
                                        {
                                            var geteledata = document.getElementById(tempfieldName+'det');
                                            geteledata.classList.add("hidelookupdiv");
                                        }
                                        console.log('tempfieldName '+tempfieldName);
                                        if(document.getElementById(tempfieldName) != null)
                                        {
                                        document.getElementById(tempfieldName).click();
                                            }
                                        compData.set("v.disabled",true);
                                    }
                                    
                                }
                            }
                            
                        }
                    }
                }
                
            }
            
        }
    }
})