@RestResource(urlMapping='/EGOSAttachement/*')
global with sharing class EGOSAttachement {
    @HttpPost
    global static String doPost(){
        try{
            RestRequest req = RestContext.request;            
            Blob inputBody = req.requestBody;
            Map<String,String> headerMap = req.headers;
            String fileName = headerMap.get('fileName');    
            System.debug('fileName = '+fileName);
            String taskOrderId = headerMap.get('taskOrderId');
            System.debug('taskOrderId = '+taskOrderId);
            // String EncodingUtil.base64Encode(inputBody );            
            //System.debug(''+inputBody.toString());
            attachDocTO(inputBody ,fileName ,taskOrderId); 
        }
        catch(Exception ee){
            system.debug('Exception = '+ee);            
        } 
        return '';    
    }
    
   private static String attachDocTO(Blob inputBody ,String fileName ,String taskOrderId ){
    
        Attachment attach = new Attachment();
        String fileNameWithoutExtension = fileName.substringBefore('.').trim();
        System.debug('fileName = '+fileName );
        System.debug('fileNameWithoutExtension = '+fileNameWithoutExtension );
        List<Attachment> attachList;
        if(Schema.sObjectType.Attachment.isAccessible()){
            if(fileNameWithoutExtension != null && fileNameWithoutExtension.length() > 0){
                
                    attachList = [Select Id From Attachment where parentId =: taskOrderId And (name =: fileName.trim() OR name =:fileNameWithoutExtension)];
            }
            else{
                attachList = [Select Id From Attachment where parentId =: taskOrderId And name =: fileName.trim() ];
            }
        }
        if(attachList != null && !attachList .isEmpty()){
            DMLManager.deleteAsUser(attachList);
           //delete attachList ;
        }
       
        attach.ParentId =taskOrderId ;
        attach.Name= fileName.trim();
        //attach.ContentType = 'application/pdf';
        attach.body = inputBody;
        try{
            DMLManager.insertAsUser(attach);
            //insert attach ;
            return 'success';
        }
        catch(Exception ex){
            system.debug('Exception = '+ex);
            return 'failed';
            
        }
        
    }
}