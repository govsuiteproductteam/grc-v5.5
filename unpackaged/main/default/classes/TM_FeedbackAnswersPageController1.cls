global  with sharing class TM_FeedbackAnswersPageController1 {
    private Id taskQuestionairId;
    private List<Feedback__c> feedbackList;
    private List<Survey_Answer__c> surveyAnsList;    
    private static List<String> optionAPIList = new List<String>{'Option_1__c', 'Option_2__c', 'Option_3__c', 'Option_4__c', 'Option_5__c', 'Option_6__c', 'Option_7__c', 'Option_8__c', 'Option_9__c', 'Option_10__c'};
    //public List<surveyAnswrapper> surveyAnsWralist{get;set;}
    public  map<id,List<SurveyAnsWrapper>> questionIdSurveyWrapListMap{get;set;}
    public map<id,Question__c> questionIdMap{get;set;}
    public String returnUrltask{get;set;}
    public String returnUrlTaskSurvey{get;set;}
    private List<Task_Order_Questionnaire__c> communityUrlList;
    public String isValidUser{get; set;}
    
    public Survey_Task__c currentSurveyTask{get;set;}
    public TM_FeedbackAnswersPageController1(ApexPages.StandardController controller){
        isValidUser = LincenseKeyHelper.checkLicensePermitionForFedTOM();
        if(isValidUser != 'Yes')
            return;
       if(Schema.sObjectType.Survey_Task__c.isAccessible() && Schema.sObjectType.Feedback__c.isAccessible() && Schema.sObjectType.Survey_Answer__c.isAccessible() && Schema.sObjectType.User.isAccessible() && Schema.sObjectType.Question__c.isAccessible() ){
            init();
       }
    }
    
    //  init() method is used to display feedback of qusetions with survey name.
    public void init(){
        taskQuestionairId = ApexPages.CurrentPage().getparameters().get('id');
        currentSurveyTask = [SELECT Name,Task_Order__r.Name, Task_Order__c, Survey__r.Name, Survey__c, Contract_Vehicle__c FROM Survey_Task__c WHERE id=:taskQuestionairId];
        feedbackList = [SELECT id , User__c, Contact__c, Company_Name__c FROM Feedback__c WHERE Survey_Task__c =:taskQuestionairId];
        if(!feedbackList.isEmpty()){            
            map<id, Feedback__c> feedBackIdMap = new map<id, Feedback__c>(feedbackList);
            questionIdSurveyWrapListMap = new map<id,List<SurveyAnsWrapper>>();
            surveyAnsList = [SELECT id, Feedback__c, Feedback__r.User__r.name, Feedback__r.Company_Name__r.name,Feedback__r.Contact__r.name, Feedback__r.Date_Response_Received__c, Feedback__r.Date_Teaming_Request_sent__c, Question__c, Question__r.question__c, Answer_Descriptive__c, Survey_Taken__c, Option_1__c, Option_2__c, Option_3__c, Option_4__c, Option_5__c, Option_6__c, Option_7__c, Option_8__c, Option_9__c, Option_10__c FROM Survey_Answer__c WHERE Feedback__c In : feedBackIdMap.keySet()];
            
            for(Survey_Answer__c surveyAns : surveyAnsList){
                if(questionIdSurveyWrapListMap.get(surveyAns.Question__c)!=Null){
                    questionIdSurveyWrapListMap.get(surveyAns.Question__c).add(new SurveyAnsWrapper(surveyAns));
                }else{
                    List<SurveyAnsWrapper> temSurveyAnsList = new List<SurveyAnsWrapper>{new SurveyAnsWrapper(surveyAns)};
                    questionIdSurveyWrapListMap.put(surveyAns.Question__c, temSurveyAnsList);
                }  
                                 
            }
            
            questionIdMap = new map<id,Question__c>([SELECT Question__c FROM Question__c WHERE Id In :questionIdSurveyWrapListMap.keySet() Order by Sequence__c ASC]);
        }
        
        communityUrlList = [SELECT id, Customer_Community_URL__c, Partner_Community_URL__c FROM Task_Order_Questionnaire__c LIMIT 1];
        
        User usr = [SELECT id, contactId,Profile.UserLicense.Name FROM User WHERE id = : UserInfo.getUserId()];
        
        if(usr.Profile.UserLicense.Name.containsIgnoreCase('partner')){
            if(communityUrlList != null && communityUrlList.size() > 0){
                // returnUrltask = 'partnet/'+ currentSurveyTask.Task_Order__c;
                returnUrltask = communityUrlList[0].Partner_Community_URL__c + '/' +  currentSurveyTask.Task_Order__c;
               // returnUrlTaskSurvey = 'partnet/'+ currentSurveyTask.Survey__c;
               returnUrlTaskSurvey = communityUrlList[0].Partner_Community_URL__c + '/' + currentSurveyTask.Survey__c;
            }
        }
        else{
            if(usr.Profile.UserLicense.Name.containsIgnoreCase('Customer')){
                if(communityUrlList != null && communityUrlList.size() > 0){
                    //returnUrltask = 'Customer/'+ currentSurveyTask.Task_Order__c;
                    returnUrltask =  communityUrlList[0].Customer_Community_URL__c + '/' + currentSurveyTask.Task_Order__c;
                    //returnUrlTaskSurvey = 'Customer/'+ currentSurveyTask.Survey__c;
                    returnUrlTaskSurvey = communityUrlList[0].Customer_Community_URL__c + '/' +  currentSurveyTask.Survey__c;
                }
            }
            else{
                returnUrltask = ''+ currentSurveyTask.Task_Order__c;
                returnUrlTaskSurvey = ''+currentSurveyTask.Survey__c;
            }
        }
        
    }
   
    /******************************** Wrapper class *************************************************/ 
    public class SurveyAnsWrapper{
        public Survey_Answer__c surveyAnswer{get;set;}
        public List<String> ansList{get;set;}
        public String descriptiveAns{get;set;}
       
        
        public SurveyAnsWrapper(Survey_Answer__c surveyAnswer){
            this.surveyAnswer = surveyAnswer;
            if(surveyAnswer.Answer_Descriptive__c!=Null){
                descriptiveAns = surveyAnswer.Answer_Descriptive__c;
            }else{
                ansList = new List<String>();
                for(String str : TM_FeedbackAnswersPageController1.optionAPIList){
                    if(surveyAnswer.get(str)!=Null && surveyAnswer.get(str)!=''){
                        ansList.add(''+surveyAnswer.get(str));
                    }
                }
            }
        }
    }
    /***********************************************************************************************/
}