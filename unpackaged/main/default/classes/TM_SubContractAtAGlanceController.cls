public class TM_SubContractAtAGlanceController {
    public TM_TOMA__SubContract__c subConVehicle{get;set;}
    public List<CLM_AtaGlanceField__c> conAtAGlanceList{get;set;}
    public String isValidLiecence{get;set;}
    
    public TM_SubContractAtAGlanceController(ApexPages.StandardController controller){
        isValidLiecence = LincenseKeyHelper.checkLicensePermitionForFedCLM();
        if(isValidLiecence == 'Yes'){
            Id recordId = apexpages.currentpage().getparameters().get('id');
            if(recordId != null){
                //String query1='SELECT '+getAllFields('TM_TOMA__SubContract__c')+' FROM TM_TOMA__SubContract__c WHERE '+' id = :recordId';
                //newlly added 24-05-2018 for onlly get at a glance field
                String query1='SELECT '+getAllATAGlanceFields('TM_TOMA__SubContract__c',recordId+'')+' FROM TM_TOMA__SubContract__c WHERE '+' id = :recordId ';
                List<TM_TOMA__SubContract__c> temList = Database.query(query1);
                if(temList != null && ! temList.isEmpty()){
                    subConVehicle=temList[0];
                    if(subConVehicle.RecordTypeId != null){
                        string recId=subConVehicle.RecordTypeId;
                        recId=recId.substring(0,15);
                        conAtAGlanceList= new List<CLM_AtaGlanceField__c>();
                        if(recId != null) {
                            conAtAGlanceList=[select Color__c,ColSpan__c,AppApi__c,Document_Id__c,FieldApiName__c,FieldLabel__c,Is_Component__c,Is_Contract__c,Order__c,RecordType_Id__c from CLM_AtaGlanceField__c where  Is_Contract__c = false AND RecordType_Id__c =: recId ORDER BY Order__c ASC];
                        }         
                        
                        
                    }
                }
            }
        }
    }
    
    public static String getAllFields(String sobjectname){
        System.debug('getAllFields');
        if(!Schema.getGlobalDescribe().containsKey(sobjectname)) return 'Exception';
        Map<String, Schema.SObjectField> fields = Schema.getGlobalDescribe().get(sobjectname).getDescribe().SObjectType.getDescribe().fields.getMap();
        List<String> accessiblefields = new List<String>();
        for(Schema.SObjectField field : fields.values()){
            if(field.getDescribe().isAccessible())
                accessiblefields.add(field.getDescribe().getName());
        }
        String allfields='';
        for(String fieldname : accessiblefields)
            allfields += fieldname+',';
        allfields = allfields.subString(0,allfields.length()-1);
        System.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'+allfields);
        return allfields;
    }
    public static String getAllATAGlanceFields(String sobjectname,string recId){//newlly added 24-05-2018
        System.debug('getAllFields');
        if(!Schema.getGlobalDescribe().containsKey(sobjectname)) return 'Exception';
        List<CLM_AtaGlanceField__c> clmAtAGlanceList= new List<CLM_AtaGlanceField__c>();
        string rectypeId;
        set<String> accessiblefields = new set<String>();
        Map<String,Schema.SObjectField> M = new Map<String,Schema.SObjectField>();
        M =  Schema.SObjectType.TM_TOMA__SubContract__c.fields.getMap();
        List<TM_TOMA__SubContract__c> contractVehicle = [SELECT Id,Name,recordTypeId FROM TM_TOMA__SubContract__c WHERE Id =:recId limit 1];
        if(contractVehicle[0].recordTypeId != Null)
        {
            rectypeId=contractVehicle[0].recordTypeId;
            rectypeId=rectypeId.substring(0,15);
        }
        if(rectypeId != '')
            clmAtAGlanceList=[select Color__c,ColSpan__c,Document_Id__c,FieldApiName__c,FieldLabel__c,Is_Component__c,Order__c,RecordType_Id__c,Is_Contract__c from CLM_AtaGlanceField__c where RecordType_Id__c=:rectypeId AND Is_Contract__c = false ORDER BY Order__c ASC];
        else
            clmAtAGlanceList=[select Color__c,ColSpan__c,Document_Id__c,FieldApiName__c,FieldLabel__c,Is_Component__c,Order__c,RecordType_Id__c,Is_Contract__c from CLM_AtaGlanceField__c where Is_Contract__c = false ORDER BY Order__c ASC];
        if(clmAtAGlanceList != null){
            for(CLM_AtaGlanceField__c clmAt:clmAtAGlanceList)
            {
                if(!clmAt.Is_Component__c)
                {
                    Schema.SObjectField field = M.get(clmAt.FieldApiName__c.trim());
                    if(field.getDescribe().isAccessible())
                        accessiblefields.add(field.getDescribe().getName());
                }
            }
        }
        String allfields='';
        if(accessiblefields != null){
            for(String fieldname : accessiblefields)
                allfields += fieldname+',';
        }
        System.debug('allfields '+allfields);
        if(!allfields.contains('RecordTypeId,'))//use to adde RecordTypeId field to get data in RecordTypeId in consturctor
        {
            System.debug('inside if not contain ,RecordTypeId, ');
            allfields += 'RecordTypeId,';
        }
        System.debug('allfields '+allfields);
        allfields = allfields.subString(0,allfields.length()-1);
        return allfields;
    }
}