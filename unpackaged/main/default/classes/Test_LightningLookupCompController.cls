@IsTest
public class Test_LightningLookupCompController {
    static testMethod void testLightningLookupCompCont(){
        Account acc =  TestDataGenerator.createAccount('TestAcc');
        insert acc;
        Account acc1 =  TestDataGenerator.createAccount('TestAcc1');
        insert acc1;
        List<SearchResult> serRes = LightningLookupCompController.getDataForLookup('Account');
        System.assertEquals(serRes.size() , 2);
        LightningLookupCompController.getNameFromId('Account',acc.Id);
        serRes = LightningLookupCompController.lookup('Test', 'Account');
        System.assertEquals(serRes.size() , 2);
        LightningLookupCompController.getNameFromId('Group','');
        
    }
}