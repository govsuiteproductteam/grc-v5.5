CLASSIFICATION: UNCLASSIFIED
This a Request for Information (RFI) regarding the potential C4ISR Systems &
CYBER Technology Training & Fielding Support acquisition. The Army
Contracting Command - Aberdeen Proving Ground (ACC-APG) is requesting
information and conducting market research in order to identify any/all RS3
prime contractors that are able to provide the required services.
All responses shall be submitted no later than Wednesday, 09 November 2017
at 1:00pm EST via email to Johnna.e.frantz.civ@mail.mil. All communications
shall be submitted in writing to johnna.e.frantz.civ@mail.mil only.
Contractors shall not contact any other Government personnel regarding this
RFI or the potential acquisition other than the individual designated above.
Contacting any Government personnel other than the individual designated
above may result in an organizational conflict of interest (OCI) and may
result in a contractor being excluded from the competition and consideration
for award.
Please see the attached RFI questions and draft Performance Work Statement
(PWS). The Government expects to make a lot of changes to the PWS format
before release of the RFP, we ask that respondents to this RFI provide
feedback on the content of the PWS so we can ensure a complete and accurate
requirement is released with the RFP.
Respectfully,
Johnna E. Frantz
Senior Contract Specialist
Army Contracting Command - APG
Division E
Currently Located at
Intelligence, Electronic Warfare & Sensors Directorate
Communications-Electronics Command Software Engineering Center
6006 Combat Drive
Aberdeen Proving Ground, MD 21005
Phone: (443) 861-3805
johnna.e.frantz.civ@mail.mil