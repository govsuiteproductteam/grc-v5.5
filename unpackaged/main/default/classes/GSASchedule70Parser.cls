//updated on july, 19 2016 
public class GSASchedule70Parser {
   
    private Map<String,List<String>> recordMap = new Map<String,List<String>>();
    private static final String RFQ_ID = 'RFQ ID';
    private static final String ACTION = 'Action';
    private static final String DATE_BUYER = 'Date/Buyer';
    private static final String QUOTES_DUE_BY = 'Quotes Due By';
    private static final String RFQ_TITLE = 'RFQ Title';  
    
    public GSASchedule70Parser(){
        init();
    }
    
    private void init(){
        recordMap.put(RFQ_ID, new List<String>());
        recordMap.put(ACTION, new List<String>());
        recordMap.put(DATE_BUYER, new List<String>());
        recordMap.put(QUOTES_DUE_BY, new List<String>());
        recordMap.put(RFQ_TITLE, new List<String>());
    }
    
    public void parse(Messaging.InboundEmail email){      
        System.debug('inside parse method');
        String emailHTMLBody = email.htmlBody;
        //System.debug('emailBody = '+emailHTMLBody);
        if(emailHTMLBody.contains('<tbody>')){
            String recordTable = emailHTMLBody.substringBetween('<tbody>','</tbody>');
            //System.debug('recordTable = '+recordTable);
            if(emailHTMLBody.contains('</tr>')){
                String[] rows = recordTable.split('</tr>');
                Integer size = rows.size();
                For(Integer index = 1; index < size; index++){//loop is starting from index = 1,coz, rows[0] is a header
                    //System.debug('Row'+index+' : '+rows[index].normalizeSpace());
                    String row = rows[index].replace('<tr>', '');            
                    insertDataInMap(row);
                }
                //System.debug('recordMap = '+recordMap);
            }
        }                
    }
    
    private void insertDataInMap(String row){
        if(row.contains('</td>')){
            String[] columns = row.split('</td>');
            if(columns.size() > 4){
                System.debug(LoggingLevel.DEBUG,''+columns[0].stripHtmlTags().trim());
                if(getTaskOrderNumber(columns[0].stripHtmlTags().trim()) != null){                    
                    recordMap.get(RFQ_ID).add(columns[0].stripHtmlTags().trim());
                    recordMap.get(ACTION).add(columns[1].stripHtmlTags().trim());
                    recordMap.get(DATE_BUYER).add(columns[2].stripHtmlTags().trim());
                    recordMap.get(QUOTES_DUE_BY).add(columns[3].stripHtmlTags().trim());
                    recordMap.get(RFQ_TITLE).add(columns[4].stripHtmlTags().trim());
                }
            }
        }
    }
    
    public List<String> getRFQIds(){
        return recordMap.get(RFQ_ID);    	
    }
    
    public Task_Order__c getTaskOrder(String toAddress,Integer index){
        DateTimeHelper dtHelper = new DateTimeHelper();       
        String rfqId = recordMap.get(RFQ_ID).get(index);
        
        if( rfqId == null || rfqId.trim().length()==0){
            System.debug('*********null');
            return null;
        }
        List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];    
        Task_Order__c taskOrder = new Task_Order__c();
        taskOrder.Contract_Vehicle__c = conVehicalList[0].Id;
        taskOrder.Contract_Vehicle_picklist__c = 'SCHEDULE 70 - GSA SCHEDULE 70 - IT EQUIPMENT, SOFT';//Contract Vehicle
        taskOrder.Name = checkValidString(rfqId,79);
        
        String act = recordMap.get(ACTION).get(index);    
        if(act!= null && act.trim().length() > 0){
            taskOrder.ASB_Action__c = act;
        }        
        String dateBuyer = recordMap.get(DATE_BUYER).get(index);
        if(dateBuyer!= null && dateBuyer.trim().length() > 0){            
            taskOrder.Release_Date__c = dtHelper.getDateTime(dateBuyer).date();
        }
        String quoteDuByeDate = recordMap.get(QUOTES_DUE_BY).get(index);
        if(quoteDuByeDate!= null && quoteDuByeDate.trim().length() > 0){            
            taskOrder.Due_Date__c = dtHelper.getDateTime(quoteDuByeDate).date(); 
            taskOrder.Proposal_Due_DateTime__c= dtHelper.getDateTime(quoteDuByeDate);
        }            
        String rfqTitle = recordMap.get(RFQ_TITLE).get(index);
        if(rfqTitle!= null && rfqTitle.trim().length() > 0 ){
            taskOrder.Task_Order_Title__c = rfqTitle;            
        }     
        
        return taskOrder;
    }
    
    public Task_Order__c updateTaskOrder(Task_Order__c taskOrder, String toAddress,Integer index){
        DateTimeHelper dtHelper = new DateTimeHelper();        
        String rfqId = recordMap.get(RFQ_ID).get(index);
       
        List<Contract_Vehicle__c> conVehicalList = [SELECT id,Account_Name__c FROM Contract_Vehicle__c WHERE Incoming_Task_Order_Mailbox__c=:toAddress];    
        //Task_Order__c taskOrder = new Task_Order__c();
        taskOrder.Contract_Vehicle__c = conVehicalList[0].Id;
        taskOrder.Contract_Vehicle_picklist__c = 'SCHEDULE 70 - GSA SCHEDULE 70 - IT EQUIPMENT, SOFT';//Contract Vehicle
        //taskOrder.Name = checkValidString(rfqId,79);
        
        String act = recordMap.get(ACTION).get(index);    
        if(act!= null && act.trim().length() > 0){
            taskOrder.ASB_Action__c = act;
        }        
        String dateBuyer = recordMap.get(DATE_BUYER).get(index);
        if(dateBuyer!= null && dateBuyer.trim().length() > 0){            
            taskOrder.Release_Date__c = dtHelper.getDateTime(dateBuyer).date();
        }
        String quoteDuByeDate = recordMap.get(QUOTES_DUE_BY).get(index);
        if(quoteDuByeDate!= null && quoteDuByeDate.trim().length() > 0){            
            taskOrder.Due_Date__c = dtHelper.getDateTime(quoteDuByeDate).date(); 
            taskOrder.Proposal_Due_DateTime__c= dtHelper.getDateTime(quoteDuByeDate);
        }            
        String rfqTitle = recordMap.get(RFQ_TITLE).get(index);
        if(rfqTitle!= null && rfqTitle.trim().length() > 0 ){
            taskOrder.Task_Order_Title__c = rfqTitle;            
        }     
        
        return taskOrder;
    }
    
    private string getTaskOrderNumber(String emailSubject){
        String[] words = emailSubject.split(' ');
        System.debug(''+words);
        String taskOrderNum = '';
        //taskOrderNum.las
        for(Integer i = 0; i<words.size();i++){
            if(words[i].startsWith('RF')){
                /*while(words[i].length() !=0){                    
                    try{
                        Integer index = words[i].length();
                        String temp = words[i].substring(index -2);
                        if(temp.startsWith('-')){
                            taskOrderNum += words[i]; 
                            return taskOrderNum;
                        }
                        Integer.valueOf(temp);
                        taskOrderNum += words[i]; 
                        return taskOrderNum;
                    }catch(Exception e){
                        taskOrderNum += words[i]+' ';
                        i++;
                        continue;
                    }
                    
                }*/
                return words[i].trim();
            }
        }
        return null;
    }
    
    private String checkValidString(String str, Integer endLimit){
        if(str.length() > endLimit){
            String validString = str.substring(0,endLimit);
            return validString;
        }
        return str;
    } 
}