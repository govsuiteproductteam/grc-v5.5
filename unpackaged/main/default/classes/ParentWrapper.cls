public class ParentWrapper{
         
        @AuraEnabled 
        public List<ContactWrapper> conWrapList{get;set;}
        @AuraEnabled 
        public List<SurveyWrapper> surveyWrapList{get;set;}
        @AuraEnabled 
        public List<EmailWrapper> emailWrapList{get;set;}
        @AuraEnabled 
        public List<AttachmentWrapper> attachWrapList{get;set;}
        
        
        public parentWrapper(List<ContactWrapper> conWrapList,List<SurveyWrapper> surveyWrapList,List<EmailWrapper>emailWrapList,List<AttachmentWrapper> attachWrapList){
            this.conWrapList= conWrapList;
            this.surveyWrapList = surveyWrapList;
            this.emailWrapList=emailWrapList;
            this.attachWrapList=attachWrapList;
        }
    }