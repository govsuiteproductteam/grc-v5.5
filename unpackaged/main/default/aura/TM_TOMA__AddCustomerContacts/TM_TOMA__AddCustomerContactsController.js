({
    checkAccess : function(component, event, helper) {
        //component.set("v.CheckLicense",true);
        var action = component.get("c.checkLicensePermition");         
        action.setCallback(this,function(a){
            var state= a.getState();
            if(state=="SUCCESS"){
                //alert(a.getReturnValue());
                component.set("v.CheckLicense",a.getReturnValue());
                if(a.getReturnValue() =='Yes'){
                    helper.getRole1(component, event, helper);
                    helper.getVehicalPatnerList1(component, event, helper);
                }
            }
        });
        $A.enqueueAction(action);  
    },
    getVehicalPatnerList : function(component, event, helper) {
        helper.getVehicalPatnerList1(component, event, helper);
    },
    getRole: function(component, event, helper) {
        helper.getRole1(component, event, helper);
    },
    editInlineRole : function(component, event, helper) {
        var selectedItem = event.currentTarget;
        var checkCounter = selectedItem.id;
        var d = document.getElementById(checkCounter+'editConInlineOut');
        d.className += " slds-hide";
        d.classList.remove("slds-show");
        var d1 = document.getElementById(checkCounter+'editConInlineIn');
        d1.className += " slds-show";
        d1.classList.remove("slds-hide");
        var d2 = document.getElementById(checkCounter+'editConDiv');
        d2.className += " slds-hide";
        d2.classList.remove("slds-show");
        var d3 = document.getElementById(checkCounter+'editConUndoDiv');
        d3.className += " slds-show";
        d3.classList.remove("slds-hide");
        
        var checksave = component.get("v.checkSavebutton");
        checksave=checksave+1;
        component.set("v.checkSavebutton",checksave);
        document.getElementById(checkCounter+'editCon').dataset.arg1 = selectedItem.dataset.arg1;
    },
    closeInlineRole : function(component, event, helper) {
        var selectedItem = event.currentTarget;
        var checkCounter = selectedItem.id;
        var oldval = selectedItem.dataset.arg1;
        //event.currentTarget.dataset.caseID;
        var d = document.getElementById(checkCounter+'InlineOut');
        d.className += " slds-show";
        d.classList.remove("slds-hide");
        var d1 = document.getElementById(checkCounter+'InlineIn');
        d1.className += " slds-hide";
        d1.classList.remove("slds-show");
        var d2 = document.getElementById(checkCounter+'Div');
        d2.className += " slds-show";
        d2.classList.remove("slds-hide");
        var d3 = document.getElementById(checkCounter+'UndoDiv');
        d3.className += " slds-hide";
        d3.classList.remove("slds-show");
        
        var checksave = component.get("v.checkSavebutton");
        checksave=checksave-1;
        component.set("v.checkSavebutton",checksave);
        var index = selectedItem.dataset.arg2;
        var temp = component.get("v.customerContactList");
        if(temp != undefined){
            for(var i=0;i<temp.length;i++)
            {
                if(index === i+'')
                {
                    temp[i].TM_TOMA__Role__c = oldval;
                }
            }
            component.set("v.customerContactList",temp);
        }
    },
    selectCurrentValue :function(component, event, helper){
        var temp = component.get("v.customerContactList");
        if(temp != undefined){
            for(var i=0;i<temp.length;i++)
            {
                if(event.getSource().get("v.text") === i)
                {
                    temp[i].TM_TOMA__Role__c = event.getSource().get("v.value");
                }
            }
            var obj=component.set("v.customerContactList",temp);
        }
    },
    addCustomerContact : function(component, event, helper) {
        var customerContactList1=component.get("v.customerContactList");
        var action = component.get("c.addNewVehicalPartner"); 
        action.setParams({
            "contractVehicalId": component.get("v.recordId")
        });
        action.setCallback(this,function(a){
            var state= a.getState();
            if(state=="SUCCESS"){
                var newVal=a.getReturnValue();
                if(newVal.Id === '' || newVal.Id === null || newVal.Id === undefined){
                    newVal.TM_TOMA__Name__c='';
                }
                customerContactList1.push(newVal);
                component.set("v.customerContactList",customerContactList1);
                var checksave = component.get("v.checkSavebutton");
                checksave=checksave+1;
                component.set("v.checkSavebutton",checksave);
            }
            else if (state == "ERROR") 
            {
                console.log(a.getError());            
            }
        });
        $A.enqueueAction(action);
    },
    editVehicalPatner : function (component, event, helper) {
        var selectedItem = event.currentTarget;
        var tempId = selectedItem.id;
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            "recordId": tempId
        });
        editRecordEvent.fire();
    },
    
    removeCustomerContact : function(component, event, helper) {
        //alert("inside remove");
        /*  var conf = confirm("Do you really want to delete it");
        if(conf== true)
        {*/
        var customerContactList1=component.get("v.customerContactList");
        var selectedItem = event.currentTarget;
        var removeCounter = selectedItem.id;
        var tempCont = selectedItem.dataset.arg1;
        if (removeCounter != undefined && removeCounter != '') {
            var action = component.get("c.deleteCustomerContact");
            action.setParams({ 
                "delId": removeCounter
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                if (state === "SUCCESS")
                {
                    helper.getVehicalPatnerList1(component, event, helper);
                    //alert('Record Deleted Successfully');
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "Customer Contact Was Deleted",
                        "type":"success"
                    });
                    toastEvent.fire();
                }
                else if (state === "ERROR") 
                {
                    console.log(response.getError());   
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": "Error occurred while deleting the record.Please refresh the page.",
                        "type":"error"
                    });
                    toastEvent.fire();
                }
            });
            $A.enqueueAction(action);
        } 
        else{
            var checksave = component.get("v.checkSavebutton");
            checksave=checksave-1;
            component.set("v.checkSavebutton",checksave);
            customerContactList1.splice(tempCont, 1);
            component.set("v.customerContactList", customerContactList1);
        }
    },
    showContact : function (component, event, helper) {
        var selectedItem = event.currentTarget;
        var tempId = selectedItem.id;
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": tempId,
            "slideDevName": "detail"
        });
        navEvt.fire();
    },
    
    saveCustomerContact1 : function (component, event, helper){
        document.getElementById("saveCustContact").disabled = true;
        var invalidDateRow = ''
        var valTableList=component.get("v.customerContactList");
        var partnerDataString = '[';
        var checkPartner = 'No';
        if(valTableList != undefined){
            for (var i=0; i<valTableList.length; i++){        
                
                var vTId = valTableList[i].Id;
                var ContractVehicle = component.get("v.recordId");
                var Partner = valTableList[i].TM_TOMA__Name__c;
                var Role__c  = valTableList[i].TM_TOMA__Role__c === undefined?'':valTableList[i].TM_TOMA__Role__c;
                var appendData = '';
                if(Partner===undefined || Partner==='' || Partner===null)
                {
                    checkPartner = 'Yes';
                    invalidDateRow=invalidDateRow+','+i;
                }
                partnerDataString += vTId+'SPLITDATA'+ContractVehicle+'SPLITDATA'+Partner+'SPLITDATA'+Role__c+'SPLITPARENT';
            }
        }
        if(checkPartner === 'No'){
            var action = component.get("c.saveCustomerContact");
            action.setParams({ 
                "customerContactstrList": partnerDataString
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                if (state === "SUCCESS")
                {
                    helper.getVehicalPatnerList1(component, event, helper);
                    component.set("v.checkSavebutton",0);
                    document.getElementById("saveCustContact").disabled = false;
                    // alert("Data save successfully");
                    /*  var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "Data Updated successfully.",
                        "type":"success"
                    });
                    toastEvent.fire(); */
                }
                else if (state === "ERROR") 
                {
                    component.set("v.checkSavebutton",0);
                    document.getElementById("saveCustContact").disabled = false;
                    console.log(response.getError());            
                }
            });
            $A.enqueueAction(action);
        }
        else{
            var checkId = invalidDateRow.split(',');
            if(checkId != undefined){
                for(var i = 1; i < checkId.length; i++) {
                    var d = document.getElementById(checkId[i]+'vehiclecon');
                    d.className += " validationClass";
                }
            }
            document.getElementById("saveCustContact").disabled = false;
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "message": "Please enter value for Contact.",
                "type":"error"
            });
            toastEvent.fire();
        }
    }
})