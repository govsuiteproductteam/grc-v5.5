@IsTest
public class Test_TM_ContractAtAGlanceController {
    static testMethod void testMethodContractAtAGlanceController(){
        Test.startTest();
        List<RecordType> rtypes = [Select Name, Id From RecordType where sObjectType='TM_TOMA__Contract_Vehicle__c' and isActive=true];
        string recId='';
        if(rtypes.size()>0)
        {
            recId=rtypes[0].Id;
        }
        CLM_AtaGlanceField__c newOppAtAGlance = TestDataGenerator.createAtaGlanceField(recId);
        newOppAtAGlance.Is_Contract__c=true;
        insert newOppAtAGlance;
        CLM_AtaGlanceField__c newOppAtAGlance1 = TestDataGenerator.createAtaGlanceField(recId);
        newOppAtAGlance1.Is_Contract__c=true;
        newOppAtAGlance1.FieldApiName__c='TM_TOMA__Contracting_Agency__c';
        newOppAtAGlance1.FieldLabel__c='Contracting Organization';
        newOppAtAGlance1.Order__c=2;
        insert newOppAtAGlance1; 
        CLM_AtaGlanceField__c newOppAtAGlance2 = TestDataGenerator.createAtaGlanceField(recId);
        newOppAtAGlance2.Is_Contract__c=true;
        newOppAtAGlance2.FieldApiName__c='TestComponent';
        newOppAtAGlance2.FieldLabel__c='TestComponent';
        newOppAtAGlance2.Is_Component__c=true;
        newOppAtAGlance2.Order__c=3;
        insert newOppAtAGlance2; 
        
        Account acc = TestDataGenerator.createAccount('testAccount');
        insert acc;
        
        Contract_Vehicle__c conVeh = TestDataGenerator.createContractVehicle('Test contract vehicle', acc);
        conVeh.recordTypeId = recId;
        conVeh.Contract_Type__c= TestDataGenerator.getPicklistValue('TM_TOMA__Contract_Vehicle__c','TM_TOMA__Contract_Type__c');
        insert conVeh;
        
        
        PageReference pageRef = Page.TM_ContractAtAGlancePage;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id', conVeh.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(conVeh);
        TM_ContractAtAGlanceController contractAtAGlanceController = new TM_ContractAtAGlanceController(sc);
        System.assertEquals(contractAtAGlanceController.conVehicle.Name, 'Test contract vehicle');
        System.assert(contractAtAGlanceController.conAtAGlanceList.size()>0);
        TM_ContractAtAGlanceController.getAllFields('TM_TOMA__Contract_Vehicle__c');
        Test.stopTest();
    }
}