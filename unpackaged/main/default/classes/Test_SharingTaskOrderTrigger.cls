@isTest
public with sharing class Test_SharingTaskOrderTrigger {
    public static TestMethod void TestSharingTaskOrderTrigger(){
        List<Profile> profileList = [SELECT Id FROM Profile where (Name = 'Custom Partner Community User' OR Name = 'Custom Partner Community Login User' OR Name  = 'Custom Customer Community Plus Login User') Limit 1];
        if(profileList != null && profileList.size()>0){
            Account acc = TestDataGenerator.createAccount('TaskOrderCapture');
            insert acc;
            
            Contact con = TestDataGenerator.createContact('Avanti',acc);
            insert con;
            
            User usr = TestDataGenerator.createUser(con,TRUE,'sharingtest1', 'atest1','sharingtest1@gmail.com','sharingtaskordercapture@demo.com','atest1',profileList[0],'America/Los_Angeles','en_US','UTF-8','en_US');
            insert usr;    
            
            Contract_Vehicle__c contractVehicle = TestDataGenerator.createContractVehicle('TMconVehicle',acc);
            insert contractVehicle;
            
            Vehicle_Partner__c vehicalPartner = TestDataGenerator.createVehiclePartner(acc,contractVehicle);
            insert vehicalPartner;
            
            Contract_Vehicle_Contact__c vehicalContact = TestDataGenerator.createContractVehicleContact(con,contractVehicle);
            insert vehicalContact;
            
            Task_Order__c taskOrder = TestDataGenerator.createTaskOrder('TMtaskOrder', acc, contractVehicle);
            insert taskOrder;
            
            Teaming_Partner__c teamingPartner = TestDataGenerator.createTeamingPartner(vehicalPartner,taskOrder);
            insert teamingPartner;
            
            Task_Order_Contact__c teamingContact = TestDataGenerator.createTaskOrderContact(taskOrder, con);
            insert teamingContact;
            List<Task_Order__share> tOrderShare;
            Test.startTest();
                teamingPartner.Status__c = 'new';
                update teamingPartner;
                tOrderShare = [SELECT Id FROM Task_Order__share];
                System.assertNotEquals(tOrderShare.size(), 0);
                teamingPartner.Status__c = 'Not Selected';
                update teamingPartner;               
            Test.stopTest();
        }
    }
}