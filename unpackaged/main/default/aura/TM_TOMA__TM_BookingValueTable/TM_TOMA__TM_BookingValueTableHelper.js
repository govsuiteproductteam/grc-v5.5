({
    getValueTableBookingListWrapper1: function(component, event, helper) {
        var action = component.get("c.getBookingValueTableList1");
        action.setParams({
            "contractVehicalId": component.get("v.recordId")
        });
        action.setCallback(this, function(a) {
            var state= a.getState();
            if(state=="SUCCESS"){
                console.log("HELLLOOO-"+JSON.stringify(a.getReturnValue()));
                var tempWrap=a.getReturnValue();
                if(tempWrap == null){ 
                    component.set("v.valueTableBookingSizeFlag",0);
                }
                else{
                    component.set("v.valueTableBookingSizeFlag",tempWrap.length);
                    //17-08-2018 issue fixed for Start_Date__c is undefined 
                   /* for(var i = 0; i < tempWrap.length; i++){
                        console.log('Start_Date__c@@@@@@@@@' + tempWrap[i].bookTab.TM_TOMA__Start_Date__c);
                        if(tempWrap[i].bookTab.TM_TOMA__Start_Date__c == undefined){
                            //valueTableList[i].Start_Date__c = 
                            tempWrap[i].bookTab.TM_TOMA__Start_Date__c='';
                        }
                    }*/
                    
                    component.set("v.valueTableBookingListWrap",tempWrap);
                }
            }
            else if (state == "ERROR") 
            {
                console.log(a.getError());            
            }
        })
        $A.enqueueAction(action); 
    },
    savebookingTableList : function (component, event, helper){
        var delBookingId=component.get("v.delBookingId");
        var valTableBookingList=component.get("v.valueTableBookingList");
        var valueTableJSON = '[';
        if(valTableBookingList != undefined){
            for (var i=0; i<valTableBookingList.length; i++){        
                if(valueTableJSON.length>1){
                    valueTableJSON +=  ',{"attributes":{"type":"TM_TOMA__Value_Table__c"},';
                    var vTId = valTableBookingList[i].Id;
                    var Modification__c = valTableBookingList[i].TM_TOMA__Modification__c;
                    var Start_Date__c = valTableBookingList[i].TM_TOMA__Start_Date__c;
                    var Description__c = valTableBookingList[i].TM_TOMA__Description__c;
                    var Increase_to_Total_Element_Value__c  = valTableBookingList[i].TM_TOMA__Increase_to_Total_Element_Value__c === undefined?null:valTableBookingList[i].TM_TOMA__Increase_to_Total_Element_Value__c;
                    var RecordTypeId  = valTableBookingList[i].RecordTypeId ;
                    var Is_this_a_Booking__c  = valTableBookingList[i].TM_TOMA__Is_this_a_Booking__c ;
                    var If_not_a_Booking_why__c  = valTableBookingList[i].TM_TOMA__If_not_a_Booking_why__c === undefined?'':valTableBookingList[i].TM_TOMA__If_not_a_Booking_why__c;
                    var Dollars__c  = valTableBookingList[i].TM_TOMA__Dollars__c === undefined?null:valTableBookingList[i].TM_TOMA__Dollars__c;
                    var appendData = '';
                    if(Start_Date__c==='')
                    {
                        checkValidationError='startDate';
                    }
                    if(!(vTId===undefined))
                    {
                        appendData +='"Id":"'+ vTId+'" ,';
                    }
                    appendData += '"TM_TOMA__Modification__c":"'+ Modification__c+'" , "TM_TOMA__Start_Date__c":"'+Start_Date__c+'","TM_TOMA__Description__c":"'+Description__c+',"TM_TOMA__If_not_a_Booking_why__c":"'+If_not_a_Booking_why__c+'","TM_TOMA__Increase_to_Total_Element_Value__c":'+Increase_to_Total_Element_Value__c+',"TM_TOMA__Is_this_a_Booking__c":'+Is_this_a_Booking__c+',"TM_TOMA__Dollars__c":'+Dollars__c+',"RecordTypeId":"'+RecordTypeId+'" }';
                    valueTableJSON += appendData;
                }else{
                    valueTableJSON +=  '{"attributes":{"type":"TM_TOMA__Value_Table__c"},';
                    var vTId = valTableBookingList[i].Id;
                    var Modification__c = valTableBookingList[i].TM_TOMA__Modification__c;
                    var Start_Date__c = valTableBookingList[i].TM_TOMA__Start_Date__c;
                    var Description__c = valTableBookingList[i].TM_TOMA__Description__c;
                    var Increase_to_Total_Element_Value__c  = valTableBookingList[i].TM_TOMA__Increase_to_Total_Element_Value__c === undefined?null:valTableBookingList[i].TM_TOMA__Increase_to_Total_Element_Value__c;
                    var RecordTypeId  = valTableBookingList[i].RecordTypeId ;
                    var Is_this_a_Booking__c  = valTableBookingList[i].TM_TOMA__Is_this_a_Booking__c ;
                    var If_not_a_Booking_why__c  = valTableBookingList[i].TM_TOMA__If_not_a_Booking_why__c === undefined?'':valTableBookingList[i].TM_TOMA__If_not_a_Booking_why__c;
                    var Dollars__c  = valTableBookingList[i].TM_TOMA__Dollars__c === undefined?null:valTableBookingList[i].TM_TOMA__Dollars__c;
                    var appendData = '';
                    if(Start_Date__c==='')
                    {
                        checkValidationError='startDate';
                    }
                    if(!(vTId===undefined))
                    {
                        appendData +='"Id":"'+ vTId+'" ,';
                    }
                    
                    appendData += '"TM_TOMA__Modification__c":"'+ Modification__c+'" , "TM_TOMA__Start_Date__c":"'+Start_Date__c+'","TM_TOMA__Description__c":"'+Description__c+',"TM_TOMA__If_not_a_Booking_why__c":"'+If_not_a_Booking_why__c+'","TM_TOMA__Increase_to_Total_Element_Value__c":'+Increase_to_Total_Element_Value__c+',"TM_TOMA__Is_this_a_Booking__c":'+Is_this_a_Booking__c+',"TM_TOMA__Dollars__c":'+Dollars__c+',"RecordTypeId":"'+RecordTypeId+'" }';
                    valueTableJSON += appendData;
                }
            } 
            valueTableJSON += ']';   
            var action = component.get("c.saveBookingTable");
            action.setParams({ 
                "bookingTableList": valueTableJSON,
                "contractVehicalId": component.get("v.recordId"),
                "delBookingIdsStr": delBookingId
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                var messageText = response.getReturnValue();
                if (state === "SUCCESS")
                {
                    component.set("v.delBookingId",'');
                    /*var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "Changes were saved successfully.",
                        "type":"success"
                    });
                    toastEvent.fire(); */
                    // "If" condition and "Else" block are newly added to restrict user access according to the object permission(17/08/2018)
                    if(messageText == '' || messageText == undefined || messageText == null){
                        alert("Changes were saved successfully.");
                    }else{
                        alert('ERROR:- ' + messageText);
                    }
                }
                else if (state === "ERROR") 
                {
                    console.log(response.getError());            
                }
            });
            $A.enqueueAction(action);  
        }
    },
    getContractVehicle1 : function(component, event, helper) {
        var action = component.get("c.getContractVehical");
        action.setParams({
            contractVehicalId: component.get("v.recordId")
        });
        action.setCallback(this, function(a) {
            var state= a.getState();
            if(state=="SUCCESS"){
                component.set("v.contract", a.getReturnValue());
            }
            else if (state == "ERROR") 
            {
                console.log(a.getError());            
            }
        })
        $A.enqueueAction(action); 
    },
    getIfNotABookingWhy1: function(component, event, helper) {
        var action = component.get("c.getPickvalIfNotABookingWhy"); //call apex controller method
        var opts = []; //create array
        action.setCallback(this,function(a){
            var state= a.getState();
            if(state=="SUCCESS"){
                var queTypelist = a.getReturnValue();
                component.set("v.IfNotABookingWhy",queTypelist);  //setting value to variable of Input field
            }
            else if (state == "ERROR") 
            {
                console.log(a.getError());            
            }
        });
        $A.enqueueAction(action);  
    },
    validatedate:function(component,event,helper,inputText)
    {
        var dateformat = /(\d{4})-(\d{2})-(\d{2})/;
        // Match the date format through regular expression
        if(inputText.match(dateformat))
        {
            //Test which seperator is used '/' or '-'
            var opera1 = inputText.split('/');
            var opera2 = inputText.split('-');
            var lopera1 = opera1.length;
            var lopera2 = opera2.length;
            // Extract the string into month, date and year
            if (lopera1>1)
            {
                var pdate = inputText.split('/');
            }
            else if (lopera2>1)
            {
                var pdate = inputText.split('-');
            }
            var mm  = parseInt(pdate[1]);
            var dd = parseInt(pdate[2]);
            var yy = parseInt(pdate[0]);
            // Create list of days of a month [assume there is no leap year by default]
            var ListofDays = [31,28,31,30,31,30,31,31,30,31,30,31];
            if (mm==1 || mm>2)
            {
                if (dd>ListofDays[mm-1])
                {
                    return false;
                }
            }
            if (mm==2)
            {
                var lyear = false;
                if ( (!(yy % 4) && yy % 100) || !(yy % 400)) 
                {
                    lyear = true;
                }
                if ((lyear==false) && (dd>=29))
                {
                    return false;
                }
                if ((lyear==true) && (dd>29))
                {
                    return false;
                }
            }
        }
        else
        {
            return false;
        }
    },
    checkValueTablePermissions : function(component, event, helper) {
        //Check "Value Table" object Permissions
        var action = component.get('c.getValueTablePermissions');
        action.setCallback(this,function(response){
            //store state of response
            var state = response.getState();
            console.log("state > "+state);
            if (state === "SUCCESS") {
                component.set('v.bookingTableCtrl', response.getReturnValue());
                var c=response.getReturnValue();
                if(c.isUpdateValueTable===false && c.isCreateValueTable===false && c.isDeleteValueTable===false ){
                    component.set("v.savePermission",false);
                }
                console.log("isUpdateValueTable > "+c.isUpdateValueTable);
            }
        });
        $A.enqueueAction(action);
        
    }
    
})