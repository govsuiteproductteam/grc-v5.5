public with sharing class GSAAttacher{
    
    public static String attachDocTO(Blob inputBody ,String fileName ,String taskOrderId ){
        
        Attachment attach = new Attachment();
        String fileNameWithoutExtension = fileName.substringBefore('.').trim();
        System.debug('fileName = '+fileName );
        System.debug('fileNameWithoutExtension = '+fileNameWithoutExtension );
        List<Attachment> attachList;
        
        
        if(Schema.sObjectType.Attachment.isAccessible()){
            if(fileNameWithoutExtension != null && fileNameWithoutExtension.length() > 0){                
                //Below code will restrict attachment to create on loop rather it will create once a day
                attachList = [Select Id From Attachment where parentId =: taskOrderId And (name =: fileName.trim() OR name =:fileNameWithoutExtension) AND CreatedDate = Today];
                if(!attachList.isEmpty())
                    return '';
                //code end                
                
                attachList = [Select Id From Attachment where parentId =: taskOrderId And (name =: fileName.trim() OR name =:fileNameWithoutExtension)];
            }
            else{
                //Below code will restrict attachment to create on loop rather it will create once a day
                attachList = [Select Id From Attachment where parentId =: taskOrderId And name =: fileName.trim() AND CreatedDate = Today];
                if(!attachList.isEmpty())
                    return '';
                //code end
                
                attachList = [Select Id From Attachment where parentId =: taskOrderId And name =: fileName.trim() ];
            }
        }
        if(attachList != null && !attachList .isEmpty()){
            DMLManager.deleteAsUser(attachList);
            //delete attachList ;
        }
        
        attach.ParentId =taskOrderId ;
        attach.Name= fileName.trim();
        //attach.ContentType = 'application/pdf';
        attach.body = inputBody;
        try{
            DMLManager.insertAsUser(attach);
            //insert attach ;
            return 'success';
        }
        catch(Exception ex){
            system.debug('Exception = '+ex);
            return 'failed';
            
        }
        
    }
}